﻿using EmailMarketingApi.Repositories.Interfaces;

namespace EmailMarketingApi.SchedulerJobs;

public class GmailApiJobs : BackgroundService
{
    private readonly PeriodicTimer _timer = new(TimeSpan.FromMilliseconds(1000 * 60 * 10)); // 10 phut
    private readonly IGmailApiRepository _gmailApiRepository;
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (await _timer.WaitForNextTickAsync(stoppingToken) && !stoppingToken.IsCancellationRequested)
        {
            await JobDayOneMore();
        }
    }
    
    private async Task JobDayOneMore()
    {
        if (DateTime.Now.Hour == 1 && DateTime.Now.Minute <= 15) // phut thu 10 se chay
        {
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "job chay");
            await _gmailApiRepository.ResetSendNumber();
            await _gmailApiRepository.ClearWrapLink();
        }
    }
}