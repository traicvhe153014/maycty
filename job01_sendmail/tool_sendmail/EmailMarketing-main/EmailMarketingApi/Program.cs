using EmailMarketingApi.Models;
using EmailMarketingApi.Repositories;
using EmailMarketingApi.Repositories.Interfaces;
using EmailMarketingApi.SchedulerJobs;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// http product
builder.WebHost.ConfigureKestrel(serverOptions => { serverOptions.ListenLocalhost(5002); });

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Add Repositories
builder.Services.AddScoped<IGmailApiRepository, GmailApiRepository>();

//Add DBContext
builder.Services.AddDbContext<EmailMarketingContext>(opt =>
    opt.UseSqlServer(builder.Configuration.GetConnectionString("ConStr")!,
        options => { options.UseNetTopologySuite(); })
);

// AddHostedService
builder.Services.AddHostedService<GmailApiJobs>();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();