﻿using System.Text;

namespace EmailMarketingApi.Extensions;

public static class General
{
    public static string CreateKeyLink()
    {
        var input = DateTime.Now.ToString("HHmmssddMMyyyy");
        return input.Select(digit => (int)char.GetNumericValue(digit)).Select(num => (char)('a' + num))
            .Aggregate("", (current, letter) => current + letter);
    }
    
    public static string CreateKeyLink2()
    {
        var input = Guid.NewGuid().ToString();
        var result = new StringBuilder();

        foreach (var c in input.Where(char.IsLetter))
        {
            result.Append(c);
        }

        return result.ToString();
    }
    
}