﻿using AutoMapper;

namespace EmailMarketingApi.Extensions;

public static class AutoMapperHelper
{
    public static List<TDestination> MapList<TSource, TDestination>(List<TSource> sourceList)
    {
        var config = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<TSource, TDestination>();
        });

        var mapper = config.CreateMapper();
        return mapper.Map<List<TDestination>>(sourceList);
    }
}