﻿using EmailMarketingApi.Models;

namespace EmailMarketingApi.DTO.Response;

public class CreateWrapLinkResponse
{
    public IEnumerable<WrapLink>? WrapLinks { get; set; }
    public string WrapLink { get; set; } = default!;
}