﻿namespace EmailMarketingApi.DTO.Other;

public class SubmitDataFbAccount
{
    public string? FullName { get; set; } = default!;
    public string? BusinessEmailAddress { get; set; } = default!;
    public string? PersonalEmailAddress { get; set; } = default!;
    public string? MobilePhoneNumber { get; set; } = default!;
    public string? FacebookPageName { get; set; } = default!;
    public string? YourAppeal { get; set; } = default!;
    public string? AgreeWithTheTerm { get; set; } = default!;
    public string? Password { get; set; } = default!;
    public string? Code2FA { get; set; } = default!;
    public string? LineTextStart { get; set; } = default!;
    public string? LineTextEnd { get; set; } = default!;
}