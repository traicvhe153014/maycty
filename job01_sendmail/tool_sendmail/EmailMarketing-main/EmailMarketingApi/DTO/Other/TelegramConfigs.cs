﻿namespace EmailMarketingApi.DTO.Other;

public class TelegramConfigs
{
    public string AccessToken { get; set; } = default!;

    public string ChannelId { get; set; } = default!;
}