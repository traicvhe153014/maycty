﻿using System.ComponentModel.DataAnnotations;

namespace EmailMarketingApi.DTO.Request;

public class UpdateOriginalDomainRequest
{
    public string OriginalLink { get; set; } = default!;
    [Required] public string WrapLink { get; set; } = default!;
}