﻿using System.ComponentModel.DataAnnotations;

namespace EmailMarketingApi.DTO.Request;

public class CreateWrapLinkRequest
{
    [Required]
    public Guid OriginalDomainId { get; set; } = default!;
    [Required]
    public string OriginalLink { get; set; } = default!;
    [Required]
    public int Type { get; set; } = default!;
}