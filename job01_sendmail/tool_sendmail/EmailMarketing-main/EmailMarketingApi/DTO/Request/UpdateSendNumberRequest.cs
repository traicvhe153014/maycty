﻿using System.ComponentModel.DataAnnotations;

namespace EmailMarketingApi.DTO.Request;

public class UpdateSendNumberRequest
{
    [Required]
    public string Gmail { get; set; } = default!;
    [Required]
    public int NumberIncreased { get; set; }
}