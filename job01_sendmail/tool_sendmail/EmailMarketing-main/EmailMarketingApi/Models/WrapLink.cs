﻿using System;
using System.Collections.Generic;

namespace EmailMarketingApi.Models
{
    public partial class WrapLink
    {
        public Guid Id { get; set; }
        public int SubId { get; set; }
        public string Link { get; set; } = null!;
        public string OriginalLink { get; set; } = null!;
        public Guid OriginalDomainId { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsBlock { get; set; }
        public string KeyLink { get; set; } = null!;

        public virtual OriginalDomain OriginalDomain { get; set; } = null!;
    }
}
