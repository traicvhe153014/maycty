﻿using System;
using System.Collections.Generic;

namespace EmailMarketingApi.Models
{
    public partial class GmailApi
    {
        public Guid Id { get; set; }
        public int SubId { get; set; }
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public int SendNumber { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}
