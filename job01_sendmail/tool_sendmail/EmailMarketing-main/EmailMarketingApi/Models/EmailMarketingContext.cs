﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EmailMarketingApi.Models
{
    public partial class EmailMarketingContext : DbContext
    {
        public EmailMarketingContext()
        {
        }

        public EmailMarketingContext(DbContextOptions<EmailMarketingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<GmailApi> GmailApis { get; set; } = null!;
        public virtual DbSet<OriginalDomain> OriginalDomains { get; set; } = null!;
        public virtual DbSet<WrapLink> WrapLinks { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server = 103.82.22.91,6666; database = EmailMarketing; uid=sa; pwd=0362351671@Trai;", x => x.UseNetTopologySuite());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GmailApi>(entity =>
            {
                entity.ToTable("gmail_api");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Email).HasColumnName("email");

                entity.Property(e => e.ModifiedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_at");

                entity.Property(e => e.Password).HasColumnName("password");

                entity.Property(e => e.SendNumber).HasColumnName("send_number");

                entity.Property(e => e.SubId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("sub_id");
            });

            modelBuilder.Entity<OriginalDomain>(entity =>
            {
                entity.ToTable("original_domain");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.Domain).HasColumnName("domain");

                entity.Property(e => e.IsBlock).HasColumnName("is_block");

                entity.Property(e => e.SubId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("sub_id");
            });

            modelBuilder.Entity<WrapLink>(entity =>
            {
                entity.ToTable("wrap_link");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.IsBlock).HasColumnName("is_block");

                entity.Property(e => e.KeyLink).HasColumnName("key_link");

                entity.Property(e => e.Link).HasColumnName("link");

                entity.Property(e => e.OriginalDomainId).HasColumnName("original_domain_id");

                entity.Property(e => e.OriginalLink).HasColumnName("original_link");

                entity.Property(e => e.SubId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("sub_id");

                entity.HasOne(d => d.OriginalDomain)
                    .WithMany(p => p.WrapLinks)
                    .HasForeignKey(d => d.OriginalDomainId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("wrap_link_wrap_link__fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
