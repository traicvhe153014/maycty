﻿using AutoMapper;
using EmailMarketingApi.DTO.Request;
using EmailMarketingApi.DTO.Response;
using EmailMarketingApi.Extensions;
using EmailMarketingApi.Models;
using EmailMarketingApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EmailMarketingApi.Repositories;

public class GmailApiRepository : ProjectRepository<GmailApi>, IGmailApiRepository
{
    public GmailApiRepository(EmailMarketingContext context) : base(context)
    {
    }

    public async Task<IEnumerable<GmailApi>> GetAllGmailApi()
    {
        return await _entities.OrderBy(x => x.SubId).ToListAsync();
    }

    public async Task CountSendNumber(string gmail, int numberIncreased)
    {
        var gmailApi = _entities.FirstOrDefault(x => x.Email.Equals(gmail));
        if (gmailApi != null)
        {
            gmailApi.SendNumber += numberIncreased;
        }

        await _context.SaveChangesAsync();
    }

    public async Task ResetSendNumber()
    {
        const string formatDate = "dd/MM/yyyy";
        var listGmailApi =
            _entities.Where(x => x.ModifiedAt.ToString(formatDate) != DateTime.Now.ToString(formatDate));
        foreach (var gmailApi in listGmailApi)
        {
            gmailApi.SendNumber = 0;
            gmailApi.ModifiedAt = DateTime.Now;
        }

        await _context.SaveChangesAsync();
    }

    public async Task<GmailApi> GetGmailApiByEmail(string email)
    {
        return await _entities.FirstAsync(x => x.Email == email);
    }

    public async Task<IEnumerable<OriginalDomain>> GetAllOriginalDomain()
    {
        return await _context.OriginalDomains.Where(x => x.IsBlock == false).ToListAsync();
    }

    public async Task<IEnumerable<WrapLink>> GetAllWrapLink()
    {
        return await _context.WrapLinks.Where(x => x.IsBlock == false)
            .OrderByDescending(x => x.SubId).ToListAsync();
    }

    public async Task<WrapLink?> GetWrapLink(string keyLink)
    {
        return await _context.WrapLinks.FirstOrDefaultAsync(x =>
            x.IsBlock == false && x.KeyLink.ToLower().Equals(keyLink.ToLower()));
    }

    public async Task<CreateWrapLinkResponse?> CreateWrapLink(CreateWrapLinkRequest request)
    {
        var key = General.CreateKeyLink2();
        var typeKey = "";
        switch (request.Type)
        {
            case (int)TypeWrapLink.Auto2S:
                break;
            case (int)TypeWrapLink.CaptchaFb:
                typeKey = "fbcaptcha/";
                break;
        }
        var originalDomain = await _context.OriginalDomains.FirstAsync(x => x.Id.Equals(request.OriginalDomainId));
        var wrapLink = new WrapLink()
        {
            Id = Guid.NewGuid(),
            Link = originalDomain.Domain + $"/{typeKey}" + key,
            OriginalLink = request.OriginalLink,
            OriginalDomainId = request.OriginalDomainId,
            CreatedAt = DateTime.Now,
            IsBlock = false,
            KeyLink = key
        };
        await _context.WrapLinks.AddAsync(wrapLink);
        await _context.SaveChangesAsync();

        var list =
            (await _context.WrapLinks.Where(x => x.IsBlock == false).OrderByDescending(x => x.SubId).ToListAsync())
            .Select(x =>
            {
                x.OriginalDomain = new OriginalDomain();
                return x;
            });

        return new CreateWrapLinkResponse()
        {
            WrapLinks = list,
            WrapLink = wrapLink.Link
        };
    }

    public async Task ClearWrapLink()
    {
        var listWrapLink = _context.WrapLinks.Where(x => (DateTime.Now - x.CreatedAt).Days > 3);
        foreach (var wrapLink in listWrapLink)
        {
            wrapLink.IsBlock = true;
        }

        await _context.SaveChangesAsync();
    }

    public async Task UpdateOriginalLink(UpdateOriginalDomainRequest request)
    {
        var wrapLink = await _context.WrapLinks.FirstOrDefaultAsync(x => x.Link == request.WrapLink);
        if (wrapLink != null)
        {
            wrapLink.OriginalLink = request.OriginalLink;
            await _context.SaveChangesAsync();
        }
    }
}

public enum TypeWrapLink
{
    CaptchaFb = 1,
    Auto2S = 2
}