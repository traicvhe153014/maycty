﻿using Microsoft.EntityFrameworkCore;

namespace EmailMarketingApi.Repositories.Interfaces;

public interface IProjectRepository<TEntity> where TEntity : class
{
    DbSet<TEntity> Entities { get; }
}