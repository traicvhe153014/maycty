﻿using EmailMarketingApi.DTO.Request;
using EmailMarketingApi.DTO.Response;
using EmailMarketingApi.Models;

namespace EmailMarketingApi.Repositories.Interfaces;

public interface IGmailApiRepository
{
    Task<IEnumerable<GmailApi>> GetAllGmailApi();

    Task CountSendNumber(string gmail, int numberIncreased);
    
    Task ResetSendNumber();
    
    Task<GmailApi> GetGmailApiByEmail(string email);

    Task<IEnumerable<OriginalDomain>> GetAllOriginalDomain();
    Task<IEnumerable<WrapLink>> GetAllWrapLink();

    Task<WrapLink?> GetWrapLink(string keyLink);
    
    Task<CreateWrapLinkResponse?> CreateWrapLink(CreateWrapLinkRequest request);

    Task ClearWrapLink();
    
    Task UpdateOriginalLink(UpdateOriginalDomainRequest request);
}