﻿using EmailMarketingApi.Models;
using EmailMarketingApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EmailMarketingApi.Repositories;

public class ProjectRepository<T> : IProjectRepository<T>
    where T : class
{
    protected DbSet<T> _entities;
    protected EmailMarketingContext _context { get; set; }

    public ProjectRepository(EmailMarketingContext context)
    {
        _context = context;
        _entities = _context.Set<T>();
    }

    public DbSet<T> Entities => _entities;
}