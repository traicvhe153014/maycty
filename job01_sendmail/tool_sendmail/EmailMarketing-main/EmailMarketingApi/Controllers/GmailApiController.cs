﻿using EmailMarketingApi.Controllers.Base;
using EmailMarketingApi.DTO.Request;
using EmailMarketingApi.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EmailMarketingApi.Controllers;

public class GmailApiController : ProjectController
{
    private readonly IGmailApiRepository _gmailApiRepository;

    public GmailApiController(IGmailApiRepository gmailApiRepository)
    {
        _gmailApiRepository = gmailApiRepository;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllGmailApi()
    {
        return Ok(await _gmailApiRepository.GetAllGmailApi());
    }
    
    [HttpPut]
    public async Task<IActionResult> UpdateSendNumber(UpdateSendNumberRequest request)
    {
        await _gmailApiRepository.CountSendNumber(request.Gmail, request.NumberIncreased);
        return Ok("hihi");
    }
    
    [HttpGet]
    public async Task<IActionResult> GetGmailApiByEmail(string email)
    {
        return Ok(await _gmailApiRepository.GetGmailApiByEmail(email));
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllOriginalDomain()
    {
        return Ok(await _gmailApiRepository.GetAllOriginalDomain());
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllWrapLink()
    {
        return Ok(await _gmailApiRepository.GetAllWrapLink());
    }
    
    [HttpGet("{keyLink}")]
    public async Task<IActionResult> GetWrapLink(string keyLink)
    {
        return Ok(await _gmailApiRepository.GetWrapLink(keyLink));
    }
    
    [HttpPost]
    public async Task<IActionResult> CreateWrapLink(CreateWrapLinkRequest request)
    {
        return Ok(await _gmailApiRepository.CreateWrapLink(request));
    }
    
    [HttpPut]
    public async Task<IActionResult> UpdateOriginalLink(UpdateOriginalDomainRequest request)
    {
        await _gmailApiRepository.UpdateOriginalLink(request);
        return Ok();
    }
}