﻿using System.Text;
using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;
using Newtonsoft.Json;

namespace EmailMarketing.Services;

public class TelegramApiService : ITelegramApiService
{
    private readonly HttpClient _httpClient;

    public TelegramApiService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
    
    public async Task SendMessTelegramApi(SubmitData data)
    {
        var jsonRequest = JsonConvert.SerializeObject(data);
        var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
        var response = await _httpClient.PostAsync("http://localhost:5002/api/TelegramBot/SendMessTelegram", content);
    }
}