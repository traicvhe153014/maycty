using System.Text.Json;
using EmailMarketing.Models;
using Mailjet.Client;
using Mailjet.Client.TransactionalEmails;
using Mailjet.Client.TransactionalEmails.Response;

namespace EmailMarketing.Services;

public abstract class MailjetService
{
    private static readonly string MailJetClientPath = 
        $"{Directory.GetCurrentDirectory()}/AppSettings/MailJetApi.json";
    public static async Task<List<TransactionalEmailResponse>> Send(SetupForm setup)
    {
        await using FileStream fileReadStream = new(MailJetClientPath, FileMode.Open, FileAccess.Read);
        var listMailJetApi = JsonSerializer.Deserialize<List<MailJetApi>>(fileReadStream);
        var mailJetClient = listMailJetApi!.FirstOrDefault(x => x.Email.Equals(setup.From));
        
        var responses = new List<TransactionalEmailResponse>();
        var contacts = setup.Contacts.Split("\n");
        foreach (var contact in contacts)
        {
            var client = new MailjetClient(
                mailJetClient!.ApiKey,
                mailJetClient.ApiSecret
            );
            var email = new TransactionalEmailBuilder()
                .WithFrom(new SendContact(setup.From, setup.Name))
                .WithSubject(setup.Subject)
                .WithHtmlPart(setup.Template)
                .WithTo(new SendContact(contact))
                .Build();

            var response = await client.SendTransactionalEmailAsync(email);
            responses.Add(response);
        }

        return responses;
    }
    
    public static async Task<List<TransactionalEmailResponse>> SendTest()
    {
        // await using FileStream fileReadStream = new(MailJetClientPath, FileMode.Open, FileAccess.Read);
        // var listMailJetApi = JsonSerializer.Deserialize<List<MailJetApi>>(fileReadStream);
        // var mailJetClient = listMailJetApi!.FirstOrDefault(x => x.Email.Equals(setup.From));
        
        var responses = new List<TransactionalEmailResponse>();
   
            var client = new MailjetClient(
                "15cf2e71cefe817ebd4f8d23ebf33eee",
                "ae20c6886e2028003f538f396e196fOc"
            );	//		 17028de6383f682dOc03b5681f953f6b
            // var client = new MailjetClient(
            //     "32d408e9f3d46d92f653ea22203c0ae2",
            //     "7fb3d30a4e7ee87b69a1a866a79fa163"
            // );
            var email = new TransactionalEmailBuilder()
                .WithFrom(new SendContact("f.s.fshihihjhhj@gmail.com", "0362351671Trai"))
                .WithSubject("test mail 24")
                .WithHtmlPart("hello0362351671")
                .WithTo(new SendContact("yanogo8878@gexige.com"))
                .Build();

            var response = await client.SendTransactionalEmailAsync(email);
            responses.Add(response);

        return responses;
    }
}