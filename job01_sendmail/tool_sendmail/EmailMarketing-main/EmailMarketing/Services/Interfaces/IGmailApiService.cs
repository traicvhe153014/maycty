﻿using EmailMarketing.DTO.Response;
using EmailMarketing.Models;

namespace EmailMarketing.Services.Interfaces;

public interface IGmailApiService
{
    Task<IEnumerable<GmailApi>?> GetAllGmailApi();

    Task CountSendNumber(string gmail, int numberIncreased);
    
    Task<GmailApi> GetGmailApiByEmail(string email);

    Task<IEnumerable<OriginalDomain>?> GetAllOriginalDomain();

    Task<IEnumerable<WrapLink>?> GetAllWrapLink();
    
    Task<WrapLink?> GetWrapLink(string keyLink);

    Task<CreateWrapLinkResponse?> CreateWrapLink(Guid originalDomainId, string originalLink, int type);

    Task<bool> UpdateOriginalLink(string originalLink, string wrapLink);
}