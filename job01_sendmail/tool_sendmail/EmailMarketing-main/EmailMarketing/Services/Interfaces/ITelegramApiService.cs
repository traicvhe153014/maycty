﻿using EmailMarketing.Models;

namespace EmailMarketing.Services.Interfaces;

public interface ITelegramApiService
{
    Task SendMessTelegramApi(SubmitData data);
}