﻿using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.Json;
using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;

namespace EmailMarketing.Services;

public class Gmail
{
    private static readonly string MailJetClientPath = 
        $"{Directory.GetCurrentDirectory()}/AppSettings/GmailApi.json";

    private readonly IGmailApiService _gmailApiService;

    public Gmail(IGmailApiService gmailApiService)
    {
        _gmailApiService = gmailApiService;
    }
    public static async Task<ResponseMail> SendEmailAsync(SetupForm setup, string contact, GmailApi gmailApi )
    {
        try
        {
             
            using var client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential(gmailApi.Email, gmailApi.Password);

            var message = new MailMessage
            {
                From = new MailAddress(setup.From, setup.Name),
                Subject = setup.Subject,
                BodyEncoding = Encoding.UTF8,
                SubjectEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Sender = new MailAddress(setup.From)
            };
            message.ReplyToList.Add(new MailAddress(setup.From));
            
            message.Body = setup.Template;
            message.To.Add(contact);
            await client.SendMailAsync(message);
            
            return new ResponseMail
            {
                Email = contact,
                Status = true
            };
        }
        catch (SmtpException ex)
        {
            await File.AppendAllTextAsync("Logs/log.txt",
                $"{Environment.NewLine} {DateTime.Now:dd/MM/yyyy HH:mm:ss} : {ex.Message}");
            return new ResponseMail
            {
                Email = contact,
                Status = false
            };
        }
    }
}