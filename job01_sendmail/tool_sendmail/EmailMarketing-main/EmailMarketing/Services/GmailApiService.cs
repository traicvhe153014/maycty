﻿using System.Text;
using EmailMarketing.Constants;
using EmailMarketing.DTO.Response;
using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;
using Newtonsoft.Json;

namespace EmailMarketing.Services;

public class GmailApiService : IGmailApiService
{
    private readonly HttpClient _httpClient;

    public GmailApiService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
    
    public async Task<IEnumerable<GmailApi>?> GetAllGmailApi()
    {
        var result = await _httpClient.GetFromJsonAsync<IEnumerable<GmailApi>>("/api/GmailApi/GetAllGmailApi");
        return result;
    }

    public async Task CountSendNumber(string gmail, int numberIncreased)
    {
        var requestBody = new { gmail, numberIncreased };
        var jsonRequest = JsonConvert.SerializeObject(requestBody);
        var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
        var response = await _httpClient.PutAsync("/api/GmailApi/UpdateSendNumber", content);
        if (response.IsSuccessStatusCode)
        {
            Console.WriteLine("Request PUT thành công!");
        }
        else
        {
            Console.WriteLine("Đã xảy ra lỗi: " + response.StatusCode );
        }
    }
    
    public async Task<GmailApi> GetGmailApiByEmail(string email)
    {
        var result = await _httpClient.GetFromJsonAsync<GmailApi>($"/api/GmailApi/GetGmailApiByEmail?email={email}");
        return result!;
    }

    public async Task<IEnumerable<OriginalDomain>?> GetAllOriginalDomain()
    {
        var result = await _httpClient.GetFromJsonAsync<IEnumerable<OriginalDomain>?>("/api/GmailApi/GetAllOriginalDomain");
        return result;
    }
    
    public async Task<IEnumerable<WrapLink>?> GetAllWrapLink()
    {
        var result = await _httpClient.GetFromJsonAsync<IEnumerable<WrapLink>?>("/api/GmailApi/GetAllWrapLink");
        return result;
    }

    public async Task<WrapLink?> GetWrapLink(string keyLink)
    {
        var result = await _httpClient.GetFromJsonAsync<WrapLink?>($"/api/GmailApi/GetWrapLink/{keyLink}");
        return result;
    }
    
    public async Task<CreateWrapLinkResponse?> CreateWrapLink(Guid originalDomainId, string originalLink, int type)
    {
        var requestBody = new { originalDomainId, originalLink, type };
        var jsonRequest = JsonConvert.SerializeObject(requestBody);
        var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
        var response = await _httpClient.PostAsync("/api/GmailApi/CreateWrapLink", content);
        if (response.IsSuccessStatusCode)
        {
            var dataJsonString = await response.Content.ReadAsStringAsync();
            var dataList = JsonConvert.DeserializeObject<CreateWrapLinkResponse>(dataJsonString);
            return dataList!;
        }
        else
        {
            return null;
        }
    }

    public async Task<bool> UpdateOriginalLink(string originalLink, string wrapLink)
    {
        var requestBody = new { originalLink, wrapLink };
        var jsonRequest = JsonConvert.SerializeObject(requestBody);
        var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
        var response = await _httpClient.PutAsync("/api/GmailApi/UpdateOriginalLink", content);
        return response.IsSuccessStatusCode;
    }
}