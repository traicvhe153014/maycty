﻿using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;
using Microsoft.AspNetCore.Components;
using AntDesign.TableModels;
using System.ComponentModel;
using EmailMarketing.Constants;
using EmailMarketing.DTO.Tables;
using EmailMarketing.Extensions;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;

namespace EmailMarketing.Pages;

public partial class CreateWrapLink
{
    [Inject] private IGmailApiService _gmailApiService { set; get; }
    [Inject] IJSRuntime _jsRuntime { set; get; }

    private IEnumerable<OptionModel>? ListOriginalDomain { get; set; }
    private WrapLinkTable[] DataWrapLink { get; set; } = default!;
    private WrapLinkForm Model { get; set; } = new();

    protected override async Task<Task> OnInitializedAsync()
    {
        await LoadData();
        return Task.CompletedTask;
    }

    private async Task LoadData()
    {
        ListOriginalDomain = (await _gmailApiService.GetAllOriginalDomain() ?? Array.Empty<OriginalDomain>())
            .Select(x => new OptionModel()
            {
                Value = x.Domain,
                Key3 = x.Id
            });
        DataWrapLink = (await _gmailApiService.GetAllWrapLink() ?? Array.Empty<Models.WrapLink>())
            .Select(x => new WrapLinkTable()
            {
                Link = x.Link,
                OriginalLink = x.OriginalLink,
                CreatedAt = ExtensionMail.DisplayTime(x.CreatedAt)
            }).ToArray();
    }
    
    private async Task CopyToClipboard(string link)
    {
        await _jsRuntime.InvokeVoidAsync("navigator.clipboard.writeText", link);
    }

    private async Task OnFinish(EditContext editContext)
    {
        var result = await _gmailApiService.CreateWrapLink(Model.OriginalDomainId, Model.OriginalLink, Model.TypeWrapLink);
        if (result != null)
        {
            DataWrapLink = result.WrapLinks!.Select(x => new WrapLinkTable()
                {
                    Link = x.Link,
                    OriginalLink = x.OriginalLink,
                    CreatedAt = ExtensionMail.DisplayTime(x.CreatedAt)
                }).ToArray();
            //await CopyToClipboard(result.WrapLink);
        }
    }

    private void OriginalLinkChange(Tuple<string,string> tuple)
    {
        DataWrapLink = DataWrapLink.Select(x =>
        {
            if (tuple.Item2 == x.Link)
            {
                x.OriginalLink = tuple.Item1;
            }
            return x;
        }).ToArray();
        StateHasChanged();
    }
    
    private readonly List<OptionModel> _listOptionSend = new List<OptionModel>()
    {
        new OptionModel
        {
            Key = (int)TypeWrapLink.CaptchaFb,
            Value = TypeWrapLink.CaptchaFb.ToString()
        },
        new OptionModel
        {
            Key = (int)TypeWrapLink.Auto2S,
            Value = TypeWrapLink.Auto2S.ToString()
        }
    };
}