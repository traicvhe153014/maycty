﻿using System.Text.RegularExpressions;
using AntDesign;
using EmailMarketing.Constants;
using EmailMarketing.Extensions;
using EmailMarketing.Models;
using EmailMarketing.Services;
using EmailMarketing.Services.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace EmailMarketing.Pages;

public partial class Home
{
    [Inject] private IGmailApiService _gmailApiService { set; get; }
    private SetupForm Model { get; set; } = new();
    private string TemplateString { get; set; } = default!;
    private List<OptionModel>? ListEmail { get; set; }
    private bool Loading { get; set; }
    private bool LoadingSelect { get; set; }
    private int CountError { get; set; }
    private int CountSuccess { get; set; }
    private List<UploadFileItem> FileList { get; set; } = new();

    protected override async Task<Task> OnInitializedAsync()
    {
        await LoadData();
        return Task.CompletedTask;
    }

    private async Task OnFinish(EditContext editContext)
    {
        try
        {
            Loading = true;
            var logs = new string[] { };

            switch (Model.TypeEmail)
            {
                case (int)TypeSendMail.ByGmail:
                    logs = (await SendGmail()).Select(x => $"{x.Email} : {(x.Status ? "success" : "error")}").ToArray();
                    break;
                case (int)TypeSendMail.ByMailJet:
                    Model.Template = TemplateString;
                    var responses = await MailjetService.Send(Model);
                    logs = responses.Select(s =>
                    {
                        var message = s.Messages.FirstOrDefault();
                        var template = $"{message?.To?.FirstOrDefault()?.Email}: {message?.Status}";
                        return template;
                    }).ToArray();
                    break;
            }

            var status = string.Join('\n', logs);
            CountSuccess = CountStatus("success", status);
            CountError = CountStatus("error", status);
            // await GmailApiRepository.CountSendNumber(Model.From, CountSuccess);
            await UpdateCountSendGmail(Model.From, CountSuccess);
            Model.Status = status;
            Loading = false;
            StateHasChanged();
        }
        catch (Exception e)
        {
            await NoticeWithIcon(NotificationType.Error, e.Message);
            await File.AppendAllTextAsync("Logs/log.txt",
                $"{Environment.NewLine} {DateTime.Now:dd/MM/yyyy HH:mm:ss} : {e.Message}");
            Loading = false;
        }
        finally
        {
            await LoadData();
        }
    }

    private async Task<List<ResponseMail>> SendGmail()
    {
        var responseMails = new List<ResponseMail>();
        var contacts = Model.Contacts.Split("\n");
        var pagesName = Model.PagesName.Split("\n");
        TemplateString = ExtensionMail.ChangeHrefInHtmlString(TemplateString, Model.WrapLink);
        var gmailApi = await _gmailApiService.GetGmailApiByEmail(Model.From);
        if (pagesName.Length != contacts.Length)
        {
            throw new Exception("PageName and contacts dont match");
        }
        for (var i = 0; i < contacts.Length; i++)
        {
            Model.Template = GetReplaceInfoInTemplate(pagesName[i], Model.KeyPageName);
            var responseMail = await Gmail.SendEmailAsync(Model, contacts[i], gmailApi);
            responseMails.Add(responseMail);
        }

        return responseMails;
    }

    private string GetReplaceInfoInTemplate(string value, string key)
    {
        var template = TemplateString.Replace(key, value);
        return template;
    }

    private void OnChangeTypeEmail(int type)
    {
        switch (type)
        {
            case (int)TypeSendMail.ByGmail:
                var listGmail = ExtensionMail.GetAllGmailApi();
                ListEmail = listGmail.Select(x => new OptionModel()
                {
                    Key = 0,
                    Value = x.Email
                }).ToList();
                break;
            case (int)TypeSendMail.ByMailJet:
                var listMailJet = ExtensionMail.GetAllMailJetApi();
                ListEmail = listMailJet.Select(x => new OptionModel()
                {
                    Key = 0,
                    Value = x.Email
                }).ToList();
                break;
        }
    }

    private async Task LoadData()
    {
        try
        {
            LoadingSelect = true;
            var listGmail = await _gmailApiService.GetAllGmailApi();
            if (listGmail != null)
            {
                ListEmail = listGmail.Select(x => new OptionModel()
                {
                    Key2 = x.Email,
                    Value = $"{x.Email} - ({x.SendNumber})"
                }).ToList();
            }
        }
        catch
        {
            LoadingSelect = false;
        }
        finally
        {
            LoadingSelect = false;
        }
    }

    private async Task UpdateCountSendGmail(string gmail, int count)
    {
        await _gmailApiService.CountSendNumber(gmail, count);
    }
    
    private static int CountStatus(string status, string input)
    {
        var pattern = $@"\b({status})\b";
        return Regex.Matches(input, pattern, RegexOptions.IgnoreCase).Count;
    }

    private async Task UploadFiles(InputFileChangeEventArgs e)
    {
        var files = e.GetMultipleFiles().ToList();
        FileList = files.Select(file => new UploadFileItem { FileName = file.Name, Size = file.Size }).ToList();
        TemplateString = await ExtensionMail.GetContentFile(files[0]);
    }

    private async Task NoticeWithIcon(NotificationType type, string description)
    {
        await Notice.Error(new NotificationConfig()
        {
            Message = "Error",
            Description = description,
            Duration = 0,
        });
    }
    
    private readonly List<OptionModel> _listOptionSend = new List<OptionModel>()
    {
        new OptionModel
        {
            Key = (int)TypeSendMail.ByGmail,
            Value = TypeSendMail.ByGmail.ToString()
        },
        new OptionModel
        {
            Key = (int)TypeSendMail.ByMailJet,
            Value = TypeSendMail.ByMailJet.ToString()
        }
    };

    private async Task SendMailJet()
    {
        var responses = await MailjetService.SendTest();
        var logs = responses.Select(s =>
        {
            var message = s.Messages.FirstOrDefault();
            var template = $"{message?.To?.FirstOrDefault()?.Email}: {message?.Status}";
            return template;
        }).ToArray();
        Console.WriteLine(string.Join('\n', logs));
    }
}