﻿using Microsoft.AspNetCore.Components;

namespace EmailMarketing.Pages.OriginalLinks;

public partial class OriginalLink2
{
    [Parameter] public string KeyLink { get; set; } = default!;
    
    [Inject] private NavigationManager _avigationManager { set; get; }
    
    protected override async Task<Task> OnInitializedAsync()
    {
        if (Guid.TryParse(KeyLink, out var guidResult))
        {
            
        }
        else
        {
            _avigationManager.NavigateTo("/metasuport/error/errorpgae");
        }
        return Task.CompletedTask;
    }
}