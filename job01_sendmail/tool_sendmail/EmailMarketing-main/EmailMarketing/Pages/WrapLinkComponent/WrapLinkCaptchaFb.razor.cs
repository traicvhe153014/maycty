﻿using EmailMarketing.Constants;
using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;
using Microsoft.AspNetCore.Components;

namespace EmailMarketing.Pages.WrapLinkComponent;

public partial class WrapLinkCaptchaFb
{
    [Parameter] public string KeyLink { get; set; } = default!;
    [Inject] private IGmailApiService _gmailApiService { set; get; }
    [Inject] private NavigationManager _avigationManager { set; get; }
    public string CaptchaValue { get; set; } = default!;
    

    private Captcha? CaptchaUser { get; set; }
    
    protected override Task<Task> OnInitializedAsync()
    {
        RandomCaptcha();
        return Task.FromResult(Task.CompletedTask);
    }

    private async void CheckCaptcha()
    {
        if (CaptchaUser != null && string.Equals(CaptchaUser.Code, CaptchaValue, StringComparison.CurrentCultureIgnoreCase))
        {
            Console.WriteLine("dung");
            await RedirectToLink();
        }
        else
        {
            Console.WriteLine("sai");
            RandomCaptcha();
        }
    }
    
    private void RandomCaptcha()
    {
        var list = GetCaptchaImageList();
        if (CaptchaUser != null)
        {
            list = list.Where(x => x.Code != CaptchaUser.Code).ToList();
        }
        
        var random = new Random();
        CaptchaUser = list[random.Next(list.Count)];
    }
    
    private static List<Captcha> GetCaptchaImageList()
    {
        var images = new List<Captcha>();

        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/captchaImg");

        if (!Directory.Exists(path)) return images;
        var files = Directory.GetFiles(path, "*.png");

        foreach (var file in files)
        {
            var fileName = Path.GetFileName(file);
            var captcha = new Captcha()
            {
                Href = $"/captchaImg/{fileName}",
                Code = fileName.Remove(fileName.LastIndexOf(".png", StringComparison.Ordinal))
            };
            images.Add(captcha);
        }

        return images;
    }
    
    private async Task RedirectToLink()
    {
        var wrapLink = await _gmailApiService.GetWrapLink(KeyLink);
        if (wrapLink != null)
        {
            _avigationManager.NavigateTo(wrapLink.OriginalLink);
        }
    }
}