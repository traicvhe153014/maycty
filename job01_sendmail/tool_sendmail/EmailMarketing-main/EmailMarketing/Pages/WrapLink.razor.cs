﻿using EmailMarketing.Models;
using EmailMarketing.Services.Interfaces;
using Microsoft.AspNetCore.Components;
using AntDesign;

namespace EmailMarketing.Pages;

public partial class WrapLink
{
    [Inject] private IGmailApiService _gmailApiService { set; get; }
    [Inject] private NavigationManager _avigationManager { set; get; }
    [Parameter] public string KeyLink { get; set; } = default!;

    protected override async Task<Task> OnInitializedAsync()
    {
        return Task.FromResult(Task.CompletedTask);
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            await RedirectAfterDelay();
        }
    }

    private async Task RedirectAfterDelay()
    {
        await Task.Delay(2000); // Đợi 2 giây
        var wrapLink = await _gmailApiService.GetWrapLink(KeyLink);
        if (wrapLink != null)
        {
            _avigationManager.NavigateTo(wrapLink.OriginalLink);
        }
    }
}