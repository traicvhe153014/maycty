﻿using AntDesign;
using EmailMarketing.Services.Interfaces;
using Microsoft.AspNetCore.Components;

namespace EmailMarketing.Pages.component;

public partial class InputEditTable
{
    [Parameter] public string TxtValue { get; set; } = default!;
    [Parameter] public string WrapLink { get; set; } = default!;
    [Parameter] public EventCallback<Tuple<string,string>> OriginalLinkChange { get; set; }

    [Inject] private IGmailApiService _gmailApiService { set; get; }
    private bool IsEdit { get; set; } = false;
    private string _size = AntSizeLDSType.Small;
    private string ValueOriginal { get; set; } = default!;

    protected override Task<Task> OnInitializedAsync()
    {
        ValueOriginal = TxtValue;
        return Task.FromResult(Task.CompletedTask);
    }

    private void OnClickCancel()
    {
        IsEdit = false;
        // TxtValue = ValueOriginal;
    }

    private async Task OnClickSave()
    {
        IsEdit = false;
        var result = await _gmailApiService.UpdateOriginalLink(TxtValue, WrapLink);
        if (result)
        {
            await OriginalLinkChange.InvokeAsync(new Tuple<string, string>(TxtValue,WrapLink));
        }
    }

    private void OnClickEdit()
    {
        IsEdit = true;
    }
}