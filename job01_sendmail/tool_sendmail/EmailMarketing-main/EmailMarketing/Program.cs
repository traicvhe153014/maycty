using EmailMarketing.Services;
using EmailMarketing.Services.Interfaces;

var appSettingsJson = "appsettings.json";

var builder = WebApplication.CreateBuilder(args);
builder.WebHost.ConfigureKestrel(serverOptions => { serverOptions.ListenLocalhost(5001); });

// config appSettings
builder.WebHost.ConfigureAppConfiguration((hostingContext, config) =>
{
    var env = hostingContext.HostingEnvironment;
    appSettingsJson = $"appsettings.{env.EnvironmentName}.json";
    
    config.AddJsonFile("appsettings.json", optional: false)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
});

// Add Repositories
builder.Services.AddScoped<IGmailApiService, GmailApiService>();
builder.Services.AddScoped<ITelegramApiService, TelegramApiService>();

var services = builder.Services;
services.AddRazorPages();
services.AddServerSideBlazor();
services.AddAntDesign();

// httpclient
var config = new ConfigurationBuilder().AddJsonFile(appSettingsJson).Build();
services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(config["ConfigUriApi:GmailApi"]!) });

var app = builder.Build();
app.UseStaticFiles();
app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
app.Run();