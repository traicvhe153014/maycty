﻿using EmailMarketing.Models;

namespace EmailMarketing.DTO.Response;

public class CreateWrapLinkResponse
{
    public IEnumerable<WrapLink>? WrapLinks { get; set; }
    public string WrapLink { get; set; } = default!;
}