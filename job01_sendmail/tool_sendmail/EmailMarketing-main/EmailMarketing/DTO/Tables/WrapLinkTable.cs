﻿using System.ComponentModel;

namespace EmailMarketing.DTO.Tables;

public class WrapLinkTable
{
    [DisplayName("WrapLink")]
    public string Link { get; set; }

    [DisplayName("Original Link")]
    public string OriginalLink { get; set; }

    [DisplayName("Created At")]
    public string CreatedAt { get; set; }
}