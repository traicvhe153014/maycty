﻿using log4net;
using OneOf.Types;

namespace EmailMarketing.Extensions;

public static class LogEmailMarketing
{
      public static void Error(Exception ex)
      {
            var log = LogManager.GetLogger(typeof(Program));
            log.Error("Error occurred", ex);
      }
}