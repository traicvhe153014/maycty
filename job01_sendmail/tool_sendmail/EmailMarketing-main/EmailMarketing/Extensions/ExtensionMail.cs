﻿using System.Text.Json;
using System.Text.RegularExpressions;
using EmailMarketing.Models;
using Microsoft.AspNetCore.Components.Forms;

namespace EmailMarketing.Extensions;

public static class ExtensionMail
{
    public static string GetNameEmail(string mail)
    {
        var atIndex = mail.IndexOf('@');
        var name = mail[..atIndex];
        return name;
    }
    
    public static IEnumerable<GmailApi> GetAllGmailApi()
    {
        var gmailJsonPath = 
            $"{Directory.GetCurrentDirectory()}/AppSettings/GmailApi.json";
        using FileStream fileReadStream = new(gmailJsonPath, FileMode.Open, FileAccess.Read);
        var listMail = JsonSerializer.Deserialize<List<GmailApi>>(fileReadStream);
        return listMail!;
    }
    
    public static IEnumerable<MailJetApi> GetAllMailJetApi()
    {
        var mailJetClientPath =
            $"{Directory.GetCurrentDirectory()}/AppSettings/MailJetApi.json";
        using FileStream fileReadStream = new(mailJetClientPath, FileMode.Open, FileAccess.Read);
        var listMail = JsonSerializer.Deserialize<List<MailJetApi>>(fileReadStream);
        return listMail!;
    }
    
    public static async Task<string> GetContentFile(IBrowserFile file)
    {
        await using var stream = file.OpenReadStream();
        using var reader = new StreamReader(stream);
        return await reader.ReadToEndAsync();
    }
    
    public static string ChangeHrefInHtmlString(string stringHtml, string wrapLink)
    {
        const string pattern = @"href=['""]([^'"">]*)['""]";
        var regex = new Regex(pattern);
        foreach(Match match in regex.Matches(stringHtml))
        {
            // code replace
            var href = match.Groups[1].Value;
            var newHref = wrapLink;
            if (!string.IsNullOrEmpty(href))
            {
                newHref = href.Replace(href, wrapLink);
            }
            stringHtml = stringHtml.Replace(match.Value,
                $"href=\"{newHref}\"");
        }
        return stringHtml;
    }

    public static string DisplayTime(DateTime date)
    {
        var timeDifference = DateTime.Now - date;
        
        return timeDifference.TotalHours switch
        {
            > 24 => date.ToString("dd/MM/yyyy"),
            >= 1 => timeDifference.Hours + " giờ trước",
            < 0.02 => "Vừa xong",
            _ => Math.Ceiling(timeDifference.TotalMinutes) + " phút trước"
        };
    }
}