﻿namespace EmailMarketing.Constants;

public enum TypeSendMail
{
    ByGmail = 1,
    ByMailJet = 2
}