﻿namespace EmailMarketing.Models;

public class ResponseFileModel
{
    public string Name { get; set; } = default!;

    public string Status { get; set; } = default!;

    public string Url { get; set; } = default!;

    public string ThumbUrl { get; set; } = default!;
}