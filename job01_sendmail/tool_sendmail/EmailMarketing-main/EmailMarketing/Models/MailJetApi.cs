﻿using System.Text.Json.Serialization;

namespace EmailMarketing.Models;

public class MailJetApi
{
    [JsonPropertyName("apiKey")] 
    public string ApiKey { get; set; } = default!;
    [JsonPropertyName("apiSecret")]
    public string ApiSecret { get; set; } = default!;
    [JsonPropertyName("email")]
    public string Email { get; set; } = default!;
}