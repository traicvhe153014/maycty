using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using EmailMarketing.Constants;

namespace EmailMarketing.Models;

public class SetupForm
{
    [Required] public string Name { get; set; } = default!;

    [Required] public string Subject { get; set; } = default!;

    [Required] public string From { get; set; } = default!;

    [Required] public string Contacts { get; set; } = default!;
    
    [Required] public string PagesName { get; set; } = default!;
    
    [Required] public string WrapLink { get; set; } = string.Empty;

    public string Template { get; set; } = default!;

    public string Status { get; set; } = default!;
    
    public int TypeEmail { get; set; } = (int)TypeSendMail.ByGmail;
    
    public string KeyPageName { get; set; } = string.Empty;
}