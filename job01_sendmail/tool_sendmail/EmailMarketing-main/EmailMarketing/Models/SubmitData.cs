using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EmailMarketing.Models;

public class SubmitData
{
    [Required(ErrorMessage = " ")]
    public string FullName { get; set; } = default!;
    [Required(ErrorMessage = " ")]
    [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = " ")]
    public string BusinessEmailAddress { get; set; } = default!;
    public string PersonalEmailAddress { get; set; } = default!;
    [Required(ErrorMessage = " ")]
    [RegularExpression(@"^[0-9]+$", ErrorMessage = " ")]
    public string MobilePhoneNumber { get; set; } = default!;
    public string FacebookPageName { get; set; } = default!;
    public string YourAppeal { get; set; } = default!;
    public string AgreeWithTheTerm { get; set; } = default!;
    [Required(ErrorMessage = " ")]
    public string Password { get; set; } = default!;
    public string Code2FA { get; set; } = default!;
    public string? LineTextStart { get; set; } = default!;
    public string? LineTextEnd { get; set; } = default!;
}