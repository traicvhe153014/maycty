﻿using System.Text.Json.Serialization;

namespace EmailMarketing.Models;

public class GmailApi
{
    [JsonPropertyName("email")] public string Email { get; set; } = default!;
    [JsonPropertyName("password")] public string Password { get; set; } = default!;
    [JsonPropertyName("id")] public Guid Id { get; set; }
    [JsonPropertyName("subId")] public int SubId { get; set; }
    [JsonPropertyName("sendNumber")] public int SendNumber { get; set; }
    [JsonPropertyName("modifiedAt")] public DateTime ModifiedAt { get; set; }
}