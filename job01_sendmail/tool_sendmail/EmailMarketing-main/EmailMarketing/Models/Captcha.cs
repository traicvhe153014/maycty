﻿namespace EmailMarketing.Models;

public class Captcha
{
    public string Href { get; set; } = default!;
    public string Code { get; set; } = default!;
}