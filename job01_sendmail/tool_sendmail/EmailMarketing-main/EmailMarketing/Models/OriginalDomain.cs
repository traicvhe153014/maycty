﻿using System;
using System.Collections.Generic;

namespace EmailMarketing.Models
{
    public partial class OriginalDomain
    {
        public OriginalDomain()
        {
            WrapLinks = new HashSet<WrapLink>();
        }

        public Guid Id { get; set; }
        public int SubId { get; set; }
        public string Domain { get; set; } = null!;
        public DateTime CreatedAt { get; set; }
        public bool IsBlock { get; set; }

        public virtual ICollection<WrapLink> WrapLinks { get; set; }
    }
}
