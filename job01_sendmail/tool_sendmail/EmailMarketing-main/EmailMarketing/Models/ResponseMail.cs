﻿namespace EmailMarketing.Models;

public class ResponseMail
{
    public string Email { get; set; } = default!;
    public bool Status { get; set; } = default!;
}