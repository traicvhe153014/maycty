﻿namespace EmailMarketing.Models;

public class OptionModel
{
    public int Key { get; set; }
    public string Value { get; set; } = default!;

    public string? Key2 { get; set; } = string.Empty;
    
    public Guid? Key3 { get; set; }
}