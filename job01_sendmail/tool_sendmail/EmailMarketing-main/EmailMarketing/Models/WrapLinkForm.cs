﻿using System.ComponentModel.DataAnnotations;

namespace EmailMarketing.Models;

public class WrapLinkForm
{
    [Required] public Guid OriginalDomainId { get; set; }
    [Required] public string OriginalLink { get; set; } = default!;
    
    [Required] public int TypeWrapLink { get; set; } = (int)Constants.TypeWrapLink.CaptchaFb;
}