﻿#!/bin/sh

dotnet restore

cd "src/app-clients/Client.Manager/ClientApp" && npm run build:dev && rm -f package-lock.json
sudo rm -rf /var/www/devv6.novanet.vn/*

cp -a "dist/novanet-manager-dev/." "/var/www/devv6.novanet.vn"
cd ../../../.. || exit

dotnet build "src/services/Service.Display/Service.Display.csproj" -c Release
dotnet build "src/services/Service.Identity/Service.Identity.csproj" -c Release
dotnet build "src/services/Service.Notification/Service.Notification.csproj" -c Release
dotnet build "src/services/Service.Report/Service.Report.csproj" -c Release
dotnet build "src/services/Service.Schedule/Service.Schedule.csproj" -c Release
dotnet build "src/services/Service.Settings/Service.Settings.csproj" -c Release
dotnet build "src/services/Service.Sharing/Service.Sharing.csproj" -c Release
dotnet build "src/services/Service.Storage/Service.Storage.csproj" -c Release

dotnet publish "src/services/Service.Display/Service.Display.csproj" -c Release
dotnet publish "src/services/Service.Identity/Service.Identity.csproj" -c Release
dotnet publish "src/services/Service.Notification/Service.Notification.csproj" -c Release
dotnet publish "src/services/Service.Report/Service.Report.csproj" -c Release
dotnet publish "src/services/Service.Schedule/Service.Schedule.csproj" -c Release
dotnet publish "src/services/Service.Settings/Service.Settings.csproj" -c Release
dotnet publish "src/services/Service.Sharing/Service.Sharing.csproj" -c Release
dotnet publish "src/services/Service.Storage/Service.Storage.csproj" -c Release

sudo lsof -iTCP -sTCP:LISTEN -P | grep :5011
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5012
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5013
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5014
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5015
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5016
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5017
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5018

pm2 kill
pm2 start -n Service.Display "dotnet src/services/Service.Display/bin/Release/net6.0/publish/Service.Display.dll --environment=dev --urls=http://localhost:5011/"
pm2 start -n Service.Identity "dotnet src/services/Service.Identity/bin/Release/net6.0/publish/Service.Identity.dll --environment=dev --urls=http://localhost:5012/"
pm2 start -n Service.Notification "dotnet src/services/Service.Notification/bin/Release/net6.0/publish/Service.Notification.dll --environment=dev --urls=http://localhost:5013/"
pm2 start -n Service.Report "dotnet src/services/Service.Report/bin/Release/net6.0/publish/Service.Report.dll --environment=dev --urls=http://localhost:5014/"
pm2 start -n Service.Schedule "dotnet src/services/Service.Schedule/bin/Release/net6.0/publish/Service.Schedule.dll --environment=dev --urls=http://localhost:5015/"
pm2 start -n Service.Settings "dotnet src/services/Service.Settings/bin/Release/net6.0/publish/Service.Settings.dll --environment=dev --urls=http://localhost:5016/"
pm2 start -n Service.Sharing "dotnet src/services/Service.Sharing/bin/Release/net6.0/publish/Service.Sharing.dll --environment=dev --urls=http://localhost:5017/"
pm2 start -n Service.Storage "dotnet src/services/Service.Storage/bin/Release/net6.0/publish/Service.Storage.dll --environment=dev --urls=http://localhost:5018/"
