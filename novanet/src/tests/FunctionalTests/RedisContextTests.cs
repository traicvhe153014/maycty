﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Redis.OM;
using Redis.OM.Modeling;
using Xunit;
using Xunit.Abstractions;

namespace FunctionalTests;

public class RedisContextTests
{
    private readonly ITestOutputHelper _testOutputHelper;
    private readonly RedisConnectionProvider _provider = new("redis://localhost:6379");

    public RedisContextTests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }

    [Fact]
    public async Task GenerateTestData()
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        await _provider.Connection.CreateIndexAsync(typeof(Campaign));
        var collection = _provider.RedisCollection<Campaign>();

        var tasks = new List<Task>();
        for (var i = 0; i < 1000000; i++)
        {
            var campaign = new Campaign
            {
                CampaignId = Guid.NewGuid().ToString(),
                CampaignName = Faker.Name.FullName(),
                Clicks = Faker.RandomNumber.Next(),
                Description = Faker.Lorem.Sentence(),
                Impressions = Faker.RandomNumber.Next(),
                Reach = Faker.RandomNumber.Next(),
                StartDate = Faker.Identification.DateOfBirth()
            };
            tasks.Add(Task.Run(async () => await collection.InsertAsync(campaign)));
        }

        await Task.WhenAll(tasks);
        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
    }

    [Fact]
    public void FilteringFullTextContains()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.CampaignName != null && x.CampaignName.Contains("Mrs"))
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }

    [Fact]
    public void FilteringFullTextContainsWithPagination()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.CampaignName == "Reid")
            .Skip(100)
            .Take(100)
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }

    [Fact]
    public void FilteringNumberGreaterThan()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.Reach < 10000)
            .Take(10)
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }

    [Fact]
    public void FilteringFullTextWildcard()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.CampaignName == "Miss")
            .Take(100)
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }

    [Fact]
    public void FilteringMultipleCondition()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.CampaignName != null && x.Reach < 200000 && x.CampaignName.Contains("Smith"))
            .Take(100)
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }

    [Fact]
    public void SelectMultipleCondition()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.CampaignName != null && (x.Reach < 200000 || x.CampaignName.Contains("Smith")))
            .Take(100)
            .Select(x => new {x.CampaignName, x.Reach})
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }

    [Fact]
    public void OrderMultipleCondition()
    {
        var campaigns = _provider.RedisCollection<Campaign>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var filteredCampaigns = campaigns
            .Where(x => x.CampaignName != null && (x.Reach < 200000 || x.CampaignName.Contains("Smith")))
            .OrderBy(x => x.Reach)
            .Skip(100)
            .Take(100)
            .ToList();

        stopwatch.Stop();

        _testOutputHelper.WriteLine("Duration: " + stopwatch.ElapsedMilliseconds + "ms");
        _testOutputHelper.WriteLine("Count: " + filteredCampaigns.Count);
        if (filteredCampaigns.Count > 0)
        {
            _testOutputHelper.WriteLine("First element: " + filteredCampaigns[0].CampaignName);
        }
    }
}

[Document]
public class Campaign
{
    [RedisIdField] public string? Id { get; set; }

    [Indexed(Aggregatable = true, Sortable = true)]
    public string? CampaignId { get; set; }

    [Searchable(Aggregatable = true, Sortable = true)]
    public string? CampaignName { get; set; }

    [Indexed(Aggregatable = true, Sortable = true)]
    public string? Description { get; set; }

    [Indexed(Aggregatable = true, Sortable = true)]
    public DateTime StartDate { get; set; }

    [Indexed(Aggregatable = true, Sortable = true)]
    public long Reach { get; set; }

    [Indexed(Aggregatable = true, Sortable = true)]
    public long Impressions { get; set; }

    [Indexed(Aggregatable = true, Sortable = true)]
    public float Clicks { get; set; }
}