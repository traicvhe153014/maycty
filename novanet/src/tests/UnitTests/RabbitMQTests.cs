﻿using System.Threading.Tasks;
using Novanet.EventBus;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests;

public class RabbitMQTests
{
    private readonly ITestOutputHelper _testOutputHelper;
    private readonly IMessageBusClient _messageBusClient;

    public RabbitMQTests(ITestOutputHelper testOutputHelper, IMessageBusClient messageBusClient)
    {
        _testOutputHelper = testOutputHelper;
        _messageBusClient = messageBusClient;
    }

    [Fact]
    public void SendMessage()
    {
        const string message = "Tết đã hết nhưng không khí Tết cùng sức sống ngày Xuân vẫn rạo rực, xôn xao";
        _messageBusClient.Publish(nameof(RabbitMQTests), message);
    }

    [Fact]
    public void ReceiveMessage()
    {
        _messageBusClient.Subscribe<object>(nameof(RabbitMQTests), (action) => Task.CompletedTask);
    }
}