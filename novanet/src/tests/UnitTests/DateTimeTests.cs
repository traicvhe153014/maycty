using System;
using System.Globalization;
using Novanet.Core.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests;

public class DateTimeTests
{
    private readonly ITestOutputHelper _testOutputHelper;

    public DateTimeTests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }

    [Fact]
    public void ConvertHexCodeToDateTime()
    {
        var result = "62148079".ToDateTime();
        _testOutputHelper.WriteLine(result.ToString());
    }
    
    [Fact]
    public void ConvertDateTimeToHexCode()
    {
        var result = DateTimeOffset.Now.ToHexCode();
        _testOutputHelper.WriteLine(result);
    }

    [Fact]
    public void ConvertUnixTimeSecondToDateTime()
    {
        var dateTime = 1645511253.ToString("X").ToDateTime();
        var output = dateTime.ToLocalTime().ToString(CultureInfo.InvariantCulture);
        _testOutputHelper.WriteLine(output);
    }
}