﻿using System.Threading.Tasks;
using Novanet.Core.RedisCache;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests;

public class RedisTests
{
    private readonly ITestOutputHelper _testOutputHelper;
    private readonly IRedisCacheService _redisCacheService;

    public RedisTests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
        _redisCacheService = new RedisCacheService("localhost:6379");
    }
    
    [Fact]
    public async Task Get()
    {
        var data = await _redisCacheService.GetAsync(RedisKeys.KeySettings<object>("hihi"));
        _testOutputHelper.WriteLine(data);
    }
    
    [Fact]
    public async Task Set()
    {
        await _redisCacheService.SetAsync(RedisKeys.KeySettings<object>("hihi"), "huhu");
    }
}