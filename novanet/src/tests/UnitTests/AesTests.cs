﻿using Novanet.Core.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests;

public class AesTests
{
    private readonly ITestOutputHelper _testOutputHelper;

    public AesTests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }

    [Fact]
    public void AesEncryptString()
    {
        var result = "Tết đã hết nhưng không khí Tết cùng sức sống ngày Xuân vẫn rạo rực, xôn xao".AesEncryptString();
        _testOutputHelper.WriteLine(result);
    }
    
    [Fact]
    public void AesDecryptString()
    {
        var result = "KTTAmuzwx2llq0EqAaYgPf936wodqncpnKUcmeAM15iS1qJ3FM+DeceKMYH8QJeR5HgjfE45844P05UMJfZccpHwj7KpUJf5ZjccNpnZMCPZYZ3sMJLaasA3CrbEJSrvXkP7ax/ZoP8oi3PsZgrwkQ==".AesDecryptString();
        _testOutputHelper.WriteLine(result);
    }
}