﻿using System.Net;
using Microsoft.AspNetCore.Hosting;
using Novanet.Core.Services;
using Novanet.Core.ValueObjects;
using Service.Health.Enums;
using Service.Health.Models;

namespace Service.Health.Services;

public class GlobalMonitorService : BackgroundService
{
    private readonly AppSettings _appSettings;
    private readonly IConfiguration _configuration;
    private readonly IWebHostEnvironment _environment;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ILogger<GlobalMonitorService> _logger;
    private readonly IMailService _mailService;

    private readonly List<StatusMonitor> _statusMonitors;

    public GlobalMonitorService(
        ILogger<GlobalMonitorService> logger,
        IHttpClientFactory httpClientFactory,
        IMailService mailService,
        AppSettings appSettings,
        IWebHostEnvironment environment,
        IConfiguration configuration)
    {
        _logger = logger;
        _httpClientFactory = httpClientFactory;
        _mailService = mailService;
        _appSettings = appSettings;
        _environment = environment;
        _configuration = configuration;
        var urls = new Urls
        {
            Logo = configuration.GetValue<string>("Urls:0"),
            ProductImageOne = configuration.GetValue<string>("Urls:1"),
            ProductImageTwo = configuration.GetValue<string>("Urls:2"),
            Sdk = configuration.GetValue<string>("Urls:3"),
            Swiper = configuration.GetValue<string>("Urls:4"),
            FlyingCarpet = configuration.GetValue<string>("Urls:5"),
            AdviewOne = configuration.GetValue<string>("Urls:6"),
            AdviewTwo = configuration.GetValue<string>("Urls:7")
        };

        _statusMonitors = new List<StatusMonitor>
        {
            new()
            {
                Service = ServiceName.Storage, Error = false, ErrorTime = null, FailLinks = 0, Links = new List<string>
                {
                    urls.Logo,
                    urls.ProductImageOne,
                    urls.ProductImageTwo
                }
            },
            new()
            {
                Service = ServiceName.Js, Error = false, ErrorTime = null, FailLinks = 0, Links = new List<string>
                {
                    urls.Sdk,
                    urls.Swiper,
                    urls.FlyingCarpet
                }
            },
            new()
            {
                Service = ServiceName.Display, Error = false, ErrorTime = null, FailLinks = 0, Links = new List<string>
                {
                    urls.AdviewOne,
                    urls.AdviewTwo
                }
            }
        };
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        const int delaySendEmailTime = 30;

        while (true)
        {
            foreach (var statusMonitor in _statusMonitors)
            {
                var statusLinks = new List<ResponseData>();
                _logger.LogInformation("Worker {Worker} running at: {Time}",
                    $"{statusMonitor.Service} Monitor",
                    DateTimeOffset.Now);

                #region Logic Here

                foreach (var statusMonitorLink in statusMonitor.Links)
                {
                    var httpClient = await _httpClientFactory.CreateClient().GetAsync(
                        statusMonitorLink,
                        stoppingToken);
                    statusLinks.Add(new ResponseData
                    {
                        StatusCode = httpClient.StatusCode,
                        Link = statusMonitorLink
                    });
                }


                TimeSpan? countTime = null;
                if (statusMonitor.ErrorTime is not null)
                    countTime = DateTimeOffset.Now.Subtract((DateTimeOffset)statusMonitor.ErrorTime);

                var failLinks =
                    statusLinks.Count(x => !x.StatusCode.Equals(HttpStatusCode.OK));

                var successLinks =
                    statusLinks.Count(x => x.StatusCode.Equals(HttpStatusCode.OK));

                if (successLinks.Equals(statusMonitor.Links.Count))
                {
                    if (countTime is not null)
                    {
                        statusMonitor.Error = false;
                        statusMonitor.ErrorTime = null;
                        statusMonitor.FailLinks = 0;

                        foreach (var httpStatus in statusLinks)
                        foreach (var receiver in _appSettings.Smtp.Receivers)
                        {
                            _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                            if (httpStatus.Link != null)
                                SendEmail("Success", receiver.Email, receiver.Name, statusMonitor.Service,
                                    httpStatus.StatusCode, httpStatus.Link);
                            _logger.LogInformation("{Time} Sent notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                        }

                        _logger.LogInformation($"{statusMonitor.Service} service: Heathy");
                    }
                    else
                    {
                        _logger.LogInformation($"{statusMonitor.Service} service: Heathy");
                    }
                }
                else if ((successLinks > 0 && failLinks != statusMonitor.FailLinks) ||
                         (countTime != null && countTime.Value.Minutes > delaySendEmailTime) ||
                         statusMonitor.ErrorTime is null)
                {
                    statusMonitor.ErrorTime = DateTimeOffset.Now;
                    statusMonitor.FailLinks = failLinks;
                    foreach (var httpStatus in statusLinks)
                    foreach (var receiver in _appSettings.Smtp.Receivers)
                        if (httpStatus.StatusCode.Equals(HttpStatusCode.OK))
                        {
                            _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                            if (httpStatus.Link != null)
                                SendEmail("Success", receiver.Email, receiver.Name, statusMonitor.Service,
                                    httpStatus.StatusCode, httpStatus.Link);
                            _logger.LogInformation("{Time} Sent notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                        }
                        else
                        {
                            _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                            if (httpStatus.Link != null)
                                SendEmail("Error", receiver.Email, receiver.Name, statusMonitor.Service,
                                    httpStatus.StatusCode, httpStatus.Link);
                            _logger.LogInformation("{Time} Sent notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                        }

                    _logger.LogError($"{statusMonitor.Service} service: Error");
                }
                else
                {
                    _logger.LogError($"{statusMonitor.Service} service: Error");
                    if (statusMonitor.ErrorTime is not null &&
                        (countTime == null || countTime.Value.Minutes <= delaySendEmailTime)) continue;
                    statusMonitor.Error = true;
                    statusMonitor.ErrorTime = DateTimeOffset.Now;
                    statusMonitor.FailLinks = failLinks;

                    foreach (var httpStatus in statusLinks)
                    foreach (var receiver in _appSettings.Smtp.Receivers)
                    {
                        _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                            receiver.Email);
                        if (httpStatus.Link != null)
                            SendEmail("Error", receiver.Email, receiver.Name, statusMonitor.Service,
                                httpStatus.StatusCode, httpStatus.Link);
                    }
                }

                #endregion
            }

            await Task.Delay(3000, stoppingToken);
        }
    }

    private string MailTemplate(string serviceName, HttpStatusCode code, string link, DateTimeOffset datetime,
        string environment)
    {
        var template =
            $@"<html>
            <body>
            <p>Môi trường: {environment},</p>
            <p>{serviceName}: {(int)code} - {code},</p>
            <p>Link: {link}</p>
            <p>Thời gian: {datetime}</p>
            </body>
            </html>";

        return template;
    }

    private async void SendEmail(string status, string email, string name, ServiceName service,
        HttpStatusCode statusCode, string link)
    {
        var tryAgain = 10;
        var failed = false;
        do
        {
            try
            {
                await _mailService.SendAsync(_appSettings.Smtp, email,
                    name,
                    $"{status} - {_appSettings.MetaData.Title} - Thông báo hệ thống",
                    MailTemplate(
                        $"{service} Service",
                        statusCode,
                        link,
                        DateTimeOffset.Now,
                        _appSettings.MetaData.Title));
            }
            catch (Exception ex)
            {
                failed = true;
                tryAgain--;
                _logger.LogWarning("FAILED: {Time} Sent notification to: {To}", DateTimeOffset.Now, email);
            }
        } while (failed && tryAgain != 0);
    }
}