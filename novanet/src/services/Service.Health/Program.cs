using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Novanet.Core.Services;
using Novanet.Core.ValueObjects;
using Serilog;
using Serilog.Events;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .CreateLogger();

try
{
    var builder = WebApplication.CreateBuilder(args);
    var environment = builder.Environment;
    var environmentName = environment.EnvironmentName;
    var assemblyPath = Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location);
    var appSettings = new AppSettings();
    new ConfigurationBuilder()
        .AddJsonFile(Path.Combine(assemblyPath!, $"appsettings.{environmentName}.json"))
        .Build().Bind(appSettings);
    var services = builder.Services;
    var configuration = builder.Configuration;
    services.AddHttpClient();
    services.AddSingleton(appSettings);
    services.AddSingleton<IMailService, MailService>();
    // Status: Move to schedule | services.AddHostedService<GlobalMonitorService>();
    services.Configure<string[]>(configuration.GetSection("Urls"));

    var app = builder.Build();
    await app.RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
    Log.CloseAndFlush();
}
