﻿namespace Service.Health.Enums;

public enum ServiceName
{
    Storage = 1,
    Js = 2,
    Display = 3
}