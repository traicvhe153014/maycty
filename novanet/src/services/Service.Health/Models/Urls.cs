﻿namespace Service.Health.Models;

public class Urls
{
    public string? Logo { get; set; }
    public string? ProductImageOne { get; set; }
    public string? ProductImageTwo { get; set; }
    public string? AdviewOne { get; set; }
    public string? AdviewTwo { get; set; }
    public string? Sdk { get; set; }
    public string? Swiper { get; set; }
    public string? FlyingCarpet { get; set; }
}