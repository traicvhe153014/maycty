﻿using System.Net;

namespace Service.Health.Models;

public class ResponseData
{
    public HttpStatusCode StatusCode;
    public string? Link;
}