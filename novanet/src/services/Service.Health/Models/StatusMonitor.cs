﻿using Service.Health.Enums;
namespace Service.Health.Models;

public class StatusMonitor
{
    public ServiceName Service;
    public bool Error = false;
    public DateTimeOffset? ErrorTime;
    public int? FailLinks;
    public List<string> Links;
}