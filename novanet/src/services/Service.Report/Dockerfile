﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["src/services/Service.Report/Service.Report.csproj", "services/Service.Report/"]
COPY ["src/building-blocks/Novanet.Core/Novanet.Core.csproj", "building-blocks/Novanet.Core/"]
COPY ["src/building-blocks/Novanet.EventBus/Novanet.EventBus.csproj", "building-blocks/Novanet.EventBus/"]
COPY ["src/core-layer/NovanetCore.Business/NovanetCore.Business.csproj", "core-layer/NovanetCore.Business/"]
COPY ["src/core-layer/NovanetCore.ValueObject/NovanetCore.ValueObject.csproj", "core-layer/NovanetCore.ValueObject/"]
RUN dotnet restore "services/Service.Report/Service.Report.csproj"

WORKDIR "/src/building-blocks/Novanet.Core"
COPY ["src/building-blocks/Novanet.Core", "."]

WORKDIR "/src/building-blocks/Novanet.EventBus"
COPY ["src/building-blocks/Novanet.EventBus", "."]

WORKDIR "/src/core-layer/NovanetCore.Business"
COPY ["src/core-layer/NovanetCore.Business", "."]

WORKDIR "/src/core-layer/NovanetCore.ValueObject"
COPY ["src/core-layer/NovanetCore.ValueObject", "."]

WORKDIR "/src/services/Service.Report"
COPY ["src/services/Service.Report", "."]
RUN dotnet build "Service.Report.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Service.Report.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
EXPOSE 5004/tcp
ENV ASPNETCORE_URLS http://*:5004
ENTRYPOINT ["dotnet", "Service.Report.dll", "--server.urls", "http://*:5004"]
