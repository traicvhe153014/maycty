﻿using System.Diagnostics;
using MongoDB.Driver;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.ValueObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessObjects;
using Service.Report.Application.Commands;
using Service.Report.Domain;
using Service.Report.Domain.AggregateModels.TrackingAggregate;

namespace Service.Report.Events;

public class SyncWebsitePublisherReportService : EventBusListenerService<SyncWebsitePublisherReportSignal>
{
    public static bool IsProcessing;
    protected override bool Disabled => IsProcessing;
    protected override string Channel => MessageBusChannels.SyncWebsitePublisherReport;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<SyncWebsitePublisherReportService> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;
    
    public SyncWebsitePublisherReportService(IServiceProvider serviceProvider,
        ILogger<SyncWebsitePublisherReportService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        IsProcessing = false;
    }

    protected override async Task Processing(SyncWebsitePublisherReportSignal signal)
    {
        IsProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(SyncWebsitePublisherReportService));
        try
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var appSettings = scope.ServiceProvider.GetRequiredService<AppSettings>();
            var context = scope.ServiceProvider.GetRequiredService<TrackingContext>();
            var messageBusClient = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();
            var redisCacheService = scope.ServiceProvider.GetRequiredService<IRedisCacheService>();

            var oldPublisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == signal.OldPublisherId);
            var newPublisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == signal.NewPublisherId);

            if (oldPublisher is null || newPublisher is null)
            {
                return;
            }

            var syncCommand = new SyncReportCommand
            {
                Action = SyncReportAction.Sync,
                ReportType = SyncReportType.Publisher,
                PublisherDimensions = new List<ReportPublisherDimension> {ReportPublisherDimension.Publisher}
            };
            // remove existing publisher report key with old publisherSubId
            await SyncReportService.RemoveKeys(
                syncCommand,
                new List<TrackingMonthPartition>
                {
                    new() {PublisherId = oldPublisher.SubId, CreatedOn = DateTimeOffset.Now},
                    new() {PublisherId = oldPublisher.SubId, CreatedOn = DateTimeExtensions.OriginTime}
                },
                SyncReportService.GenerateLogConfig(syncCommand),
                redisCacheService);

            var updatePersistentBuilder = Builders<TrackingPersistentPartition>.Update;
            await context.TrackingPersistentPartitions
                .Collection(appSettings.MongoLongTime.Url, appSettings.MongoLongTime.Database)
                .UpdateManyAsync(
                    new ExpressionFilterDefinition<TrackingPersistentPartition>(x =>
                        x.PublisherId == oldPublisher.SubId && x.WebsiteId == signal.WebsiteSubId),
                    updatePersistentBuilder.Set(x => x.PublisherId, newPublisher.SubId));
            var updateMonthBuilder = Builders<TrackingMonthPartition>.Update;
            await context.TrackingMonthPartitions
                .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                .UpdateManyAsync(
                    new ExpressionFilterDefinition<TrackingMonthPartition>(x =>
                        x.PublisherId == oldPublisher.SubId && x.WebsiteId == signal.WebsiteSubId),
                    updateMonthBuilder.Set(x => x.PublisherId, newPublisher.SubId));
            var updateWeekBuilder = Builders<TrackingWeekPartition>.Update;
            await context.TrackingWeekPartitions
                .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                .UpdateManyAsync(
                    new ExpressionFilterDefinition<TrackingWeekPartition>(x =>
                        x.PublisherId == oldPublisher.SubId && x.WebsiteId == signal.WebsiteSubId),
                    updateWeekBuilder.Set(x => x.PublisherId, newPublisher.SubId));

            messageBusClient.Publish(MessageBusChannels.SyncReport, syncCommand);
        }
        catch (Exception e)
        {
            _logger.LogError("{Name} Error: {Error}", nameof(SyncWebsitePublisherReportService), e.ToString());
        }
        finally
        {
            stopwatch.Stop();
            _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(SyncWebsitePublisherReportService),
                stopwatch.ElapsedMilliseconds.ToString());
            IsProcessing = false;
        }
    }
}