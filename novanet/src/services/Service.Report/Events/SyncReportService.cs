﻿using System.Diagnostics;
using MongoDB.Bson;
using MongoDB.Driver;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Mapper;
using Novanet.Core.ValueObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessObjects;
using NovanetCore.Business.BusinessUtils;
using Service.Report.Application.Commands;
using Service.Report.Domain;
using Service.Report.Domain.AggregateModels.TrackingAggregate;

namespace Service.Report.Events;

public class SyncReportService : EventBusListenerService<SyncReportCommand>
{
    public static bool IsProcessing;
    protected override bool Disabled => IsProcessing;
    protected override string Channel => MessageBusChannels.SyncReport;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<SyncReportService> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;
    
    private const int BatchSize = 2000;
    
    public SyncReportService(IServiceProvider serviceProvider,
        ILogger<SyncReportService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        IsProcessing = false;
    }

    protected override async Task Processing(SyncReportCommand signal)
    {
        IsProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(SyncReportService));
        try
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var logConfig = GenerateLogConfig(signal);
            var context = scope.ServiceProvider.GetRequiredService<TrackingContext>();
            var redisLogService = scope.ServiceProvider.GetRequiredService<RedisLogService>();
            var redisCacheService = scope.ServiceProvider.GetRequiredService<IRedisCacheService>();
            var appSettings = scope.ServiceProvider.GetRequiredService<AppSettings>();

            var persistentQueryOptions = new FindOptions<TrackingPersistentPartition>
            {
                BatchSize = BatchSize
            };
            var monthQueryOptions = new FindOptions<TrackingMonthPartition>
            {
                BatchSize = BatchSize
            };
            var weekQueryOptions = new FindOptions<TrackingWeekPartition>
            {
                BatchSize = BatchSize
            };

            if (signal.ReportType == SyncReportType.All)
            {
                await redisCacheService.FlushDatabaseAsync((int) RedisDatabases.ServiceReport);
            }
            else
            {
                using var removeTrackingPersistentListCursor = await context.TrackingPersistentPartitions
                    .Collection(appSettings.MongoLongTime.Url, appSettings.MongoLongTime.Database)
                    .FindAsync(x =>
                        (signal.StartDate == null || x.CreatedOn > signal.StartDate) &&
                        (signal.EndDate == null || x.CreatedOn < signal.EndDate), persistentQueryOptions);
                while (await removeTrackingPersistentListCursor.MoveNextAsync())
                {
                    await RemoveKeys(signal,
                        removeTrackingPersistentListCursor.Current.Select(x => x.MapTo<TrackingMonthPartition>())
                            .ToList(), logConfig, redisCacheService);
                }
                _logger.LogInformation("{Name}: Finish remove persistent keys", nameof(SyncReportService));

                using var removeTrackingMonthListCursor = await context.TrackingMonthPartitions
                    .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                    .FindAsync(x =>
                        (signal.StartDate == null || x.CreatedOn > signal.StartDate) &&
                        (signal.EndDate == null || x.CreatedOn < signal.EndDate), monthQueryOptions);
                while (await removeTrackingMonthListCursor.MoveNextAsync())
                {
                    await RemoveKeys(signal, removeTrackingMonthListCursor.Current.ToList(), logConfig,
                        redisCacheService);
                }
                _logger.LogInformation("{Name}: Finish remove month keys", nameof(SyncReportService));

                using var removeTrackingWeekListCursor = await context.TrackingWeekPartitions
                    .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                    .FindAsync(x =>
                        (signal.StartDate == null || x.CreatedOn > signal.StartDate) &&
                        (signal.EndDate == null || x.CreatedOn < signal.EndDate), weekQueryOptions); 
                while (await removeTrackingWeekListCursor.MoveNextAsync())
                {
                    await RemoveKeys(signal,
                        removeTrackingWeekListCursor.Current.Select(x => x.MapTo<TrackingMonthPartition>()).ToList(),
                        logConfig,
                        redisCacheService);
                }
            }
            _logger.LogInformation("{Name}: Finish remove keys", nameof(SyncReportService));

            // generate from persistent tracking
            using var trackingPersistentListCursor = await context.TrackingPersistentPartitions
                .Collection(appSettings.MongoLongTime.Url, appSettings.MongoLongTime.Database)
                .FindAsync(x =>
                    (signal.StartDate == null || x.CreatedOn > signal.StartDate) &&
                    (signal.EndDate == null || x.CreatedOn < signal.EndDate), persistentQueryOptions);
            while (await trackingPersistentListCursor.MoveNextAsync())
            {
                await GenerateKeyReports(trackingPersistentListCursor.Current.Select(x => x.MapTo<TrackingMonthPartition>()),
                    logConfig, redisLogService);
            }
            _logger.LogInformation("{Name}: Finish generate persistent keys", nameof(SyncReportService));

            // generate from month tracking
            using var trackingMonthListCursor = await context.TrackingMonthPartitions
                .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                .FindAsync(x =>
                    (signal.StartDate == null || x.CreatedOn > signal.StartDate) &&
                    (signal.EndDate == null || x.CreatedOn < signal.EndDate), monthQueryOptions);
            while (await trackingMonthListCursor.MoveNextAsync()) 
            { 
                await GenerateKeyReports(trackingMonthListCursor.Current, logConfig, redisLogService); 
            }
            _logger.LogInformation("{Name}: Finish generate month keys", nameof(SyncReportService));

            // generate from week tracking
            PipelineDefinition<TrackingWeekPartition, TrackingWeekPartition> pipeline = new BsonDocument[] { };
            if (signal.StartDate != null && signal.EndDate != null)
            {
                pipeline = pipeline.AppendStage<TrackingWeekPartition, TrackingWeekPartition, TrackingWeekPartition>(
                    new BsonDocument("$match", new BsonDocument("$and", new BsonArray
                    {
                        new BsonDocument("CreatedOn.0", new BsonDocument("$gt", signal.StartDate.Value.Ticks)),
                        new BsonDocument("CreatedOn.0", new BsonDocument("$lt", signal.EndDate.Value.Ticks))
                    })));
            }

            pipeline = pipeline.AppendStage<TrackingWeekPartition, TrackingWeekPartition, TrackingWeekPartition>(
                new BsonDocument("$lookup", new BsonDocument
                {
                    {"from", "TrackingMonthPartitions"},
                    {"localField", "_id"},
                    {"foreignField", "_id"},
                    {"as", "TrackingMonthPartitions"}
                }));
            pipeline = pipeline.AppendStage<TrackingWeekPartition, TrackingWeekPartition, TrackingWeekPartition>(
                new BsonDocument("$match", new BsonDocument("TrackingMonthPartitions", new BsonArray())));
            pipeline = pipeline.AppendStage<TrackingWeekPartition, TrackingWeekPartition, TrackingWeekPartition>(
                new BsonDocument("$project", new BsonDocument("TrackingMonthPartitions", new BsonInt32(0))));

            using var trackingWeekListCursor = await context.TrackingWeekPartitions
                .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                .AggregateAsync(pipeline, new AggregateOptions
                {
                    BatchSize = BatchSize
                });
             while (await trackingWeekListCursor.MoveNextAsync())
             {
                 await GenerateKeyReports(
                     trackingWeekListCursor.Current.Select(x => x.MapTo<TrackingMonthPartition>()),
                     logConfig,
                     redisLogService);
             }
        }
        catch (Exception e)
        {
            _logger.LogError("{Name} Error: {Error}", nameof(SyncReportService), e.ToString());
        }
        finally
        {
            stopwatch.Stop();
            _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(SyncReportService),
                stopwatch.ElapsedMilliseconds.ToString());
            IsProcessing = false;
        }
    }

    public static LogConfig GenerateLogConfig(SyncReportCommand signal)
    {
        var logConfig = new LogConfig
        {
            IsLogAdverMoney = true,
            IsLogPubMoney = true
        };
        // single advertiser dimensions
        if (signal.ReportType is SyncReportType.Advertiser or SyncReportType.All)
        {
            if (signal.AdvertiserDimensions is null)
            {
                logConfig.EAdverForSingleObjLog = new List<ReportAdvertiserDimension>
                {
                    ReportAdvertiserDimension.Advertising,
                    ReportAdvertiserDimension.AdvertisingSet,
                    ReportAdvertiserDimension.Campaign,
                    ReportAdvertiserDimension.Advertiser,
                    ReportAdvertiserDimension.Product,
                    ReportAdvertiserDimension.ProductFeed,
                    ReportAdvertiserDimension.ProductGroup,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    ReportAdvertiserDimension.AdvertisingTemplate
                };
            }
            else
            {
                logConfig.EAdverForSingleObjLog = signal.AdvertiserDimensions;
            }
        }
        
        // single publisher dimensions
        if (signal.ReportType is SyncReportType.Publisher or SyncReportType.All)
        {
            if (signal.PublisherDimensions is null)
            {
                logConfig.EPubForSingleObjLog = new List<ReportPublisherDimension>
                {
                    ReportPublisherDimension.Zone,
                    ReportPublisherDimension.Website,
                    ReportPublisherDimension.Publisher
                };
            }
            else
            {
                logConfig.EPubForSingleObjLog = signal.PublisherDimensions;
            }
        }
        
        // single client dimensions
        if (signal.ReportType is SyncReportType.Client or SyncReportType.All)
        {
            if (signal.ClientDimensions is null)
            {
                logConfig.EClientSingleObjLog = new List<ReportClientDimension>
                {
                    ReportClientDimension.ClientId
                };
            }
            else
            {
                logConfig.EClientSingleObjLog = signal.ClientDimensions;
            }
        }
        
        // advertiser targeting dimensions
        if (signal.ReportType is SyncReportType.AdvertiserTargeting or SyncReportType.All)
        {
            if (signal.AdvertiserTargetingDimensions is null)
            {
                logConfig.EAdverForAdverTargeting = new List<ReportAdvertiserDimension>
                    {ReportAdvertiserDimension.Campaign};
                logConfig.ETargetForAdverTargeting = new List<ReportTargetingDimension>
                    {ReportTargetingDimension.Location};
            }
            else
            {
                logConfig.EAdverForAdverTargeting = signal.AdvertiserTargetingDimensions.Advertisers ??
                                                    new List<ReportAdvertiserDimension>();
                logConfig.ETargetForAdverTargeting = signal.AdvertiserTargetingDimensions.Targeting ??
                                                     new List<ReportTargetingDimension>();
            }
        }
        
        // advertiser publisher dimensions
        if (signal.ReportType is SyncReportType.AdvertiserPublisher or SyncReportType.All)
        {
            if (signal.AdvertiserPublisherDimensions is null)
            {
                logConfig.EAdverForAdverPub = new List<ReportAdvertiserDimension>
                    {ReportAdvertiserDimension.Campaign, ReportAdvertiserDimension.AdvertisingSet};
                logConfig.EPubForAdverPub = new List<ReportPublisherDimension>
                    {ReportPublisherDimension.Website};
            }
            else
            {
                logConfig.EAdverForAdverPub = signal.AdvertiserPublisherDimensions.Advertisers ??
                                              new List<ReportAdvertiserDimension>();
                logConfig.EPubForAdverPub = signal.AdvertiserPublisherDimensions.Publishers ??
                                            new List<ReportPublisherDimension>();
            }
        }
        
        // multiple advertiser dimensions
        if (signal.ReportType is SyncReportType.MultipleAdvertiser or SyncReportType.All)
        {
            if (signal.MultipleAdvertiserDimensions is null)
            {
                logConfig.EAdverForMultipleObjLog = new List<List<ReportAdvertiserDimension>>
                {
                    new()
                    {
                        ReportAdvertiserDimension.Advertising,
                        ReportAdvertiserDimension.AdvertisingTemplate
                    },
                    new()
                    {
                        ReportAdvertiserDimension.Campaign,
                        ReportAdvertiserDimension.Product
                    }
                };
            }
            else
            {
                logConfig.EAdverForMultipleObjLog = signal.MultipleAdvertiserDimensions;
            }
        }

        return logConfig;
    }

    public static async Task RemoveKeys(
        SyncReportCommand signal,
        List<TrackingMonthPartition> trackingList,
        LogConfig logConfig,
        IRedisCacheService redisCacheService)
    {
        var metrics = new[]
        {
            ReportMetric.Click,
            ReportMetric.View,
            ReportMetric.BackupView,
            ReportMetric.BannerClick,
            ReportMetric.DefaultBannerView,
            ReportMetric.Earned,
            ReportMetric.Paid,
            ReportMetric.Purchase,
            ReportMetric.Conversion,
        };
        var campaignIdSet = new HashSet<long>();
        var advertisingSetSet = new HashSet<long>();
        var advertisingSet = new HashSet<long>();
        var advertiserSet = new HashSet<long>();
        var productSet = new HashSet<long>();
        var productFeedSet = new HashSet<long>();
        var advertisingSetProductGroupSet = new HashSet<long>();

        foreach (var trackingMonthPartition in trackingList)
        {
            campaignIdSet.Add(trackingMonthPartition.CampaignId);
            advertisingSetSet.Add(trackingMonthPartition.AdvertisingSetId);
            advertisingSet.Add(trackingMonthPartition.AdvertisingId);
            advertiserSet.Add(trackingMonthPartition.AdvertiserId);
            foreach (var productId in trackingMonthPartition.ProductIds ?? new List<long>())
            {
                productSet.Add(productId);
            }
            productFeedSet.Add(trackingMonthPartition.ProductFeedId);
            foreach (var advertisingSetProductGroupId in trackingMonthPartition.AdvertisingSetProductGroupIds ?? new List<long>())
            {
                advertisingSetProductGroupSet.Add(advertisingSetProductGroupId);
            }
        }
        var advertiserIds = new Dictionary<ReportAdvertiserDimension, List<long>>
        {
            {ReportAdvertiserDimension.Campaign, campaignIdSet.ToList()},
            {ReportAdvertiserDimension.AdvertisingSet, advertisingSetSet.ToList()},
            {ReportAdvertiserDimension.Advertising, advertisingSet.ToList()},
            {ReportAdvertiserDimension.Advertiser, advertiserSet.ToList()},
            {ReportAdvertiserDimension.Product, productSet.ToList()},
            {ReportAdvertiserDimension.ProductFeed, productFeedSet.ToList()},
            {ReportAdvertiserDimension.AdvertisingSetProductGroup, advertisingSetProductGroupSet.ToList()},
            {ReportAdvertiserDimension.AdvertisingTemplate, CacheManager.Templates.Values.Select(x => x.SubId).ToList()},
        };

        var publisherIdSet = new HashSet<long>();
        var websiteSet = new HashSet<long>();
        var zoneSet = new HashSet<long>();
        foreach (var trackingMonthPartition in trackingList)
        {
            publisherIdSet.Add(trackingMonthPartition.PublisherId);
            websiteSet.Add(trackingMonthPartition.WebsiteId);
            zoneSet.Add(trackingMonthPartition.ZoneId);
        }
        var publisherIds = new Dictionary<ReportPublisherDimension, List<long>>
        {
            {ReportPublisherDimension.Publisher, publisherIdSet.ToList()},
            {ReportPublisherDimension.Website, websiteSet.ToList()},
            {ReportPublisherDimension.Zone, zoneSet.ToList()},
        };

        var targetingIds = new Dictionary<ReportTargetingDimension, List<long>>
        {
            {ReportTargetingDimension.Location, CacheManager.Locations.Keys.ToList()},
        };

        var minHour = trackingList.Min(x => x.CreatedOn).GetHourOfYear();
        var maxHour = trackingList.Max(x => x.CreatedOn).GetHourOfYear();
        var minHourPart = int.MaxValue;
        var maxHourPart = 0;
        for (var i = minHour; i <= maxHour; i++)
        {
            _ = int.TryParse(DateTimeExtensions.GetHourParts(i)[0], out var hourPart);
            if (hourPart > maxHourPart)
            {
                maxHourPart = hourPart;
            }

            if (hourPart < minHourPart)
            {
                minHourPart = hourPart;
            }
        }

        // remove single advertiser keys
        if (signal.ReportType is SyncReportType.Advertiser or SyncReportType.All)
        {
            await RemoveSingleKeys(logConfig.EAdverForSingleObjLog ?? new List<ReportAdvertiserDimension>(), metrics,
                minHourPart, maxHourPart, advertiserIds,
                RedisReportKeys.GetAdverKey,
                RedisReportKeys.GetAdverKey,
                redisCacheService
            );
        }
        
        // remove single publisher keys
        if (signal.ReportType is SyncReportType.Publisher or SyncReportType.All)
        {
            await RemoveSingleKeys(logConfig.EPubForSingleObjLog ?? new List<ReportPublisherDimension>(), metrics,
                minHourPart, maxHourPart, publisherIds,
                RedisReportKeys.GetPubKey,
                RedisReportKeys.GetPubKey,
                redisCacheService
            );
        }
        
        // remove single client keys
        if (signal.ReportType is SyncReportType.Client or SyncReportType.All)
        {
            foreach (var dimension in logConfig.EClientSingleObjLog ?? new List<ReportClientDimension>())
            {
                foreach (var metric in metrics)
                {
                    var keys = redisCacheService.EnumerateKeyByPatternAsync(new RedisKey
                    {
                        Database = (int) RedisDatabases.ServiceReport,
                        KeyName = $"{dimension}_{metric}_*"
                    });
                    await foreach (var key in keys)
                    {
                        await redisCacheService.KeyDeleteAsync(new RedisKey
                        {
                            Database = (int) RedisDatabases.ServiceReport,
                            KeyName = key
                        });
                    }
                }
            }
        }

        // remove advertiser publisher keys
        if (signal.ReportType is SyncReportType.AdvertiserPublisher or SyncReportType.All)
        {
            foreach (var publisherDimension in logConfig.EPubForAdverPub ?? new List<ReportPublisherDimension>())
            {
                foreach (var advertiserDimension in
                         logConfig.EAdverForAdverPub ?? new List<ReportAdvertiserDimension>())
                {
                    var pubIds = publisherIds.TryGetValue(publisherDimension, out var pubValue) ? pubValue : new List<long>();
                    var adverIds = advertiserIds.TryGetValue(advertiserDimension, out var adverValue) ? adverValue : new List<long>();
                    foreach (var pubId in pubIds)
                    {
                        foreach (var adverId in adverIds)
                        {
                            foreach (var metric in metrics)
                            {
                                await redisCacheService.KeyDeleteAsync(
                                    RedisReportKeys.GetAdver_PubKey(advertiserDimension, publisherDimension, metric,
                                        adverId, pubId));
                            }
                        }
                    }
                }
            }
        }

        // remove advertiser targeting keys
        if (signal.ReportType is SyncReportType.AdvertiserTargeting or SyncReportType.All)
        {
            foreach (var advertiserDimension in
                     logConfig.EAdverForAdverTargeting ?? new List<ReportAdvertiserDimension>())
            {
                foreach (var targetingDimension in logConfig.ETargetForAdverTargeting ??
                                                   new List<ReportTargetingDimension>())
                {
                    var targetIds = targetingIds.TryGetValue(targetingDimension, out var targetValue)
                        ? targetValue
                        : new List<long>();
                    var adverIds = advertiserIds.TryGetValue(advertiserDimension, out var adverValue)
                        ? adverValue
                        : new List<long>();
                    foreach (var adverId in adverIds)
                    {
                        foreach (var targetId in targetIds)
                        {
                            foreach (var metric in metrics)
                            {
                                await redisCacheService.KeyDeleteAsync(
                                    RedisReportKeys.GetAdver_TargetingKey(advertiserDimension, targetingDimension,
                                        metric,
                                        adverId, targetId));
                            }
                        }
                    }
                }
            }
        }
        
        // remove multiple advertiser keys
        if (signal.ReportType is SyncReportType.MultipleAdvertiser or SyncReportType.All)
        {
            foreach (var advertiserDimensions in
                     logConfig.EAdverForMultipleObjLog ?? new List<List<ReportAdvertiserDimension>>())
            {
                var idLists = new List<List<long>>();
                foreach (var dimension in advertiserDimensions)
                {
                    var ids = advertiserIds.TryGetValue(dimension, out var value) ? value : new List<long>();
                    idLists.Add(ids);
                }

                foreach (var ids in idLists.GetPermutations())
                {
                    var idList = ids.ToList();
                    foreach (var metric in metrics)
                    {
                        await redisCacheService.KeyDeleteAsync(
                            RedisReportKeys.GetAdverKey(advertiserDimensions,
                                metric,
                                idList));
                    }
                }
            }
        }
    }

    private static async Task GenerateKeyReports(
        IEnumerable<TrackingMonthPartition> trackingList,
        LogConfig logConfig,
        RedisLogService redisLogService)
    {
        var arrMoney = new[]
        {
            ReportMetric.Earned,
            ReportMetric.PromotionEarned,
            ReportMetric.Paid,
            ReportMetric.PromotionPaid
        };
        var groupedTrackings = trackingList.GroupBy(x => x.TrackingType);
        foreach (var groupedTracking in groupedTrackings)
        {
            var metric = ConvertUtils.GetMetric(groupedTracking.Key);
            if (metric is null)
            {
                continue;
            }

            var processTrackedItems = groupedTracking.Where(x => !x.IsDefault && !x.IsBackup).Cast<TrackingCore>()
                .ToList();
            await redisLogService.Save(
                processTrackedItems,
                arrMoney,
                metric.Value,
                logConfig);
            if (metric == ReportMetric.Click)
            {
                var bannerClickTrackedItems = groupedTracking
                    .Where(x => x.ProductIds == null || x.ProductIds.Count == 0)
                    .Cast<TrackingCore>()
                    .ToList();
                await redisLogService.Save(
                    bannerClickTrackedItems,
                    arrMoney,
                    ReportMetric.BannerClick,
                    AdvertisingLogFlagService.BannerClickLogConfig);
            }

            if (metric == ReportMetric.View)
            {
                var bannerViewTrackedItems = groupedTracking
                    .Where(x => x.IsDefault)
                    .Cast<TrackingCore>()
                    .ToList();
                await redisLogService.Save(
                    bannerViewTrackedItems,
                    arrMoney,
                    ReportMetric.DefaultBannerView,
                    AdvertisingLogFlagService.BannerViewLogConfig);

                var backupViewTrackedItems = groupedTracking
                    .Where(x => x.IsBackup)
                    .Cast<TrackingCore>()
                    .ToList();
                await redisLogService.Save(
                    backupViewTrackedItems,
                    arrMoney,
                    ReportMetric.BackupView,
                    AdvertisingLogFlagService.BannerViewLogConfig);
            }
        }
    }
    
    private static async Task RemoveSingleKeys<TDimension, TKey>(
        List<TDimension> dimensions,
        ReportMetric[] metrics,
        int minHourPart,
        int maxHourPart,
        Dictionary<TDimension, List<TKey>> idsDictionary,
        Func<TDimension, ReportMetric, TKey, RedisKey> getRedisKey,
        Func<TDimension, ReportMetric, string, TKey, RedisKey> getHourRedisKey,
        IRedisCacheService redisCacheService) where TDimension : Enum
    {
        foreach (var dimension in dimensions)
        {
            var ids = idsDictionary.TryGetValue(dimension, out var value) ? value : new List<TKey>();
            foreach (var id in ids)
            {
                foreach (var metric in metrics)
                {
                    await redisCacheService.KeyDeleteAsync(getRedisKey(dimension, metric, id));
                    for (var i = minHourPart; i <= maxHourPart; i++)
                    {
                        await redisCacheService.KeyDeleteAsync(getHourRedisKey(dimension, metric, i.ToString(), id));
                    }
                }
            }
        }
    }
}

public class AggregatedTrackingWeekPartition : TrackingWeekPartition
{
    public List<TrackingMonthPartition>? TrackingMonthPartitions { get; set; }
}