﻿using System.Diagnostics;
using MongoDB.Driver;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.ValueObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessObjects;
using Service.Report.Application.Commands;
using Service.Report.Domain;
using Service.Report.Domain.AggregateModels.TrackingAggregate;

namespace Service.Report.Events;

public class BatchSyncWebsitePublisherReportService : EventBusListenerService<List<SyncWebsitePublisherReportSignal>>
{
    public static bool IsProcessing;
    protected override bool Disabled => IsProcessing;
    protected override string Channel => MessageBusChannels.BatchSyncWebsitePublisherReport;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<BatchSyncWebsitePublisherReportService> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;
    
    public BatchSyncWebsitePublisherReportService(IServiceProvider serviceProvider,
        ILogger<BatchSyncWebsitePublisherReportService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        IsProcessing = false;
    }

    protected override async Task Processing(List<SyncWebsitePublisherReportSignal> signal)
    {
        IsProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(BatchSyncWebsitePublisherReportService));
        try
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var appSettings = scope.ServiceProvider.GetRequiredService<AppSettings>();
            var context = scope.ServiceProvider.GetRequiredService<TrackingContext>();
            var messageBusClient = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();
            var redisCacheService = scope.ServiceProvider.GetRequiredService<IRedisCacheService>();

            var syncCommand = new SyncReportCommand
            {
                Action = SyncReportAction.Sync,
                ReportType = SyncReportType.Publisher,
                PublisherDimensions = new List<ReportPublisherDimension> {ReportPublisherDimension.Publisher}
            };

            foreach (var websiteSignal in signal)
            {
                var oldPublisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == websiteSignal.OldPublisherId);
                var newPublisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == websiteSignal.NewPublisherId);

                if (oldPublisher is null || newPublisher is null)
                {
                    continue;
                }

                // remove existing publisher report key with old publisherSubId
                await SyncReportService.RemoveKeys(
                    syncCommand,
                    new List<TrackingMonthPartition>
                    {
                        new() {PublisherId = oldPublisher.SubId, CreatedOn = DateTimeOffset.Now},
                        new() {PublisherId = oldPublisher.SubId, CreatedOn = DateTimeExtensions.OriginTime}
                    },
                    SyncReportService.GenerateLogConfig(syncCommand),
                    redisCacheService);
                _logger.LogInformation(
                    "{Name}: Remove existing old publisher report key finished. PublisherId: {PublisherId}",
                    nameof(BatchSyncWebsitePublisherReportService), oldPublisher.SubId);

                var updatePersistentBuilder = Builders<TrackingPersistentPartition>.Update;
                await context.TrackingPersistentPartitions
                    .Collection(appSettings.MongoLongTime.Url, appSettings.MongoLongTime.Database)
                    .UpdateManyAsync(
                        new ExpressionFilterDefinition<TrackingPersistentPartition>(x =>
                            x.PublisherId == oldPublisher.SubId && x.WebsiteId == websiteSignal.WebsiteSubId),
                        updatePersistentBuilder.Set(x => x.PublisherId, newPublisher.SubId));
                _logger.LogInformation(
                    "{Name}: Update persistent tracking finished. PublisherId: {PublisherId}",
                    nameof(BatchSyncWebsitePublisherReportService), oldPublisher.SubId);
                var updateMonthBuilder = Builders<TrackingMonthPartition>.Update;
                await context.TrackingMonthPartitions
                    .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                    .UpdateManyAsync(
                        new ExpressionFilterDefinition<TrackingMonthPartition>(x =>
                            x.PublisherId == oldPublisher.SubId && x.WebsiteId == websiteSignal.WebsiteSubId),
                        updateMonthBuilder.Set(x => x.PublisherId, newPublisher.SubId));
                _logger.LogInformation(
                    "{Name}: Update month tracking finished. PublisherId: {PublisherId}",
                    nameof(BatchSyncWebsitePublisherReportService), oldPublisher.SubId);
                var updateWeekBuilder = Builders<TrackingWeekPartition>.Update;
                await context.TrackingWeekPartitions
                    .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                    .UpdateManyAsync(
                        new ExpressionFilterDefinition<TrackingWeekPartition>(x =>
                            x.PublisherId == oldPublisher.SubId && x.WebsiteId == websiteSignal.WebsiteSubId),
                        updateWeekBuilder.Set(x => x.PublisherId, newPublisher.SubId));
                _logger.LogInformation(
                    "{Name}: Update week tracking finished. PublisherId: {PublisherId}",
                    nameof(BatchSyncWebsitePublisherReportService), oldPublisher.SubId);
            }
            messageBusClient.Publish(MessageBusChannels.SyncReport, syncCommand);
        }
        catch (Exception e)
        {
            _logger.LogError("{Name} Error: {Error}", nameof(BatchSyncWebsitePublisherReportService), e.ToString());
        }
        finally
        {
            stopwatch.Stop();
            _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(BatchSyncWebsitePublisherReportService),
                stopwatch.ElapsedMilliseconds.ToString());
            IsProcessing = false;
        }
    }
}