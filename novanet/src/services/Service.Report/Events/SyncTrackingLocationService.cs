﻿using System.Diagnostics;
using MongoDB.Driver;
using Novanet.Core.ValueObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessUtils;
using Service.Report.Domain;
using Service.Report.Domain.AggregateModels.TrackingAggregate;

namespace Service.Report.Events;

public class SyncTrackingLocationService: EventBusListenerService<object>
{
    public static bool IsProcessing;
    protected override bool Disabled => IsProcessing;
    protected override string Channel => MessageBusChannels.SyncTrackingLocation;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<SyncTrackingLocationService> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;
    
    private const int BatchSize = 2000;
    
    public SyncTrackingLocationService(IServiceProvider serviceProvider,
        ILogger<SyncTrackingLocationService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        IsProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        IsProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(SyncTrackingLocationService));
        try
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<TrackingContext>();
            var appSettings = scope.ServiceProvider.GetRequiredService<AppSettings>();
            var locationService = scope.ServiceProvider.GetRequiredService<ILocationService>();
            var persistentQueryOptions = new FindOptions<TrackingPersistentPartition>
            {
                BatchSize = BatchSize
            };
            var monthQueryOptions = new FindOptions<TrackingMonthPartition>
            {
                BatchSize = BatchSize
            };
            var weekQueryOptions = new FindOptions<TrackingWeekPartition>
            {
                BatchSize = BatchSize
            };
            // update location from persistent tracking
            using var trackingPersistentListCursor = await context.TrackingPersistentPartitions
                .Collection(appSettings.MongoLongTime.Url, appSettings.MongoLongTime.Database)
                .FindAsync(x => x.Location == null || x.Location == 0, persistentQueryOptions);
            while (await trackingPersistentListCursor.MoveNextAsync())
            {
                foreach (var partition in trackingPersistentListCursor.Current)
                {
                    var ipLocation = await locationService.IPToLocation(ConvertUtils.ConvertIp(partition.Ip));
                    var location =
                        CacheManager.Locations.Values.FirstOrDefault(x => x.ProvinceId == ipLocation?.VnProvinceId);
                    partition.Location = location?.SubId ?? default;
                    partition.IpLocation = ipLocation?.SubId ?? default;
                    await context.TrackingPersistentPartitions.UpdateAsync(partition.Id, partition);
                }
            }
            _logger.LogInformation("{Name}: Finish syncing location for persistent keys", nameof(SyncTrackingLocationService));
            
            // update location from month tracking
            using var trackingMonthListCursor = await context.TrackingMonthPartitions
                .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                .FindAsync(x => x.Location == null || x.Location == 0, monthQueryOptions);
            while (await trackingMonthListCursor.MoveNextAsync())
            {
                foreach (var partition in trackingMonthListCursor.Current)
                {
                    var ipLocation = await locationService.IPToLocation(ConvertUtils.ConvertIp(partition.Ip));
                    var location =
                        CacheManager.Locations.Values.FirstOrDefault(x => x.ProvinceId == ipLocation?.VnProvinceId);
                    partition.Location = location?.SubId ?? default;
                    partition.IpLocation = ipLocation?.SubId ?? default;
                    await context.TrackingMonthPartitions.UpdateAsync(partition.Id, partition);
                }
            }
            _logger.LogInformation("{Name}: Finish syncing location for month keys", nameof(SyncTrackingLocationService));
            
            // update location from week tracking
            using var trackingWeekListCursor = await context.TrackingWeekPartitions
                .Collection(appSettings.MongoShortTime.Url, appSettings.MongoShortTime.Database)
                .FindAsync(x => x.Location == null || x.Location == 0, weekQueryOptions);
            while (await trackingWeekListCursor.MoveNextAsync())
            {
                foreach (var partition in trackingWeekListCursor.Current)
                {
                    var ipLocation = await locationService.IPToLocation(ConvertUtils.ConvertIp(partition.Ip));
                    var location =
                        CacheManager.Locations.Values.FirstOrDefault(x => x.ProvinceId == ipLocation?.VnProvinceId);
                    partition.Location = location?.SubId ?? default;
                    partition.IpLocation = ipLocation?.SubId ?? default;
                    await context.TrackingWeekPartitions.UpdateAsync(partition.Id, partition);
                }
            }
            _logger.LogInformation("{Name}: Finish syncing location for week keys", nameof(SyncTrackingLocationService));
        }
        catch (Exception e)
        {
            _logger.LogError("{Name} Error: {Error}", nameof(SyncTrackingLocationService), e.ToString());
        }
        finally
        {
            stopwatch.Stop();
            _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(SyncTrackingLocationService),
                stopwatch.ElapsedMilliseconds.ToString());
            IsProcessing = false;
        }
    }
}