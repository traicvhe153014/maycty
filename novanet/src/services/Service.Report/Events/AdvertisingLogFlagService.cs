﻿using System.Diagnostics;
using System.IO.Compression;
using MongoDB.Driver;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessConfigs;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessObjects;
using NovanetCore.Business.BusinessUtils;
using Service.Report.Domain;
using Service.Report.Domain.AggregateModels.TrackingAggregate;
using System;
using System.Text;
using Novanet.Core.ValueObjects;

namespace Service.Report.Events;

public class AdvertisingLogFlagService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.AdvertisingLogFlag;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<AdvertisingLogFlagService> _logger;

    public AdvertisingLogFlagService(IServiceProvider serviceProvider,
        ILogger<AdvertisingLogFlagService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(AdvertisingLogFlagService));
        try
        {
            var redisQueueService = ServiceProvider.GetRequiredService<IRedisQueueService>();
            var redisLogService = ServiceProvider.GetRequiredService<RedisLogService>();
            var appSettings = ServiceProvider.GetRequiredService<AppSettings>();

            const int timeToGetKey = ConfigValues.TotalMinuteGetData;
            var currentTime = DateTimeOffset.UtcNow.GetMinuteOfYear();
            var endTime = currentTime - 1;
            var startTime = endTime - timeToGetKey;
            var typeLogs = new List<ReportMetric>
            {
                ReportMetric.Click,
                ReportMetric.View,
                ReportMetric.Conversion,
                ReportMetric.Video25Percent,
                ReportMetric.Video50Percent,
                ReportMetric.Video75Percent,
                ReportMetric.Video100Percent,
                ReportMetric.VideoView3s
            };
            foreach (var typeLog in typeLogs)
            {
                for (var i = startTime; i <= endTime; i++)
                {
                    var queuedListKey = RedisReportKeys.QueueKey(typeLog, i);
                    var trackedItems = (await redisQueueService.ListAsync(queuedListKey))
                        .Select(x => x.ToString().Deserialize<TrackingWeekPartition>()).ToList();
                    
                    // save to file
                    var zipFilePath = Path.Combine(appSettings.StorageSettings?.Saved ?? "C:",
                        appSettings.StorageSettings?.Bucket ?? "novanet-trackings");
                    if (!Directory.Exists(zipFilePath))
                    {
                        Directory.CreateDirectory(zipFilePath);
                    }
                    var fileName = $"TRACKING_{typeLog}_{i}";
                    await using var fileStream = new FileStream(Path.Combine(zipFilePath, fileName + ".zip"),
                        FileMode.OpenOrCreate);
                    var currentFileData = new List<TrackingWeekPartition>();
                    if (fileStream.Length > 0)
                    {
                        try
                        {
                            currentFileData = Encoding.ASCII.GetString(fileStream.ReadToEnd())
                                .Deserialize<List<TrackingWeekPartition>>();
                        }
                        catch (Exception)
                        {
                            // ignored
                        }
                    }
                    currentFileData.AddRange(trackedItems);

                    await using var memoryStream = new MemoryStream();
                    using var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, leaveOpen: true);
                    var zipEntry = zipArchive.CreateEntry(fileName + ".txt");
                    await using var entryStream = zipEntry.Open();
                    var trackedItemsBytes = Encoding.ASCII.GetBytes(currentFileData.Serialize());
                    await entryStream.WriteAsync(trackedItemsBytes);
                    zipArchive.Dispose();
                    var zipBytes = memoryStream.ToArray();

                    fileStream.Seek(0, SeekOrigin.Begin);
                    await fileStream.WriteAsync(zipBytes);

                    var arrMoney = new[]
                    {
                        ReportMetric.Earned,
                        ReportMetric.PromotionEarned,
                        ReportMetric.Paid,
                        ReportMetric.PromotionPaid
                    };

                    if (trackedItems.Count <= 0) continue;
                    {
                        var logConfig = new LogConfig();
                        switch (typeLog)
                        {
                            // TODO: DONE # Hòa NX - Logic log View chưa có ?
                            // log view đang lấy chung case với log conversion
                            case ReportMetric.View:
                                //log chiều domain, zone theo thời gian => báo cáo tỉ lệ trống                                
                                logConfig = CoreConfig;
                                break;
                            case ReportMetric.Click: //log all
                                logConfig = CoreConfig;
                                break;
                            case ReportMetric.Conversion:
                                logConfig = ConversionConfig;
                                break;
                            case ReportMetric.Video25Percent:
                            case ReportMetric.Video50Percent:
                            case ReportMetric.Video75Percent:
                            case ReportMetric.Video100Percent:
                            case ReportMetric.VideoView3s:
                                logConfig = VideoViewConfig;
                                break;
                        }

                        // Note: đoạn này đã lọc ra các tracking theo click, impression riêng
                        // nên khi log tiền là log theo CPM riêng, CPC riêng
                        //create report keys
                        var processTrackedItems = trackedItems.Where(x => !x.IsDefault && !x.IsBackup).Cast<TrackingCore>().ToList();
                        await redisLogService.Save(
                            processTrackedItems,
                            arrMoney,
                            typeLog,
                            logConfig);
                        if (typeLog == ReportMetric.Click)
                        {
                            var bannerClickTrackedItems = trackedItems
                                .Where(x => x.ProductIds == null || x.ProductIds.Count == 0)
                                .Cast<TrackingCore>()
                                .ToList();
                            await redisLogService.Save(
                                bannerClickTrackedItems,
                                arrMoney,
                                ReportMetric.BannerClick,
                                BannerClickLogConfig);
                        }

                        if (typeLog == ReportMetric.View)
                        {
                            var bannerViewTrackedItems = trackedItems
                                .Where(x => x.IsDefault)
                                .Cast<TrackingCore>()
                                .ToList();
                            await redisLogService.Save(
                                bannerViewTrackedItems,
                                arrMoney,
                                ReportMetric.DefaultBannerView,
                                BannerViewLogConfig);
                            
                            var backupViewTrackedItems = trackedItems
                                .Where(x => x.IsBackup)
                                .Cast<TrackingCore>()
                                .ToList();
                            await redisLogService.Save(
                                backupViewTrackedItems,
                                arrMoney,
                                ReportMetric.BackupView,
                                BannerViewLogConfig);
                        }

                        //Remove Flag Key
                        await redisQueueService.KeyDeleteAsync(queuedListKey);
                    }
                }
            }

            stopwatch.Stop();
            _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(AdvertisingLogFlagService),
                stopwatch.ElapsedMilliseconds.ToString());
            _isProcessing = false;
        }
        catch (Exception e)
        {
            stopwatch.Stop();
            _logger.LogError("{Time} Error: {Error}", DateTimeOffset.Now, e.Message);
            _isProcessing = false;
        }
    }

    public static LogConfig CoreConfig => new()
    {
        EAdverForSingleObjLog = new List<ReportAdvertiserDimension>
        {
            ReportAdvertiserDimension.Advertising,
            ReportAdvertiserDimension.AdvertisingSet,
            ReportAdvertiserDimension.Campaign,
            ReportAdvertiserDimension.Advertiser,
            ReportAdvertiserDimension.Product,
            ReportAdvertiserDimension.ProductFeed,
            ReportAdvertiserDimension.ProductGroup,
            ReportAdvertiserDimension.AdvertisingSetProductGroup,
            ReportAdvertiserDimension.AdvertisingTemplate
        },
        EAdverForMultipleObjLog = new List<List<ReportAdvertiserDimension>>
        {
            new()
            {
                ReportAdvertiserDimension.Advertising,
                ReportAdvertiserDimension.AdvertisingTemplate
            },
            new()
            {
                ReportAdvertiserDimension.Campaign,
                ReportAdvertiserDimension.Product
            }
        },
        EPubForSingleObjLog = new List<ReportPublisherDimension>
        {
            ReportPublisherDimension.Zone,
            ReportPublisherDimension.Website,
            ReportPublisherDimension.Publisher
        },
        EClientSingleObjLog = new List<ReportClientDimension>
        {
            ReportClientDimension.ClientId
        },

        EAdverForAdverTargeting = new List<ReportAdvertiserDimension>
            {ReportAdvertiserDimension.Campaign},
        ETargetForAdverTargeting = new List<ReportTargetingDimension>
        {
            ReportTargetingDimension.Location
        },

        EAdverForAdverPub = new List<ReportAdvertiserDimension>
            {ReportAdvertiserDimension.Campaign, ReportAdvertiserDimension.AdvertisingSet},
        EPubForAdverPub = new List<ReportPublisherDimension>
            {ReportPublisherDimension.Website},

        EPubForPubTargeting = new List<ReportPublisherDimension>
        {
            ReportPublisherDimension.Website,
            ReportPublisherDimension.Zone,
            ReportPublisherDimension.Publisher
        },
        IsLogAdverMoney =
            true, // TODO: DONE # Hòa NX - check thuộc tính SellPriceType == CPC = true -> log tiền cho click vào adver
        IsLogPubMoney =
            true // TODO: DONE # Hòa NX - check thuộc tính SellPriceType == CPC = true -> log tiền cho click vào adver
    };

    public static LogConfig BannerClickLogConfig => new()
    {
        EAdverForSingleObjLog = new List<ReportAdvertiserDimension>
        {
            ReportAdvertiserDimension.Advertising
        },
        EPubForSingleObjLog = new List<ReportPublisherDimension>(),
        EClientSingleObjLog = new List<ReportClientDimension>(),
        EAdverForAdverTargeting = new List<ReportAdvertiserDimension>(),
        EAdverForAdverPub = new List<ReportAdvertiserDimension>(),
        EPubForAdverPub = new List<ReportPublisherDimension>(),
        EPubForPubTargeting = new List<ReportPublisherDimension>(),
        IsLogAdverMoney = false,
        IsLogPubMoney = false
    };

    public static LogConfig BannerViewLogConfig => new()
    {
        EAdverForSingleObjLog = new List<ReportAdvertiserDimension>(),
        EPubForSingleObjLog = new List<ReportPublisherDimension>
        {
            ReportPublisherDimension.Zone,
        },
        EClientSingleObjLog = new List<ReportClientDimension>(),
        EAdverForAdverTargeting = new List<ReportAdvertiserDimension>(),
        EAdverForAdverPub = new List<ReportAdvertiserDimension>(),
        EPubForAdverPub = new List<ReportPublisherDimension>(),
        EPubForPubTargeting = new List<ReportPublisherDimension>(),
        IsLogAdverMoney = false,
        IsLogPubMoney = false
    };

    public static LogConfig ConversionConfig => new()
    {
        EAdverForSingleObjLog = new List<ReportAdvertiserDimension>
        {
            ReportAdvertiserDimension.Advertising,
            ReportAdvertiserDimension.AdvertisingSet,
            ReportAdvertiserDimension.Campaign,
            ReportAdvertiserDimension.Advertiser,
            ReportAdvertiserDimension.Product,
            ReportAdvertiserDimension.ProductFeed,
            ReportAdvertiserDimension.ProductGroup,
            ReportAdvertiserDimension.AdvertisingSetProductGroup,
            ReportAdvertiserDimension.AdvertisingTemplate
        },
        EAdverForMultipleObjLog = new List<List<ReportAdvertiserDimension>>
        {
            new()
            {
                ReportAdvertiserDimension.Advertising,
                ReportAdvertiserDimension.AdvertisingTemplate
            },
            new()
            {
                ReportAdvertiserDimension.Campaign,
                ReportAdvertiserDimension.Product
            }
        },
        EPubForSingleObjLog = new List<ReportPublisherDimension>
        {
            ReportPublisherDimension.Zone,
            ReportPublisherDimension.Website,
            ReportPublisherDimension.Publisher
        },
        EClientSingleObjLog = new List<ReportClientDimension>
        {
            ReportClientDimension.ClientId
        },
        EAdverForAdverTargeting = new List<ReportAdvertiserDimension>
            {ReportAdvertiserDimension.Campaign},
        ETargetForAdverTargeting = new List<ReportTargetingDimension>
        {
            ReportTargetingDimension.Location
        },
        EAdverForAdverPub = new List<ReportAdvertiserDimension>
            {ReportAdvertiserDimension.Campaign, ReportAdvertiserDimension.AdvertisingSet},
        EPubForAdverPub = new List<ReportPublisherDimension>
            {ReportPublisherDimension.Website},
        EPubForPubTargeting = new List<ReportPublisherDimension>(),
        IsLogAdverMoney = false,
        IsLogPubMoney = false
    };
    
    public static LogConfig VideoViewConfig => new()
    {
        EAdverForSingleObjLog = new List<ReportAdvertiserDimension>
        {
            ReportAdvertiserDimension.Advertising,
            ReportAdvertiserDimension.AdvertisingSet,
            ReportAdvertiserDimension.Campaign,
            ReportAdvertiserDimension.Advertiser,
            ReportAdvertiserDimension.Product,
            ReportAdvertiserDimension.ProductFeed,
            ReportAdvertiserDimension.ProductGroup,
            ReportAdvertiserDimension.AdvertisingSetProductGroup,
            ReportAdvertiserDimension.AdvertisingTemplate
        },
        EAdverForMultipleObjLog = new List<List<ReportAdvertiserDimension>>
        {
            new()
            {
                ReportAdvertiserDimension.Advertising,
                ReportAdvertiserDimension.AdvertisingTemplate
            },
            new()
            {
                ReportAdvertiserDimension.Campaign,
                ReportAdvertiserDimension.Product
            }
        },
        EPubForSingleObjLog = new List<ReportPublisherDimension>
        {
            ReportPublisherDimension.Zone,
            ReportPublisherDimension.Website,
            ReportPublisherDimension.Publisher
        },
        EClientSingleObjLog = new List<ReportClientDimension>
        {
            ReportClientDimension.ClientId
        },
        EAdverForAdverTargeting = new List<ReportAdvertiserDimension>
            {ReportAdvertiserDimension.Campaign},
        ETargetForAdverTargeting = new List<ReportTargetingDimension>
        {
            ReportTargetingDimension.Location
        },
        EAdverForAdverPub = new List<ReportAdvertiserDimension>
            {ReportAdvertiserDimension.Campaign, ReportAdvertiserDimension.AdvertisingSet},
        EPubForAdverPub = new List<ReportPublisherDimension>
            {ReportPublisherDimension.Website},
        EPubForPubTargeting = new List<ReportPublisherDimension>(),
        IsLogAdverMoney = false,
        IsLogPubMoney = false
    };
}