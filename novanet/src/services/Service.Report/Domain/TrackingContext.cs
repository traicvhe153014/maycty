﻿using AioCore.Mongo.Driver;
using AioCore.Mongo.Driver.Abstracts;
using Service.Report.Domain.AggregateModels.TrackingAggregate;

namespace Service.Report.Domain;

public class TrackingContext : MongoContext
{
    public TrackingContext(IMongoContextBuilder builder) : base(builder)
    {
    }

    public MongoSet<TrackingWeekPartition> TrackingWeekPartitions { get; set; } = default!;
    
    public MongoSet<TrackingMonthPartition> TrackingMonthPartitions { get; set; } = default!;
    
    public MongoSet<TrackingPersistentPartition> TrackingPersistentPartitions { get; set; } = default!;

}