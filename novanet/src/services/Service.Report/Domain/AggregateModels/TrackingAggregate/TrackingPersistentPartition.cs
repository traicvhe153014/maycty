﻿using Novanet.Core.Mapper;
using NovanetCore.Business.BusinessDomain.Service.Display;

namespace Service.Report.Domain.AggregateModels.TrackingAggregate;

public class TrackingPersistentPartition : TrackingCore, IMapFrom<TrackingMonthPartition>
{
}