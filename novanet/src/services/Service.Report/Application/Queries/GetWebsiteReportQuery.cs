﻿using System.Linq.Expressions;
using FluentValidation;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessDomain.Service.Settings;

namespace Service.Report.Application.Queries;

public class GetWebsiteReportQuery : INovanetRequest<WebsiteReportResponse>
{
    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }
    
    public string? WebsiteIds { get; set; }

    internal class Handler : NovanetRequestHandler<GetWebsiteReportQuery, WebsiteReportResponse>
    {
        private readonly IReportService _reportService;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor, IReportService reportService) :
            base(logger, httpContextAccessor)
        {
            _reportService = reportService;;
        }

        protected override async Task<WebsiteReportResponse> HandleAsync(GetWebsiteReportQuery request,
            CancellationToken cancellationToken)
        {
            var isAdmin = CanViewAll();
            Expression<Func<KeyValuePair<long, WebsiteCore>, bool>> whereExpression = x => isAdmin || x.Value.PublisherId == UserClaimsValue.Id;
            if (!string.IsNullOrWhiteSpace(request.WebsiteIds))
            {
                var requestWebsiteIds = request.WebsiteIds.Split(',').Select(x => x.ToGuid()).ToList();
                whereExpression = whereExpression.And(x => requestWebsiteIds.Contains(x.Value.Id));
            }
            var websiteIds = CacheManager.Websites
                .Where(whereExpression.Compile())
                .Select(x => (object) x.Key)
                .ToList();
            var hasDateRange = HasDateRange(request);

            var clickReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Click,
                    ReportPublisherDimension.Website,
                    websiteIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.Click,
                    ReportPublisherDimension.Website,
                    websiteIds);
            var viewReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.View,
                    ReportPublisherDimension.Website,
                    websiteIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.View,
                    ReportPublisherDimension.Website,
                    websiteIds);
            var earnedReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Earned,
                    ReportPublisherDimension.Website,
                    websiteIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.Earned,
                    ReportPublisherDimension.Website,
                    websiteIds);

            var report = websiteIds.Select(websiteId =>
            {
                var websiteSubId = (long) websiteId;
                CacheManager.Websites.TryGetValue(websiteSubId, out var website);
                var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == website?.PublisherId);

                var item = new WebsiteReport
                {
                    Id = website?.Id,
                    DomainName = website?.Domain,
                    Url = website?.Url,
                    PublisherEmail = publisher?.Email,
                    Clicks = clickReports.TryGetValue(websiteSubId, out var clickValue) ? clickValue : default,
                    Views = viewReports.TryGetValue(websiteSubId, out var viewValue) ? viewValue : default,
                    Earned = earnedReports.TryGetValue(websiteSubId, out var earnedValue) ? earnedValue : default,
                };
                item.Ctr = item.Views != 0 ? item.Clicks / item.Views : 0;
                return item;
            }).ToList();

            var summary = new WebsiteReport
            {
                Clicks = report.Aggregate(0d, (current, item) => current + item.Clicks),
                Earned = report.Aggregate(0d, (current, item) => current + item.Earned),
                Views = report.Aggregate(0d, (current, item) => current + item.Views),
            };
            summary.Ctr = summary.Views != 0 ? summary.Clicks / summary.Views : 0;

            return new WebsiteReportResponse
            {
                Data = report,
                Summary = summary
            };
        }

        private bool CanViewAll()
        {
            return UserClaimsValue.Roles.Contains("Admin") || UserClaimsValue.Roles.Contains("PublisherManager");
        }
        
        private static bool HasDateRange(GetWebsiteReportQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
    }
}

public class GetWebsiteReportQueryValidator : AbstractValidator<GetWebsiteReportQuery>
{
    public GetWebsiteReportQueryValidator()
    {
        RuleFor(x => x.EndDate)
            .GreaterThan(x => x.StartDate);
    }
}