﻿using System.Linq.Expressions;
using FluentValidation;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Specifications;

namespace Service.Report.Application.Queries;

public class GetPublisherReportQuery : INovanetRequest<PublisherReportResponse>
{
    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }
    
    public string? PublisherIds { get; set; }

    internal class Handler : NovanetRequestHandler<GetPublisherReportQuery, PublisherReportResponse>
    {
        private readonly IReportService _reportService;
        private readonly IGlobalCacheService _globalCacheService;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor, IReportService reportService, IGlobalCacheService globalCacheService) :
            base(logger, httpContextAccessor)
        {
            _reportService = reportService;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<PublisherReportResponse> HandleAsync(GetPublisherReportQuery request,
            CancellationToken cancellationToken)
        {
            var users = CacheManager.IdentityUsers.Values.ToList();
            Expression<Func<IdentityUserValue, bool>> whereExpression = x => x.Roles != null && x.Roles.All(role => role == "Publisher") == true;
            if (!string.IsNullOrWhiteSpace(request.PublisherIds))
            {
                var requestPublisherIds = request.PublisherIds.Split(',').Select(x => x.ToGuid()).ToList();
                whereExpression = whereExpression.And(x => requestPublisherIds.Contains(x.Id));
            }
            var publishers = users
                .DistinctBy(x => x.Id)
                .Where(whereExpression.Compile())
                .ToList();
          
            var publisherIds = publishers.Select(x => x.SubId).Cast<object>().ToList();
            var hasDateRange = HasDateRange(request);
            var clickReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Click,
                    ReportPublisherDimension.Publisher,
                    publisherIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.Click,
                    ReportPublisherDimension.Publisher,
                    publisherIds);
            var viewReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.View,
                    ReportPublisherDimension.Publisher,
                    publisherIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.View,
                    ReportPublisherDimension.Publisher,
                    publisherIds);
            var earnedReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Earned,
                    ReportPublisherDimension.Publisher,
                    publisherIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.Earned,
                    ReportPublisherDimension.Publisher,
                    publisherIds);

            var report = publishers.Select(x =>
            {
                var item = new PublisherReport
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    UserName = x.UserName,
                    Email = x.Email,
                    Clicks = clickReports.TryGetValue(x.SubId, out var clickValue) ? clickValue : default,
                    Views = viewReports.TryGetValue(x.SubId, out var viewValue) ? viewValue : default,
                    Earned = earnedReports.TryGetValue(x.SubId, out var earnedValue) ? earnedValue : default,
                };
                item.Ctr = item.Views != 0 ? item.Clicks / item.Views : 0;
                return item;
            }).ToList();

            var summary = new PublisherReport
            {
                Clicks = report.Aggregate(0d, (current, item) => current + item.Clicks),
                Earned = report.Aggregate(0d, (current, item) => current + item.Earned),
                Views = report.Aggregate(0d, (current, item) => current + item.Views),
            };
            summary.Ctr = summary.Views != 0 ? summary.Clicks / summary.Views : 0;

            return new PublisherReportResponse
            {
                Data = report,
                Summary = summary
            };
        }
        
        private static bool HasDateRange(GetPublisherReportQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
    }
}

public class GetPublisherReportQueryValidator : AbstractValidator<GetPublisherReportQuery>
{
    public GetPublisherReportQueryValidator()
    {
        RuleFor(x => x.EndDate)
            .GreaterThan(x => x.StartDate);
    }
}