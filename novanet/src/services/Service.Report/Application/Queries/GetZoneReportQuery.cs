﻿using System.Linq.Expressions;
using FluentValidation;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessDomain.Service.Settings;

namespace Service.Report.Application.Queries;

public class GetZoneReportQuery : INovanetRequest<ZoneReportResponse>
{
    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }
    
    public string? ZoneIds { get; set; }
    
    public int pageSize { get; set; }
    
    public int page { get; set; }

    internal class Handler : NovanetRequestHandler<GetZoneReportQuery, ZoneReportResponse>
    {
        private readonly IReportService _reportService;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor, IReportService reportService) :
            base(logger, httpContextAccessor)
        {
            _reportService = reportService;;
        }

        protected override async Task<ZoneReportResponse> HandleAsync(GetZoneReportQuery request,
            CancellationToken cancellationToken)
        {
            var isAdmin = CanViewAll();
            Expression<Func<ZoneWebsite, bool>> whereExpression =
                x => isAdmin || x.Website != null && x.Website.PublisherId == UserClaimsValue.Id;
            if (!string.IsNullOrWhiteSpace(request.ZoneIds))
            {
                var requestZoneIds = request.ZoneIds.Split(',').Select(x => x.ToGuid()).ToList();
                whereExpression = whereExpression.And(x => requestZoneIds.Contains(x.Zone.Value.Id));
            }
            if (request.page == 0 || request.pageSize == 0)
            {
                request.page = 1;
                request.pageSize = 24;
            }

            var zones = CacheManager.Zones
                .Select(x => new ZoneWebsite
                {
                    Zone = x,
                    Website = CacheManager.Websites.Values.FirstOrDefault(website => website.Id == x.Value.WebsiteId)
                })
                .Where(whereExpression.Compile())
                .ToList();
            var zoneIds = zones.Select(x => (object) x.Zone.Key).ToList();
            var hasDateRange = HasDateRange(request);
            var clickReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Click,
                    ReportPublisherDimension.Zone,
                    zoneIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.Click,
                    ReportPublisherDimension.Zone,
                    zoneIds);
            var viewReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.View,
                    ReportPublisherDimension.Zone,
                    zoneIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.View,
                    ReportPublisherDimension.Zone,
                    zoneIds);
            var earnedReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Earned,
                    ReportPublisherDimension.Zone,
                    zoneIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.Earned,
                    ReportPublisherDimension.Zone,
                    zoneIds);

            var defaultBannerViewReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.DefaultBannerView,
                    ReportPublisherDimension.Zone,
                    zoneIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.DefaultBannerView,
                    ReportPublisherDimension.Zone,
                    zoneIds);

            var backupViewReports = hasDateRange
                ? await _reportService.GetPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.BackupView,
                    ReportPublisherDimension.Zone,
                    zoneIds)
                : await _reportService.GetTotalPublisherReport(
                    ReportMetric.BackupView,
                    ReportPublisherDimension.Zone,
                    zoneIds);

            var report = zones.Select(zoneItem =>
            {
                var zone = zoneItem.Zone.Value;
                var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == zoneItem.Website?.PublisherId);
                var templateTypes = zone.ZoneTemplateCores
                    .Select(x =>
                        CacheManager.Templates.Values.FirstOrDefault(template =>
                            template.Id == x.AdvertisingTemplateId))
                    .Select(x => x?.TemplateType ?? default)
                    .ToList();
                var item = new ZoneReport
                {
                    Id = zone.Id,
                    ZoneName = zone.Name,
                    DomainName = zoneItem.Website?.Domain ?? string.Empty,
                    TemplateTypes = templateTypes,
                    Url = zoneItem.Website?.Url ?? string.Empty,
                    PublisherEmail = publisher?.Email,
                    Clicks = clickReports.TryGetValue(zone.SubId, out var clickValue) ? clickValue : default,
                    Views = viewReports.TryGetValue(zone.SubId, out var viewValue) ? viewValue : default,
                    Earned = earnedReports.TryGetValue(zone.SubId, out var earnedValue) ? earnedValue : default,
                    DefaultBannerViews = defaultBannerViewReports.TryGetValue(zone.SubId, out var defaultBannerView) ? defaultBannerView : default,
                    BackupViews = backupViewReports.TryGetValue(zone.SubId, out var backupView) ? backupView : default,
                };
                item.Ctr = item.Views != 0 ? item.Clicks / item.Views : 0;
                return item;
            }).ToList();
            var pagedReport = report.Skip(request.pageSize * (request.page - 1))
                .Take(request.pageSize).ToList();

            var summary = new ZoneReport
            {
                Clicks = report.Aggregate(0d, (current, item) => current + item.Clicks),
                Earned = report.Aggregate(0d, (current, item) => current + item.Earned),
                Views = report.Aggregate(0d, (current, item) => current + item.Views),
                DefaultBannerViews = report.Aggregate(0d, (current, item) => current + item.DefaultBannerViews),
                BackupViews = report.Aggregate(0d, (current, item) => current + item.BackupViews),
                Total = report.Count
            };
            summary.Ctr = summary.Views != 0 ? summary.Clicks / summary.Views : 0;

            return new ZoneReportResponse
            {
                Data = pagedReport,
                Summary = summary
            };
        }
        
        private static bool HasDateRange(GetZoneReportQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
        
        private bool CanViewAll()
        {
            return UserClaimsValue.Roles.Contains("Admin") || UserClaimsValue.Roles.Contains("PublisherManager");
        }
    }

    public class ZoneWebsite
    {
        public KeyValuePair<long, ZoneCore> Zone { get; set; }
        public WebsiteCore? Website { get; set; }
    }
}

public class GetZoneReportQueryValidator : AbstractValidator<GetZoneReportQuery>
{
    public GetZoneReportQueryValidator()
    {
        RuleFor(x => x.EndDate)
            .GreaterThan(x => x.StartDate);
    }
}