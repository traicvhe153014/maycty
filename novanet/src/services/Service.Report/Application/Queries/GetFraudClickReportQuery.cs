﻿using System.Text;
using FluentValidation;
using Novanet.Core.Enums;
using Novanet.Core.Enums.ReportKeyObjects;

namespace Service.Report.Application.Queries;

public class GetFraudClickReportQuery : INovanetRequest<FraudClickReportResponse>
{
    public DateTime? From { get; set; }

    public DateTime? To { get; set; }

    internal class Handler : NovanetRequestHandler<GetFraudClickReportQuery, FraudClickReportResponse>
    {
        private readonly IRedisCacheService _redisCacheService;

        public Handler(ILogger<Handler> logger,
            IRedisCacheService redisCacheService,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _redisCacheService = redisCacheService;
        }

        protected override async Task<FraudClickReportResponse> HandleAsync(GetFraudClickReportQuery request,
            CancellationToken cancellationToken)
        {
            var fromDate = request.From?.Date ?? DateTime.Today;
            var toDate = request.To?.Date.AddDays(1).AddMilliseconds(-1) ??
                         DateTime.Today.AddDays(1).AddMilliseconds(-1);

            var from = new DateTimeOffset(fromDate).GetDayOfYear();
            var to = new DateTimeOffset(toDate).GetDayOfYear();

            var websiteIds = CacheManager.Websites.Keys;
            var fraudClickTypes = Enum.GetValues(typeof(FraudClickTypeEnum)).Cast<FraudClickTypeEnum>()
                .Select(x => $"'{x}'")
                .ToList();
            var joinedFraudClickTypes = string.Join(", ", fraudClickTypes);

            var script = new StringBuilder();

            script.AppendLine("local table = {};");
            script.AppendLine($"local falseClickTypes = {{{joinedFraudClickTypes}}};");
            script.AppendLine($"local websiteIds = {{{string.Join(", ", websiteIds.ToArray())}}};");
            script.AppendLine($"for i=1,{websiteIds.Count} do");
            script.AppendLine($"for j={from},{to} do");
            script.AppendLine("local websiteId = tostring(websiteIds[i])");
            script.AppendLine("if table[websiteId] == nil then table[websiteId] = {} end");
            script.AppendLine(
                $"local trueClick = redis.call('hget', '{ReportPublisherDimension.Website}_{ReportMetric.Click}_' .. websiteId, j) or 0;");
            script.AppendLine("table[websiteId]['TrueClick'] = (table[websiteId]['TrueClick'] or 0) + trueClick;");
            script.AppendLine($"for fcIter=1,{fraudClickTypes.Count} do");
            script.AppendLine("local falseClickKey = falseClickTypes[fcIter];");
            script.AppendLine(
                $"local falseClick = redis.call('hget', 'FRAUD_' .. falseClickKey .. '_{ReportPublisherDimension.Website}_' .. websiteId, j) or 0;");
            script.AppendLine("table[websiteId][falseClickKey] = (table[websiteId][falseClickKey] or 0) + falseClick;");
            script.AppendLine("end");
            script.AppendLine("end");
            script.AppendLine("end");
            script.AppendLine("return cjson.encode(table);");

            var scriptString = script.ToString();

            var executeResult =
                await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
            var result = executeResult?.Deserialize<Dictionary<long, Dictionary<string, long>>>() ??
                         new Dictionary<long, Dictionary<string, long>>();
            var report = result.Select(x => new FraudClickReport
            {
                WebsiteId = x.Key,
                Domain = CacheManager.Websites.GetValueOrDefault(x.Key)?.Domain ?? "",
                AllClick = x.Value.Aggregate(0L, (current, item) => current + item.Value),
                TrueClick = x.Value.GetValueOrDefault("TrueClick"),
                FalseClick = GetFalseClick(x.Value),
                ClickRate = x.Value.GetValueOrDefault(FraudClickTypeEnum.ClientIdClickRates.ToString()),
                ClientCreate = x.Value.GetValueOrDefault(FraudClickTypeEnum.CreateTimeClientId.ToString()),
                DoubleClick = x.Value.GetValueOrDefault(FraudClickTypeEnum.DoubleClick.ToString()),
                MaxCtr = x.Value.GetValueOrDefault(FraudClickTypeEnum.MaxCtr.ToString()),
                RealTime = x.Value.GetValueOrDefault(FraudClickTypeEnum.RealTime.ToString()),
                Ip = x.Value.GetValueOrDefault(FraudClickTypeEnum.IP.ToString()),
                CountryNotConfig = x.Value.GetValueOrDefault(FraudClickTypeEnum.CountryNotConfig.ToString()),
                PercentCutNovanet = x.Value.GetValueOrDefault(FraudClickTypeEnum.PercentCutNovanet.ToString()),
                OutOfTimeSpan = x.Value.GetValueOrDefault(FraudClickTypeEnum.OutOfTimeSpan.ToString()),
                VnProvinceNotConfig = x.Value.GetValueOrDefault(FraudClickTypeEnum.VNProvinceNotConfig.ToString()),
            }).ToList();

            var summary = new FraudClickReport
            {
                AllClick = report.Aggregate(0L, (current, item) => current + item.AllClick),
                TrueClick = report.Aggregate(0L, (current, item) => current + item.TrueClick),
                FalseClick = report.Aggregate(0L, (current, item) => current + item.FalseClick),
                ClickRate = report.Aggregate(0L, (current, item) => current + item.ClickRate),
                ClientCreate = report.Aggregate(0L, (current, item) => current + item.ClientCreate),
                DoubleClick = report.Aggregate(0L, (current, item) => current + item.DoubleClick),
                MaxCtr = report.Aggregate(0L, (current, item) => current + item.MaxCtr),
                RealTime = report.Aggregate(0L, (current, item) => current + item.RealTime),
                Ip = report.Aggregate(0L, (current, item) => current + item.Ip),
                CountryNotConfig = report.Aggregate(0L, (current, item) => current + item.CountryNotConfig),
                PercentCutNovanet = report.Aggregate(0L, (current, item) => current + item.PercentCutNovanet),
                OutOfTimeSpan = report.Aggregate(0L, (current, item) => current + item.OutOfTimeSpan),
                VnProvinceNotConfig = report.Aggregate(0L, (current, item) => current + item.VnProvinceNotConfig),
            };
            return new FraudClickReportResponse
            {
                Data = report,
                Summary = summary
            };
        }
    }

    private static long GetFalseClick(Dictionary<string, long> data)
    {
        return data.Aggregate(0L,
            (result, item) => item.Key != "TrueClick" && item.Key != FraudClickTypeEnum.RealTime.ToString()
                ? result + item.Value
                : result);
    }
}

public class GetFraudClickReportQueryValidator : AbstractValidator<GetFraudClickReportQuery>
{
    public GetFraudClickReportQueryValidator()
    {
        RuleFor(x => x.To)
            .GreaterThan(x => x.From);
    }
}