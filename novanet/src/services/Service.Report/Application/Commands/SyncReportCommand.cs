﻿using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.EventBus;
using Service.Report.Events;

namespace Service.Report.Application.Commands;

public class SyncReportCommand : INovanetRequest<string>
{
    public SyncReportAction Action { get; set; }

    public SyncReportType ReportType { get; set; }
    
    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public List<ReportAdvertiserDimension>? AdvertiserDimensions { get; set; }
    
    public List<ReportPublisherDimension>? PublisherDimensions { get; set; }
    
    public List<ReportClientDimension>? ClientDimensions { get; set; }

    public GroupDimensions? AdvertiserPublisherDimensions { get; set; }
    
    public GroupDimensions? AdvertiserTargetingDimensions { get; set; }

    public List<List<ReportAdvertiserDimension>>? MultipleAdvertiserDimensions { get; set; }

    internal class Handler : NovanetRequestHandler<SyncReportCommand, string>
    {
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<NovanetRequestHandler<SyncReportCommand, string>> logger,
            IHttpContextAccessor httpContextAccessor, IMessageBusClient messageBusClient) : base(logger,
            httpContextAccessor)
        {
            _messageBusClient = messageBusClient;
        }

        protected override async Task<string> HandleAsync(SyncReportCommand request, CancellationToken cancellationToken)
        {
            switch (request.Action)
            {
                case SyncReportAction.Cancel:
                    SyncReportService.IsProcessing = false;
                    return "Cancel success";
                case SyncReportAction.Status:
                    return SyncReportService.IsProcessing ? "Job running" : "Not running";
                case SyncReportAction.Sync:
                    _messageBusClient.Publish(MessageBusChannels.SyncReport, request);
                    return "Sync start";
                default:
                    return "Invalid operation";
            }
        }
    }
}

public class GroupDimensions
{
    public List<ReportAdvertiserDimension>? Advertisers { get; set; }
    public List<ReportPublisherDimension>? Publishers { get; set; }
    public List<ReportTargetingDimension>? Targeting { get; set; }
}

public enum SyncReportAction
{
    Sync = 1,
    Cancel = 2,
    Status = 3
}

public enum SyncReportType
{
    All = 1,
    Advertiser,
    Publisher,
    AdvertiserTargeting,
    AdvertiserPublisher,
    MultipleAdvertiser,
    Client
}