﻿using Novanet.EventBus;
using Service.Report.Events;

namespace Service.Report.Application.Commands;

public class SyncTrackingLocationCommand : INovanetRequest<string>
{
    internal class Handler : NovanetRequestHandler<SyncTrackingLocationCommand, string>
    {
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<NovanetRequestHandler<SyncTrackingLocationCommand, string>> logger,
            IHttpContextAccessor httpContextAccessor, IMessageBusClient messageBusClient) : base(logger,
            httpContextAccessor)
        {
            _messageBusClient = messageBusClient;
        }

        protected override async Task<string> HandleAsync(SyncTrackingLocationCommand request, CancellationToken cancellationToken)
        {
            if (SyncTrackingLocationService.IsProcessing)
            {
                return "Job is already running";
            }
            _messageBusClient.Publish(MessageBusChannels.SyncTrackingLocation, new object());
            return "Job start";
        }
    }
}