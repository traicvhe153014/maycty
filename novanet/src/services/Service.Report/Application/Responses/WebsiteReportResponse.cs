﻿namespace Service.Report.Application.Responses;

public class WebsiteReportResponse
{
    public List<WebsiteReport> Data { get; set; } = default!;
    public WebsiteReport Summary { get; set; } = default!;
}

public class WebsiteReport
{
    public Guid? Id { get; set; }
    public string? DomainName { get; set; }
    public string? Url { get; set; }
    public string? PublisherEmail { get; set; }
    public double Clicks { get; set; }
    public double Views { get; set; }
    public double Ctr { get; set; }
    public double Earned { get; set; }
}