﻿using NovanetCore.Business.BusinessDomain.Service.Settings;

namespace Service.Report.Application.Responses;

public class ZoneReportResponse
{
    public List<ZoneReport> Data { get; set; } = default!;
    public ZoneReport Summary { get; set; } = default!;
}

public class ZoneReport
{
    public Guid? Id { get; set; }
    public string? ZoneName { get; set; }
    public List<TemplateType>? TemplateTypes { get; set; }
    public string? DomainName { get; set; }
    public string? Url { get; set; }
    public string? PublisherEmail { get; set; }
    public double Clicks { get; set; }
    public double Views { get; set; }
    public double Ctr { get; set; }
    public double Earned { get; set; }
    public double DefaultBannerViews { get; set; }
    public double BackupViews { get; set; }
    public int Total { get; set; }
}