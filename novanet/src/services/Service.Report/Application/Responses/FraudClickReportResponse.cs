﻿namespace Service.Report.Application.Responses;

public class FraudClickReportResponse
{
    public List<FraudClickReport> Data { get; set; } = new();
    public FraudClickReport Summary { get; set; } = default!;
}

public class FraudClickReport
{
    public long WebsiteId { get; set; }
    public string? Domain { get; set; }
    public long AllClick { get; set; }
    public long TrueClick { get; set; }
    public long FalseClick { get; set; }
    public long RealTime { get; set; }
    public long ClickRate { get; set; }
    public long Ip { get; set; }
    public long DoubleClick { get; set; }
    public long ClientCreate { get; set; }
    public long OutOfTimeSpan { get; set; }
    public long VnProvinceNotConfig { get; set; }
    public long MaxCtr { get; set; }
    public long CountryNotConfig { get; set; }
    public long PercentCutNovanet { get; set; }
} 