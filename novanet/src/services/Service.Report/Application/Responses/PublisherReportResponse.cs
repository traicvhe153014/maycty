﻿namespace Service.Report.Application.Responses;

public class PublisherReportResponse
{
    public List<PublisherReport> Data { get; set; } = default!;
    public PublisherReport Summary { get; set; } = default!;
}

public class PublisherReport
{
    public Guid? Id { get; set; }
    public string? Email { get; set; }
    public string? UserName { get; set; }
    public string? FullName { get; set; }
    public double Clicks { get; set; }
    public double Views { get; set; }
    public double Ctr { get; set; }
    public double Earned { get; set; }
}