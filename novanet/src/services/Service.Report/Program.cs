using AioCore.Mongo.Driver;
using Novanet.EventBus;
using NovanetCore.Business;
using NovanetCore.Business.BusinessParallels;
using Service.Report.Domain;
using Service.Report.Events;

WebApplication.CreateBuilder(args).NovanetCore("Service.Report",
    out var app, (service, appSettings) =>
    {
        service.AddMongoContext<TrackingContext>(
            appSettings.MongoShortTime.Url,
            appSettings.MongoShortTime.Database);
        service.AddSingleton(appSettings);
        service.AddBusinessCore();
        service.AddRabbitMQ(appSettings.RabbitMQ);
        service.AddCacheManagerServices();
        service.AddScoped<IReportService, ReportService>();
        service.AddSingleton<RedisLogService>();
        service.AddSingleton<IMessageBusClient, MessageBusClient>();
        service.AddHostedService<AdvertisingLogFlagService>();
        service.AddHostedService<SyncReportService>();
        service.AddHostedService<SyncWebsitePublisherReportService>();
        service.AddHostedService<BatchSyncWebsitePublisherReportService>();
        service.AddHostedService<SyncTrackingLocationService>();
    }, application =>
    {
        application.MapGet("/", () => $"Report - Hello World!");
        var publisher = application.Services.GetRequiredService<Publisher>();
        publisher.Publish(new UpdateCacheManagerParallel(), PublishStrategy.ParallelNoWait);
    });
app.Run();