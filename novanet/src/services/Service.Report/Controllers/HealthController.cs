using Microsoft.AspNetCore.Mvc;

namespace Service.Report.Controllers;

[ApiController]
[Route("report/health")]
public class HealthController : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> Health()
    {
        return Ok(await Task.FromResult("OK"));
    }
}