﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Controllers;
using Novanet.Core.Authorize;
using Novanet.Core.Constants;
using Service.Report.Application.Commands;

namespace Service.Report.Controllers;

[Route("report/api/v1/[action]")]
public class ReportController : NovanetController
{
    private readonly ILogger<ReportController> _logger;
    private readonly IMediator _mediator;

    public ReportController(ILogger<ReportController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Report, nameof(FraudClick))]
    public async Task<IActionResult> FraudClick([FromQuery] GetFraudClickReportQuery query)
    {
        return Ok(await _mediator.Send(query));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.Report, nameof(Website))]
    public async Task<IActionResult> Website([FromQuery] GetWebsiteReportQuery query)
    {
        return Ok(await _mediator.Send(query));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.Report, nameof(Publisher))]
    public async Task<IActionResult> Publisher([FromQuery] GetPublisherReportQuery query)
    {
        return Ok(await _mediator.Send(query));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.Report, nameof(Zone))]
    public async Task<IActionResult> Zone([FromQuery] GetZoneReportQuery query)
    {
        return Ok(await _mediator.Send(query));
    }
    
    [HttpPost]
    [AllowAnonymous]
    // [NovanetAccessControl(Aggregates.Report, nameof(Sync))]
    public async Task<IActionResult> Sync(SyncReportCommand command)
    {
        return Ok(await _mediator.Send(command));
    }
    
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> SyncLocation(SyncTrackingLocationCommand command)
    {
        return Ok(await _mediator.Send(command));
    }
}