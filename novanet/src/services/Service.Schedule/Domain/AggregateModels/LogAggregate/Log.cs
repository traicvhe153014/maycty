﻿namespace Service.Schedule.Domain.AggregateModels.LogAggregate;

public class Log
{
    public Guid Id { get; set; }

    public string Title { get; set; } = default!;

    public DateTimeOffset ExecutionStart { get; set; }

    public DateTimeOffset ExecutionEnd { get; set; }

    public bool Success { get; set; }

    public TimeSpan TimeSpent { get; set; }

    public string? Message { get; set; }
}