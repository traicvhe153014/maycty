﻿namespace Service.Schedule.Controllers;

public class TasksController : ScheduleController
{
    [HttpGet]
    public IActionResult List()
    {
        return Ok();
    }

    [HttpGet]
    public IActionResult History()
    {
        return Ok();
    }

    [HttpPost]
    public IActionResult Trigger()
    {
        return Ok();
    }

    [HttpPost]
    public IActionResult Clear()
    {
        return Ok();
    }
}