namespace Service.Schedule.Controllers;

[ApiController]
[Route("schedule/health")]
public class HealthController : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> Health()
    {
        return Ok(await Task.FromResult("OK"));
    }
}