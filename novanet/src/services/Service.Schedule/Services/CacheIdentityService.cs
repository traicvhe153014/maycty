﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class CacheIdentityService : ICronJob
{
    private readonly ILogger<CacheIdentityService> _logger;
    private readonly IMessageBusClient _messageBus;

    public CacheIdentityService(
        ILogger<CacheIdentityService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(CacheIdentityService));
        _messageBus.Publish(MessageBusChannels.UpdateServiceIdentityCache, new object());
        return await Task.FromResult($"Published event cache {nameof(CacheIdentityService)}");
    }
}