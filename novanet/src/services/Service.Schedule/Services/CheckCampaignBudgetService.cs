﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class CheckCampaignBudgetService: ICronJob
{
    private readonly ILogger<CheckCampaignBudgetService> _logger;
    private readonly IMessageBusClient _messageBus;
    
    public CheckCampaignBudgetService(
        ILogger<CheckCampaignBudgetService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(CheckCampaignBudgetService));
        _messageBus.Publish(MessageBusChannels.CheckCampaignBudget, new object());
        return await Task.FromResult($"Published event cache {nameof(CheckCampaignBudgetService)}");
    }
}