﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class AdvertisingLogFlagService : ICronJob
{
    private readonly ILogger<AdvertisingLogFlagService> _logger;
    private readonly IMessageBusClient _messageBus;

    public AdvertisingLogFlagService(
        ILogger<AdvertisingLogFlagService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(AdvertisingLogFlagService));
        _messageBus.Publish(MessageBusChannels.AdvertisingLogFlag, new object());
        return await Task.FromResult($"Published event cache {nameof(AdvertisingLogFlagService)}");
    }
}