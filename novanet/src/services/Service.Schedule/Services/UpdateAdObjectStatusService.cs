﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class UpdateAdObjectStatusService: ICronJob
{
    private readonly ILogger<UpdateAdObjectStatusService> _logger;
    private readonly IMessageBusClient _messageBus;
    
    public UpdateAdObjectStatusService(
        ILogger<UpdateAdObjectStatusService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(UpdateAdObjectStatusService));
        _messageBus.Publish(MessageBusChannels.UpdateAdObjectStatus, new object());
        return await Task.FromResult($"Published event cache {nameof(UpdateAdObjectStatusService)}");
    }
}