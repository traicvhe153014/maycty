﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class AdvertisingFraudClickService : ICronJob
{
    private readonly ILogger<AdvertisingFraudClickService> _logger;
    private readonly IMessageBusClient _messageBus;

    public AdvertisingFraudClickService(
        IMessageBusClient messageBus, ILogger<AdvertisingFraudClickService> logger)
    {
        _messageBus = messageBus;
        _logger = logger;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(AdvertisingFraudClickService));
        _messageBus.Publish(MessageBusChannels.AdvertisingFraudClick, new object());
        return await Task.FromResult($"Published event cache {nameof(AdvertisingFraudClickService)}");
    }
}