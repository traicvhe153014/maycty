﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class AdvertisingLogService : ICronJob
{
    private readonly ILogger<AdvertisingLogService> _logger;
    private readonly IMessageBusClient _messageBus;

    public AdvertisingLogService(
        ILogger<AdvertisingLogService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(AdvertisingLogService));
        _messageBus.Publish(MessageBusChannels.AdvertisingLog, new object());
        return await Task.FromResult($"Published event cache {nameof(AdvertisingLogService)}");
    }
}