﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class CacheSettingsService : ICronJob
{
    private readonly ILogger<CacheSettingsService> _logger;
    private readonly IMessageBusClient _messageBus;

    public CacheSettingsService(
        ILogger<CacheSettingsService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(CacheSettingsService));
        _messageBus.Publish(MessageBusChannels.UpdateServiceSettingsCache, new object());
        return await Task.FromResult($"Published event cache {nameof(CacheSettingsService)}");
    }
}