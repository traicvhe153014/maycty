﻿using Hangfire.States;
using NovanetCore.Business.BusinessEvents;

namespace Service.Schedule.Services;

public class InitializeCacheManagerService : IHostedService
{
    private readonly ILogger<InitializeCacheManagerService> _logger;
    private readonly AppSettings _appSettings;
    private readonly IHttpClientFactory _httpClientFactory;
    private bool _isProcessing = true;

    public InitializeCacheManagerService(ILogger<InitializeCacheManagerService> logger, AppSettings appSettings, IHttpClientFactory httpClientFactory)
    {
        _logger = logger;
        _appSettings = appSettings;
        _httpClientFactory = httpClientFactory;
    }

    public async Task StartAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("{Name} running.", nameof(InitializeCacheManagerService));

        _ = Task.Run(async () =>
        {
            var tryCount = 0;
            const int maxTries = 10;

            while (true)
            {
                if (tryCount == maxTries)
                {
                    _isProcessing = false;
                    return;
                }

                if (!_isProcessing)
                {
                    return;
                }

                await DoWork(stoppingToken);
                tryCount++;
                await Task.Delay(1000, stoppingToken);
            }
        }, stoppingToken);
    }

    private async Task DoWork(CancellationToken stoppingToken)
    {
        foreach (var groupedHealthUrl in _appSettings.HealthUrls.GroupBy(x => x.ServiceName))
        {
            if (groupedHealthUrl.Key is "Storage" or "Frontend")
            {
                continue;
            }
            var healthUrl = groupedHealthUrl.FirstOrDefault();
            if (healthUrl is null)
            {
                continue;
            }

            try
            {
                var response = await _httpClientFactory.CreateClient().GetAsync(healthUrl.Url, stoppingToken);
                if (!response.IsSuccessStatusCode)
                {
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        
        // all service are healthy
        RecurringJob.Trigger(nameof(CacheManagerService));
        await StopAsync(stoppingToken);
    }

    public Task StopAsync(CancellationToken stoppingToken)
    {
        _isProcessing = false;
        _logger.LogInformation("{Name} is stopping.", nameof(InitializeCacheManagerService));
        return Task.CompletedTask;
    }
}