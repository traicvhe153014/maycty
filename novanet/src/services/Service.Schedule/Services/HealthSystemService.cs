﻿using System.Net;
using Novanet.Core.Hangfire;
using Novanet.Core.Models;
using Novanet.Core.Services;

namespace Service.Schedule.Services;

public class HealthSystemService : ICronJob
{
    private readonly AppSettings _appSettings;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ILogger<CheckCampaignBudgetService> _logger;
    private readonly IMailService _mailService;

    public HealthSystemService(
        ILogger<CheckCampaignBudgetService> logger,
        AppSettings appSettings,
        IHttpClientFactory httpClientFactory,
        IMailService mailService)
    {
        _logger = logger;
        _appSettings = appSettings;
        _httpClientFactory = httpClientFactory;
        _mailService = mailService;
    }

    public async Task<string> Run()
    {
        return null;
        const int delaySendEmailTime = 30;
        var healthUrls = _appSettings.HealthUrls;
        while (true)
        {
            foreach (var grouping in healthUrls.GroupBy(x => x.ServiceName))
            {
                DateTimeOffset? errorTime = null;
                var statusLinks = new List<ResponseData>();
                _logger.LogInformation("Worker {Worker} running at: {Time}",
                    $"{grouping.Key} Monitor",
                    DateTimeOffset.Now);

                foreach (var healthUrl in grouping)
                {
                    var httpClient = await _httpClientFactory.CreateClient().GetAsync(healthUrl.Url);
                    statusLinks.Add(new ResponseData
                    {
                        StatusCode = httpClient.StatusCode,
                        Url = healthUrl.Url
                    });
                }

                TimeSpan? countTime = null;

                var failLinks =
                    statusLinks.Count(x => !x.StatusCode.Equals(HttpStatusCode.OK));

                var successLinks =
                    statusLinks.Count(x => x.StatusCode.Equals(HttpStatusCode.OK));

                if (successLinks.Equals(grouping.Count()))
                {
                    if (countTime is null) continue;

                    foreach (var httpStatus in statusLinks)
                    foreach (var receiver in _appSettings.Smtp.Receivers)
                    {
                        _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now, receiver.Email);
                        if (httpStatus.Url != null)
                            SendEmail("Success", receiver.Email, receiver.Name, grouping.Key,
                                httpStatus.StatusCode, httpStatus.Url);
                        _logger.LogInformation("{Time} Sent notification to: {To}", DateTimeOffset.Now, receiver.Email);
                    }
                }
                else if ((successLinks > 0 && failLinks != 0) || countTime is { Minutes: > delaySendEmailTime } ||
                         errorTime is null)
                {
                    foreach (var httpStatus in statusLinks)
                    foreach (var receiver in _appSettings.Smtp.Receivers)
                        if (httpStatus.StatusCode.Equals(HttpStatusCode.OK))
                        {
                            _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                            if (httpStatus.Url != null)
                                SendEmail("Success", receiver.Email, receiver.Name, grouping.Key,
                                    httpStatus.StatusCode, httpStatus.Url);
                            _logger.LogInformation("{Time} Sent notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                        }
                        else
                        {
                            _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                            if (httpStatus.Url != null)
                                SendEmail("Error", receiver.Email, receiver.Name, grouping.Key,
                                    httpStatus.StatusCode, httpStatus.Url);
                            _logger.LogInformation("{Time} Sent notification to: {To}", DateTimeOffset.Now,
                                receiver.Email);
                        }

                    _logger.LogError($"{grouping.Key} service: Error");
                }
                else
                {
                    _logger.LogError($"{grouping.Key} service: Error");
                    if (countTime is not { Minutes: > delaySendEmailTime }) continue;

                    foreach (var httpStatus in statusLinks)
                    foreach (var receiver in _appSettings.Smtp.Receivers)
                    {
                        _logger.LogInformation("{Time} Send notification to: {To}", DateTimeOffset.Now,
                            receiver.Email);
                        if (httpStatus.Url != null)
                            SendEmail("Error", receiver.Email, receiver.Name, grouping.Key,
                                httpStatus.StatusCode, httpStatus.Url);
                    }
                }
            }

            await Task.Delay(3000);
        }
    }

    private string MailTemplate(string serviceName, HttpStatusCode code, string link, DateTimeOffset datetime,
        string environment)
    {
        var template =
            $@"<html>
            <body>
            <p>Môi trường: {environment}</p>
            <p>{serviceName}: {(int)code} - {code},</p>
            <p>Link: {link}</p>
            <p>Thời gian: {datetime}</p>
            </body>
            </html>";

        return template;
    }

    private async void SendEmail(string status, string email, string name, string serviceName,
        HttpStatusCode statusCode, string link)
    {
        var tryAgain = 10;
        var failed = false;
        do
        {
            try
            {
                await _mailService.SendAsync(_appSettings.Smtp, email,
                    name,
                    $"{status} - {_appSettings.MetaData.Title} - Thông báo hệ thống",
                    MailTemplate(
                        $"{serviceName} Service",
                        statusCode,
                        link,
                        DateTimeOffset.Now,
                        _appSettings.MetaData.Title));
            }
            catch (Exception ex)
            {
                failed = true;
                tryAgain--;
                _logger.LogWarning("FAILED: {Time} Sent notification to: {To}", DateTimeOffset.Now, email);
            }
        } while (failed && tryAgain != 0);
    }
}