﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class UpdateWebsitePriceStatusService : ICronJob
{
    private readonly ILogger<UpdateWebsitePriceStatusService> _logger;
    private readonly IMessageBusClient _messageBus;
    
    public UpdateWebsitePriceStatusService(
        ILogger<UpdateWebsitePriceStatusService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(UpdateWebsitePriceStatusService));
        _messageBus.Publish(MessageBusChannels.UpdateWebsitePriceStatus, new object());
        return await Task.FromResult($"Published event cache {nameof(UpdateWebsitePriceStatusService)}");
    }
}