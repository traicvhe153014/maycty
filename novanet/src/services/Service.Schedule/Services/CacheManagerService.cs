﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class CacheManagerService : ICronJob
{
    private readonly ILogger<CacheManagerService> _logger;
    private readonly IMessageBusClient _messageBus;

    public CacheManagerService(
        ILogger<CacheManagerService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(CacheManagerService));
        _messageBus.Publish(MessageBusChannels.UpdateCacheManager, new object());
        return await Task.FromResult($"Published event cache {nameof(CacheManagerService)}");
    }
}