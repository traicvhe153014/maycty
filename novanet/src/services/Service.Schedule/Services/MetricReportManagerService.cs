﻿using Novanet.Core.Hangfire;

namespace Service.Schedule.Services;

public class MetricReportManagerService : ICronJob
{
    private readonly ILogger<MetricReportManagerService> _logger;
    private readonly IMessageBusClient _messageBus;

    public MetricReportManagerService(
        ILogger<MetricReportManagerService> logger,
        IMessageBusClient messageBus)
    {
        _logger = logger;
        _messageBus = messageBus;
    }

    public async Task<string> Run()
    {
        _logger.LogInformation("Starting job: {Job}", nameof(MetricReportManagerService));
        _messageBus.Publish(MessageBusChannels.UpdateMetricReportManager, new object());
        return await Task.FromResult($"Published event cache {nameof(MetricReportManagerService)}");
    }
}