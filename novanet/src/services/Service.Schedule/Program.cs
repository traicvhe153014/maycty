using Novanet.Core.Hangfire;
using Novanet.Core.Services;

WebApplication.CreateBuilder(args).NovanetCore<ScheduleContext>(ScheduleContext.Schema,
    out var app, (service, appSettings) =>
    {
        service.AddSingleton(appSettings);
        service.AddRabbitMQ(appSettings.RabbitMQ);
        service.AddHangfireConfiguration(appSettings, ScheduleContext.Schema);
        service.AddSingleton<IMailService, MailService>();
        
        service.AddScoped<ICronJob, AdvertisingFraudClickService>();
        service.AddScoped<ICronJob, AdvertisingLogService>();
        service.AddScoped<ICronJob, AdvertisingLogFlagService>();
        service.AddScoped<ICronJob, CacheIdentityService>();
        service.AddScoped<ICronJob, CacheSettingsService>();
        service.AddScoped<ICronJob, CacheSharingService>();
        service.AddScoped<ICronJob, CacheManagerService>();
        service.AddScoped<ICronJob, CheckCampaignBudgetService>();
        service.AddScoped<ICronJob, HealthSystemService>();
        service.AddScoped<ICronJob, UpdateAdObjectStatusService>();
        service.AddScoped<ICronJob, MetricReportManagerService>();
        service.AddScoped<ICronJob, UpdateWebsitePriceStatusService>();
        service.AddHostedService<InitializeCacheManagerService>();
    }, application =>
    {
        application.MapGet("/", () => $"{ScheduleContext.Schema} - Hello World!");
        application.UseJobs();
        application.UseHangfire();
    });

app.Run();