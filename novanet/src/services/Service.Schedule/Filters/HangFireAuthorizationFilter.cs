﻿using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace Service.Schedule.Filters;

public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
{
    public bool Authorize([NotNull] DashboardContext context)
    {
        return true;
    }
}