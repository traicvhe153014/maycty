﻿namespace Service.Schedule.Abstracts;

[AutomaticRetry(Attempts = 0)]
public interface ICronJob
{
    Task<string> Run();
}