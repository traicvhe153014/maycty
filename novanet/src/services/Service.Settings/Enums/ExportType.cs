﻿namespace Service.Settings.Enums;

public enum ExportType
{
    Detail = 1,
    Campaign = 2,
    AdvertisingSet = 3,
    Advertising = 4
}