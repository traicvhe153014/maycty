using System.ComponentModel;
using System.Data;
using Novanet.Core.SqlScripts;
using Novanet.EventBus;
using NovanetCore.Business;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessServices;
using OfficeOpenXml;
using Service.Settings.Events;
using LicenseContext = OfficeOpenXml.LicenseContext;

var tmp = FulltextSearchBuilder.FulltextSearchScript<SettingsContext>();

ExcelPackage.LicenseContext = LicenseContext.Commercial;

WebApplication.CreateBuilder(args).NovanetCore<SettingsContext>(SettingsContext.Schema,
    out var app, (service, appSettings) =>
    {
        service.AddSingleton(appSettings);
        service.AddBusinessCore();
        service.AddRabbitMQ(appSettings.RabbitMQ);
        service.AddGrpc();
        service.AddCacheManagerServices();
        service.AddHostedService<CacheSettingsService>();
        service.AddHostedService<UpdateAdObjectStatusService>();
        service.AddHostedService<CheckCampaignBudgetService>();
        service.AddHostedService<UpdateMetricReportManagerService>();
        service.AddHostedService<UpdateWebsitePriceStatusService>();
        service.AddHostedService<UpdateAdvertisingVideoValueService>();
        service.AddScoped<MetricReportManagerService>();
        service.AddSingleton<SheetsService>();
        service.AddScoped<IReportService, ReportService>();
        service.AddScoped<IProductFeedService, ProductFeedService>();
        service.AddScoped<IProductGroupService, ProductGroupService>();
        service.AddScoped<IUpdateStatusService, UpdateStatusService>();
        service
            .AddScoped<IExportDataToExcelFile<Dictionary<string, object>>,
                ExportDataToExcelFile<Dictionary<string, object>>>();
    }, application =>
    {
        application.MapGet("/", () => $"{SettingsContext.Schema} - Hello World!");
        application.MigrateDatabase<SettingsContext>((context, services) =>
        {
            var logger = services.GetService<ILogger<SettingsContextSeed>>();
            var connection = services.GetRequiredService<IDbConnection>();
            SettingsContextSeed.SeedAsync(context, connection, logger, services).Wait();
        });
        var appSettings = application.Services.GetRequiredService<AppSettings>();
        appSettings.Kestrel.Endpoints.TryGetValue("gRPC", out var kestrelConfiguration);
        var grpcUrl = kestrelConfiguration?.Url ?? string.Empty;
        var grpcUri = new Uri(grpcUrl);
        application.UseWhen(context => context.Connection.LocalPort == grpcUri.Port, builder =>
        {
            builder.UseRouting();
            builder.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GrpcCacheService>();
            });
        });
    });
app.Run();