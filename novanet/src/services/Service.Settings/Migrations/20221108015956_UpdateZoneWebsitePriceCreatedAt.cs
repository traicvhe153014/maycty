﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateZoneWebsitePriceCreatedAt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "Zones",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "Zones",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "WebsitePrices",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "WebsitePrices",
                type: "datetimeoffset",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "Zones");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "Zones");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "WebsitePrices");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "WebsitePrices");
        }
    }
}
