﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class AddAdvertisingSetCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "Sequence_63C9459F73E2BB3F5E988FD97E1A9376",
                schema: "Service.Settings");

            migrationBuilder.CreateTable(
                name: "AdvertisingSetCategories",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Search = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_63C9459F73E2BB3F5E988FD97E1A9376"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetCategories_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetCategories_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetCategories",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetCategories_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetCategories",
                column: "SubId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvertisingSetCategories",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_63C9459F73E2BB3F5E988FD97E1A9376",
                schema: "Service.Settings");
        }
    }
}
