﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateProductEntityHashKeyName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntityCore_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntityCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntityCore_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductGroupEntityCore",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.RenameTable(
                name: "ProductGroupEntityCore",
                schema: "Service.Settings",
                newName: "ProductGroupEntities",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_SubId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductAttributeValueId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductAttributeValueHashCode");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductGroupEntities",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValueCore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductGroupId",
                principalSchema: "Service.Settings",
                principalTable: "ProductGroups",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductGroupEntities",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.RenameTable(
                name: "ProductGroupEntities",
                schema: "Service.Settings",
                newName: "ProductGroupEntityCore",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_SubId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductAttributeValueId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductAttributeValueHashCode");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductGroupEntityCore",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntityCore_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValueCore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntityCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntityCore_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductGroupId",
                principalSchema: "Service.Settings",
                principalTable: "ProductGroups",
                principalColumn: "Id");
        }
    }
}
