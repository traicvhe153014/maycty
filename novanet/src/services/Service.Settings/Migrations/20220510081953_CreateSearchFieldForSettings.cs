﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class CreateSearchFieldForSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ZoneTemplates",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "Zones",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "Websites",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "WebsitePrices",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductStatus",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductGroups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductFeeds",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductEntities",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductAttributes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "Payment",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ObjectGroupWebsiteConditions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ObjectGroups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "ObjectGroupConsumerBehavior",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "DomainAliases",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "CampaignScheduling",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "Campaigns",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingTemplates",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributeValues",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetScheduling",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSets",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetProhibitedCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetProductGroups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetLocations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetGenders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetAges",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingAppliedTemplates",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Settings",
                table: "Advertising",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ZoneTemplates");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "Zones");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "Websites");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "WebsitePrices");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductStatus");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductGroups");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductFeeds");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductEntities");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ProductAttributes");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ObjectGroupWebsiteConditions");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ObjectGroups");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "ObjectGroupConsumerBehavior");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "DomainAliases");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "CampaignScheduling");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingTemplates");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributeValues");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributes");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetScheduling");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSets");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetProhibitedCategories");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetProductGroups");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetLocations");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetGenders");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingSetAges");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "AdvertisingAppliedTemplates");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Settings",
                table: "Advertising");
        }
    }
}
