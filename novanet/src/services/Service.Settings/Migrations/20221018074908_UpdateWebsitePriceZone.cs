﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateWebsitePriceZone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ZoneTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "Service.Settings",
                table: "WebsitePrices",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                schema: "Service.Settings",
                table: "WebsitePrices",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteTemplatePrices_ZoneTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                column: "ZoneTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteTemplatePrices_ZoneTemplates_ZoneTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                column: "ZoneTemplateId",
                principalSchema: "Service.Settings",
                principalTable: "ZoneTemplates",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteTemplatePrices_ZoneTemplates_ZoneTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteTemplatePrices_ZoneTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropColumn(
                name: "ZoneTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "Service.Settings",
                table: "WebsitePrices");

            migrationBuilder.DropColumn(
                name: "Status",
                schema: "Service.Settings",
                table: "WebsitePrices");
        }
    }
}
