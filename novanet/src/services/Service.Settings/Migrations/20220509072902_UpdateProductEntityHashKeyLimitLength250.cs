﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateProductEntityHashKeyLimitLength250 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValues_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValues_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductStatus_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductGroupEntities",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductAttributeValues",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.RenameTable(
                name: "ProductGroupEntities",
                schema: "Service.Settings",
                newName: "ProductGroupEntityCore",
                newSchema: "Service.Settings");

            migrationBuilder.RenameTable(
                name: "ProductAttributeValues",
                schema: "Service.Settings",
                newName: "ProductAttributeValueCore",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_SubId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntities_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                newName: "IX_ProductGroupEntityCore_ProductAttributeValueId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_SubId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_AttributeId");

            migrationBuilder.AlterColumn<string>(
                name: "ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductGroupEntityCore",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductAttributeValueCore",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupEntityCore_ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductAttributeValueHashCode");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributeValueCore_HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "HashKey");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValueCore_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "AttributeId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValueCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntityCore_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValueCore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntityCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntityCore_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore",
                column: "ProductGroupId",
                principalSchema: "Service.Settings",
                principalTable: "ProductGroups",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductStatus_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValueCore",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValueCore_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValueCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntityCore_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntityCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntityCore_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductStatus_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductGroupEntityCore",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropIndex(
                name: "IX_ProductGroupEntityCore_ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductAttributeValueCore",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropIndex(
                name: "IX_ProductAttributeValueCore_HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductGroupEntityCore");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.RenameTable(
                name: "ProductGroupEntityCore",
                schema: "Service.Settings",
                newName: "ProductGroupEntities",
                newSchema: "Service.Settings");

            migrationBuilder.RenameTable(
                name: "ProductAttributeValueCore",
                schema: "Service.Settings",
                newName: "ProductAttributeValues",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_SubId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductGroupEntityCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                newName: "IX_ProductGroupEntities_ProductAttributeValueId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_SubId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_AttributeId");

            migrationBuilder.AlterColumn<string>(
                name: "ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductGroupEntities",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductAttributeValues",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValues_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "AttributeId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValues_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductGroupId",
                principalSchema: "Service.Settings",
                principalTable: "ProductGroups",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductStatus_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValues",
                principalColumn: "Id");
        }
    }
}
