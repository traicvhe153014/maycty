﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateWebsitePriceCategoryType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "Sequence_13AF7C8D3D628ADFD5E22EF771849FEE",
                schema: "Service.Settings");

            migrationBuilder.CreateTable(
                name: "WebsitePriceCampaignTypes",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Search = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_13AF7C8D3D628ADFD5E22EF771849FEE"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    WebsitePriceId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CampaignType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsitePriceCampaignTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsitePriceCampaignTypes_WebsitePrices_WebsitePriceId",
                        column: x => x.WebsitePriceId,
                        principalSchema: "Service.Settings",
                        principalTable: "WebsitePrices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePriceCampaignTypes_SubId",
                schema: "Service.Settings",
                table: "WebsitePriceCampaignTypes",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePriceCampaignTypes_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsitePriceCampaignTypes",
                column: "WebsitePriceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WebsitePriceCampaignTypes",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_13AF7C8D3D628ADFD5E22EF771849FEE",
                schema: "Service.Settings");
        }
    }
}
