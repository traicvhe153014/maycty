﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class InitialDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_01671C87C9EF6D617D02B5399C36A049",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_09D324B4606B04A87998B1EE763F938F",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_18F26C662C135FAE8BC2D12B4FB7630C",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_196C180B502A388E23EF5A8B1316F30E",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_2399BFB09E2ED19244D2AC7074A7C540",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_2E2305599FA8FA7D730E3EB3262DC45B",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_363C11CAF7830101BD8FF5A1FFEAD3B8",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_4D8103FD130302C0340BEDB179EBAB90",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_51A221E542628DCEEB178C1CF97B3950",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_5B1F376B4BE8CA1B0830EC8C1C916ECD",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_5EB35D370663D7F69AC09CB5B9142254",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_64EE5E90BB09AD30025B214E71380726",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_75152972327A4B994A823CC941E98C3C",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_7FDE2EB04D4D44DC076D49CDE6D3F9CC",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_829F3FCA51894DE50976AE3AE428EF61",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_84ABCE5875BE16429298BB757F8CF341",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_868562B4253B89079EF9DD6A55810B51",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_A32EFAB34CD7C4071565461A2B48AB84",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_A86C66B5F824F408196567DBCD05ED8C",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_A97BDCE31FA2A7C569D2C060DF88E052",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_B115135A1AFF9825454EC973D842D983",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_BFB829AEB56C1384422D5B23618A2341",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_C11C05DB66B5E2E3EF7933EAD03C11C8",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_CA1577B3FD99BBB49DD4E883695688E9",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_D376DEEAE22C06F2AA41CCF551D44D40",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_D5F63C431EC35A8392EAE21A6EF9DE28",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_E50EF2E191A93596A4E7C4E1123E0968",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_E65DE1C10F0B689F6115F43BFA39B78E",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_EF09BA28EFC465F95CE0B1FA1D64B7C0",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_F4CDFFB8E9C458F188B59EA0EB6BC345",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_F51E9A4F71DD60B300BB3726D052531C",
                schema: "Service.Settings");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_F8E2510BBCBCE6232E75ED5714F90C5C",
                schema: "Service.Settings");

            migrationBuilder.CreateTable(
                name: "AdvertisingTemplateAttributes",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_D376DEEAE22C06F2AA41CCF551D44D40"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    DisplayedName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    DataType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingTemplateAttributes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingTemplates",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_363C11CAF7830101BD8FF5A1FFEAD3B8"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Width = table.Column<int>(type: "int", nullable: false),
                    Height = table.Column<int>(type: "int", nullable: false),
                    TemplateType = table.Column<int>(type: "int", nullable: false),
                    TimeToDisplay = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ObjectGroups",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_F4CDFFB8E9C458F188B59EA0EB6BC345"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ConnectionStatus = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Payment",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CustomerName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Amount = table.Column<double>(type: "float", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_4D8103FD130302C0340BEDB179EBAB90"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductAttributes",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_868562B4253B89079EF9DD6A55810B51"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Required = table.Column<bool>(type: "bit", nullable: false),
                    MaxLength = table.Column<int>(type: "int", nullable: false),
                    DataType = table.Column<int>(type: "int", nullable: false),
                    Order = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAttributes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductFeeds",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_196C180B502A388E23EF5A8B1316F30E"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SourceType = table.Column<int>(type: "int", nullable: false),
                    SourcePath = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Permission = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ValidProductsCount = table.Column<int>(type: "int", nullable: false),
                    InvalidProductsCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductFeeds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Websites",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_18F26C662C135FAE8BC2D12B4FB7630C"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PublisherId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Domain = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Url = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Keywords = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AlexaRank = table.Column<int>(type: "int", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Websites", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ObjectGroupConsumerBehavior",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_CA1577B3FD99BBB49DD4E883695688E9"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    BehaviorGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ObjectGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    BehaviorType = table.Column<int>(type: "int", nullable: false),
                    BeforeBehaviorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AfterBehaviorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ExcludeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ObjectType = table.Column<int>(type: "int", nullable: false),
                    From = table.Column<int>(type: "int", nullable: true),
                    To = table.Column<int>(type: "int", nullable: true),
                    Unit = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectGroupConsumerBehavior", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObjectGroupConsumerBehavior_ObjectGroups_ObjectGroupId",
                        column: x => x.ObjectGroupId,
                        principalSchema: "Service.Settings",
                        principalTable: "ObjectGroups",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ObjectGroupWebsiteConditions",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_A86C66B5F824F408196567DBCD05ED8C"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ObjectGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    BeforeConditionType = table.Column<int>(type: "int", nullable: false),
                    AfterConditionType = table.Column<int>(type: "int", nullable: false),
                    BeforeConditionId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AfterConditionId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    MatchingType = table.Column<int>(type: "int", nullable: false),
                    ConditionType = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectGroupWebsiteConditions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObjectGroupWebsiteConditions_ObjectGroups_ObjectGroupId",
                        column: x => x.ObjectGroupId,
                        principalSchema: "Service.Settings",
                        principalTable: "ObjectGroups",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Campaigns",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_C11C05DB66B5E2E3EF7933EAD03C11C8"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    CampaignType = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    ShowSpeed = table.Column<int>(type: "int", nullable: false),
                    EcommerceAmountSpent = table.Column<double>(type: "float", nullable: false),
                    EcommerceDailyBudget = table.Column<double>(type: "float", nullable: false),
                    EcommerceProductFeedId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EcommerceStartDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    EcommerceEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    EcommerceScheduled = table.Column<bool>(type: "bit", nullable: true),
                    EcommerceScheduledAllTime = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Campaigns_ProductFeeds_EcommerceProductFeedId",
                        column: x => x.EcommerceProductFeedId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductFeeds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductEntities",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_09D324B4606B04A87998B1EE763F938F"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductFeedId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    IsValid = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductEntities_ProductFeeds_ProductFeedId",
                        column: x => x.ProductFeedId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductFeeds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductGroups",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_5B1F376B4BE8CA1B0830EC8C1C916ECD"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    ProductFeedId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductAttributeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductGroups_ProductAttributes_ProductAttributeId",
                        column: x => x.ProductAttributeId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductAttributes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductGroups_ProductFeeds_ProductFeedId",
                        column: x => x.ProductFeedId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductFeeds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DomainAliases",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_64EE5E90BB09AD30025B214E71380726"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    WebsiteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DomainAliases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DomainAliases_Websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalSchema: "Service.Settings",
                        principalTable: "Websites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Zones",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_7FDE2EB04D4D44DC076D49CDE6D3F9CC"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Width = table.Column<int>(type: "int", nullable: false),
                    Height = table.Column<int>(type: "int", nullable: false),
                    CampaignType = table.Column<int>(type: "int", nullable: false),
                    UsingBackup = table.Column<bool>(type: "bit", nullable: false),
                    BackupType = table.Column<int>(type: "int", nullable: true),
                    BackupCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    WebsiteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Zones_Websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalSchema: "Service.Settings",
                        principalTable: "Websites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSets",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_2E2305599FA8FA7D730E3EB3262DC45B"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    AdvertisingSetMappingId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CampaignId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ApplyAllProductGroups = table.Column<bool>(type: "bit", nullable: false),
                    HasProhibitedCategories = table.Column<bool>(type: "bit", nullable: false),
                    HasObject = table.Column<bool>(type: "bit", nullable: false),
                    RemarketingType = table.Column<int>(type: "int", nullable: true),
                    RemarketingTime = table.Column<int>(type: "int", nullable: true),
                    TimeOnDisplay = table.Column<int>(type: "int", nullable: true),
                    AdImpressions = table.Column<int>(type: "int", nullable: true),
                    TargetingMarketingByProduct = table.Column<bool>(type: "bit", nullable: true),
                    Uniformed = table.Column<bool>(type: "bit", nullable: true),
                    UniformedPrice = table.Column<double>(type: "float", nullable: true),
                    UniformedUnit = table.Column<int>(type: "int", nullable: true),
                    ClickedAdvertising = table.Column<bool>(type: "bit", nullable: true),
                    Viewer = table.Column<bool>(type: "bit", nullable: true),
                    ViewedMax = table.Column<int>(type: "int", nullable: true),
                    ExcludedWebsite = table.Column<bool>(type: "bit", nullable: true),
                    ScheduledAllTime = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSets_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalSchema: "Service.Settings",
                        principalTable: "Campaigns",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CampaignScheduling",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_EF09BA28EFC465F95CE0B1FA1D64B7C0"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CampaignId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DayOfWeek = table.Column<int>(type: "int", nullable: false),
                    Timing = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignScheduling", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CampaignScheduling_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalSchema: "Service.Settings",
                        principalTable: "Campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductAttributeValues",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_D5F63C431EC35A8392EAE21A6EF9DE28"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AttributeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StringValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateTimeValue = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    NumberValue = table.Column<long>(type: "bigint", nullable: true),
                    BooleanValue = table.Column<bool>(type: "bit", nullable: true),
                    DoubleValue = table.Column<double>(type: "float", nullable: true),
                    HashKey = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAttributeValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductAttributeValues_ProductAttributes_AttributeId",
                        column: x => x.AttributeId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductAttributes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductAttributeValues_ProductEntities_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ZoneTemplates",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_75152972327A4B994A823CC941E98C3C"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ZoneId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingTemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZoneTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZoneTemplates_AdvertisingTemplates_AdvertisingTemplateId",
                        column: x => x.AdvertisingTemplateId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingTemplates",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ZoneTemplates_Zones_ZoneId",
                        column: x => x.ZoneId,
                        principalSchema: "Service.Settings",
                        principalTable: "Zones",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Advertising",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_A97BDCE31FA2A7C569D2C060DF88E052"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DailyBudget = table.Column<double>(type: "float", nullable: false),
                    AmountSpent = table.Column<double>(type: "float", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    CampaignId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Location = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MinimumTimeOnSiteRemarketing = table.Column<int>(type: "int", nullable: false),
                    RetargetScore = table.Column<int>(type: "int", nullable: true),
                    TemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RedirectLink = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Advertising", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Advertising_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Advertising_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalSchema: "Service.Settings",
                        principalTable: "Campaigns",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetAges",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_BFB829AEB56C1384422D5B23618A2341"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AgeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetAges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetAges_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetGenders",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_B115135A1AFF9825454EC973D842D983"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    GenderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetGenders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetGenders_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetLocations",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_E50EF2E191A93596A4E7C4E1123E0968"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LocationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetLocations_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetObjects",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_01671C87C9EF6D617D02B5399C36A049"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ObjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetObjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetObjects_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetProductGroups",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_F51E9A4F71DD60B300BB3726D052531C"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetProductGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetProductGroups_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AdvertisingSetProductGroups_ProductGroups_ProductGroupId",
                        column: x => x.ProductGroupId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductGroups",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetProhibitedCategories",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_A32EFAB34CD7C4071565461A2B48AB84"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProhibitedCategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetProhibitedCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetProhibitedCategories_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetScheduling",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_E65DE1C10F0B689F6115F43BFA39B78E"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DayOfWeek = table.Column<int>(type: "int", nullable: false),
                    Timing = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetScheduling", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetScheduling_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingSetWebsites",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_F8E2510BBCBCE6232E75ED5714F90C5C"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WebsiteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WebsiteCondition = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingSetWebsites", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingSetWebsites_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "WebsitePrices",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_829F3FCA51894DE50976AE3AE428EF61"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    WebsiteId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingSetId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Timestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    StartDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    EndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    TemplateType = table.Column<int>(type: "int", nullable: false),
                    BuyPriceType = table.Column<int>(type: "int", nullable: false),
                    BuyPrice = table.Column<double>(type: "float", nullable: false),
                    SellPriceType = table.Column<int>(type: "int", nullable: false),
                    SellPrice = table.Column<double>(type: "float", nullable: false),
                    ZoneId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsitePrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsitePrices_AdvertisingSets_AdvertisingSetId",
                        column: x => x.AdvertisingSetId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingSets",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WebsitePrices_Websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalSchema: "Service.Settings",
                        principalTable: "Websites",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WebsitePrices_Zones_ZoneId",
                        column: x => x.ZoneId,
                        principalSchema: "Service.Settings",
                        principalTable: "Zones",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductGroupEntities",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_5EB35D370663D7F69AC09CB5B9142254"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductAttributeValueId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductAttributeValueHashCode = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductGroupEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductGroupEntities_ProductAttributeValues_ProductAttributeValueId",
                        column: x => x.ProductAttributeValueId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductAttributeValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductGroupEntities_ProductEntities_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductEntities",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductGroupEntities_ProductGroups_ProductGroupId",
                        column: x => x.ProductGroupId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductGroups",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductStatus",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Success = table.Column<bool>(type: "bit", nullable: false),
                    ErrorMessage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ErrorType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SheetPosition = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductIdValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductEntityId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductAttributeValueId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProductFeedId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_51A221E542628DCEEB178C1CF97B3950"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductStatus_ProductAttributeValues_ProductAttributeValueId",
                        column: x => x.ProductAttributeValueId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductAttributeValues",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductStatus_ProductEntities_ProductEntityId",
                        column: x => x.ProductEntityId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductEntities",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProductStatus_ProductFeeds_ProductFeedId",
                        column: x => x.ProductFeedId,
                        principalSchema: "Service.Settings",
                        principalTable: "ProductFeeds",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingAppliedTemplates",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_2399BFB09E2ED19244D2AC7074A7C540"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingTemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingAppliedTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingAppliedTemplates_Advertising_AdvertisingId",
                        column: x => x.AdvertisingId,
                        principalSchema: "Service.Settings",
                        principalTable: "Advertising",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AdvertisingAppliedTemplates_AdvertisingTemplates_AdvertisingTemplateId",
                        column: x => x.AdvertisingTemplateId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingTemplates",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingTemplateAttributeValues",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_84ABCE5875BE16429298BB757F8CF341"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingTemplateAttributeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingAppliedTemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    StringValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateTimeValue = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    NumberValue = table.Column<long>(type: "bigint", nullable: true),
                    BooleanValue = table.Column<bool>(type: "bit", nullable: true),
                    DoubleValue = table.Column<double>(type: "float", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingTemplateAttributeValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingTemplateAttributeValues_AdvertisingAppliedTemplates_AdvertisingAppliedTemplateId",
                        column: x => x.AdvertisingAppliedTemplateId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingAppliedTemplates",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AdvertisingTemplateAttributeValues_AdvertisingTemplateAttributes_AdvertisingTemplateAttributeId",
                        column: x => x.AdvertisingTemplateAttributeId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingTemplateAttributes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Advertising_AdvertisingSetId",
                schema: "Service.Settings",
                table: "Advertising",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_Advertising_CampaignId",
                schema: "Service.Settings",
                table: "Advertising",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Advertising_SubId",
                schema: "Service.Settings",
                table: "Advertising",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingAppliedTemplates_AdvertisingId",
                schema: "Service.Settings",
                table: "AdvertisingAppliedTemplates",
                column: "AdvertisingId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingAppliedTemplates_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "AdvertisingAppliedTemplates",
                column: "AdvertisingTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingAppliedTemplates_SubId",
                schema: "Service.Settings",
                table: "AdvertisingAppliedTemplates",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetAges_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetAges",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetAges_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetAges",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetGenders_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetGenders",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetGenders_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetGenders",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetLocations_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetLocations",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetLocations_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetLocations",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetObjects_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetObjects_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetProductGroups_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetProductGroups",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetProductGroups_ProductGroupId",
                schema: "Service.Settings",
                table: "AdvertisingSetProductGroups",
                column: "ProductGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetProductGroups_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetProductGroups",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetProhibitedCategories_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetProhibitedCategories",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetProhibitedCategories_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetProhibitedCategories",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSets_CampaignId",
                schema: "Service.Settings",
                table: "AdvertisingSets",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSets_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSets",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetScheduling_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetScheduling",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetScheduling_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetScheduling",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetWebsites_AdvertisingSetId",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetWebsites_SubId",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingTemplateAttributes_SubId",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributes",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingTemplateAttributeValues_AdvertisingAppliedTemplateId",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributeValues",
                column: "AdvertisingAppliedTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingTemplateAttributeValues_AdvertisingTemplateAttributeId",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributeValues",
                column: "AdvertisingTemplateAttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingTemplateAttributeValues_SubId",
                schema: "Service.Settings",
                table: "AdvertisingTemplateAttributeValues",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingTemplates_SubId",
                schema: "Service.Settings",
                table: "AdvertisingTemplates",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns",
                column: "EcommerceProductFeedId");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_SubId",
                schema: "Service.Settings",
                table: "Campaigns",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignScheduling_CampaignId",
                schema: "Service.Settings",
                table: "CampaignScheduling",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignScheduling_SubId",
                schema: "Service.Settings",
                table: "CampaignScheduling",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_DomainAliases_SubId",
                schema: "Service.Settings",
                table: "DomainAliases",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_DomainAliases_WebsiteId",
                schema: "Service.Settings",
                table: "DomainAliases",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_ObjectGroupConsumerBehavior_ObjectGroupId",
                schema: "Service.Settings",
                table: "ObjectGroupConsumerBehavior",
                column: "ObjectGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ObjectGroupConsumerBehavior_SubId",
                schema: "Service.Settings",
                table: "ObjectGroupConsumerBehavior",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ObjectGroups_SubId",
                schema: "Service.Settings",
                table: "ObjectGroups",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ObjectGroupWebsiteConditions_ObjectGroupId",
                schema: "Service.Settings",
                table: "ObjectGroupWebsiteConditions",
                column: "ObjectGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ObjectGroupWebsiteConditions_SubId",
                schema: "Service.Settings",
                table: "ObjectGroupWebsiteConditions",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_SubId",
                schema: "Service.Settings",
                table: "Payment",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributes_SubId",
                schema: "Service.Settings",
                table: "ProductAttributes",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributeValues_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributeValues_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductAttributeValues_SubId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductEntities_ProductFeedId",
                schema: "Service.Settings",
                table: "ProductEntities",
                column: "ProductFeedId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductEntities_SubId",
                schema: "Service.Settings",
                table: "ProductEntities",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductFeeds_SubId",
                schema: "Service.Settings",
                table: "ProductFeeds",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupEntities_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductAttributeValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupEntities_ProductGroupId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroupEntities_SubId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroups_ProductAttributeId",
                schema: "Service.Settings",
                table: "ProductGroups",
                column: "ProductAttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroups_ProductFeedId",
                schema: "Service.Settings",
                table: "ProductGroups",
                column: "ProductFeedId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductGroups_SubId",
                schema: "Service.Settings",
                table: "ProductGroups",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductStatus_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductAttributeValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductStatus_ProductEntityId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductStatus_ProductFeedId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductFeedId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductStatus_SubId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePrices_AdvertisingSetId",
                schema: "Service.Settings",
                table: "WebsitePrices",
                column: "AdvertisingSetId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePrices_SubId",
                schema: "Service.Settings",
                table: "WebsitePrices",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePrices_WebsiteId",
                schema: "Service.Settings",
                table: "WebsitePrices",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsitePrices_ZoneId",
                schema: "Service.Settings",
                table: "WebsitePrices",
                column: "ZoneId");

            migrationBuilder.CreateIndex(
                name: "IX_Websites_SubId",
                schema: "Service.Settings",
                table: "Websites",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Zones_SubId",
                schema: "Service.Settings",
                table: "Zones",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Zones_WebsiteId",
                schema: "Service.Settings",
                table: "Zones",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_ZoneTemplates_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "ZoneTemplates",
                column: "AdvertisingTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_ZoneTemplates_SubId",
                schema: "Service.Settings",
                table: "ZoneTemplates",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ZoneTemplates_ZoneId",
                schema: "Service.Settings",
                table: "ZoneTemplates",
                column: "ZoneId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvertisingSetAges",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetGenders",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetLocations",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetObjects",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetProductGroups",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetProhibitedCategories",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetScheduling",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSetWebsites",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingTemplateAttributeValues",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "CampaignScheduling",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "DomainAliases",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ObjectGroupConsumerBehavior",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ObjectGroupWebsiteConditions",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "Payment",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductGroupEntities",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductStatus",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "WebsitePrices",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ZoneTemplates",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingAppliedTemplates",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingTemplateAttributes",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ObjectGroups",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductGroups",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductAttributeValues",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "Zones",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "Advertising",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingTemplates",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductAttributes",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductEntities",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "Websites",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "AdvertisingSets",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "Campaigns",
                schema: "Service.Settings");

            migrationBuilder.DropTable(
                name: "ProductFeeds",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_01671C87C9EF6D617D02B5399C36A049",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_09D324B4606B04A87998B1EE763F938F",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_18F26C662C135FAE8BC2D12B4FB7630C",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_196C180B502A388E23EF5A8B1316F30E",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_2399BFB09E2ED19244D2AC7074A7C540",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_2E2305599FA8FA7D730E3EB3262DC45B",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_363C11CAF7830101BD8FF5A1FFEAD3B8",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_4D8103FD130302C0340BEDB179EBAB90",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_51A221E542628DCEEB178C1CF97B3950",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_5B1F376B4BE8CA1B0830EC8C1C916ECD",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_5EB35D370663D7F69AC09CB5B9142254",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_64EE5E90BB09AD30025B214E71380726",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_75152972327A4B994A823CC941E98C3C",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_7FDE2EB04D4D44DC076D49CDE6D3F9CC",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_829F3FCA51894DE50976AE3AE428EF61",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_84ABCE5875BE16429298BB757F8CF341",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_868562B4253B89079EF9DD6A55810B51",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_A32EFAB34CD7C4071565461A2B48AB84",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_A86C66B5F824F408196567DBCD05ED8C",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_A97BDCE31FA2A7C569D2C060DF88E052",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_B115135A1AFF9825454EC973D842D983",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_BFB829AEB56C1384422D5B23618A2341",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_C11C05DB66B5E2E3EF7933EAD03C11C8",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_CA1577B3FD99BBB49DD4E883695688E9",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_D376DEEAE22C06F2AA41CCF551D44D40",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_D5F63C431EC35A8392EAE21A6EF9DE28",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_E50EF2E191A93596A4E7C4E1123E0968",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_E65DE1C10F0B689F6115F43BFA39B78E",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_EF09BA28EFC465F95CE0B1FA1D64B7C0",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_F4CDFFB8E9C458F188B59EA0EB6BC345",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_F51E9A4F71DD60B300BB3726D052531C",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_F8E2510BBCBCE6232E75ED5714F90C5C",
                schema: "Service.Settings");
        }
    }
}
