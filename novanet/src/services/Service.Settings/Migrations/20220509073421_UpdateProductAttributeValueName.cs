﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateProductAttributeValueName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValueCore_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValueCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductStatus_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductAttributeValueCore",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore");

            migrationBuilder.RenameTable(
                name: "ProductAttributeValueCore",
                schema: "Service.Settings",
                newName: "ProductAttributeValues",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_SubId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_HashKey");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValueCore_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                newName: "IX_ProductAttributeValues_AttributeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductAttributeValues",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValues_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "AttributeId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValues_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductStatus_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValues",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValues_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductAttributeValues_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductStatus_ProductAttributeValues_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductAttributeValues",
                schema: "Service.Settings",
                table: "ProductAttributeValues");

            migrationBuilder.RenameTable(
                name: "ProductAttributeValues",
                schema: "Service.Settings",
                newName: "ProductAttributeValueCore",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_SubId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_SubId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_HashKey");

            migrationBuilder.RenameIndex(
                name: "IX_ProductAttributeValues_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                newName: "IX_ProductAttributeValueCore_AttributeId");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductAttributeValueCore",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValueCore_ProductAttributes_AttributeId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "AttributeId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductAttributeValueCore_ProductEntities_ProductId",
                schema: "Service.Settings",
                table: "ProductAttributeValueCore",
                column: "ProductId",
                principalSchema: "Service.Settings",
                principalTable: "ProductEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroupEntities_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValueCore",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductStatus_ProductAttributeValueCore_ProductAttributeValueId",
                schema: "Service.Settings",
                table: "ProductStatus",
                column: "ProductAttributeValueId",
                principalSchema: "Service.Settings",
                principalTable: "ProductAttributeValueCore",
                principalColumn: "Id");
        }
    }
}
