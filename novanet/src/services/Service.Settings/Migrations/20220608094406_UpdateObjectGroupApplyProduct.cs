﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateObjectGroupApplyProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ApplyAllProducts",
                schema: "Service.Settings",
                table: "ObjectGroups",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ProductName",
                schema: "Service.Settings",
                table: "ObjectGroups",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductType",
                schema: "Service.Settings",
                table: "ObjectGroups",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplyAllProducts",
                schema: "Service.Settings",
                table: "ObjectGroups");

            migrationBuilder.DropColumn(
                name: "ProductName",
                schema: "Service.Settings",
                table: "ObjectGroups");

            migrationBuilder.DropColumn(
                name: "ProductType",
                schema: "Service.Settings",
                table: "ObjectGroups");
        }
    }
}
