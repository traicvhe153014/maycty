﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateWebsitePriceTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PublisherPriceType",
                schema: "Service.Settings",
                table: "WebsitePrices",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "WebsiteTemplatePrice",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Search = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    TemplateType = table.Column<int>(type: "int", nullable: false),
                    BuyPriceType = table.Column<int>(type: "int", nullable: false),
                    BuyPrice = table.Column<double>(type: "float", nullable: false),
                    SellPriceType = table.Column<int>(type: "int", nullable: false),
                    SellPrice = table.Column<double>(type: "float", nullable: false),
                    WebsitePriceId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    AdvertisingTemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteTemplatePrice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsiteTemplatePrice_AdvertisingTemplates_AdvertisingTemplateId",
                        column: x => x.AdvertisingTemplateId,
                        principalSchema: "Service.Settings",
                        principalTable: "AdvertisingTemplates",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WebsiteTemplatePrice_WebsitePrices_WebsitePriceId",
                        column: x => x.WebsitePriceId,
                        principalSchema: "Service.Settings",
                        principalTable: "WebsitePrices",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetObjects_ObjectId",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects",
                column: "ObjectId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteTemplatePrice_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                column: "AdvertisingTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteTemplatePrice_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                column: "WebsitePriceId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdvertisingSetObjects_ObjectGroups_ObjectId",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects",
                column: "ObjectId",
                principalSchema: "Service.Settings",
                principalTable: "ObjectGroups",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdvertisingSetObjects_ObjectGroups_ObjectId",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects");

            migrationBuilder.DropTable(
                name: "WebsiteTemplatePrice",
                schema: "Service.Settings");

            migrationBuilder.DropIndex(
                name: "IX_AdvertisingSetObjects_ObjectId",
                schema: "Service.Settings",
                table: "AdvertisingSetObjects");

            migrationBuilder.DropColumn(
                name: "PublisherPriceType",
                schema: "Service.Settings",
                table: "WebsitePrices");
        }
    }
}
