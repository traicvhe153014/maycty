﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class AddWebsiteAdvertisingSetInclude : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingSetWebsites_WebsiteId",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdvertisingSetWebsites_Websites_WebsiteId",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites",
                column: "WebsiteId",
                principalSchema: "Service.Settings",
                principalTable: "Websites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdvertisingSetWebsites_Websites_WebsiteId",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites");

            migrationBuilder.DropIndex(
                name: "IX_AdvertisingSetWebsites_WebsiteId",
                schema: "Service.Settings",
                table: "AdvertisingSetWebsites");
        }
    }
}
