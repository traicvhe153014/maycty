﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateCreatedAtAndModifiedAtCampaign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "Campaigns",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "Campaigns",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "AdvertisingSets",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "AdvertisingSets",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "Advertising",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "Advertising",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "AdvertisingSets");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "AdvertisingSets");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "Service.Settings",
                table: "Advertising");

            migrationBuilder.DropColumn(
                name: "ModifiedAt",
                schema: "Service.Settings",
                table: "Advertising");
        }
    }
}
