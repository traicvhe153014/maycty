﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class CreateWebsiteProhibitedCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "Sequence_D53EDCEEF193324FC72B4BC524A6DBAD",
                schema: "Service.Settings");

            migrationBuilder.AddColumn<bool>(
                
                name: "Active",
                schema: "Service.Settings",
                table: "Websites",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                schema: "Service.Settings",
                table: "Websites",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "WebsiteProhibitedCategories",
                schema: "Service.Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Search = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_D53EDCEEF193324FC72B4BC524A6DBAD"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ProhibitedCategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WebsiteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteProhibitedCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebsiteProhibitedCategories_Websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalSchema: "Service.Settings",
                        principalTable: "Websites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteProhibitedCategories_SubId",
                schema: "Service.Settings",
                table: "WebsiteProhibitedCategories",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteProhibitedCategories_WebsiteId",
                schema: "Service.Settings",
                table: "WebsiteProhibitedCategories",
                column: "WebsiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WebsiteProhibitedCategories",
                schema: "Service.Settings");

            migrationBuilder.DropSequence(
                name: "Sequence_D53EDCEEF193324FC72B4BC524A6DBAD",
                schema: "Service.Settings");

            migrationBuilder.DropColumn(
                name: "Active",
                schema: "Service.Settings",
                table: "Websites");

            migrationBuilder.DropColumn(
                name: "Status",
                schema: "Service.Settings",
                table: "Websites");
        }
    }
}
