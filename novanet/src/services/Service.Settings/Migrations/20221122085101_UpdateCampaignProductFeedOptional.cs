﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateCampaignProductFeedOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaigns_ProductFeeds_EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns");

            migrationBuilder.AlterColumn<Guid>(
                name: "EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<int>(
                name: "CampaignType",
                schema: "Service.Settings",
                table: "AdvertisingTemplates",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TemplatePosition",
                schema: "Service.Settings",
                table: "AdvertisingTemplates",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_Campaigns_ProductFeeds_EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns",
                column: "EcommerceProductFeedId",
                principalSchema: "Service.Settings",
                principalTable: "ProductFeeds",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaigns_ProductFeeds_EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "CampaignType",
                schema: "Service.Settings",
                table: "AdvertisingTemplates");

            migrationBuilder.DropColumn(
                name: "TemplatePosition",
                schema: "Service.Settings",
                table: "AdvertisingTemplates");

            migrationBuilder.AlterColumn<Guid>(
                name: "EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaigns_ProductFeeds_EcommerceProductFeedId",
                schema: "Service.Settings",
                table: "Campaigns",
                column: "EcommerceProductFeedId",
                principalSchema: "Service.Settings",
                principalTable: "ProductFeeds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
