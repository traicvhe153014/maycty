﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateWebsitePriceTemplateSequence : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteTemplatePrice_AdvertisingTemplates_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteTemplatePrice_WebsitePrices_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteTemplatePrice",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice");

            migrationBuilder.RenameTable(
                name: "WebsiteTemplatePrice",
                schema: "Service.Settings",
                newName: "WebsiteTemplatePrices",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_WebsiteTemplatePrice_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                newName: "IX_WebsiteTemplatePrices_WebsitePriceId");

            migrationBuilder.RenameIndex(
                name: "IX_WebsiteTemplatePrice_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                newName: "IX_WebsiteTemplatePrices_AdvertisingTemplateId");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_3CA3E3109101C6C1EF64B6ED9A2D268D",
                schema: "Service.Settings");

            migrationBuilder.AlterColumn<long>(
                name: "SubId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                type: "bigint",
                nullable: false,
                defaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_3CA3E3109101C6C1EF64B6ED9A2D268D",
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteTemplatePrices",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_WebsiteTemplatePrices_SubId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                column: "SubId");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteTemplatePrices_AdvertisingTemplates_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                column: "AdvertisingTemplateId",
                principalSchema: "Service.Settings",
                principalTable: "AdvertisingTemplates",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteTemplatePrices_WebsitePrices_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices",
                column: "WebsitePriceId",
                principalSchema: "Service.Settings",
                principalTable: "WebsitePrices",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteTemplatePrices_AdvertisingTemplates_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropForeignKey(
                name: "FK_WebsiteTemplatePrices_WebsitePrices_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteTemplatePrices",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropIndex(
                name: "IX_WebsiteTemplatePrices_SubId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrices");

            migrationBuilder.DropSequence(
                name: "Sequence_3CA3E3109101C6C1EF64B6ED9A2D268D",
                schema: "Service.Settings");

            migrationBuilder.RenameTable(
                name: "WebsiteTemplatePrices",
                schema: "Service.Settings",
                newName: "WebsiteTemplatePrice",
                newSchema: "Service.Settings");

            migrationBuilder.RenameIndex(
                name: "IX_WebsiteTemplatePrices_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                newName: "IX_WebsiteTemplatePrice_WebsitePriceId");

            migrationBuilder.RenameIndex(
                name: "IX_WebsiteTemplatePrices_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                newName: "IX_WebsiteTemplatePrice_AdvertisingTemplateId");

            migrationBuilder.AlterColumn<long>(
                name: "SubId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldDefaultValueSql: "NEXT VALUE FOR [Service.Settings].Sequence_3CA3E3109101C6C1EF64B6ED9A2D268D");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteTemplatePrice",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteTemplatePrice_AdvertisingTemplates_AdvertisingTemplateId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                column: "AdvertisingTemplateId",
                principalSchema: "Service.Settings",
                principalTable: "AdvertisingTemplates",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WebsiteTemplatePrice_WebsitePrices_WebsitePriceId",
                schema: "Service.Settings",
                table: "WebsiteTemplatePrice",
                column: "WebsitePriceId",
                principalSchema: "Service.Settings",
                principalTable: "WebsitePrices",
                principalColumn: "Id");
        }
    }
}
