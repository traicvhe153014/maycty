﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateProductEntityDeletedFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                schema: "Service.Settings",
                table: "ProductEntities",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                schema: "Service.Settings",
                table: "ProductGroupEntities");

            migrationBuilder.DropColumn(
                name: "Deleted",
                schema: "Service.Settings",
                table: "ProductEntities");

            migrationBuilder.DropColumn(
                name: "Deleted",
                schema: "Service.Settings",
                table: "ProductAttributeValues");
        }
    }
}
