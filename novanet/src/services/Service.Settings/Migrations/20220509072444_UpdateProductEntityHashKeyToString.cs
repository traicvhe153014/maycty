﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Settings.Migrations
{
    public partial class UpdateProductEntityHashKeyToString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ProductAttributeValueHashCode",
                schema: "Service.Settings",
                table: "ProductGroupEntities",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HashKey",
                schema: "Service.Settings",
                table: "ProductAttributeValues",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
