using Service.Settings.Domain.AggregateModels.TemporaryAggregate;

namespace Service.Settings.Domain;

public class SettingsContext : NovanetContext<SettingsContext>
{
    public static readonly string? Schema = typeof(SettingsContext).GetTypeInfo().Assembly.GetName().Name;

    public SettingsContext(DbContextOptions<SettingsContext> options, AppSettings appSettings)
        : base(options, appSettings)
    {
    }

    public DbSet<Advertising> Advertising { get; set; } = default!;

    public DbSet<AdvertisingAppliedTemplate> AdvertisingAppliedTemplates { get; set; } = default!;

    public DbSet<AdvertisingTemplateAttribute> AdvertisingTemplateAttributes { get; set; } = default!;

    public DbSet<AdvertisingTemplateAttributeValue> AdvertisingTemplateAttributeValues { get; set; } = default!;

    public DbSet<AdvertisingSet> AdvertisingSets { get; set; } = default!;

    public DbSet<AdvertisingSetAge> AdvertisingSetAges { get; set; } = default!;

    public DbSet<AdvertisingSetCategory> AdvertisingSetCategories { get; set; } = default!;
    
    public DbSet<AdvertisingSetGender> AdvertisingSetGenders { get; set; } = default!;

    public DbSet<AdvertisingSetLocation> AdvertisingSetLocations { get; set; } = default!;

    public DbSet<AdvertisingSetObject> AdvertisingSetObjects { get; set; } = default!;

    public DbSet<AdvertisingSetProductGroup> AdvertisingSetProductGroups { get; set; } = default!;

    public DbSet<AdvertisingSetProhibitedCategory> AdvertisingSetProhibitedCategories { get; set; } = default!;
    
    public DbSet<AdvertisingSetScheduling> AdvertisingSetScheduling { get; set; } = default!;

    public DbSet<AdvertisingSetWebsite> AdvertisingSetWebsites { get; set; } = default!;

    public DbSet<AdvertisingTemplate> AdvertisingTemplates { get; set; } = default!;

    public DbSet<Campaign> Campaigns { get; set; } = default!;
    public DbSet<CampaignScheduling> CampaignScheduling { get; set; } = default!;

    public DbSet<DomainAlias> DomainAliases { get; set; } = default!;

    public DbSet<ObjectGroup> ObjectGroups { get; set; } = default!;

    public DbSet<ObjectGroupWebsiteCondition> ObjectGroupWebsiteConditions { get; set; } = default!;
    
    public DbSet<ObjectGroupConsumerBehavior> ObjectGroupConsumerBehavior { get; set; } = default!;

    public DbSet<ProductEntity> ProductEntities { get; set; } = default!;

    public DbSet<ProductAttribute> ProductAttributes { get; set; } = default!;

    public DbSet<ProductAttributeValue> ProductAttributeValues { get; set; } = default!;

    public DbSet<ProductStatus> ProductStatus { get; set; } = default!;

    public DbSet<ProductFeed> ProductFeeds { get; set; } = default!;
    
    public DbSet<Payment> Payment { get; set; } = default!;

    public DbSet<ProductGroup> ProductGroups { get; set; } = default!;

    public DbSet<ProductGroupEntity> ProductGroupEntities { get; set; } = default!;

    public DbSet<Website> Websites { get; set; } = default!;

    public DbSet<WebsitePrice> WebsitePrices { get; set; } = default!;

    public DbSet<WebsitePriceCampaignType> WebsitePriceCampaignTypes { get; set; } = default!;

    public DbSet<WebsiteProhibitedCategory> WebsiteProhibitedCategories { get; set; } = default!;

    public DbSet<WebsiteTemplatePrice> WebsiteTemplatePrices { get; set; } = default!;

    public DbSet<Zone> Zones { get; set; } = default!;
    
    public DbSet<ZoneTemplate> ZoneTemplates { get; set; } = default!;

    public IQueryable<OrderedSubIdTempTable> OrderedSubIdTempTable => Set<OrderedSubIdTempTable>();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.HasDefaultSchema(Schema);
        new Advertising().ModelCreating<Advertising>(modelBuilder, Schema);
        new AdvertisingAppliedTemplate().ModelCreating<AdvertisingAppliedTemplate>(modelBuilder, Schema);
        new AdvertisingTemplateAttribute().ModelCreating<AdvertisingTemplateAttribute>(modelBuilder, Schema);
        new AdvertisingTemplateAttributeValue().ModelCreating<AdvertisingTemplateAttributeValue>(modelBuilder, Schema);
        new AdvertisingSet().ModelCreating<AdvertisingSet>(modelBuilder, Schema);
        new AdvertisingSetAge().ModelCreating<AdvertisingSetAge>(modelBuilder, Schema);
        new AdvertisingSetCategory().ModelCreating<AdvertisingSetCategory>(modelBuilder, Schema);
        new AdvertisingSetGender().ModelCreating<AdvertisingSetGender>(modelBuilder, Schema);
        new AdvertisingSetLocation().ModelCreating<AdvertisingSetLocation>(modelBuilder, Schema);
        new AdvertisingSetObject().ModelCreating<AdvertisingSetObject>(modelBuilder, Schema);
        new AdvertisingSetProductGroup().ModelCreating<AdvertisingSetProductGroup>(modelBuilder, Schema);
        new AdvertisingSetProhibitedCategory().ModelCreating<AdvertisingSetProhibitedCategory>(modelBuilder, Schema);
        new AdvertisingSetScheduling().ModelCreating<AdvertisingSetScheduling>(modelBuilder, Schema);
        new AdvertisingSetWebsite().ModelCreating<AdvertisingSetWebsite>(modelBuilder, Schema);
        new AdvertisingTemplate().ModelCreating<AdvertisingTemplate>(modelBuilder, Schema);
        new Campaign().ModelCreating<Campaign>(modelBuilder, Schema);
        new CampaignScheduling().ModelCreating<CampaignScheduling>(modelBuilder, Schema);
        new DomainAlias().ModelCreating<DomainAlias>(modelBuilder, Schema);
        new ObjectGroup().ModelCreating<ObjectGroup>(modelBuilder, Schema);
        new ObjectGroupWebsiteCondition().ModelCreating<ObjectGroupWebsiteCondition>(modelBuilder, Schema);
        new ObjectGroupConsumerBehavior().ModelCreating<ObjectGroupConsumerBehavior>(modelBuilder, Schema);
        new Payment().ModelCreating<Payment>(modelBuilder, Schema);
        new ProductEntity().ModelCreating<ProductEntity>(modelBuilder, Schema);
        new ProductAttribute().ModelCreating<ProductAttribute>(modelBuilder, Schema);
        new ProductAttributeValue().ModelCreating<ProductAttributeValue>(modelBuilder, Schema);
        new ProductStatus().ModelCreating<ProductStatus>(modelBuilder, Schema);
        new ProductFeed().ModelCreating<ProductFeed>(modelBuilder, Schema);
        new ProductGroup().ModelCreating<ProductGroup>(modelBuilder, Schema);
        new ProductGroupEntity().ModelCreating<ProductGroupEntity>(modelBuilder, Schema);
        new Website().ModelCreating<Website>(modelBuilder, Schema);
        new WebsitePrice().ModelCreating<WebsitePrice>(modelBuilder, Schema);
        new WebsitePriceCampaignType().ModelCreating<WebsitePriceCampaignType>(modelBuilder, Schema);
        new WebsiteProhibitedCategory().ModelCreating<WebsiteProhibitedCategory>(modelBuilder, Schema);
        new WebsiteTemplatePrice().ModelCreating<WebsiteTemplatePrice>(modelBuilder, Schema);
        new Zone().ModelCreating<Zone>(modelBuilder, Schema);
        new ZoneTemplate().ModelCreating<ZoneTemplate>(modelBuilder, Schema);

        modelBuilder.Entity<OrderedSubIdTempTable>(entity =>
        {
            entity.HasAlternateKey(x => x.Id);
            entity.ToView("#OrderedSubIdTempTable");
        });
    }
}