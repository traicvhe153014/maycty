﻿namespace Service.Settings.Domain.AggregateModels.DomainAliasAggregate;

public class DomainAlias : DomainAliasCore
{
    public Website Website { get; set; } = default!;
}