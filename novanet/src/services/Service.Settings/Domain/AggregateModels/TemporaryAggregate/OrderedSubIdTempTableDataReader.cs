﻿using System.Data;

namespace Service.Settings.Domain.AggregateModels.TemporaryAggregate;

public class OrderedSubIdTempTableDataReader : IDataReader
{
    private readonly IEnumerator<long> _enumerator;
    private int _currentIndex;
    private readonly bool _isDescending;

    public bool IsDBNull(int i)
    {
        throw new NotImplementedException();
    }

    public int FieldCount => 1;

    public OrderedSubIdTempTableDataReader(
        IEnumerable<long> values,
        int length,
        bool isDescending = true)
    {
        _enumerator = values.GetEnumerator();
        _currentIndex = length;
        _isDescending = isDescending;
    }

    public void Close()
    {
        throw new NotImplementedException();
    }

    public DataTable? GetSchemaTable()
    {
        throw new NotImplementedException();
    }

    public bool NextResult()
    {
        throw new NotImplementedException();
    }

    public bool Read()
    {
        return _enumerator.MoveNext();
    }

    public string GetString(int i)
    {
        throw new NotImplementedException();
    }

    public object GetValue(int i)
    {
        if (i == 0)
            return _enumerator.Current;
        if (i == 1)
        {
            if (_isDescending)
            {
                _currentIndex -= 1;
            }
            else
            {
                _currentIndex += 1;
            }
            return _currentIndex;
        }
        throw new ArgumentOutOfRangeException();
    }

    public int GetValues(object[] values)
    {
        throw new NotImplementedException();
    }

    public void Dispose()
    {
        _enumerator.Dispose();
    }

    // all other members throw NotImplementedException
    public int Depth => throw new NotImplementedException();
    public bool IsClosed { get; }
    public int RecordsAffected { get; }
    public object this[int i] => throw new NotImplementedException();

    public object this[string name] => throw new NotImplementedException();

    public bool GetBoolean(int i) => throw new NotImplementedException();
    public byte GetByte(int i)
    {
        throw new NotImplementedException();
    }

    public long GetBytes(int i, long fieldOffset, byte[]? buffer, int bufferoffset, int length)
    {
        throw new NotImplementedException();
    }

    public char GetChar(int i)
    {
        throw new NotImplementedException();
    }

    public long GetChars(int i, long fieldoffset, char[]? buffer, int bufferoffset, int length)
    {
        throw new NotImplementedException();
    }

    public IDataReader GetData(int i)
    {
        throw new NotImplementedException();
    }

    public string GetDataTypeName(int i)
    {
        throw new NotImplementedException();
    }

    public DateTime GetDateTime(int i)
    {
        throw new NotImplementedException();
    }

    public decimal GetDecimal(int i)
    {
        throw new NotImplementedException();
    }

    public double GetDouble(int i)
    {
        throw new NotImplementedException();
    }

    public Type GetFieldType(int i)
    {
        throw new NotImplementedException();
    }

    public float GetFloat(int i)
    {
        throw new NotImplementedException();
    }

    public Guid GetGuid(int i)
    {
        throw new NotImplementedException();
    }

    public short GetInt16(int i)
    {
        throw new NotImplementedException();
    }

    public int GetInt32(int i)
    {
        throw new NotImplementedException();
    }

    public long GetInt64(int i)
    {
        throw new NotImplementedException();
    }

    public string GetName(int i)
    {
        throw new NotImplementedException();
    }

    public int GetOrdinal(string name)
    {
        throw new NotImplementedException();
    }
    
  
}