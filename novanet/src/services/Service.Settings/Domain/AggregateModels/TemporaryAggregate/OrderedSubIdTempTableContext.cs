﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Service.Settings.Domain.AggregateModels.TemporaryAggregate;

public class OrderedSubIdTempTableContext
{
    private readonly SettingsContext _context;

    public OrderedSubIdTempTableContext(SettingsContext context)
    {
        _context = context;
    }

    private string GetCreateTableSql(
        bool createPk)
    {
        var sqlGenHelper = _context.GetService<ISqlGenerationHelper>();

        var entity = _context.Model.FindEntityType(typeof(OrderedSubIdTempTable));
        var tableName = entity!.GetViewName();
        var escapedTableName = sqlGenHelper.DelimitIdentifier(tableName!);

        var idProperty = entity!.FindProperty(nameof(OrderedSubIdTempTable.Id));
        var columnName = idProperty!.GetColumnName();
        var escapedColumnName = sqlGenHelper.DelimitIdentifier(columnName);
        var columnType = idProperty!.GetColumnType();
        var nullability = idProperty!.IsNullable ? "NULL" : "NOT NULL";

        var pkSql = createPk ? $", PRIMARY KEY ({escapedColumnName})" : null;
        
        var valueProperty = entity.FindProperty(nameof(OrderedSubIdTempTable.Value));
        var valueColumnName = valueProperty!.GetColumnName();
        var escapedValueColumnName = sqlGenHelper.DelimitIdentifier(valueColumnName);
        var valueColumnType = valueProperty!.GetColumnType();

        var sql = $@"
            CREATE TABLE {escapedTableName}
            (
               {escapedColumnName} {columnType} {nullability}
               ,{escapedValueColumnName} {valueColumnType}
               {pkSql}
            );";
        return sql;
    }
    
    public async Task CreateTableAsync(
        bool createPk,
        CancellationToken cancellationToken = default)
    {
        var sql = GetCreateTableSql(createPk);
        await _context.Database.OpenConnectionAsync(cancellationToken);
        try
        {
            await _context.Database.ExecuteSqlRawAsync(sql, cancellationToken);
        }
        catch (Exception)
        {
            await _context.Database.CloseConnectionAsync();
            throw;
        }
    }
    
    private SqlBulkCopy GetSqlBulkCopy()
    {
        var sqlGenHelper = _context.GetService<ISqlGenerationHelper>();

        var sqlCon = (SqlConnection)_context.Database.GetDbConnection();
        var sqlTx = (SqlTransaction?)_context.Database.CurrentTransaction?.GetDbTransaction();

        var entity = _context.Model.FindEntityType(typeof(OrderedSubIdTempTable));
        var tableName = entity!.GetViewName();
        var escapedTableName = sqlGenHelper.DelimitIdentifier(tableName!);

        var idProperty = entity!.FindProperty(nameof(OrderedSubIdTempTable.Id));
        var idColumnName = idProperty!.GetColumnName();
        
        var valueProperty = entity.FindProperty(nameof(OrderedSubIdTempTable.Value));
        var valueColumnName = valueProperty!.GetColumnName();

        return new SqlBulkCopy(sqlCon, SqlBulkCopyOptions.Default, sqlTx)
        {
            DestinationTableName = escapedTableName,
            ColumnMappings =
            {
                new SqlBulkCopyColumnMapping(0, idColumnName),
                new SqlBulkCopyColumnMapping(1, valueColumnName)
            }
        };
    }
    
    public async Task BulkInsertAsync(
        IEnumerable<long> values,
        int length,
        bool isDescending,
        CancellationToken cancellationToken = default)
    {
        using var bulkCopy = GetSqlBulkCopy();
        await _context.Database.OpenConnectionAsync(cancellationToken);

        try
        {
            using var reader = new OrderedSubIdTempTableDataReader(values, length, isDescending);
            await bulkCopy.WriteToServerAsync(reader, cancellationToken);
        }
        finally
        {
            await _context.Database.CloseConnectionAsync();
        }
    }
}