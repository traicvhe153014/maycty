﻿namespace Service.Settings.Domain.AggregateModels.TemporaryAggregate;

public class OrderedSubIdTempTable
{
    public long Id { get; set; }
    public int Value { get; set; }
}