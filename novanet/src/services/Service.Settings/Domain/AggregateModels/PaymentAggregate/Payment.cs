﻿namespace Service.Settings.Domain.AggregateModels.PaymentAggregate;

public class Payment: PaymentCore
{
    public string Email { get; set; }

    public string? CustomerName { get; set; }

    public double Amount { get; set; }

    public string? Note { get; set; }
}