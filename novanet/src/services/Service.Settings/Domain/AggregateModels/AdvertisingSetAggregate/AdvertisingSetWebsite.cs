﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetWebsite : AdvertisingSetWebsiteCore
{
    [JsonIgnore]
    [ForeignKey(nameof(AdvertisingSetId))]
    public AdvertisingSet AdvertisingSet { get; set; } = default!;
    
    
    public override void ModelCreating<T>(ModelBuilder modelBuilder, string? schema)
    {
        base.ModelCreating<T>(modelBuilder, schema);
        modelBuilder.Entity<AdvertisingSetWebsite>()
            .HasOne(x => x.AdvertisingSet)
            .WithMany(x => x.AdvertisingSetWebsites)
            .HasForeignKey(x => x.AdvertisingSetId)
            .OnDelete(DeleteBehavior.NoAction);
        modelBuilder.Entity<AdvertisingSetWebsite>()
            .HasOne(x => x.AdvertisingSet)
            .WithMany(x => x.AdvertisingSetWebsites)
            .HasForeignKey(x => x.AdvertisingSetId)
            .OnDelete(DeleteBehavior.NoAction);
    }
}