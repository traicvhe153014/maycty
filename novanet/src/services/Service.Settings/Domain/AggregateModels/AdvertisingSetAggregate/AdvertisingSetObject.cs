﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetObject : AdvertisingSetObjectCore
{
    [JsonIgnore]
    [ForeignKey(nameof(AdvertisingSetId))] 
    public AdvertisingSet? AdvertisingSet { get; set; }
    
    [JsonIgnore]
    [ForeignKey(nameof(ObjectId))] 
    public ObjectGroup? ObjectGroup { get; set; }
}