﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSet : AdvertisingSetCore, IMapFrom<AdvertisingSetCore>
{
    [ForeignKey(nameof(CampaignId))] public Campaign? Campaign { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetProductGroup>? AdvertisingSetProductGroups { get; set; }

    [JsonIgnore]
    public List<AdvertisingSetObject>? AdvertisingSetObjects { get; set; }

    [JsonIgnore]
    public List<AdvertisingSetLocation>? AdvertisingSetLocations { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetCategory>? AdvertisingSetCategories { get; set; }

    [JsonIgnore]
    public List<AdvertisingSetAge>? AdvertisingSetAges { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetGender>? AdvertisingSetGenders { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetProhibitedCategory>? AdvertisingSetProhibitedCategories { get; set; }
    
    [JsonIgnore]
    public List<WebsitePrice>? WebsitePrices { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetScheduling>? AdvertisingSetScheduling { get; set; }
        
    [JsonIgnore] 
    public List<AdvertisingSetWebsite>? AdvertisingSetWebsites { get; set; }
    
    [JsonIgnore]
    public List<Advertising>? Advertisings { get; set; }
}