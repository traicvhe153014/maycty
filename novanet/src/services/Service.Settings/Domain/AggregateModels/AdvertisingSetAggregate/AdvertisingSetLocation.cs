﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetLocation : AdvertisingSetLocationCore
{
    [JsonIgnore]
    [ForeignKey(nameof(AdvertisingSetId))]
    public AdvertisingSet? AdvertisingSet { get; set; }
}