﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetProductGroup : AdvertisingSetProductGroupCore
{
    [JsonIgnore]
    [ForeignKey(nameof(AdvertisingSetId))]
    public AdvertisingSet? AdvertisingSet { get; set; }

    [ForeignKey(nameof(ProductGroupId))] public ProductGroup? ProductGroup { get; set; }
}