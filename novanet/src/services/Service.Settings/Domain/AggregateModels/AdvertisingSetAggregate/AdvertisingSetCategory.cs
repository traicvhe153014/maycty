using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetCategory : AdvertisingSetCategoryCore
{
    [JsonIgnore]
    [ForeignKey(nameof(AdvertisingSetId))]
    public AdvertisingSet? AdvertisingSet { get; set; }
}