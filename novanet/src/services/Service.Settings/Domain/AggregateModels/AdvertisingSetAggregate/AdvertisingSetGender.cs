﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetGender : AdvertisingSetGenderCore
{
    [JsonIgnore]
    [ForeignKey(nameof(AdvertisingSetId))]
    public AdvertisingSet? AdvertisingSet { get; set; }
}