﻿
namespace Service.Settings.Domain.AggregateModels.AdvertisingSetAggregate;

public class AdvertisingSetScheduling : AdvertisingSetSchedulingCore
{
    [ForeignKey(nameof(AdvertisingSetId))] public AdvertisingSet? AdvertisingSet { get; set; }
}