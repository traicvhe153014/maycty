﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ProductFeedAggregate;

public class ProductFeed : ProductFeedCore
{
    [JsonIgnore]
    public List<ProductEntity> ProductEntities { get; set; } = default!;

    public ICollection<ProductStatus> ProductStatus { get; set; } = default!;
    public ICollection<Campaign> Campaigns { get; set; } = default!;


    public void Update(string sourcePath, DateTimeOffset createdAt, List<ProductEntity> productEntities)
    {
        SourcePath = sourcePath;
        CreatedAt = createdAt;
        ProductEntities = productEntities;
    }
}