﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ProductAggregate;

public class ProductAttribute : ProductAttributeCore, IMapFrom<ProductAttributeCore>
{
    [JsonIgnore]
    public ICollection<ProductAttributeValue> AttributeValues { get; set; } = default!;
}