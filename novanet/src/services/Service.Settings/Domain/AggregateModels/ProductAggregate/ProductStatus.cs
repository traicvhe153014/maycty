﻿using Novanet.Core.Domain;

namespace Service.Settings.Domain.AggregateModels.ProductAggregate;

public class ProductStatus : NovanetDocument
{
    public bool Success { get; set; }

    public string? ErrorMessage { get; set; }

    public string? ErrorType { get; set; }

    public string? SheetPosition { get; set; }

    public string? ProductIdValue { get; set; }

    public Guid? ProductEntityId { get; set; }

    public Guid? ProductAttributeValueId { get; set; }

    public Guid? ProductFeedId { get; set; }

    [ForeignKey(nameof(ProductEntityId))] public ProductEntity? ProductEntity { get; set; }

    [ForeignKey(nameof(ProductAttributeValueId))]
    public ProductAttributeValue? ProductAttributeValue { get; set; }

    [ForeignKey(nameof(ProductFeedId))] public ProductFeed? ProductFeed { get; set; }
}