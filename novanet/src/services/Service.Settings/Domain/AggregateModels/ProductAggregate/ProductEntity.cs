﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ProductAggregate;

public class ProductEntity : ProductEntityCore
{
    [JsonIgnore]
    public List<ProductAttributeValue>? AttributeValues { get; set; }

    [ForeignKey(nameof(ProductFeedId))]
    public ProductFeed ProductFeed { get; set; } = default!;

    [JsonIgnore]
    public ICollection<ProductStatus>? ProductStatus { get; set; }
    
    [JsonIgnore]
    public ICollection<ProductGroupEntity>? ProductGroupEntities { get; set; }
}