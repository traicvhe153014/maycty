﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ProductAggregate;

public class ProductAttributeValue : ProductAttributeValueCore
{
    [ForeignKey(nameof(ProductId))] public ProductEntity ProductEntity { get; set; } = default!;

    [ForeignKey(nameof(AttributeId))] public ProductAttribute ProductAttribute { get; set; } = default!;

    [JsonIgnore] public ICollection<ProductStatus>? ProductStatus { get; set; } = default!;

    [JsonIgnore] public ICollection<ProductGroupEntity>? ProductGroupEntities { get; set; } = default!;

    [NotMapped] public List<string>? SupportedValues { get; set; }

    public override void ModelCreating<T>(ModelBuilder modelBuilder, string schema)
    {
        base.ModelCreating<T>(modelBuilder, schema);
        modelBuilder.Entity<ProductAttributeValue>()
            .Property(x => x.HashKey)
            .HasMaxLength(250);
        modelBuilder.Entity<ProductAttributeValue>()
            .HasIndex(x => x.HashKey);
    }
}