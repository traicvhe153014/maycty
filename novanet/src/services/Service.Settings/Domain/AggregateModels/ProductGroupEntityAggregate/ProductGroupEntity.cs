using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ProductGroupEntityAggregate;

public class ProductGroupEntity : ProductGroupEntityCore
{
    [JsonIgnore]
    [ForeignKey(nameof(ProductGroupId))]
    public ProductGroup ProductGroup { get; set; } = default!;

    [JsonIgnore]
    [ForeignKey(nameof(ProductAttributeValueId))]
    public ProductAttributeValue ProductAttributeValue { get; set; } = default!;

    [JsonIgnore]
    [ForeignKey(nameof(ProductId))]
    public ProductEntity? ProductEntity { get; set; }
    
        
    public override void ModelCreating<T>(ModelBuilder modelBuilder, string? schema)
    {
        base.ModelCreating<T>(modelBuilder, schema);
        modelBuilder.Entity<ProductGroupEntity>()
            .HasOne(x => x.ProductGroup)
            .WithMany(x => x.ProductGroupEntities)
            .HasForeignKey(x => x.ProductGroupId)
            .OnDelete(DeleteBehavior.NoAction);
        modelBuilder.Entity<ProductGroupEntity>()
            .HasOne(x => x.ProductEntity)
            .WithMany(x => x.ProductGroupEntities)
            .HasForeignKey(x => x.ProductId)
            .OnDelete(DeleteBehavior.NoAction);
        modelBuilder.Entity<ProductGroupEntity>()
            .Property(x => x.ProductAttributeValueHashCode)
            .HasMaxLength(250);
        modelBuilder.Entity<ProductGroupEntity>()
            .HasIndex(x => x.ProductAttributeValueHashCode);
    }
}