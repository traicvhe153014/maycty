﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ObjectGroupAggregate;

public class ObjectGroupConsumerBehavior : ObjectGroupConsumerBehaviorCore
{
    [JsonIgnore]
    [ForeignKey(nameof(ObjectGroupId))] public ObjectGroup? ObjectGroup { get; set; }
}