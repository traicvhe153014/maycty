﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ObjectGroupAggregate;

public class ObjectGroupWebsiteCondition : ObjectGroupWebsiteConditionCore
{
    [JsonIgnore]
    [ForeignKey(nameof(ObjectGroupId))] public ObjectGroup? ObjectGroup { get; set; }
}