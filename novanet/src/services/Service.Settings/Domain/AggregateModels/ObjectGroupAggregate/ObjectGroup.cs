﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ObjectGroupAggregate;

public class ObjectGroup : ObjectGroupCore
{
    public List<ObjectGroupWebsiteCondition>? WebsiteConditionsCore { get; set; }
    
    public List<ObjectGroupConsumerBehavior>? ConsumerBehaviorsCore { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetObject>? AdvertisingSetObjects { get; set; }

    public void Update(string name) {
        Name = !string.IsNullOrEmpty(name) ? name : Name;
    }
    
    public void UpdateModifiedAt()
    {
        ModifiedAt = DateTimeOffset.Now;
    }
}