using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingTemplateAggregate;

public class AdvertisingTemplate : AdvertisingTemplateCore
{
    [JsonIgnore]
    public List<AdvertisingAppliedTemplate>? AdvertisingAppliedTemplates { get; set; }
    
    [JsonIgnore]
    public List<ZoneTemplate>? ZoneTemplates { get; set; }
    
    [JsonIgnore]
    public List<WebsiteTemplatePrice>? WebsiteTemplatePrices { get; set; }

    public AdvertisingTemplate()
    {
    }

    public AdvertisingTemplate(
        string name,
        int width,
        int height,
        TemplateType templateType)
    {
        Name = name;
        Width = width;
        Height = height;
        TemplateType = templateType;
    }
}