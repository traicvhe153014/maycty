﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ZoneAggregate;

public class ZoneTemplate : ZoneTemplateCore
{
    [ForeignKey(nameof(ZoneId))]
    public Zone? Zone { get; set; }
    
    [ForeignKey(nameof(AdvertisingTemplateId))]
    public AdvertisingTemplate? AdvertisingTemplate { get; set; }
    
    [JsonIgnore]
    public List<WebsiteTemplatePrice>? WebsiteTemplatePrices { get; set; }
}