﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ZoneAggregate;

public class Zone : ZoneCore
{
    [ForeignKey(nameof(WebsiteId))] public Website? Website { get; set; }
    
    [JsonIgnore]
    public List<ZoneTemplate>? ZoneTemplates { get; set; }

    public void Update(string code)
    {
        Code = code;
    }
}