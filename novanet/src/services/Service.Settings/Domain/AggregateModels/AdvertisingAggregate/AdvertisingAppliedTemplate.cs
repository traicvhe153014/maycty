﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingAggregate;

public class AdvertisingAppliedTemplate : AdvertisingAppliedTemplateCore
{
    [ForeignKey(nameof(AdvertisingId))]
    public Advertising? Advertising { get; set; }
    
    [ForeignKey(nameof(AdvertisingTemplateId))]
    public AdvertisingTemplate? AdvertisingTemplate { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingTemplateAttributeValue>? AdvertisingTemplateAttributeValues { get; set; }
}