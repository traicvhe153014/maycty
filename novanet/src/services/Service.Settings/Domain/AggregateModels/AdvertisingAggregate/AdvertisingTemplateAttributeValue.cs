﻿namespace Service.Settings.Domain.AggregateModels.AdvertisingAggregate;

public class AdvertisingTemplateAttributeValue : AdvertisingTemplateAttributeValueCore
{
    [ForeignKey(nameof(AdvertisingAppliedTemplateId))]
    public AdvertisingAppliedTemplate? AdvertisingAppliedTemplate { get; set; }

    [ForeignKey(nameof(AdvertisingTemplateAttributeId))]
    public AdvertisingTemplateAttribute? AdvertisingTemplateAttribute { get; set; }
}