﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingAggregate;

public class Advertising : AdvertisingCore
{
    [ForeignKey(nameof(AdvertisingSetId))] public AdvertisingSet? AdvertisingSet { get; set; }

    [JsonIgnore] public List<AdvertisingAppliedTemplate>? AdvertisingAppliedTemplates { get; set; }
}