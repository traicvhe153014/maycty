﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.AdvertisingAggregate;

public class AdvertisingTemplateAttribute : AdvertisingTemplateAttributeCore
{
    [JsonIgnore]
    public List<AdvertisingTemplateAttributeValue>? AdvertisingTemplateAttributeValues { get; set; }
}