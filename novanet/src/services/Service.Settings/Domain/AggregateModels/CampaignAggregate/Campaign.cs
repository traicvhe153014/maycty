﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.CampaignAggregate;

public class Campaign : CampaignCore, IMapFrom<CampaignCore>
{
    [JsonIgnore]
    public List<CampaignScheduling>? CampaignScheduling { get; set; }

    [JsonIgnore]
    public List<AdvertisingSet>? AdvertisingSets { get; set; }

    [ForeignKey(nameof(EcommerceProductFeedId))]
    [JsonIgnore]
    public ProductFeed? EcommerceProductFeed { get; set; } = default!;
    
    [JsonIgnore]
    public List<Advertising>? Advertisings { get; set; }
}