﻿namespace Service.Settings.Domain.AggregateModels.CampaignAggregate;

public class CampaignScheduling : CampaignSchedulingCore
{
    [ForeignKey(nameof(CampaignId))] public Campaign? Campaign { get; set; }
}