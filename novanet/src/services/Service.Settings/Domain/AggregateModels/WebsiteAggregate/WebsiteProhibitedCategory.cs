using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.WebsiteAggregate;

public class WebsiteProhibitedCategory : WebsiteProhibitedCategoryCore
{
    [JsonIgnore]
    [ForeignKey(nameof(WebsiteId))]
    public Website Website { get; set; } = default!;
}