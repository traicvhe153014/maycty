﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.WebsiteAggregate;

public class Website : WebsiteCore
{
    [JsonIgnore]
    public List<Zone>? Zones { get; set; }

    [JsonIgnore]
    public List<DomainAlias>? DomainAliases { get; set; }
    
    [JsonIgnore]
    public List<WebsitePrice>? WebsitePrices { get; set; }
    
    [JsonIgnore]
    public List<WebsiteProhibitedCategory>? WebsiteProhibitedCategories { get; set; }
    
    [JsonIgnore]
    public List<AdvertisingSetWebsite>? AdvertisingSetWebsites { get; set; }

    // public override void ModelCreating<T>(ModelBuilder modelBuilder, string? schema)
    // {
    //     ((Novanet.Core.Domain.NovanetDocument) this).ModelCreating<T>(modelBuilder, schema);
    //     modelBuilder.Entity<Website>().Ignore(x => x.Publisher);
    // }
}