﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.WebsiteAggregate;

public class WebsitePriceCampaignType : WebsitePriceCampaignTypeCore
{
    [ForeignKey(nameof(WebsitePriceId))]
    public WebsitePrice WebsitePrice { get; set; } = default!;
}