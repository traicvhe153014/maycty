﻿using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.WebsiteAggregate;

public class WebsitePrice : WebsitePriceCore
{
    [ForeignKey(nameof(ZoneId))] public virtual Zone? Zone { get; set; }
    
    [ForeignKey(nameof(WebsiteId))] public Website? Website { get; set; }

    [ForeignKey(nameof(AdvertisingSetId))] public virtual AdvertisingSet? AdvertisingSet { get; set; }
    
    [JsonIgnore]
    public List<WebsiteTemplatePrice>? WebsiteTemplatePrices { get; set; }

    [JsonIgnore]
    public List<WebsitePriceCampaignType>? WebsitePriceCampaignTypes { get; set; }
}