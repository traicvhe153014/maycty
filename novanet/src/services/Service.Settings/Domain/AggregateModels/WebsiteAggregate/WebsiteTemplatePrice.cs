﻿namespace Service.Settings.Domain.AggregateModels.WebsiteAggregate;

public class WebsiteTemplatePrice : WebsiteTemplatePriceCore
{
    [ForeignKey(nameof(WebsitePriceId))]
    public WebsitePrice? WebsitePrice { get; set; }
    
    [ForeignKey(nameof(AdvertisingTemplateId))]
    public AdvertisingTemplate? AdvertisingTemplate { get; set; }
    
    [ForeignKey(nameof(ZoneTemplateId))]
    public ZoneTemplate? ZoneTemplate { get; set; }
}