using System.Text.Json.Serialization;

namespace Service.Settings.Domain.AggregateModels.ProductGroupAggregate;

public class ProductGroup : ProductGroupCore
{
    [JsonIgnore]
    [ForeignKey(nameof(ProductFeedId))]
    public ProductFeed ProductFeed { get; set; } = default!;

    [JsonIgnore]
    [ForeignKey(nameof(ProductAttributeId))]
    public ProductAttribute ProductAttribute { get; set; } = default!;
    
    [JsonIgnore]
    public ICollection<ProductGroupEntity>? ProductGroupEntities { get; set; } = default!;
    
    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset ModifiedAt { get; set; } = DateTimeOffset.Now;
    
    [JsonIgnore]
    public List<AdvertisingSetProductGroup> AdvertisingSetProductGroups { get; set; }
}