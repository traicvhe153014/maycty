﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ProductAttributeValueController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.ProductAttributeValue, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListProductAttributeValueQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
}