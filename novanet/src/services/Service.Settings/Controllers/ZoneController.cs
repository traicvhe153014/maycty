﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ZoneController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.Zone, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListZoneQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Zone, nameof(Create))]
    public async Task<IActionResult> Create(CreateZoneCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Zone, nameof(Update))]
    public async Task<IActionResult> Update(UpdateZoneCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}