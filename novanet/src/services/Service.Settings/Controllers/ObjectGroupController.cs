﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ObjectGroupController : SettingsController
{
    private readonly IMediator _mediator;

    public ObjectGroupController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.ObjectGroup, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListObjectGroupQuery query)
    {
        return Ok(await _mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.ObjectGroup, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetObjectGroupById query)
    {
        return Ok(await _mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.ObjectGroup, nameof(Create))]
    public async Task<IActionResult> Create(CreateObjectGroupCommand command)
    {
        return Ok(await _mediator.Send(command));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.ObjectGroup, nameof(Update))]
    public async Task<IActionResult> Update(UpdateObjectGroupCommand command)
    {
        return Ok(await _mediator.Send(command));
    }
}