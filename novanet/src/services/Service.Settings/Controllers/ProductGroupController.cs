﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ProductGroupController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.ProductGroup, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListProductGroupQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.ProductGroup, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetProductGroupQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.ProductGroup, nameof(Create))]
    public async Task<IActionResult> Create(CreateProductGroupCommand query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.ProductGroup, nameof(Update))]
    public async Task<IActionResult> Update(UpdateProductGroupCommand query)
    {
        return Ok(await Mediator.Send(query));
    }
}