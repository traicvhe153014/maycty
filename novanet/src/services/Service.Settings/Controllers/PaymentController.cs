﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class PaymentController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.Payment, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListTransactionQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Payment, nameof(Create))]
    public async Task<IActionResult> Create(CreateTransactionCommand query)
    {
        return Ok(await Mediator.Send(query));
    }
}