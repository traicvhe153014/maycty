﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ProductAttributeController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.ProductAttribute, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListProductAttributeQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
}