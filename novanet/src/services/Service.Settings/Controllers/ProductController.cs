﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ProductController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.Product, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListProductQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Product, nameof(Overview))]
    public async Task<IActionResult> Overview()
    {
        return Ok(await Mediator.Send(new GetProductOverview()));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Product, nameof(Update))]
    public async Task<IActionResult> Update(UpdateProductCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPost]
    [AllowAnonymous]
    public async Task<FileInformation> ExportDataToExcel(ExportProductToExcelCommand command)
    {
        var response = await Mediator.Send(command);

        return new FileInformation
        {
            FileName = response.Data.FileName,
            ResponseContent = response.Data.Content.ToArray()
        };
    }
}