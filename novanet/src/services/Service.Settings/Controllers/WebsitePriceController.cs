﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class WebsitePriceController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.WebsitePrice, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListWebsitePriceQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.WebsitePrice, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetWebsitePriceQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
    
    [HttpPost]
    [NovanetAccessControl(Aggregates.WebsitePrice, nameof(Create))]
    public async Task<IActionResult> Create(CreateWebsitePriceCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.WebsitePrice, nameof(Update))]
    public async Task<IActionResult> Update(UpdateWebsitePriceCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [AllowAnonymous]
    public async Task<IActionResult> Sync(SyncWebsitePriceCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}