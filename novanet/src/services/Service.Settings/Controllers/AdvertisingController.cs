﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class AdvertisingController : SettingsController
{
    [HttpPost]
    [NovanetAccessControl(Aggregates.Advertising, nameof(List))]
    public async Task<IActionResult> List([FromBody] ListAdvertisingQuery command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Advertising, nameof(ListTemplate))]
    public async Task<IActionResult> ListTemplate([FromQuery] ListAdvertisingTemplateQuery command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Advertising, nameof(ListTemplate))]
    public async Task<IActionResult> Get([FromQuery] GetAdvertisingByIdQuery command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.Advertising, nameof(ReportTemplate))]
    public async Task<IActionResult> ReportTemplate([FromQuery] GetAdvertisingTemplateReportQuery command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Advertising, nameof(Create))]
    public async Task<IActionResult> Create([FromBody] CreateAdvertisingCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.Advertising, nameof(Update))]
    public async Task<IActionResult> Update(UpdateAdvertisingCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}