﻿using Novanet.Core.Authorize;
using Novanet.EventBus;

namespace Service.Settings.Controllers;

public class CampaignController : SettingsController
{
    
    private readonly IMessageBusClient _messageBusClient;

    public CampaignController(IMessageBusClient messageBusClient)
    {
        _messageBusClient = messageBusClient;
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Campaign, nameof(List))]
    public async Task<IActionResult> List(ListCampaignQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Campaign, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetCampaignQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Campaign, nameof(Create))]
    public async Task<IActionResult> Create(CreateCampaignCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPost]
    [AllowAnonymous]
    public async Task<FileInformation> ExportDataToExcel(ExportCampaignToExcelCommand command)
    {
        var response = await Mediator.Send(command);

        return new FileInformation
        {
            FileName = response.Data.FileName,
            ResponseContent = response.Data.Content.ToArray()
        };
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.Campaign, nameof(Update))]
    public async Task<IActionResult> Update(UpdateCampaignCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}