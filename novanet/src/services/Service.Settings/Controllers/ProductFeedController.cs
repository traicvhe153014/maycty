﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class ProductFeedController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.ProductFeed, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListProductFeedQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.ProductFeed, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetProductFeedQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Any()
    {
        return Ok(await Mediator.Send(new CheckHasAnyProductFeedQuery()));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.ProductFeed, nameof(Create))]
    public async Task<IActionResult> Create(CreateProductFeedCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.ProductFeed, nameof(Update))]
    public async Task<IActionResult> Update(UpdateProductFeedCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.ProductFeed, nameof(Refresh))]
    public async Task<IActionResult> Refresh(RefreshProductFeedsCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpDelete]
    [NovanetAccessControl(Aggregates.ProductFeed, nameof(Delete))]
    public async Task<IActionResult> Delete([FromBody] DeleteProductFeedsCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}