﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class AdvertisingSetController : SettingsController
{
    [HttpPost]
    [NovanetAccessControl(Aggregates.AdvertisingSet, nameof(List))]
    public async Task<IActionResult> List([FromBody] ListAdvertisingSetQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.AdvertisingSet, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetAdvertisingSetQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.AdvertisingSet, nameof(Create))]
    public async Task<IActionResult> Create([FromBody] CreateAdvertisingSetCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.AdvertisingSet, nameof(Update))]
    public async Task<IActionResult> Update(UpdateAdvertisingSetCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    // TODO: Merge to update API
    [HttpPut]
    [NovanetAccessControl(Aggregates.AdvertisingSet, nameof(Update))]
    public async Task<IActionResult> UpdateProductGroup(UpdateAdvertisingSetProductGroupCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}