namespace Service.Settings.Controllers;

[ApiController]
[Route("settings/health")]
public class HealthController : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> Health()
    {
        return Ok(await Task.FromResult("OK"));
    }
}