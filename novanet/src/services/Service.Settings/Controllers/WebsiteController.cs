﻿using Novanet.Core.Authorize;

namespace Service.Settings.Controllers;

public class WebsiteController : SettingsController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.Website, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListWebsiteQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Website, nameof(Create))]
    public async Task<IActionResult> Create([FromBody] CreateWebsiteCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Website, nameof(Update))]
    public async Task<IActionResult> Update([FromBody] UpdateWebsiteCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [AllowAnonymous]
    public async Task<IActionResult> BatchUpdatePublisher([FromBody] BatchUpdatePublisherWebsiteCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Website, nameof(Update))]
    public async Task<IActionResult> UpdateProhibitedCategory([FromBody] UpdateWebsiteProhibitedCategoryCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpGet]
    [NovanetAccessControl(Aggregates.Website, nameof(List))]
    public async Task<IActionResult> ListDomainAliases([FromQuery] ListDomainAliasesQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Website, nameof(Update))]
    public async Task<IActionResult> UpdateDomainAlias([FromBody] UpdateDomainAliasesCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpDelete]
    [NovanetAccessControl(Aggregates.Website, nameof(Update))]
    public async Task<IActionResult> DeleteDomainAlias([FromBody] DeleteDomainAliasesCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPost]
    [NovanetAccessControl(Aggregates.Website, nameof(Create))]
    public async Task<IActionResult> CreateDomainAlias([FromBody] CreateDomainAliasesCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
}