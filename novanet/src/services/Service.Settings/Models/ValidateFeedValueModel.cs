﻿namespace Service.Settings.Models;

public class ValidateFeedValueModel
{
    public string FieldName { get; set; } = default!;

    public List<string> Errors { get; set; } = default!;
}