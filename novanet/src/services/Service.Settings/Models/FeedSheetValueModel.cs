﻿namespace Service.Settings.Models;

public class FeedSheetValueModel
{
    public string? Position { get; set; }
    
    public object? Value { get; set; }
}