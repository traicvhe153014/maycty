﻿using System.Text;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Novanet.Core.Exceptions;
using Novanet.Core.Models;
using Novanet.Core.RedisCache;
using Novanet.Core.Utils;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using Service.Settings.Models;

namespace Service.Settings.Services;

public interface IProductFeedService
{
    Task<string> GetFirstSheetName(string sheet);
    Task<List<Dictionary<string, FeedSheetValueModel>>> ListAsync(string sheetId);

    Task ProcessGoogleSheet(Guid productFeedId, Guid? currentUserId, bool isAdmin);
}

public class ProductFeedService : IProductFeedService
{
    private readonly AppSettings _appSettings;
    private readonly SettingsContext _context;
    private readonly IGlobalCacheService _globalCacheService;
    private readonly IMediator _mediator;
    private readonly IMessageBusClient _messageBusClient;
    private readonly IProductGroupService _productGroupService;
    private readonly string[] _scopes = { SheetsService.Scope.Spreadsheets };
    private readonly IServiceScopeFactory _serviceScopeFactory;

    private SheetsService _sheetsService;

    public ProductFeedService(
        SheetsService sheetsService,
        AppSettings appSettings,
        SettingsContext context,
        IMediator mediator,
        IProductGroupService productGroupService,
        IServiceScopeFactory serviceScopeFactory,
        IGlobalCacheService globalCacheService,
        IMessageBusClient messageBusClient)
    {
        _sheetsService = sheetsService;
        _appSettings = appSettings;
        _context = context;
        _mediator = mediator;
        _serviceScopeFactory = serviceScopeFactory;
        _productGroupService = productGroupService;
        _globalCacheService = globalCacheService;
        _messageBusClient = messageBusClient;
    }

    public async Task<List<Dictionary<string, FeedSheetValueModel>>> ListAsync(string sheetId)
    {
        var credential = await Credential();
        var productAttributes = await _context.ProductAttributes.Select(x => x.Name).ToListAsync();
        var mappingAttributes = ProductFeedConstants.MappingAttributes()
            .Where(x => productAttributes.Contains(x.Attribute))
            .SelectMany(x => x.Mappings)
            .Select(x => x.ToLower()).ToList();

        _sheetsService = new SheetsService(new BaseClientService.Initializer { HttpClientInitializer = credential });
        var sheetName = await GetFirstSheetName(sheetId);

        var range = $"{sheetName}!A:ZZZ";
        var request = _sheetsService.Spreadsheets.Values.Get(sheetId, range);
        var response = await request.ExecuteAsync();
        var values = response.Values;

        var properties = values.First().Select(x => x.ToString()?.Normalize().Trim().ToLower()).ToList();

        var validFields = properties.Where(x => x != null && mappingAttributes.Contains(x.ToString())).ToList();

        var dictionaries = new List<Dictionary<string, FeedSheetValueModel>>();
        for (var i = 1; i < values.Count; i++)
        {
            if (validFields == null) continue;
            var dict = new Dictionary<string, FeedSheetValueModel>();
            foreach (var field in validFields)
            {
                var index = properties.IndexOf(field);
                var attribute = ProductFeedConstants.MappingAttributes()
                    .FirstOrDefault(x => x.Mappings.Contains(field?.ToLower()));
                if (field == null || attribute == null) continue;
                var valueIndex = values[i].Count;
                if (index >= valueIndex)
                {
                    dict.Add(attribute.Attribute, new FeedSheetValueModel
                    {
                        Value = string.Empty,
                        Position = $"{index.ToSheetColumn()}{i + 1}"
                    });
                }
                else
                {
                    var value = values[i][index];
                    var position = $"{index.ToSheetColumn()}{i + 1}";
                    dict.Add(attribute.Attribute, new FeedSheetValueModel
                    {
                        Value = value,
                        Position = position
                    });
                }
            }

            dictionaries.Add(dict);
        }

        return dictionaries;
    }

    public async Task<string> GetFirstSheetName(string sheetId)
    {
        var sheetsService = await GetSheetServices();
        var spreadSheet = await sheetsService.Spreadsheets.Get(sheetId).ExecuteAsync();
        return spreadSheet.Sheets.FirstOrDefault()?.Properties?.Title ?? "";
    }

    public async Task ProcessGoogleSheet(Guid productFeedId, Guid? currentUserId, bool isAdmin)
    {
        var cancellationToken = new CancellationToken();
        using var scope = _serviceScopeFactory.CreateScope();

        var updateStatusService = scope.ServiceProvider.GetRequiredService<IUpdateStatusService>();

        var productAttributes = await _context.ProductAttributes.ToListAsync(cancellationToken);
        var productFeed = await _context.ProductFeeds
            .Include(x => x.ProductEntities)
            .Where(x => !x.Deleted)
            .FirstOrDefaultAsync(x => x.Id == productFeedId, cancellationToken);
        if (productFeed is null) throw new BadRequestException("Không tìm thấy nguồn cấp dữ liệu sản phẩm");

        NovanetResponse<List<Dictionary<string, FeedSheetValueModel>>> productFeedResponse;
        try
        {
            productFeedResponse = await _mediator.Send(new ListProductFeedGoogleSheetItemsQuery
            {
                SheetId = productFeed.SourcePath
            }, cancellationToken);
        }
        catch (Exception ex)
        {
            var errorMessage = ex.Message switch
            {
                ErrorMessageValidate.ValidateDuplicateColumn.ValidateDuplicateColumnId => ErrorMessageValidate
                    .ValidateDuplicateColumn.ValidateDuplicateColumnIdVi,
                _ => ErrorMessageValidate.ValidateUnauthorized
            };
            var errorType = ex.Message switch
            {
                ErrorMessageValidate.ValidateDuplicateColumn.ValidateDuplicateColumnId => nameof(ErrorMessageValidate
                    .ValidateDuplicateColumn),
                _ => ErrorMessageValidate.ValidateUnauthorized
            };
            var permission = ex.Message switch
            {
                ErrorMessageValidate.ValidateDuplicateColumn.ValidateDuplicateColumnId => ProductFeedPermission.Granted,
                _ => ProductFeedPermission.CannotAccess
            };
            productFeed.Permission = permission;
            productFeed.InvalidProductsCount = 0;
            productFeed.ValidProductsCount = 0;
            _context.ProductFeeds.Update(productFeed);
            await _context.ProductStatus.AddAsync(new ProductStatus
            {
                Success = false,
                ProductFeedId = productFeedId,
                ErrorMessage = errorMessage,
                ErrorType = errorType
            }, cancellationToken);
            await UpdateCampaignsToError(productFeedId);
            await DeleteAllProducts(productFeedId, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return;
        }

        var productFeedItems = productFeedResponse.Data;

        var attributes = productFeedItems.FirstOrDefault()?.Select(x => x.Key).ToList();
        if (attributes == null || attributes.Count.Equals(0))
        {
            productFeed.Permission = ProductFeedPermission.CannotParse;
            _context.ProductFeeds.Update(productFeed);
            await _context.ProductStatus.AddAsync(new ProductStatus
            {
                Success = false,
                ProductFeedId = productFeedId,
                ErrorMessage = string.Format(ErrorMessageValidate.ValidateMissingAttribute,
                    ProductFeedConstants.RequiredAttributes.Select(x => x.DisplayName).JoinString(", ")),
                ErrorType = nameof(ErrorMessageValidate.ValidateMissingAttribute)
            }, cancellationToken);
            await UpdateCampaignsToError(productFeedId);
            await DeleteAllProducts(productFeedId, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            await updateStatusService.UpdateProductGroup(productFeedId);
            return;
        }

        var missingAttribute = ValidateRequired(attributes);
        if (missingAttribute.Count > 0)
        {
            productFeed.Permission = ProductFeedPermission.CannotParse;
            _context.ProductFeeds.Update(productFeed);
            await _context.ProductStatus.AddAsync(new ProductStatus
            {
                Success = false,
                ProductFeedId = productFeedId,
                ErrorMessage = string.Format(ErrorMessageValidate.ValidateMissingAttribute,
                    missingAttribute.JoinString(", ")),
                ErrorType = nameof(ErrorMessageValidate.ValidateMissingAttribute)
            }, cancellationToken);
            await UpdateCampaignsToError(productFeedId);
            await DeleteAllProducts(productFeedId, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            await updateStatusService.UpdateProductGroup(productFeedId);
            return;
        }

        int validProducts = 0, invalidProducts = 0;
        var runningProducts = 0;

        var idAttribute = productAttributes.FirstOrDefault(x => x.Name == "Id");
        var titleAttribute = productAttributes.FirstOrDefault(x => x.Name == "Title");
        var descriptionAttribute = productAttributes.FirstOrDefault(x => x.Name == "Description");
        var imageLinkAttribute = productAttributes.FirstOrDefault(x => x.Name == "ImageLink");
        var linkAttribute = productAttributes.FirstOrDefault(x => x.Name == "Link");

        // remove non existing products
        var existingProductIds = new List<Guid>();

        foreach (var productFeedItem in productFeedItems)
        {
            var idValue = productFeedItem.FirstOrDefault(x => x.Key.Equals("Id"))
                .Value.Value?.ToString();
            var titleValue = productFeedItem.FirstOrDefault(x => x.Key.Equals("Title"))
                .Value.Value?.ToString();
            var descriptionValue = productFeedItem.FirstOrDefault(x => x.Key.Equals("Description"))
                .Value.Value?.ToString();
            var linkValue = productFeedItem.FirstOrDefault(x => x.Key.Equals("Link"))
                .Value.Value?.ToString();
            var imageLinkValue = productFeedItem.FirstOrDefault(x => x.Key.Equals("ImageLink"))
                .Value.Value?.ToString();

            var currentProduct = await _context.ProductEntities
                .Include(x => x.AttributeValues)
                .FirstOrDefaultAsync(x =>
                        (isAdmin || x.CreatedBy == currentUserId) && !x.Deleted
                                                                  && x.AttributeValues!.Any(y =>
                                                                      y.AttributeId == idAttribute!.Id &&
                                                                      y.StringValue == idValue)
                                                                  && x.AttributeValues!.Any(y =>
                                                                      y.AttributeId == titleAttribute!.Id &&
                                                                      y.StringValue == titleValue)
                                                                  && x.AttributeValues!.Any(y =>
                                                                      y.AttributeId == descriptionAttribute!.Id &&
                                                                      y.StringValue == descriptionValue)
                                                                  && x.AttributeValues!.Any(y =>
                                                                      y.AttributeId == linkAttribute!.Id &&
                                                                      y.StringValue == linkValue)
                                                                  && x.AttributeValues!.Any(y =>
                                                                      y.AttributeId == imageLinkAttribute!.Id &&
                                                                      y.StringValue == imageLinkValue),
                    cancellationToken);

            if (currentProduct is not null && currentProduct.ProductFeedId != productFeedId)
            {
                var duplicatedProductStatus = new ProductStatus
                {
                    Success = false,
                    SheetPosition = productFeedItem.FirstOrDefault(x => x.Key.Equals("Id"))
                        .Value.Position,
                    ProductFeedId = productFeedId,
                    ErrorMessage = ErrorMessageValidate.ValidateDuplicatedProduct,
                    ErrorType = nameof(ErrorMessageValidate.ValidateDuplicatedProduct),
                    ProductIdValue = idValue
                };
                await _context.ProductStatus.AddAsync(duplicatedProductStatus, cancellationToken);
                continue;
            }

            var productHasError = false;
            var productAvailability = string.Empty;
            var productEntity = new ProductEntity
            {
                ProductFeedId = productFeed.Id,
                CreatedAt = DateTimeOffset.Now,
                CreatedBy = currentUserId,
                AttributeValues = new List<ProductAttributeValue>(),
                IsValid = true,
                IsActive = true
            };
            if (currentProduct is not null && currentProduct.ProductFeedId == productFeedId)
                productEntity.Id = currentProduct.Id;

            foreach (var (key, value) in productFeedItem)
            {
                var productAttribute = productAttributes.FirstOrDefault(x => x.Name.Equals(key));
                var mappingAttribute = ProductFeedConstants.MappingAttributes()
                    .FirstOrDefault(x => x.Attribute.Equals(key));
                var supportedValues = mappingAttribute?.SupportedValues;
                var displayName = mappingAttribute?.DisplayName;

                if (productAttribute == null) continue;

                var attributeValue = new ProductAttributeValue
                {
                    AttributeId = productAttribute.Id,
                    ProductId = productEntity.Id
                };

                attributeValue = ValidateDataValue(productAttribute, attributeValue, value.Value!, out var error,
                    out var errorType);
                attributeValue.ProductStatus = new List<ProductStatus>
                {
                    new()
                    {
                        Success = string.IsNullOrEmpty(error),
                        ProductAttributeValueId = attributeValue.Id,
                        ProductEntityId = productEntity.Id,
                        ErrorMessage = error,
                        ErrorType = errorType,
                        ProductIdValue = idValue,
                        SheetPosition = value.Position
                    }
                };

                if (supportedValues != null && !supportedValues.Contains(value.Value?.ToString()))
                {
                    error = string.Format(ErrorMessageValidate.ValidateValueCouldNotSupport, displayName,
                        supportedValues.JoinString(","));
                    attributeValue.ProductStatus.Add(new ProductStatus
                    {
                        Success = false,
                        ProductAttributeValueId = attributeValue.Id,
                        ProductEntityId = productEntity.Id,
                        ErrorMessage = error,
                        ErrorType = nameof(ErrorMessageValidate.ValidateValueCouldNotSupport),
                        ProductIdValue = idValue,
                        SheetPosition = value.Position
                    });
                }

                attributeValue.HashKey = string.Concat(attributeValue.DoubleValue, attributeValue.NumberValue,
                    attributeValue.StringValue, attributeValue.DateTimeValue, attributeValue.BooleanValue).CreateMd5();

                productEntity.AttributeValues.Add(attributeValue);
                if (error != string.Empty && productAttribute.Required) productHasError = true;

                if (productAttribute.Name == "Availability")
                    productAvailability = attributeValue.StringValue ?? string.Empty;
            }

            if (productHasError)
            {
                productEntity.IsValid = false;
                productEntity.IsActive = false;
                invalidProducts += 1;
            }
            else
            {
                validProducts += 1;
                productEntity.Status = productAvailability.ToLower() == "hết hàng"
                    ? ProductEntityStatus.OutOfStock
                    : ProductEntityStatus.Running;
                if (productEntity.Status.Equals(ProductEntityStatus.Running)) runningProducts += 1;
            }

            if (currentProduct is not null && currentProduct.ProductFeedId == productFeedId)
            {
                var currentAttributeValues = await _context.ProductAttributeValues
                    .Where(x => x.ProductId == currentProduct.Id && !x.Deleted)
                    .ToListAsync(cancellationToken);
                var updatingAttributeValues = new List<ProductAttributeValue>();
                var addingAttributeValues = new List<ProductAttributeValue>();
                var newProductStatusList = new List<ProductStatus>();

                foreach (var attributeValue in productEntity.AttributeValues ?? new List<ProductAttributeValue>())
                {
                    var existedAttributeValue =
                        currentAttributeValues.FirstOrDefault(x => x.AttributeId == attributeValue.AttributeId);
                    if (existedAttributeValue is not null)
                    {
                        existedAttributeValue.BooleanValue = attributeValue.BooleanValue;
                        existedAttributeValue.DoubleValue = attributeValue.DoubleValue;
                        existedAttributeValue.NumberValue = attributeValue.NumberValue;
                        existedAttributeValue.StringValue = attributeValue.StringValue;
                        existedAttributeValue.DateTimeValue = attributeValue.DateTimeValue;
                        existedAttributeValue.HashKey = attributeValue.HashKey;
                        updatingAttributeValues.Add(existedAttributeValue);
                        newProductStatusList.AddRange(attributeValue.ProductStatus?.Select(x =>
                        {
                            x.ProductAttributeValueId = existedAttributeValue.Id;
                            return x;
                        }) ?? new List<ProductStatus>());
                    }
                    else
                    {
                        addingAttributeValues.Add(attributeValue);
                    }
                }

                var removingAttributeValues = currentAttributeValues
                    .Where(x =>
                    {
                        return addingAttributeValues.All(y => y.AttributeId != x.AttributeId) &&
                               updatingAttributeValues.All(y => y.AttributeId != x.AttributeId);
                    })
                    .Select(x =>
                    {
                        x.Deleted = true;
                        return x;
                    });

                await _context.ProductAttributeValues.AddRangeAsync(addingAttributeValues, cancellationToken);
                _context.ProductAttributeValues.UpdateRange(updatingAttributeValues);
                await _context.ProductStatus.AddRangeAsync(newProductStatusList, cancellationToken);
                _context.ProductAttributeValues.UpdateRange(removingAttributeValues);
                currentProduct.Status = productEntity.Status;
                currentProduct.IsActive = productEntity.IsActive;
                currentProduct.IsValid = productEntity.IsValid;
                currentProduct.ModifiedBy = currentUserId;
                currentProduct.ModifiedAt = DateTimeOffset.Now;
                _context.ProductEntities.Update(currentProduct);
                existingProductIds.Add(currentProduct.Id);
            }
            else
            {
                await _context.ProductEntities.AddAsync(productEntity, cancellationToken);
                existingProductIds.Add(productEntity.Id);
            }
        }

        productFeed.Permission = ProductFeedPermission.Granted;
        productFeed.ModifiedAt = DateTimeOffset.Now;
        productFeed.ValidProductsCount = validProducts;
        productFeed.InvalidProductsCount = invalidProducts;

        _context.ProductFeeds.Update(productFeed);

        var productsToDelete = await _context.ProductEntities
            .Include(x => x.AttributeValues)
            .Include(x => x.ProductStatus)
            .Include(x => x.ProductGroupEntities)
            .Where(x => x.ProductFeedId == productFeedId && !existingProductIds.Contains(x.Id) && !x.Deleted)
            .ToListAsync(cancellationToken);
        productsToDelete.ForEach(x =>
        {
            x.Deleted = true;
            x.AttributeValues?.ForEach(y => { y.Deleted = true; });
            x.ProductGroupEntities?.ToList().ForEach(y => { y.Deleted = true; });
        });
        _context.ProductEntities.UpdateRange(productsToDelete);
        await _context.SaveChangesAsync(cancellationToken);
        await UpdateCache(productFeedId, cancellationToken);
        await _productGroupService.RefreshProductGroups(productFeedId);
        await updateStatusService.UpdateProductGroup(productFeedId);
        await updateStatusService.UpdateAdvertisingStatus(productFeedId);
        await updateStatusService.UpdateAdvertisingSetStatus(productFeedId);
        await updateStatusService.UpdateCampaignStatus(productFeedId);
    }

    private async Task<GoogleCredential> Credential()
    {
        var credentialJson = _appSettings.GoogleSheetCredentials.Base64Decode();
        var byteArray = Encoding.UTF8.GetBytes(credentialJson);
        var stream = new MemoryStream(byteArray);
        var credential = await GoogleCredential.FromStreamAsync(stream, CancellationToken.None);
        return credential.CreateScoped(_scopes);
    }

    private static List<string?> ValidateRequired(List<string> attributes)
    {
        if (attributes == null) throw new ArgumentNullException(nameof(attributes));
        var requiredAttributes = new List<MappingAttribute?>(ProductFeedConstants.RequiredAttributes);
        foreach (var requiredAttribute in ProductFeedConstants.RequiredAttributes)
        {
            var existed = requiredAttribute.Mappings.Intersect(attributes).FirstOrDefault();
            if (string.IsNullOrEmpty(existed)) continue;
            var attribute = requiredAttributes.FirstOrDefault(x => x != null && x.Attribute.Equals(existed));
            var indexOfAttribute = requiredAttributes.IndexOf(attribute);
            requiredAttributes.RemoveAt(indexOfAttribute);
        }

        return requiredAttributes.Select(x => x?.DisplayName).ToList();
    }

    private static ProductAttributeValue ValidateDataValue(ProductAttributeCore productAttribute,
        ProductAttributeValue productAttributeValue, object value, out string error, out string errorType)
    {
        bool _;
        var errorMessage = string.Empty;
        var type = string.Empty;

        var validateMessage = productAttribute.Required
            ? ErrorMessageValidate.ValidateCouldNotParseRequired
            : ErrorMessageValidate.ValidateCouldNotParse;
        var validateMessageType = productAttribute.Required
            ? nameof(ErrorMessageValidate.ValidateCouldNotParseRequired)
            : nameof(ErrorMessageValidate.ValidateCouldNotParse);

        var dataType = productAttribute.DataType;
        switch (dataType)
        {
            case NovanetDataType.String:
                productAttributeValue.StringValue = value.ToString();
                if (string.IsNullOrWhiteSpace(productAttributeValue.StringValue) && productAttribute.Required)
                {
                    errorMessage = string.Format(ErrorMessageValidate.ValidateCannotBlank, productAttribute.Name);
                    type = validateMessageType;
                    break;
                }

                if (productAttribute.MaxLength > 0 &&
                    productAttributeValue.StringValue?.Length > productAttribute.MaxLength)
                {
                    errorMessage = string.Format(ErrorMessageValidate.ValidateMaxLength, productAttribute.Name);
                    type = validateMessageType;
                }

                break;
            case NovanetDataType.Url:
            {
                productAttributeValue.StringValue = value.ToString();
                var urls = (value.ToString() ?? string.Empty).Split(',');
                foreach (var url in urls)
                    if (!string.IsNullOrEmpty(url) && !RegexUtils.UrlRegex.IsMatch(url))
                    {
                        errorMessage = string.Format(ErrorMessageValidate.ValidateMalformedUrl, productAttribute.Name);
                        type = validateMessageType;
                    }

                if (string.IsNullOrEmpty(productAttributeValue.StringValue) && productAttribute.Required)
                {
                    errorMessage = string.Format(ErrorMessageValidate.ValidateCannotBlank, productAttribute.Name);
                    type = validateMessageType;
                }

                break;
            }
            case NovanetDataType.Number:
                _ = int.TryParse(value.ToString(), out var valueNumber);
                productAttributeValue.NumberValue = _ ? valueNumber : null;
                if (!_)
                {
                    errorMessage = string.Format(validateMessage, productAttribute.Name,
                        nameof(NovanetDataType.Number));
                    type = validateMessageType;
                }

                break;
            case NovanetDataType.Double:
                var rawDoubleValue = value.ToString()?.TrimPrice();
                _ = double.TryParse(rawDoubleValue, out var valueDouble);
                productAttributeValue.DoubleValue = _ ? valueDouble : null;
                if (!_)
                {
                    errorMessage = string.Format(validateMessage, productAttribute.Name,
                        nameof(NovanetDataType.Double));
                    type = validateMessageType;
                }

                break;
            case NovanetDataType.DateTime:
                _ = DateTimeOffset.TryParse(value.ToString(), out var valueDateTime);
                productAttributeValue.DateTimeValue = _ ? valueDateTime : null;
                if (!_)
                {
                    errorMessage = string.Format(validateMessage, productAttribute.Name,
                        nameof(NovanetDataType.DateTime));
                    type = validateMessageType;
                }

                break;
            case NovanetDataType.Boolean:
                _ = bool.TryParse(value.ToString(), out var booleanValue);
                productAttributeValue.BooleanValue = _ ? booleanValue : null;
                if (!_)
                {
                    errorMessage = string.Format(validateMessage, productAttribute.Name,
                        nameof(NovanetDataType.Boolean));
                    type = validateMessageType;
                }

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        error = errorMessage;
        errorType = type;
        return productAttributeValue;
    }

    private async Task UpdateCampaignsToError(Guid productFeedId)
    {
        var campaignsToUpdate = await _context.Campaigns
            .Include(x => x.AdvertisingSets).Include(x => x.Advertisings)
            .Where(x => x.EcommerceProductFeedId.Equals(productFeedId))
            .ToListAsync();
        campaignsToUpdate.ForEach(x => { x.Status = CampaignStatus.Error; });
        var advertisingSets = new List<AdvertisingSet>();
        var advertisings = new List<Advertising>();
        foreach (var campaignToUpdate in campaignsToUpdate)
        {
            campaignToUpdate.Status = CampaignStatus.Error;
            foreach (var AdvertisingSet in campaignToUpdate?.AdvertisingSets ?? new List<AdvertisingSet>())
            {
                AdvertisingSet.Status = AdvertisingSetStatus.Error;
                advertisingSets.Add(AdvertisingSet);
            }

            foreach (var advertising in campaignToUpdate?.Advertisings ?? new List<Advertising>())
            {
                advertising.Status = AdvertisingStatus.Error;
                advertisings.Add(advertising);
            }
        }

        _context.AdvertisingSets.UpdateRange(advertisingSets);
        _context.Advertising.UpdateRange(advertisings);
        _context.Campaigns.UpdateRange(campaignsToUpdate);
    }

    private async Task<SheetsService> GetSheetServices()
    {
        var credential = await Credential();
        return new SheetsService(new BaseClientService.Initializer { HttpClientInitializer = credential });
    }

    private async Task UpdateCache(Guid productFeedId, CancellationToken cancellationToken)
    {
        var productFeed = await _context.ProductFeeds
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == productFeedId, cancellationToken);
        if (productFeed is null) return;
        await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductFeedCore>(productFeed.SubId), productFeed);
        _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductFeedCore>(),
            new PatchCacheManagerSignal<ProductFeedCore>
            {
                Entity = productFeed,
                EntitySubId = productFeed.SubId
            });

        var products = await _context.ProductEntities
            .AsNoTracking()
            .Include(x => x.AttributeValues)
            .Where(x => x.ProductFeedId == productFeedId)
            .ToListAsync(cancellationToken);
        foreach (var product in products)
            if (product.Deleted)
            {
                await _globalCacheService.KeyDeleteAsync(RedisKeys.KeySettings<ProductEntityCore>(product.SubId));
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductEntityCore>(),
                    new PatchCacheManagerSignal<ProductEntityCore>
                    {
                        Entity = null,
                        EntitySubId = product.SubId
                    });
            }
            else
            {
                product.ProductAttributeValueCores =
                    product.AttributeValues?.Cast<ProductAttributeValueCore>().ToList() ??
                    new List<ProductAttributeValueCore>();
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductEntityCore>(product.SubId), product);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductEntityCore>(),
                    new PatchCacheManagerSignal<ProductEntityCore>
                    {
                        Entity = product,
                        EntitySubId = product.SubId
                    });
            }
    }

    private async Task DeleteAllProducts(Guid productFeedId, CancellationToken cancellationToken)
    {
        var products = await _context.ProductEntities
            .Where(x => x.ProductFeedId == productFeedId)
            .ToListAsync(cancellationToken);
        foreach (var product in products)
        {
            product.Deleted = true;

            await _globalCacheService.KeyDeleteAsync(RedisKeys.KeySettings<ProductEntityCore>(product.SubId));
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductEntityCore>(),
                new PatchCacheManagerSignal<ProductEntityCore>
                {
                    Entity = null,
                    EntitySubId = product.SubId
                });
        }

        _context.UpdateRange(products);
    }
}