﻿using System.Drawing;
using Novanet.Core.Extensions.LinqExtensions;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Service.Settings.Services;

public interface IExportDataToExcelFile<T>
{
    Task<MemoryStream> ExportFile(IEnumerable<ExportDataResponse<List<T>>> listData);
}

public class ExportDataToExcelFile<T> : IExportDataToExcelFile<T>
{
    public Task<MemoryStream> ExportFile(IEnumerable<ExportDataResponse<List<T>>> listData)
    {
        var streamFile = new MemoryStream();
        using var package = new ExcelPackage(streamFile);

        foreach (var items in listData)
        {
            var sheetName = items.SheetName as string;
            var workSheet = package.Workbook.Worksheets.Add(sheetName);  
            workSheet.Cells.LoadFromCollection(items.Data, true).AutoFitColumns();
            workSheet.View.FreezePanes(2, 1);

            for (var i = 1; i < workSheet.Columns.Count() + 1; i++)
            {
                for (var j = 1; j <= items.Data!.Count + 1; j++)
                {
                    workSheet.Cells[j, i].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[j, i].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[j, i].Style.HorizontalAlignment = 
                        ExcelHorizontalAlignment.Left;
                    workSheet.Cells[j, i].Style.Font.SetFromFont("Times New Roman", 12);
                }
                
                workSheet.Cells[1, i].Style.Font.Bold = true;
                workSheet.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                workSheet.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(155, 194, 230));
            }
        }
        package.Save();
        streamFile.Position = 0;
        
        return Task.FromResult(streamFile);
    }
}