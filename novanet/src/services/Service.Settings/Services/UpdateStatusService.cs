﻿using System.Linq.Expressions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessConstants;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Services;

public interface IUpdateStatusService
{
    Task UpdateProductGroup(Guid productFeedId);
    Task UpdateAdvertisingStatus(Guid productFeedId);

    Task UpdateAdvertisingSetStatus(Guid productFeedId);

    Task UpdateCampaignStatus(Guid? productFeedId);

    Task UpdateSingleCampaignStatus(Guid campaignId);
}

public class UpdateStatusService : IUpdateStatusService
{
    private readonly SettingsContext _context;
    private readonly IGlobalCacheService _globalCacheService;
    private readonly IMessageBusClient _messageBusClient;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public UpdateStatusService(
        SettingsContext context,
        IGlobalCacheService globalCacheService,
        IMessageBusClient messageBusClient,
        IServiceScopeFactory serviceScopeFactory)
    {
        _context = context;
        _globalCacheService = globalCacheService;
        _messageBusClient = messageBusClient;
        _serviceScopeFactory = serviceScopeFactory;
    }


    public async Task UpdateProductGroup(Guid productFeedId)
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var appSettings = scope.ServiceProvider.GetRequiredService<AppSettings>();
        var connectionstring = appSettings.ConnectionStrings.DefaultConnection;

        var optionsBuilder = new DbContextOptionsBuilder<SettingsContext>();
        optionsBuilder.UseSqlServer(connectionstring);
        await using var context = new SettingsContext(optionsBuilder.Options, appSettings);

        var cancellationToken = new CancellationToken();
        var advertisingSets = await context.AdvertisingSets
            .Include(x => x.AdvertisingSetProductGroups)!
            .ThenInclude(x => x.ProductGroup)
            .ThenInclude(x => x!.ProductGroupEntities)!
            .ThenInclude(x => x.ProductEntity)
            .Where(x => x.Campaign!.EcommerceProductFeedId.Equals(productFeedId))
            .ToListAsync(cancellationToken);

        var productFeed = await context.ProductFeeds.FirstOrDefaultAsync(x =>
            x.Permission.Equals(ProductFeedPermission.Granted) && x.Id.Equals(productFeedId));

        advertisingSets.ForEach(advertisingSet =>
        {
            var allProductGroups =
                advertisingSet.AdvertisingSetProductGroups?.ToList();

            allProductGroups?.ForEach(x =>
            {
                var allProducts = x.ProductGroup?.ProductGroupEntities?.Count(z =>
                    z.ProductEntity is {Deleted: false});
                var products = x.ProductGroup?.ProductGroupEntities?.Count(z =>
                    z.ProductEntity is
                        {Deleted: false, IsActive: true, Status: ProductEntityStatus.Running, IsValid: true});
                var errorProducts =
                    x.ProductGroup?.ProductGroupEntities!.Count(z =>
                        z.ProductEntity is {Deleted: false, IsValid: false} or
                            {Deleted: false, IsValid: true, Status: ProductEntityStatus.OutOfStock});
                if (errorProducts.Equals(allProducts) || products is 0 || productFeed is null)
                    x.Status = AdvertisingSetStatus.Error;
                else
                    x.Status = x.IsActive ? AdvertisingSetStatus.Running : AdvertisingSetStatus.Paused;
            });
            if (allProductGroups != null) context.AdvertisingSetProductGroups.UpdateRange(allProductGroups);
        });
        await context.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateAdvertisingStatus(Guid productFeedId)
    {
        var cancellationToken = new CancellationToken();
        var productsValid = CacheManager.Products.Values.Count(x =>
            x.ProductFeedId.Equals(productFeedId) &&
            !x.Deleted &&
            x.IsValid &&
            x.Status.Equals(ProductEntityStatus.Running) &&
            x.IsActive.Equals(true));

        var advertisings = await _context.Advertising
            .Include(x => x.AdvertisingAppliedTemplates)
            .Include(x => x.AdvertisingSet)
            .ThenInclude(x => x!.Campaign)
            .Where(x => x.AdvertisingSet!.Campaign!.EcommerceProductFeedId == productFeedId)
            .ToListAsync(cancellationToken);

        if (productsValid.Equals(0))
        {
            foreach (var advertising in advertisings)
            {
                advertising.Status = AdvertisingStatus.Error;

                var cachedAdvertising =
                    CacheManager.Advertising.Values.FirstOrDefault(x => x.Id.Equals(advertising.Id));
                if (cachedAdvertising is null) continue;
                cachedAdvertising.Status = advertising.Status;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingCore>(cachedAdvertising.SubId),
                    cachedAdvertising);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingCore>(),
                    new PatchCacheManagerSignal<AdvertisingCore>
                    {
                        Entity = cachedAdvertising,
                        EntitySubId = cachedAdvertising.SubId
                    });
            }

            _context.Advertising.UpdateRange(advertisings);
        }
        else
        {
            var updateAdvertising = new List<Advertising>();

            foreach (var advertising in advertisings)
            {
                foreach (var advertisingAppliedTemplate in advertising.AdvertisingAppliedTemplates ??
                                                           new List<AdvertisingAppliedTemplate>())
                {
                    var template = CacheManager.Templates.Values.FirstOrDefault(x =>
                        x.Id == advertisingAppliedTemplate.AdvertisingTemplateId);
                    advertising.Status = advertising.IsActive.Equals(true)
                        ? AdvertisingStatus.Running
                        : AdvertisingStatus.Paused;
                    if (template != null &&
                        TemplateValueProducts.TemplateRequiredProducts.TryGetValue(template.TemplateType,
                            out var valueTemplate) &&
                        valueTemplate > productsValid) advertising.Status = AdvertisingStatus.Error;
                    updateAdvertising.Add(advertising);
                }

                var cachedAdvertising =
                    CacheManager.Advertising.Values.FirstOrDefault(x => x.Id.Equals(advertising.Id));
                if (cachedAdvertising is null) continue;
                cachedAdvertising.Status = advertising.Status;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingCore>(cachedAdvertising.SubId),
                    cachedAdvertising);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingCore>(),
                    new PatchCacheManagerSignal<AdvertisingCore>
                    {
                        Entity = cachedAdvertising,
                        EntitySubId = cachedAdvertising.SubId
                    });
            }

            _context.Advertising.UpdateRange(updateAdvertising);
        }

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateAdvertisingSetStatus(Guid productFeedId)
    {
        var cancellationToken = new CancellationToken();
        var advertisingSets = await _context.AdvertisingSets
            .Include(x => x.AdvertisingSetProductGroups)!
            .ThenInclude(x => x.ProductGroup)
            .ThenInclude(x => x!.ProductGroupEntities)!
            .ThenInclude(x => x.ProductEntity)
            .Where(x => x.Campaign!.EcommerceProductFeedId.Equals(productFeedId))
            .ToListAsync(cancellationToken);

        var productsValid = await _context.ProductEntities.CountAsync(x =>
            x.ProductFeedId.Equals(productFeedId) &&
            !x.Deleted &&
            x.IsValid &&
            x.Status.Equals(ProductEntityStatus.Running) &&
            x.IsActive.Equals(true));

        foreach (var advertisingSet in advertisingSets)
        {
            var advertisings = CacheManager.Advertising.Values
                .Count(x =>
                    x.AdvertisingSetId.Equals(advertisingSet.Id));

            var validAdvertisings = CacheManager.Advertising.Values
                .Count(x =>
                    x.AdvertisingSetId.Equals(advertisingSet.Id) &&
                    x.IsActive.Equals(true) &&
                    x.Status.Equals(AdvertisingStatus.Running));

            var runningProductGroups =
                advertisingSet.AdvertisingSetProductGroups?.Where(x => x.Status.Equals(AdvertisingSetStatus.Running))
                    .ToList();

            var allProductGroups =
                advertisingSet.AdvertisingSetProductGroups?.ToList();

            allProductGroups?.ForEach(advertisingSetProductGroup =>
            {
                var allProducts = advertisingSetProductGroup.ProductGroup?.ProductGroupEntities?.Count(z =>
                    z.ProductEntity is {Deleted: false});
                var products = advertisingSetProductGroup.ProductGroup?.ProductGroupEntities?.Count(z =>
                    z.ProductEntity is
                        {Deleted: false, IsActive: true, Status: ProductEntityStatus.Running, IsValid: true});
                var errorProducts =
                    advertisingSetProductGroup.ProductGroup?.ProductGroupEntities!.Count(z =>
                        z.ProductEntity is {Deleted: false, IsValid: false} or
                            {Deleted: false, IsValid: true, Status: ProductEntityStatus.OutOfStock});
                if (errorProducts.Equals(allProducts) || products is 0)
                    advertisingSetProductGroup.Status = AdvertisingSetStatus.Error;
                else
                    advertisingSetProductGroup.Status = advertisingSetProductGroup.IsActive
                        ? AdvertisingSetStatus.Running
                        : AdvertisingSetStatus.Paused;
            });

            if (runningProductGroups is {Count: 0})
            {
                if (advertisingSet is {ApplyAllProductGroups: true})
                {
                    if (validAdvertisings > 0 && advertisingSet.IsActive)
                    {
                        advertisingSet.Status = AdvertisingSetStatus.Running;
                    }
                    else
                    {
                        if (advertisings.Equals(0))
                        {
                            advertisingSet.Status = productsValid.Equals(0)
                                ? AdvertisingSetStatus.Error
                                : AdvertisingSetStatus.Unfinished;
                        }
                        else
                        {
                            var errorAdvertising = CacheManager.Advertising.Values
                                .Count(x =>
                                    x.AdvertisingSetId.Equals(advertisingSet.Id) &&
                                    x.Status.Equals(AdvertisingStatus.Error));
                            advertisingSet.Status = errorAdvertising.Equals(advertisings)
                                ? AdvertisingSetStatus.Error
                                : AdvertisingSetStatus.Paused;
                        }
                    }
                }
                else
                {
                    advertisingSet.Status = !advertisings.Equals(0)
                        ? validAdvertisings.Equals(0) ? AdvertisingSetStatus.Error : AdvertisingSetStatus.Paused
                        : !productsValid.Equals(0)
                            ? AdvertisingSetStatus.Unfinished
                            : AdvertisingSetStatus.Error;
                }
            }
            else if (runningProductGroups is not null)
            {
                if (validAdvertisings > 0 && advertisingSet.IsActive)
                {
                    advertisingSet.Status = AdvertisingSetStatus.Running;
                }
                else
                {
                    if (advertisings.Equals(0))
                    {
                        advertisingSet.Status = productsValid.Equals(0)
                            ? AdvertisingSetStatus.Error
                            : AdvertisingSetStatus.Unfinished;
                    }
                    else
                    {
                        var errorAdvertising = CacheManager.Advertising.Values
                            .Count(x =>
                                x.AdvertisingSetId.Equals(advertisingSet.Id) &&
                                x.Status.Equals(AdvertisingStatus.Error));
                        advertisingSet.Status = errorAdvertising.Equals(advertisings)
                            ? AdvertisingSetStatus.Error
                            : AdvertisingSetStatus.Paused;
                    }
                }
            }

            var cachedAdvertisingSet =
                CacheManager.AdvertisingSets.Values.FirstOrDefault(x =>
                    advertisingSet != null && x.Id.Equals(advertisingSet.Id));
            if (cachedAdvertisingSet is null) continue;
            if (advertisingSet != null) cachedAdvertisingSet.Status = advertisingSet.Status;
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingCore>(cachedAdvertisingSet.SubId),
                cachedAdvertisingSet);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(),
                new PatchCacheManagerSignal<AdvertisingSetCore>
                {
                    Entity = cachedAdvertisingSet,
                    EntitySubId = cachedAdvertisingSet.SubId
                });
        }

        _context.AdvertisingSets.UpdateRange(advertisingSets);

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateCampaignStatus(Guid? productFeedId)
    {
        Expression<Func<Campaign, bool>> whereExpression = x => true;
        if (productFeedId is not null) whereExpression = x => x.EcommerceProductFeedId.Equals(productFeedId);

        if (CacheManager.LastUpdatedEnd is null)
        {
            // wait for 2 minutes for cache to be loaded
            await Task.Delay(2 * 60 * 1000);
        }

        var cancellationToken = new CancellationToken();
        var campaigns = await _context.Campaigns
            .Include(x => x.CampaignScheduling)
            .Where(whereExpression)
            .ToListAsync(cancellationToken);

        foreach (var campaign in campaigns)
        {
            var validAdvertisingSets = CacheManager.AdvertisingSets.Values
                .Count(x =>
                    x.CampaignId.Equals(campaign.Id) &&
                    x.IsActive.Equals(true) &&
                    x.Status.Equals(AdvertisingSetStatus.Running));

            var errorAdvertisingSets = CacheManager.AdvertisingSets.Values
                .Count(x =>
                    x.CampaignId.Equals(campaign.Id) &&
                    x.Status.Equals(AdvertisingSetStatus.Error));

            var advertisingSets = CacheManager.AdvertisingSets.Values
                .Count(x =>
                    x.CampaignId.Equals(campaign.Id));

            var advertisings = CacheManager.Advertising.Values
                .Count(x =>
                    x.CampaignId.Equals(campaign.Id));

            if (advertisingSets.Equals(0) || advertisings.Equals(0))
            {
                campaign.Status = CampaignStatus.Unfinished;
            }
            else// if (!campaign.Status.Equals(CampaignStatus.Unfinished))
            {
                var campaignStartDate = DateOnly.FromDateTime(campaign.EcommerceStartDate.DateTime);
                var today = DateOnly.FromDateTime(DateTime.Now);

                if (campaignStartDate > today)
                {
                    campaign.Status = errorAdvertisingSets.Equals(advertisingSets)
                        ? CampaignStatus.Error
                        : CampaignStatus.NotStarted;
                }
                else
                {
                    if (campaign.EcommerceEndDate != null)
                    {
                        var campaignEndDate = DateOnly.FromDateTime(campaign.EcommerceEndDate.Value.DateTime);
                        campaign.Status = !campaign.IsActive || validAdvertisingSets.Equals(0)
                            ? errorAdvertisingSets.Equals(advertisingSets)
                                ? campaign.Status.Equals(CampaignStatus.Finished)
                                    ? CampaignStatus.Finished
                                    : CampaignStatus.Error
                                : campaign.Status.Equals(CampaignStatus.Finished)
                                    ? CampaignStatus.Finished
                                    : CampaignStatus.Paused
                            : campaignEndDate < today
                                ? CampaignStatus.Finished
                                : CampaignStatus.Running;
                    }
                    else
                    {
                        campaign.Status = campaign.IsActive && !validAdvertisingSets.Equals(0)
                            ? CampaignStatus.Running
                            : errorAdvertisingSets.Equals(advertisingSets)
                                ? CampaignStatus.Error
                                : CampaignStatus.Paused;
                    }
                }
            }

            var cachedCampaign =
                CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id.Equals(campaign.Id));
            if (cachedCampaign is null) continue;
            cachedCampaign.Status = campaign.Status;
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<CampaignCore>(cachedCampaign.SubId),
                cachedCampaign);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<CampaignCore>(),
                new PatchCacheManagerSignal<CampaignCore>
                {
                    Entity = cachedCampaign,
                    EntitySubId = cachedCampaign.SubId
                });
        }

        _context.Campaigns.UpdateRange(campaigns);

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateSingleCampaignStatus(Guid campaignId)
    {
        var cancellationToken = new CancellationToken();
        var campaign = await _context.Campaigns
            .Include(x => x.CampaignScheduling)
            .FirstOrDefaultAsync(x => x.Id == campaignId, cancellationToken);
        if (campaign is null)
        {
            return;
        }

        var validAdvertisingSets = CacheManager.AdvertisingSets.Values
            .Count(x =>
                x.CampaignId.Equals(campaign.Id) &&
                x.IsActive.Equals(true) &&
                x.Status.Equals(AdvertisingSetStatus.Running));

        var errorAdvertisingSets = CacheManager.AdvertisingSets.Values
            .Count(x =>
                x.CampaignId.Equals(campaign.Id) &&
                x.Status.Equals(AdvertisingSetStatus.Error));

        var advertisingSets = CacheManager.AdvertisingSets.Values
            .Count(x =>
                x.CampaignId.Equals(campaign.Id));

        var advertisings = CacheManager.Advertising.Values
            .Count(x =>
                x.CampaignId.Equals(campaign.Id));

        if (advertisingSets.Equals(0) || advertisings.Equals(0))
        {
            campaign.Status = CampaignStatus.Unfinished;
        }
        else if (!campaign.Status.Equals(CampaignStatus.Unfinished))
        {
            var campaignStartDate = DateOnly.FromDateTime(campaign.EcommerceStartDate.DateTime);
            var today = DateOnly.FromDateTime(DateTime.Now);

            if (campaignStartDate > today)
            {
                campaign.Status = errorAdvertisingSets.Equals(advertisingSets)
                    ? CampaignStatus.Error
                    : CampaignStatus.NotStarted;
            }
            else
            {
                if (campaign.EcommerceEndDate != null)
                {
                    var campaignEndDate = DateOnly.FromDateTime(campaign.EcommerceEndDate.Value.DateTime);
                    campaign.Status = !campaign.IsActive || validAdvertisingSets.Equals(0)
                        ? errorAdvertisingSets.Equals(advertisingSets)
                            ? campaign.Status.Equals(CampaignStatus.Finished)
                                ? CampaignStatus.Finished
                                : CampaignStatus.Error
                            : campaign.Status.Equals(CampaignStatus.Finished)
                                ? CampaignStatus.Finished
                                : CampaignStatus.Paused
                        : campaignEndDate < today
                            ? CampaignStatus.Finished
                            : CampaignStatus.Running;
                }
                else
                {
                    campaign.Status = campaign.IsActive && !validAdvertisingSets.Equals(0)
                        ? CampaignStatus.Running
                        : errorAdvertisingSets.Equals(advertisingSets)
                            ? CampaignStatus.Error
                            : CampaignStatus.Paused;
                }
            }
        }

        var cachedCampaign =
            CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id.Equals(campaign.Id));
        if (cachedCampaign is null) return;
        cachedCampaign.Status = campaign.Status;
        await _globalCacheService.SetAsync(RedisKeys.KeySettings<CampaignCore>(cachedCampaign.SubId),
            cachedCampaign);
        _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<CampaignCore>(),
            new PatchCacheManagerSignal<CampaignCore>
            {
                Entity = cachedCampaign,
                EntitySubId = cachedCampaign.SubId
            });


        _context.Campaigns.Update(campaign);

        await _context.SaveChangesAsync(cancellationToken);
    }
}