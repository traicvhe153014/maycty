﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Services;

public interface IProductGroupService
{
    Task RefreshProductGroups(Guid productFeedId);
}

public class ProductGroupService : IProductGroupService
{
    private readonly SettingsContext _context;
    private readonly IGlobalCacheService _globalCacheService;
    private readonly IMessageBusClient _messageBusClient;

    public ProductGroupService(SettingsContext context, IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService)
    {
        _context = context;
        _messageBusClient = messageBusClient;
        _globalCacheService = globalCacheService;
    }

    public async Task RefreshProductGroups(Guid productFeedId)
    {
        var productGroups = await _context.ProductGroups
            .Include(x => x.ProductGroupEntities)
            .Where(x => x.ProductFeedId == productFeedId)
            .ToListAsync();
        var products = await _context.ProductEntities
            .Include(x => x.AttributeValues)
            .Where(x => x.ProductFeedId == productFeedId && !x.Deleted)
            .ToListAsync();
        var updateProductGroupEntityEntries = new List<ProductGroupEntity>();
        var deleteProductGroupEntityEntries = new List<ProductGroupEntity>();

        foreach (var productGroup in productGroups)
        {
            var productGroupCount = 0;
            var valueHashKeys = productGroup.ProductGroupEntities?
                .Select(x => x.ProductAttributeValueHashCode)
                .Distinct()
                .ToList() ?? new List<string?>();
            foreach (var product in products)
            {
                var attributeValue =
                    product.AttributeValues?.FirstOrDefault(x => x.AttributeId == productGroup.ProductAttributeId);
                var productGroupEntity = productGroup.ProductGroupEntities?.FirstOrDefault(x => x.ProductId == product.Id);
                if (!valueHashKeys.Contains(attributeValue?.HashKey))
                {
                    if (productGroupEntity is not null)
                    {
                        productGroupEntity.Deleted = true;
                        _context.ProductGroupEntities.Update(productGroupEntity);
                        deleteProductGroupEntityEntries.Add(productGroupEntity);
                    }
                }
                else
                {
                    productGroupCount += 1;
                    if (productGroupEntity is null)
                    {
                        var entityEntry = await _context.ProductGroupEntities.AddAsync(new ProductGroupEntity
                        {
                            CreatedBy = productGroup.CreatedBy,
                            ModifiedBy = productGroup.ModifiedBy,
                            ProductId = product.Id,
                            ProductGroupId = productGroup.Id,
                            ProductAttributeValueId = attributeValue?.Id,
                            ProductAttributeValueHashCode = attributeValue?.HashKey
                        });
                        updateProductGroupEntityEntries.Add(entityEntry.Entity);
                    } 
                    else if (productGroupEntity.ProductAttributeValueHashCode != attributeValue?.HashKey)
                    {
                        productGroupEntity.ProductAttributeValueHashCode = attributeValue?.HashKey;
                        _context.ProductGroupEntities.Update(productGroupEntity);
                        updateProductGroupEntityEntries.Add(productGroupEntity);
                    }
                }
            }

            productGroup.Amount = productGroupCount;
        }
        await _context.SaveChangesAsync();

        foreach (var entity in deleteProductGroupEntityEntries)
        {
            await _globalCacheService.KeyDeleteAsync(RedisKeys.KeySettings<ProductGroupEntityCore>(entity.SubId));
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductGroupEntityCore>(), new PatchCacheManagerSignal<ProductGroupEntityCore>
            {
                Entity = null,
                EntitySubId = entity.SubId,
            });
        }
        foreach (var entity in updateProductGroupEntityEntries)
        {
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductGroupEntityCore>(entity.SubId), entity);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductGroupEntityCore>(), new PatchCacheManagerSignal<ProductGroupEntityCore>
            {
                Entity = entity,
                EntitySubId = entity.SubId,
            });
        }
    }
}