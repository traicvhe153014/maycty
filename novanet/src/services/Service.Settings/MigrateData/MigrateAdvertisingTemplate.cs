namespace Service.Settings.MigrateData;

public static class MigrateAdvertisingTemplate
{
    public static List<AdvertisingTemplate> AdvertisingTemplates => new()
    {
        new AdvertisingTemplate("Mobile banner card", 375, 180, TemplateType.MobileBannerCard),
        new AdvertisingTemplate("Interactive banner", 375, 810, TemplateType.InteractiveBanner),
        new AdvertisingTemplate("Flying carpet", 375, 810, TemplateType.FlyingCarpet),
        new AdvertisingTemplate("In read ecommerce", 375, 600, TemplateType.InReadEcommerce),
        new AdvertisingTemplate("Post in read", 375, 600, TemplateType.PostInRead),
        new AdvertisingTemplate("Catfish ecom", 375, 120, TemplateType.CatfishEcom)
    };
}