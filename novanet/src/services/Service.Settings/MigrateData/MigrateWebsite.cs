﻿namespace Service.Settings.MigrateData;

public static class MigrateWebsite
{
    public static IEnumerable<Website> Websites => new List<Website>
    {
        new()
        {
            Name = "thethao247.vn",
            Domain = "thethao247.vn",
            Url = "https://thethao247.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.thethao247.vn"
                }
            }
        },
        new()
        {
            Name = "plo.vn",
            Domain = "plo.vn",
            Url = "https://plo.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.plo.vn"
                }
            }
        },
        new()
        {
            Name = "tiepthitieudung.com",
            Description = "https://tiepthitieudung.com",
            Domain = "tiepthitieudung.com",
            Url = "https://tiepthitieudung.com",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.tiepthitieudung.vn"
                }
            }
        },
        new()
        {
            Name = "tuoitre.vn",
            Domain = "tuoitre.vn",
            Url = "https://tuoitre.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.tuoitre.vn"
                }
            }
        },
        new()
        {
            Name = "vietnamnet.vn",
            Domain = "vietnamnet.vn",
            Url = "https://vietnamnet.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.vietnamnet.vn"
                }
            }
        },
        new()
        {
            Name = "infonet.vietnamnet.vn",
            Domain = "infonet.vietnamnet.vn",
            Url = "https://infonet.vietnamnet.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.infonet.vietnamnet.vn"
                }
            }
        },
        new()
        {
            Name = "thethaovanhoa.vn",
            Domain = "thethaovanhoa.vn",
            Url = "https://thethaovanhoa.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.thethaovanhoa.vn"
                }
            }
        },
        new()
        {
            Name = "laodong.vn",
            Domain = "laodong.vn",
            Url = "https://laodong.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.laodong.vn"
                }
            }
        },
        new()
        {
            Name = "tienphong.vn",
            Domain = "tienphong.vn",
            Url = "https://tienphong.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.tienphong.vn"
                }
            }
        },
        new()
        {
            Name = "vietbao.vn",
            Domain = "vietbao.vn",
            Url = "https://vietbao.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.vietbao.vn"
                }
            }
        },
        new()
        {
            Name = "docbao.vn",
            Domain = "docbao.vn",
            Url = "https://docbao.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.docbao.vn"
                }
            }
        },
        new()
        {
            Name = "bongdaplus.vn",
            Domain = "bongdaplus.vn",
            Url = "https://bongdaplus.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.bongdaplus.vn"
                }
            }
        },
        new()
        {
            Name = "techz.vn",
            Domain = "techz.vn",
            Url = "https://techz.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.techz.vn"
                }
            }
        },
        new()
        {
            Name = "kinhtedothi.vn",
            Domain = "kinhtedothi.vn",
            Url = "https://kinhtedothi.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.kinhtedothi.vn"
                }
            }
        },
        new()
        {
            Name = "hoahoctro.tienphong.vn",
            Domain = "hoahoctro.tienphong.vn",
            Url = "https://hoahoctro.tienphong.vn",
            CreatedAt = DateTimeOffset.Now,
            ModifiedAt = DateTimeOffset.Now,
            DomainAliases = new List<DomainAlias>
            {
                new()
                {
                    Name = "*.hoahoctro.tienphong.vn"
                }
            }
        },
    };
}