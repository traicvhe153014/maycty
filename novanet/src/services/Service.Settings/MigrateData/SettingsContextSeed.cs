﻿using System.Data;
using Dapper;
using Microsoft.Data.SqlClient;
using Novanet.Core.SqlScripts;
using NovanetCore.Business.BusinessParallels;
using Polly;
using Polly.Retry;

namespace Service.Settings.MigrateData;

public class SettingsContextSeed
{
    public static async Task SeedAsync(
        SettingsContext context,
        IDbConnection connection,
        ILogger<SettingsContextSeed>? logger,
        IServiceProvider services)
    {
        var policy = CreatePolicy(logger, nameof(SettingsContextSeed));

        var catalogExisted = await connection.QueryFirstOrDefaultAsync<int>(FulltextSearchBuilder.CatalogExisted<SettingsContext>());
        var queryReferenceFulltextSearch = FulltextSearchBuilder.FulltextSearchScript<SettingsContext>();
        if (catalogExisted != 1) await connection.ExecuteAsync(queryReferenceFulltextSearch);

        await policy.ExecuteAsync(async () =>
        {
            if (!await context.ProductAttributes.AnyAsync())
                await context.ProductAttributes.AddRangeAsync(MigrateProductAttribute.ProductAttributes);

            if (!await context.AdvertisingTemplates.AnyAsync())
                await context.AdvertisingTemplates.AddRangeAsync(MigrateAdvertisingTemplate.AdvertisingTemplates);

            if (!await context.AdvertisingTemplateAttributes.AnyAsync())
                await context.AdvertisingTemplateAttributes.AddRangeAsync(MigrateAdvertisingTemplateAttribute
                    .AdvertisingTemplateAttributes);

            if (!await context.Websites.AnyAsync())
                await context.Websites.AddRangeAsync(MigrateWebsite.Websites);

            await context.SaveChangesAsync();
            
            var publisher = services.GetRequiredService<Publisher>();
            await publisher.Publish(new UpdateCacheManagerParallel(), PublishStrategy.ParallelNoWait);
            await publisher.Publish(new UpdateMetricReportManagerParallel(), PublishStrategy.ParallelNoWait);
        });
    }

    private static AsyncRetryPolicy CreatePolicy(ILogger? logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().WaitAndRetryAsync(
            retryCount: retries,
            sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
            onRetry: (exception, timeSpan, retry, ctx) =>
            {
                logger?.LogWarning(exception,
                    "[{TimeSpan}: {Prefix}] Exception {ExceptionType} with message {Message} detected on attempt {Retry} of {Retries}",
                    timeSpan, prefix, exception.GetType().Name, exception.Message, retry, retries);
            }
        );
    }
}