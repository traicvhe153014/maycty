﻿namespace Service.Settings.MigrateData;

public static class MigrateProductAttribute
{
    public static IEnumerable<ProductAttribute> ProductAttributes => new List<ProductAttribute>
    {
        // Basic product data
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Id",
            Description = "Sử dụng thuộc tính mã nhận dạng [id] để xác định chính xác từng sản phẩm",
            Required = true,
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 1
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Title",
            Description =
                " Tiêu đề là một trong những phần nổi bật nhất của quảng cáo hoặc trang thông tin miễn phí của bạn. Tiêu đề cụ thể và chính xác sẽ giúp chúng tôi giới thiệu sản phẩm của bạn cho những khách hàng thích hợp",
            Required = true,
            DataType = NovanetDataType.String,
            MaxLength = 150,
            Order = 3
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Description",
            Description =
                "Sử dụng thuộc tính nội dung mô tả [description] để giới thiệu sản phẩm cho khách hàng. Liệt kê các tính năng, thông số kỹ thuật và đặc điểm trực quan của sản phẩm. Nội dung mô tả chi tiết sẽ giúp chúng tôi giới thiệu sản phẩm của bạn cho khách hàng phù hợp",
            DataType = NovanetDataType.String,
            MaxLength = 5000,
            Required = true,
            Order = 10
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Link",
            Description =
                "Khi người dùng nhấp vào sản phẩm, họ sẽ được chuyển đến trang đích dành cho sản phẩm đó trên trang web của bạn. Đặt URL cho trang đích này bằng thuộc tính đường liên kết [link]",
            Required = true,
            DataType = NovanetDataType.Url,
            MaxLength = 4000,
            Order = 9
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ImageLink",
            Description =
                "Cung cấp URL cho hình ảnh chính về sản phẩm bằng thuộc tính đường liên kết của hình ảnh [image_link]. Khách hàng tiềm năng sẽ nhìn thấy bức ảnh đó trong quảng cáo và trang thông tin miễn phí của sản phẩm.",
            Required = true,
            DataType = NovanetDataType.Url,
            MaxLength = 4000,
            Order = 8
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "AdditionalImageLink",
            Description =
                "sử dụng thuộc tính đường liên kết của hình ảnh bổ sung [additional_image_link] để cung cấp thêm hình ảnh cho sản phẩm cùng với hình ảnh chính mà bạn cung cấp trong thuộc tính đường liên kết của hình ảnh [image_link]. Khách hàng tiềm năng có thể thấy hình ảnh bổ sung của sản phẩm và các bức ảnh này thường dùng để giới thiệu sản phẩm ở nhiều góc nhìn hoặc khi có các yếu tố giúp trưng bày sản phẩm",
            DataType = NovanetDataType.Url,
            MaxLength = 2000,
            Order = 11
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "MobileLink",
            Description =
                "Thuộc tính đường liên kết dành cho thiết bị di động [mobile_link] cho phép bạn thêm URL tới phiên bản được tối ưu hóa cho thiết bị di động của trang đích. Người dùng các thiết bị di động, như điện thoại hoặc máy tính bảng, sẽ nhìn thấy phiên bản này. Khi sử dụng thuộc tính này, bạn cũng sẽ xem được dữ liệu báo cáo bổ sung trong Merchant Center về các vấn đề tiềm ẩn liên quan đến những trang đích được tối ưu hóa cho thiết bị di động",
            DataType = NovanetDataType.Url,
            MaxLength = 2000,
            Order = 12
        },
        //
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Availability",
            Description =
                "Sử dụng thuộc tính tình trạng còn hàng [availability] để cho người dùng và Google biết sản phẩm của bạn có còn hàng hay không",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Required = true,
            Order = 7
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "AvailabilityDate",
            Description =
                "Sử dụng thuộc tính ngày có hàng [availability_date] để cho khách hàng biết thời gian vận chuyển của một sản phẩm được đặt hàng trước hoặc được giao sau",
            DataType = NovanetDataType.DateTime,
            Order = 13
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "CostOfGoodsSold",
            Description =
                "Bạn có thể sử dụng thuộc tính giá vốn hàng bán [cost_of_goods_sold] khi báo cáo lượt chuyển đổi bằng dữ liệu giỏ hàng để nhận dữ liệu báo cáo bổ sung về lợi nhuận gộp. Lưu ý: bạn phải báo cáo thông tin cấp mặt hàng của từng lượt chuyển đổi để sử dụng loại báo cáo này",
            DataType = NovanetDataType.Double,
            Order = 14
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ExpirationDate",
            Description =
                "Sử dụng thuộc tính ngày hết hạn [expiration_date] để sản phẩm ngừng xuất hiện vào một ngày cụ thể. Ví dụ: bạn có thể cần dùng thuộc tính này cho các sản phẩm theo mùa hoặc có số lượng có hạn",
            DataType = NovanetDataType.DateTime,
            Order = 15
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Price",
            Description =
                "Sử dụng thuộc tính giá [price] để cho người dùng biết giá mà bạn tính cho sản phẩm của mình. Người dùng sẽ nhìn thấy thông tin này",
            DataType = NovanetDataType.Double,
            Required = true,
            Order = 5
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "SalePrice",
            Description =
                "Sử dụng thuộc tính giá ưu đãi [sale_price] để khách hàng biết được giá mà bạn tính cho sản phẩm trong thời gian giảm giá. Trong thời gian giảm giá, giá ưu đãi của bạn sẽ hiển thị là giá hiện tại. Nếu giá gốc và giá ưu đãi của bạn đáp ứng một số yêu cầu, cả hai loại giá có thể sẽ cùng xuất hiện, qua đó giúp mọi người thấy rõ sự khác biệt",
            DataType = NovanetDataType.Double,
            Order = 6
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "SalePriceEffectiveDate",
            Description =
                "Sử dụng thuộc tính ngày giá ưu đãi có hiệu lực [sale_price_effective_date] để thông báo cho chúng tôi thời gian bạn muốn giới thiệu một mức giá ưu đãi cụ thể cho người dùng",
            DataType = NovanetDataType.DateTime,
            Order = 16
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "UnitPricingMeasure",
            Description =
                "Hãy sử dụng thuộc tính số lượng đo lường để định giá theo đơn vị [unit_pricing_measure] để xác định số đo và kích thước của sản phẩm. Giá trị này giúp người dùng hiểu chính xác chi phí trên mỗi đơn vị sản phẩm",
            DataType = NovanetDataType.String,
            Order = 17
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "UnitPricingBaseMeasure",
            Description =
                "The unit pricing base measure [unit_pricing_base_measure] attribute lets you include the denominator for your unit price",
            DataType = NovanetDataType.String,
            Order = 18
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Installment",
            Description =
                "Sử dụng thuộc tính trả góp [installment] để giới thiệu cho khách hàng chi tiết về gói thanh toán trả góp hằng tháng mà bạn hỗ trợ cho sản phẩm của mình",
            DataType = NovanetDataType.Double,
            Order = 19
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "SubscriptionCost",
            Description =
                "Sử dụng thuộc tính phí thuê bao [subscription_cost] để bao gồm các thông tin chi tiết về gói thanh toán hằng tháng hoặc hằng năm, trong đó bao gồm một hợp đồng dịch vụ viễn thông đi kèm với sản phẩm không dây (điện thoại di động hoặc máy tính bảng) của bạn",
            DataType = NovanetDataType.Double,
            Order = 20
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "LoyaltyPoints",
            Description =
                "Hãy sử dụng thuộc tính điểm khách hàng thân thiết [loyalty_points] để chỉ định số điểm và loại điểm mà khách hàng nhận được khi mua sản phẩm của bạn",
            DataType = NovanetDataType.Double,
            Order = 21
        },
        // Product Portfolio
        new()
        {
            Id = Guid.NewGuid(),
            Name = "GoogleProductCategory",
            Description =
                "Tất cả các sản phẩm được tự động chỉ định vào một danh mục sản phẩm theo phân loại sản phẩm thay đổi liên tục của Google. Cung cấp tiêu đề và mô tả chất lượng cao, đúng chủ đề, cũng như thông tin chính xác về giá cả, thương hiệu và GTIN sẽ góp phần đảm bảo sản phẩm của bạn được phân loại chính xác. Thuộc tính danh mục sản phẩm của Google [google_product_category] là không bắt buộc và có thể dùng để thay thế cách phân loại tự động của Google trong một số trường hợp",
            DataType = NovanetDataType.String,
            Order = 22
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductType",
            Description =
                "Sử dụng thuộc tính loại sản phẩm [product_type] để cung cấp cách phân loại sản phẩm của riêng bạn trong dữ liệu sản phẩm. Không giống như thuộc tính danh mục sản phẩm của Google [google_product_category], nghĩa là sử dụng một tập hợp những loại sản phẩm đã được xác định sẵn, bạn sẽ chọn giá trị để thêm vào loại sản phẩm. Các giá trị mà bạn gửi có thể dùng để sắp xếp việc đặt giá thầu và báo cáo trong chiến dịch Mua sắm Google Ads",
            DataType = NovanetDataType.String,
            MaxLength = 150,
            Order = 2
        },
        // Product Identifier
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Brand",
            Description =
                "Bạn có thể sử dụng thuộc tính thương hiệu [brand] để cho biết tên thương hiệu của sản phẩm. Thương hiệu giúp mọi người nhận dạng sản phẩm của bạn và là thông tin khách hàng sẽ nhìn thấy. Tên thương hiệu phải thể hiện rõ ràng ở mặt trước bao bì hoặc nhãn sản phẩm. Tên thương hiệu là một phần không thể thiếu trên bao bì và không được thêm một cách giả tạo vào hình ảnh sản phẩm",
            Required = false,
            DataType = NovanetDataType.String,
            MaxLength = 70,
            Order = 23
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Gtin",
            Description =
                "Có thể dùng thuộc tính GTIN (Mã số sản phẩm thương mại toàn cầu) [gtin] để gửi Mã số sản phẩm thương mại toàn cầu (GTIN). GTIN là mã số duy nhất nhận dạng sản phẩm của bạn. Mã số cụ thể này giúp chúng tôi làm cho quảng cáo hoặc trang thông tin không tính phí của bạn phong phú hơn và giúp khách hàng dễ dàng tìm thấy sản phẩm của bạn. Những sản phẩm bạn gửi mà không có số nhận dạng sản phẩm duy nhất sẽ khó phân loại và có thể không đủ điều kiện để cho mọi tính năng hoặc chương trình của Mua sắm",
            Required = false,
            DataType = NovanetDataType.Double,
            MaxLength = 50,
            Order = 24
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Mpn",
            Description =
                " Có thể sử dụng thuộc tính MPN (Mã số linh kiện của nhà sản xuất) [mpn] để gửi MPN của sản phẩm. MPN dùng để nhận dạng chính xác một sản phẩm cụ thể trong số tất cả các sản phẩm của cùng một nhà sản xuất. Vì có thể người mua sắm sẽ tìm thông tin bằng một MPN cụ thể, nên việc cung cấp MPN có thể giúp đảm bảo sản phẩm của bạn xuất hiện trong các trường hợp có liên quan",
            Required = false,
            DataType = NovanetDataType.String,
            MaxLength = 70,
            Order = 25
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "IdentifierExists",
            Description =
                "Sử dụng thuộc tính có mã nhận dạng [identifier_exists] để cho biết là sản phẩm của bạn không có số nhận dạng sản phẩm duy nhất (UPI). Gửi số nhận dạng sản phẩm duy nhất bằng các thuộc tính GTIN (Mã số sản phẩm thương mại toàn cầu) [gtin], MPN (Mã số linh kiện của nhà sản xuất) [mpn] và thương hiệu [brand]",
            DataType = NovanetDataType.Boolean,
            Order = 26
        },
        // Detailed Product Description
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Condition",
            Description =
                "Sử dụng thuộc tính tình trạng [condition] để cho khách hàng tiềm năng biết tình trạng của sản phẩm mà bạn đang bán. Bạn phải đặt đúng giá trị này vì giá trị này dùng để tinh chỉnh kết quả tìm kiếm",
            DataType = NovanetDataType.String,
            Order = 4,
            Required = true
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Adult",
            Description =
                "có thể sử dụng thuộc tính người lớn [adult] để cho biết các sản phẩm là dành riêng cho người lớn vì những sản phẩm này chứa nội dung người lớn như ảnh khỏa thân, nội dung khiêu dâm hoặc nội dung nhằm kích thích hoạt động tình dục",
            DataType = NovanetDataType.Boolean,
            Order = 27
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "MultiPack",
            Description =
                "Bạn có thể sử dụng thuộc tính lô sản phẩm [multipack] để cho biết rằng bạn đã nhóm nhiều sản phẩm giống nhau để bán dưới dạng một sản phẩm",
            DataType = NovanetDataType.Number,
            Order = 28
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "IsBundle",
            Description =
                "Sử dụng thuộc tính gói [is_bundle] để cho biết bạn đã tạo một gói: một sản phẩm chính mà bạn đã nhóm với các sản phẩm khác, bán cùng nhau dưới dạng một gói với một mức giá duy nhất",
            DataType = NovanetDataType.Boolean,
            Order = 29
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "EnergyEfficiencyClass",
            Description =
                "Sử dụng các thuộc tính cấp hiệu suất năng lượng [energy_efficiency_class], cấp hiệu suất năng lượng tối thiểu [min_energy_efficiency_class] và cấp hiệu suất năng lượng tối đa [max_energy_efficiency_class] để giới thiệu nhãn năng lượng của sản phẩm cho khách hàng",
            DataType = NovanetDataType.String,
            Order = 30
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "MinEnergyEfficiencyClass",
            Description =
                "Sử dụng các thuộc tính cấp hiệu suất năng lượng [energy_efficiency_class], cấp hiệu suất năng lượng tối thiểu [min_energy_efficiency_class] và cấp hiệu suất năng lượng tối đa [max_energy_efficiency_class] để giới thiệu nhãn năng lượng của sản phẩm cho khách hàng",
            DataType = NovanetDataType.String,
            Order = 31
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "AgeGroup",
            Description =
                "Bạn có thể sử dụng thuộc tính nhóm tuổi [age_group] để đặt thông tin nhân khẩu học mà sản phẩm của bạn nhắm tới",
            Required = false,
            DataType = NovanetDataType.String,
            Order = 32
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Color",
            Description =
                "Có thể sử dụng thuộc tính màu sắc [color] để mô tả màu sắc của sản phẩm. Thông tin này giúp tạo ra các bộ lọc chính xác để khách hàng có thể sử dụng nhằm thu hẹp kết quả tìm kiếm. Nếu sản phẩm của bạn có các biến thể thay đổi theo màu sắc, hãy sử dụng thuộc tính này để cung cấp thông tin đó",
            Required = false,
            DataType = NovanetDataType.String,
            Order = 33
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Gender",
            Description =
                "Chỉ ra giới tính mà sản phẩm của bạn nhắm đến bằng cách sử dụng thuộc tính giới tính [gender]",
            Required = false,
            DataType = NovanetDataType.String,
            Order = 34
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Material",
            Description =
                "Sử dụng thuộc tính chất liệu [material] để mô tả loại vải hoặc chất liệu chính dùng để làm ra sản phẩm",
            Required = false,
            DataType = NovanetDataType.String,
            MaxLength = 200,
            Order = 35
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Pattern",
            Description = "Sử dụng thuộc tính hoa văn [pattern] để mô tả hoa văn hoặc hình in trên sản phẩm của bạn",
            Required = false,
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 36
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Size",
            Description = "Sử dụng thuộc tính loại kích thước [size_type] để mô tả kiểu kích thước sản phẩm của bạn",
            Required = false,
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 37
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "SizeType",
            Description = "sử dụng thuộc tính loại kích thước [size_type] để mô tả kiểu kích thước sản phẩm của bạn",
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 38
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "SizeSystem",
            Description =
                "Khi dùng thuộc tính hệ thống kích thước [size_system], bạn có thể nêu rõ sản phẩm của bạn sử dụng hệ thống kích thước của quốc gia nào",
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 39
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ItemGroupId",
            Description =
                "Hãy sử dụng thuộc tính mã nhóm mặt hàng [item_group_id] để gộp nhóm các biến thể sản phẩm trong dữ liệu sản phẩm",
            DataType = NovanetDataType.String,
            Required = false,
            MaxLength = 50,
            Order = 40
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductLength",
            Description =
                "Sử dụng thuộc tính chiều dài sản phẩm [product_length], chiều rộng sản phẩm [product_width], chiều cao sản phẩm [product_height] và trọng lượng sản phẩm [product_weight] để nêu rõ các thông tin đo lường của sản phẩm mà bạn kinh doanh",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 41
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductWidth",
            Description =
                "Sử dụng thuộc tính chiều dài sản phẩm [product_length], chiều rộng sản phẩm [product_width], chiều cao sản phẩm [product_height] và trọng lượng sản phẩm [product_weight] để nêu rõ các thông tin đo lường của sản phẩm mà bạn kinh doanh",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 42
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductHeight",
            Description =
                "Sử dụng thuộc tính chiều dài sản phẩm [product_length], chiều rộng sản phẩm [product_width], chiều cao sản phẩm [product_height] và trọng lượng sản phẩm [product_weight] để nêu rõ các thông tin đo lường của sản phẩm mà bạn kinh doanh",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 43
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductWeight",
            Description =
                "Sử dụng thuộc tính chiều dài sản phẩm [product_length], chiều rộng sản phẩm [product_width], chiều cao sản phẩm [product_height] và trọng lượng sản phẩm [product_weight] để nêu rõ các thông tin đo lường của sản phẩm mà bạn kinh doanh",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 44
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductDetail",
            Description =
                "Sử dụng thuộc tính product_detail [chi_tiết_sản_phẩm] để cung cấp thông số kỹ thuật hoặc các chi tiết bổ sung. Bạn có thể mô tả bất kỳ đặc tính nào của sản phẩm chưa được các thuộc tính khác biểu thị rõ ràng",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 45
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ProductHighlight",
            Description =
                "Sử dụng thuộc tính điểm nổi bật của sản phẩm [product_highlight] để cung cấp danh sách ngắn, có dấu đầu dòng về các điểm nổi bật phù hợp nhất của sản phẩm",
            DataType = NovanetDataType.String,
            MaxLength = 150,
            Order = 46
        },
        // Shopping campaigns and other configurations
        new()
        {
            Id = Guid.NewGuid(),
            Name = "AdsRedirect",
            Description =
                "Hãy dùng thuộc tính quảng cáo chuyển hướng [ads_redirect] để chỉ định các thông số bổ sung liên quan đến trang đích trên trang sản phẩm mà bạn muốn sử dụng cho Quảng cáo mua sắm",
            DataType = NovanetDataType.String,
            MaxLength = 2000,
            Order = 47
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "CustomLabel04",
            Description =
                "Các nhãn tùy chỉnh, từ nhãn tùy chỉnh 0 [custom_label_0] đến nhãn tùy chỉnh 4 [custom_label_4], cho phép bạn tạo các bộ lọc cụ thể để sử dụng trong chiến dịch Mua sắm",
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 48
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "PromotionId",
            Description =
                "Đối với các nhà bán lẻ sử dụng chương trình khuyến mãi, thuộc tính mã khuyến mãi [promotion_id] có thể được dùng trong cả dữ liệu sản phẩm và dữ liệu khuyến mãi để ghép sản phẩm với đúng chương trình khuyến mãi",
            DataType = NovanetDataType.String,
            MaxLength = 50,
            Order = 49
        },
        // Destination
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ExcludedDestination",
            Description =
                "Sử dụng thuộc tính vị trí không được phép xuất hiện [excluded_destination] để ngăn sản phẩm của bạn xuất hiện ở những vị trí đã chọn",
            DataType = NovanetDataType.String,
            Order = 50
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "IncludedDestination",
            Description =
                "Sử dụng thuộc tính vị trí được phép xuất hiện [included_destination] (cùng với thuộc tính vị trí không được phép xuất hiện [excluded_destination]) để kiểm soát vị trí sản phẩm xuất hiện",
            DataType = NovanetDataType.String,
            Order = 51
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShoppingAdsExcludedCountry",
            Description =
                "Sử dụng thuộc tính quốc gia bị loại trừ khỏi Quảng cáo mua sắm [shopping_ads_excluded_country] để kiểm soát những quốc gia nơi bạn quảng cáo sản phẩm thông qua Quảng cáo mua sắm",
            DataType = NovanetDataType.String,
            Order = 52
        },
        // Transport
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Shipping",
            Description =
                "Thuộc tính phí vận chuyển [shipping] cho phép bạn cung cấp thông tin về tốc độ và chi phí vận chuyển của một sản phẩm",
            DataType = NovanetDataType.String,
            Order = 53
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShippingLabel",
            Description =
                "Nhãn mà bạn chỉ định cho một sản phẩm để giúp chỉ định phí vận chuyển chính xác trong chế độ cài đặt tài khoản Merchant Center",
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 54
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShippingWeight",
            Description =
                "Thuộc tính trọng lượng vận chuyển [shipping_weight] cho phép bạn đưa thông tin về trọng lượng vào để sử dụng khi tính toán phí vận chuyển thông qua phần cài đặt trong Merchant Center",
            DataType = NovanetDataType.String,
            Order = 55
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShippingLength",
            Description =
                "Nhập kích thước của gói sản phẩm bằng cách sử dụng các thuộc tính chiều dài vận chuyển [shipping_length], chiều rộng vận chuyển [shipping_width] và chiều cao vận chuyển [shipping_height]",
            DataType = NovanetDataType.String,
            Order = 56
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShippingWidth",
            Description =
                "Nhập kích thước của gói sản phẩm bằng cách sử dụng các thuộc tính chiều dài vận chuyển [shipping_length], chiều rộng vận chuyển [shipping_width] và chiều cao vận chuyển [shipping_height]",
            DataType = NovanetDataType.String,
            Order = 57
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShippingHeight",
            Description =
                "Nhập kích thước của gói sản phẩm bằng cách sử dụng các thuộc tính chiều dài vận chuyển [shipping_length], chiều rộng vận chuyển [shipping_width] và chiều cao vận chuyển [shipping_height]",
            DataType = NovanetDataType.String,
            Order = 58
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "ShipsFromCountry",
            Description =
                "Để cho chúng tôi biết bạn thường gửi sản phẩm từ quốc gia nào, bạn có thể dùng thuộc tính quốc gia gửi hàng [ships_from_country]",
            DataType = NovanetDataType.String,
            MaxLength = 2,
            Order = 59
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "TransitTimeLabel",
            Description =
                "Nếu các nhóm sản phẩm khác nhau có thời gian vận chuyển khác nhau, hãy dùng thuộc tính nhãn thời gian vận chuyển [transit_time_label] và áp dụng thuộc tính này cho các sản phẩm tương ứng trong nguồn cấp dữ liệu của bạn",
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 60
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "MaxHandlingTime",
            Description =
                "Sử dụng thuộc tính thời gian xử lý tối thiểu [min_handling_time] và thời gian xử lý tối đa [max_handling_time] để giúp chúng tôi cung cấp cho người dùng thông tin chính xác về thời gian để giao sản phẩm đến nơi",
            DataType = NovanetDataType.Number,
            Order = 61
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "MinHandlingTime",
            Description =
                "Sử dụng thuộc tính thời gian xử lý tối thiểu [min_handling_time] và thời gian xử lý tối đa [max_handling_time] để giúp chúng tôi cung cấp cho người dùng thông tin chính xác về thời gian để giao sản phẩm đến nơi",
            DataType = NovanetDataType.Number,
            Order = 62
        },
        // Tax
        new()
        {
            Id = Guid.NewGuid(),
            Name = "Tax",
            Description =
                "Thuộc tính thuế [tax] cho phép bạn thay thế các chế độ cài đặt về thuế ở Hoa Kỳ mà bạn đã tạo trong Merchant Center",
            DataType = NovanetDataType.String,
            Order = 63
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "TaxCategory",
            Description =
                "Thuộc tính danh mục thuế [tax_category] cho phép bạn sắp xếp sản phẩm theo các quy tắc thuế tùy chỉnh",
            DataType = NovanetDataType.String,
            MaxLength = 100,
            Order = 64
        },
        new()
        {
            Id = Guid.NewGuid(),
            Name = "IsActivated",
            Description =
                "Thuộc tính trạng thái cho phép bạn bật tắt sản phẩm trong chiến dịch quảng cáo",
            DataType = NovanetDataType.Boolean,
            Order = 65
        },
    };
}