﻿namespace Service.Settings.MigrateData;

public class MigrateAdvertisingTemplateAttribute
{
    public static IEnumerable<AdvertisingTemplateAttribute> AdvertisingTemplateAttributes =>
        new List<AdvertisingTemplateAttribute>
        {
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "useBannerTemplate",
                DisplayedName = "Sử dụng mẫu banner có sẵn",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.Url,
                Name = "backgroundImageUrl",
                DisplayedName = "Ảnh nền",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "productName",
                DisplayedName = "Tên sản phẩm",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "showDiscountedPrice",
                DisplayedName = "Hiển thị giá khuyến mại và giảm giá",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "showFreeDelivery",
                DisplayedName = "Hiển thị Freeship cho tất cả sản phẩm",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "avatarUrl",
                DisplayedName = "Ảnh Avatar",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "avatarName",
                DisplayedName = "Tên Avatar",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "postContent",
                DisplayedName = "Nội dung",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "bannerImage",
                DisplayedName = "Ảnh nền",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "expandedContentUrl",
                DisplayedName = "Ảnh/Html5 mở rộng",
                CampaignType = CampaignType.Ecommerce
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "title",
                DisplayedName = "Tiêu đề",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "description",
                DisplayedName = "Nội dung",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "logoUrl",
                DisplayedName = "Logo",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "shortcutNotification",
                DisplayedName = "Thông báo rút gọn",
                CampaignType = CampaignType.Display,
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "extendNotification",
                DisplayedName = "Thông báo mở rộng",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "isCta",
                DisplayedName = "Sử dụng CTA",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "cta",
                DisplayedName = "Nội dung CTA",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "ctaUrl",
                DisplayedName = "URL CTA",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "isCustomImage",
                DisplayedName = "Sử dụng hình ảnh trong banner mở rộng",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "images",
                DisplayedName = "Hình ảnh trong banner mở rộng",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.Boolean,
                Name = "isDelay",
                DisplayedName = "Có độ trễ",
                CampaignType = CampaignType.Display
            },
            new()
            {
                DataType = NovanetDataType.String,
                Name = "delay",
                DisplayedName = "Độ trễ",
                CampaignType = CampaignType.Display
            },
        };
}