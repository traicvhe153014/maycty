﻿namespace Service.Settings.Application.Responses;

public class ProductFeedErrorResponse
{
    public Guid Id { get; set; }

    public bool Success { get; set; }

    public string? ErrorMessage { get; set; }
    
    public string? ErrorType { get; set; }
    
    public string? SheetPosition { get; set; }

    public string? ProductIdValue { get; set; }
    
    public string? AttributeValue { get; set; }

    public Guid? ProductEntityId { get; set; }

    public Guid? ProductAttributeValueId { get; set; }
    
    public Guid? ProductFeedId { get; set; }
    
}