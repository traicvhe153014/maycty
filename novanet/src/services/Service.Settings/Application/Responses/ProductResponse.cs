﻿namespace Service.Settings.Application.Responses;

public class ListProductResponse
{
    public List<ProductResponse> Data { get; set; } = default!;
    
    public Dictionary<string, object?>? Summary { get; set; }
}

public class ProductResponse
{
    public Guid ProductId { get; set; }
    public Guid ProductFeedId { get; set; }

    public string? ProductFeedName { get; set; }
    
    public List<Campaign>? RunningCampaigns { get; set; }

    public bool IsActive { get; set; }
    
    public ProductEntityStatus? Status { get; set; }

    public Dictionary<string, object?>? Data { get; set; }

    public DateTimeOffset? ModifiedAt { get; set; }
}