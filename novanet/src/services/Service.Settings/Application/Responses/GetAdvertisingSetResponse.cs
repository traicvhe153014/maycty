﻿namespace Service.Settings.Application.Responses;

public class GetAdvertisingSetResponse : IMapFrom<AdvertisingSet>
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    public bool IsActive { get; set; }
    public AdvertisingSetStatus Status { get; set; }
    public Guid AdvertisingSetMappingId { get; set; }
    public Guid? CampaignId { get; set; }
    public bool ApplyAllProductGroups { get; set; }
    public bool HasProhibitedCategories { get; set; }
    public bool HasObject { get; set; }
    public RemarketingType? RemarketingType { get; set; }
    public int? RemarketingTime { get; set; }
    public int? TimeOnDisplay { get; set; }
    public int? AdImpressions { get; set; }
    public bool? TargetingMarketingByProduct { get; set; }
    public bool? Uniformed { get; set; }
    public double? UniformedPrice { get; set; }
    public UniformedUnit? UniformedUnit { get; set; }
    public bool? ClickedAdvertising { get; set; }
    public bool? Viewer { get; set; }
    public int? ViewedMax { get; set; }
    public Campaign? Campaign { get; set; }
    public List<AdvertisingSetProductGroup>? AdvertisingSetProductGroups { get; set; }
    public List<AdvertisingSetObject>? AdvertisingSetObjects { get; set; }
    public List<AdvertisingSetLocation>? AdvertisingSetLocations { get; set; }
    public List<AdvertisingSetAge>? AdvertisingSetAges { get; set; }
    public List<AdvertisingSetCategory>? AdvertisingSetCategories { get; set; }
    public List<AdvertisingSetGender>? AdvertisingSetGenders { get; set; }
    public List<AdvertisingSetProhibitedCategory>? AdvertisingSetProhibitedCategories { get; set; }
    public List<WebsitePrice>? WebsitePrices { get; set; }
    public List<AdvertisingSetScheduling>? AdvertisingSetScheduling { get; set; }
    public List<AdvertisingSetWebsite>? AdvertisingSetWebsites { get; set; }
    public bool? ExcludedWebsite { get; set; }
    public bool ScheduledAllTime { get; set; }
    public Guid EcommerceProductFeedId { get; set; }
}