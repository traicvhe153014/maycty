﻿namespace Service.Settings.Application.Responses;

public class CreateZoneResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public int Width { get; set; }

    public int Height { get; set; }
    
    public CampaignType CampaignType { get; set; }

    public bool UsingBackup { get; set; }

    public string Code { get; set; } = default!;

    public Guid WebsiteId { get; set; }
}