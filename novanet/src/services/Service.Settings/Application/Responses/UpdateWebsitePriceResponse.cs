﻿namespace Service.Settings.Application.Responses;

public class UpdateWebsitePriceResponse
{
    public Guid Id { get; set; }
    
    public Guid? WebsiteId { get; set; }
    
    public Guid? ZoneId { get; set; }

    public PublisherPriceType PublisherPriceType { get; set; }
    
    public bool IsActive { get; set; }
    
    public WebsitePriceStatus Status { get; set; }

    public Guid? PriceId { get; set; }
    
    public double SellPrice { get; set; }
    
    public PriceType SellPriceType { get; set; }
    
    public double BuyPrice { get; set; }
    
    public PriceType BuyPriceType { get; set; }

    public DateTimeOffset StartDate { get; set; }

}