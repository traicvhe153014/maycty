﻿namespace Service.Settings.Application.Responses;

public class ProductGroupResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public int Amount { get; set; }

    public DateTimeOffset Modified { get; set; }

    public ProductFeed? ProductFeed { get; set; }

    public IEnumerable<AdvertisingSetResponse>? AdvertisingSetRunning { get; set; }
    
    public List<ProductGroupEntityResponse>? ProductGroupEntities { get; set; } = default!;
    
    public int RunningProducts { get; set; }
    
    public int StoppedProducts { get; set; }
}