﻿namespace Service.Settings.Application.Responses;

public class CreateProductFeedResponse
{
    public Guid? Id { get; set; }

    public string Name { get; set; } = default!;

    public string? ErrorMessage { get; set; }
    
    public CreateProductFeedResponse(string? errorMessage)
    {
        ErrorMessage = errorMessage;
    }

    public CreateProductFeedResponse(Guid? id, string name)
    {
        Id = id;
        Name = name;
    }
}