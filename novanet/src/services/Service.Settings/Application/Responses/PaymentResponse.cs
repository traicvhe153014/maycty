﻿namespace Service.Settings.Application.Responses;

public class PaymentResponse
{
    public string Email { get; set; }
    public string? Customer { get; set; }

    public double Amount { get; set; }
    
    public string? Note { get; set; }

    public DateTimeOffset CreatedAt { get; set; }
}