﻿namespace Service.Settings.Application.Responses;

public class ProductOverviewResponse
{
    public int Total { get; set; }
    
    public int? Running { get; set; }

    public int? Deactivated { get; set; }
    
    public int? Error { get; set; }
    
    public int? OutOfStock { get; set; }
}