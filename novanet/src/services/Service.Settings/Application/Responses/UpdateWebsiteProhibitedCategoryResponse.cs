namespace Service.Settings.Application.Responses;

public class UpdateWebsiteProhibitedCategoryResponse
{
    public Guid Id { get; set; }
    
    public Guid ProhibitedCategoryId { get; set; }
    
    public Guid WebsiteId { get; set; }
}