namespace Service.Settings.Application.Responses;

public class DomainAliasesResponse
{
    public List<DomainAlias> Data { get; set; }
}