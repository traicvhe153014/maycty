﻿namespace Service.Settings.Application.Responses;

public class ListProductAttributeValueResponse
{
    public List<ProductAttributeValueResponse> Data { get; set; } = default!;
    
    public Dictionary<string, object?>? Summary { get; set; }
}

public class ProductAttributeValueResponse
{
    public Object? ProductAttributeHashcodeValue { get; set; }
    
    public string? ProductAttributeValue { get; set; }

    public int Amount { get; set; }

}