﻿using ProductFeedPermission = NovanetCore.Business.BusinessDomain.Service.Settings.ProductFeedPermission;
using SourceType = NovanetCore.Business.BusinessDomain.Service.Settings.SourceType;

namespace Service.Settings.Application.Responses;

public class ProductFeedResponse
{
    public Guid? Id { get; set; }
    
    public string Name { get; set; } = default!;

    public SourceType SourceType { get; set; }

    public string SourcePath { get; set; } = default!;

    public ProductFeedPermission Permission { get; set; }

    public DateTimeOffset ModifiedAt { get; set; }

    public int ActiveProduct { get; set; }
    
    public int InActiveProduct { get; set; }
    
    public List<ProductEntityResponse>? ProductEntities { get; set; }
    
    public List<ProductFeedErrorResponse>? ProductStatusList { get; set; }

}