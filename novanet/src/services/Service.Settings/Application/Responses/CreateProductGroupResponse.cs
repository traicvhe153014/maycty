﻿namespace Service.Settings.Application.Responses;

public class CreateProductGroupResponse
{
    public Guid? Id { get; set; }

    public string Name { get; set; } = default!;
    
    public string? ErrorMessage { get; set; }

    public CreateProductGroupResponse(string errorMessage)
    {
        ErrorMessage = errorMessage;
    }

    public CreateProductGroupResponse(Guid id, string name)
    {
        Id = id;
        Name = name;
    }
}