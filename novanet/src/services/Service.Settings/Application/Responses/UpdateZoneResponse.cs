﻿namespace Service.Settings.Application.Responses;

public class UpdateZoneResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    public CampaignType CampaignType { get; set; }
    public bool UsingBackup { get; set; }
    public string? BackupCode { get; set; }
    public ZoneBackupType? BackupType { get; set; }
    public Guid WebsiteId { get; set; }
    public string? RedirectLink { get; set; }
}