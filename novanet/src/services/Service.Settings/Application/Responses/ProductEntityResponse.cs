﻿namespace Service.Settings.Application.Responses;

public class ProductEntityResponse
{
    public Guid Id { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public Guid ProductFeedId { get; set; }

    public Dictionary<string, object?>? AttributeValues { get; set; }
}