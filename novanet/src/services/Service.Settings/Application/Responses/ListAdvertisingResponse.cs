﻿using System.ComponentModel;

namespace Service.Settings.Application.Responses;

public class ListAdvertisingResponse
{
    public List<AdvertisingResponse> Data { get; set; }
    public Dictionary<string, object?> Summary { get; set; }
}

public partial class AdvertisingResponse
{
    public Guid Id { get; set; }
    
    [Description("Tên quảng cáo")]
    public string Name { get; set; } = default!;
    
    [Description("Bật/Tắt")]
    public bool IsActive { get; set; }
    
    [Description("Trạng Thái")]
    public AdvertisingStatus? Status { get; set; }
    public CampaignType? CampaignType { get; set; }
    public Guid? CampaignId { get; set; }
    
    [Description("Tên chiến dịch")]
    public string? CampaignName { get; set; }
    public Guid? ProductFeedId { get; set; }
    public Guid? AdvertisingSetId { get; set; }
    
    [Description("Tên nhóm quảng cáo")]
    public string? AdvertisingSetName { get; set; }
    
    [Description("URL Đích")]
    public string? RedirectLink { get; set; } = default!;
    public Dictionary<string, object?> Data { get; set; } = default!;
    public List<AdvertisingAppliedTemplateResponse>? Templates { get; set; }
}

public class AdvertisingAppliedTemplateResponse
{
    public Guid? TemplateId { get; set; }
    public string? TemplateName { get; set; }
    public TemplateType? TemplateType { get; set; }
    
    public int? Height { get; set; }
    
    public int? Width { get; set; }

    public Dictionary<string, object?> Configurations { get; set; } = default!;
}
