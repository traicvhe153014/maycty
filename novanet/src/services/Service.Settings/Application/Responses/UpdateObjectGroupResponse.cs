﻿namespace Service.Settings.Application.Responses;

public class UpdateObjectGroupResponse
{
    public Guid? Id { get; set; }

    public string Name { get; set; } = default!;
    
    public string? ErrorMessage { get; set; }

    public UpdateObjectGroupResponse(string errorMessage)
    {
        ErrorMessage = errorMessage;
    }

    public UpdateObjectGroupResponse(Guid id, string name)
    {
        Id = id;
        Name = name;
    }
}