﻿namespace Service.Settings.Application.Responses;

public class GetCampaignResponse : IMapFrom<Campaign>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public CampaignType CampaignType { get; set; } = CampaignType.Ecommerce;

    public bool IsActive { get; set; }

    public CampaignStatus Status { get; set; }

    public int ShowSpeed { get; set; }

    public double EcommerceAmountSpent { get; set; }

    public double EcommerceDailyBudget { get; set; }

    public Guid EcommerceProductFeedId { get; set; }

    public DateTimeOffset EcommerceStartDate { get; set; }

    public DateTimeOffset? EcommerceEndDate { get; set; }

    public bool? EcommerceScheduled { get; set; }
    
    public bool EcommerceScheduledAllTime { get; set; }

    public List<CampaignScheduling>? CampaignScheduling { get; set; }
    
    public double TotalCost { get; set; }
}