﻿using System.ComponentModel;

namespace Service.Settings.Application.Responses;

public class ListCampaignResponse
{
    public List<CampaignResponse> Data { get; set; } = default!;
    public Dictionary<string, object?>? Summary { get; set; } 
}

public class CampaignResponse
{
    public Guid Id { get; set; }
    
    [Description("Tên chiến dịch")]
    public string? Name { get; set; }
    
    [Description("Bật tắt")]
    public bool IsActive { get; set; }
    
    [Description("Trạng thái")]
    public CampaignStatus Status { get; set; }
    
    [Description("Loại QC")]
    public CampaignType CampaignType { get; set; }
    
    [Description("Data")]
    public Dictionary<string, object?> Data { get; set; } = default!;
    
    [Description("Ngày bắt đầu")]
    public DateTimeOffset StartDate { get; set; }
    
    [Description("Ngày kết thúc")]
    public DateTimeOffset? EndDate { get; set; }
}
