﻿using NovanetCore.Business.BusinessDomain.Service.Identity;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace Service.Settings.Application.Responses;


public class ListWebsiteResponse
{
    public List<WebsiteResponse> Data { get; set; }
    public int Total { get; set; }
}
public class WebsiteResponse
{
    public Guid Id { get; set; } = default!;

    public string Name { get; set; } = default!;

    public string? Description { get; set; }

    public Guid PublisherId { get; set; }

    public string? PublisherEmail { get; set; }
    
    public string PublisherName { get; set; }
    
    public string PublisherPhoneNumber { get; set; } = default!;

    public string Domain { get; set; } = default!;
    
    public string Url { get; set; } = default!;

    public string? Keywords { get; set; }

    public int? AlexaRank { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset ModifiedAt { get; set; }
    
    public WebsiteStatus Status { get; set; }
    
    public bool Active { get; set; }
    
    public List<DomainAlias>? DomainAliases { get; set; }
    
    public List<ProhibitedCategoryCore>? ProhibitedCategory { get; set; }

    public int SummaryZoneReport { get; set; }
    
    public int SummaryCampaignRunning { get; set; }

}