﻿namespace Service.Settings.Application.Responses;

public class UpdateAdvertisingSetProductGroup
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid? ProductGroupId { get; set; }
    
    public bool IsActive { get; set; }
    
    public AdvertisingSetStatus Status { get; set; }

    public AdvertisingSetStatus? AdvertisingSetStatus { get; set; }
}