﻿namespace Service.Settings.Application.Responses;

public class UpdateProductGroupResponse
{
    public Guid? Id { get; set; }

    public string Name { get; set; } = default!;
    
    public string? ErrorMessage { get; set; }

    public UpdateProductGroupResponse(string errorMessage)
    {
        ErrorMessage = errorMessage;
    }

    public UpdateProductGroupResponse(Guid id, string name)
    {
        Id = id;
        Name = name;
    }
}