﻿namespace Service.Settings.Application.Responses;

public class ListWebsitePriceResponse
{
    public List<WebsitePriceResponse> Data { get; set; } = default!;
}

public class WebsitePriceResponse
{
    public Guid WebsiteId { get; set; }
    public Guid? ZoneId { get; set; }
    public string? ZoneName { get; set; }
    public int? Width { get; set; }
    public int? Height { get; set; }
    public Guid PublisherId { get; set; }
    public string PublisherEmail { get; set; } = default!;
    public string Url { get; set; } = default!;
    public string Domain { get; set; } = default!;
    public PublisherPriceType PublisherPriceType { get; set; }
    public List<DetailedWebsitePrice> Prices { get; set; } = default!;
}

public class DetailedWebsitePrice
{
    public Guid WebsitePriceId { get; set; }
    public bool? IsActive { get; set; }
    public WebsitePriceStatus Status { get; set; }
    public DateTimeOffset? StartDate { get; set; }
    public DateTimeOffset? EndDate { get; set; }
    public Dictionary<int, TemplatePrice> TemplatePrices { get; set; } = default!;
    public List<CampaignType> CampaignTypes { get; set; } = default!;
}

public class TemplatePrice
{
    public TemplateType TemplateType { get; set; }
    public PriceType SellPriceType { get; set; }
    public double SellPrice { get; set; }
    public PriceType BuyPriceType { get; set; }
    public double BuyPrice { get; set; }
}

public class GetWebsitePriceResponse
{
    public Guid Id { get; set; }
    public Guid WebsiteId { get; set; }
    public PublisherPriceType PublisherPriceType { get; set; }
    public List<CampaignType> CampaignTypes { get; set; } = default!;
    public Guid? ZoneId { get; set; }
    public string? ZoneName { get; set; }
    public string PublisherEmail { get; set; } = default!;
    public string Url { get; set; } = default!;
    public string Domain { get; set; } = default!;
    public DateTimeOffset StartDate { get; set; }
    public List<TemplatePrice> TemplatePrices { get; set; } = default!; 
    public List<ZoneTemplateResponse>? ZoneTemplates { get; set; }
}