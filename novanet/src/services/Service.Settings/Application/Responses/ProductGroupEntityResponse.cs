﻿namespace Service.Settings.Application.Responses;

public class ProductGroupEntityResponse
{

    public Guid? ProductGroupId { get; set; }
    
    public Guid? ProductId { get; set; }
    public Guid? ProductAttributeValueId { get; set; }

    public string? ProductAttributeValueHashCode { get; set; }

    public object? ProductAttributeValue { get; set; }

}