﻿namespace Service.Settings.Application.Responses;

public class CreateWebsiteResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public string Domain { get; set; } = default!;

    public string Url { get; set; } = default!;

    public Guid PublisherId { get; set; }

    public List<string>? DomainAliases { get; set; }
}

public class CreateWebsiteError
{
    public MessagesCreateWebsite ErrorType { get; set; } //enum
    public string? Value { get; set; }
}

public enum MessagesCreateWebsite
{
    DomainAliasExisted = 1,
    UrlExisted = 2,
    DomainExisted = 3,
}