﻿namespace Service.Settings.Application.Responses;

public class ListZoneResponse
{
    public List<ZoneResponse> Data { get; set; }
    public int Total { get; set; }
}

public class ZoneTemplateResponse
{
    public Guid? Id { get; set; }
    public TemplateType? TemplateType { get; set; }
    public Guid? ZoneId { get; set; }
    public Guid? AdvertisingTemplateId { get; set; }
}

public class ZoneResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public int Height { get; set; }

    public int Width { get; set; }

    public Guid WebsiteId { get; set; }

    public string? Domain { get; set; }

    public string? Url { get; set; }

    public bool UsingBackup { get; set; }

    public ZoneBackupType? BackupType { get; set; }

    public string? BackupCode { get; set; }

    public string Code { get; set; } = default!;

    public Guid? PublisherId { get; set; }

    public string? PublisherEmail { get; set; }

    public List<ZoneTemplateResponse>? ZoneTemplates { get; set; }
    
    public List<string?>? AdvertisingTemplateName { get; set; }
    
    public string? RedirectLink { get; set; }
    
    public List<CampaignType> CampaignTypes { get; set; } = default!;
}