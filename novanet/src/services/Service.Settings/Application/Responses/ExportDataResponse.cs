﻿using System.ComponentModel;

namespace Service.Settings.Application.Responses;


public class FileInformation
{
    public string FileName { get; set; }

    public MemoryStream Content { get; set; }
    
    public Byte[]? ResponseContent { get; set; }
}


public class ExportDataResponse<T>
{
    public string? SheetName { get; set; }

    public T? Data { get; set; }
}

public class ExportData
{
    [Description("Id quảng cáo")]
    [NotMapped]
    public Guid Id { get; set; } = default!;

    [Description("Tháng")]
    public string? Month { get; set; } = default!;
    
    [Description("Tuần")]
    public string? Week { get; set; } = default!;

    [Description("Ngày hiển thị số liệu")]
    public DateOnly? Date { get; set; }
    
    [Description("Khu vực")]
    public string? Location { get; set; }
    
    [Description("Id khu vực")]
    public string? LocationId { get; set; }

    [Description("Website")]
    public string? Website { get; set; }
    
    [Description("Id website")]
    public string? WebsiteId { get; set; }

    [Description("Id chiến dịch")]
    public Guid? CampaignId { get; set; }
    
    [Description("Tên chiến dịch")]
    public string? CampaignName { get; set; }
    
    [Description("Giờ")]
    public string? Hour { get; set; }

    [Description("Tên nhóm quảng cáo")]
    public string? AdvertisingSetName { get; set; }
    
    [Description("Id Nhóm sản phẩm")]
    public Guid? ProductGroupId { get; set; }
    
    [Description("Nhóm sản phẩm")]
    public string? ProductGroupName { get; set; }
    
    [Description("Tên tin quảng cáo")]
    public string? Name { get; set; } = default!;
    
    [Description("Loại quảng cáo")]
    public string? AdvertisingType { get; set; }
    
    [Description("Kích thước quảng cáo")]
    public string? AdvertisingSize { get; set; }

    [Description("URL Đích")]
    public string? RedirectLink { get; set; } = default!;

    [Description("Chi phí(VNĐ)")] public double? TotalCost { get; set; } = 0;
    
    [Description("Lượt xem")]
    public double? Impressions { get; set; } = 0;
    
    [Description("Lượt nhấn")]
    public double? Clicks { get; set; } = 0;
    
    [Description("Lượt nhấn banner")]
    public double? BannerClicks { get; set; } = 0;
    
    [Description("%Lượt nhấn banner")]
    public double? BannerClickRate { get; set; } = 0;
    
    [Description("CTR(%)")]
    public double? CTR { get; set; } = 0;
    
    [Description("CPC(VNĐ)")]
    public int? CPC { get; set; } = 0;
    
    [Description("Số Lượng mua hàng")]
    public double? Purchases { get; set; } = 0;
    
    [Description("% chuyển đổi mua hàng")]
    public double? PurchaseConversion { get; set; } = 0;
    
    [Description("CPS(VNĐ)")]
    public int? CPS { get; set; } = 0;
    
    [Description("Ngày bắt đầu")]
    public string? StartDate { get; set; }
    
    [Description("Ngày kết thúc")]
    public string? EndDate { get; set; }
}


public class ExportProductData
{
    [Description("Id chiến dịch")]
    public Guid? CampaignId { get; set; }
    
    [Description("Chiến dịch quảng cáo")]
    public string? CampaignName { get; set; }
    
    [Description("Id sản phẩm")]
    public Guid? ProductId { get; set; }
    
    [Description("Tên sản phẩm")]
    public string? ProductName { get; set; }
    
    [Description("Tình Trạng")]
    public string? ProductCondition { get; set; }
    
    [Description("Lượt xem")] 
    public double? Impressions { get; set; } = 0;
    
    [Description("Lượt nhấn")]
    public double? Clicks { get; set; } = 0;
    
    [Description("CTR(%)")]
    public double? CTR { get; set; } = 0;

    [Description("Số Lượng mua hàng")]
    public double? Purchases { get; set; } = 0;
    
    [Description("% chuyển đổi mua hàng")]
    public double? PurchaseConversion { get; set; } = 0;
    
    [Description("Loại sản phẩm")]
    public string? ProductType { get; set; }
    
    [Description("Cập nhật")]
    public string? Modified { get; set; }
    
    [Description("Nguồn dữ liệu")]
    public string? ProductFeed { get; set; }
}