﻿namespace Service.Settings.Application.Responses;

public class GetAdvertisingTemplateReportResponse
{
    public List<AdvertisingTemplateReport> Data { get; set; } = default!;
    public AdvertisingTemplateReport Summary { get; set; } = default!;
}

public class AdvertisingTemplateReport
{
    public Guid? Id { get; set; }
    public long SubId { get; set; }
    public string TemplateName { get; set; } = default!;
    public TemplateType TemplateType { get; set; }
    public int Height { get; set; }
    public int Width { get; set; }
    public double Cost { get; set; }
    public double Clicks { get; set; }
    public double Impressions { get; set; }
    public double Ctr { get; set; }
    public double Cpc { get; set; }
    public double Purchase { get; set; }
    public double PurchaseConversion { get; set; }
    public double Cps { get; set; }
    public CampaignType CampaignType { get; set; }
}