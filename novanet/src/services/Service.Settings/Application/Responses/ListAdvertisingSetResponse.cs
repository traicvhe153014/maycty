﻿namespace Service.Settings.Application.Responses;

public class ListAdvertisingSetResponse
{
    public List<AdvertisingSetResponse> Data { get; set; } = default!;
    public Dictionary<string, object?>? Summary { get; set; }
}

public class AdvertisingSetResponse
{
    public Guid Id { get; set; }
    
    public long? SubId { get; set; }
    public string Name { get; set; } = default!;
    public bool IsActive { get; set; }
    public AdvertisingSetStatus Status { get; set; }
    public CampaignType? CampaignType { get; set; }
    public Guid? CampaignId { get; set; }
    public string? CampaignName { get; set; }
    public Dictionary<string, object?> Data { get; set; } = default!;
    public List<AdvertisingSetProductGroupResponse>? ProductGroupData { get; set; }
}

public class AdvertisingSetProductGroupResponse
{
    public Guid? Id { get; set; }
    public string Name { get; set; }
    public bool IsActive { get; set; }
    public AdvertisingSetStatus Status { get; set; }
    public Dictionary<string, object?> Data { get; set; } = default!;

}