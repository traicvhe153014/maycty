﻿namespace Service.Settings.Application.Responses;

public class CreateWebsitePriceResponse
{
    public Guid Id { get; set; }
    
    public Guid? WebsiteId { get; set; }
    
    public Guid? AdvertisingSetId { get; set; }

    public TemplateType TemplateType { get; set; }

    public double SellPrice { get; set; }
    
    public PriceType SellPriceType { get; set; }

    public double BuyPrice { get; set; }
    
    public PriceType BuyPriceType { get; set; }
    
    public DateTimeOffset StartDate { get; set; }
    
    public DateTimeOffset? EndDate { get; set; }

    public Guid? ZoneId { get; set; }
    
    public DateTimeOffset Timestamp { get; set; }

}