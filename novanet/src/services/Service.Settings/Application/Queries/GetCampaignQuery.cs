﻿using Novanet.Core.Exceptions;

namespace Service.Settings.Application.Queries;

public class GetCampaignQuery : INovanetRequest<GetCampaignResponse>
{
    public Guid Id { get; set; }

    internal class Handler : NovanetRequestHandler<GetCampaignQuery, GetCampaignResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<GetCampaignResponse> HandleAsync(GetCampaignQuery request,
            CancellationToken cancellationToken)
        {
            var campaign = await _context.Campaigns
                .Include(x => x.CampaignScheduling)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (campaign is null)
            {
                throw new BadRequestException("Chiến dịch không tồn tại");
            }

            var campaignResponse = campaign.MapTo<GetCampaignResponse>();

            return campaignResponse;
        }
    }
}