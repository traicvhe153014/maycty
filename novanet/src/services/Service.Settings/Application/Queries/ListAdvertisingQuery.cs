﻿using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Models;
using Novanet.Core.Specifications;
using Novanet.Core.Utils;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;
using NovanetCore.Business.BusinessUtils;
using Service.Settings.Domain.AggregateModels.TemporaryAggregate;

namespace Service.Settings.Application.Queries;

public class ListAdvertisingQuery : INovanetRequest<ListAdvertisingResponse>
{
    public List<PredicateModel>? Filters { get; set; }

    public AdvertisingStatus? Status { get; set; }

    public CampaignType? CampaignType { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public int PageSize { get; set; }

    public int Page { get; set; }

    public bool WithSummary { get; set; }

    public bool WithReport { get; set; }

    public List<Guid?>? CampaignIds { get; set; } = default!;

    public List<Guid?>? AdvertisingSetIds { get; set; } = default!;
    
    public string? Sorts { get; set; }

    public List<string>? Fields { get; set; }
    
    internal class Handler : NovanetRequestHandler<ListAdvertisingQuery, ListAdvertisingResponse>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;
        
        private static readonly List<string> DefaultFields = new()
        {
            "totalCost", "impressions", "clicks",
            "ctr", "cpc",
            "videoView3s",
            "purchase", "purchaseConversion", "cps"
        };

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor,
            IReportService reportService) : base(logger, httpContextAccessor)
        {
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<ListAdvertisingResponse> HandleAsync(ListAdvertisingQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sorts = request.Sorts != null ? request.Sorts?.Split(",") : new []{"-ModifiedAt"};

            Expression<Func<Advertising, bool>> whereExpression = x => true;
            Expression<Func<Advertising, bool>> whereExpressionCacheable = x => true;
            if (request.Filters != null)
            {
                whereExpression = whereExpression.And(ToExpression(request.Filters, false));
                whereExpressionCacheable = whereExpressionCacheable.And(ToExpression(request.Filters, true));
            }

            if (request.Status != null)
            {
                whereExpression = whereExpression.And(x => x.Status == request.Status);
                whereExpressionCacheable = whereExpressionCacheable.And(x => x.Status == request.Status);
            }
            
            if (request.CampaignType != null)
            {
                whereExpression = whereExpression.And(x => x.AdvertisingSet!.Campaign!.CampaignType == request.CampaignType);
                whereExpressionCacheable = whereExpressionCacheable.And(x => x.AdvertisingSet!.Campaign!.CampaignType == request.CampaignType);
            }

            if (request.AdvertisingSetIds?.Count > 0)
            {
                whereExpression = whereExpression.And(x => request.AdvertisingSetIds.Contains(x.AdvertisingSetId));
                whereExpressionCacheable = whereExpressionCacheable.And(x => request.AdvertisingSetIds.Contains(x.AdvertisingSetId));
            }
            else if (request.CampaignIds?.Count > 0)
            {
                whereExpression = whereExpression.And(x => request.CampaignIds.Contains(x.CampaignId));
                whereExpressionCacheable = whereExpressionCacheable.And(x => request.CampaignIds.Contains(x.CampaignId));
            }
            
            Expression<Func<Advertising, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            var templates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);
            var templateAttributes = await _context.AdvertisingTemplateAttributes.ToListAsync(cancellationToken);

            var shouldIncludeTemplate = request.Filters is not null && request.Filters.Any(x => x.Field == "Template");

            // sort by redis
            var firstSortCriteria = sorts?.FirstOrDefault()?.Trim('-') ?? string.Empty;
            var isMetricExist = ReportConstants.MetricSorts.TryGetValue(firstSortCriteria, out var metricExpression);
            List<Advertising> advertisings;
            
            if (isMetricExist && metricExpression is not null)
            {
                var isDescending = sorts?.FirstOrDefault()?.StartsWith('-') == true;
                var sortedSubIds = _reportService.GetSortedAdvertiserIds(
                    request.StartDate,
                    request.EndDate,
                    ReportAdvertiserDimension.Advertising,
                    metricExpression,
                    isDescending);

                var tempContext = new OrderedSubIdTempTableContext(_context);
                await tempContext.CreateTableAsync(false, cancellationToken);
                await tempContext.BulkInsertAsync(sortedSubIds, isDescending ? sortedSubIds.Count : 0, isDescending, cancellationToken);

                advertisings = await _context.Advertising
                    .AsSplitQuery()
                    .Include(x => x.AdvertisingSet)
                    !.ThenInclude(x => x.Campaign)
                    .Include(x => x.AdvertisingAppliedTemplates)
                    .ThenInclude(x => x.AdvertisingTemplateAttributeValues)
                    .If(shouldIncludeTemplate,
                        x => x!.Include(y => y.AdvertisingAppliedTemplates)
                            !.ThenInclude(y => y.AdvertisingTemplate))
                    .LeftJoin(
                        _context.OrderedSubIdTempTable,
                        ad => ad.SubId,
                        temp => temp.Id,
                        (ad, temp) => new
                        {
                            Advertising = ad,
                            Rank = temp.Value
                        })
                    .SortBy(isDescending ? "-Rank" : "Rank")
                    .Select(x => x.Advertising)
                    .Where(whereExpression)
                    .Where(whereIsAdminExpression)
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .ToListAsync(cancellationToken);
            }
            else
            {
                advertisings = await _context.Advertising
                    .AsSplitQuery()
                    .Include(x => x.AdvertisingSet)
                    !.ThenInclude(x => x.Campaign)
                    .Include(x => x.AdvertisingAppliedTemplates)
                    .ThenInclude(x => x.AdvertisingTemplateAttributeValues)
                    .If(shouldIncludeTemplate,
                        x => x!.Include(y => y.AdvertisingAppliedTemplates)
                            !.ThenInclude(y => y.AdvertisingTemplate))
                    .AdvertisingOrderBy(sorts)
                    .Where(whereExpression)
                    .Where(whereIsAdminExpression)
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .ToListAsync(cancellationToken);
            }

            var data = advertisings.Select(advertising =>
                {
                    var appliedTemplates = new List<AdvertisingAppliedTemplateResponse>();
                    foreach (var appliedTemplate in advertising.AdvertisingAppliedTemplates ??
                                                    new List<AdvertisingAppliedTemplate>())
                    {
                        var template = templates.FirstOrDefault(x => x.Id == appliedTemplate.AdvertisingTemplateId);
                        var configurations = new Dictionary<string, object?>();
                        appliedTemplate.AdvertisingTemplateAttributeValues?.ForEach(x =>
                        {
                            var attribute =
                                templateAttributes.FirstOrDefault(attr => attr.Id == x.AdvertisingTemplateAttributeId);
                            configurations.Add(attribute?.Name ?? "", x.GetValue(attribute?.DataType));
                        });

                        var appliedTemplateResponse = new AdvertisingAppliedTemplateResponse
                        {
                            TemplateId = appliedTemplate.AdvertisingTemplateId,
                            TemplateName = template?.Name,
                            TemplateType = template?.TemplateType,
                            Height = template?.Height,
                            Width = template?.Width,
                            Configurations = configurations
                        };
                        appliedTemplates.Add(appliedTemplateResponse);
                    }

                    var responseItem = new AdvertisingResponse
                    {
                        Id = advertising.Id,
                        Name = advertising.Name,
                        CampaignName = advertising.AdvertisingSet?.Campaign?.Name,
                        CampaignId = advertising.AdvertisingSet?.CampaignId,
                        AdvertisingSetName = advertising.AdvertisingSet?.Name,
                        AdvertisingSetId = advertising.AdvertisingSetId,
                        Status = advertising.Status,
                        CampaignType = advertising.Campaign?.CampaignType,
                        IsActive = advertising.IsActive,
                        RedirectLink = advertising.RedirectLink ?? string.Empty,
                        Templates = appliedTemplates,
                        Data = new Dictionary<string, object?>()
                    };
                    return responseItem;
                })
                .ToList();
            if (request.WithReport)
            {
                var reportData = await FillReportData(request, advertisings);
                foreach (var dataItem in data)
                {
                    dataItem.Data = reportData.TryGetValue(dataItem.Id, out var item)
                        ? item
                        : new Dictionary<string, object?>();
                }
            }

            if (!request.WithSummary)
            {
                return new ListAdvertisingResponse
                {
                    Data = data
                };
            }

            var totalAdvertisings = CacheManager.Advertising.Values
                .Select(x =>
                {
                    var advertising = new Advertising
                    {
                        Id = x.Id,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        IsActive = x.IsActive,
                        SubId = x.SubId,
                        CampaignId = x.CampaignId,
                        AdvertisingSetId = x.AdvertisingSetId,
                        Search = x.Search ?? string.Empty,
                        AdvertisingAppliedTemplates = shouldIncludeTemplate ? x.AdvertisingAppliedTemplateCores.Select(adTemplate =>
                            new AdvertisingAppliedTemplate
                            {
                                AdvertisingTemplate = new AdvertisingTemplate
                                {
                                    Name = CacheManager.Templates.Values.FirstOrDefault(template =>
                                        template.Id == adTemplate.AdvertisingTemplateId)?.Name ?? string.Empty
                                }
                            }).ToList() : null
                    };
                    var advertisingSet = CacheManager.AdvertisingSets.Values
                        .FirstOrDefault(adset => adset.Id == x.AdvertisingSetId);
                    advertising.AdvertisingSet = new AdvertisingSet
                    {
                        Search = advertisingSet?.Search ?? string.Empty
                    };
                    var campaign = CacheManager.Campaigns.Values
                        .FirstOrDefault(campaign => campaign.Id == x.CampaignId);
                    advertising.AdvertisingSet.Campaign = new Campaign
                    {
                        CampaignType = campaign?.CampaignType ?? default,
                        Search = campaign?.Search ?? string.Empty
                    };
                    return advertising;
                })
                .Where(whereExpressionCacheable.Compile())
                .Where(whereIsAdminExpression.Compile())
                .Select(x => new Advertising
                {
                    Id = x.Id,
                    Name = x.Name,
                    SubId = x.SubId
                })
                .ToList();
            var summaryReport = await FillReportData(request, totalAdvertisings);

            var summary = SumReport(request, summaryReport);
            summary["total"] = totalAdvertisings.Count;

            return new ListAdvertisingResponse
            {
                Data = data,
                Summary = summary
            };
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillReportData(
            ListAdvertisingQuery request,
            List<Advertising> advertisings)
        {
            var now = DateTimeOffset.UtcNow;
            var advertisingIds = advertisings.ToDictionary(x => x.Id, x => (object) x.SubId);
            var advertisingSubIds = advertisingIds.Values.ToList();

            var hasDateRange = HasDateRange(request);
            var fields = request.Fields ?? DefaultFields;
            var baseMetrics = fields
                .Select(ReportConstants.GetMetricExpression)
                .Where(x => x is not null)
                .SelectMany(x => x.ReportMetrics)
                .Distinct()
                .ToList();

            var totalMetrics = new Dictionary<ReportMetric, Dictionary<long, double>>();
            foreach (var metric in baseMetrics)
            {
                totalMetrics[metric] = hasDateRange
                    ? await _reportService.GetAdvertiserReportByDay(
                        request.StartDate ?? now,
                        request.EndDate ?? now,
                        metric,
                        ReportAdvertiserDimension.Advertising,
                        advertisingSubIds)
                    : await _reportService.GetTotalAdvertiserReport(metric,
                        ReportAdvertiserDimension.Advertising,
                        advertisingSubIds);
            }

            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var advertising in advertisings)
            {
                var itemData = new Dictionary<string, object?>();
                foreach (var field in fields)
                {
                    var currentExpression = ReportConstants.GetMetricExpression(field);
                    var firstMetric = currentExpression.ReportMetrics.ElementAtOrDefault(0);
                    totalMetrics.TryGetValue(firstMetric, out var firstMetricValues);
                    double firstMetricValue = default;
                    firstMetricValues?.TryGetValue(advertising.SubId, out firstMetricValue);

                    var secondMetric = currentExpression.ReportMetrics.ElementAtOrDefault(1);
                    totalMetrics.TryGetValue(secondMetric, out var secondMetricValues);
                    double secondMetricValue = default;
                    secondMetricValues?.TryGetValue(advertising.SubId, out secondMetricValue);

                    var currentValue = MathUtils.CalculateMetric(
                        currentExpression.ExpressionType,
                        firstMetricValue,
                        secondMetricValue);

                    itemData[field] = currentValue;
                }
                result[advertising.Id] = itemData;
            }

            return result;
        }

        private static Dictionary<string, object?> SumReport(ListAdvertisingQuery request, Dictionary<Guid, Dictionary<string, object?>> data)
        {
            var fields = request.Fields ?? DefaultFields;
            var summary = new Dictionary<string, object?>();

            foreach (var field in fields)
            {
                summary[field] = AdvertisingUtils.SumField(data, field);
            }
            return summary;
        }

        private static bool HasDateRange(ListAdvertisingQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }

        private static Expression<Func<Advertising, bool>> ToExpression(List<PredicateModel> filters, bool cacheable)
        {
            Expression<Func<Advertising, bool>> whereExpression = x => true;
            foreach (var filter in filters)
            {
                if (filter.Field == "Ad.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x => !cacheable
                            ? EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : x.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x => !cacheable
                            ? !EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : !x.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.Equals => whereExpression.And(x => x.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x => x.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x => x.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Campaign.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            !cacheable
                                ? EF.Functions.Contains(x.AdvertisingSet!.Campaign!.Search, filter.Value.KeywordPretreatment())
                                : x.AdvertisingSet!.Campaign!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                            !cacheable
                                ? !EF.Functions.Contains(x.AdvertisingSet!.Campaign!.Search, filter.Value.KeywordPretreatment())
                                : !x.AdvertisingSet!.Campaign!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.Equals => whereExpression.And(x =>
                            x.AdvertisingSet!.Campaign!.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Campaign!.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Campaign!.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Adset.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            !cacheable
                                ? EF.Functions.Contains(x.AdvertisingSet!.Search, filter.Value.KeywordPretreatment())
                                : x.AdvertisingSet!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                            !cacheable
                                ? !EF.Functions.Contains(x.AdvertisingSet!.Search, filter.Value.KeywordPretreatment())
                                : !x.AdvertisingSet!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.Equals =>
                            whereExpression.And(x => x.AdvertisingSet!.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Template" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x.AdvertisingAppliedTemplates != null &&
                            x.AdvertisingAppliedTemplates.Any(a => a.AdvertisingTemplate!.Name.ToLower().Contains(filter.Value.ToLower()))
                        ),
                        _ => whereExpression
                    };
                }
            }

            return whereExpression;
        }
    }
}

public static class ListAdvertisingQueryExtensions
{
    public static IQueryable<Advertising> AdvertisingOrderBy(this IQueryable<Advertising> source, params string[]? sortExpression)
    {
        if (sortExpression == null || sortExpression.Length == 0)
            return source;

        IOrderedQueryable<Advertising>? orderedQuery = null;
        for (var index = 0; index < sortExpression.Length; index++)
        {
            var exp = sortExpression[index];
            if (string.IsNullOrEmpty(exp))
            {
                continue;
            }

            var sortField = Regex.Replace(sortExpression[index], @"[\+\-]", string.Empty);
            if (sortField == "status")
            {
                var statusOrderExpression = LinqExtensions.DescriptionOrder((Advertising x) => x.Status);
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(statusOrderExpression)
                        : orderedQuery!.ThenByDescending(statusOrderExpression);
                }
                else
                {
                    orderedQuery = index == 0
                        ? source.OrderBy(statusOrderExpression)
                        : orderedQuery!.ThenBy(statusOrderExpression);
                }
            }
            else
            {
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(sortField)
                        : orderedQuery.ThenByDescending(sortField);
                }
                else
                {
                    orderedQuery = index == 0 ? source.OrderBy(sortField) : orderedQuery.ThenBy(sortField);
                }
            }
        }

        return orderedQuery ?? source;
    }
}