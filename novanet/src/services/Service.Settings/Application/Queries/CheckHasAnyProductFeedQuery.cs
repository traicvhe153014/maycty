﻿namespace Service.Settings.Application.Queries;

public class CheckHasAnyProductFeedQuery : INovanetRequest<bool>
{
    internal class Handler : NovanetRequestHandler<CheckHasAnyProductFeedQuery, bool>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<bool> HandleAsync(CheckHasAnyProductFeedQuery request,
            CancellationToken cancellationToken)
        {
            return await _context.ProductFeeds.AnyAsync(cancellationToken);
        }
    }
}