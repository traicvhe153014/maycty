using NovanetCore.Business.BusinessManager;
using System.Linq.Expressions;
using MongoDB.Driver.Linq;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Models;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessDomain.Service.Identity;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace Service.Settings.Application.Queries;

public class ListWebsiteQuery : INovanetRequest<ListWebsiteResponse>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public string? KeyWord { get; set; }

    public string? Sorts { get; set; }

    public string? Filters { get; set; }

    public WebsiteStatus? Status { get; set; }
    
    public Guid? PublisherId { get; set; }

    internal class Handler : NovanetRequestHandler<ListWebsiteQuery, ListWebsiteResponse>
    {
        private readonly SettingsContext _context;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<ListWebsiteResponse> HandleAsync(ListWebsiteQuery request,
            CancellationToken cancellationToken)
        {
            var canViewAll = CanViewAll();

            Expression<Func<Website, bool>> whereExpression = x => true;
            Expression<Func<WebsiteCore, bool>> cacheWhereExpression = x => true;

            Expression<Func<Website, bool>> keyWordWhereExpression = x => true;
            Expression<Func<WebsiteCore, bool>> cacheKeyWordWhereExpression = x => true;

            if (request.PublisherId is not null)
            {
                keyWordWhereExpression =
                    keyWordWhereExpression.And(x => x.PublisherId.Equals(request.PublisherId));
                cacheKeyWordWhereExpression =
                    cacheKeyWordWhereExpression.And(x => x.PublisherId.Equals(request.PublisherId));
            }

            var listFilters = request.Filters.Deserialize<List<PredicateModel<FieldLevelEnum, string>>>();
            if (listFilters is not null && listFilters.Count > 0)
            {
                foreach (var keyWord in listFilters)
                {
                    var loweredKeyWord = keyWord.Value.ToLower();
                    if (keyWord.Field == FieldLevelEnum.EmailPublisher)
                    {
                        var publisher =
                            CacheManager.IdentityUsers.Values.Where(x => x.Email
                                    .ToLower()
                                    .Contains(loweredKeyWord))
                                .Select(x => x.Id).ToList();
                        if (publisher is not null)
                        {
                            keyWordWhereExpression =
                                keyWordWhereExpression.And(x => publisher.Contains(x.PublisherId));
                            cacheKeyWordWhereExpression =
                                cacheKeyWordWhereExpression.And(x => publisher.Contains(x.PublisherId));
                        }
                        else
                        {
                            keyWordWhereExpression = keyWordWhereExpression.And(x => x.PublisherId.Equals(""));
                            cacheKeyWordWhereExpression =
                                cacheKeyWordWhereExpression.And(x => x.PublisherId.Equals(""));
                        }
                    }

                    if (keyWord.Field == FieldLevelEnum.NameDomainWebsites)
                    {
                        keyWordWhereExpression =
                            keyWordWhereExpression.And(x => x.Domain.ToLower().Contains(loweredKeyWord));
                        cacheKeyWordWhereExpression =
                            cacheKeyWordWhereExpression.And(x => x.Domain.ToLower().Contains(loweredKeyWord));
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(request.KeyWord))
            {
                var loweredKeyWord = request.KeyWord.ToLower();
                keyWordWhereExpression = x => x.Url.ToLower().Contains(loweredKeyWord);
                cacheKeyWordWhereExpression = x => x.Url.ToLower().Contains(loweredKeyWord);
                if (canViewAll)
                {
                    keyWordWhereExpression = keyWordWhereExpression.Or(x =>
                        x.Publisher != null && x.Publisher.Email.ToLower().Contains(loweredKeyWord));
                    cacheKeyWordWhereExpression = cacheKeyWordWhereExpression.Or(x =>
                        x.Publisher != null && x.Publisher.Email.ToLower().Contains(loweredKeyWord));
                }
            }

            if (!canViewAll)
            {
                whereExpression = whereExpression.And(x => x.PublisherId == UserClaimsValue.Id);
                cacheWhereExpression = cacheWhereExpression.And(x => x.PublisherId == UserClaimsValue.Id);
            }

            if (request.Status is not null)
            {
                whereExpression = whereExpression.And(x => x.Status == request.Status);
                cacheWhereExpression = cacheWhereExpression.And(x => x.Status == request.Status);
            }

            var sortsWebsite = request.Sorts?.Split(',') ?? Array.Empty<string>();

            var response = new ListWebsiteResponse();
            response.Total = CacheManager.Websites.Values
                .Select(x =>
                {
                    var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(y => y.Id == x.PublisherId);
                    x.Publisher = new UserCore
                    {
                        Email = publisher?.Email ?? string.Empty,
                        Id = publisher?.Id ?? Guid.Empty
                    };
                    return x;
                })
                .Where(cacheWhereExpression.Compile())
                .Where(cacheKeyWordWhereExpression.Compile())
                .Count();

            if (request.Sorts is not null && request.Sorts.ToLower().Contains("fullname"))
            {
                if (request.Sorts.StartsWith("-"))
                {
                    response.Data = (await _context.Websites.AsNoTracking()
                            .AsSplitQuery()
                            .Include(x => x.DomainAliases)
                            .Include(x => x.WebsiteProhibitedCategories)
                            .Include(x => x.Zones)
                            .Include(x => x.AdvertisingSetWebsites)
                            .ToListAsync(cancellationToken))
                        .Select(x =>
                        {
                            var publisher =
                                CacheManager.IdentityUsers.Values.FirstOrDefault(y => y.Id == x.PublisherId);
                            x.Publisher = new UserCore
                            {
                                Email = publisher?.Email ?? string.Empty,
                                FullName = publisher?.FullName ?? string.Empty,
                                PhoneNumber = publisher?.PhoneNumber ?? string.Empty
                            };
                            return x;
                        })
                        .Where(whereExpression.Compile())
                        .Where(keyWordWhereExpression.Compile())
                        .OrderByDescending(x => x.Publisher!.FullName)
                        .Skip((request.Page - 1) * request.PageSize)
                        .Take(request.PageSize)
                        .Select(x => new WebsiteResponse
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Description = x.Description,
                            PublisherId = x.PublisherId,
                            Domain = x.Domain,
                            Url = x.Url,
                            AlexaRank = x.AlexaRank,
                            Keywords = x.Keywords,
                            CreatedAt = x.CreatedAt,
                            ModifiedAt = x.ModifiedAt,
                            PublisherEmail = x.Publisher!.Email,
                            PublisherName = x.Publisher!.FullName,
                            PublisherPhoneNumber = x.Publisher!.PhoneNumber,
                            Status = x.Status,
                            Active = x.Active,
                            SummaryZoneReport = x.Zones is null ? 0 : x.Zones.Count(),
                            DomainAliases = x.DomainAliases?
                                .Select(domainAlias => new DomainAlias
                                {
                                    Id = domainAlias.Id,
                                    Name = domainAlias.Name
                                }).Take(4)
                                .ToList(),
                            ProhibitedCategory = x.WebsiteProhibitedCategories is null
                                ? null
                                : CacheManager.ProhibitedCategories.Values
                                    .Where(y =>
                                        x.WebsiteProhibitedCategories
                                            .Select(z => z.Id)
                                            .Contains(y.Id))
                                    .Select(w => new ProhibitedCategoryCore
                                    {
                                        Id = w.Id,
                                        Name = w.Name,
                                    })
                                    .ToList(),
                            SummaryCampaignRunning = x.AdvertisingSetWebsites is not null
                                ? CacheManager.AdvertisingSets.Values
                                    .Where(z => x.AdvertisingSetWebsites
                                        .Where(a => a.WebsiteCondition == WebsiteCondition.Include)
                                        .Select(b => b.AdvertisingSetId).Contains(z.Id)).Distinct().Count()
                                : 0
                        })
                        .ToList();
                }
                else
                {
                    response.Data = (await _context.Websites.AsNoTracking()
                            .AsSplitQuery()
                            .Include(x => x.DomainAliases)
                            .Include(x => x.WebsiteProhibitedCategories)
                            .Include(x => x.Zones)
                            .Include(x => x.AdvertisingSetWebsites)
                            .ToListAsync(cancellationToken))
                        .Select(x =>
                        {
                            var publisher =
                                CacheManager.IdentityUsers.Values.FirstOrDefault(y => y.Id == x.PublisherId);
                            x.Publisher = new UserCore
                            {
                                Email = publisher?.Email ?? string.Empty,
                                FullName = publisher?.FullName ?? string.Empty,
                                PhoneNumber = publisher?.PhoneNumber ?? string.Empty
                            };
                            return x;
                        })
                        .Where(whereExpression.Compile())
                        .Where(keyWordWhereExpression.Compile())
                        .OrderBy(x => x.Publisher!.FullName)
                        .Skip((request.Page - 1) * request.PageSize)
                        .Take(request.PageSize)
                        .Select(x => new WebsiteResponse
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Description = x.Description,
                            PublisherId = x.PublisherId,
                            Domain = x.Domain,
                            Url = x.Url,
                            AlexaRank = x.AlexaRank,
                            Keywords = x.Keywords,
                            CreatedAt = x.CreatedAt,
                            ModifiedAt = x.ModifiedAt,
                            PublisherEmail = x.Publisher!.Email,
                            PublisherName = x.Publisher!.FullName,
                            PublisherPhoneNumber = x.Publisher!.PhoneNumber,
                            Status = x.Status,
                            Active = x.Active,
                            SummaryZoneReport = x.Zones is null ? 0 : x.Zones.Count(),
                            DomainAliases = x.DomainAliases?
                                .Select(domainAlias => new DomainAlias
                                {
                                    Id = domainAlias.Id,
                                    Name = domainAlias.Name
                                }).Take(4)
                                .ToList(),
                            ProhibitedCategory = x.WebsiteProhibitedCategories is null
                                ? null
                                : CacheManager.ProhibitedCategories.Values
                                    .Where(y =>
                                        x.WebsiteProhibitedCategories
                                            .Select(z => z.Id)
                                            .Contains(y.Id))
                                    .Select(w => new ProhibitedCategoryCore
                                    {
                                        Id = w.Id,
                                        Name = w.Name,
                                    })
                                    .ToList(),
                            SummaryCampaignRunning = x.AdvertisingSetWebsites is not null
                                ? CacheManager.AdvertisingSets.Values
                                    .Where(z => x.AdvertisingSetWebsites
                                        .Where(a => a.WebsiteCondition == WebsiteCondition.Include)
                                        .Select(b => b.AdvertisingSetId).Contains(z.Id)).Distinct().Count()
                                : 0
                        })
                        .ToList();
                }
            }
            else
            {
                response.Data = (await _context.Websites.AsNoTracking()
                        .AsSplitQuery()
                        .Include(x => x.DomainAliases)
                        .Include(x => x.WebsiteProhibitedCategories)
                        .Include(x => x.Zones)
                        .Include(x => x.AdvertisingSetWebsites)
                        .SortBy(sortsWebsite)
                        .ToListAsync(cancellationToken))
                    .Select(x =>
                    {
                        var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(y => y.Id == x.PublisherId);
                        x.Publisher = new UserCore
                        {
                            Email = publisher?.Email ?? string.Empty,
                            FullName = publisher?.FullName ?? string.Empty,
                            PhoneNumber = publisher?.PhoneNumber ?? string.Empty
                        };
                        return x;
                    })
                    .Where(whereExpression.Compile())
                    .Where(keyWordWhereExpression.Compile())
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .Select(x => new WebsiteResponse
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description,
                        PublisherId = x.PublisherId,
                        Domain = x.Domain,
                        Url = x.Url,
                        AlexaRank = x.AlexaRank,
                        Keywords = x.Keywords,
                        CreatedAt = x.CreatedAt,
                        ModifiedAt = x.ModifiedAt,
                        PublisherEmail = x.Publisher!.Email,
                        PublisherName = x.Publisher!.FullName,
                        PublisherPhoneNumber = x.Publisher!.PhoneNumber,
                        Status = x.Status,
                        Active = x.Active,
                        SummaryZoneReport = x.Zones is null ? 0 : x.Zones.Count(),
                        DomainAliases = x.DomainAliases?
                            .Select(domainAlias => new DomainAlias
                            {
                                Id = domainAlias.Id,
                                Name = domainAlias.Name
                            }).Take(4)
                            .ToList(),
                        ProhibitedCategory = x.WebsiteProhibitedCategories is null
                            ? null
                            : CacheManager.ProhibitedCategories.Values
                                .Where(y =>
                                    x.WebsiteProhibitedCategories
                                        .Select(z => z.ProhibitedCategoryId)
                                        .Contains(y.Id))
                                .Select(w => new ProhibitedCategoryCore
                                {
                                    Id = w.Id,
                                    Name = w.Name,
                                })
                                .ToList(),
                        SummaryCampaignRunning = x.AdvertisingSetWebsites is not null
                            ? CacheManager.AdvertisingSets.Values
                                .Where(z => x.AdvertisingSetWebsites
                                    .Where(a => a.WebsiteCondition == WebsiteCondition.Include)
                                    .Select(b => b.AdvertisingSetId).Contains(z.Id)).Distinct().Count()
                            : 0
                    })
                    .ToList();
            }
            return response;
        }

        private bool CanViewAll()
        {
            return UserClaimsValue.Roles.All(x => x != "Publisher");
        }
    }
}

public enum FieldLevelEnum
{
    EmailPublisher,
    NameDomainWebsites,
}