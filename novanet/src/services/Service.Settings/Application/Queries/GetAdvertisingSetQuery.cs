﻿using Novanet.Core.Exceptions;

namespace Service.Settings.Application.Queries;

public class GetAdvertisingSetQuery : INovanetRequest<GetAdvertisingSetResponse>
{
    public Guid Id { get; set; }

    internal class Handler : NovanetRequestHandler<GetAdvertisingSetQuery, GetAdvertisingSetResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<GetAdvertisingSetResponse> HandleAsync(GetAdvertisingSetQuery request,
            CancellationToken cancellationToken)
        {
            var advertisingSet = await _context.AdvertisingSets
                .Include(x => x.AdvertisingSetAges)
                .Include(x => x.AdvertisingSetCategories)
                .Include(x => x.AdvertisingSetGenders)
                .Include(x => x.AdvertisingSetLocations)
                .Include(x => x.AdvertisingSetObjects)
                .Include(x => x.AdvertisingSetProductGroups)
                .Include(x => x.AdvertisingSetProhibitedCategories)
                .Include(x => x.Campaign)
                .Include(x => x.AdvertisingSetWebsites)
                .Include(x => x.AdvertisingSetScheduling)
                .AsSplitQuery()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (advertisingSet == null)
            {
                throw new BadRequestException("Nhóm quảng cáo không tồn tại");
            }

            var advertisingSetResponse = advertisingSet.MapTo<GetAdvertisingSetResponse>();
            return advertisingSetResponse;
        }
    }
}