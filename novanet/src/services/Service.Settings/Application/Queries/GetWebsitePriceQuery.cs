﻿using Novanet.Core.Exceptions;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Queries;

public class GetWebsitePriceQuery : INovanetRequest<GetWebsitePriceResponse>
{
    public Guid Id { get; set; }

    internal class Handler : NovanetRequestHandler<GetWebsitePriceQuery, GetWebsitePriceResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor, SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<GetWebsitePriceResponse> HandleAsync(GetWebsitePriceQuery request, CancellationToken cancellationToken)
        {
            var websitePrice = await _context.WebsitePrices
                .Include(x => x.WebsiteTemplatePrices)
                !.ThenInclude(x => x.AdvertisingTemplate)
                .Include(x => x.WebsitePriceCampaignTypes)
                .AsSplitQuery()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (websitePrice is null)
            {
                throw new BadRequestException("Không tồn tại bảng giá");
            }
            var website = await _context.Websites
                .FirstOrDefaultAsync(x => x.Id == websitePrice.WebsiteId, cancellationToken);
            var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == website?.PublisherId);
            var response = new GetWebsitePriceResponse
            {
                Id = websitePrice.Id,
                PublisherPriceType = websitePrice.PublisherPriceType,
                StartDate = websitePrice.StartDate,
                WebsiteId = websitePrice.WebsiteId ?? Guid.Empty,
                ZoneId = websitePrice.ZoneId,
                Domain = website?.Domain ?? string.Empty,
                Url = website?.Url ?? string.Empty,
                PublisherEmail = publisher?.Email ?? string.Empty,
                TemplatePrices = websitePrice.WebsiteTemplatePrices?.Select(x => new TemplatePrice
                {
                    BuyPrice = x.BuyPrice,
                    BuyPriceType = x.BuyPriceType,
                    SellPrice = x.SellPrice,
                    SellPriceType = x.SellPriceType,
                    TemplateType = x.AdvertisingTemplate?.TemplateType ?? default
                }).ToList() ?? new List<TemplatePrice>(),
                CampaignTypes = websitePrice.WebsitePriceCampaignTypes?.Select(x => x.CampaignType).ToList() ?? new List<CampaignType>()
            };
            if (websitePrice.PublisherPriceType == PublisherPriceType.Zone)
            {
                var zone = await _context.Zones
                    .Include(x => x.ZoneTemplates)
                    !.ThenInclude(x => x.AdvertisingTemplate)
                    .FirstOrDefaultAsync(x => x.Id == websitePrice.ZoneId, cancellationToken);
                response.ZoneName = zone?.Name;
                response.ZoneTemplates = zone?.ZoneTemplates?.Select(x => new ZoneTemplateResponse
                {
                    Id = x.Id,
                    ZoneId = x.ZoneId,
                    AdvertisingTemplateId = x.AdvertisingTemplateId,
                    TemplateType = x.AdvertisingTemplate?.TemplateType
                }).ToList();
            }
            return response;
        }
    }
}