﻿using System.Linq.Expressions;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;

namespace Service.Settings.Application.Queries;

public class ListProductGroupQuery : INovanetRequest<List<ProductGroupResponse>>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public string? KeyWord { get; set; }

    public string? Sorts { get; set; }

    public Guid? ProductFeedId { get; set; }

    internal class Handler : NovanetRequestHandler<ListProductGroupQuery, List<ProductGroupResponse>>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<ProductGroupResponse>> HandleAsync(ListProductGroupQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sorts = request.Sorts?.Split(",");

            Expression<Func<ProductGroup, bool>> whereExpression = x => true;
            if (!string.IsNullOrEmpty(request.KeyWord))
            {
                whereExpression = x => EF.Functions.Contains(x.Search, request.KeyWord.KeywordPretreatment());
                whereExpression = whereExpression.Or(x => x.ProductFeed.Name.Contains(request.KeyWord)).
                    Or(x => x.AdvertisingSetProductGroups.Any(y=> y.AdvertisingSet!.Name.Contains(request.KeyWord)));
            }

            Expression<Func<ProductGroup, bool>> whereProductFeedExpression = x => true;
            if (request.ProductFeedId != null)
            {
                whereProductFeedExpression = x => x.ProductFeed.Id.Equals(request.ProductFeedId);
            }

            Expression<Func<ProductGroup, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            var productGroups = await _context.ProductGroups
                .Include(x => x.ProductFeed)
                .Include(x => x.AdvertisingSetProductGroups).ThenInclude(x => x.AdvertisingSet)
                .Include(x => x.ProductGroupEntities)!
                .ThenInclude(x => x.ProductEntity)
                .Where(whereExpression.And(whereProductFeedExpression).And(whereIsAdminExpression))
                .SortBy(sorts).Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new ProductGroupResponse
                {
                    Id = x.Id,
                    ProductFeed = x.ProductFeed,
                    Modified = x.ModifiedAt,
                    AdvertisingSetRunning = x.AdvertisingSetProductGroups!
                        .Where(z => z.AdvertisingSet != null &&
                                    z.AdvertisingSet.Status.Equals(AdvertisingSetStatus.Running))
                        .Select(y => new AdvertisingSetResponse
                        {
                            Id = y.AdvertisingSet!.Id,
                            Name = y.AdvertisingSet.Name,
                            IsActive = y.AdvertisingSet.IsActive,
                            Status = y.AdvertisingSet.Status,
                        }),
                    Amount = x.Amount,
                    Name = x.Name,
                    RunningProducts = x.ProductGroupEntities!.Count(y => y.ProductEntity!.Status == ProductEntityStatus.Running && !y.ProductEntity!.Deleted),
                    StoppedProducts = x.ProductGroupEntities!.Count(y => y.ProductEntity!.Status != ProductEntityStatus.Running && !y.ProductEntity!.Deleted),
                }).ToListAsync(cancellationToken: cancellationToken);
                
            return productGroups;
        }
    }
}