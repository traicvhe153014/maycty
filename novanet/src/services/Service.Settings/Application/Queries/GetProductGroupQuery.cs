﻿namespace Service.Settings.Application.Queries;

public class GetProductGroupQuery : INovanetRequest<List<ProductGroupResponse>>
{
     public string Ids { get; set; } = default!;

     internal class Handler : NovanetRequestHandler<GetProductGroupQuery, List<ProductGroupResponse>>
     {
         private readonly SettingsContext _context;
         private readonly AppSettings _appSettings;

         public Handler(
             ILogger<Handler> logger,
             SettingsContext context,
             IHttpContextAccessor httpContextAccessor,
             AppSettings appSettings) : base(logger, httpContextAccessor)
         {
             _context = context;
             _appSettings = appSettings;
         }

         protected override async Task<List<ProductGroupResponse>> HandleAsync(GetProductGroupQuery request,
             CancellationToken cancellationToken)
         {
             var productGroupIds = request.Ids!.Split(",").Select(x => x.ToGuid());
             
             var productGroups = await _context.ProductGroups
                 .Include(x => x.ProductGroupEntities!)
                 .Select(productGroupEntity => new ProductGroupResponse
                 {
                     Id = productGroupEntity.Id,
                     Name = productGroupEntity.Name,
                     ProductGroupEntities = productGroupEntity.ProductGroupEntities!.Select(productGroupEntity => new ProductGroupEntityResponse
                     {
                         ProductId = productGroupEntity.ProductId,
                         ProductAttributeValue = productGroupEntity.ProductAttributeValue,
                         ProductGroupId = productGroupEntity.ProductGroupId,
                         ProductAttributeValueId = productGroupEntity.ProductAttributeValueId,
                         ProductAttributeValueHashCode = productGroupEntity.ProductAttributeValueHashCode
                     }).ToList(),
                     ProductFeed = productGroupEntity.ProductFeed,
                     Amount = productGroupEntity.Amount
                 })
                 .Where(x => productGroupIds.Contains(x.Id))
                 .ToListAsync(cancellationToken);
             
             return productGroups;
         }
     }
}