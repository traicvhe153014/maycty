﻿namespace Service.Settings.Application.Queries;

public class ListProductAttributeQuery : INovanetRequest<List<ProductAttributeResponse>>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public int Skip { get; set; }


    internal class Handler : NovanetRequestHandler<ListProductAttributeQuery, List<ProductAttributeResponse>>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<ProductAttributeResponse>> HandleAsync(ListProductAttributeQuery request,
            CancellationToken cancellationToken)
        {
            var skip = request.Skip.Equals(0) ? (request.Page - 1) * request.PageSize : request.Skip;
            var mapping = ProductFeedConstants.MappingAttributes();

            var productAttributes = await _context.ProductAttributes
                .OrderBy(x => x.Order)
                .Skip(skip)
                .Take(request.PageSize)
                .ToListAsync(cancellationToken);

            return productAttributes
                .Select(productAttribute => new ProductAttributeResponse
                {
                    Id = productAttribute.Id,
                    Name = mapping.FirstOrDefault(x => x.Attribute.Equals(productAttribute.Name))?.DisplayName!
                })
                .ToList();
        }
    }
}