﻿using System.Linq.Expressions;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;

namespace Service.Settings.Application.Queries;

public class ListTransactionQuery : INovanetRequest<List<PaymentResponse>>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public int Skip { get; set; }

    public string? Keyword { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public string? Sorts { get; set; }

    internal class Handler : NovanetRequestHandler<ListTransactionQuery, List<PaymentResponse>>
    {
        private readonly SettingsContext _context;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<PaymentResponse>> HandleAsync(ListTransactionQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;

            var sorts = request.Sorts?.Split(",");
            Expression<Func<Payment, bool>> whereExpression = x => true;
            if (!string.IsNullOrEmpty(request.Keyword))
            {
                whereExpression = x => x.Email.Contains(request.Keyword);
                whereExpression = whereExpression.Or(x => x.CustomerName.Contains(request.Keyword));
            }
            
            Expression<Func<Payment, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            if (request.StartDate != null && request.EndDate != null)
            {
                whereExpression = whereExpression.And(x =>
                    x.CreatedAt >= request.StartDate.Value.DateTime &&
                    x.CreatedAt <= request.EndDate.Value.DateTime);
            }

            var skip = request.Skip.Equals(0) ? (request.Page - 1) * request.PageSize : request.Skip;
            var transactionHistory = await _context.Payment
                .Where(whereExpression)
                .Where(whereIsAdminExpression)
                .OrderByDescending(x => x.CreatedAt)
                .SortBy(sorts)
                .Skip(skip)
                .Take(request.PageSize)
                .Select(x => new PaymentResponse
                {
                    Email = x.Email,
                    Customer = x.CustomerName,
                    Amount = x.Amount,
                    Note = x.Note,
                    CreatedAt = x.CreatedAt,
                })
                .ToListAsync(cancellationToken);

            return transactionHistory;
        }
    }
}