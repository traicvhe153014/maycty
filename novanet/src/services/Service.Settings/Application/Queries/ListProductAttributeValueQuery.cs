﻿using System.Linq.Expressions;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Queries;

public class ListProductAttributeValueQuery : INovanetRequest<ListProductAttributeValueResponse>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public int Skip { get; set; }

    public string? KeyWord { get; set; }

    public Guid? ProductAttributeId { get; set; }

    public Guid? ProductFeedId { get; set; }
    
    public bool? WithSummary { get; set; }
    

    internal class Handler : NovanetRequestHandler<ListProductAttributeValueQuery, ListProductAttributeValueResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger, SettingsContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<ListProductAttributeValueResponse> HandleAsync(
            ListProductAttributeValueQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<ProductAttributeValue, bool>> whereExpression = x => true;
            if (!string.IsNullOrEmpty(request.KeyWord))
            {
                whereExpression = x =>
                    (x.StringValue + x.DateTimeValue + x.NumberValue + x.BooleanValue + x.DoubleValue).Contains(
                        request.KeyWord);
            }

            var skip = (request.Page - 1) * request.PageSize;

            var productAttributeValues = await _context.ProductAttributeValues
                .Where(x => x.ProductEntity.IsValid && x.ProductEntity.ProductFeed.Id.Equals(request.ProductFeedId) && !x.ProductEntity.Deleted)
                .Where(x => x.AttributeId.Equals(request.ProductAttributeId))
                .Where(whereExpression)
                .GroupBy(x => x.HashKey)
                .Select(x => new
                {
                    Value = x.First(),
                    Count = x.Count()
                })
                .Skip(skip)
                .Take(request.PageSize)
                .ToListAsync(cancellationToken);

            var productAttributeValueResponse = productAttributeValues
                .Select(productAttributeValue => new ProductAttributeValueResponse
                {
                    ProductAttributeHashcodeValue = productAttributeValue.Value.HashKey,
                    ProductAttributeValue = productAttributeValue.Value.OutputValue.ToString(),
                    Amount = productAttributeValue.Count
                })
                .Where(x => x.Amount > 0)
                .ToList();

            var listProductAttributeValueResponse = new ListProductAttributeValueResponse
            {
                Data = productAttributeValueResponse
            };

            if (request.WithSummary == null || !request.WithSummary.Value) return listProductAttributeValueResponse;
            {
                var countProductAttributeValues = CacheManager.ProductAttributeValues.Values
                    .Select(x =>
                    {
                        var productEntityCore = CacheManager.Products.Values.FirstOrDefault(z => z.Id.Equals(x.ProductId));

                        if (productEntityCore == null) return null;
                        var productEntity = new ProductEntity
                        {
                            Id = productEntityCore.Id,
                            ProductFeedId = productEntityCore.ProductFeedId,
                            IsValid = productEntityCore.IsValid
                        };
                        var productAttributeValue = new ProductAttributeValue
                        {
                            Id = x.Id,
                            ProductId = x.ProductId,
                            AttributeId = x.AttributeId,
                            CreatedBy = x.CreatedBy,
                            SubId = x.SubId,
                            HashKey = x.HashKey,
                            ProductEntity = productEntity
                        };

                        return productAttributeValue;
                    })
                    .Where(x => x != null && (request.ProductFeedId == null ||
                                              x.ProductEntity.ProductFeedId.Equals(request.ProductFeedId)))
                    .Where(x => x != null && x.ProductEntity.IsValid)
                    .Where(x => x != null && (request.ProductAttributeId == null || x.AttributeId.Equals(request.ProductAttributeId)))
                    .Where(whereExpression.Compile()!)
                    .GroupBy(x => x?.HashKey)
                    .Select(x => x.First())
                    .Count();

                var amount = CacheManager.ProductAttributeValues.Values
                    .Select(x =>
                    {
                        var productEntityCore =
                            CacheManager.Products.Values.FirstOrDefault(z => z.Id.Equals(x.ProductId));

                        if (productEntityCore == null) return null;
                        var productEntity = new ProductEntity
                        {
                            Id = productEntityCore.Id,
                            ProductFeedId = productEntityCore.ProductFeedId,
                            IsValid = productEntityCore.IsValid,
                        };
                        var productAttributeValue = new ProductAttributeValue
                        {
                            Id = x.Id,
                            ProductId = x.ProductId,
                            AttributeId = x.AttributeId,
                            CreatedBy = x.CreatedBy,
                            SubId = x.SubId,
                            HashKey = x.HashKey,
                            ProductEntity = productEntity
                        };

                        return productAttributeValue;
                    })
                    .Where(x => x != null && x.ProductEntity.IsValid && !x.ProductEntity.Deleted)
                    .Where(x => x != null && x.ProductEntity.ProductFeedId.Equals(request.ProductFeedId))
                    .Count(x => x != null && x.AttributeId.Equals(request.ProductAttributeId));
                
                var summary = new Dictionary<string, object?>
                {
                    {"total", countProductAttributeValues},
                    {"amount", amount},
                };
                
                var listProductAttributeValueResponseWithSummary = new ListProductAttributeValueResponse
                {
                    Data = productAttributeValueResponse,
                    Summary = summary
                };
                return listProductAttributeValueResponseWithSummary;
            }

        }
    }
}