﻿using System.Linq.Expressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Models;
using Novanet.Core.Specifications;
using Novanet.Core.Utils;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;
using Service.Settings.Domain.AggregateModels.TemporaryAggregate;

namespace Service.Settings.Application.Queries;

public class ListProductQuery : INovanetRequest<ListProductResponse>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public int Skip { get; set; }

    public string? Fields { get; set; }

    public string? KeyWord { get; set; }

    public string? Sorts { get; set; }

    public ProductEntityStatus? Status { get; set; }
    public string? Availability { get; set; }
    public Guid? CampaignId { get; set; }
    public string? ProductFeedIds { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public bool WithReport { get; set; }

    public string? Filters { get; set; }

    internal class Handler : NovanetRequestHandler<ListProductQuery, ListProductResponse>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;


        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IReportService reportService,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<ListProductResponse> HandleAsync(ListProductQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sorts = request.Sorts?.Split(",");

            Expression<Func<ProductEntity, bool>> whereExpressionFilter = x => true;
            var filters = request.Filters.Deserialize<List<PredicateModel<string,string>>>();
            if (filters != null) whereExpressionFilter = whereExpressionFilter.And(ToExpression(filters));

            Expression<Func<ProductEntity, bool>> whereExpression = x => x.IsValid && !x.Deleted;
            if (!string.IsNullOrWhiteSpace(request.ProductFeedIds))
            {
                var listProductFeedId = request.ProductFeedIds?.Split(",").Select(s => Guid.Parse(s)).ToList();
                if (listProductFeedId!.Count() > 0)
                    whereExpression = whereExpression.And(y => listProductFeedId!.Any(x => x.Equals(y.ProductFeedId)));
            }

            Expression<Func<ProductEntity, bool>> whereStatusExpression = x => x.IsValid;
            if (!request.Status.Equals(null) && !request.Status.Equals(ProductEntityStatus.Deactivated))
                whereStatusExpression = whereStatusExpression.And(x => x.Status.Equals(request.Status));

            Expression<Func<ProductEntity, bool>> whereAvailabilityExpression = x => x.IsValid;
            if (!string.IsNullOrWhiteSpace(request.Availability))
                whereAvailabilityExpression = whereAvailabilityExpression.And(x => x.AttributeValues.Any(y =>
                    y.ProductAttribute.Name.Equals(nameof(request.Availability))
                    && y.StringValue!.Contains(request.Availability)));

            Expression<Func<ProductEntity, bool>> whereCampaignIdExpression = x => x.IsValid;
            if (!request.CampaignId.Equals(null))
            {
                var productFeed = CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id.Equals(request.CampaignId));
                whereCampaignIdExpression =
                    whereCampaignIdExpression.And(x => x.ProductFeedId.Equals(productFeed!.EcommerceProductFeedId));
            }

            Expression<Func<ProductEntity, bool>> whereStatusDeactivatedExpression = x => x.IsValid;
            if (!request.Status.Equals(null) && request.Status.Equals(ProductEntityStatus.Deactivated))
                whereStatusDeactivatedExpression = whereStatusDeactivatedExpression.And(x => !x.IsActive);

            Expression<Func<ProductEntity, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin) whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);

            Expression<Func<ProductEntity, bool>> whereKeywordExpression = x => true;
            if (!string.IsNullOrWhiteSpace(request.KeyWord))
                whereKeywordExpression = x => x.ProductFeed.Campaigns.Any(x => x.Name.Contains(request.KeyWord));

            var skip = request.Skip.Equals(0) ? (request.Page - 1) * request.PageSize : request.Skip;

            var fields = request.Fields.Split(",");

            Expression<Func<ProductEntity, bool>> whereAttributeValuesExpression = x => x.AttributeValues!
                .Where(y => string.IsNullOrEmpty(request.KeyWord) ||
                            EF.Functions.Contains(y.Search, request.KeyWord.KeywordPretreatment()))
                .Select(y => y.AttributeId)
                .Any(z => _context.ProductAttributes
                    .Where(n => fields.Contains(n.Name))
                    .Select(m => new { m.Id, m.DataType, m.Name })
                    .Select(t => t.Id).Contains(z));
            Expression<Func<ProductEntity, bool>> whereAttributeValuesReportExpression = x => x.AttributeValues
                .Any(y => string.IsNullOrEmpty(request.KeyWord) || (y.Search != null &&
                                                                    y.Search.FullTextContains(
                                                                        request.KeyWord.KeywordPretreatment()) ==
                                                                    true)) == true;

            // sort by redis
            var firstSortCriteria = sorts?.FirstOrDefault()?.Trim('-') ?? string.Empty;
            var isMetricExist = ReportConstants.MetricSorts.TryGetValue(firstSortCriteria, out var metricExpression);
            List<ProductEntity> products;

            if (isMetricExist && metricExpression is not null)
            {
                var isDescending = sorts?.FirstOrDefault()?.StartsWith('-') == true;
                var sortedSubIds = _reportService.GetSortedAdvertiserIds(
                    request.StartDate,
                    request.EndDate,
                    ReportAdvertiserDimension.Product,
                    metricExpression,
                    isDescending);

                var tempContext = new OrderedSubIdTempTableContext(_context);
                await tempContext.CreateTableAsync(false, cancellationToken);
                await tempContext.BulkInsertAsync(sortedSubIds, isDescending ? sortedSubIds.Count : 0, isDescending,
                    cancellationToken);

                products = await _context.ProductEntities
                    .Include(x => x.ProductFeed).ThenInclude(x => x.Campaigns)
                    .Include(x => x.AttributeValues)!
                    .ThenInclude(x => x.ProductAttribute)
                    .LeftJoin(
                        _context.OrderedSubIdTempTable,
                        product => product.SubId,
                        temp => temp.Id,
                        (product, temp) => new
                        {
                            Product = product,
                            Rank = temp.Value
                        })
                    .SortBy(isDescending ? "-Rank" : "Rank")
                    .Select(x => x.Product)
                    .Where(whereExpression)
                    .Where(whereStatusExpression)
                    .Where(whereIsAdminExpression)
                    .Where(whereStatusDeactivatedExpression)
                    .Where(whereAvailabilityExpression)
                    .Where(whereCampaignIdExpression)
                    .Where(whereExpressionFilter)
                    .Where(whereAttributeValuesExpression.Or(whereKeywordExpression))
                    .Skip(skip)
                    .Take(request.PageSize)
                    .ToListAsync(cancellationToken);
            }
            else
            {
                products = await _context.ProductEntities
                    .Include(x => x.ProductFeed).ThenInclude(x => x.Campaigns)
                    .Include(x => x.AttributeValues)!
                    .ThenInclude(x => x.ProductAttribute)
                    .Where(whereExpression)
                    .Where(whereStatusExpression)
                    .Where(whereIsAdminExpression)
                    .Where(whereStatusDeactivatedExpression)
                    .Where(whereAvailabilityExpression)
                    .Where(whereCampaignIdExpression)
                    .Where(whereExpressionFilter)
                    .Where(whereAttributeValuesExpression.Or(whereKeywordExpression))
                    .Select(x => new ProductEntitySortable
                    {
                        ProductEntity = x,
                        Title = x.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "Title")!.StringValue,
                        Condition = x.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "Condition")!
                            .StringValue,
                        ProductType = x.AttributeValues!.Any(y => y.ProductAttribute.Name == "ProductType")
                            ? x.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "ProductType")!
                                .StringValue
                            : ""
                    })
                    .SortBy(sorts)
                    .Skip(skip)
                    .Take(request.PageSize)
                    .Select(x => x.ProductEntity)
                    .ToListAsync(cancellationToken);
            }

            var productResponses = new List<ProductResponse>();
            foreach (var product in products)
            {
                var productResponse = new ProductResponse
                {
                    ProductId = product.Id,
                    ProductFeedId = product.ProductFeedId,
                    ProductFeedName = product.ProductFeed.Name,
                    Status = product.Status,
                    IsActive = product.IsActive,
                    ModifiedAt = product.ModifiedAt,
                    Data = new Dictionary<string, object>()!,
                    RunningCampaigns = product.ProductFeed.Campaigns.ToList()
                };

                foreach (var attributeValue in product.AttributeValues!)
                {
                    if (productResponse.Data.ContainsKey(attributeValue.ProductAttribute.Name)) continue;
                    productResponse.Data.Add(attributeValue.ProductAttribute.Name, attributeValue.OutputValue);
                }

                productResponses.Add(productResponse);
            }

            if (request.WithReport)
            {
                var reportData = await FillReportData(request, products);
                foreach (var productResponse in productResponses)
                {
                    var dataItem = reportData.TryGetValue(productResponse.ProductId, out var item)
                        ? item
                        : new Dictionary<string, object?>();
                    foreach (var (dataKey, dataValue) in dataItem)
                    {
                        productResponse.Data ??= new Dictionary<string, object?>();
                        productResponse.Data[dataKey] = dataValue;
                    }
                }
            }

            var totalProducts = CacheManager.Products.Values
                .Select(x =>
                {
                    var productEntity = new ProductEntity
                    {
                        Id = x.Id,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        IsActive = x.IsActive,
                        SubId = x.SubId,
                        ProductFeedId = x.ProductFeedId,
                        IsValid = x.IsValid
                    };
                    var productFeed = CacheManager.ProductFeeds.Values
                        .FirstOrDefault(feed => feed.Id == x.ProductFeedId);
                    productEntity.ProductFeed = new ProductFeed
                    {
                        Name = productFeed?.Name ?? string.Empty,
                        Campaigns = CacheManager.Campaigns.Values
                            .Where(z => z.EcommerceProductFeedId.Equals(x?.ProductFeedId))
                            .Select(z => z.MapTo<Campaign>()).ToList() ?? new List<Campaign>()
                    };

                    productEntity.AttributeValues = x.ProductAttributeValueCores
                        ?.Select(item => new ProductAttributeValue
                        {
                            Id = item.Id,
                            StringValue = item.StringValue,
                            ProductAttribute =
                                CacheManager.ProductAttributes.Values.FirstOrDefault(x => x.Id.Equals(item.AttributeId))
                                    ?.MapTo<ProductAttribute>() ?? new ProductAttribute()
                        })
                        .ToList() ?? new List<ProductAttributeValue>();

                    return productEntity;
                })
                .Where(whereExpression.Compile())
                .Where(whereStatusExpression.Compile())
                .Where(whereIsAdminExpression.Compile())
                .Where(whereStatusDeactivatedExpression.Compile())
                .Where(whereAvailabilityExpression.Compile())
                .Where(whereCampaignIdExpression.Compile())
                .Where(whereExpressionFilter.Compile())
                .Where(whereAttributeValuesReportExpression.Or(whereKeywordExpression).Compile())
                .Select(x => new ProductEntity
                {
                    Id = x.Id,
                    SubId = x.SubId
                })
                .ToList();
            var summaryReport = await FillReportData(request, totalProducts);

            var summary = SumReport(summaryReport);
            summary["total"] = totalProducts.Count;

            var response = new ListProductResponse
            {
                Data = productResponses,
                Summary = summary
            };

            return response;
        }

        private static Expression<Func<ProductEntity, bool>> ToExpression(List<PredicateModel<string,string>> filters)
        {
            Expression<Func<ProductEntity, bool>> whereExpression = x => true;

            foreach (var filter in filters)
                if (filter.Field == "title" && !string.IsNullOrEmpty(filter.Value))
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x!.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name.Equals("Title")) != null
                            && x!.AttributeValues!.FirstOrDefault(y => y!.ProductAttribute!.Name!.Equals("Title"))!
                                .StringValue!.ToUpper().Contains(filter.Value.ToUpper())),
                        _ => whereExpression
                    };
                else if (filter.Field == "type" && !string.IsNullOrEmpty(filter.Value))
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x!.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name.Equals("ProductType")) !=
                            null
                            && x!.AttributeValues!.FirstOrDefault(y =>
                                    y!.ProductAttribute!.Name!.Equals("ProductType"))!.StringValue!.ToUpper()
                                .Contains(filter.Value.ToUpper())),
                        _ => whereExpression
                    };
                else if (filter.Field == "source" && !string.IsNullOrEmpty(filter.Value))
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x.ProductFeed.Name != null &&
                            x.ProductFeed.Name.ToUpper().Contains(filter.Value.ToUpper())),
                        _ => whereExpression
                    };
                else if (filter.Field == "Id" && !string.IsNullOrEmpty(filter.Value))
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x!.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name.Equals("Id")) != null
                            && x!.AttributeValues!.FirstOrDefault(y => y!.ProductAttribute!.Name!.Equals("Id"))!
                                .StringValue!.Contains(filter.Value)),
                        _ => whereExpression
                    };

            return whereExpression;
        }

        private static Dictionary<string, object?> SumReport(Dictionary<Guid, Dictionary<string, object?>> data)
        {
            var totalCost = AdvertisingUtils.SumField(data, "totalCost");
            var clicks = AdvertisingUtils.SumField(data, "clicks");
            var impressions = AdvertisingUtils.SumField(data, "impressions");
            var purchase = AdvertisingUtils.SumField(data, "purchase");
            var purchaseConversion = AdvertisingUtils.SumField(data, "purchaseConversion");

            var summary = new Dictionary<string, object?>
            {
                { "totalCost", totalCost },
                { "clicks", clicks },
                { "impressions", impressions },
                { "ctr", impressions != 0 ? clicks / impressions : 0 },
                { "cpc", clicks != 0 ? totalCost / clicks : 0 },
                { "purchase", purchase },
                { "purchaseConversion", purchaseConversion },
                { "cps", purchaseConversion != 0 ? totalCost / purchaseConversion : 0 }
            };
            return summary;
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillReportData(
            ListProductQuery request,
            List<ProductEntity> products)
        {
            var now = DateTimeOffset.UtcNow;
            var productIds = products.ToDictionary(x => x.Id, x => (object)x.SubId);
            var productSubIds = productIds.Values.ToList();

            var hasDateRange = HasDateRange(request);

            // total report
            var totalClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Click,
                    ReportAdvertiserDimension.Product,
                    productSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Click,
                    ReportAdvertiserDimension.Product,
                    productSubIds);

            var totalViewMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.View,
                    ReportAdvertiserDimension.Product,
                    productSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.View,
                    ReportAdvertiserDimension.Product,
                    productSubIds);

            var totalPaidMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Paid,
                    ReportAdvertiserDimension.Product,
                    productSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Paid,
                    ReportAdvertiserDimension.Product,
                    productSubIds);

            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var product in products)
            {
                var totalClicks = totalClickMetrics.GetValueOrDefault(product.SubId);
                var totalImpressions = totalViewMetrics.GetValueOrDefault(product.SubId);
                var totalCost = totalPaidMetrics.GetValueOrDefault(product.SubId);
                var totalCtr = totalImpressions != 0 ? totalClicks / totalImpressions : default;
                var totalCpc = totalClicks != 0 ? totalCost / totalClicks : default;
                double totalPurchase = default;
                double totalPurchaseConversion = default;
                double totalCps = default;
                var itemData = new Dictionary<string, object?>
                {
                    { "clicks", totalClicks },
                    { "impressions", totalImpressions },
                    { "totalCost", totalCost },
                    { "ctr", totalCtr },
                    { "cpc", totalCpc },
                    { "purchase", totalPurchase },
                    { "purchaseConversion", totalPurchaseConversion },
                    { "cps", totalCps }
                };
                result[product.Id] = itemData;
            }

            return result;
        }

        private static bool HasDateRange(ListProductQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
    }
}

public class ProductEntitySortable
{
    public ProductEntity ProductEntity { get; set; } = default!;
    public string? Title { get; set; }
    public string? Condition { get; set; }
    public string? ProductType { get; set; }
}