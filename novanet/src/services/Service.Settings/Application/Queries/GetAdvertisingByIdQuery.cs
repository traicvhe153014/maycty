﻿namespace Service.Settings.Application.Queries;

public class GetAdvertisingByIdQuery : INovanetRequest<AdvertisingResponse>
{
    public Guid Id { get; set; }

    internal class Handler : NovanetRequestHandler<GetAdvertisingByIdQuery, AdvertisingResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<AdvertisingResponse> HandleAsync(GetAdvertisingByIdQuery request,
            CancellationToken cancellationToken)
        {
            var advertising = await _context.Advertising
                .Include(x => x.AdvertisingSet)
                .ThenInclude(x => x!.Campaign)
                .Include(x => x.AdvertisingAppliedTemplates)!
                .ThenInclude(x => x.AdvertisingTemplateAttributeValues)!
                .ThenInclude(x => x.AdvertisingTemplateAttribute)
                .Include(x => x.AdvertisingAppliedTemplates)!
                .ThenInclude(x => x.AdvertisingTemplate)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            var advertisingResponse = new AdvertisingResponse
            {
                Id = advertising!.Id,
                Name = advertising.Name,
                IsActive = advertising.IsActive,
                RedirectLink = advertising.RedirectLink,
                ProductFeedId = advertising.AdvertisingSet?.Campaign?.EcommerceProductFeedId,
                Templates = new List<AdvertisingAppliedTemplateResponse>(),
                CampaignType = advertising.AdvertisingSet!.Campaign!.CampaignType
            };

            if (advertising.AdvertisingAppliedTemplates == null) return advertisingResponse;
            foreach (var advertisingAdvertisingAppliedTemplate in advertising.AdvertisingAppliedTemplates)
            {
                if (advertisingAdvertisingAppliedTemplate
                        .AdvertisingTemplateAttributeValues == null)
                    continue;
                var advertisingTemplateResponse = new AdvertisingAppliedTemplateResponse
                {
                    TemplateId = advertisingAdvertisingAppliedTemplate.AdvertisingTemplateId,
                    TemplateName = advertisingAdvertisingAppliedTemplate.AdvertisingTemplate?.Name,
                    TemplateType = advertisingAdvertisingAppliedTemplate.AdvertisingTemplate?.TemplateType,
                    Configurations = new Dictionary<string, object>()!
                };


                foreach (var attributeValue in advertisingAdvertisingAppliedTemplate
                             .AdvertisingTemplateAttributeValues)
                {
                    if (attributeValue.AdvertisingTemplateAttribute?.Name == null) continue;
                    advertisingTemplateResponse.Configurations.Add(
                        attributeValue.AdvertisingTemplateAttribute.Name,
                        attributeValue.OutputValue);
                }

                advertisingResponse.Templates.Add(advertisingTemplateResponse);
            }

            return advertisingResponse;
        }
    }
}