﻿using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Novanet.Core.Domain;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Models;
using Novanet.Core.Specifications;
using Novanet.Core.Utils;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;
using NovanetCore.Business.BusinessUtils;
using Service.Settings.Domain.AggregateModels.TemporaryAggregate;

namespace Service.Settings.Application.Queries;

public class ListAdvertisingSetQuery : INovanetRequest<ListAdvertisingSetResponse>
{
    public List<PredicateModel>? Filters { get; set; }

    public AdvertisingSetStatus? Status { get; set; }
    
    public CampaignType? CampaignType { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public int PageSize { get; set; }

    public int Page { get; set; }

    public bool WithSummary { get; set; }

    public bool WithReport { get; set; }

    public List<Guid?>? CampaignIds { get; set; } = default!;

    public string? Sorts { get; set; }

    public List<string>? Fields { get; set; }

    private static Expression<Func<AdvertisingSet, bool>> ToExpression(List<PredicateModel> filters, bool cacheable)
    {
        Expression<Func<AdvertisingSet, bool>> whereExpression = x => true;
        foreach (var filter in filters)
            if (filter.Field == "Adset.Name" && !string.IsNullOrEmpty(filter.Value))
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        !cacheable
                            ? EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : x.Search.FullTextContains(filter.Value)),
                    PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                        !cacheable
                            ? !EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : !x.Search.FullTextContains(filter.Value)),
                    PredicateOperatorEnum.Equals => whereExpression.And(x => x.Name == filter.Value),
                    PredicateOperatorEnum.StartsWith => whereExpression.And(x => x.Name.StartsWith(filter.Value)),
                    PredicateOperatorEnum.EndsWith => whereExpression.And(x => x.Name.EndsWith(filter.Value)),
                    _ => whereExpression
                };
            else if (filter.Field == "Campaign.Name" && !string.IsNullOrEmpty(filter.Value))
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        !cacheable
                            ? EF.Functions.Contains(x.Campaign!.Search, filter.Value.KeywordPretreatment())
                            : x.Campaign!.Search.FullTextContains(filter.Value)),
                    PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                        !cacheable
                            ? !EF.Functions.Contains(x.Campaign!.Search, filter.Value.KeywordPretreatment())
                            : !x.Campaign!.Search.FullTextContains(filter.Value)),
                    PredicateOperatorEnum.Equals => whereExpression.And(x => x.Campaign!.Name == filter.Value),
                    PredicateOperatorEnum.StartsWith => whereExpression.And(x =>
                        x.Campaign!.Name.StartsWith(filter.Value)),
                    PredicateOperatorEnum.EndsWith => whereExpression.And(x => x.Campaign!.Name.EndsWith(filter.Value)),
                    _ => whereExpression
                };
            else if (filter.Field == "Ad.Name" && !string.IsNullOrEmpty(filter.Value))
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        x.Advertisings!.Any(y => !cacheable
                            ? EF.Functions.Contains(y.Search, filter.Value.KeywordPretreatment())
                            : y.Search.FullTextContains(filter.Value))),
                    _ => whereExpression
                };
            else if (filter.Field == "Template" && !string.IsNullOrEmpty(filter.Value))
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        x.Advertisings!.Any(y =>
                            y.AdvertisingAppliedTemplates != null &&
                            y.AdvertisingAppliedTemplates.Any(a => a.AdvertisingTemplate!.Name.ToLower().Contains(filter.Value.ToLower()))
                        )),
                    _ => whereExpression
                };

        return whereExpression;
    }

    internal class Handler : NovanetRequestHandler<ListAdvertisingSetQuery, ListAdvertisingSetResponse>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;
        
        private static readonly List<string> DefaultFields = new()
        {
            "totalCost", "impressions", "clicks",
            "ctr", "cpc",
            "videoView3s",
            "purchase", "purchaseConversion", "cps"
        };

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IReportService reportService,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<ListAdvertisingSetResponse> HandleAsync(ListAdvertisingSetQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sorts = request.Sorts != null ? request.Sorts?.Split(",") : new[] {"-ModifiedAt" };

            Expression<Func<AdvertisingSet, bool>> whereExpression = x => true;
            Expression<Func<AdvertisingSet, bool>> whereExpressionCacheable = x => true;
            if (request.Filters != null)
            {
                whereExpression = whereExpression.And(ToExpression(request.Filters, false));
                whereExpressionCacheable = whereExpressionCacheable.And(ToExpression(request.Filters, true));
            }

            if (request.Status != null)
            {
                whereExpression = whereExpression.And(x => x.Status == request.Status);
                whereExpressionCacheable = whereExpressionCacheable.And(x => x.Status == request.Status);
            }

            if (request.CampaignIds?.Count > 0)
            {
                whereExpression = whereExpression.And(x => request.CampaignIds.Contains(x.CampaignId));
                whereExpressionCacheable =
                    whereExpressionCacheable.And(x => request.CampaignIds.Contains(x.CampaignId));
            }
            if (request.CampaignType != null)
            {
                whereExpression = whereExpression.And(x => x.Campaign!.CampaignType == request.CampaignType);
                whereExpressionCacheable = whereExpressionCacheable.And(x => x.Campaign!.CampaignType == request.CampaignType);
            }

            Expression<Func<AdvertisingSet, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin) whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);

            var shouldIncludeTemplate = request.Filters is not null && request.Filters.Any(x => x.Field == "Template");

            // sort by redis
            var firstSortCriteria = sorts?.FirstOrDefault()?.Trim('-') ?? string.Empty;
            var isMetricExist = ReportConstants.MetricSorts.TryGetValue(firstSortCriteria, out var metricExpression);

            List<AdvertisingSet> advertisingSets;

            if (isMetricExist && metricExpression is not null)
            {
                var isDescending = sorts?.FirstOrDefault()?.StartsWith('-') == true;
                var sortedSubIds = _reportService.GetSortedAdvertiserIds(
                    request.StartDate,
                    request.EndDate,
                    ReportAdvertiserDimension.AdvertisingSet,
                    metricExpression,
                    isDescending);

                var tempContext = new OrderedSubIdTempTableContext(_context);
                await tempContext.CreateTableAsync(false, cancellationToken);
                await tempContext.BulkInsertAsync(sortedSubIds, isDescending ? sortedSubIds.Count : 0, isDescending,
                    cancellationToken);

                advertisingSets = await _context.AdvertisingSets
                    .AsSplitQuery()
                    .Include(x => x.Campaign)
                    .Include(x => x.Advertisings)
                    .If(shouldIncludeTemplate,
                        x => x!.ThenInclude(y => y.AdvertisingAppliedTemplates)
                            !.ThenInclude(y => y.AdvertisingTemplate))
                    .Include(x => x.AdvertisingSetProductGroups)
                    !.ThenInclude(x => x.ProductGroup)
                    .LeftJoin(
                        _context.OrderedSubIdTempTable,
                        adset => adset.SubId,
                        temp => temp.Id,
                        (adset, temp) => new
                        {
                            AdvertisingSet = adset,
                            Rank = temp.Value
                        })
                    .SortBy(isDescending ? "-Rank" : "Rank")
                    .Select(x => x.AdvertisingSet)
                    .Where(whereExpression)
                    .Where(whereIsAdminExpression)
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .ToListAsync(cancellationToken);
            }
            else
            {
                advertisingSets = await _context.AdvertisingSets
                    .AsSplitQuery()
                    .Include(x => x.Campaign)
                    .Include(x => x.Advertisings)
                    .If(shouldIncludeTemplate,
                        x => x!.ThenInclude(y => y.AdvertisingAppliedTemplates)
                            !.ThenInclude(y => y.AdvertisingTemplate))
                    .Include(x => x.AdvertisingSetProductGroups)
                    !.ThenInclude(x => x.ProductGroup)
                    .AdvertisingSetOrderBy(sorts)
                    .Where(whereExpression)
                    .Where(whereIsAdminExpression)
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .ToListAsync(cancellationToken);
            }

            var data = advertisingSets.Select(advertisingSet =>
                {
                    var adSetResponse = new AdvertisingSetResponse
                    {
                        Id = advertisingSet.Id,
                        Name = advertisingSet.Name,
                        CampaignName = advertisingSet.Campaign?.Name,
                        CampaignId = advertisingSet.CampaignId,
                        Status = advertisingSet.Status,
                        CampaignType = advertisingSet.Campaign?.CampaignType,
                        IsActive = advertisingSet.IsActive,
                        Data = new Dictionary<string, object?>(),
                        ProductGroupData = advertisingSet.AdvertisingSetProductGroups?.Select(x =>
                            new AdvertisingSetProductGroupResponse
                            {
                                Id = x.ProductGroupId,
                                Name = x.ProductGroup?.Name ?? string.Empty,
                                Status = x.Status,
                                IsActive = x.IsActive,
                                Data = new Dictionary<string, object?>()
                            }).ToList()
                    };
                    return adSetResponse;
                })
                .ToList();
            if (request.WithReport)
            {
                var reportData = await FillCommonReportData(request, advertisingSets, ReportAdvertiserDimension.AdvertisingSet);
                foreach (var dataItem in data)
                    dataItem.Data = reportData.TryGetValue(dataItem.Id, out var item)
                        ? item
                        : new Dictionary<string, object?>();
                foreach (var advertisingSetResponse in data)
                {
                    var advertisingSet = advertisingSets.FirstOrDefault(x => x.Id == advertisingSetResponse.Id);
                    var groupReports = await FillCommonReportData(request,
                        advertisingSet?.AdvertisingSetProductGroups ?? new List<AdvertisingSetProductGroup>(),
                        ReportAdvertiserDimension.AdvertisingSetProductGroup);
                    foreach (var groupData in advertisingSetResponse.ProductGroupData ??
                                              new List<AdvertisingSetProductGroupResponse>())
                    {
                        var advertisingSetProductGroup =
                            advertisingSet?.AdvertisingSetProductGroups?.FirstOrDefault(x =>
                                x.ProductGroupId == groupData.Id);
                        groupData.Data =
                            groupReports.TryGetValue(advertisingSetProductGroup?.Id ?? Guid.Empty, out var value)
                                ? value
                                : new Dictionary<string, object?>();
                    }
                }
            }

            if (!request.WithSummary)
                return new ListAdvertisingSetResponse
                {
                    Data = data
                };

            var totalAdvertisingSets = CacheManager.AdvertisingSets.Values
                .Select(x =>
                {
                    var advertisingSet = new AdvertisingSet
                    {
                        Id = x.Id,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        IsActive = x.IsActive,
                        SubId = x.SubId,
                        CampaignId = x.CampaignId,
                        Search = x.Search ?? string.Empty
                    };
                    var campaign = CacheManager.Campaigns.Values
                        .FirstOrDefault(campaign => campaign.Id == x.CampaignId);
                    advertisingSet.Advertisings = CacheManager.Advertising.Values
                        .Where(ad => ad.AdvertisingSetId == x.Id)
                        .Select(ad => new Advertising
                        {
                            Search = ad.Search ?? string.Empty,
                            AdvertisingAppliedTemplates = shouldIncludeTemplate ? ad.AdvertisingAppliedTemplateCores.Select(adTemplate =>
                                new AdvertisingAppliedTemplate
                                {
                                    AdvertisingTemplate = new AdvertisingTemplate
                                    {
                                        Name = CacheManager.Templates.Values.FirstOrDefault(template =>
                                            template.Id == adTemplate.AdvertisingTemplateId)?.Name ?? string.Empty
                                    }
                                }).ToList() : null
                        })
                        .ToList();
                    advertisingSet.Campaign = new Campaign
                    {
                        CampaignType = campaign?.CampaignType ?? default,
                        Search = campaign?.Search ?? string.Empty
                    };
                    return advertisingSet;
                })
                .Where(whereExpressionCacheable.Compile())
                .Where(whereIsAdminExpression.Compile())
                .Select(x => new AdvertisingSet
                {
                    Id = x.Id,
                    Name = x.Name,
                    SubId = x.SubId
                })
                .ToList();
            var summaryReport = await FillCommonReportData(request, totalAdvertisingSets, ReportAdvertiserDimension.AdvertisingSet);

            var summary = SumReport(request, summaryReport);
            summary["total"] = totalAdvertisingSets.Count;

            return new ListAdvertisingSetResponse
            {
                Data = data,
                Summary = summary
            };
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillCommonReportData<T>(
            ListAdvertisingSetQuery request,
            List<T> data,
            ReportAdvertiserDimension reportAdvertiserDimension) where T : INovanetDocument
        {
            var now = DateTimeOffset.UtcNow;

            var dataIds = data.ToDictionary(x => x.Id, x => (object)x.SubId);
            var dataSubIds = dataIds.Values.ToList();
            var hasDateRange = HasDateRange(request);
            var fields = request.Fields ?? DefaultFields;
            var baseMetrics = fields
                .Select(ReportConstants.GetMetricExpression)
                .Where(x => x is not null)
                .SelectMany(x => x.ReportMetrics)
                .Distinct()
                .ToList();

            var totalMetrics = new Dictionary<ReportMetric, Dictionary<long, double>>();
            foreach (var metric in baseMetrics)
            {
                totalMetrics[metric] = hasDateRange
                    ? await _reportService.GetAdvertiserReportByDay(
                        request.StartDate ?? now,
                        request.EndDate ?? now,
                        metric,
                        reportAdvertiserDimension,
                        dataSubIds)
                    : await _reportService.GetTotalAdvertiserReport(
                        metric,
                        reportAdvertiserDimension,
                        dataSubIds);
            }

            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var item in data)
            {
                var itemData = new Dictionary<string, object?>();
                foreach (var field in fields)
                {
                    var currentExpression = ReportConstants.GetMetricExpression(field);
                    var firstMetric = currentExpression.ReportMetrics.ElementAtOrDefault(0);
                    totalMetrics.TryGetValue(firstMetric, out var firstMetricValues);
                    double firstMetricValue = default;
                    firstMetricValues?.TryGetValue(item.SubId, out firstMetricValue);

                    var secondMetric = currentExpression.ReportMetrics.ElementAtOrDefault(1);
                    totalMetrics.TryGetValue(secondMetric, out var secondMetricValues);
                    double secondMetricValue = default;
                    secondMetricValues?.TryGetValue(item.SubId, out secondMetricValue);

                    var currentValue = MathUtils.CalculateMetric(
                        currentExpression.ExpressionType,
                        firstMetricValue,
                        secondMetricValue);

                    itemData[field] = currentValue;
                }
                result[item.Id] = itemData;
            }

            return result;
        }

        private static Dictionary<string, object?> SumReport(ListAdvertisingSetQuery request, Dictionary<Guid, Dictionary<string, object?>> data)
        {
            var fields = request.Fields ?? DefaultFields;
            var summary = new Dictionary<string, object?>();

            foreach (var field in fields)
            {
                summary[field] = AdvertisingUtils.SumField(data, field);
            }
            return summary;
        }

        private static bool HasDateRange(ListAdvertisingSetQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
    }
}

public static class ListAdvertisingSetQueryExtensions
{
    public static IQueryable<AdvertisingSet> AdvertisingSetOrderBy(this IQueryable<AdvertisingSet> source, params string[]? sortExpression)
    {
        if (sortExpression == null || sortExpression.Length == 0)
            return source;

        IOrderedQueryable<AdvertisingSet>? orderedQuery = null;
        for (var index = 0; index < sortExpression.Length; index++)
        {
            var exp = sortExpression[index];
            if (string.IsNullOrEmpty(exp))
            {
                continue;
            }

            var sortField = Regex.Replace(sortExpression[index], @"[\+\-]", string.Empty);
            if (sortField == "status")
            {
                var statusOrderExpression = LinqExtensions.DescriptionOrder((AdvertisingSet x) => x.Status);
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(statusOrderExpression)
                        : orderedQuery!.ThenByDescending(statusOrderExpression);
                }
                else
                {
                    orderedQuery = index == 0
                        ? source.OrderBy(statusOrderExpression)
                        : orderedQuery!.ThenBy(statusOrderExpression);
                }
            }
            else
            {
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(sortField)
                        : orderedQuery.ThenByDescending(sortField);
                }
                else
                {
                    orderedQuery = index == 0 ? source.OrderBy(sortField) : orderedQuery.ThenBy(sortField);
                }
            }
        }

        return orderedQuery ?? source;
    }
}
