﻿using Novanet.Core.Exceptions;

namespace Service.Settings.Application.Queries;

public class GetProductFeedQuery : INovanetRequest<ProductFeedResponse>
{
    public string Id { get; set; } = default!;

    public bool IsFull { get; set; } = true;

    internal class Handler : NovanetRequestHandler<GetProductFeedQuery, ProductFeedResponse>
    {
        private readonly SettingsContext _context;
        private readonly AppSettings _appSettings;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor,
            AppSettings appSettings) : base(logger, httpContextAccessor)
        {
            _context = context;
            _appSettings = appSettings;
        }

        protected override async Task<ProductFeedResponse> HandleAsync(GetProductFeedQuery request,
            CancellationToken cancellationToken)
        {
            var productFeed = await _context.ProductFeeds
                .Include(x => x.ProductEntities)
                .ThenInclude(x => x.AttributeValues)
                !.ThenInclude(x => x.ProductAttribute)
                .Where(x => !x.Deleted)
                .FirstOrDefaultAsync(x => x.Id.ToString() == request.Id, cancellationToken);
            if (productFeed == null)
            {
                throw new BadRequestException("Không tìm thấy nguồn cấp dữ liệu sản phẩm");
            }

            if (!request.IsFull)
            {
                return new ProductFeedResponse
                {
                    Id = productFeed.Id,
                    Name = productFeed.Name,
                    SourcePath = string.Format(_appSettings.Configurations.GoogleSheetSourcePath,
                        productFeed.SourcePath),
                    SourceType = productFeed.SourceType,
                    Permission = productFeed.Permission,
                    ModifiedAt = productFeed.ModifiedAt,
                    ActiveProduct = productFeed.ValidProductsCount,
                    InActiveProduct = productFeed.InvalidProductsCount,
                };
            }

            var errorStatusList = await _context.ProductStatus
                .Include(x => x.ProductEntity)
                .Include(x => x.ProductAttributeValue)
                .Where(x => !x.Success &&
                            (x.ProductEntity != null && x.ProductEntity.ProductFeedId.ToString() == request.Id ||
                             x.ProductFeedId.ToString() == request.Id))
                .ToListAsync(cancellationToken);

            var productErrorList = errorStatusList
                .Select(x => new ProductFeedErrorResponse
                {
                    Id = x.Id,
                    Success = x.Success,
                    ErrorMessage = x.ErrorMessage,
                    ErrorType = x.ErrorType,
                    ProductEntityId = x.ProductEntityId,
                    ProductIdValue = x.ProductIdValue,
                    SheetPosition = x.SheetPosition,
                    ProductFeedId = x.ProductFeedId,
                    ProductAttributeValueId = x.ProductAttributeValueId,
                    AttributeValue = GetAttributeValue(x.ProductAttributeValue)?.ToString()
                })
                .ToList();

            var productFeedResponse = new ProductFeedResponse
            {
                Id = productFeed.Id,
                Name = productFeed.Name,
                SourcePath = string.Format(_appSettings.Configurations.GoogleSheetSourcePath, productFeed.SourcePath),
                SourceType = productFeed.SourceType,
                Permission = productFeed.Permission,
                ModifiedAt = productFeed.ModifiedAt,
                ActiveProduct = productFeed.ValidProductsCount,
                InActiveProduct = productFeed.InvalidProductsCount,
                ProductEntities = productFeed.ProductEntities.Select(y => new ProductEntityResponse
                {
                    Id = y.Id,
                    CreatedAt = y.CreatedAt,
                    ProductFeedId = y.ProductFeedId,
                    AttributeValues = GetAttributeValues(y.AttributeValues)
                }).ToList(),
                ProductStatusList = productErrorList
            };
            return productFeedResponse;
        }

        private static Dictionary<string, object?> GetAttributeValues(List<ProductAttributeValue>? attributeValues)
        {
            var data = new Dictionary<string, object?>();
            if (attributeValues == null) return data;
            foreach (var attributeValue in attributeValues)
            {
                if (data.ContainsKey(attributeValue.ProductAttribute.Name))
                {
                    continue;
                }
                data.Add(attributeValue.ProductAttribute.Name, GetAttributeValue(attributeValue));
            }

            return data;
        }

        private static object? GetAttributeValue(ProductAttributeValue? attributeValue)
        {
            if (attributeValue == null)
            {
                return string.Empty;
            }

            switch (attributeValue.ProductAttribute.DataType)
            {
                case NovanetDataType.String:
                case NovanetDataType.Url:
                    return attributeValue.StringValue;
                case NovanetDataType.Number:
                    return attributeValue.NumberValue;
                case NovanetDataType.Double:
                    return attributeValue.DoubleValue;
                case NovanetDataType.DateTime:
                    return attributeValue.DateTimeValue;
                case NovanetDataType.Boolean:
                    return attributeValue.BooleanValue;
                default:
                    return string.Empty;
            }
        }
    }
}