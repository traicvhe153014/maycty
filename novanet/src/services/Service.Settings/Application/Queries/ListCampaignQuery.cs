﻿using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Models;
using Novanet.Core.Specifications;
using Novanet.Core.Utils;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;
using NovanetCore.Business.BusinessUtils;
using Service.Settings.Domain.AggregateModels.TemporaryAggregate;

namespace Service.Settings.Application.Queries;

public class ListCampaignQuery : INovanetRequest<ListCampaignResponse>
{
    public List<PredicateModel>? Filters { get; set; }

    public CampaignStatus? Status { get; set; }
    
    public CampaignType? CampaignType { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public int PageSize { get; set; }

    public int Page { get; set; }

    public bool WithSummary { get; set; }

    public bool WithReport { get; set; }

    public string? Sorts { get; set; }
    
    public List<string>? Fields { get; set; }

    internal class Handler : NovanetRequestHandler<ListCampaignQuery, ListCampaignResponse>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;

        private static readonly List<string> DefaultFields = new()
        {
            "budget", "cost", "impressions", "clicks",
            "ctr", "cpc",
            "videoView3s",
            "purchase", "purchaseConversion", "cps"
        };

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IReportService reportService,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<ListCampaignResponse> HandleAsync(ListCampaignQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sorts = request.Sorts != null ? request.Sorts?.Split(",") : new[] {"-isActive","+status","-modifiedAt"};

            Expression<Func<Campaign, bool>> whereExpression = x => true;
            Expression<Func<Campaign, bool>> whereExpressionCacheable = x => true;
            if (request.Filters != null)
            {
                whereExpression = whereExpression.And(ToExpression(request.Filters, false));
                whereExpressionCacheable = whereExpressionCacheable.And(ToExpression(request.Filters, true));
            }

            if (request.Status != null)
            {
                whereExpression = whereExpression.And(x => x.Status == request.Status);
                whereExpressionCacheable = whereExpressionCacheable.And(x => x.Status == request.Status);
            }
            
            if (request.CampaignType != null)
            {
                whereExpression = whereExpression.And(x => x.CampaignType == request.CampaignType);
                whereExpressionCacheable = whereExpressionCacheable.And(x => x.CampaignType == request.CampaignType);
            }

            Expression<Func<Campaign, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            var shouldIncludeTemplate = request.Filters is not null && request.Filters.Any(x => x.Field == "Template");

            // sort by redis
            var firstSortCriteria = sorts?.FirstOrDefault()?.Trim('-') ?? string.Empty;
            var isMetricExist = ReportConstants.MetricSorts.TryGetValue(firstSortCriteria, out var metricExpression);
            List<Campaign> campaigns;

            if (isMetricExist && metricExpression is not null && !metricExpression.Ignored)
            {
                var isDescending = sorts?.FirstOrDefault()?.StartsWith('-') == true;
                var sortedSubIds = _reportService.GetSortedAdvertiserIds(
                    metricExpression.MetricDuration == MetricDuration.Total ? request.StartDate : DateTimeOffset.Now,
                    metricExpression.MetricDuration == MetricDuration.Total ? request.EndDate : DateTimeOffset.Now,
                    ReportAdvertiserDimension.Campaign,
                    metricExpression,
                    isDescending);

                var tempContext = new OrderedSubIdTempTableContext(_context);
                await tempContext.CreateTableAsync(false, cancellationToken);
                await tempContext.BulkInsertAsync(sortedSubIds, isDescending ? sortedSubIds.Count : 0, isDescending, cancellationToken);
                campaigns = await _context.Campaigns
                    .AsNoTracking()
                    .Include(x => x.AdvertisingSets)
                    .Include(x => x.Advertisings)
                    .If(shouldIncludeTemplate,
                        x => x!.ThenInclude(y => y.AdvertisingAppliedTemplates)
                            !.ThenInclude(y => y.AdvertisingTemplate))
                    .LeftJoin(
                        _context.OrderedSubIdTempTable,
                        campaign => campaign.SubId,
                        temp => temp.Id,
                        (campaign, temp) => new
                        {
                            Campaign = campaign,
                            Rank = temp.Value
                        })
                    .SortBy(isDescending ? "-Rank" : "Rank")
                    .Select(x => x.Campaign)
                    .Where(whereExpression.And(whereIsAdminExpression))
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .AsSplitQuery()
                    .ToListAsync(cancellationToken);
            }
            else
            { 
                campaigns = await _context.Campaigns
                    .AsNoTracking()
                    .Include(x => x.AdvertisingSets)
                    .Include(x => x.Advertisings)
                    .If(shouldIncludeTemplate,
                        x => x!.ThenInclude(y => y.AdvertisingAppliedTemplates)
                            !.ThenInclude(y => y.AdvertisingTemplate))
                    .CampaignOrderBy(sorts)
                    .Where(whereExpression.And(whereIsAdminExpression))
                    .Skip((request.Page - 1) * request.PageSize)
                    .Take(request.PageSize)
                    .AsSplitQuery()
                    .ToListAsync(cancellationToken);
            }

            var data = campaigns.Select(campaign =>
                {
                    var campaignResponse = new CampaignResponse
                    {
                        Id = campaign.Id,
                        Name = campaign.Name,
                        Status = campaign.Status,
                        IsActive = campaign.IsActive,
                        CampaignType = campaign.CampaignType,
                        StartDate = campaign.EcommerceStartDate,
                        EndDate = campaign.EcommerceEndDate,
                        Data = new Dictionary<string, object?>()
                    };
                    return campaignResponse;
                })
                .ToList();
            if (request.WithReport)
            {
                var reportData = await FillReportData(request, campaigns);
                foreach (var dataItem in data)
                {
                    dataItem.Data = reportData.TryGetValue(dataItem.Id, out var item)
                        ? item
                        : new Dictionary<string, object?>();
                }
            }

            if (!request.WithSummary)
            {
                return new ListCampaignResponse
                {
                    Data = data
                };
            }

            var totalCampaigns = CacheManager.Campaigns.Values
                .Select(x =>
                {
                    var campaign = new Campaign
                    {
                        Id = x.Id,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        IsActive = x.IsActive,
                        SubId = x.SubId,
                        EcommerceAmountSpent = x.EcommerceAmountSpent,
                        EcommerceDailyBudget = x.EcommerceDailyBudget,
                        Search = x.Search ?? string.Empty,
                        CampaignType = x.CampaignType,
                        AdvertisingSets = CacheManager.AdvertisingSets.Values
                            .Where(advertisingSetCore => advertisingSetCore.CampaignId == x.Id)
                            .Select(advertisingSetCore => new AdvertisingSet
                            {
                                Search = advertisingSetCore.Search ?? string.Empty
                            })
                            .ToList(),
                        Advertisings = CacheManager.Advertising.Values
                            .Where(ad => ad.CampaignId == x.Id)
                            .Select(ad => new Advertising
                            {
                                Search = ad.Search ?? string.Empty,
                                AdvertisingAppliedTemplates = ad.AdvertisingAppliedTemplateCores.Select(adTemplate =>
                                    new AdvertisingAppliedTemplate
                                    {
                                        AdvertisingTemplate = new AdvertisingTemplate
                                        {
                                            Name = CacheManager.Templates.Values.FirstOrDefault(template =>
                                                template.Id == adTemplate.AdvertisingTemplateId)?.Name ?? string.Empty
                                        }
                                    }).ToList()
                            })
                            .ToList()
                    };
                    return campaign;
                })
                .Where(whereExpressionCacheable.Compile())
                .Where(whereIsAdminExpression.Compile())
                .Select(x => new Campaign
                {
                    Id = x.Id,
                    Name = x.Name,
                    SubId = x.SubId,
                    EcommerceAmountSpent = x.EcommerceAmountSpent,
                    EcommerceDailyBudget = x.EcommerceDailyBudget
                })
                .ToList();
            var summaryReport = await FillReportData(request, totalCampaigns);

            var summary = SumReport(request, summaryReport);
            summary["dailyBudget"] =
                totalCampaigns.Aggregate(0d, (total, campaign) => campaign.EcommerceDailyBudget + total);
            summary["totalBudget"] =
                totalCampaigns.Aggregate(0d, (total, campaign) => campaign.EcommerceAmountSpent + total);
            summary["total"] = totalCampaigns.Count;

            return new ListCampaignResponse
            {
                Data = data,
                Summary = summary,
            };
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillReportData(
            ListCampaignQuery request,
            List<Campaign> campaigns)
        {
            var now = DateTimeOffset.UtcNow;
            var campaignIds = campaigns.ToDictionary(x => x.Id, x => (object) x.SubId);
            var campaignSubIds = campaignIds.Values.ToList();

            var hasDateRange = HasDateRange(request);
            var fields = request.Fields ?? DefaultFields;
            var baseMetrics = fields
                .Select(ReportConstants.GetMetricExpression)
                .Where(x => x is not null)
                .SelectMany(x => x.ReportMetrics)
                .Distinct()
                .ToList();
            
            var totalMetrics = new Dictionary<ReportMetric, Dictionary<long, double>>();
            foreach (var metric in baseMetrics)
            {
                totalMetrics[metric] = await GetMetricReport(request, hasDateRange, campaignSubIds, metric);
            }

            var dailyMetrics = new Dictionary<ReportMetric, Dictionary<long, double>>();
            if (!hasDateRange)
            {
                foreach (var metric in baseMetrics)
                {
                    dailyMetrics[metric] = await _reportService.GetAdvertiserReportByDay(
                        now,
                        now,
                        metric,
                        ReportAdvertiserDimension.Campaign,
                        campaignSubIds);
                }
            }

            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var campaign in campaigns)
            {
                var itemData = new Dictionary<string, object?>();
                foreach (var field in fields)
                {
                    if (field == "budget")
                    {
                        itemData["totalBudget"] = campaign.EcommerceAmountSpent;
                        continue;
                    }
                    var currentExpression = ReportConstants.GetMetricExpression(field);
                    var firstMetric = currentExpression.ReportMetrics.ElementAtOrDefault(0);
                    totalMetrics.TryGetValue(firstMetric, out var firstMetricValues);
                    double firstMetricValue = default;
                    firstMetricValues?.TryGetValue(campaign.SubId, out firstMetricValue);

                    var secondMetric = currentExpression.ReportMetrics.ElementAtOrDefault(1);
                    totalMetrics.TryGetValue(secondMetric, out var secondMetricValues);
                    double secondMetricValue = default;
                    secondMetricValues?.TryGetValue(campaign.SubId, out secondMetricValue);

                    var currentValue = MathUtils.CalculateMetric(
                        currentExpression.ExpressionType,
                        firstMetricValue,
                        secondMetricValue);
                    
                    var totalField = $"total{field[0].ToString().ToUpper()}{field.AsSpan(1)}";
                    itemData[totalField] = currentValue;
                }

                if (!hasDateRange)
                {
                    foreach (var field in fields)
                    {
                        if (field == "budget")
                        {
                            itemData["dailyBudget"] = campaign.EcommerceDailyBudget;
                            continue;
                        }
                        var currentExpression = ReportConstants.GetMetricExpression(field);
                        var firstMetric = currentExpression.ReportMetrics.ElementAtOrDefault(0);
                        dailyMetrics.TryGetValue(firstMetric, out var firstMetricValues);
                        double firstMetricValue = default;
                        firstMetricValues?.TryGetValue(campaign.SubId, out firstMetricValue);

                        var secondMetric = currentExpression.ReportMetrics.ElementAtOrDefault(1);
                        dailyMetrics.TryGetValue(secondMetric, out var secondMetricValues);
                        double secondMetricValue = default;
                        secondMetricValues?.TryGetValue(campaign.SubId, out secondMetricValue);

                        var currentValue = MathUtils.CalculateMetric(
                            currentExpression.ExpressionType,
                            firstMetricValue,
                            secondMetricValue);
                    
                        var dailyField = $"daily{field[0].ToString().ToUpper()}{field.AsSpan(1)}";
                        itemData[dailyField] = currentValue;
                    }
                }

                result[campaign.Id] = itemData;
            }

            return result;
        }

        private static Dictionary<string, object?> SumReport(ListCampaignQuery request, Dictionary<Guid, Dictionary<string, object?>> data)
        {
            var fields = request.Fields ?? DefaultFields;
            var summary = new Dictionary<string, object?>();

            foreach (var field in fields)
            {
                var totalField = $"total{field[0].ToString().ToUpper()}{field.AsSpan(1)}";
                summary[totalField] = AdvertisingUtils.SumField(data, totalField);
                
                var dailyField = $"daily{field[0].ToString().ToUpper()}{field.AsSpan(1)}";
                summary[dailyField] = AdvertisingUtils.SumField(data, dailyField);
            }
            return summary;
        }

        private static bool HasDateRange(ListCampaignQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }

        private async Task<Dictionary<long, double>> GetMetricReport(ListCampaignQuery request, bool hasDateRange, List<object> campaignSubIds, ReportMetric reportMetric)
        {
            return hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    reportMetric,
                    ReportAdvertiserDimension.Campaign,
                    campaignSubIds)
                : await _reportService.GetTotalAdvertiserReport(reportMetric,
                    ReportAdvertiserDimension.Campaign,
                    campaignSubIds);
        }
    }

    private static Expression<Func<Campaign, bool>> ToExpression(List<PredicateModel> filters, bool cacheable)
    {
        Expression<Func<Campaign, bool>> whereExpression = x => true;

        foreach (var filter in filters)
        {
            if (filter.Field == "Campaign.Name" && !string.IsNullOrEmpty(filter.Value))
            {
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        !cacheable
                            ? EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : x.Search.FullTextContains(filter.Value)),
                    PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                        !cacheable
                            ? !EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : !x.Search.FullTextContains(filter.Value)),
                    PredicateOperatorEnum.Equals => whereExpression.And(x => x.Search == filter.Value),
                    PredicateOperatorEnum.StartsWith => whereExpression.And(x => x.Search.StartsWith(filter.Value)),
                    PredicateOperatorEnum.EndsWith => whereExpression.And(x => x.Search.EndsWith(filter.Value)),
                    _ => whereExpression
                };
            }
            else if (filter.Field == "Adset.Name" && !string.IsNullOrEmpty(filter.Value))
            {
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        x.AdvertisingSets!.Any(y =>
                            !cacheable
                                ? EF.Functions.Contains(y.Search, filter.Value.KeywordPretreatment())
                                : y.Search.FullTextContains(filter.Value))),
                    _ => whereExpression
                };
            }
            else if (filter.Field == "Ad.Name" && !string.IsNullOrEmpty(filter.Value))
            {
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        x.Advertisings!.Any(y =>
                            !cacheable
                                ? EF.Functions.Contains(y.Search, filter.Value.KeywordPretreatment())
                                : y.Search.FullTextContains(filter.Value))),
                    _ => whereExpression
                };
            } 
            else if (filter.Field == "Template" && !string.IsNullOrEmpty(filter.Value))
            {
                whereExpression = filter.Operator switch
                {
                    PredicateOperatorEnum.Contains => whereExpression.And(x =>
                        x.Advertisings!.Any(y =>
                            y.AdvertisingAppliedTemplates != null &&
                            y.AdvertisingAppliedTemplates.Any(a => a.AdvertisingTemplate!.Name.ToLower().Contains(filter.Value.ToLower()))
                        )),
                    _ => whereExpression
                };
            }
        }

        return whereExpression;
    }
}

public static class ListCampaignQueryExtensions
{
    public static IQueryable<Campaign> CampaignOrderBy(this IQueryable<Campaign> source, params string[]? sortExpression)
    {
        if (sortExpression == null || sortExpression.Length == 0)
            return source;

        IOrderedQueryable<Campaign>? orderedQuery = null;
        for (var index = 0; index < sortExpression.Length; index++)
        {
            var exp = sortExpression[index];
            if (string.IsNullOrEmpty(exp))
            {
                continue;
            }

            var sortField = Regex.Replace(sortExpression[index], @"[\+\-]", string.Empty);
            if (sortField == "status")
            {
                var statusOrderExpression = LinqExtensions.DescriptionOrder((Campaign x) => x.Status);
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(statusOrderExpression)
                        : orderedQuery!.ThenByDescending(statusOrderExpression);
                }
                else
                {
                    orderedQuery = index == 0
                        ? source.OrderBy(statusOrderExpression)
                        : orderedQuery!.ThenBy(statusOrderExpression);
                }
            }
            else
            {
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(sortField)
                        : orderedQuery.ThenByDescending(sortField);
                }
                else
                {
                    orderedQuery = index == 0 ? source.OrderBy(sortField) : orderedQuery.ThenBy(sortField);
                }
            }
        }

        return orderedQuery ?? source;
    }
}