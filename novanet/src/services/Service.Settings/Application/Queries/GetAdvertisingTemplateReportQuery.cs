﻿using System.Linq.Expressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;

namespace Service.Settings.Application.Queries;

public class GetAdvertisingTemplateReportQuery : INovanetRequest<GetAdvertisingTemplateReportResponse>
{
    public string? TemplateTypes { get; set; }
    public string? Sorts { get; set; }
    public CampaignType? CampaignType { get; set; }
    public DateTimeOffset? StartDate { get; set; }
    public DateTimeOffset? EndDate { get; set; }

    internal class Handler : NovanetRequestHandler<GetAdvertisingTemplateReportQuery,
        GetAdvertisingTemplateReportResponse>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor, SettingsContext context,
            IReportService reportService) : base(logger, httpContextAccessor)
        {
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<GetAdvertisingTemplateReportResponse> HandleAsync(GetAdvertisingTemplateReportQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<AdvertisingTemplate, bool>> whereExpression = x => true;
            
            // filter by template types
            if (request.TemplateTypes is not null)
            {
                var templateTypes = request.TemplateTypes.Split(',').Select(x => (TemplateType) int.Parse(x)).ToList();
                whereExpression = x => templateTypes.Contains(x.TemplateType);
            }
            if (request.CampaignType != null)
            {
                whereExpression = whereExpression.And(x => x.CampaignType == request.CampaignType);
            }
            var templateReports = await _context.AdvertisingTemplates
                .Where(whereExpression)
                .Select(x => new AdvertisingTemplateReport
                {
                    Id = x.Id,
                    SubId = x.SubId,
                    Height = x.Height,
                    Width = x.Width,
                    TemplateType = x.TemplateType,
                    TemplateName = x.Name,
                    CampaignType = x.CampaignType,
                })
                .ToListAsync(cancellationToken);

            var templateSubIds = templateReports.Select(x => (object) x.SubId).ToList();
            var advertisingSubIds = UserClaimsValue.IsAdmin
                ? new List<object>()
                : CacheManager.Advertising.Values.Where(x => x.CreatedBy == UserClaimsValue.Id)
                    .Select(x => (object) x.SubId).ToList();
            var totalClickMetrics =
                await GetReportData(request, ReportMetric.Click, templateSubIds, advertisingSubIds);

            var totalViewMetrics =
                await GetReportData(request, ReportMetric.View, templateSubIds, advertisingSubIds);

            var totalPaidMetrics =
                await GetReportData(request, ReportMetric.Paid, templateSubIds, advertisingSubIds);

            var totalConversionMetrics =
                await GetReportData(request, ReportMetric.Conversion, templateSubIds, advertisingSubIds);

            foreach (var reportItem in templateReports)
            {
                var totalClicks = totalClickMetrics.GetValueOrDefault(reportItem.SubId);
                var totalImpressions = totalViewMetrics.GetValueOrDefault(reportItem.SubId);
                var totalCost = totalPaidMetrics.GetValueOrDefault(reportItem.SubId);
                var totalCtr = totalImpressions != 0 ? totalClicks / totalImpressions : default;
                var totalCpc = totalClicks != 0 ? totalCost / totalClicks : default;
                var totalPurchase = totalConversionMetrics.GetValueOrDefault(reportItem.SubId);
                var totalPurchaseConversion = totalImpressions != 0 ? totalPurchase / totalImpressions : default;
                var totalCps = totalPurchase != 0 ? totalCost / totalPurchase : default;
                
                reportItem.Clicks = totalClicks;
                reportItem.Impressions = totalImpressions;
                reportItem.Cost = totalCost;
                reportItem.Ctr = totalCtr;
                reportItem.Cpc = totalCpc;
                reportItem.Purchase = totalPurchase;
                reportItem.PurchaseConversion = totalPurchaseConversion;
                reportItem.Cps = totalCps;
            }
            
            // sort report and filter template with no data
            var sorts = request.Sorts != null ? request.Sorts.Split(",") : new[] {"templateType"};
            var newSorts = new string[sorts.Length + 1];
            var found = false;
            for (var i = 0; i < sorts.Length; i++)
            {
                newSorts[found ? i + 1 : i] = sorts[i];
                switch (sorts[i])
                {
                    case "campaignType":
                        newSorts[i + 1] = "templateName";
                        found = true;
                        break;
                    case "-campaignType":
                        newSorts[i + 1] = "-templateName";
                        found = true;
                        break;
                }
            }
            templateReports = templateReports
                .Where(x => x.Cost != 0 || x.Clicks != 0 || x.Impressions != 0 || x.Purchase != 0)
                .SortBy(newSorts)
                .ToList();
            
            var summary = new AdvertisingTemplateReport
            {
                Cost = templateReports.Aggregate(0d, (sum, item) => sum + item.Cost),
                Impressions = templateReports.Aggregate(0d, (sum, item) => sum + item.Impressions),
                Clicks = templateReports.Aggregate(0d, (sum, item) => sum + item.Clicks),
                Purchase = templateReports.Aggregate(0d, (sum, item) => sum + item.Purchase)
            };
            summary.Ctr = summary.Impressions != 0 ? summary.Clicks / summary.Impressions : default;
            summary.Cpc = summary.Clicks != 0 ? summary.Cost / summary.Clicks : default;
            summary.PurchaseConversion = summary.Impressions != 0 ? summary.Purchase / summary.Impressions : default;
            summary.Cps = summary.Purchase != 0 ? summary.Cost / summary.Purchase : default;

            return new GetAdvertisingTemplateReportResponse
            {
                Data = templateReports,
                Summary = summary,
            };
        }

        private async Task<Dictionary<long, double>> GetReportData(GetAdvertisingTemplateReportQuery request,
            ReportMetric metric, List<object> templateSubIds, List<object> advertisingSubIds)
        {
            var now = DateTimeOffset.UtcNow;
            var hasDateRange = HasDateRange(request);

            if (UserClaimsValue.IsAdmin)
            {
                return hasDateRange
                    ? await _reportService.GetAdvertiserReportByDay(
                        request.StartDate!.Value,
                        request.EndDate!.Value,
                        metric,
                        ReportAdvertiserDimension.AdvertisingTemplate,
                        templateSubIds)
                    : await _reportService.GetTotalAdvertiserReport(metric,
                        ReportAdvertiserDimension.AdvertisingTemplate,
                        templateSubIds);
            }
            var objectIds = new List<List<object>>
            {
                advertisingSubIds,
                templateSubIds
            };
            var dimensions = new List<ReportAdvertiserDimension>
            {
                ReportAdvertiserDimension.Advertising,
                ReportAdvertiserDimension.AdvertisingTemplate
            };
            var reports = hasDateRange
                ? await _reportService.GetMultipleAdvertiserReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    metric,
                    dimensions,
                    objectIds)
                : await _reportService.GetTotalMultipleAdvertiserReport(
                    metric,
                    dimensions,
                    objectIds);
            return reports
                .GroupBy(x => x.Key.Split('_').LastOrDefault())
                .ToDictionary(x => long.TryParse(x.Key, out var value) ? value : default,
                    x => x.Aggregate(0d, (res, val) => res + val.Value));
        }
        
        private static bool HasDateRange(GetAdvertisingTemplateReportQuery query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
    }
}