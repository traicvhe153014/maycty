﻿using System.Linq.Expressions;
using Novanet.Core.Exceptions;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Queries;

public class ListWebsitePriceQuery : INovanetRequest<ListWebsitePriceResponse>
{
    public int Page { get; set; }
    public int PageSize { get; set; }
    public PublisherPriceType PublisherPriceType { get; set; }
    public Guid WebsiteId { get; set; }
    public string? Sorts { get; set; }

    internal class Handler : NovanetRequestHandler<ListWebsitePriceQuery, ListWebsitePriceResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor, SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<ListWebsitePriceResponse> HandleAsync(ListWebsitePriceQuery request,
            CancellationToken cancellationToken)
        {
            Expression<Func<WebsitePrice, bool>> whereExpression =
                x => x.PublisherPriceType == request.PublisherPriceType;
            var website =
                await _context.Websites.FirstOrDefaultAsync(x => x.Id == request.WebsiteId, cancellationToken);
            if (website is null)
            {
                throw new BadRequestException("Không tồn tại website này");
            }

            whereExpression = whereExpression.And(x => x.WebsiteId == request.WebsiteId);
            var websitePrices = await _context.WebsitePrices
                .Include(x => x.WebsiteTemplatePrices)
                .Include(x => x.WebsitePriceCampaignTypes)
                .Where(whereExpression)
                .ToListAsync(cancellationToken);
            var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == website.PublisherId);

            if (request.PublisherPriceType == PublisherPriceType.Website)
            {
                var responses = new List<WebsitePriceResponse>();
                foreach (var groupedWebsitePrices in websitePrices.GroupBy(x => x.WebsiteId))
                {
                    var detailedPrices = new List<DetailedWebsitePrice>();
                    foreach (var websitePrice in groupedWebsitePrices.OrderBy(x => x.Status).ThenBy(x => x.StartDate))
                    {
                        var templatePrices = new Dictionary<int, TemplatePrice>();
                        if (websitePrice.WebsiteTemplatePrices is null || !websitePrice.WebsiteTemplatePrices.Any())
                        {
                            continue;
                        }

                        foreach (var websiteTemplatePrice in websitePrice.WebsiteTemplatePrices)
                        {
                            templatePrices[(int)websiteTemplatePrice.TemplateType] = new TemplatePrice
                            {
                                BuyPrice = websiteTemplatePrice.BuyPrice,
                                BuyPriceType = websiteTemplatePrice.BuyPriceType,
                                SellPrice = websiteTemplatePrice.SellPrice,
                                SellPriceType = websiteTemplatePrice.SellPriceType,
                                TemplateType = websiteTemplatePrice.TemplateType
                            };
                        }

                        var detailedPrice = new DetailedWebsitePrice
                        {
                            StartDate = websitePrice.StartDate,
                            EndDate = websitePrice.EndDate,
                            WebsitePriceId = websitePrice.Id,
                            TemplatePrices = templatePrices,
                            Status = websitePrice.Status,
                            IsActive = websitePrice.IsActive,
                            CampaignTypes = GetCampaignTypeByTemplateType(
                                websitePrice.WebsiteTemplatePrices!.Select(x => x.TemplateType).ToList())
                        };
                        detailedPrices.Add(detailedPrice);
                    }

                    var websitePriceResponse = new WebsitePriceResponse
                    {
                        WebsiteId = request.WebsiteId,
                        Prices = detailedPrices,
                        Domain = website.Domain,
                        Url = website.Url,
                        PublisherId = website.PublisherId,
                        PublisherEmail = publisher?.Email ?? string.Empty,
                        PublisherPriceType = request.PublisherPriceType
                    };
                    responses.Add(websitePriceResponse);
                }

                if (request.Sorts is not null)
                {
                    Func<WebsitePriceResponse, object?> orderExpression = x => request.Sorts.ToLower().Trim('-') switch
                    {
                        "enddate" => x.Prices.FirstOrDefault()?.EndDate,
                        "status" => x.Prices.FirstOrDefault()?.Status,
                        _ => true
                    };
                    responses = responses
                        .IfThenElse(() => request.Sorts.StartsWith("-"),
                            e => e.OrderByDescending(orderExpression),
                            e => e.OrderBy(orderExpression))
                        .ToList();
                }

                return new ListWebsitePriceResponse
                {
                    Data = responses
                };
            }

            if (request.PublisherPriceType == PublisherPriceType.Zone)
            {
                var zones = await _context.Zones.Where(x => x.WebsiteId == request.WebsiteId)
                    .ToListAsync(cancellationToken);
                var responses = new List<WebsitePriceResponse>();
                foreach (var groupedWebsitePrices in websitePrices.GroupBy(x => x.ZoneId))
                {
                    var detailedPrices = new List<DetailedWebsitePrice>();
                    foreach (var websitePrice in groupedWebsitePrices.OrderBy(x => x.Status)
                                 .ThenByDescending(x => x.StartDate))
                    {
                        var templatePrices = new Dictionary<int, TemplatePrice>();
                        if (websitePrice.WebsiteTemplatePrices is null || !websitePrice.WebsiteTemplatePrices.Any())
                        {
                            continue;
                        }

                        foreach (var websiteTemplatePrice in websitePrice.WebsiteTemplatePrices)
                        {
                            templatePrices[(int)websiteTemplatePrice.TemplateType] = new TemplatePrice
                            {
                                BuyPrice = websiteTemplatePrice.BuyPrice,
                                BuyPriceType = websiteTemplatePrice.BuyPriceType,
                                SellPrice = websiteTemplatePrice.SellPrice,
                                SellPriceType = websiteTemplatePrice.SellPriceType,
                                TemplateType = websiteTemplatePrice.TemplateType
                            };
                        }

                        var detailedPrice = new DetailedWebsitePrice
                        {
                            StartDate = websitePrice.StartDate,
                            EndDate = websitePrice.EndDate,
                            WebsitePriceId = websitePrice.Id,
                            TemplatePrices = templatePrices,
                            Status = websitePrice.Status,
                            IsActive = websitePrice.IsActive,
                            CampaignTypes = GetCampaignTypeByTemplateType(
                                websitePrice.WebsiteTemplatePrices!.Select(x => x.TemplateType).ToList())
                        };
                        detailedPrices.Add(detailedPrice);
                    }

                    var zone = zones.FirstOrDefault(x => x.Id == groupedWebsitePrices.Key);
                    var websitePriceResponse = new WebsitePriceResponse
                    {
                        WebsiteId = request.WebsiteId,
                        ZoneId = groupedWebsitePrices.Key,
                        ZoneName = zone?.Name,
                        Width = zone?.Width,
                        Height = zone?.Height,
                        Prices = detailedPrices,
                        Domain = website.Domain,
                        Url = website.Url,
                        PublisherId = website.PublisherId,
                        PublisherEmail = publisher?.Email ?? string.Empty,
                        PublisherPriceType = request.PublisherPriceType
                    };
                    responses.Add(websitePriceResponse);
                }

                if (request.Sorts is not null)
                {
                    Func<WebsitePriceResponse, object?> orderExpression = x => request.Sorts.ToLower().Trim('-') switch
                    {
                        "enddate" => x.Prices.FirstOrDefault()?.EndDate,
                        "status" => x.Prices.FirstOrDefault()?.Status,
                        _ => true
                    };
                    responses = responses
                        .IfThenElse(() => request.Sorts.StartsWith("-"),
                            e => e.OrderByDescending(orderExpression),
                            e => e.OrderBy(orderExpression))
                        .ToList();
                }

                return new ListWebsitePriceResponse
                {
                    Data = responses
                };
            }

            throw new BadRequestException("Không hỗ trợ loại giá này");
        }

        private static List<CampaignType> GetCampaignTypeByTemplateType(List<TemplateType> templateTypes)
        {
            var campaignTypes = new List<CampaignType>();
            var advertisingTemplates =
                CacheManager.Templates.Values.ToList();
            foreach (var templateType in templateTypes)
            {
                campaignTypes.Add(
                    advertisingTemplates.FirstOrDefault(x => x.TemplateType == templateType)!.CampaignType);
            }

            return campaignTypes.Distinct().ToList();
        }
    }
}

public class ListWebsitePriceQueryValidator : AbstractValidator<ListWebsitePriceQuery>
{
    public ListWebsitePriceQueryValidator()
    {
        RuleFor(x => x.WebsiteId)
            .NotEmpty().NotNull();
    }
}