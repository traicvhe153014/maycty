using System.Linq.Expressions;
using Novanet.Core.Specifications;

namespace Service.Settings.Application.Queries;

public class ListDomainAliasesQuery : INovanetRequest<List<DomainAlias>>
{
    public Guid WebsiteId { get; set; }
    internal class Handler : NovanetRequestHandler<ListDomainAliasesQuery, List<DomainAlias>>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<DomainAlias>> HandleAsync(ListDomainAliasesQuery request, 
            CancellationToken cancellationToken)
        {
            Expression<Func<DomainAlias, bool>> whereExpression = x => true;

            whereExpression = whereExpression.And(x => x.WebsiteId == request.WebsiteId);
            
            return await _context.DomainAliases.Where(whereExpression).ToListAsync(cancellationToken);
        }
    }
}