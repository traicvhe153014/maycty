﻿using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Queries;

public class GetProductOverview : INovanetRequest<ProductOverviewResponse>
{
    internal class Handler : NovanetRequestHandler<GetProductOverview, ProductOverviewResponse>
    {
        private readonly SettingsContext _context;
        private readonly AppSettings _appSettings;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor,
            AppSettings appSettings) : base(logger, httpContextAccessor)
        {
            _context = context;
            _appSettings = appSettings;
        }

        protected override Task<ProductOverviewResponse> HandleAsync(GetProductOverview request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            
            var total = CacheManager.Products.Count(x => IsAdmin(x.Value.CreatedBy, currentUserId));
            var error = CacheManager.Products.Count(x => !x.Value.IsValid && IsAdmin(x.Value.CreatedBy, currentUserId));
            var availabilityAttribute =
                CacheManager.ProductAttributes.Values.FirstOrDefault(x => x.Name == "Availability");
            var outOfStock = CacheManager.Products.Values.Count(x =>
            {
                if (!x.IsValid || !x.IsActive)
                {
                    return false;
                }
                var attributeValueCore = x.ProductAttributeValueCores?.FirstOrDefault(value =>
                    value.AttributeId == availabilityAttribute?.Id);
                var value = attributeValueCore?.GetValue(availabilityAttribute?.DataType)?.ToString() ?? string.Empty;
                return value.ToLower() == "hết hàng" && IsAdmin(x.CreatedBy, currentUserId);
            });

            var running = CacheManager.Products.Values.Count(x => x.IsValid
                                                                  && x.Status.Equals(ProductEntityStatus.Running)
                                                                  && IsAdmin(x.CreatedBy, currentUserId));

            var deactivated =
                CacheManager.Products.Values.Count(x =>
                    x.IsValid && !x.IsActive && IsAdmin(x.CreatedBy, currentUserId));

            var productOverviewResponse = new ProductOverviewResponse
            {
                Total = total,
                Error = error,
                OutOfStock = outOfStock,
                Running = running,
                Deactivated = deactivated
            };
            return Task.FromResult(productOverviewResponse);
        }

        private bool IsAdmin(Guid? createdBy, Guid? currentUserId)
        {
            return UserClaimsValue.IsAdmin || createdBy.Equals(currentUserId);
        }
    }
}