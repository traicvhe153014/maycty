﻿using System.Linq.Expressions;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessDomain.Service.Identity;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Queries;

public class ListZoneQuery : INovanetRequest<ListZoneResponse>
{
    public int Page { get; set; }

    public int PageSize { get; set; }
    
    public string? KeyWord { get; set; }
    
    public Guid? WebsiteId { get; set; }
    
    public bool WithZoneTemplate { get; set; }
    
    public string? Sorts { get; set; }

    internal class Handler : NovanetRequestHandler<ListZoneQuery, ListZoneResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) :
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<ListZoneResponse> HandleAsync(ListZoneQuery request,
            CancellationToken cancellationToken)
        {
            Expression<Func<Zone, bool>> whereExpression = x => true;
            Expression<Func<ZoneCore, bool>> cacheWhereExpression = x => true;

            Expression<Func<Zone, bool>> keyWordWhereExpression = x => true;
            Expression<Func<ZoneCore, bool>> cacheKeyWordWhereExpression = x => true;
            var canViewAll = CanViewAll();
            if (!string.IsNullOrWhiteSpace(request.KeyWord))
            {
                var loweredKeyWord = request.KeyWord.ToLower();
                keyWordWhereExpression = x =>
                    x.ZoneTemplates!.Any(y => y.AdvertisingTemplate!.Name.ToLower().Contains(loweredKeyWord)) ||
                    (x.Website != null && x.Website.Url.ToLower().Contains(request.KeyWord));
                cacheKeyWordWhereExpression = x =>
                    x.ZoneTemplateCores!.Any(y => y.AdvertisingTemplateCore!.Name.ToLower().Contains(loweredKeyWord)) ||
                    (x.WebsiteCore != null && x.WebsiteCore!.Url.ToLower().Contains(request.KeyWord));

                if (canViewAll)
                {
                    keyWordWhereExpression = keyWordWhereExpression.Or(x =>
                        x.Website != null && x.Website.Publisher != null && x.Website.Publisher.Email.ToLower().Contains(loweredKeyWord));
                    cacheKeyWordWhereExpression = cacheKeyWordWhereExpression.Or(x =>
                        x.WebsiteCore != null && x.WebsiteCore.Publisher != null && x.WebsiteCore.Publisher.Email.ToLower().Contains(loweredKeyWord));
                }
            }

            if (!canViewAll)
            {
                whereExpression = whereExpression.And(x => x.Website!.PublisherId == UserClaimsValue.Id);
                cacheWhereExpression = cacheWhereExpression.And(x =>
                    x.WebsiteCore != null && x.WebsiteCore!.PublisherId == UserClaimsValue.Id);
            }

            if (request.WebsiteId is not null)
            {
                whereExpression = whereExpression.And(x => x.WebsiteId == request.WebsiteId);
                cacheWhereExpression = cacheWhereExpression.And(x => x.WebsiteId == request.WebsiteId);
            }

            var sortsZones = request.Sorts?.Split(',') ?? Array.Empty<string>();
            var zones = (await _context.Zones
                    .Include(x => x.Website)
                    .Include(x => x.ZoneTemplates)
                    !.ThenInclude(x => x.AdvertisingTemplate)
                    .Where(whereExpression)
                    .SortBy(sortsZones)
                    .ToListAsync(cancellationToken))
                .Select(x =>
                {
                    if (x.Website is not null)
                    {
                        var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(y => y.Id == x.Website.PublisherId);
                        x.Website.Publisher = new UserCore
                        {
                            Email = publisher?.Email ?? string.Empty
                        };
                    }
                    return x;
                })
                .Where(keyWordWhereExpression.Compile())
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new ZoneResponse
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Height = x.Height,
                        Width = x.Width,
                        WebsiteId = x.WebsiteId,
                        Domain = x.Website != null ? x.Website.Domain : string.Empty,
                        Url = x.Website != null ? x.Website.Url : string.Empty,
                        UsingBackup = x.UsingBackup,
                        Code = x.Code,
                        BackupCode = x.BackupCode,
                        BackupType = x.BackupType,
                        PublisherId = x.Website?.PublisherId,
                        PublisherEmail = x.Website?.Publisher?.Email,
                        ZoneTemplates = !request.WithZoneTemplate ? null : x.ZoneTemplates?.Select(y => new ZoneTemplateResponse
                        {
                            Id = y.Id,
                            TemplateType = y.AdvertisingTemplate?.TemplateType,
                            ZoneId = y.ZoneId,
                            AdvertisingTemplateId = y.AdvertisingTemplateId
                        }).ToList(),
                        AdvertisingTemplateName = !request.WithZoneTemplate ? null 
                            : x.ZoneTemplates?.Select(y =>y.AdvertisingTemplate?.Name).ToList(),
                        RedirectLink = x.RedirectLink,
                        CampaignTypes = x.ZoneTemplates!.Select(y => y.AdvertisingTemplate!.CampaignType).Distinct().ToList(),
                    }
                )
                .ToList();
            var total = CacheManager.Zones.Values
                .Select(x =>
                {
                    var website = CacheManager.Websites.Values.FirstOrDefault(y => y.Id == x.WebsiteId);
                    if (website is not null)
                    {
                        var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(y => y.Id == website.PublisherId); 
                        website.Publisher = new UserCore
                        {
                            Email = publisher?.Email ?? string.Empty
                        };
                    }
                    return new ZoneCore
                    {
                        Id = x.Id,
                        WebsiteCore = website,
                        WebsiteId = x.WebsiteId,
                        ZoneTemplateCores = x.ZoneTemplateCores.Select(y =>
                        {
                            y.AdvertisingTemplateCore =
                                CacheManager.Templates.Values.FirstOrDefault(t => t.Id == y.AdvertisingTemplateId);
                            return y;
                        }).ToList()
                    };
                })
                .Where(cacheWhereExpression.Compile())
                .Where(cacheKeyWordWhereExpression.Compile())
                .Count();
            var response = new ListZoneResponse
            {
                Data = zones,
                Total = total
            };
            return response;
        }
        
         private bool CanViewAll()
        {
            return UserClaimsValue.Roles.All(x => x != "Publisher");
        }
    }
}