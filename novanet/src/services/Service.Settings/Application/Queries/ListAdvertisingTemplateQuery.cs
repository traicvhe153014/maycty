﻿namespace Service.Settings.Application.Queries;

public class ListAdvertisingTemplateQuery : INovanetRequest<List<AdvertisingTemplate>>
{
    internal class Handler : NovanetRequestHandler<ListAdvertisingTemplateQuery, List<AdvertisingTemplate>>
    {
        private readonly SettingsContext _context;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<AdvertisingTemplate>> HandleAsync(ListAdvertisingTemplateQuery request,
            CancellationToken cancellationToken)
        {
            return await _context.AdvertisingTemplates.ToListAsync(cancellationToken);
        }
    }
}