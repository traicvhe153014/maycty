﻿using Service.Settings.Models;

namespace Service.Settings.Application.Queries;

public class ListProductFeedGoogleSheetItemsQuery : INovanetRequest<List<Dictionary<string, FeedSheetValueModel>>>
{
    public string SheetId { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<ListProductFeedGoogleSheetItemsQuery,
        List<Dictionary<string, FeedSheetValueModel>>>
    {
        private readonly IProductFeedService _productFeedService;

        public Handler(ILogger<Handler> logger,
            IProductFeedService productFeedService,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _productFeedService = productFeedService;
        }

        protected override async Task<List<Dictionary<string, FeedSheetValueModel>>> HandleAsync(
            ListProductFeedGoogleSheetItemsQuery request, CancellationToken cancellationToken)
        {
            var values = await _productFeedService.ListAsync(request.SheetId);
            return values;
        }
    }
}

public class Validator : AbstractValidator<ListProductFeedGoogleSheetItemsQuery>
{
    public Validator()
    {
        RuleFor(t => t.SheetId).NotEmpty();
    }
}