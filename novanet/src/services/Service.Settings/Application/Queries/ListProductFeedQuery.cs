﻿using System.Linq.Expressions;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;

namespace Service.Settings.Application.Queries;

public class ListProductFeedQuery : INovanetRequest<List<ProductFeedResponse>>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public string? Keyword { get; set; }

    public string? Ids { get; set; }

    public string? Sorts { get; set; }

    public ProductFeedPermission? Permission { get; set; }

    public bool? WithProduct { get; set; }

    public int? ProductValidGreaterThan { get; set; }
    
    public bool? WithAvailableProducts { get; set; }

    internal class Handler : NovanetRequestHandler<ListProductFeedQuery, List<ProductFeedResponse>>
    {
        private readonly SettingsContext _context;
        private readonly AppSettings _appSettings;

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            AppSettings appSettings,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
            _appSettings = appSettings;
        }

        protected override async Task<List<ProductFeedResponse>> HandleAsync(ListProductFeedQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sorts = request.Sorts?.Split(",");

            Expression<Func<ProductFeed, bool>> whereExpression = x => !x.Deleted;
            if (request.Ids != null)
            {
                var idArray = request.Ids.Split(",");
                whereExpression = whereExpression.And(x => idArray.Contains(x.Id.ToString()));
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                whereExpression = whereExpression.And(x => x.Name.Contains(request.Keyword));
            }

            if (request.Permission is not null)
            {
                whereExpression = whereExpression.And(x => x.Permission == request.Permission);
            }

            if (request.ProductValidGreaterThan != null)
            {
                whereExpression = whereExpression.And(x => x.ValidProductsCount > request.ProductValidGreaterThan);
            }
            
            Expression<Func<ProductFeed, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            Expression<Func<ProductFeed, bool>> whereProductAvailabilityExpression = x => true;
            if (request.WithAvailableProducts == true)
            {
                whereProductAvailabilityExpression = x =>
                    x.ProductEntities.Any(y => y.Status == ProductEntityStatus.Running && !y.Deleted);
            }

            if (request.WithProduct == true)
            {
                var productFeeds = await _context.ProductFeeds.Include(x => x.ProductEntities)
                    .ThenInclude(x => x.AttributeValues)!.ThenInclude(x => x.ProductAttribute)
                    .Where(whereExpression)
                    .Where(whereIsAdminExpression)
                    .Where(whereProductAvailabilityExpression)
                    .SortBy(sorts)
                    .Skip((request.Page - 1) * request.PageSize).Take(request.PageSize)
                    .ToListAsync(cancellationToken);

                var productFeedResponses = productFeeds.Select(x => new ProductFeedResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    SourceType = x.SourceType,
                    SourcePath = string.Format(_appSettings.Configurations.GoogleSheetSourcePath, x.SourcePath),
                    Permission = x.Permission,
                    ModifiedAt = x.ModifiedAt,
                    ActiveProduct = x.ValidProductsCount,
                    InActiveProduct = x.InvalidProductsCount,
                    ProductEntities = x.ProductEntities.Select(y => new ProductEntityResponse
                    {
                        Id = y.Id,
                        CreatedAt = y.CreatedAt,
                        ProductFeedId = y.ProductFeedId,
                        AttributeValues = GetAttributeValue(y.AttributeValues)
                    }).ToList()
                });
                return productFeedResponses.ToList();
            }
            else
            {
                var productFeeds = await _context.ProductFeeds.Include(x => x.ProductEntities)
                    .Where(whereExpression)
                    .Where(whereIsAdminExpression)
                    .Where(whereProductAvailabilityExpression)
                    .SortBy(sorts)
                    .Skip((request.Page - 1) * request.PageSize).Take(request.PageSize)
                    .ToListAsync(cancellationToken);

                var productFeedResponses = productFeeds.Select(x => new ProductFeedResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    SourceType = x.SourceType,
                    SourcePath = string.Format(_appSettings.Configurations.GoogleSheetSourcePath, x.SourcePath),
                    Permission = x.Permission,
                    ModifiedAt = x.ModifiedAt,
                    ActiveProduct = x.ValidProductsCount,
                    InActiveProduct = x.InvalidProductsCount,
                });
                return productFeedResponses.ToList();
            }
        }

        private static Dictionary<string, object?> GetAttributeValue(List<ProductAttributeValue>? attributeValues)
        {
            var data = new Dictionary<string, object?>();
            if (attributeValues == null) return data;
            foreach (var attributeValue in attributeValues)
            {
                if (data.ContainsKey(attributeValue.ProductAttribute.Name))
                {
                    continue;
                }
                switch (attributeValue.ProductAttribute.DataType)
                {
                    case NovanetDataType.String:
                    case NovanetDataType.Url:
                        data.Add(attributeValue.ProductAttribute.Name, attributeValue.StringValue);
                        break;
                    case NovanetDataType.Number:
                        data.Add(attributeValue.ProductAttribute.Name, attributeValue.NumberValue);
                        break;
                    case NovanetDataType.Double:
                        data.Add(attributeValue.ProductAttribute.Name, attributeValue.DoubleValue);
                        break;
                    case NovanetDataType.DateTime:
                        data.Add(attributeValue.ProductAttribute.Name, attributeValue.DateTimeValue);
                        break;
                    case NovanetDataType.Boolean:
                        data.Add(attributeValue.ProductAttribute.Name, attributeValue.BooleanValue);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return data;
        }
    }
}