﻿using System.Linq.Expressions;
using Novanet.Core.Extensions.LinqExtensions;

namespace Service.Settings.Application.Queries;

public class ListObjectGroupQuery : INovanetRequest<List<ObjectGroupResponse>>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public int Skip { get; set; }

    public string? Keyword { get; set; }

    public string? Sorts { get; set; }

    internal class Handler : NovanetRequestHandler<ListObjectGroupQuery, List<ObjectGroupResponse>>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<ObjectGroupResponse>> HandleAsync(ListObjectGroupQuery request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            
            var sorts = request.Sorts?.Split(",");
            Expression<Func<ObjectGroup, bool>> whereExpression = x => true;
            if (!string.IsNullOrEmpty(request.Keyword))
            {
                whereExpression = x => x.Name.Contains(request.Keyword);
            }
            
            Expression<Func<ObjectGroup, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            var skip = request.Skip.Equals(0) ? (request.Page - 1) * request.PageSize : request.Skip;

            var objectGroups = await _context.ObjectGroups
                .Where(whereExpression)
                .Where(whereIsAdminExpression)
                .SortBy(sorts)
                .Skip(skip)
                .Take(request.PageSize)
                .Select(x => new ObjectGroupResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    ConnectionStatus = x.ConnectionStatus,
                    SubId = x.SubId,
                    ModifiedAt = x.ModifiedAt
                })
                .ToListAsync(cancellationToken);
            return objectGroups;
        }
    }
}