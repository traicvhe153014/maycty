﻿using Novanet.Core.Exceptions;

namespace Service.Settings.Application.Queries;

public class GetObjectGroupById : INovanetRequest<ObjectGroupResponse>
{
    public Guid ObjectGroupId { get; set; }

    internal class Handler : NovanetRequestHandler<GetObjectGroupById, ObjectGroupResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<ObjectGroupResponse> HandleAsync(GetObjectGroupById request,
            CancellationToken cancellationToken)
        {
            var objectGroup = await _context.ObjectGroups
                .Include(x => x.ConsumerBehaviorsCore)
                .Include(x => x.WebsiteConditionsCore)
                .FirstOrDefaultAsync(x => x.Id.Equals(request.ObjectGroupId), cancellationToken: cancellationToken);

            if (objectGroup == null) throw new BadRequestException("Không tìm thấy nhóm đối tượng");
            var objectGroupResponse = new ObjectGroupResponse
            {
                Id = objectGroup.Id,
                Name = objectGroup.Name,
                CreatedAt = objectGroup.CreatedAt,
                ModifiedAt = objectGroup.ModifiedAt,
                WebsiteConditionsCore = objectGroup.WebsiteConditionsCore,
                ConsumerBehaviorsCore = objectGroup.ConsumerBehaviorsCore,
                ProductName = objectGroup.ProductName,
                ProductType = objectGroup.ProductType,
                ApplyAllProducts = objectGroup.ApplyAllProducts
            };

            return objectGroupResponse;
        }
    }
}