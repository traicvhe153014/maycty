﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateProductCommand : INovanetRequest<ProductResponse>
{
    public Guid Id { get; set; }

    public bool IsActive { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateProductCommand, ProductResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Handler(ILogger<Handler> logger,
            IServiceScopeFactory serviceScopeFactory,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _serviceScopeFactory = serviceScopeFactory;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<ProductResponse> HandleAsync(
            UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var product = await _context.ProductEntities
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (product == null) throw new BadRequestException("Sản phẩm không tồn tại");

            product.IsActive = request.IsActive;

            product.ModifiedBy = currentUserId;
            if (!request.IsActive)
            {
                product.Status = ProductEntityStatus.Deactivated;
            }
            else
            {
                var availability = await _context.ProductAttributeValues
                    .Include(x => x.ProductAttribute)
                    .FirstOrDefaultAsync(x => x.ProductId == request.Id && x.ProductAttribute.Name == "Availability", cancellationToken);
                var availabilityValue = availability?.StringValue?.ToLower();
                product.Status = availabilityValue switch
                {
                    "hết hàng" => ProductEntityStatus.OutOfStock,
                    null => product.Status,
                    _ => ProductEntityStatus.Running
                };
            }
            product.ModifiedAt = DateTime.Now;
            _context.ProductEntities.Update(product);
            
            CacheManager.Products.TryGetValue(product.SubId, out var cachedProduct);
            if (cachedProduct is not null)
            {
                cachedProduct.IsActive = product.IsActive;
                cachedProduct.Status = product.Status;

                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductEntityCore>(cachedProduct.SubId), cachedProduct);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductEntityCore>(), new PatchCacheManagerSignal<ProductEntityCore>
                {
                    Entity = cachedProduct,
                    EntitySubId = cachedProduct.SubId
                });
            }


            await _context.SaveChangesAsync(cancellationToken);
            
            using var scope = _serviceScopeFactory.CreateScope();
            var updateStatusService = scope.ServiceProvider.GetRequiredService<IUpdateStatusService>();
            
            await updateStatusService.UpdateProductGroup(product.ProductFeedId);
            
            await updateStatusService.UpdateAdvertisingStatus(product.ProductFeedId);
            
            await updateStatusService.UpdateAdvertisingSetStatus(product.ProductFeedId);
            
            await updateStatusService.UpdateCampaignStatus(product.ProductFeedId);
            
            return new ProductResponse
            {
                  ProductId = product.Id,
                  IsActive = product.IsActive,
                  ProductFeedId = product.ProductFeedId,
                  Status = product.Status
            };
        }

        private bool IsAdmin(Guid? createdBy, Guid? currentUserId)
        {
            return UserClaimsValue.IsAdmin || createdBy.Equals(currentUserId);
        }
    }
}