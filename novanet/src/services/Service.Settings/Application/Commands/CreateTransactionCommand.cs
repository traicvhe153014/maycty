﻿using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Exceptions;
using Novanet.Core.Models;
using Novanet.Core.Models.MessageBusModels;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;

namespace Service.Settings.Application.Commands;

public class CreateTransactionCommand : INovanetRequest<PaymentResponse>
{
    public string Email { get; set; } = default!;

    public double Amount { get; set; }

    public string? Note { get; set; }

    internal class Handler : NovanetRequestHandler<CreateTransactionCommand, PaymentResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBus;
        private readonly IReportService _reportService;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor,
            IGlobalCacheService globalCacheService,
            IMessageBusClient messageBus, IReportService reportService) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBus = messageBus;
            _reportService = reportService;
        }

        protected override async Task<PaymentResponse> HandleAsync(CreateTransactionCommand request,
            CancellationToken cancellationToken)
        {
            IdentityUserValue user;
            try
            {
                user = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Email == request.Email) ?? throw new Exception();
            }
            catch (Exception)
            {
                throw new BadRequestException("Tài khoản giao dịch không có trong hệ thống bạn vui lòng kiểm tra lại");
            }

            if (user is null)
                throw new BadRequestException("Tài khoản giao dịch không có trong hệ thống bạn vui lòng kiểm tra lại");

            if (user.Roles?.All(x => x == "Accountant") == true)
            {
                throw new BadRequestException("Không thể nạp tiền cho kế toán");
            }
            
            var paid = await _reportService.GetSingleAdvertiserReport(ReportMetric.Paid,
                ReportAdvertiserDimension.Advertiser, user.SubId);

            if (request.Amount < 0 && user.Amount - paid + request.Amount < 0)
            {
                throw new BadRequestException("Không thể rút số tiền lớn hơn số tiền trong tài khoản");
            }

            var currentUserId = UserClaimsValue.Id;
            var payment = new Payment
            {
                Email = request.Email,
                Amount = request.Amount,
                Note = request.Note,
                ModifiedAt = DateTimeOffset.Now,
                CreatedAt = DateTimeOffset.Now,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
            };

            await _context.AddAsync(payment, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            _messageBus.Publish(MessageBusChannels.TransactionValidator, new TransactionValidatorModel
            {
                Amount = request.Amount,
                Id = user.Id
            });

            var paymentResponse = new PaymentResponse
            {
                Email = request.Email,
                Amount = request.Amount,
                CreatedAt = DateTimeOffset.Now
            };

            return paymentResponse;
        }
    }
}

public class CreateTransactionCommandValidator : AbstractValidator<CreateTransactionCommand>
{
    public CreateTransactionCommandValidator()
    {
        RuleFor(x => x.Email).NotEmpty();
        RuleFor(x => x.Amount).NotEmpty();
    }
}