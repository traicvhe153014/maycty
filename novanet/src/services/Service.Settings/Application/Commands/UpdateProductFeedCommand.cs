﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;
using SourceType = NovanetCore.Business.BusinessDomain.Service.Settings.SourceType;

namespace Service.Settings.Application.Commands;

public class UpdateProductFeedCommand : INovanetRequest<ProductFeed>
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;

    public SourceType SourceType { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateProductFeedCommand, ProductFeed>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(
            ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<ProductFeed> HandleAsync(UpdateProductFeedCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var productFeed =
                await _context.ProductFeeds.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (productFeed is null)
            {
                throw new BadRequestException("Nguồn dữ liệu sản phẩm không tồn tại");
            }

            productFeed.Name = request.Name;
            productFeed.SourceType = request.SourceType;
            productFeed.ModifiedBy = currentUserId;
            _context.ProductFeeds.Update(productFeed);
            await _context.SaveChangesAsync(cancellationToken);
            
            CacheManager.ProductFeeds.TryGetValue(productFeed.SubId, out var cachedProductFeed);
            if (cachedProductFeed is not null)
            {
                cachedProductFeed.Name = productFeed.Name;
                cachedProductFeed.SourceType = productFeed.SourceType;
                cachedProductFeed.ModifiedBy = productFeed.ModifiedBy;

                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductFeedCore>(cachedProductFeed.SubId), cachedProductFeed);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductFeedCore>(), new PatchCacheManagerSignal<ProductFeedCore>
                {
                    Entity = cachedProductFeed,
                    EntitySubId = cachedProductFeed.SubId
                });
            }

            return productFeed;
        }
    }
}

public class UpdateProductFeedCommandValidator : AbstractValidator<UpdateProductFeedCommand>
{
    public UpdateProductFeedCommandValidator()
    {
        RuleFor(x => x.Name).NotEmpty();
        RuleFor(x => x.SourceType).NotEmpty();
    }
}