﻿namespace Service.Settings.Application.Commands;

public class RefreshProductFeedsCommand : INovanetRequest<List<ProductFeedResponse>>
{
    public List<Guid> ProductFeedIds { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<RefreshProductFeedsCommand, List<ProductFeedResponse>>
    {
        private readonly SettingsContext _context;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IMediator _mediator;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IServiceScopeFactory serviceScopeFactory,
            IHttpContextAccessor httpContextAccessor,
            IMediator mediator) : base(logger, httpContextAccessor)
        {
            _context = context;
            _serviceScopeFactory = serviceScopeFactory;
            _mediator = mediator;
        }

        protected override async Task<List<ProductFeedResponse>> HandleAsync(RefreshProductFeedsCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var productFeeds = await _context.ProductFeeds
                .Include(x => x.ProductEntities)
                .Where(x => request.ProductFeedIds.Contains(x.Id))
                .ToListAsync(cancellationToken);

            foreach (var productFeed in productFeeds)
            {
                _context.ProductStatus.RemoveRange(
                    _context.ProductStatus.Where(x => x.ProductFeedId == productFeed.Id));
                _context.ProductStatus.RemoveRange(
                    _context.ProductStatus.Where(x =>
                        x.ProductEntity != null && x.ProductEntity.ProductFeedId == productFeed.Id));
                _context.ProductStatus.RemoveRange(
                    _context.ProductStatus.Where(x =>
                        x.ProductAttributeValue != null &&
                        x.ProductAttributeValue.ProductEntity.ProductFeedId == productFeed.Id));
                // _context.ProductAttributeValues.RemoveRange(
                //     _context.ProductAttributeValues.Where(x => x.ProductEntity.ProductFeedId == productFeed.Id));
                // _context.ProductEntities.RemoveRange(productFeed.ProductEntities);
                await _context.SaveChangesAsync(cancellationToken);

                productFeed.ModifiedAt = DateTimeOffset.Now;
                productFeed.Permission = ProductFeedPermission.Processing;
                productFeed.InvalidProductsCount = 0;
                productFeed.ValidProductsCount = 0;
                _context.ProductFeeds.Update(productFeed);
                await _context.SaveChangesAsync(cancellationToken);

                // TODO: add to processing queue
                _ = Task.Run(async () =>
                {
                    using var scope = _serviceScopeFactory.CreateScope();
                    var productFeedService = scope.ServiceProvider.GetRequiredService<IProductFeedService>();
                    await productFeedService.ProcessGoogleSheet(productFeed.Id, currentUserId, UserClaimsValue.IsAdmin);
                }, CancellationToken.None);
            }

            var response = await _mediator.Send(new ListProductFeedQuery
            {
                Ids = request.ProductFeedIds.Select(x => x.ToString()).JoinString(","),
                Page = 1,
                PageSize = request.ProductFeedIds.Count
            }, cancellationToken);
            return response.Data;
        }
    }
}