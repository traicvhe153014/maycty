﻿namespace Service.Settings.Application.Commands;

public class SyncWebsitePriceCommand : INovanetRequest<object>
{
    internal class Handler : NovanetRequestHandler<SyncWebsitePriceCommand, object>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<NovanetRequestHandler<SyncWebsitePriceCommand, object>> logger,
            IHttpContextAccessor httpContextAccessor, SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<object> HandleAsync(SyncWebsitePriceCommand request, CancellationToken cancellationToken)
        {
            var templates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);

            var websitePrices = await _context.WebsitePrices
                .Where(x => x.PublisherPriceType != PublisherPriceType.Website).ToListAsync(cancellationToken);
            foreach (var websitePrice in websitePrices)
            {
                var zone = await _context.Zones.Include(x => x.ZoneTemplates)
                    .FirstOrDefaultAsync(x => x.Id == websitePrice.ZoneId, cancellationToken);
                if (zone?.ZoneTemplates is null)
                {
                    continue;
                }
                foreach (var zoneTemplate in zone.ZoneTemplates)
                {
                    var template = templates.FirstOrDefault(x => x.Id == zoneTemplate.AdvertisingTemplateId);
                    var websiteTemplatePrice = new WebsiteTemplatePrice
                    {
                        BuyPrice = websitePrice.BuyPrice,
                        BuyPriceType = websitePrice.BuyPriceType,
                        SellPrice = websitePrice.SellPrice,
                        SellPriceType = websitePrice.SellPriceType,
                        TemplateType = websitePrice.TemplateType,
                        WebsitePriceId = websitePrice.Id,
                        AdvertisingTemplateId = template?.Id,
                        ZoneTemplateId = zoneTemplate.Id,
                        CreatedBy = websitePrice.CreatedBy,
                        ModifiedBy = websitePrice.ModifiedBy
                    };
                    await _context.WebsiteTemplatePrices.AddAsync(websiteTemplatePrice, cancellationToken);
                }
                websitePrice.PublisherPriceType = PublisherPriceType.Zone;
                _context.WebsitePrices.Update(websitePrice);
            }

            await _context.SaveChangesAsync(cancellationToken);
            return "Ok";
        }
    }
}