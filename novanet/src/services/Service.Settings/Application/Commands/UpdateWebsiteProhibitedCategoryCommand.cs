using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateWebsiteProhibitedCategoryCommand : INovanetRequest<List<UpdateWebsiteProhibitedCategoryResponse>>
{
    public Guid WebsiteId { get; set; }

    public List<Guid>? ProhibitedCategoriesId { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateWebsiteProhibitedCategoryCommand,
        List<UpdateWebsiteProhibitedCategoryResponse>>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;
        
        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService) : base(
            logger, httpContextAccessor)
        {
            _context = context;
            _messageBusClient = messageBusClient;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<List<UpdateWebsiteProhibitedCategoryResponse>> HandleAsync(
            UpdateWebsiteProhibitedCategoryCommand request, CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var websiteOld = _context.WebsiteProhibitedCategories.Where(x => x.WebsiteId == request.WebsiteId).ToList();
            _context.WebsiteProhibitedCategories.RemoveRange(websiteOld);

            var response = new List<EntityEntry<WebsiteProhibitedCategory>>();
            if (request.ProhibitedCategoriesId is not null)
            {
                foreach (var categoriesId in request.ProhibitedCategoriesId)
                {
                    var newWebsite = await _context.WebsiteProhibitedCategories.AddAsync(new WebsiteProhibitedCategory
                    {
                        WebsiteId = request.WebsiteId,
                        ProhibitedCategoryId = categoriesId,
                        CreatedBy = currentUserId,
                        ModifiedBy = currentUserId,
                    }, cancellationToken);
                    response.Add(newWebsite);
                }
            }
            await _context.SaveChangesAsync(cancellationToken);

            var cachedWebsite = CacheManager.Websites.Values.FirstOrDefault(x => x.Id == request.WebsiteId);
            if (cachedWebsite is not null)
            {
                // cachedWebsite.WebsiteProhibitedCategoryCores
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<WebsiteCore>(cachedWebsite.SubId), cachedWebsite);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsiteCore>(), new PatchCacheManagerSignal<WebsiteCore>
                {
                    Entity = cachedWebsite,
                    EntitySubId = cachedWebsite.SubId
                });
            }

            var listWebsiteProhibitedCategoryResponse = new List<UpdateWebsiteProhibitedCategoryResponse>();
            foreach (var webRespone in response)
            {
                listWebsiteProhibitedCategoryResponse
                    .Add(new UpdateWebsiteProhibitedCategoryResponse
                {
                    Id = webRespone.Entity.Id,
                    ProhibitedCategoryId = webRespone.Entity.ProhibitedCategoryId,
                    WebsiteId = webRespone.Entity.WebsiteId,
                });
            }

            return listWebsiteProhibitedCategoryResponse;
        }
    }
}

public class CreateWebsiteProhibitedCategoryCommandValidator : AbstractValidator<UpdateWebsiteProhibitedCategoryCommand>
{
    public CreateWebsiteProhibitedCategoryCommandValidator()
    {
        RuleFor(x => x.WebsiteId).NotEmpty();
    }
}