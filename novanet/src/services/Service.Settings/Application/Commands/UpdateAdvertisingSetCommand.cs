﻿using System.Linq.Expressions;
using Novanet.Core.Domain;
using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateAdvertisingSetCommand : INovanetRequest<AdvertisingSet>
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    public bool? IsActive { get; set; }
    public bool IsFullUpdate { get; set; }
    public bool? ApplyAllProductGroups { get; set; }
    public List<Guid>? ProductGroupIds { get; set; }
    public bool? HasProhibitedCategories { get; set; }
    public List<Guid>? ProhibitedCategoryIds { get; set; }
    public bool? HasObject { get; set; }
    public List<Guid>? ObjectIds { get; set; }
    public List<Guid>? LocationIds { get; set; }
    
    public List<Guid>? CategoryIds { get; set; }
    public List<Guid>? GenderIds { get; set; }
    public List<Guid>? AgeIds { get; set; }
    public List<Guid>? SelectedWebsites { get; set; }
    public List<Guid>? ExcludedWebsites { get; set; }
    public bool? Uniformed { get; set; }
    public double? UniformedPrice { get; set; }
    public UniformedUnit? UniformedUnit { get; set; }
    public RemarketingType? RemarketingType { get; set; }
    public int? AdImpressions { get; set; }
    public int? RemarketingTime { get; set; }
    public int? TimeOnDisplay { get; set; }
    public bool? TargetingMarketingByProduct { get; set; }
    public bool? ClickedAdvertising { get; set; }
    public bool? Viewer { get; set; }
    public int? ViewedMax { get; set; }
    public bool? ExcludedWebsite { get; set; }
    public List<AdvertisingSetScheduling>? Schedulings { get; set; }
    public bool? ScheduledAllTime { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateAdvertisingSetCommand, AdvertisingSet>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            IServiceScopeFactory serviceScopeFactory,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _serviceScopeFactory = serviceScopeFactory;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<AdvertisingSet> HandleAsync(UpdateAdvertisingSetCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var advertisingSet = await _context.AdvertisingSets
                .Include(x => x.Campaign)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (advertisingSet == null)
            {
                throw new BadRequestException("Nhóm quảng cáo không tồn tại");
            }

            var advertisingSetAges =
                await _context.AdvertisingSetAges.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetCategories =
                await _context.AdvertisingSetCategories.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetGenders =
                await _context.AdvertisingSetGenders.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetLocations =
                await _context.AdvertisingSetLocations.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetObjects =
                await _context.AdvertisingSetObjects.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetProductGroups =
                await _context.AdvertisingSetProductGroups.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetProhibitedCategories =
                await _context.AdvertisingSetProhibitedCategories.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetWebsites =
                await _context.AdvertisingSetWebsites.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);
            var advertisingSetSchedulings =
                await _context.AdvertisingSetScheduling.Where(x => x.AdvertisingSetId == advertisingSet.Id)
                    .ToListAsync(cancellationToken);

            var campaign = await _context.Campaigns
                .FirstOrDefaultAsync(x => x.Id == advertisingSet.CampaignId, cancellationToken);
            if (campaign?.CampaignType == CampaignType.Ecommerce)
            {
                campaign = await _context.Campaigns
                    .Include(x => x.EcommerceProductFeed)
                    .ThenInclude(x => x!.ProductEntities)
                    .FirstOrDefaultAsync(x => x.Id == advertisingSet.CampaignId, cancellationToken);
            }

            if (request.IsFullUpdate)
            {
                // update ages
                await UpdateChildrenEntities(
                    advertisingSetAges,
                    request.AgeIds,
                    x => x.AgeId,
                    x => new AdvertisingSetAge {AdvertisingSetId = advertisingSet.Id, AgeId = x},
                    cancellationToken);

                // update genders
                await UpdateChildrenEntities(
                    advertisingSetGenders,
                    request.GenderIds,
                    x => x.GenderId,
                    x => new AdvertisingSetGender {AdvertisingSetId = advertisingSet.Id, GenderId = x},
                    cancellationToken);
                
                // update Category
                await UpdateChildrenEntities(
                    advertisingSetCategories,
                    request.CategoryIds,
                    x => x.CategoryId,
                x => new AdvertisingSetCategory() {AdvertisingSetId = advertisingSet.Id, CategoryId = x},
                    cancellationToken);
                
                // update location
                await UpdateChildrenEntities(
                    advertisingSetLocations,
                    request.LocationIds,
                    x => x.LocationId,
                    x => new AdvertisingSetLocation {AdvertisingSetId = advertisingSet.Id, LocationId = x},
                    cancellationToken);

                // update objects
                await UpdateChildrenEntities(
                    advertisingSetObjects,
                    request.ObjectIds,
                    x => x.ObjectId,
                    x => new AdvertisingSetObject {AdvertisingSetId = advertisingSet.Id, ObjectId = x},
                    cancellationToken,
                    async (removedList, addedList) =>
                    {
                        var objectIds = removedList.Select(x => x.ObjectId).Distinct().ToList();
                        var objectIdRunningCounts = await _context.AdvertisingSetObjects
                            .Where(x => objectIds.Any(y => y == x.ObjectId))
                            .GroupBy(x => x.ObjectId)
                            .Select(x =>
                                new
                                {
                                    ObjectId = x.Key,
                                    Count = x.Count()
                                })
                            .Where(x => x.Count == 0)
                            .ToListAsync(cancellationToken);
                        foreach (var objectId in objectIds)
                        {
                            var existing = objectIdRunningCounts.FirstOrDefault(item => item.ObjectId == objectId);
                            if (existing is null)
                            {
                                var objectGroupToUpdate = new ObjectGroup { Id = objectId!.Value };
                                _context.ObjectGroups.Attach(objectGroupToUpdate);
                                _context.Entry(objectGroupToUpdate).Property(x => x.ConnectionStatus).IsModified = true;
                                objectGroupToUpdate.ConnectionStatus = false;
                            }
                        }

                        foreach (var item in addedList)
                        {
                            var objectGroupToUpdate = new ObjectGroup { Id = item.ObjectId!.Value };
                            _context.ObjectGroups.Attach(objectGroupToUpdate);
                            _context.Entry(objectGroupToUpdate).Property(x => x.ConnectionStatus).IsModified = true;
                            objectGroupToUpdate.ConnectionStatus = true;
                        }

                    });

                // update prohibited categories
                await UpdateChildrenEntities(
                    advertisingSetProhibitedCategories,
                    request.ProhibitedCategoryIds,
                    x => x.ProhibitedCategoryId,
                    x => new AdvertisingSetProhibitedCategory
                        {AdvertisingSetId = advertisingSet.Id, ProhibitedCategoryId = x},
                    cancellationToken);

                await UpdateWebsites(
                    advertisingSet.Id,
                    advertisingSetWebsites,
                    request.ExcludedWebsites,
                    request.SelectedWebsites,
                    cancellationToken);

                advertisingSet.HasProhibitedCategories = request.HasProhibitedCategories ?? default;
                advertisingSet.HasObject = request.HasObject ?? default;
                advertisingSet.Uniformed = request.Uniformed;
                advertisingSet.RemarketingType = request.RemarketingType;
                advertisingSet.AdImpressions = request.AdImpressions;
                advertisingSet.RemarketingTime = request.RemarketingTime;
                advertisingSet.TimeOnDisplay = request.TimeOnDisplay;
                advertisingSet.TargetingMarketingByProduct = request.TargetingMarketingByProduct;
                advertisingSet.ClickedAdvertising = request.ClickedAdvertising;
                advertisingSet.Viewer = request.Viewer;
                advertisingSet.ViewedMax = request.ViewedMax;
                advertisingSet.ScheduledAllTime = request.ScheduledAllTime ?? default;
                advertisingSet.ExcludedWebsite = request.ExcludedWebsite ?? default;
                advertisingSet.ModifiedAt = DateTimeOffset.Now;

                if (campaign?.EcommerceStartDate > DateTimeOffset.UtcNow)
                {
                    advertisingSet.UniformedPrice = request.UniformedPrice;
                    advertisingSet.UniformedUnit = request.UniformedUnit;
                }

                if (advertisingSet.ApplyAllProductGroups == false)
                {
                    advertisingSet.ApplyAllProductGroups = request.ApplyAllProductGroups ?? default;
                    // update product groups
                    await UpdateChildrenEntities(
                        advertisingSetProductGroups,
                        request.ProductGroupIds,
                        x => x.ProductGroupId,
                        x => new AdvertisingSetProductGroup
                        {
                            AdvertisingSetId = advertisingSet.Id,
                            ProductGroupId = x,
                            IsActive = true,
                            Status = AdvertisingSetStatus.Running
                        },
                        cancellationToken);
                }
            }

            if (campaign?.EcommerceScheduled == false)
            {
                _context.AdvertisingSetScheduling.RemoveRange(advertisingSetSchedulings);
                var updatedSchedulings = request.Schedulings?.Select(x => new AdvertisingSetScheduling
                {
                    Timing = x.Timing,
                    AdvertisingSetId = advertisingSet.Id,
                    DayOfWeek = x.DayOfWeek
                }).ToList() ?? new List<AdvertisingSetScheduling>();
                await _context.AdvertisingSetScheduling.AddRangeAsync(updatedSchedulings, cancellationToken);
            }
            
            var runningAdvertising = CacheManager.Advertising.Values
                .Count(x => x.IsActive.Equals(true) && x.AdvertisingSetId.Equals(advertisingSet.Id) && x.Status.Equals(AdvertisingStatus.Running));

            var totalAdvertisingSetProductGroup = CacheManager.AdvertisingSetProductGroups.Values
                .Count(x => x.IsActive.Equals(true) && x.AdvertisingSetId.Equals(advertisingSet.Id) && x.Status.Equals(AdvertisingSetStatus.Running));


            if (!request.IsActive.Equals(null) && !advertisingSet.Status.Equals(AdvertisingSetStatus.Error) && !advertisingSet.Status.Equals(AdvertisingSetStatus.Unfinished))
            {
                if (totalAdvertisingSetProductGroup.Equals(0))
                {
                    if (advertisingSet.ApplyAllProductGroups)
                    {
                        if (!advertisingSet.Status.Equals(AdvertisingSetStatus.Error))
                        {
                            advertisingSet.Status = advertisingSet.Status.Equals(AdvertisingSetStatus.Unfinished) ? AdvertisingSetStatus.Unfinished :
                                request.IsActive.Value && !runningAdvertising.Equals(0) ? AdvertisingSetStatus.Running : AdvertisingSetStatus.Paused;   
                        }
                    }
                    else
                    {
                        advertisingSet.Status = AdvertisingSetStatus.Paused;
                    }
                }
                else
                {
                    if (advertisingSet.IsActive != request.IsActive && advertisingSet.Status != AdvertisingSetStatus.Error &&
                        advertisingSet.Status != AdvertisingSetStatus.Finished)
                    {
                        advertisingSet.Status = 
                            campaign?.Status == CampaignStatus.NotStarted ? AdvertisingSetStatus.Paused
                            : advertisingSet.Status.Equals(AdvertisingSetStatus.Unfinished) ? AdvertisingSetStatus.Unfinished :
                            request.IsActive.Value && !runningAdvertising.Equals(0) ? AdvertisingSetStatus.Running : AdvertisingSetStatus.Paused;
                    }   
                }
            }

            advertisingSet.Name = request.Name;
            if (campaign?.EcommerceProductFeed?.Deleted == true ||
                campaign?.EcommerceProductFeed?.Permission is ProductFeedPermission.CannotAccess or ProductFeedPermission.CannotParse || (campaign?.EcommerceProductFeed?.ProductEntities.All(x => x.Status is ProductEntityStatus.OutOfStock or ProductEntityStatus.Deactivated) ?? false))
            {
                advertisingSet.Status = AdvertisingSetStatus.Error;
            }
            
            if (request.IsActive != null) advertisingSet.IsActive = request.IsActive.Value;
            advertisingSet.ModifiedBy = currentUserId;
            if (advertisingSet.Status.Equals(AdvertisingSetStatus.Running))
            {
                advertisingSet.ModifiedAt = DateTime.Now;
            }
            _context.AdvertisingSets.Update(advertisingSet);

            await _context.SaveChangesAsync(cancellationToken);

            var updatedAdvertisingSet = await _context.AdvertisingSets
                .AsNoTracking()
                .Include(x => x.AdvertisingSetLocations)
                .Include(x => x.AdvertisingSetCategories)
                .Include(x => x.AdvertisingSetWebsites)
                .Include(x => x.AdvertisingSetScheduling)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (updatedAdvertisingSet is not null)
            {
                updatedAdvertisingSet.LocationCores =
                    updatedAdvertisingSet.AdvertisingSetLocations?.Cast<AdvertisingSetLocationCore>().ToList() ??
                    new List<AdvertisingSetLocationCore>();
                updatedAdvertisingSet.CategoryCores =
                    updatedAdvertisingSet.AdvertisingSetCategories?.Cast<AdvertisingSetCategoryCore>().ToList() ??
                    new List<AdvertisingSetCategoryCore>(); 
                updatedAdvertisingSet.AdvertisingSetWebsiteCores =
                    updatedAdvertisingSet.AdvertisingSetWebsites?.Cast<AdvertisingSetWebsiteCore>().ToList() ??
                    new List<AdvertisingSetWebsiteCore>();
                updatedAdvertisingSet.SchedulingCore =
                    updatedAdvertisingSet.AdvertisingSetScheduling?.Cast<AdvertisingSetSchedulingCore>().ToList() ??
                    new List<AdvertisingSetSchedulingCore>();
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetCore>(updatedAdvertisingSet.SubId), updatedAdvertisingSet);

                CacheManager.AdvertisingSets[updatedAdvertisingSet.SubId] = updatedAdvertisingSet;
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(), new PatchCacheManagerSignal<AdvertisingSetCore>
                {
                    Entity = updatedAdvertisingSet,
                    EntitySubId = updatedAdvertisingSet.SubId
                });

                await UpdateAdvertisingSetChildrenCache<AdvertisingSetProductGroupCore, AdvertisingSetProductGroup>(updatedAdvertisingSet);
                await UpdateAdvertisingSetChildrenCache<AdvertisingSetObjectCore, AdvertisingSetObject>(updatedAdvertisingSet);
            }
            
            using var scope = _serviceScopeFactory.CreateScope();

            var updateStatusService = scope.ServiceProvider.GetRequiredService<IUpdateStatusService>();

            if (advertisingSet.Campaign?.CampaignType == CampaignType.Ecommerce &&
                advertisingSet.Campaign?.EcommerceProductFeedId == null) return advertisingSet;

            if (advertisingSet.Status.Equals(AdvertisingSetStatus.Error))
                return advertisingSet;
            switch (advertisingSet.Campaign?.CampaignType)
            {
                case CampaignType.Ecommerce:
                    await updateStatusService.UpdateAdvertisingSetStatus(advertisingSet.Campaign!.EcommerceProductFeedId!.Value);
                    await updateStatusService.UpdateCampaignStatus(advertisingSet.Campaign?.EcommerceProductFeedId);
                    break;
                case CampaignType.Display:
                    await updateStatusService.UpdateSingleCampaignStatus(advertisingSet.CampaignId!.Value);
                    break;
                default:
                    break;
            }
            return advertisingSet;
        }

        private async Task UpdateChildrenEntities<T>(
            List<T>? entities,
            List<Guid>? updateIds,
            Func<T, Guid?> getExistingId,
            Func<Guid, T> getNewEntity,
            CancellationToken cancellationToken,
            Func<List<T>, List<T>, Task>? callback = null) where T : NovanetDocument
        {
            var removedList = new List<T>();
            var addedList = new List<T>();

            // Process entities
            if (entities is not null)
            {
                // Delete entities
                foreach (var existingEntity in entities)
                {
                    var existingId = getExistingId(existingEntity);
                    if (existingId is null) continue;
                    if (!updateIds?.Any(c => c == existingId) ?? default)
                    {
                        _context.Set<T>().Remove(existingEntity);
                        removedList.Add(existingEntity);
                    }
                }

                // Update and Insert entity
                foreach (var id in updateIds ?? new List<Guid>())
                {
                    var existingEntity = entities.FirstOrDefault(c => getExistingId(c) == id);

                    if (existingEntity is not null) continue;
                    // Insert entity
                    var newEntity = getNewEntity(id);
                    await _context.Set<T>().AddAsync(newEntity, cancellationToken);
                    addedList.Add(newEntity);
                }
            }

            if (callback is not null)
            {
                await callback(removedList, addedList);
            }
        }

        private async Task UpdateWebsites(
            Guid advertisingId,
            List<AdvertisingSetWebsite>? websites,
            List<Guid>? excludedWebsiteIds,
            List<Guid>? includedWebsiteIds,
            CancellationToken cancellationToken)
        {
            // Process websites
            if (websites is not null)
            {
                // Delete websites
                foreach (var existingEntity in websites)
                {
                    var existedInExcluded = !excludedWebsiteIds?.Any(c => c == existingEntity.Id) ?? default;
                    var existedInIncluded = !includedWebsiteIds?.Any(c => c == existingEntity.Id) ?? default;

                    if (existedInExcluded && existedInIncluded)
                    {
                        _context.AdvertisingSetWebsites.Remove(existingEntity);
                    }
                }

                // Update and Insert age
                foreach (var id in excludedWebsiteIds ?? new List<Guid>())
                {
                    var existingEntity = websites.FirstOrDefault(c => c.Id == id);

                    if (existingEntity is not null)
                    {
                        existingEntity.WebsiteCondition = WebsiteCondition.Exclude;
                        _context.AdvertisingSetWebsites.Update(existingEntity);
                    }
                    else
                    {
                        // Insert age
                        var newEntity = new AdvertisingSetWebsite
                        {
                            AdvertisingSetId = advertisingId,
                            WebsiteId = id,
                            WebsiteCondition = WebsiteCondition.Exclude
                        };
                        await _context.AdvertisingSetWebsites.AddAsync(newEntity, cancellationToken);
                    }
                }

                foreach (var id in includedWebsiteIds ?? new List<Guid>())
                {
                    var existingEntity = websites.FirstOrDefault(c => c.Id == id);

                    if (existingEntity is not null)
                    {
                        existingEntity.WebsiteCondition = WebsiteCondition.Include;
                        _context.AdvertisingSetWebsites.Update(existingEntity);
                    }
                    else
                    {
                        // Insert age
                        var newEntity = new AdvertisingSetWebsite
                        {
                            AdvertisingSetId = advertisingId,
                            WebsiteId = id,
                            WebsiteCondition = WebsiteCondition.Include
                        };
                        await _context.AdvertisingSetWebsites.AddAsync(newEntity, cancellationToken);
                    }
                }
            }
        }

        private async Task UpdateAdvertisingSetChildrenCache<TCore, TEntity>(AdvertisingSet advertisingSet)
            where TCore : NovanetDocument 
            where TEntity : TCore
        {
            var param = Expression.Parameter(typeof(TEntity), "e");
            var propExpression = Expression.Property(param, "AdvertisingSetId");

            var filterLambda = Expression.Lambda<Func<TEntity, bool>>(
                Expression.Equal(
                    propExpression,
                    Expression.Convert(Expression.Constant(advertisingSet.Id), propExpression.Type)
                ),
                param
            );

            var updatedItems = await _context.Set<TEntity>()
                .Where(filterLambda)
                .ToListAsync();

            var cachedItems = CacheManager.Set<TCore>(Logger)?.Values
                .Where(x => (Guid) x.GetType().GetProperty("AdvertisingSetId")!.GetValue(x, null)! == advertisingSet.Id)
                .ToList() ?? new List<TCore>();

            foreach (var item in cachedItems)
            {
                if (updatedItems.Any(x => x.Id == item.Id)) continue;
                await _globalCacheService.KeyDeleteAsync(
                    RedisKeys.KeySettings<TCore>(item.SubId));
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<TCore>(),
                    new PatchCacheManagerSignal<TCore>
                    {
                        Entity = null,
                        EntitySubId = item.SubId
                    });
            }

            foreach (var item in updatedItems)
            {
                await _globalCacheService.SetAsync(
                    RedisKeys.KeySettings<TCore>(item.SubId), item);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<TCore>(),
                    new PatchCacheManagerSignal<TCore>
                    {
                        Entity = item,
                        EntitySubId = item.SubId
                    });
            }
        }
    }
}