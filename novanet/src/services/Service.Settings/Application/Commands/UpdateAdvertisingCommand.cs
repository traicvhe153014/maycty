﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using System.Linq.Expressions;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateAdvertisingCommand : INovanetRequest<Advertising>
{
    public Guid Id { get; set; }
    public string? Name { get; set; }
    public string? RedirectUrl { get; set; }

    public string? PostContent { get; set; }

    public string? BannerImage { get; set; }

    public string? BackgroundImageUrl { get; set; }

    public string? AvatarName { get; set; }

    public string? AvatarUrl { get; set; }

    public bool? ShowFreeDelivery { get; set; }

    public bool? ShowDiscountedPrice { get; set; }

    public bool? ProductName { get; set; }

    public bool? UseBannerTemplate { get; set; }

    public string? ExpandedContentUrl { get; set; }

    public bool? IsActive { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Logo { get; set; }
    public string? ShortcutNotification { get; set; }
    public string? ExtendNotification { get; set; }
    public bool? IsCta { get; set; }
    public string? Cta { get; set; }
    public string? CtaUrl { get; set; }
    public bool? IsCustomImage { get; set; }
    public string[]? Images { get; set; }
    public bool? IsDelay { get; set; }
    public int? Delay { get; set; }
    public string? CollapseBanner { get; set; }
    public string? ExtendBanner { get; set; }
    public string? Video { get; set; }
    public string? ThumbnailVideo { get; set; }

    public bool IsFullUpdate { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateAdvertisingCommand, Advertising>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(
            logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<Advertising> HandleAsync(UpdateAdvertisingCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var advertising = await _context.Advertising
                .Include(x => x.AdvertisingSet)
                !.ThenInclude(x => x.Campaign)
                .Include(x => x.AdvertisingAppliedTemplates)!
                .ThenInclude(x => x.AdvertisingTemplateAttributeValues)!
                .ThenInclude(x => x.AdvertisingTemplateAttribute)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (advertising == null)
            {
                throw new BadRequestException("Tin quảng cáo không tồn tại");
            }

            if (request.IsFullUpdate)
            {
                advertising.RedirectLink = request.RedirectUrl;
                foreach (var advertisingTemplateAttributeValue in advertising.AdvertisingAppliedTemplates[0]
                             .AdvertisingTemplateAttributeValues)
                {
                    switch (advertisingTemplateAttributeValue.AdvertisingTemplateAttribute?.Name)
                    {
                        case "avatarUrl":
                            advertisingTemplateAttributeValue.StringValue = request.AvatarUrl;
                            break;
                        case "showFreeDelivery":
                            advertisingTemplateAttributeValue.BooleanValue = request.ShowFreeDelivery;
                            break;
                        case "backgroundImageUrl":
                            advertisingTemplateAttributeValue.StringValue = request.BackgroundImageUrl;
                            break;
                        case "avatarName":
                            advertisingTemplateAttributeValue.StringValue = request.AvatarName;
                            break;
                        case "bannerImage":
                            advertisingTemplateAttributeValue.StringValue = request.BannerImage;
                            break;
                        case "productName":
                            advertisingTemplateAttributeValue.BooleanValue = request.ProductName;
                            break;
                        case "showDiscountedPrice":
                            advertisingTemplateAttributeValue.BooleanValue = request.ShowDiscountedPrice;
                            break;
                        case "postContent":
                            advertisingTemplateAttributeValue.StringValue = request.PostContent;
                            break;
                        case "useBannerTemplate":
                            advertisingTemplateAttributeValue.BooleanValue = request.UseBannerTemplate;
                            break;
                        case "expandedContentUrl":
                            advertisingTemplateAttributeValue.StringValue = request.ExpandedContentUrl;
                            break;
                        case "title":
                            advertisingTemplateAttributeValue.StringValue = request.Title;
                            break;
                        case "description":
                            advertisingTemplateAttributeValue.StringValue = request.Description;
                            break;
                        case "logo":
                            advertisingTemplateAttributeValue.StringValue = request.Logo;
                            break;
                        case "shortcutNotification":
                            advertisingTemplateAttributeValue.StringValue = request.ShortcutNotification;
                            break;
                        case "extendNotification":
                            advertisingTemplateAttributeValue.StringValue = request.ExtendNotification;
                            break;
                        case "isCta":
                            advertisingTemplateAttributeValue.BooleanValue = request.IsCta;
                            break;
                        case "cta":
                            advertisingTemplateAttributeValue.StringValue = request.Cta;
                            break;
                        case "ctaUrl":
                            advertisingTemplateAttributeValue.StringValue = request.CtaUrl;
                            break;
                        case "isCustomImage":
                            advertisingTemplateAttributeValue.BooleanValue = request.IsCustomImage;
                            break;
                        case "images":
                            advertisingTemplateAttributeValue.StringValue = request.Images?.Serialize();
                            break;
                        case "isDelay":
                            advertisingTemplateAttributeValue.BooleanValue = request.IsDelay;
                            break;
                        case "delay":
                            advertisingTemplateAttributeValue.NumberValue = request.Delay;
                            break;
                        case "collapseBanner":
                            advertisingTemplateAttributeValue.StringValue = request.CollapseBanner;
                            break;
                        case "extendBanner":
                            advertisingTemplateAttributeValue.StringValue = request.ExtendBanner;
                            break;
                        case "video":
                            advertisingTemplateAttributeValue.StringValue = request.Video;
                            break;
                        case "thumbnailVideo":
                            advertisingTemplateAttributeValue.StringValue = request.ThumbnailVideo;
                            break;
                    }
                }
            }

            advertising.Name = request.Name ?? advertising.Name;
            advertising.ModifiedAt = DateTimeOffset.Now;

            if (request.IsActive is not null)
            {
                var totalAdvertisingSet = CacheManager.AdvertisingSets.Values
                    .Count(x =>
                        x.IsActive.Equals(true) &&
                        x.CampaignId.Equals(advertising.AdvertisingSet.CampaignId) &&
                        x.Status.Equals(AdvertisingSetStatus.Running)
                    );

                advertising.IsActive = request.IsActive.Value;
                if (advertising.Status != AdvertisingStatus.Finished && advertising.Status != AdvertisingStatus.Error)
                {
                    if (advertising.AdvertisingSet.Status.Equals(AdvertisingSetStatus.Error))
                    {
                        advertising.Status = AdvertisingStatus.Error;
                    }
                    else
                    {
                        advertising.Status =
                            request.IsActive.Value ? AdvertisingStatus.Running : AdvertisingStatus.Paused;
                    }
                }

                advertising.ModifiedBy = currentUserId;

                var currentAdvertisingSetStatus = advertising!.AdvertisingSet!.Status;

                // Count total Advertising available to check status of campaign
                Expression<Func<Advertising, bool>> whereIsAdminExpression = x => true;
                if (!UserClaimsValue.IsAdmin)
                {
                    whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
                }

                var totalAdvertising = await _context.Advertising
                    .Where(whereIsAdminExpression)
                    .Where(x => x.IsActive.Equals(true) && x.AdvertisingSetId.Equals(advertising!.AdvertisingSetId) &&
                                x.Status.Equals(AdvertisingStatus.Running))
                    .CountAsync(cancellationToken);
                if (totalAdvertising == 1 && request.IsActive.Equals(false) &&
                    advertising!.AdvertisingSet!.Status != AdvertisingSetStatus.Error)
                {
                    advertising!.AdvertisingSet!.Status = AdvertisingSetStatus.Paused;
                }

                else if (advertising!.AdvertisingSet!.Status.Equals(AdvertisingSetStatus.Paused) &&
                         advertising!.AdvertisingSet!.IsActive.Equals(true) &&
                         request.IsActive.Equals(true))
                {
                    if (advertising.Status.Equals(AdvertisingStatus.Running) || !totalAdvertising.Equals(0))
                    {
                        advertising.AdvertisingSet.Status = AdvertisingSetStatus.Running;
                    }
                    else
                    {
                        advertising!.AdvertisingSet!.Status = AdvertisingSetStatus.Paused;
                    }
                }

                if (!advertising!.AdvertisingSet!.Campaign!.Status.Equals(CampaignStatus.Error) &&
                    !advertising!.AdvertisingSet!.Campaign!.Status.Equals(CampaignStatus.Finished) &&
                    advertising!.AdvertisingSet!.Campaign!.IsActive.Equals(true))
                {
                    DateOnly? campaignEndDate = advertising.AdvertisingSet.Campaign.EcommerceEndDate is not null
                        ? DateOnly.FromDateTime(advertising.AdvertisingSet.Campaign.EcommerceEndDate!.Value.DateTime)
                        : null;
                    var campaignStartDate =
                        DateOnly.FromDateTime(advertising.AdvertisingSet.Campaign.EcommerceStartDate.DateTime);
                    var today = DateOnly.FromDateTime(DateTime.Now);

                    if (!advertising!.AdvertisingSet!.Status.Equals(currentAdvertisingSetStatus))
                    {
                        if (advertising!.AdvertisingSet!.Status!.Equals(AdvertisingSetStatus.Running))
                        {
                            totalAdvertisingSet += 1;
                        }
                        else if (totalAdvertisingSet > 0)
                        {
                            totalAdvertisingSet -= 1;
                        }
                    }

                    if (campaignStartDate > today)
                    {
                        advertising!.AdvertisingSet!.Campaign!.Status = CampaignStatus.NotStarted;
                    }
                    else
                    {
                        if (advertising!.AdvertisingSet!.Status!.Equals(AdvertisingSetStatus.Paused) &&
                            !request.IsActive.Equals(true) && totalAdvertisingSet.Equals(0))
                        {
                            advertising!.AdvertisingSet!.Campaign!.Status = CampaignStatus.Paused;
                        }

                        else if (request.IsActive.Equals(true) && totalAdvertisingSet > 0 &&
                                 campaignStartDate <= today &&
                                 (campaignEndDate >= today ||
                                  advertising!.AdvertisingSet!.Campaign?.EcommerceEndDate is null) &&
                                 !advertising!.AdvertisingSet!.Campaign!.Status.Equals(CampaignStatus.Unfinished))
                        {
                            advertising!.AdvertisingSet!.Campaign!.Status = CampaignStatus.Running;
                        }
                    }
                }
            }

            _context.Advertising.Update(advertising);
            await _context.SaveChangesAsync(cancellationToken);

            // update cache
            var updatedAdvertising = await _context.Advertising
                .AsNoTracking()
                .Include(x => x.AdvertisingAppliedTemplates)
                !.ThenInclude(x => x.AdvertisingTemplateAttributeValues)
                .AsSplitQuery()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (updatedAdvertising is null) return advertising;
            {
                updatedAdvertising.AdvertisingAppliedTemplateCores = updatedAdvertising.AdvertisingAppliedTemplates
                    ?.Select(x => new AdvertisingAppliedTemplateCore
                    {
                        AdvertisingId = x.AdvertisingId,
                        Id = x.Id,
                        SubId = x.SubId,
                        AdvertisingTemplateId = x.AdvertisingTemplateId,
                        AdvertisingTemplateAttributeValueCores = x.AdvertisingTemplateAttributeValues
                            ?.Cast<AdvertisingTemplateAttributeValueCore>().ToList() ?? default!
                    }).ToList() ?? default!;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingCore>(updatedAdvertising.SubId),
                    updatedAdvertising);

                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingCore>(),
                    new PatchCacheManagerSignal<AdvertisingCore>
                    {
                        Entity = updatedAdvertising,
                        EntitySubId = updatedAdvertising.SubId,
                    });
            }

            var updatedAdvertisingSet = CacheManager.AdvertisingSets.Values
                .FirstOrDefault(x =>
                    x.Id.Equals(advertising.AdvertisingSetId)
                );
            if (updatedAdvertisingSet is null) return advertising;
            if (advertising.AdvertisingSet != null) updatedAdvertisingSet.Status = advertising.AdvertisingSet.Status;
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetCore>(updatedAdvertisingSet.SubId),
                updatedAdvertisingSet);

            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(),
                new PatchCacheManagerSignal<AdvertisingSetCore>
                {
                    Entity = updatedAdvertisingSet,
                    EntitySubId = updatedAdvertisingSet.SubId
                });

            return advertising;
        }
    }
}