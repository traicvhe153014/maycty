﻿using System.ComponentModel;
using System.Linq.Expressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessServices;

namespace Service.Settings.Application.Commands;

public class ExportProductToExcelCommand : INovanetRequest<FileInformation>
{
    public AdvertisingStatus? Status { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public List<Guid?>? ProductIds { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<ExportProductToExcelCommand, FileInformation>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;
        private readonly IExportDataToExcelFile<Dictionary<string, object>> _exportDataToExcelFile;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IReportService reportService,
            IExportDataToExcelFile<Dictionary<string, object>> exportDataToExcelFile,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _exportDataToExcelFile = exportDataToExcelFile;
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<FileInformation> HandleAsync(ExportProductToExcelCommand request,
            CancellationToken cancellationToken)
        {
            Expression<Func<ProductEntity, bool>> whereExpression = x => true;
            if (request.ProductIds != null)
            {
                whereExpression = whereExpression.And(x => request.ProductIds.Contains(x.Id));
            }
            
            var hasDateRange = HasDateRange(request);

            var campaigns = new List<Campaign>();

            var productDataList = new List<ExportProductData>();
            
            var exportDataDictionary = new List<Dictionary<string, object>>();

            var products = await _context.ProductEntities
                .Include(x => x.ProductFeed).ThenInclude(x => x.Campaigns)
                .Include(x => x.AttributeValues)!
                .ThenInclude(x => x.ProductAttribute)
                .Where(whereExpression)
                .Select(x => new ProductEntitySortable
                {
                    ProductEntity = x,
                    Title = x.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "Title")!.StringValue,
                    Condition = x.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "Condition")!
                        .StringValue,
                    ProductType = x.AttributeValues!.Any(y => y.ProductAttribute.Name == "ProductType")
                        ? x.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "ProductType")!
                            .StringValue
                        : ""
                })
                .Select(x => x.ProductEntity)
                .ToListAsync(cancellationToken);

            foreach (var productEntity in products)
            {
                foreach (var productFeedCampaign in productEntity.ProductFeed.Campaigns)
                {
                    var exist = campaigns.Find(x => x.Id.Equals(productFeedCampaign.Id));

                    if (exist is null)
                    {
                        campaigns.Add(productFeedCampaign);
                    }
                }
            }
            var reportData = await FillProductData(request, products, campaigns);
            
            var firstAdvertisingDate = campaigns.Min(x => x?.EcommerceStartDate) ?? DateTimeOffset.Now;

            
            foreach (var campaign in campaigns)
            {
                foreach (var product in products)
                {
                    var existCampaign = product.ProductFeed.Campaigns.FirstOrDefault(x => x.Id.Equals(campaign.Id));
                    if (existCampaign is null) continue;
                    reportData.TryGetValue(product.Id.ToString() + campaign.Id, out var dataItem);
                    if (dataItem is null) continue;

                    var item = new ExportProductData
                    {
                        CampaignId = campaign.Id,
                        CampaignName = campaign.Name,
                        ProductId = product.Id,
                        ProductName = product.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "Title")!
                            .StringValue,
                        ProductCondition =
                            product.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "Condition")!
                                .StringValue,
                        ProductType = product.AttributeValues!.Any(y => y.ProductAttribute.Name == "ProductType")
                            ? product.AttributeValues!.FirstOrDefault(y => y.ProductAttribute.Name == "ProductType")!
                                .StringValue
                            : "",
                        Modified = product.ModifiedAt is null ? "-" : DateOnly.FromDateTime(product.ModifiedAt!.Value.DateTime).ToString("dd/MM/yyyy"),
                        ProductFeed = product.ProductFeed.Name,
                        Clicks = dataItem["clicks"] as double? ?? 0,
                        Impressions = dataItem["impressions"] as double? ?? 0,
                        CTR = dataItem["ctr"] as double? ?? 0,
                        Purchases = dataItem["purchase"] as double? ?? 0,
                        PurchaseConversion = dataItem["purchaseConversion"] as double? ?? 0
                    };
                    productDataList.Add(item);
                }
            }

            productDataList = productDataList.OrderBy(x => x.CampaignId).ToList();

            var totalValue = new ExportProductData
            {
                CampaignName = "Tổng",
                Impressions = productDataList.Sum(x => x.Impressions),
                Clicks = productDataList.Sum(x => x.Clicks),
                Purchases = productDataList.Sum(x => x.Purchases),
                CTR = productDataList.Sum(z => z.Impressions) != 0
                    ? productDataList.Sum(z => z.Clicks) / productDataList.Sum(z => z.Impressions) * 100
                    : 0,
                PurchaseConversion = productDataList.Sum(z => z.Impressions) != 0
                    ? productDataList.Sum(z => z.Purchases) / productDataList.Sum(z => z.Impressions) * 100
                    : 0,
            };
            
            productDataList.Add(totalValue);
            
            productDataList.ForEach(x =>
            {
                var dataDictionary = x.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(prop =>
                        prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                        prop.GetValue(x, null)! is not Guid)
                    .ToDictionary(prop =>
                        {
                            var attrs = prop.GetCustomAttributes(true);

                            foreach (var attr in attrs)
                            {
                                if (attr is DescriptionAttribute
                                    attribute) // check if attr implements interface (you may have to reflect to get this)
                                {
                                    return attribute?.Description;
                                }
                            }

                            return string.Empty;
                        },
                        prop => prop.GetValue(x, null)! is double
                            ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                            : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" : prop.GetValue(x, null)!);
                exportDataDictionary.Add(dataDictionary!);
            });

            var exportData = new List<ExportDataResponse<List<Dictionary<string, object>>>>
            {
                new()
                {
                    SheetName = "Báo cáo theo sản phẩm",
                    Data = exportDataDictionary
                }
            };
            
            var streamAdvertisingData = await _exportDataToExcelFile.ExportFile(exportData);
            var returnFile = new FileInformation
            {
                FileName = hasDateRange
                    ? $"Báo cáo theo sản phẩm ({request.StartDate!.Value.DateTime:dd-MM-yyyy} đến {request.EndDate!.Value.DateTime:dd-MM-yyyy}).xlsx"
                    : $"Báo cáo theo sản phẩm ({firstAdvertisingDate:dd-MM-yyyy} đến {DateTimeOffset.Now:dd-MM-yyyy})",
                Content = streamAdvertisingData
            };
            
            return returnFile;
        }

        private async Task<Dictionary<string, Dictionary<string, double>>> GetReportData(
            ExportProductToExcelCommand request,
            ReportMetric metric, List<object> productSubIds, List<object> campaignSubIds)
        {
            var now = DateTimeOffset.UtcNow;
            var hasDateRange = HasDateRange(request);

            var objectIds = new List<List<object>>
            {
                campaignSubIds,
                productSubIds
            };
            var dimensions = new List<ReportAdvertiserDimension>
            {
                ReportAdvertiserDimension.Campaign,
                ReportAdvertiserDimension.Product
            };
            var reports = hasDateRange
                ? await _reportService.GetMultipleAdvertiserReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    metric,
                    dimensions,
                    objectIds)
                : await _reportService.GetTotalMultipleAdvertiserReport(
                    metric,
                    dimensions,
                    objectIds);

            var returnReports = new Dictionary<string, Dictionary<string, double>>();

            foreach (var (key, d) in reports)
            {
                if (returnReports.ContainsKey(key.Split("_").FirstOrDefault()!))
                {
                    returnReports[key.Split("_").FirstOrDefault()!][key.Split("_").LastOrDefault()!] = d;
                }
                else
                {
                    returnReports[key.Split("_").FirstOrDefault()!] = new Dictionary<string, double>
                    {
                        {
                            key.Split("_").LastOrDefault()!,
                            d
                        }
                    };
                }
            }

            return returnReports;
        }

        private async Task<Dictionary<string, Dictionary<string, object?>>> FillProductData(
            ExportProductToExcelCommand request, List<ProductEntity> products, List<Campaign> campaigns)
        {
            var productSubIds = products.Select(x => (object) x.SubId).ToList();

            var campaignIds = campaigns.Select(x => (object) x.SubId).ToList();

            var totalClickMetrics =
                await GetReportData(request, ReportMetric.Click, productSubIds, campaignIds);

            var totalViewMetrics =
                await GetReportData(request, ReportMetric.View, productSubIds, campaignIds);

            var totalPaidMetrics =
                await GetReportData(request, ReportMetric.Paid, productSubIds, campaignIds);

            var totalConversionMetrics =
                await GetReportData(request, ReportMetric.Conversion, productSubIds, campaignIds);
            var result = new Dictionary<string, Dictionary<string, object?>>();

            foreach (var campaign in campaigns)
            {
                foreach (var product in products)
                {
                    var totalClicks = totalClickMetrics[campaign.SubId.ToString()]
                        .GetValueOrDefault(product.SubId.ToString());
                    var totalImpressions = totalViewMetrics[campaign.SubId.ToString()]
                        .GetValueOrDefault(product.SubId.ToString());
                    var totalCost = totalPaidMetrics[campaign.SubId.ToString()]
                        .GetValueOrDefault(product.SubId.ToString());
                    var totalConversion = totalConversionMetrics[campaign.SubId.ToString()]
                        .GetValueOrDefault(product.SubId.ToString());
                    var totalCtr = totalImpressions != 0 ? totalClicks / totalImpressions * 100 : default;
                    var totalCpc = totalClicks != 0 ? totalCost / totalClicks : default;
                    var totalPurchase = totalConversionMetrics[campaign.SubId.ToString()]
                        .GetValueOrDefault(product.SubId.ToString());
                    var totalPurchaseConversion =
                        totalImpressions != 0 ? totalPurchase / totalImpressions * 100 : default;

                    double totalCps = default;
                    var itemData = new Dictionary<string, object?>
                    {
                        {"totalCost", totalCost},
                        {"clicks", totalClicks},
                        {"impressions", totalImpressions},
                        {"ctr", totalCtr},
                        {"cpc", totalCpc},
                        {"purchase", totalConversion},
                        {"purchaseConversion", totalPurchaseConversion},
                        {"cps", totalCps}
                    };
                    result[product.Id + campaign.Id.ToString()] = itemData;
                }
            }

            return result;
        }

        private static bool HasDateRange(ExportProductToExcelCommand query)
        {
            return query.StartDate is not null && query.EndDate is not null;
        }
    }
}