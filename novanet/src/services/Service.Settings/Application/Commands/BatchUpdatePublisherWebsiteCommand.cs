﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessObjects;

namespace Service.Settings.Application.Commands;

public class BatchUpdatePublisherWebsiteCommand: INovanetRequest<List<Website>>
{
    public Dictionary<Guid, Guid> Data { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<BatchUpdatePublisherWebsiteCommand, List<Website>>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<NovanetRequestHandler<BatchUpdatePublisherWebsiteCommand, List<Website>>> logger,
            IHttpContextAccessor httpContextAccessor, SettingsContext context, IGlobalCacheService globalCacheService,
            IMessageBusClient messageBusClient) : base(logger, httpContextAccessor) 
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<List<Website>> HandleAsync(BatchUpdatePublisherWebsiteCommand request, CancellationToken cancellationToken)
        {
            var websiteIds = request.Data.Keys.ToList();
            var websites = await _context.Websites.Where(x => websiteIds.Contains(x.Id)).ToListAsync(cancellationToken);
            var websiteSignals = new List<SyncWebsitePublisherReportSignal>();
            foreach (var website in websites)
            {
                request.Data.TryGetValue(website.Id, out var newPublisherId);
                if (newPublisherId == Guid.Empty)
                {
                    continue;
                }
                var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == newPublisherId);
                if (publisher?.Roles is null || !publisher.Roles.Contains("Publisher"))
                {
                    continue;
                }
                websiteSignals.Add(new SyncWebsitePublisherReportSignal
                {
                    NewPublisherId = newPublisherId,
                    OldPublisherId = website.PublisherId,
                    WebsiteSubId = website.SubId
                });
                website.PublisherId = newPublisherId;
                
                CacheManager.Websites.TryGetValue(website.SubId, out var cachedWebsite);
                if (cachedWebsite is not null)
                {
                    cachedWebsite.PublisherId = website.PublisherId;
                    await _globalCacheService.SetAsync(RedisKeys.KeySettings<WebsiteCore>(website.SubId), cachedWebsite);
                    _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsiteCore>(), new PatchCacheManagerSignal<WebsiteCore>
                    {
                        Entity = cachedWebsite,
                        EntitySubId = website.SubId,
                    });
                }
            }
            _context.Websites.UpdateRange(websites);
            await _context.SaveChangesAsync(cancellationToken);

            _messageBusClient.Publish(MessageBusChannels.BatchSyncWebsitePublisherReport, websiteSignals);
            return websites;
        }
    }
}
