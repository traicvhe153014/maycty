﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessObjects;

namespace Service.Settings.Application.Commands;

public class UpdateWebsiteCommand : INovanetRequest<Website>
{
    public Guid Id { get; set; }
    
    public Guid? PublisherId { get; set; }
    
    public bool? IsActive { get; set; }
    
    public string? Domain { get; set; }
    internal class Handler : NovanetRequestHandler<UpdateWebsiteCommand, Website>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<NovanetRequestHandler<UpdateWebsiteCommand, Website>> logger,
            IHttpContextAccessor httpContextAccessor, SettingsContext context, IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _messageBusClient = messageBusClient;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<Website> HandleAsync(UpdateWebsiteCommand request, CancellationToken cancellationToken)
        {
            var website = await _context.Websites.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (website is null)
            {
                throw new BadRequestException("Không tìm thấy website");
            }
            var isAdmin = UserClaimsValue.Roles.Contains("Admin") || UserClaimsValue.Roles.Contains("PublisherManager");
            if (!isAdmin && website.PublisherId != UserClaimsValue.Id)
            {
                throw new BadRequestException("Không có quyền chỉnh sửa website");
            }
            if (request.PublisherId is not null && isAdmin)
            {
                _messageBusClient.Publish(MessageBusChannels.SyncWebsitePublisherReport, new SyncWebsitePublisherReportSignal
                {
                    WebsiteSubId = website.SubId,
                    OldPublisherId = website.PublisherId,
                    NewPublisherId = request.PublisherId.Value
                });
                website.PublisherId = request.PublisherId.Value;
            }

            if (request.IsActive is not null)
            {
                website.Active = request.IsActive.Value;
                if (website.Active)
                {
                    website.Status = WebsiteStatus.Running;
                }
                else
                {
                    website.Status = WebsiteStatus.Disabled;
                }
            }

            if (request.Domain is not null)
            {
                var web = await _context.Websites.FirstOrDefaultAsync(x=> x.Domain == request.Domain && x.PublisherId == website.PublisherId, cancellationToken);
                if (web is not null && web.Id != website.Id)
                {
                    throw new BadRequestException("Domain đã tồn tại");
                }
                else
                {
                    website.Domain = request.Domain;
                }
            }
            website.ModifiedAt = DateTimeOffset.Now;

            _context.Websites.Update(website);
            await _context.SaveChangesAsync(cancellationToken);
            
            CacheManager.Websites.TryGetValue(website.SubId, out var cachedWebsite);
            if (cachedWebsite is not null)
            {
                if (isAdmin)
                {
                    cachedWebsite.PublisherId = website.PublisherId;
                }
                cachedWebsite.Active = website.Active;
                cachedWebsite.Status = website.Status;
                cachedWebsite.Domain = website.Domain;
                cachedWebsite.ModifiedAt = website.ModifiedAt;

                await _globalCacheService.SetAsync(RedisKeys.KeySettings<WebsiteCore>(website.SubId), cachedWebsite);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsiteCore>(), new PatchCacheManagerSignal<WebsiteCore>
                {
                    Entity = cachedWebsite,
                    EntitySubId = website.SubId,
                });
            }
            return website;
        }
    }
}