using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class CreateProductGroupCommand : INovanetRequest<CreateProductGroupResponse>, IMapFrom<ProductGroup>
{
    public string Name { get; set; } = default!;

    public int Amount { get; set; } = default!;

    public Guid ProductFeedId { get; set; } = default!;

    public Guid ProductAttributeId { get; set; } = default!;

    public List<string> ProductAttributeHashKeyValues { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<CreateProductGroupCommand, CreateProductGroupResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor, IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _messageBusClient = messageBusClient;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<CreateProductGroupResponse> HandleAsync(CreateProductGroupCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var productGroup = new ProductGroup
            {
                Name = request.Name,
                Amount = request.Amount,
                ProductFeedId = request.ProductFeedId,
                ProductAttributeId = request.ProductAttributeId,
                ModifiedAt = DateTimeOffset.Now,
                CreatedAt = DateTimeOffset.Now,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
            };

            var entityEntry = await _context.AddAsync(productGroup, cancellationToken);
            var productGroupEntityEntries = new List<ProductGroupEntity>();

            foreach (var requestProductAttributeHashKeyValue in request.ProductAttributeHashKeyValues)
            {
                var attributeValues = await _context.ProductAttributeValues
                    .Where(x => x.ProductEntity.ProductFeed.Id.Equals(request.ProductFeedId))
                    .Where(x => x.AttributeId.Equals(request.ProductAttributeId))
                    .Where(x => x.ProductEntity.IsValid && !x.ProductEntity.Deleted)
                    .Where(x => x.HashKey != null && x.HashKey.Equals(requestProductAttributeHashKeyValue))
                    .ToListAsync(cancellationToken: cancellationToken);
                
                var productIds = attributeValues
                    .Select(x => x.ProductId)
                    .Distinct()
                    .ToList();

                var anyInvalidProduct = await _context.ProductEntities
                    .AnyAsync(x => productIds.Contains(x.Id) && !x.IsValid, cancellationToken);
                if (anyInvalidProduct)
                {
                    throw new BadRequestException("Không thể tạo nhóm sản phẩm với sản phẩm lỗi dữ liệu");
                }
            
                var productGroupEntities = attributeValues.Select(productAttributeValueId => new ProductGroupEntity
                {
                    ProductGroupId = productGroup.Id,
                    ProductId = productAttributeValueId.ProductId,
                    ProductAttributeValueId = productAttributeValueId.Id,
                    ProductAttributeValueHashCode = requestProductAttributeHashKeyValue,
                    CreatedBy = currentUserId
                });
            
                foreach (var productGroupEntity in productGroupEntities)
                {
                    var productGroupEntityEntry = await _context.AddAsync(productGroupEntity, cancellationToken);
                    productGroupEntityEntries.Add(productGroupEntityEntry.Entity);
                }
            }

            await _context.SaveChangesAsync(cancellationToken);
            foreach (var productGroupEntityEntry in productGroupEntityEntries)
            {
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductGroupEntityCore>(productGroupEntityEntry.SubId), productGroupEntityEntry);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductGroupEntityCore>(), new PatchCacheManagerSignal<ProductGroupEntityCore>
                {
                    Entity = productGroupEntityEntry,
                    EntitySubId = productGroupEntityEntry.SubId
                });
            }

            return new CreateProductGroupResponse(entityEntry.Entity.Id, entityEntry.Entity.Name);
        }
    }
}