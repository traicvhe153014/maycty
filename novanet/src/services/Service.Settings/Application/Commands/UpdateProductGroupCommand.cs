﻿using Novanet.Core.Exceptions;

namespace Service.Settings.Application.Commands;

public class UpdateProductGroupCommand : INovanetRequest<UpdateProductGroupResponse>, IMapFrom<ProductGroup>
{
    public Guid Id { get; set; } = default!;

    public string Name { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<UpdateProductGroupCommand, UpdateProductGroupResponse>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : 
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<UpdateProductGroupResponse> HandleAsync(UpdateProductGroupCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var productGroup = _context.ProductGroups
                .FirstOrDefault(x => x.Id.Equals(request.Id));

            if (productGroup == null) throw new BadRequestException("ProductGroup không tồn tại");
            productGroup.Name = request.Name;
            productGroup.ModifiedAt = DateTimeOffset.Now;
            productGroup.ModifiedBy = currentUserId;

            _context.ProductGroups.Update(productGroup);
            await _context.SaveChangesAsync(cancellationToken);

            return new UpdateProductGroupResponse(productGroup.Id, productGroup.Name);
        }
    }
}