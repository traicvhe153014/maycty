using Novanet.Core.Exceptions;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class CreateDomainAliasesCommand : INovanetRequest<DomainAlias>
{
    public String Name { get; set; }

    public Guid WebsiteId { get; set; }
    
    public Guid PublisherId { get; set; }

    internal class Handler : NovanetRequestHandler<CreateDomainAliasesCommand, DomainAlias>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(
            logger,
            httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<DomainAlias> HandleAsync(CreateDomainAliasesCommand request,
            CancellationToken cancellationToken)
        {
            var website = CacheManager.Websites.Values.FirstOrDefault(x => x.Id == request.WebsiteId);
            var domain = await _context.DomainAliases.Include(x=>x.Website).FirstOrDefaultAsync(x => 
                x.Name.ToLower().Equals(request.Name.ToLower()) && x.Website.PublisherId == request.PublisherId, cancellationToken);
            if (domain is not null)
            {
                throw new BadRequestException("Domain đã tồn tại");
            }
            if (website is not null)
            {
                var currentUserId = UserClaimsValue.Id;
                var domainAlias = await _context.DomainAliases
                    .AddAsync(new DomainAlias
                    {
                        Name = request.Name,
                        WebsiteId = request.WebsiteId,
                        CreatedBy = currentUserId,
                        ModifiedBy = currentUserId,
                    }, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
                return domainAlias.Entity;
            }
            throw new BadRequestException("error");
        }
    }
}