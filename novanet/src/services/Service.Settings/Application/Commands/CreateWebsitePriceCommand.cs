﻿using Novanet.Core.Exceptions;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class CreateWebsitePriceCommand : INovanetRequest<WebsitePrice>
{
    public Guid WebsiteId { get; set; }

    public PublisherPriceType PublisherPriceType { get; set; }

    public Guid? ZoneId { get; set; }

    public DateTimeOffset StartDate { get; set; }

    public List<TemplatePrice> TemplatePrices { get; set; } = default!;
    
    internal class Handler : NovanetRequestHandler<CreateWebsitePriceCommand, WebsitePrice>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<WebsitePrice> HandleAsync(CreateWebsitePriceCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var existingWebsitePrice = await _context.WebsitePrices
                .AnyAsync(x =>
                        x.PublisherPriceType == request.PublisherPriceType &&
                        (request.PublisherPriceType == PublisherPriceType.Zone
                            ? x.ZoneId == request.ZoneId
                            : x.WebsiteId == request.WebsiteId) &&
                              x.StartDate.Date == request.StartDate.Date &&
                              x.StartDate.Hour == request.StartDate.Hour &&
                              x.StartDate.Minute == request.StartDate.Minute,
                    cancellationToken);
            if (existingWebsitePrice)
            {
                throw new BadRequestException("Thời gian áp dụng bảng giá đã tồn tại");
            }

            var templates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);

            request.StartDate = new DateTimeOffset(request.StartDate.Year, request.StartDate.Month,
                request.StartDate.Day, request.StartDate.Hour, request.StartDate.Minute, 0, request.StartDate.Offset);
            var websitePrice = new WebsitePrice
            {
                WebsiteId = request.WebsiteId,
                SellPrice = 0,
                SellPriceType = 0,
                BuyPrice = 0,
                BuyPriceType = 0,
                TemplateType = 0,
                StartDate = request.StartDate,
                Timestamp = DateTimeOffset.Now,
                CreatedBy = currentUserId,
                CreatedAt = DateTimeOffset.Now,
                ModifiedAt = DateTimeOffset.Now,
                IsActive = true,
                PublisherPriceType = request.PublisherPriceType,
                WebsiteTemplatePrices = new List<WebsiteTemplatePrice>()
            };
            if (request.PublisherPriceType == PublisherPriceType.Zone)
            {
                websitePrice.ZoneId = request.ZoneId;
            }
            websitePrice.Status = websitePrice.StartDate > DateTimeOffset.Now
                ? WebsitePriceStatus.NotStarted
                : WebsitePriceStatus.Running;

            foreach (var templatePrice in request.TemplatePrices)
            {
                var template = templates.FirstOrDefault(x => x.TemplateType == templatePrice.TemplateType);
                var templateId = template?.Id;
                var websiteTemplatePrice = new WebsiteTemplatePrice
                {
                    SellPrice = templatePrice.SellPrice,
                    SellPriceType = templatePrice.SellPriceType,
                    BuyPrice = templatePrice.BuyPrice,
                    BuyPriceType = templatePrice.BuyPriceType,
                    TemplateType = templatePrice.TemplateType,
                    CreatedBy = currentUserId,
                    AdvertisingTemplateId = templateId,
                    WebsitePriceId = websitePrice.Id
                };
                if (request.PublisherPriceType == PublisherPriceType.Zone)
                {
                    var zoneTemplate = await _context.ZoneTemplates.FirstOrDefaultAsync(
                        x => x.ZoneId == request.ZoneId && x.AdvertisingTemplateId == templateId, cancellationToken);
                    websiteTemplatePrice.ZoneTemplateId = zoneTemplate?.Id;
                }

                websitePrice.WebsiteTemplatePrices.Add(websiteTemplatePrice);
            }

            var relatedWebsitePrices = await _context.WebsitePrices
                .Where(x =>
                    x.PublisherPriceType == request.PublisherPriceType &&
                    (request.PublisherPriceType == PublisherPriceType.Zone
                        ? x.ZoneId == request.ZoneId
                        : x.WebsiteId == request.WebsiteId))
                .OrderBy(x => x.StartDate)
                .ToListAsync(cancellationToken);
            var previousWebsitePrice = relatedWebsitePrices.LastOrDefault(x => x.StartDate < request.StartDate);
            var nextWebsitePrice = relatedWebsitePrices.FirstOrDefault(x => x.StartDate > request.StartDate);
            if (previousWebsitePrice is not null)
            {
                previousWebsitePrice.EndDate = request.StartDate;
                _context.WebsitePrices.Update(previousWebsitePrice);
            }
            if (nextWebsitePrice is not null)
            {
                websitePrice.EndDate = nextWebsitePrice.StartDate;
            }
            await _context.WebsitePrices.AddAsync(websitePrice, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            websitePrice.WebsiteTemplatePriceCores =
                websitePrice.WebsiteTemplatePrices?.Cast<WebsiteTemplatePriceCore>().ToList() ??
                new List<WebsiteTemplatePriceCore>();
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<WebsitePriceCore>(websitePrice.SubId), websitePrice);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsitePriceCore>(), new PatchCacheManagerSignal<WebsitePriceCore>
            {
                Entity = websitePrice,
                EntitySubId = websitePrice.SubId,
            });

            return websitePrice;
        }
    }
}