﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using ProductFeedPermission = NovanetCore.Business.BusinessDomain.Service.Settings.ProductFeedPermission;
using SourceType = NovanetCore.Business.BusinessDomain.Service.Settings.SourceType;

namespace Service.Settings.Application.Commands;

public class CreateProductFeedCommand : INovanetRequest<CreateProductFeedResponse>, IMapFrom<ProductFeed>
{
    public string Name { get; set; } = default!;

    public SourceType SourceType { get; set; } = default!;

    public string SourcePath { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<CreateProductFeedCommand, CreateProductFeedResponse>
    {
        private readonly SettingsContext _context;
        private readonly IProductFeedService _productFeedService;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor,
            IProductFeedService productFeedService,
            IServiceScopeFactory serviceScopeFactory,
            IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _productFeedService = productFeedService;
            _serviceScopeFactory = serviceScopeFactory;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<CreateProductFeedResponse> HandleAsync(CreateProductFeedCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var sheetId = request.SourcePath.Split("/").OrderByDescending(s => s.Length).First();
            var existedProductFeed =
                await _context.ProductFeeds.AnyAsync(x => x.SourcePath == sheetId && !x.Deleted && x.CreatedBy == currentUserId, cancellationToken);
            if (existedProductFeed)
            {
                throw new BadRequestException("Tạo nguồn mới không thành công do URL link tài khoản đã tồn tại");
            }

            var productFeed = new ProductFeed
            {
                Name = request.Name,
                SourceType = request.SourceType,
                Permission = ProductFeedPermission.Processing,
                ModifiedAt = DateTimeOffset.Now,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
            };
            productFeed.Update(sheetId, DateTimeOffset.Now, new List<ProductEntity>());
            var entityEntry = await _context.ProductFeeds.AddAsync(productFeed, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            await _globalCacheService.SetAsync(RedisKeys.KeySettings<ProductFeedCore>(entityEntry.Entity.SubId), entityEntry.Entity);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductFeedCore>(), new PatchCacheManagerSignal<ProductFeedCore>
            {
                Entity = entityEntry.Entity,
                EntitySubId = entityEntry.Entity.SubId
            });
            
            // TODO: add to processing queue
            _ = Task.Run(async () =>
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var productFeedService = scope.ServiceProvider.GetRequiredService<IProductFeedService>();
                await productFeedService.ProcessGoogleSheet(entityEntry.Entity.Id, currentUserId, UserClaimsValue.IsAdmin);
            }, CancellationToken.None);

            return new CreateProductFeedResponse(entityEntry.Entity.Id, entityEntry.Entity.Name);
        }
    }
}

public class Validator : AbstractValidator<CreateProductFeedCommand>
{
    public Validator()
    {
        RuleFor(t => t.Name).NotEmpty();
        RuleFor(t => t.SourceType).NotEmpty();
        RuleFor(t => t.SourcePath).NotEmpty();
    }
}