namespace Service.Settings.Application.Commands;

public class DeleteDomainAliasesCommand : INovanetRequest<string>
{
    public Guid Id { get; set; }

    internal class Handler : NovanetRequestHandler<DeleteDomainAliasesCommand, string>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<string> HandleAsync(DeleteDomainAliasesCommand request, CancellationToken cancellationToken)
        {
            var domainAlias = await _context.DomainAliases.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (domainAlias is not null)
            {
                _context.DomainAliases.Remove(domainAlias);
                await _context.SaveChangesAsync(cancellationToken);
                return "done";
            }
            return "error";
        }
    }
}