﻿using System.Text;
using Novanet.Core.Models;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class CreateZoneCommand : INovanetRequest<CreateZoneResponse>
{
    public string Name { get; set; } = default!;

    /// <summary>
    /// w375 - MobileBannerCard = 1,
    /// w375 - InteractiveBanner = 2,
    /// w375 - FlyingCarpet = 3,
    /// w375 - InReadEcommerce = 4,
    /// w375 - PostInRead = 5,
    /// w375 - Catfish = 6
    /// </summary>
    public int Width { get; set; }

    /// <summary>
    /// h180 - MobileBannerCard = 1,
    /// h810 - InteractiveBanner = 2,
    /// h810 - FlyingCarpet = 3,
    /// h600 - InReadEcommerce = 4,
    /// h600 - PostInRead = 5,
    /// h120 - Catfish = 6
    /// </summary>
    public int Height { get; set; }

    /// <summary>
    /// Ecommerce = 0
    /// Native = 1
    /// Display = 2
    /// </summary>
    public CampaignType CampaignType { get; set; }

    public bool UsingBackup { get; set; }
    
    public ZoneBackupType? BackupType { get; set; }

    public string? BackupCode { get; set; }

    public Guid WebsiteId { get; set; }

    public List<TemplateType> TemplateTypes { get; set; } = default!;
    
    public string? RedirectLink { get; set; }

    internal class Handler : NovanetRequestHandler<CreateZoneCommand, CreateZoneResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger,
            httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<CreateZoneResponse> HandleAsync(CreateZoneCommand request,
            CancellationToken cancellationToken)
        {
            var websiteId = await _context.Websites.Select(x => new {x.Id, x.SubId})
                .FirstOrDefaultAsync(x => x.Id.Equals(request.WebsiteId), cancellationToken);
            if (websiteId is null || websiteId.SubId == 0) throw new NullReferenceException("Không tìm thấy website");
            var (websiteKey, websiteValue) = CacheManager.Websites.FirstOrDefault(x => x.Key.Equals(websiteId.SubId));
            if (websiteValue is null) throw new NullReferenceException("Bộ nhớ đệm website chưa được cập nhật");
            IdentityUserValue publisher;
            try
            {
                publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == websiteValue.PublisherId) ?? throw new Exception();
            }
            catch (Exception)
            {
                throw new NullReferenceException("Website không tồn tại publisher. Liên hệ admin để kiểm tra lại");
            }
            if (publisher is null)
                throw new NullReferenceException("Website không tồn tại publisher. Liên hệ admin để kiểm tra lại");

            var currentUserId = UserClaimsValue.Id;
            var anyWebsite = await _context.Websites.AnyAsync(x => x.Id.Equals(request.WebsiteId), cancellationToken);
            if (!anyWebsite) throw new NullReferenceException("Không tìm thấy website");

            var advertisingTemplates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);
            var zoneTemplates = request.TemplateTypes.Select(x =>
            {
                var advertisingTemplateId =
                    advertisingTemplates.FirstOrDefault(y => y.TemplateType == x)?.Id;
                return new ZoneTemplate
                {
                    AdvertisingTemplateId = advertisingTemplateId,
                    CreatedBy = currentUserId
                };
            }).ToList();
            
            var zoneEntity = await _context.Zones.AddAsync(new Zone
            {
                Name = request.Name,
                Width = request.Width,
                Height = request.Height,
                CampaignType = request.CampaignType,
                UsingBackup = request.UsingBackup,
                WebsiteId = websiteId.Id,
                CreatedBy = currentUserId,
                ZoneTemplates = zoneTemplates,
                BackupType = request.BackupType,
                BackupCode = request.BackupCode,
                Code = string.Empty,
                RedirectLink = request.RedirectLink,
                CreatedAt = DateTimeOffset.Now,
                ModifiedAt = DateTimeOffset.Now
            }, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            var zone = zoneEntity.Entity;
            zone.Update(BuildEmbedCode(publisher.SubId, zone.SubId, zone.Width, zone.Height));
            _context.Zones.Update(zone);
            await _context.SaveChangesAsync(cancellationToken);
            
            zone.ZoneTemplateCores = zone.ZoneTemplates?.Cast<ZoneTemplateCore>().ToList() ?? new List<ZoneTemplateCore>();
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<ZoneCore>(zone.SubId), zone);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ZoneCore>(), new PatchCacheManagerSignal<ZoneCore>
            {
                Entity = zone,
                EntitySubId = zone.SubId,
            });
            
            return new CreateZoneResponse
            {
                Id = zoneEntity.Entity.Id,
                Name = zoneEntity.Entity.Name,
                Width = zoneEntity.Entity.Width,
                Height = zoneEntity.Entity.Height,
                CampaignType = request.CampaignType,
                UsingBackup = request.UsingBackup,
                WebsiteId = zoneEntity.Entity.WebsiteId
            };
        }

        private static string BuildEmbedCode(long pubId, long zoneId, int width, int height)
        {
            return new StringBuilder()
                .AppendLine($"<div id=\"novanet-zone-{zoneId}\"></div>")
                .AppendLine("<script>")
                .AppendLine("   window.Novanet.init({")
                .AppendLine($"      event: 'default',")
                .AppendLine($"      width: {width},")
                .AppendLine($"      height: {height},")
                .AppendLine($"      pubId: {pubId},")
                .AppendLine($"      zoneId: {zoneId}")
                .AppendLine("   });")
                .AppendLine("</script>")
                .ToString();
        }
    }
}

public class CreateZoneCommandValidator : AbstractValidator<CreateZoneCommand>
{
    public CreateZoneCommandValidator()
    {
        RuleFor(x => x.Name).NotEmpty();
        RuleFor(x => x.CampaignType).NotEmpty();
        RuleFor(x => x.WebsiteId).NotEmpty();
    }
}
