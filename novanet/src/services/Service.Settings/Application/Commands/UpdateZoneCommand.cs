﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateZoneCommand : INovanetRequest<UpdateZoneResponse>
{
    public Guid Id { get; set; }

    public string? Name { get; set; }

    public bool? UsingBackup { get; set; }
    
    public ZoneBackupType? BackupType { get; set; }
    
    public string? BackupCode { get; set; }

    public CampaignType? CampaignType { get; set; }
    
    public string? url { get; set; }

    public string? RedirectLink { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateZoneCommand, UpdateZoneResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<UpdateZoneResponse> HandleAsync(UpdateZoneCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var zone = await _context.Zones
                .FirstOrDefaultAsync(x => x.Id.Equals(request.Id), cancellationToken);
            if (zone is null) throw new NullReferenceException("Không tìm thấy thông tin vùng");
            zone.ModifiedBy = currentUserId;
            zone.ModifiedAt = DateTimeOffset.Now;
            if (request.Name is not null)
            {
                zone.Name = request.Name;
            }
            if (request.UsingBackup is not null)
            {
                zone.UsingBackup = request.UsingBackup.Value;
            }
            if (request.BackupType is not null)
            {
                zone.BackupType = request.BackupType.Value;
            }
            if (request.BackupCode is not null)
            {
                zone.BackupCode = request.BackupCode;
            }
            if (request.CampaignType is not null)
            {
                zone.CampaignType = request.CampaignType.Value;
            }
            if (request.RedirectLink is not null)
            {
                zone.RedirectLink = request.RedirectLink;
            }
            _context.Zones.Update(zone);
            await _context.SaveChangesAsync(cancellationToken);

            CacheManager.Zones.TryGetValue(zone.SubId, out var cachedZone);
            if (cachedZone is not null)
            {
                cachedZone.Name = zone.Name;
                cachedZone.UsingBackup = zone.UsingBackup;
                cachedZone.BackupType = zone.BackupType;
                cachedZone.BackupCode = zone.BackupCode;
                cachedZone.CampaignType = zone.CampaignType;
                cachedZone.ModifiedBy = zone.ModifiedBy;
                cachedZone.RedirectLink = zone.RedirectLink;
                cachedZone.ModifiedAt = zone.ModifiedAt;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ZoneCore>(zone.SubId), cachedZone);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ZoneCore>(), new PatchCacheManagerSignal<ZoneCore>
                {
                    Entity = cachedZone,
                    EntitySubId = zone.SubId
                });
            }

            return new UpdateZoneResponse
            {
                Id = zone.Id,
                Name = zone.Name,
                UsingBackup = zone.UsingBackup,
                BackupType = zone.BackupType,
                BackupCode = zone.BackupCode,
                CampaignType = zone.CampaignType,
                WebsiteId = zone.WebsiteId,
                RedirectLink = zone.RedirectLink
            };
        }
    }
}