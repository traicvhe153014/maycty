﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class CreateWebsiteCommand : INovanetRequest<CreateWebsiteResponse>
{
    public string Domain { get; set; } = default!;

    public string Url { get; set; } = default!;

    public Guid PublisherId { get; set; }

    public List<Guid> ProhibitedCategoryIds { get; set; } = default!;

    public List<string>? DomainAliases { get; set; }

    public WebsiteStatus Status { get; set; }

    public bool Active { get; set; }


    internal class Handler : NovanetRequestHandler<CreateWebsiteCommand, CreateWebsiteResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService) : base(
            logger,
            httpContextAccessor)
        {
            _context = context;
            _messageBusClient = messageBusClient;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<CreateWebsiteResponse> HandleAsync(CreateWebsiteCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var existingDomain = await _context.Websites
                .AnyAsync(
                    x => x.Domain.ToLower() == request.Domain.ToLower() &&
                         x.PublisherId == request.PublisherId, cancellationToken);
            var errorMessages = new List<CreateWebsiteError>();
            if (existingDomain)
            {
                errorMessages.Add(new CreateWebsiteError
                {
                    ErrorType = MessagesCreateWebsite.DomainExisted
                });
            }

            var existingUrl = await _context.Websites
                .AnyAsync(
                    x => x.Url.ToLower() == request.Url.ToLower() &&
                         x.PublisherId == request.PublisherId, cancellationToken);
            if (existingUrl)
            {
                errorMessages.Add(new CreateWebsiteError
                {
                    ErrorType = MessagesCreateWebsite.UrlExisted
                });
            }

            if (request.DomainAliases is not null)
            {
                foreach (var domainAlias in request.DomainAliases)
                {
                    var existingDomainAlias = await _context.DomainAliases
                        .Include(x => x.Website)
                        .AnyAsync(
                            x => x.Name.ToLower() == domainAlias.ToLower() &&
                                 x.Website.PublisherId == request.PublisherId, cancellationToken);
                    if (existingDomainAlias)
                    {
                        errorMessages.Add(new CreateWebsiteError
                        {
                            ErrorType = MessagesCreateWebsite.DomainAliasExisted,
                            Value = domainAlias
                        });
                    }
                }
            }

            if (errorMessages.Count > 0)
            {
                throw new BadRequestException(errorMessages.Serialize());
            }


            var website = new Website
            {
                Active = true,
                Status = WebsiteStatus.Running,
                Name = request.Domain,
                Domain = request.Domain,
                Url = request.Url,
                PublisherId = request.PublisherId,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
                CreatedAt = DateTimeOffset.Now,
                ModifiedAt = DateTimeOffset.Now,
                DomainAliases = request.DomainAliases?.Select(x => new DomainAlias
                {
                    Name = x
                }).ToList(),
                WebsiteProhibitedCategories = request.ProhibitedCategoryIds.Select(x =>
                    new WebsiteProhibitedCategory
                    {
                        CreatedBy = currentUserId,
                        ProhibitedCategoryId = x
                    }).ToList()
            };
            var response = await _context.Websites.AddAsync(website, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            await _globalCacheService.SetAsync(RedisKeys.KeySettings<WebsiteCore>(response.Entity.SubId),
                response.Entity);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsiteCore>(),
                new PatchCacheManagerSignal<WebsiteCore>
                {
                    Entity = response.Entity,
                    EntitySubId = response.Entity.SubId
                });
            return new CreateWebsiteResponse
            {
                Id = response.Entity.Id,
                Name = response.Entity.Name,
                Domain = response.Entity.Domain,
                Url = response.Entity.Url,
                PublisherId = response.Entity.PublisherId,
                DomainAliases = response.Entity.DomainAliases?.Select(x => x.Name).ToList()
            };
        }
    }
}

public class CreateWebsiteCommandValidator : AbstractValidator<CreateWebsiteCommand>
{
    public CreateWebsiteCommandValidator()
    {
        RuleFor(x => x.Domain).NotEmpty();
        RuleFor(x => x.Url).NotEmpty();
        RuleFor(x => x.PublisherId).NotEmpty();
    }
}