using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateDomainAliasesCommand : INovanetRequest<DomainAlias>
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public Guid? WebsiteId { get; set; }
    
    public Guid? PublisherId { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateDomainAliasesCommand, DomainAlias>
    {
        private readonly SettingsContext _context;

        public Handler(ILogger<NovanetRequestHandler<UpdateDomainAliasesCommand, DomainAlias>> logger,
            IHttpContextAccessor httpContextAccessor, SettingsContext context) :
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<DomainAlias> HandleAsync(UpdateDomainAliasesCommand request,
            CancellationToken cancellationToken)
        {
            var domain = await _context.DomainAliases.Include(x=>x.Website).FirstOrDefaultAsync(x =>
                x.Name.ToLower().Equals(request.Name.ToLower()) && x.Website.PublisherId == request.PublisherId &&
                !x.Id.Equals(request.Id), cancellationToken);
            if (domain is not null)
            {
                throw new BadRequestException("Domain đã tồn tại");
            }

            var domainAlias =
                await _context.DomainAliases.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            var currentUserId = UserClaimsValue.Id;
            if (domainAlias is not null)
            {
                domainAlias.Name = request.Name;
                domainAlias.ModifiedBy = currentUserId;
                _context.DomainAliases.Update(domainAlias);
                await _context.SaveChangesAsync(cancellationToken);
                return domainAlias;
            }

            throw new BadRequestException("error");
        }
    }
}