﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class CreateCampaignCommand : INovanetRequest<CampaignCore>
{
    public string Name { get; set; } = default!;

    public CampaignType CampaignType { get; set; }

    public double EcommerceAmountSpent { get; set; }

    public double EcommerceDailyBudget { get; set; }

    public Guid? EcommerceProductFeedId { get; set; }

    public DateTimeOffset EcommerceStartDate { get; set; }

    public DateTimeOffset? EcommerceEndDate { get; set; }

    public bool? EcommerceScheduled { get; set; }

    public bool? EcommerceScheduledAllTime { get; set; }

    public List<CampaignScheduling>? Schedulings { get; set; }

    internal class Handler : NovanetRequestHandler<CreateCampaignCommand, CampaignCore>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<CampaignCore> HandleAsync(CreateCampaignCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var campaign = new Campaign
            {
                Name = request.Name,
                CampaignType = request.CampaignType,
                EcommerceScheduled = request.EcommerceScheduled,
                EcommerceAmountSpent = request.EcommerceAmountSpent,
                EcommerceDailyBudget = request.EcommerceDailyBudget,
                EcommerceStartDate = request.EcommerceStartDate,
                EcommerceEndDate = request.EcommerceEndDate,
                EcommerceProductFeedId = request.EcommerceProductFeedId,
                CampaignScheduling = request.Schedulings,
                IsActive = true,
                Status = CampaignStatus.Unfinished, // do chưa có advertisingSet/advertising,
                EcommerceScheduledAllTime = request.EcommerceScheduledAllTime ?? default,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
            };

            var entityEntry = await _context.Campaigns.AddAsync(campaign, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            // update cache
            entityEntry.Entity.SchedulingCore = entityEntry.Entity.CampaignScheduling?.Select(x => new CampaignSchedulingCore
            {
                Id = x.Id,
                Timing = x.Timing,
                CampaignId = x.CampaignId,
                SubId = x.SubId,
                DayOfWeek = x.DayOfWeek
            }).ToList() ?? new List<CampaignSchedulingCore>();
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<CampaignCore>(entityEntry.Entity.SubId), entityEntry.Entity);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<CampaignCore>(), new PatchCacheManagerSignal<CampaignCore>
            {
                Entity = entityEntry.Entity,
                EntitySubId = entityEntry.Entity.SubId,
            });
            return campaign;
        }
    }
}

public class CreateCampaignCommandValidator : AbstractValidator<CreateCampaignCommand>
{
    public CreateCampaignCommandValidator()
    {
        RuleFor(x => x.Name)
            .NotEmpty();
        RuleFor(x => x.EcommerceAmountSpent)
            .NotEmpty()
            .GreaterThanOrEqualTo(100000);
        RuleFor(x => x.EcommerceDailyBudget)
            .NotEmpty()
            .GreaterThanOrEqualTo(1000)
            .LessThanOrEqualTo(x => x.EcommerceAmountSpent);
        RuleFor(x => x.EcommerceProductFeedId)
            .NotEmpty()
            .When(x => x.CampaignType == CampaignType.Display);
        RuleFor(x => x.EcommerceStartDate)
            .GreaterThanOrEqualTo(DateTimeOffset.Now);
        RuleFor(x => x.EcommerceEndDate)
            .GreaterThanOrEqualTo(DateTimeOffset.Now.AddDays(1));
    }
}