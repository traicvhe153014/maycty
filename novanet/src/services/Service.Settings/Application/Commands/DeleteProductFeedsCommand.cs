﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class DeleteProductFeedsCommand : INovanetRequest<string>
{
    public Guid[] Ids { get; set; }

    internal class Handler : NovanetRequestHandler<DeleteProductFeedsCommand, string>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context,
            IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<string> HandleAsync(DeleteProductFeedsCommand request,
            CancellationToken cancellationToken)
        {
            var productFeeds = await _context.ProductFeeds
                .Include(x => x.ProductEntities)
                .Where(x => request.Ids.Contains(x.Id))
                .ToListAsync(cancellationToken);

            foreach (var productFeed in productFeeds)
            {
                _context.ProductStatus.RemoveRange(
                    _context.ProductStatus.Where(x => x.ProductFeedId == productFeed.Id));
                _context.ProductStatus.RemoveRange(
                    _context.ProductStatus.Where(x =>
                        x.ProductEntity != null && x.ProductEntity.ProductFeedId == productFeed.Id));
                _context.ProductStatus.RemoveRange(
                    _context.ProductStatus.Where(x =>
                        x.ProductAttributeValue != null &&
                        x.ProductAttributeValue.ProductEntity.ProductFeedId == productFeed.Id));
                var productAttributeValues =
                    _context.ProductAttributeValues.Where(x => x.ProductEntity.ProductFeedId == productFeed.Id);
                await productAttributeValues.ForEachAsync(x =>
                {
                    x.Deleted = true;
                }, cancellationToken);
                _context.ProductAttributeValues.UpdateRange(productAttributeValues);
                
                var productEntities =
                    _context.ProductEntities.Where(x => x.ProductFeedId == productFeed.Id);
                await productEntities.ForEachAsync(x =>
                {
                    x.Deleted = true;
                }, cancellationToken);
                _context.ProductEntities.UpdateRange(productEntities);

                // handle campaigns
                var campaigns = await _context.Campaigns
                    .Where(x => x.EcommerceProductFeedId == productFeed.Id)
                    .ToListAsync(cancellationToken);
                campaigns.ForEach(x =>
                {
                    x.Status = CampaignStatus.Error;
                });
                
                var advertisingSets = await _context.AdvertisingSets
                    .Include(x => x.Campaign)
                    .Include(x => x.AdvertisingSetProductGroups)!
                    .ThenInclude(x => x.ProductGroup)
                    .ThenInclude(x => x!.ProductGroupEntities)!
                    .ThenInclude(x => x.ProductEntity)
                    .Where(x => x.Campaign!.EcommerceProductFeedId.Equals(productFeed.Id))
                    .ToListAsync(cancellationToken);
                advertisingSets.ForEach(x =>
                {
                    x.AdvertisingSetProductGroups?.ForEach(z =>
                    {
                        z.Status = AdvertisingSetStatus.Error;
                    });
                    x.Status = AdvertisingSetStatus.Error;
                });

                var advertising = await _context.Advertising
                    .Include(x => x.AdvertisingSet)
                    .ThenInclude(x => x!.Campaign)
                    .Where(x => x.AdvertisingSet!.Campaign!.EcommerceProductFeedId.Equals(productFeed.Id))
                    .ToListAsync(cancellationToken);
                advertising.ForEach(x =>
                {
                    x.Status = AdvertisingStatus.Error;
                });


                _context.Campaigns.UpdateRange(campaigns);
                _context.AdvertisingSets.UpdateRange(advertisingSets);
                _context.Advertising.UpdateRange(advertising);

                productFeed.Deleted = true;
                _context.ProductFeeds.Update(productFeed);

                // update cache
                await _globalCacheService.KeyDeleteAsync(RedisKeys.KeySettings<ProductFeedCore>(productFeed.SubId));
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductFeedCore>(), new PatchCacheManagerSignal<ProductFeedCore>
                {
                    Entity = null,
                    EntitySubId = productFeed.SubId
                });
                foreach (var productEntity in CacheManager.Products.Values
                             .Where(x => x.ProductFeedId == productFeed.Id))
                {
                    await _globalCacheService.KeyDeleteAsync(RedisKeys.KeySettings<ProductEntityCore>(productEntity.SubId));
                    _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ProductEntityCore>(), new PatchCacheManagerSignal<ProductFeedCore>
                    {
                        Entity = null,
                        EntitySubId = productEntity.SubId
                    });
                }
            }

            await _context.SaveChangesAsync(cancellationToken);
            return "done";
        }
    }
}