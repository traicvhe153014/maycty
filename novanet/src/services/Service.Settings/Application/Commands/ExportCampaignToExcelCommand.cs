﻿using System.ComponentModel;
using System.Linq.Expressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Models;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;
using Service.Settings.Enums;

namespace Service.Settings.Application.Commands;

public class ExportCampaignToExcelCommand : INovanetRequest<FileInformation>
{
    public List<PredicateModel>? Filters { get; set; }

    public AdvertisingStatus? Status { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public DateTimeOffset? EndDate { get; set; }

    public List<Guid?>? CampaignIds { get; set; } = default!;

    public List<Guid?>? AdvertisingSetIds { get; set; } = default!;


    public List<Guid?>? AdvertisingIds { get; set; } = default!;

    public string? Sorts { get; set; }

    public ExportType ExportType { get; set; }

    internal class Handler : NovanetRequestHandler<ExportCampaignToExcelCommand, FileInformation>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;
        private readonly IExportDataToExcelFile<Dictionary<string, object>> _exportDataToExcelFile;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IReportService reportService,
            IExportDataToExcelFile<Dictionary<string, object>> exportDataToExcelFile,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _exportDataToExcelFile = exportDataToExcelFile;
            _context = context;
            _reportService = reportService;
        }

        protected override async Task<FileInformation> HandleAsync(ExportCampaignToExcelCommand request,
            CancellationToken cancellationToken)
        {
            var dataDictionaryList = new List<Dictionary<string, object>>();

            var dataAdvertisingSetsDictionaryList = new List<Dictionary<string, object>>();

            var dayDataDictionaryList = new List<Dictionary<string, object>>();

            var weekDataDictionaryList = new List<Dictionary<string, object>>();

            var monthDataDictionaryList = new List<Dictionary<string, object>>();
            
            var locationDataDictionaryList = new List<Dictionary<string, object>>();
            
            var websiteDataDictionaryList = new List<Dictionary<string, object>>();
            
            var hourDataDictionaryList = new List<Dictionary<string, object>>();

            var exportData = new List<ExportDataResponse<List<Dictionary<string, object>>>>();

            FileInformation returnFile;

            var firstCampaignDate = DateTimeOffset.Now;

            Expression<Func<Advertising, bool>> whereExpression = x => true;
            var sorts = request.Sorts != null ? request.Sorts?.Split(",") : new[] {"-ModifiedAt"};

            if (request.Filters != null)
            {
                whereExpression = whereExpression.And(ToExpression(request.Filters, true));
            }

            if (request.Status != null)
            {
                whereExpression = whereExpression.And(x => x.Status == request.Status);
            }

            whereExpression = request.ExportType switch
            {
                ExportType.AdvertisingSet => whereExpression.And(x =>
                    request.AdvertisingSetIds!.Contains(x.AdvertisingSetId)),
                ExportType.Advertising => whereExpression.And(x => request.AdvertisingIds!.Contains(x.Id)),
                _ => whereExpression.And(x => request.CampaignIds!.Contains(x.CampaignId))
            };

            var advertisings = await _context.Advertising
                .Include(x => x.AdvertisingSet)
                !.ThenInclude(x => x.Campaign)
                .Include(x => x.AdvertisingAppliedTemplates)
                .ThenInclude(x => x.AdvertisingTemplateAttributeValues)
                .SortBy(sorts)
                .Where(whereExpression)
                .ToListAsync(cancellationToken);
            var firstAdvertisingDate = advertisings.Min(x => x.AdvertisingSet?.Campaign?.EcommerceStartDate) ?? DateTimeOffset.Now;

            var data = await GetOverviewData(request, advertisings, cancellationToken);
            
            data.ForEach(x =>
            {
                var dataDictionary = x.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(prop =>
                        prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                        prop.GetValue(x, null)! is not Guid)
                    .ToDictionary(prop =>
                        {
                            var attrs = prop.GetCustomAttributes(true);

                            foreach (var attr in attrs)
                            {
                                if (attr is DescriptionAttribute
                                    attribute) // check if attr implements interface (you may have to reflect to get this)
                                {
                                    return attribute?.Description;
                                }
                            }

                            return string.Empty;
                        },
                        prop => prop.GetValue(x, null)! is double
                            ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                            : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" : prop.GetValue(x, null)! );
                dataDictionaryList.Add(dataDictionary!);
            });

            if (request.ExportType.Equals(Enums.ExportType.Detail) || request.ExportType.Equals(Enums.ExportType.AdvertisingSet))
            {
                        
                var dataAdvertisingSets = await GetOverviewAdvertisingSetData(request, cancellationToken);

                dataAdvertisingSets.ForEach(x =>
                {
                    var dataDictionary = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    dataAdvertisingSetsDictionaryList.Add(dataDictionary!);
                });
    
            }
            
            if (request.ExportType.Equals(Enums.ExportType.Detail))
            {
                var rawDayData = await FillReportDayData(request, advertisings);

                var dayData = rawDayData
                    .GroupBy(x => new {x.Date, x.Week, x.CampaignId, x.CampaignName, x.EndDate, x.StartDate, x.Name})
                    .Select(x =>
                        new ExportData
                        {
                            CampaignName = x.Key.CampaignName,
                            CampaignId = x.Key.CampaignId,
                            Week = x.Key.Week,
                            EndDate = x.Key.EndDate,
                            StartDate = x.Key.StartDate,
                            Date = x.Key.Date,
                            Name = x.Key.Name,
                            Clicks = x.Sum(z => z.Clicks),
                            BannerClicks = null,
                            BannerClickRate = null,
                            TotalCost = x.Sum(z => z.TotalCost),
                            Impressions = x.Sum(z => z.Impressions),
                            CPC = (int) (x.Sum(z => z.Clicks) != 0 ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Clicks) : 0)!,
                            CPS = (int) (x.Sum(z => z.Purchases) != 0
                                ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Purchases)
                                : 0)!,
                            CTR = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Clicks) / x.Sum(z => z.Impressions) * 100
                                : 0,
                            Purchases = x.Sum(z => z.Purchases),
                            PurchaseConversion = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Purchases) / x.Sum(z => z.Impressions) * 100
                                : 0,
                        })
                    .OrderBy(x => x.CampaignId)
                    .ThenBy(x => x.Date)
                    .ToList();
                
                var returnDayData = rawDayData
                    .GroupBy(x => new {x.Date, x.Week, x.CampaignId, x.CampaignName, x.EndDate, x.StartDate, x.Name})
                    .Select(x =>
                        new ExportData
                        {
                            CampaignName = x.Key.CampaignName,
                            CampaignId = x.Key.CampaignId,
                            Week = null,
                            EndDate = x.Key.EndDate,
                            StartDate = x.Key.StartDate,
                            Date = x.Key.Date,
                            Name = x.Key.Name,
                            Clicks = x.Sum(z => z.Clicks),
                            BannerClicks = null,
                            BannerClickRate = null,
                            TotalCost = x.Sum(z => z.TotalCost),
                            Impressions = x.Sum(z => z.Impressions),
                            CPC = (int) (x.Sum(z => z.Clicks) != 0 ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Clicks) : 0)!,
                            CPS = (int) (x.Sum(z => z.Purchases) != 0 ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Purchases) : 0)!,
                            CTR = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Clicks) / x.Sum(z => z.Impressions) * 100
                                : 0,
                            Purchases = x.Sum(z => z.Purchases),
                            PurchaseConversion = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Purchases) / x.Sum(z => z.Impressions) * 100
                                : 0,
                        })
                    .OrderBy(x => x.CampaignId)
                    .ThenBy(x => x.Date)
                    .ToList();


                returnDayData.ForEach(x =>
                {
                    var weekDataDictionary = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is DateOnly ? DateOnly.Parse(prop.GetValue(x, null)!.ToString()!).ToString("dd/MM/yyyy") : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    dayDataDictionaryList.Add(weekDataDictionary!);
                });

                var monthData = dayData
                    .GroupBy(x => new
                    {
                        x.Date.GetValueOrDefault().Year, x.Date.GetValueOrDefault().Month, x.CampaignName, x.CampaignId
                    })
                    .Select(x =>
                        new ExportData
                        {
                            CampaignName = x.Key.CampaignName,
                            CampaignId = x.Key.CampaignId,
                            Month = x.Key.Month.ToString("D2") + "/" + x.Key.Year,
                            Clicks = x.Sum(z => z.Clicks),
                            BannerClicks = null,
                            BannerClickRate = null,
                            TotalCost = x.Sum(z => z.TotalCost),
                            Impressions = x.Sum(z => z.Impressions),
                            CPC = (int) (x.Sum(z => z.Clicks) != 0 ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Clicks) : 0)!,
                            CPS = (int) (x.Sum(z => z.Purchases) != 0
                                ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Purchases)
                                : 0)!,
                            CTR = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Clicks) / x.Sum(z => z.Impressions) * 100
                                : 0,
                            Purchases = x.Sum(z => z.Purchases),
                            PurchaseConversion = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Purchases) / x.Sum(z => z.Impressions) * 100
                                : 0,
                        })
                    .ToList();

                monthData.ForEach(x =>
                {
                    var monthDataDic = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    monthDataDictionaryList.Add(monthDataDic!);
                });

                var weekData = dayData
                    .GroupBy(x => new {x.Week, x.CampaignName, x.CampaignId})
                    .Select(x =>
                        new ExportData
                        {
                            CampaignName = x.Key.CampaignName,
                            CampaignId = x.Key.CampaignId,
                            Week = x.Key.Week,
                            Clicks = x.Sum(z => z.Clicks),
                            BannerClicks = null,
                            BannerClickRate = null,
                            TotalCost = x.Sum(z => z.TotalCost),
                            Impressions = x.Sum(z => z.Impressions),
                            CPC = (int) (x.Sum(z => z.Clicks) != 0 ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Clicks) : 0)!,
                            CPS = (int ) (x.Sum(z => z.Purchases) != 0
                                ? x.Sum(z => z.TotalCost) / x.Sum(z => z.Purchases)
                                : 0)!,
                            CTR = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Clicks) / x.Sum(z => z.Impressions) * 100
                                : 0,
                            Purchases = x.Sum(z => z.Purchases),
                            PurchaseConversion = x.Sum(z => z.Impressions) != 0
                                ? x.Sum(z => z.Purchases) / x.Sum(z => z.Impressions) * 100
                                : 0,
                        })
                    .ToList();

                weekData.ForEach(x =>
                {
                    var weekDataDictionary = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    weekDataDictionaryList.Add(weekDataDictionary);
                });
             
                var campaigns = await GetCampaignList(request, cancellationToken);
                firstCampaignDate = campaigns.Min(x => x.EcommerceStartDate);
                // get campaign report by location
                var rawLocationData = await FillReportLocationData(request, campaigns);
                rawLocationData.ForEach(x =>
                {
                    x.LocationId = null;
                    x.BannerClicks = null;
                    x.BannerClickRate = null;
                });
                rawLocationData.ForEach(x =>
                {
                    var locationDictionary = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            (prop.GetValue(x, null) is not null || prop.PropertyType == typeof(double?)) && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .Where(prop => prop.Name != "BannerClicks" && prop.Name != "BannerClickRate")
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    locationDataDictionaryList.Add(locationDictionary);
                });
                
                // get campaign report by hour
                var rawHourData = await FillReportHourData(request, campaigns);
                rawHourData.ForEach(x =>
                {
                    x.BannerClicks = null;
                    x.BannerClickRate = null;
                });
                rawHourData.ForEach(x =>
                {
                    var hourDictionary = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            prop.GetValue(x, null) is not null && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    hourDataDictionaryList.Add(hourDictionary);
                });
                
                // get campaign report by website
                var rawWebsiteData = await FillReportWebsiteData(request, campaigns);
                rawWebsiteData.ForEach(x =>
                {
                    x.WebsiteId = null;
                    x.BannerClicks = null;
                    x.BannerClickRate = null;
                });
                rawWebsiteData.ForEach(x =>
                {
                    var websiteDictionary = x.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(prop =>
                            (prop.GetValue(x, null) is not null || prop.PropertyType == typeof(double?)) && prop.GetValue(x, null)! is not bool &&
                            prop.GetValue(x, null)! is not Guid)
                        .Where(prop => prop.Name != "BannerClicks" && prop.Name != "BannerClickRate")
                        .ToDictionary(prop =>
                            {
                                var attrs = prop.GetCustomAttributes(true);

                                foreach (var attr in attrs)
                                {
                                    if (attr is DescriptionAttribute
                                        attribute) // check if attr implements interface (you may have to reflect to get this)
                                    {
                                        return attribute?.Description;
                                    }
                                }

                                return string.Empty;
                            },
                            prop => prop.GetValue(x, null)! is double
                                ? (double) (prop.GetValue(x, null) ?? 0) % 1 == 0 ?  $"{prop.GetValue(x, null)!:N0}" :  $"{prop.GetValue(x, null)!:N2}" 
                                : prop.GetValue(x, null)! is int ? $"{prop.GetValue(x, null)!:N0}" :  prop.GetValue(x, null)! );
                    websiteDataDictionaryList.Add(websiteDictionary);
                });
            }

            var hasDateRange = HasDateRange(request);
            switch (request.ExportType)
            {
                case ExportType.Detail:
                    var detailData = new List<ExportDataResponse<List<Dictionary<string, object>>>>
                    {
                        new()
                        {
                            SheetName = "Báo cáo tổng quan",
                            Data = dataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo nhóm sản phẩm",
                            Data = dataAdvertisingSetsDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo chiến dịch theo ngày",
                            Data = dayDataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo chiến dịch theo tuần",
                            Data = weekDataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo chiến dịch theo tháng",
                            Data = monthDataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo chiến dịch theo địa lý",
                            Data = locationDataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo chiến dịch theo giờ",
                            Data = hourDataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo chiến dịch theo website",
                            Data = websiteDataDictionaryList
                        },
                    };
                    exportData.AddRange(detailData);
                    var streamDetailData = await _exportDataToExcelFile.ExportFile(exportData);
                    returnFile = new FileInformation
                    {
                        FileName = hasDateRange
                            ? $"Báo cáo chi tiết chiến dịch ({request.StartDate!.Value.DateTime:dd-MM-yyyy} đến {request.EndDate!.Value.DateTime:dd-MM-yyyy}).xlsx"
                            : $"Báo cáo chi tiết chiến dịch ({firstCampaignDate:dd-MM-yyyy} đến {DateTimeOffset.Now:dd-MM-yyyy})",
                        Content = streamDetailData
                    };
                    break;
                case ExportType.Campaign:
                    var campaignData = new List<ExportDataResponse<List<Dictionary<string, object>>>>
                    {
                        new()
                        {
                            SheetName = "Báo cáo tổng quan",
                            Data = dataDictionaryList
                        }
                    };
                    exportData.AddRange(campaignData);
                    var streamCampaignData = await _exportDataToExcelFile.ExportFile(exportData);
                    returnFile = new FileInformation
                    {
                        FileName = hasDateRange
                            ? $"Báo cáo tổng quan chiến dịch ({request.StartDate!.Value.DateTime:dd-MM-yyyy} đến {request.EndDate!.Value.DateTime:dd-MM-yyyy}).xlsx"
                            : $"Báo cáo tổng quan chiến dịch ({firstCampaignDate:dd-MM-yyyy} đến {DateTimeOffset.Now:dd-MM-yyyy}).xlsx",
                        Content = streamCampaignData
                    };
                    break;
                case ExportType.AdvertisingSet:
                    var advertisingSetData = new List<ExportDataResponse<List<Dictionary<string, object>>>>
                    {   
                        new()
                        {
                            SheetName = "Báo cáo tổng quan",
                            Data = dataDictionaryList
                        },
                        new()
                        {
                            SheetName = "Báo cáo nhóm sản phẩm",
                            Data = dataAdvertisingSetsDictionaryList
                        }
                    };
                    exportData.AddRange(advertisingSetData);
                    var streamAdvertisingSetData = await _exportDataToExcelFile.ExportFile(exportData);
                    returnFile = new FileInformation
                    {
                        FileName = hasDateRange
                            ? $"Báo cáo nhóm quảng cáo ({request.StartDate!.Value.DateTime:dd-MM-yyyy} đến {request.EndDate!.Value.DateTime:dd-MM-yyyy}).xlsx"
                            : $"Báo cáo nhóm quảng cáo ({firstAdvertisingDate:dd-MM-yyyy} đến {DateTimeOffset.Now:dd-MM-yyyy}).xlsx",
                        Content = streamAdvertisingSetData
                    };
                    break;
                case ExportType.Advertising:
                    var advertisingData = new List<ExportDataResponse<List<Dictionary<string, object>>>>
                    {
                        new()
                        {
                            SheetName = "Báo cáo tin quảng cáo",
                            Data = dataDictionaryList
                        }
                    };
                    exportData.AddRange(advertisingData);
                    var streamAdvertisingData = await _exportDataToExcelFile.ExportFile(exportData);
                    returnFile = new FileInformation
                    {
                        FileName = hasDateRange
                            ? $"Báo cáo tin quảng cáo ({request.StartDate!.Value.DateTime:dd-MM-yyyy} đến {request.EndDate!.Value.DateTime:dd-MM-yyyy}).xlsx"
                            : $"Báo cáo tin quảng cáo ({firstAdvertisingDate:dd-MM-yyyy} đến {DateTimeOffset.Now:dd-MM-yyyy})",
                        Content = streamAdvertisingData
                    };
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return returnFile;
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillReportData(
            ExportCampaignToExcelCommand request,
            List<Advertising> advertisings)
        {
            var now = DateTimeOffset.UtcNow;
            var advertisingIds = advertisings.ToDictionary(x => x.Id, x => (object) x.SubId);
            var advertisingSubIds = advertisingIds.Values.ToList();
            var hasDateRange = HasDateRange(request);
            // total report
            var totalClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Click,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Click,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds);

            var totalBannerClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.BannerClick,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.BannerClick,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds);

            var totalViewMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.View,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.View,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds);

            var totalPaidMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Paid,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Paid,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds);
            
            var totalPurchaseMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Conversion,
                    ReportAdvertiserDimension.Advertising,
                    advertisingSubIds);

            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var advertising in advertisings)
            {
                var totalClicks = totalClickMetrics.GetValueOrDefault(advertising.SubId);
                var totalBannerClicks = totalBannerClickMetrics.GetValueOrDefault(advertising.SubId);
                var bannerClickRate = totalClicks != 0 ? totalBannerClicks / totalClicks * 100 : default;
                var totalImpressions = totalViewMetrics.GetValueOrDefault(advertising.SubId);
                var totalCost = Math.Round(totalPaidMetrics.GetValueOrDefault(advertising.SubId));
                var totalCtr = totalImpressions != 0 ? totalClicks / totalImpressions * 100 : default;
                var totalCpc = totalClicks != 0 ? totalCost / totalClicks : default;
                double totalPurchase = totalPurchaseMetrics.GetValueOrDefault(advertising.SubId);
                double totalPurchaseConversion = totalImpressions != 0 ? totalPurchase / totalImpressions * 100 : default;
                double totalCps = default;
                var itemData = new Dictionary<string, object?>
                {
                    {"totalCost", totalCost},
                    {"clicks", totalClicks},
                    {"bannerClicks", totalBannerClicks},
                    {"bannerClickRate", bannerClickRate},
                    {"impressions", totalImpressions},
                    {"ctr", totalCtr},
                    {"cpc", totalCpc},
                    {"purchase", totalPurchase},
                    {"purchaseConversion", totalPurchaseConversion},
                    {"cps", totalCps}
                };
                result[advertising.Id] = itemData;
            }

            return result;
        }

        private async Task<List<ExportData>> FillReportDayData(
            ExportCampaignToExcelCommand request,
            List<Advertising> advertisings)
        {
            var now = DateTimeOffset.UtcNow;
            var firstDay = advertisings.Min(x => x.AdvertisingSet?.Campaign?.EcommerceStartDate) ?? DateTimeOffset.Now;
            var advertisingIds = advertisings.ToDictionary(x => x.Id, x => (object) x.SubId);
            var advertisingSubIds = advertisingIds.Values.ToList();

            // total report
            var totalClickMetrics = await _reportService.GetAdvertiserReport(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.Click,
                ReportAdvertiserDimension.Advertising,
                advertisingSubIds);

            var totalViewMetrics = await _reportService.GetAdvertiserReport(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.View,
                ReportAdvertiserDimension.Advertising,
                advertisingSubIds);

            var totalPaidMetrics = await _reportService.GetAdvertiserReport(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.Paid,
                ReportAdvertiserDimension.Advertising,
                advertisingSubIds);

            var totalConversionMetrics = await _reportService.GetAdvertiserReport(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.Conversion,
                ReportAdvertiserDimension.Advertising,
                advertisingSubIds);

            var advertisingData = new List<ExportData>();

            foreach (var advertising in advertisings)
            {
                var startDate =
                    DateOnly.FromDateTime(advertising.AdvertisingSet!.Campaign!.EcommerceStartDate.DateTime);
                var totalCost = totalPaidMetrics?.GetValueOrDefault(advertising.SubId)?.ToList();
                totalCost?.ForEach(item =>
                {
                    var date = DateOnly.FromDateTime(long.Parse(item.Key).GetDateFromOrigin().DateTime);
                    var firstDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetFirstDayOfWeek();
                    var lastDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetLastDayOfWeek();
                    var existItem = advertisingData.Find(x => x.Date.Equals(date) && x.Id.Equals(advertising.Id));
                    if (date < startDate) return;
                    if (existItem is not null)
                    {
                        existItem.TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()));
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = advertising.Id,
                            Date = date,
                            Week = DateOnly.FromDateTime(firstDayOfWeek).ToString("dd/MM/yyyy") + " - " +
                                   DateOnly.FromDateTime(lastDayOfWeek).ToString("dd/MM/yyyy"),
                            CampaignName = advertising.AdvertisingSet?.Campaign?.Name,
                            CampaignId = advertising.AdvertisingSet?.Campaign?.Id,
                            TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()))
                        };
                        advertisingData.Add(reportByDay);
                    }
                });

                var totalClicks = totalClickMetrics?.GetValueOrDefault(advertising.SubId)?.ToList();
                totalClicks?.ForEach(item =>
                {
                    var date = DateOnly.FromDateTime(long.Parse(item.Key).GetDateFromOrigin().DateTime);
                    var firstDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetFirstDayOfWeek();
                    var lastDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetLastDayOfWeek();
                    var existItem = advertisingData.Find(x => x.Date.Equals(date) && x.Id.Equals(advertising.Id));
                    if (date < startDate) return;
                    if (existItem is not null)
                    {
                        existItem.Clicks = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = advertising.Id,
                            Date = date,
                            Week = DateOnly.FromDateTime(firstDayOfWeek).ToString("dd/MM/yyyy") + " - " +
                                   DateOnly.FromDateTime(lastDayOfWeek).ToString("dd/MM/yyyy"),
                            CampaignName = advertising.AdvertisingSet?.Campaign?.Name,
                            CampaignId = advertising.AdvertisingSet?.Campaign?.Id,
                            Clicks = Convert.ToDouble(item.Value.ToString())
                        };
                        advertisingData.Add(reportByDay);
                    }
                });

                var totalViews = totalViewMetrics?.GetValueOrDefault(advertising.SubId)?.ToList();
                totalViews?.ForEach(item =>
                {
                    var date = DateOnly.FromDateTime(long.Parse(item.Key).GetDateFromOrigin().DateTime);
                    var firstDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetFirstDayOfWeek();
                    var lastDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetLastDayOfWeek();
                    var existItem = advertisingData.Find(x => x.Date.Equals(date) && x.Id.Equals(advertising.Id));
                    if (date < startDate) return;
                    if (existItem is not null)
                    {
                        existItem.Impressions = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = advertising.Id,
                            Date = date,
                            Week = DateOnly.FromDateTime(firstDayOfWeek).ToString("dd/MM/yyyy") + " - " +
                                   DateOnly.FromDateTime(lastDayOfWeek).ToString("dd/MM/yyyy"),
                            CampaignName = advertising.AdvertisingSet?.Campaign?.Name,
                            CampaignId = advertising.AdvertisingSet?.Campaign?.Id,
                            Impressions = Convert.ToDouble(item.Value.ToString())
                        };
                        advertisingData.Add(reportByDay);
                    }
                });

                var totalPurchase = totalConversionMetrics?.GetValueOrDefault(advertising.SubId)?.ToList();
                totalPurchase?.ForEach(item =>
                {
                    var date = DateOnly.FromDateTime(long.Parse(item.Key).GetDateFromOrigin().DateTime);
                    var firstDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetFirstDayOfWeek();
                    var lastDayOfWeek = date.ToDateTime(TimeOnly.Parse("00:00")).GetLastDayOfWeek();
                    var existItem = advertisingData.Find(x => x.Date.Equals(date) && x.Id.Equals(advertising.Id));
                    if (date < startDate) return;
                    if (existItem is not null)
                    {
                        existItem.Purchases = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = advertising.Id,
                            Date = date,
                            Week = DateOnly.FromDateTime(firstDayOfWeek).ToString("dd/MM/yyyy") + " - " +
                                   DateOnly.FromDateTime(lastDayOfWeek).ToString("dd/MM/yyyy"),
                            CampaignName = advertising.AdvertisingSet?.Campaign?.Name,
                            CampaignId = advertising.AdvertisingSet?.Campaign?.Id,
                            Purchases = Convert.ToDouble(item.Value.ToString())
                        };
                        advertisingData.Add(reportByDay);
                    }
                });
            }

            advertisingData.ForEach(x =>
            {
                x.CPC = (int) (x.Clicks != 0 ? x.TotalCost / x.Clicks : 0)!;
                x.CTR = x.Impressions != 0 ? x.Clicks / x.Impressions * 100 : 0;
                x.PurchaseConversion = x.Impressions != 0 ? x.Purchases / x.Impressions * 100 : 0;
                x.CPS = (int) (x.Purchases != 0 ? x.TotalCost / x.Purchases : 0)!;
            });

            return advertisingData;
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillAdvertisingSet(
            ExportCampaignToExcelCommand request,
            List<AdvertisingSetResponse> advertisingSetProductGroups)
        {
            var now = DateTimeOffset.UtcNow;
            var advertisingSetProductGroupIds =
                advertisingSetProductGroups.ToDictionary(x => x.Id, x => (object) x.SubId!);
            var advertisingSetProductGroupSubIds = advertisingSetProductGroupIds.Values.ToList();
            // total report
            var hasDateRange = HasDateRange(request);
            var totalClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Click,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Click,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds);

            var totalViewMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.View,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.View,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds);

            var totalPaidMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Paid,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Paid,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds);
            
            var totalPurchaseMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Conversion,
                    ReportAdvertiserDimension.AdvertisingSet,
                    advertisingSetProductGroupSubIds);
            
            
            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var advertisingSetProductGroup in advertisingSetProductGroups)
            {
                var totalClicks = totalClickMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId!.Value);
                var totalImpressions = totalViewMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId.Value);
                var totalCost = Math.Round(totalPaidMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId.Value));
                var totalCtr = totalImpressions != 0 ? totalClicks / totalImpressions * 100 : default;
                var totalCpc = totalClicks != 0 ? totalCost / totalClicks : default;
                var totalPurchase = totalPurchaseMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId!.Value);
                var totalPurchaseConversion = totalImpressions != 0 ? totalPurchase / totalImpressions * 100 : default;
                double totalCps = default;
                var itemData = new Dictionary<string, object?>
                {
                    {"totalCost", totalCost},
                    {"clicks", totalClicks},
                    {"impressions", totalImpressions},
                    {"ctr", totalCtr},
                    {"cpc", totalCpc},
                    {"purchase", totalPurchase},
                    {"purchaseConversion", totalPurchaseConversion},
                    {"cps", totalCps}
                };
                result[advertisingSetProductGroup.Id] = itemData;
            }

            return result;
        }

        private async Task<Dictionary<Guid, Dictionary<string, object?>>> FillAdvertisingSetProductGroup(
            ExportCampaignToExcelCommand request,
            List<AdvertisingSetProductGroup> advertisingSetProductGroups)
        {
            var now = DateTimeOffset.UtcNow;
            var advertisingSetProductGroupIds =
                advertisingSetProductGroups.ToDictionary(x => x.Id, x => (object) x.SubId);
            var advertisingSetProductGroupSubIds = advertisingSetProductGroupIds.Values.ToList();
            // total report
            var hasDateRange = HasDateRange(request);
            var totalClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Click,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Click,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds);

            var totalViewMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.View,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.View,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds);

            var totalPaidMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Paid,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Paid,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds);
            
            var totalPurchaseMetrics = hasDateRange
                ? await _reportService.GetAdvertiserReportByDay(
                    request.StartDate ?? now,
                    request.EndDate ?? now,
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds)
                : await _reportService.GetTotalAdvertiserReport(ReportMetric.Conversion,
                    ReportAdvertiserDimension.AdvertisingSetProductGroup,
                    advertisingSetProductGroupSubIds);
            
            var result = new Dictionary<Guid, Dictionary<string, object?>>();
            foreach (var advertisingSetProductGroup in advertisingSetProductGroups)
            {
                var totalClicks = totalClickMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId);
                var totalImpressions = totalViewMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId);
                var totalCost = Math.Round(totalPaidMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId));
                var totalCtr = totalImpressions != 0 ? totalClicks / totalImpressions * 100 : default;
                var totalCpc = totalClicks != 0 ? totalCost / totalClicks : default;
                var totalPurchase = totalPurchaseMetrics.GetValueOrDefault(advertisingSetProductGroup.SubId);
                var totalPurchaseConversion = totalImpressions != 0 ? totalPurchase / totalImpressions * 100 : default;
                double totalCps = default;
                var itemData = new Dictionary<string, object?>
                {
                    {"totalCost", totalCost},
                    {"clicks", totalClicks},
                    {"impressions", totalImpressions},
                    {"ctr", totalCtr},
                    {"cpc", totalCpc},
                    {"purchase", totalPurchase},
                    {"purchaseConversion", totalPurchaseConversion},
                    {"cps", totalCps}
                };
                result[advertisingSetProductGroup.Id] = itemData;
            }

            return result;
        }

        private async Task<List<ExportData>> GetOverviewData(ExportCampaignToExcelCommand request,
            List<Advertising> advertisings,
            CancellationToken cancellationToken)
        {
            var templates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);
            var templateAttributes = await _context.AdvertisingTemplateAttributes.ToListAsync(cancellationToken);

            var data = advertisings.Select(advertising =>
                {
                    var appliedTemplates = new List<AdvertisingAppliedTemplateResponse>();
                    foreach (var appliedTemplate in advertising.AdvertisingAppliedTemplates ??
                                                    new List<AdvertisingAppliedTemplate>())
                    {
                        var template = templates.FirstOrDefault(x => x.Id == appliedTemplate.AdvertisingTemplateId);
                        var configurations = new Dictionary<string, object?>();
                        appliedTemplate.AdvertisingTemplateAttributeValues?.ForEach(x =>
                        {
                            var attribute =
                                templateAttributes.FirstOrDefault(attr => attr.Id == x.AdvertisingTemplateAttributeId);
                            configurations.Add(attribute?.Name ?? "", x.GetValue(attribute?.DataType));
                        });

                        var appliedTemplateResponse = new AdvertisingAppliedTemplateResponse
                        {
                            TemplateId = appliedTemplate.AdvertisingTemplateId,
                            TemplateName = template?.Name,
                            TemplateType = template?.TemplateType,
                            Height = template?.Height,
                            Width = template?.Width,
                            Configurations = configurations
                        };
                        appliedTemplates.Add(appliedTemplateResponse);
                    }

                    var responseItem = new ExportData
                    {
                        Id = advertising.Id,
                        Name = advertising.Name,
                        CampaignName = advertising.AdvertisingSet?.Campaign?.Name,
                        CampaignId = advertising.AdvertisingSet?.Campaign?.Id,
                        AdvertisingSetName = advertising.AdvertisingSet?.Name,
                        AdvertisingType = advertising.AdvertisingAppliedTemplates?[0].AdvertisingTemplate?.Name,
                        AdvertisingSize =
                            $"{advertising.AdvertisingAppliedTemplates?[0].AdvertisingTemplate?.Width.ToString()}x{advertising.AdvertisingAppliedTemplates?[0].AdvertisingTemplate?.Height.ToString()}px",
                        RedirectLink = advertising.RedirectLink ?? string.Empty,
                        StartDate =
                            advertising.AdvertisingSet?.Campaign?.EcommerceStartDate.ToString("dd/MM/yyyy HH:mm"),
                        EndDate = advertising.AdvertisingSet?.Campaign?.EcommerceEndDate is not null ? advertising.AdvertisingSet?.Campaign?.EcommerceEndDate?.ToString("dd/MM/yyyy HH:mm") : "-"
                    };
                    return responseItem;
                })
                .ToList();

            var reportData = await FillReportData(request, advertisings);
            foreach (var exportData in data)
            {
                reportData.TryGetValue(exportData.Id, out var dataItem);
                if (dataItem is null) continue;
                exportData.TotalCost = Math.Round(dataItem["totalCost"] as double? ?? 0);
                exportData.Clicks = dataItem["clicks"] as double? ?? 0;
                exportData.Impressions = dataItem["impressions"] as double? ?? 0;
                exportData.BannerClicks = dataItem["bannerClicks"] as double? ?? 0;
                exportData.BannerClickRate = dataItem["bannerClickRate"] as double? ?? 0;
                exportData.CTR = dataItem["ctr"] as double? ?? 0;
                exportData.CPC = Convert.ToInt32(dataItem["cpc"]);
                exportData.CPS = Convert.ToInt32(dataItem["cps"]);
                exportData.Purchases = dataItem["purchase"] as double? ?? 0;
                exportData.PurchaseConversion = dataItem["purchaseConversion"] as double? ?? 0;
            }

            data = data.OrderBy(x => x.CampaignId).ToList();

            return data;
        }

        private async Task<List<ExportData>> GetOverviewAdvertisingSetData(ExportCampaignToExcelCommand request,
            CancellationToken cancellationToken)
        {
            var sorts = request.Sorts != null ? request.Sorts?.Split(",") : new[] {"-ModifiedAt"};

            Expression<Func<AdvertisingSet, bool>> whereExpression = x => true;

            whereExpression = whereExpression.And(x => request.ExportType.Equals(ExportType.Detail) ? request.CampaignIds!.Contains(x.Campaign.Id) : request.AdvertisingSetIds!.Contains(x.Id));

            var advertisingSets = await _context.AdvertisingSets
                .Include(x => x.Campaign)
                .Include(x => x.Advertisings)
                .Include(x => x.AdvertisingSetProductGroups)
                !.ThenInclude(x => x.ProductGroup)
                .SortBy(sorts)
                .Where(whereExpression)
                .ToListAsync(cancellationToken);

            var data = advertisingSets.Select(advertisingSet =>
                {
                    var adSetResponse = new AdvertisingSetResponse
                    {
                        Id = advertisingSet.Id,
                        SubId = advertisingSet.SubId,
                        Name = advertisingSet.Name,
                        CampaignName = advertisingSet.Campaign?.Name,
                        CampaignId = advertisingSet.CampaignId,
                        Status = advertisingSet.Status,
                        IsActive = advertisingSet.IsActive,
                        Data = new Dictionary<string, object?>(),
                        ProductGroupData = advertisingSet.AdvertisingSetProductGroups?.Select(x =>
                            new AdvertisingSetProductGroupResponse
                            {
                                Id = x.ProductGroupId,
                                Name = x.ProductGroup?.Name ?? string.Empty,
                                Status = x.Status,
                                IsActive = x.IsActive,
                                Data = new Dictionary<string, object?>()
                            }).ToList()
                    };
                    return adSetResponse;
                })
                .ToList();

            var advertingSetsReport = new List<ExportData>();

            var allAdvertingSets = new List<ExportData>();

            var advertisingSetReports = await FillAdvertisingSet(request,
                data);

            var advertisingSetProductGroups = new List<ExportData>();

            foreach (var advertisingSetResponse in data)
            {
                var advertisingSetProductGroupsLocal = new List<ExportData>();

                var advertisingSet = advertisingSets.FirstOrDefault(x => x.Id == advertisingSetResponse.Id);

                var totalAdvertisings = CacheManager.Advertising.Values
                    .Select(x =>
                    {
                        var advertising = new Advertising
                        {
                            Id = x.Id,
                            Status = x.Status,
                            CreatedBy = x.CreatedBy,
                            IsActive = x.IsActive,
                            SubId = x.SubId,
                            Name = x.Name,
                            CampaignId = x.CampaignId,
                            AdvertisingSetId = x.AdvertisingSetId,
                            Search = x.Search ?? string.Empty
                        };
                        return advertising;
                    })
                    .Count(x => x.AdvertisingSetId.Equals(advertisingSet!.Id));

                if (totalAdvertisings.Equals(0)) continue;
                advertisingSet?.AdvertisingSetProductGroups?.ForEach(y =>
                {
                    var group = new ExportData
                    {
                        Id = advertisingSet.Id,
                        AdvertisingSetName = advertisingSet.Name,
                        CampaignName = advertisingSet.Campaign?.Name,
                        CampaignId = advertisingSet.CampaignId,
                        ProductGroupId = y.Id,
                        ProductGroupName = y.ProductGroup?.Name
                    };
                    advertisingSetProductGroupsLocal.Add(group);
                });

                var advertisingSetExportData = new ExportData
                {
                    Id = advertisingSetResponse.Id,
                    AdvertisingSetName = advertisingSetResponse.Name,
                    CampaignName = advertisingSetResponse.CampaignName,
                    CampaignId = advertisingSetResponse.CampaignId,
                    ProductGroupName = "Tất cả"
                };

                advertisingSetReports.TryGetValue(advertisingSetExportData.Id, out var dataAdvertisingSetItem);
                if (dataAdvertisingSetItem is null) continue;
                advertisingSetExportData.TotalCost = null;
                advertisingSetExportData.Clicks = dataAdvertisingSetItem["clicks"] as double? ?? 0;
                advertisingSetExportData.Impressions = dataAdvertisingSetItem["impressions"] as double? ?? 0;
                advertisingSetExportData.BannerClicks = null;
                advertisingSetExportData.BannerClickRate = null;
                advertisingSetExportData.CTR = dataAdvertisingSetItem["ctr"] as double? ?? 0;
                advertisingSetExportData.CPC = null;
                advertisingSetExportData.CPS = null;
                advertisingSetExportData.Purchases = dataAdvertisingSetItem["purchase"] as double? ?? 0;
                advertisingSetExportData.PurchaseConversion =
                    dataAdvertisingSetItem["purchaseConversion"] as double? ?? 0;

                if (advertisingSetResponse.ProductGroupData!.Count.Equals(0))
                {
                    advertingSetsReport.Add(advertisingSetExportData);
                    allAdvertingSets.Add(advertisingSetExportData);
                }
                else
                {
                    allAdvertingSets.Add(advertisingSetExportData);
                }

                var groupReports = await FillAdvertisingSetProductGroup(request,
                    advertisingSet?.AdvertisingSetProductGroups ?? new List<AdvertisingSetProductGroup>());
                foreach (var exportData in advertisingSetProductGroupsLocal)
                {
                    groupReports.TryGetValue(exportData.ProductGroupId!.Value, out var dataItem);
                    if (dataItem is null) continue;
                    exportData.TotalCost = null;
                    exportData.Clicks = dataItem["clicks"] as double? ?? 0;
                    exportData.Impressions = dataItem["impressions"] as double? ?? 0;
                    exportData.BannerClicks = null;
                    exportData.BannerClickRate = null;
                    exportData.CTR = dataItem["ctr"] as double? ?? 0;
                    exportData.CPC = null;
                    exportData.CPS = null;
                    exportData.Purchases = dataItem["purchase"] as double? ?? 0;
                    exportData.PurchaseConversion = dataItem["purchaseConversion"] as double? ?? 0;
                }

                advertisingSetProductGroups.AddRange(advertisingSetProductGroupsLocal);
            }

            advertisingSetProductGroups.AddRange(advertingSetsReport);

            advertisingSetProductGroups = advertisingSetProductGroups.OrderBy(x => x.CampaignId).ToList();

            var totalRecord = new ExportData
            {
                CampaignName = "Tổng",
                Clicks = allAdvertingSets.Sum(z => z.Clicks),
                Impressions = advertisingSetProductGroups.Sum(z => z.Impressions),
                CTR = advertisingSetProductGroups.Sum(z => z.Impressions) != 0
                    ? allAdvertingSets.Sum(z => z.Clicks) /
                    advertisingSetProductGroups.Sum(z => z.Impressions) * 100
                    : default,
                Purchases = allAdvertingSets.Sum(z => z.Purchases),
                PurchaseConversion = advertisingSetProductGroups.Sum(z => z.Impressions) != 0
                    ? allAdvertingSets.Sum(z =>
                        z.Purchases) / advertisingSetProductGroups.Sum(z => z.Impressions) * 100
                    : 0,
            };
            advertisingSetProductGroups.Add(totalRecord);

            return advertisingSetProductGroups;
        }

        private async Task<List<ExportData>> FillReportLocationData(
            ExportCampaignToExcelCommand request,
            List<Campaign> campaigns)
        {
            var campaignSubIds = campaigns.Select(x => (object) x.SubId).ToList();
            var locationIds = CacheManager.Locations.Select(x => (object) x.Value.SubId).ToList();

            var hasDateRange = HasDateRange(request);

            // total report
            var totalClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserTargetingReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Click,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds)
                : await _reportService.GetTotalAdvertiserTargetingReport(
                    ReportMetric.Click,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds);

            var totalViewMetrics = hasDateRange
                ? await _reportService.GetAdvertiserTargetingReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.View,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds)
                : await _reportService.GetTotalAdvertiserTargetingReport(
                    ReportMetric.View,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds);

            var totalPaidMetrics = hasDateRange
                ? await _reportService.GetAdvertiserTargetingReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Paid,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds)
                : await _reportService.GetTotalAdvertiserTargetingReport(
                    ReportMetric.Paid,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds);

            var totalConversionMetrics = hasDateRange
                ? await _reportService.GetAdvertiserTargetingReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds)
                : await _reportService.GetTotalAdvertiserTargetingReport(
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.Campaign,
                    ReportTargetingDimension.Location,
                    campaignSubIds,
                    locationIds);

            var campaignData = new List<ExportData>();

            foreach (var campaign in campaigns)
            {
                var totalCost = totalPaidMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalCost?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var locationSubId);
                    CacheManager.Locations.TryGetValue(locationSubId, out var location);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.LocationId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()));
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            LocationId = item.Key,
                            Location = location?.ProvinceName ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()))
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalClicks = totalClickMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalClicks?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var locationSubId);
                    CacheManager.Locations.TryGetValue(locationSubId, out var location);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.LocationId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Clicks = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            LocationId = item.Key,
                            Location = location?.ProvinceName ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Clicks = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalViews = totalViewMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalViews?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var locationSubId);
                    CacheManager.Locations.TryGetValue(locationSubId, out var location);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.LocationId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Impressions = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            LocationId = item.Key,
                            Location = location?.ProvinceName ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Impressions = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalPurchase = totalConversionMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalPurchase?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var locationSubId);
                    CacheManager.Locations.TryGetValue(locationSubId, out var location);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.LocationId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Purchases = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            LocationId = item.Key,
                            Location = location?.ProvinceName ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Purchases = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });
            }

            campaignData.ForEach(x =>
            {
                x.CPC = (int) (x.Clicks != 0 ? x.TotalCost / x.Clicks : 0)!;
                x.CTR = x.Impressions != 0 ? x.Clicks / x.Impressions * 100 : 0;
                x.PurchaseConversion = x.Impressions != 0 ? x.Purchases / x.Impressions * 100 : 0;
                x.CPS = (int) (x.Purchases != 0 ? x.TotalCost / x.Purchases : 0)!;
            });

            campaignData = campaignData
                .Where(x => x.Clicks > 0 || x.Impressions > 0 || x.TotalCost > 0 || x.Purchases > 0)
                .OrderBy(x => x.Location)
                .ThenBy(x => x.CampaignName)
                .ToList();
            if (campaignData.Count == 0)
            {
                campaignData = new List<ExportData>
                {
                    new()
                    {
                        Location = "",
                        CampaignName = "",
                        Clicks = null,
                        Impressions = null,
                        TotalCost = null,
                        CPC = null,
                        CTR = null,
                        Purchases = null,
                        PurchaseConversion = null,
                        CPS = null
                    }
                };
            }

            return campaignData;
        }
        
        private async Task<List<ExportData>> FillReportHourData(
            ExportCampaignToExcelCommand request,
            List<Campaign> campaigns)
        {
            var now = DateTimeOffset.UtcNow;
            var firstDay = campaigns.Min(x => x.CreatedAt) ?? now;
            var campaignSubIds = campaigns.Select(x => (object) x.SubId).ToList();

            // total report
            var totalClickMetrics = await _reportService.GetAdvertiserReportByHour(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.Click,
                ReportAdvertiserDimension.Campaign,
                campaignSubIds);

            var totalViewMetrics = await _reportService.GetAdvertiserReportByHour(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.View,
                ReportAdvertiserDimension.Campaign,
                campaignSubIds);

            var totalPaidMetrics = await _reportService.GetAdvertiserReportByHour(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.Paid,
                ReportAdvertiserDimension.Campaign,
                campaignSubIds);

            var totalConversionMetrics = await _reportService.GetAdvertiserReportByHour(
                request.StartDate ?? firstDay,
                request.EndDate ?? now,
                ReportMetric.Conversion,
                ReportAdvertiserDimension.Campaign,
                campaignSubIds);

            var campaignData = new List<ExportData>();

            foreach (var campaign in campaigns)
            {
                var totalCost = totalPaidMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalCost?.ForEach(item =>
                {
                    var existItem = campaignData.Find(x => item.Key.Equals(x.Hour) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()));
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            Hour = item.Key,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()))
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalClicks = totalClickMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalClicks?.ForEach(item =>
                {
                    var existItem = campaignData.Find(x => item.Key.Equals(x.Hour) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Clicks = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            Hour = item.Key,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Clicks = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalViews = totalViewMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalViews?.ForEach(item =>
                {
                    var existItem = campaignData.Find(x => item.Key.Equals(x.Hour) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Impressions = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            Hour = item.Key,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Impressions = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalPurchase = totalConversionMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalPurchase?.ForEach(item =>
                {
                    var existItem = campaignData.Find(x => item.Key.Equals(x.Hour) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Purchases = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            Hour = item.Key,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Purchases = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });
            }

            campaignData.ForEach(x =>
            {
                x.Hour = $"{(int.TryParse(x.Hour, out var hour) ? hour : default):00}";
                x.CPC = (int) (x.Clicks != 0 ? x.TotalCost / x.Clicks : 0)!;
                x.CTR = x.Impressions != 0 ? x.Clicks / x.Impressions * 100 : 0;
                x.PurchaseConversion = x.Impressions != 0 ? x.Purchases / x.Impressions * 100 : 0;
                x.CPS = (int) (x.Purchases != 0 ? x.TotalCost / x.Purchases : 0)!;
            });

            campaignData = campaignData
                .OrderBy(x => x.CampaignName)
                .ThenBy(x => x.Hour)
                .ToList();

            return campaignData;
        }
        
        private async Task<List<ExportData>> FillReportWebsiteData(
            ExportCampaignToExcelCommand request,
            List<Campaign> campaigns)
        {
            var campaignSubIds = campaigns.Select(x => (object) x.SubId).ToList();
            var websiteIds = CacheManager.Websites.Select(x => (object) x.Value.SubId).ToList();

            var hasDateRange = HasDateRange(request);

            // total report
            var totalClickMetrics = hasDateRange
                ? await _reportService.GetAdvertiserPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Click,
                    ReportAdvertiserDimension.Campaign,
                    ReportPublisherDimension.Website,
                    campaignSubIds,
                    websiteIds)
                : await _reportService.GetTotalAdvertiserPublisherReport(
                    ReportMetric.Click,
                    ReportAdvertiserDimension.Campaign,
                    ReportPublisherDimension.Website,
                    campaignSubIds,
                    websiteIds);

            var totalViewMetrics = hasDateRange?await _reportService.GetAdvertiserPublisherReportByDay(
                request.StartDate!.Value,
                request.EndDate!.Value,
                ReportMetric.View,
                ReportAdvertiserDimension.Campaign,
                ReportPublisherDimension.Website,
                campaignSubIds,
                websiteIds)  : await _reportService.GetTotalAdvertiserPublisherReport(
                ReportMetric.View,
                ReportAdvertiserDimension.Campaign,
                ReportPublisherDimension.Website,
                campaignSubIds,
                websiteIds);

            var totalPaidMetrics = hasDateRange?await _reportService.GetAdvertiserPublisherReportByDay(
                request.StartDate!.Value,
                request.EndDate!.Value,
                ReportMetric.Paid,
                ReportAdvertiserDimension.Campaign,
                ReportPublisherDimension.Website,
                campaignSubIds,
                websiteIds)  : await _reportService.GetTotalAdvertiserPublisherReport(
                ReportMetric.Paid,
                ReportAdvertiserDimension.Campaign,
                ReportPublisherDimension.Website,
                campaignSubIds,
                websiteIds);

            var totalConversionMetrics = hasDateRange
                ? await _reportService.GetAdvertiserPublisherReportByDay(
                    request.StartDate!.Value,
                    request.EndDate!.Value,
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.Campaign,
                    ReportPublisherDimension.Website,
                    campaignSubIds,
                    websiteIds)
                : await _reportService.GetTotalAdvertiserPublisherReport(
                    ReportMetric.Conversion,
                    ReportAdvertiserDimension.Campaign,
                    ReportPublisherDimension.Website,
                    campaignSubIds,
                    websiteIds);

            var campaignData = new List<ExportData>();

            foreach (var campaign in campaigns)
            {
                var totalCost = totalPaidMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalCost?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var websiteSubId);
                    CacheManager.Websites.TryGetValue(websiteSubId, out var website);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.WebsiteId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()));
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            WebsiteId = item.Key,
                            Website = website?.Domain ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            TotalCost = Math.Round(Convert.ToDouble(item.Value.ToString()))
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalClicks = totalClickMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalClicks?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var websiteSubId);
                    CacheManager.Websites.TryGetValue(websiteSubId, out var website);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.WebsiteId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Clicks = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            WebsiteId = item.Key,
                            Website = website?.Domain ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Clicks = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalViews = totalViewMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalViews?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var websiteSubId);
                    CacheManager.Websites.TryGetValue(websiteSubId, out var website);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.WebsiteId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Impressions = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            WebsiteId = item.Key,
                            Website = website?.Domain ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Impressions = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });

                var totalPurchase = totalConversionMetrics?.GetValueOrDefault(campaign.SubId)?.ToList();
                totalPurchase?.ForEach(item =>
                {
                    _ = long.TryParse(item.Key, out var websiteSubId);
                    CacheManager.Websites.TryGetValue(websiteSubId, out var website);
                    var existItem = campaignData.Find(x => item.Key.Equals(x.WebsiteId) && x.Id.Equals(campaign.Id));
                    if (existItem is not null)
                    {
                        existItem.Purchases = Convert.ToDouble(item.Value.ToString());
                    }
                    else
                    {
                        var reportByDay = new ExportData
                        {
                            Id = campaign.Id,
                            WebsiteId = item.Key,
                            Website = website?.Domain ?? string.Empty,
                            CampaignName = campaign.Name,
                            CampaignId = campaign.Id,
                            Purchases = Convert.ToDouble(item.Value.ToString())
                        };
                        campaignData.Add(reportByDay);
                    }
                });
            }

            campaignData.ForEach(x =>
            {
                x.CPC = (int) (x.Clicks != 0 ? x.TotalCost / x.Clicks : 0)!;
                x.CTR = x.Impressions != 0 ? x.Clicks / x.Impressions * 100 : 0;
                x.PurchaseConversion = x.Impressions != 0 ? x.Purchases / x.Impressions * 100 : 0;
                x.CPS = (int) (x.Purchases != 0 ? x.TotalCost / x.Purchases : 0)!;
            }); 

            campaignData = campaignData
                .Where(x => x.Clicks > 0 || x.Impressions > 0 || x.TotalCost > 0 || x.Purchases > 0)
                .OrderBy(x => x.Website)
                .ThenBy(x => x.CampaignName)
                .ToList();
            
            if (campaignData.Count == 0)
            {
                campaignData = new List<ExportData>
                {
                    new()
                    {
                        Website = "",
                        CampaignName = "",
                        Clicks = null,
                        Impressions = null,
                        TotalCost = null,
                        CPC = null,
                        CTR = null,
                        Purchases = null,
                        PurchaseConversion = null,
                        CPS = null
                    }
                };
            }

            return campaignData;
        }

        private async Task<List<Campaign>> GetCampaignList(ExportCampaignToExcelCommand request, CancellationToken cancellationToken)
        {
            Expression<Func<Campaign, bool>> whereExpression = x => true;
            var sorts = request.Sorts != null ? request.Sorts?.Split(",") : new[] {"-ModifiedAt"};

            if (request.Filters != null)
            {
                whereExpression = whereExpression.And(ToCampaignExpression(request.Filters));
            }
            if (request.Status != null)
            {
                Enum.TryParse(request.Status.ToString(), out CampaignStatus campaignStatus);
                whereExpression = whereExpression.And(x => x.Status == campaignStatus);
            }

            whereExpression = whereExpression.And(x => request.CampaignIds!.Contains(x.Id));

            var campaigns = await _context.Campaigns
                .SortBy(sorts)
                .Where(whereExpression)
                .ToListAsync(cancellationToken);
            return campaigns;
        }

        private static Expression<Func<Advertising, bool>> ToExpression(List<PredicateModel> filters, bool cacheable)
        {
            Expression<Func<Advertising, bool>> whereExpression = x => true;
            foreach (var filter in filters)
            {
                if (filter.Field == "Ad.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x => !cacheable
                            ? EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : x.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x => !cacheable
                            ? !EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())
                            : !x.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.Equals => whereExpression.And(x => x.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x => x.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x => x.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Campaign.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            !cacheable
                                ? EF.Functions.Contains(x.AdvertisingSet!.Campaign!.Search,
                                    filter.Value.KeywordPretreatment())
                                : x.AdvertisingSet!.Campaign!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                            !cacheable
                                ? !EF.Functions.Contains(x.AdvertisingSet!.Campaign!.Search,
                                    filter.Value.KeywordPretreatment())
                                : !x.AdvertisingSet!.Campaign!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.Equals => whereExpression.And(x =>
                            x.AdvertisingSet!.Campaign!.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Campaign!.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Campaign!.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Adset.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            !cacheable
                                ? EF.Functions.Contains(x.AdvertisingSet!.Search, filter.Value.KeywordPretreatment())
                                : x.AdvertisingSet!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                            !cacheable
                                ? !EF.Functions.Contains(x.AdvertisingSet!.Search, filter.Value.KeywordPretreatment())
                                : !x.AdvertisingSet!.Search.FullTextContains(filter.Value)),
                        PredicateOperatorEnum.Equals =>
                            whereExpression.And(x => x.AdvertisingSet!.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x =>
                            x.AdvertisingSet!.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
            }

            return whereExpression;
        }
        
        private static Expression<Func<Campaign, bool>> ToCampaignExpression(List<PredicateModel> filters)
        {
            Expression<Func<Campaign, bool>> whereExpression = x => true;

            foreach (var filter in filters)
            {
                if (filter.Field == "Campaign.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())),
                        PredicateOperatorEnum.NotContain => whereExpression.And(x =>
                            !EF.Functions.Contains(x.Search, filter.Value.KeywordPretreatment())),
                        PredicateOperatorEnum.Equals => whereExpression.And(x => x.Search == filter.Value),
                        PredicateOperatorEnum.StartsWith => whereExpression.And(x => x.Search.StartsWith(filter.Value)),
                        PredicateOperatorEnum.EndsWith => whereExpression.And(x => x.Search.EndsWith(filter.Value)),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Adset.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x.AdvertisingSets!.Any(y =>
                                EF.Functions.Contains(y.Search, filter.Value.KeywordPretreatment()))),
                        _ => whereExpression
                    };
                }
                else if (filter.Field == "Ad.Name" && !string.IsNullOrEmpty(filter.Value))
                {
                    whereExpression = filter.Operator switch
                    {
                        PredicateOperatorEnum.Contains => whereExpression.And(x =>
                            x.Advertisings!.Any(
                                y => EF.Functions.Contains(y.Search, filter.Value.KeywordPretreatment()))),
                        _ => whereExpression
                    };
                }
            }

            return whereExpression;
        }
        
        private static bool HasDateRange(ExportCampaignToExcelCommand request)
        {
            return request.StartDate is not null && request.EndDate is not null;
        }
    }
}