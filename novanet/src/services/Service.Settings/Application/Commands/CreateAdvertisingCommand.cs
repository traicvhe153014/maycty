﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class CreateAdvertisingCommand : INovanetRequest<List<Advertising>>
{
    public string Name { get; set; } = default!;
    public Guid AdvertisingSetId { get; set; }
    public string? RedirectLink { get; set; }
    public List<TemplateConfiguration> TemplateConfigurations { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<CreateAdvertisingCommand, List<Advertising>>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _messageBusClient = messageBusClient;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<List<Advertising>> HandleAsync(CreateAdvertisingCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var advertisings = new List<Advertising>();
            var templates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);
            var templateAttributes = await _context.AdvertisingTemplateAttributes.ToListAsync(cancellationToken);
            var appliedTemplates = new List<AdvertisingAppliedTemplate>();
            foreach (var configuration in request.TemplateConfigurations)
            {
                var appliedTemplate = new AdvertisingAppliedTemplate
                {
                    AdvertisingTemplateId =
                        templates.FirstOrDefault(x => x.TemplateType == configuration.TemplateType)?.Id,
                    AdvertisingTemplateAttributeValues = configuration.Configurations.Select(x =>
                    {
                        var attribute = templateAttributes.FirstOrDefault(attribute => attribute.Name == x.Key);
                        var value = new AdvertisingTemplateAttributeValue
                        {
                            AdvertisingTemplateAttributeId = attribute?.Id,
                        };
                        value.SetValue(attribute?.DataType, x.Value);
                        return value;
                    }).ToList()
                };
                appliedTemplates.Add(appliedTemplate);
            }

            var advertisingSet =
                await _context.AdvertisingSets.FirstOrDefaultAsync(x => x.Id == request.AdvertisingSetId,
                    cancellationToken);

            foreach (var advertisingAppliedTemplate in appliedTemplates)
            {
                var advertisingTemplateItems = new List<AdvertisingAppliedTemplate> {advertisingAppliedTemplate};
                var advertising = new Advertising
                {
                    Name = request.Name,
                    AdvertisingSetId = request.AdvertisingSetId,
                    RedirectLink = request.RedirectLink,
                    AdvertisingAppliedTemplates = advertisingTemplateItems,
                    CampaignId = advertisingSet?.CampaignId,
                    CreatedBy = currentUserId,
                    ModifiedBy = currentUserId,
                    Status = AdvertisingStatus.Running,
                    IsActive = true
                };
                advertisings.Add(advertising);
                await _context.Advertising.AddAsync(advertising, cancellationToken);
            }

            // update adset, campaign status if it's error because of not having any ad
            if (advertisingSet is not null)
            {
                var campaign = await _context.Campaigns
                    .FirstOrDefaultAsync(x => x.Id == advertisingSet.CampaignId, cancellationToken);
                if (campaign?.Status == CampaignStatus.Unfinished)
                {
                    var startDate = DateOnly.FromDateTime(campaign.EcommerceStartDate.DateTime);
                    var today = DateOnly.FromDateTime(DateTime.Now);
                    campaign.Status = !campaign.IsActive
                        ? CampaignStatus.Paused
                        : startDate > today
                            ? CampaignStatus.NotStarted
                            : CampaignStatus.Running;
                    advertisingSet.Status = advertisingSet.IsActive
                        ? AdvertisingSetStatus.Running
                        : AdvertisingSetStatus.Paused;
                    _context.Campaigns.Update(campaign);
                    _context.AdvertisingSets.Update(advertisingSet);
                    await UpdateCampaignCache(campaign);
                    await UpdateAdvertisingSetsCache(advertisingSet);
                }
                else if (campaign?.Status != CampaignStatus.Error && campaign?.Status != CampaignStatus.Finished)
                {
                    var startDate = DateOnly.FromDateTime(campaign!.EcommerceStartDate.DateTime);
                    var today = DateOnly.FromDateTime(DateTime.Now);
   
                    campaign.Status = !campaign.IsActive
                        ? CampaignStatus.Paused
                        : startDate > today
                            ? CampaignStatus.NotStarted
                            : CampaignStatus.Running;
                    advertisingSet.Status = advertisingSet.IsActive
                        ? AdvertisingSetStatus.Running
                        : AdvertisingSetStatus.Paused;
                    _context.Campaigns.Update(campaign);
                    _context.AdvertisingSets.UpdateRange(advertisingSet);
                    await UpdateCampaignCache(campaign);
                    await UpdateAdvertisingSetsCache(advertisingSet);
                }

                else if(campaign.Status == CampaignStatus.Finished)
                {
                    var startDate = DateOnly.FromDateTime(campaign.EcommerceStartDate.DateTime);
                    var today = DateOnly.FromDateTime(DateTime.Now);
                    advertisingSet.Status = advertisingSet.IsActive
                        ? AdvertisingSetStatus.Running
                        : AdvertisingSetStatus.Paused;
                    _context.AdvertisingSets.Update(advertisingSet);
                    await UpdateAdvertisingSetsCache(advertisingSet);
                }
            }

            await _context.SaveChangesAsync(cancellationToken);
            
            // set advertising cache
            foreach (var advertising in advertisings)
            {
                advertising.AdvertisingAppliedTemplateCores = advertising.AdvertisingAppliedTemplates?.Select(x => new AdvertisingAppliedTemplateCore
                {
                    AdvertisingId = x.AdvertisingId,
                    Id = x.Id,
                    SubId = x.SubId,
                    AdvertisingTemplateId = x.AdvertisingTemplateId,
                    AdvertisingTemplateAttributeValueCores = x.AdvertisingTemplateAttributeValues?.Cast<AdvertisingTemplateAttributeValueCore>().ToList() ?? default!
                }).ToList() ?? default!;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingCore>(advertising.SubId), advertising);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingCore>(), new PatchCacheManagerSignal<AdvertisingCore>
                {
                    Entity = advertising,
                    EntitySubId = advertising.SubId
                });   
            }

            return advertisings;
        }
        
        private async Task UpdateCampaignCache(Campaign? campaign)
        {
            if (campaign is null)
            {
                return;
            }
            CacheManager.Campaigns.TryGetValue(campaign.SubId, out var currentCampaign);
            if (currentCampaign is null)
            {
                return;
            }

            currentCampaign.Status = campaign.Status;
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<CampaignCore>(currentCampaign.SubId), currentCampaign);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<CampaignCore>(), new PatchCacheManagerSignal<CampaignCore>
            {
                Entity = currentCampaign,
                EntitySubId = currentCampaign.SubId
            });
        }
        
        private async Task UpdateAdvertisingSetsCache(AdvertisingSet advertisingSet)
        {
            CacheManager.AdvertisingSets.TryGetValue(advertisingSet.SubId, out var currentAdvertisingSet);
            if (currentAdvertisingSet is not null)
            {
                currentAdvertisingSet.Status = advertisingSet.Status;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetCore>(currentAdvertisingSet.SubId), currentAdvertisingSet);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(), new PatchCacheManagerSignal<AdvertisingSetCore>
                {
                    Entity = currentAdvertisingSet,
                    EntitySubId = currentAdvertisingSet.SubId
                });
            }
        }
    }
}

public class TemplateConfiguration
{
    public TemplateType TemplateType { get; set; }
    public Dictionary<string, object> Configurations { get; set; } = default!;
}