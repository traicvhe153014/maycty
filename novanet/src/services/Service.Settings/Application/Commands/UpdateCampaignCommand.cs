﻿using System.Linq.Expressions;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessServices;

namespace Service.Settings.Application.Commands;

public class UpdateCampaignCommand : INovanetRequest<Campaign>
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    public bool? IsActive { get; set; }
    public bool IsEcommerceUpdating { get; set; }
    public double? EcommerceAmountSpent { get; set; }
    public double? EcommerceDailyBudget { get; set; }
    public DateTimeOffset? EcommerceStartDate { get; set; }
    public DateTimeOffset? EcommerceEndDate { get; set; }
    public bool? EcommerceScheduled { get; set; }
    public bool? EcommerceScheduledAllTime { get; set; }
    public List<CampaignScheduling>? Schedulings { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateCampaignCommand, Campaign>
    {
        private readonly SettingsContext _context;
        private readonly IReportService _reportService;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IReportService reportService, IMessageBusClient messageBusClient,
            IGlobalCacheService globalCacheService) : base(logger, httpContextAccessor)
        {
            _context = context;
            _reportService = reportService;
            _messageBusClient = messageBusClient;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<Campaign> HandleAsync(UpdateCampaignCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var campaign = await _context.Campaigns
                .Include(x => x.CampaignScheduling)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (campaign == null)
            {
                throw new BadRequestException("Chiến dịch không tồn tại");
            }

            var paid = await _reportService.GetSingleAdvertiserReport(ReportMetric.Paid,
                ReportAdvertiserDimension.Campaign,
                campaign.SubId);
            // validate
            if (request.EcommerceStartDate < DateTimeOffset.UtcNow && request.EcommerceAmountSpent < paid)
            {
                throw new BadRequestException(
                    "Ngân sách tổng không được chỉnh sửa nhỏ hơn tổng chi phí chạy quảng cáo");
            }

            Expression<Func<AdvertisingSet, bool>> whereIsAdminAdvertisingSetsExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminAdvertisingSetsExpression = x => x.CreatedBy.Equals(currentUserId);
            }

            var totalAdvertisingSet = await _context.AdvertisingSets
                .Where(whereIsAdminAdvertisingSetsExpression)
                .Where(x => x.IsActive.Equals(true) && x.CampaignId.Equals(campaign.Id) &&
                            !x.Status.Equals(AdvertisingSetStatus.Paused) &&
                            !x.Status.Equals(AdvertisingSetStatus.Error))
                .CountAsync(cancellationToken);

            var advertisings = CacheManager.Advertising.Values
                .Count(x =>
                    x.CampaignId.Equals(campaign.Id));

            // update status
            if (campaign.Status.Equals(CampaignStatus.Error))
            {
                campaign.Status = CampaignStatus.Error;
                if (request.IsActive is not null)
                {
                    campaign.IsActive = request.IsActive.Value;
                }
            }
            else
            {
                if (!request.IsActive.Equals(null))
                {
                    if (campaign.IsActive != request.IsActive)
                    {
                        if (advertisings.Equals(0))
                        {
                            if (!campaign.Status.Equals(CampaignStatus.Error))
                            {
                                campaign.Status = CampaignStatus.Unfinished;
                            }
                        }
                        else if (campaign.Status != CampaignStatus.Error &&
                                 campaign.Status != CampaignStatus.Finished &&
                                 campaign.Status != CampaignStatus.Unfinished)
                        {
                            var campaignStartDate = DateOnly.FromDateTime(campaign.EcommerceStartDate.DateTime);
                            var today = DateOnly.FromDateTime(DateTime.Now);

                            if (campaign.EcommerceEndDate != null &&
                                today > DateOnly.FromDateTime(campaign.EcommerceEndDate.Value.DateTime))
                            {
                                campaign.Status = CampaignStatus.Finished;
                            }
                            else
                            {
                                campaign.Status = !request.IsActive.Value ? CampaignStatus.Paused
                                    : campaignStartDate > today ? CampaignStatus.NotStarted
                                    : !totalAdvertisingSet.Equals(0) ? CampaignStatus.Running
                                    : CampaignStatus.Paused;
                            }
                        }

                        campaign.IsActive = request.IsActive.Value;
                    }
                }
                else
                {
                    if (advertisings.Equals(0))
                    {
                        if (!campaign.Status.Equals(CampaignStatus.Error))
                        {
                            campaign.Status = CampaignStatus.Unfinished;
                        }
                    }
                    else if (!totalAdvertisingSet.Equals(0))
                    {
                        if (request.EcommerceEndDate != null)
                        {
                            var requestEndDate = DateOnly.FromDateTime(request.EcommerceEndDate.Value.DateTime);
                            var campaignStartDate = DateOnly.FromDateTime(request.EcommerceStartDate?.DateTime ??
                                                                          campaign.EcommerceStartDate.DateTime);

                            var today = DateOnly.FromDateTime(DateTime.Now);

                            if (request.EcommerceStartDate != null)
                            {
                                var requestStartDate = DateOnly.FromDateTime(request.EcommerceStartDate.Value.DateTime);
                                if (today.Equals(requestStartDate) && campaign.Status != CampaignStatus.Unfinished)
                                {
                                    campaign.Status = CampaignStatus.Running;
                                    campaign.IsActive = true;
                                }
                            }
                            else
                            {
                                if (campaign.EcommerceEndDate != null)
                                {
                                    var campaignEndDate =
                                        DateOnly.FromDateTime(campaign.EcommerceEndDate.Value.DateTime);

                                    if (campaign.Status == CampaignStatus.Finished)
                                    {
                                        campaign.Status = requestEndDate > campaignEndDate
                                            ? CampaignStatus.Running
                                            : campaignEndDate < today
                                                ? CampaignStatus.Finished
                                                : CampaignStatus.Running;
                                        if (campaign.Status != CampaignStatus.Error &&
                                            campaign.Status != CampaignStatus.Finished)
                                        {
                                            campaign.IsActive = true;
                                        }
                                    }
                                    else if (campaign.Status != CampaignStatus.Error &&
                                             campaign.Status != CampaignStatus.Unfinished)
                                    {
                                        campaign.Status = today >= campaignStartDate
                                            ? CampaignStatus.Running
                                            : campaign.Status;
                                    }
                                }
                            }
                        }
                        else if (campaign.EcommerceEndDate is null && campaign.Status == CampaignStatus.Finished)
                        {
                            campaign.Status = CampaignStatus.Running;
                            campaign.IsActive = true;
                        }
                        else if (request.EcommerceStartDate is not null)
                        {
                            var requestStartDate = DateOnly.FromDateTime(request.EcommerceStartDate.Value.DateTime);
                            var today = DateOnly.FromDateTime(DateTime.Now);
                            if (today.Equals(requestStartDate) && !campaign.Status.Equals(CampaignStatus.Unfinished))
                            {
                                campaign.Status = CampaignStatus.Running;
                                campaign.IsActive = true;
                            }
                            else if (campaign.EcommerceEndDate is null && campaign.Status == CampaignStatus.Finished)
                            {
                                campaign.Status = CampaignStatus.Running;
                                campaign.IsActive = true;
                            }
                        }
                    }
                    else
                    {
                        campaign.Status = CampaignStatus.Paused;
                    }
                }
            }

            if (request.EcommerceEndDate < campaign.EcommerceEndDate)
            {
                throw new BadRequestException("Không được sửa ngày kết thúc nhỏ hơn ngày kết thúc ban đầu");
            }

            campaign.Name = request.Name;
            campaign.ModifiedBy = currentUserId;
            campaign.ModifiedAt = DateTimeOffset.Now;
            if (request.IsEcommerceUpdating)
            {
                _context.CampaignScheduling.RemoveRange(campaign.CampaignScheduling ?? new List<CampaignScheduling>());

                campaign.EcommerceAmountSpent = request.EcommerceAmountSpent!.Value;
                campaign.EcommerceDailyBudget = request.EcommerceDailyBudget!.Value;
                campaign.EcommerceScheduled = request.EcommerceScheduled;
                campaign.EcommerceScheduledAllTime = request.EcommerceScheduledAllTime!.Value;
                campaign.EcommerceEndDate = request.EcommerceEndDate;

                if (campaign.EcommerceStartDate > DateTimeOffset.UtcNow)
                {
                    campaign.EcommerceStartDate = request.EcommerceStartDate!.Value;
                }

                if (campaign.EcommerceEndDate is not null && campaign.EcommerceEndDate > DateTimeOffset.UtcNow &&
                    campaign.Status == CampaignStatus.Finished)
                {
                    campaign.Status = campaign.IsActive ? CampaignStatus.Running : CampaignStatus.Paused;
                }

                if (campaign.CampaignScheduling is not null)
                {
                    var updatedSchedulings = request.Schedulings?.Select(x => new CampaignScheduling
                    {
                        Timing = x.Timing,
                        CampaignId = campaign.Id,
                        DayOfWeek = x.DayOfWeek
                    }).ToList() ?? new List<CampaignScheduling>();
                    await _context.CampaignScheduling.AddRangeAsync(updatedSchedulings, cancellationToken);
                }
            }

            _context.Campaigns.Update(campaign);
            await _context.SaveChangesAsync(cancellationToken);

            var updatedCampaign = await _context.Campaigns
                .AsNoTracking()
                .Include(x => x.CampaignScheduling)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (updatedCampaign is not null)
            {
                updatedCampaign.SchedulingCore = updatedCampaign.CampaignScheduling?.Select(x =>
                    new CampaignSchedulingCore
                    {
                        Id = x.Id,
                        Timing = x.Timing,
                        CampaignId = x.CampaignId,
                        SubId = x.SubId,
                        DayOfWeek = x.DayOfWeek
                    }).ToList() ?? new List<CampaignSchedulingCore>();
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<CampaignCore>(updatedCampaign.SubId),
                    updatedCampaign);

                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<CampaignCore>(),
                    new PatchCacheManagerSignal<CampaignCore>
                    {
                        Entity = updatedCampaign,
                        EntitySubId = updatedCampaign.SubId,
                    });
            }

            return campaign;
        }
    }

    public class UpdateCampaignCommandValidator : AbstractValidator<UpdateCampaignCommand>
    {
        public UpdateCampaignCommandValidator()
        {
            RuleFor(x => x.EcommerceAmountSpent)
                .NotNull()
                .When(x => x.IsEcommerceUpdating);

            RuleFor(x => x.EcommerceScheduledAllTime)
                .NotNull()
                .When(x => x.IsEcommerceUpdating);

            RuleFor(x => x.EcommerceDailyBudget)
                .NotNull()
                .When(x => x.IsEcommerceUpdating);

            RuleFor(x => x.EcommerceScheduled)
                .NotNull()
                .When(x => x.IsEcommerceUpdating);
        }
    }
}