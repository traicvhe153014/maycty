﻿using System.Linq.Expressions;
using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateAdvertisingSetProductGroupCommand : INovanetRequest<UpdateAdvertisingSetProductGroup>
{
    public Guid AdvertisingSetId { get; set; }
    public Guid ProductGroupId { get; set; }

    public bool IsActive { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateAdvertisingSetProductGroupCommand, UpdateAdvertisingSetProductGroup>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            IServiceScopeFactory serviceScopeFactory,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _serviceScopeFactory = serviceScopeFactory;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<UpdateAdvertisingSetProductGroup> HandleAsync(
            UpdateAdvertisingSetProductGroupCommand request, CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var item = await _context.AdvertisingSetProductGroups
                .Include(x => x.ProductGroup)
                .FirstOrDefaultAsync(x =>
                        x.AdvertisingSetId == request.AdvertisingSetId && x.ProductGroupId == request.ProductGroupId,
                    cancellationToken);
            if (item == null) throw new BadRequestException("Nhóm sản phẩm không tồn tại");
            item.IsActive = request.IsActive;
            
            if (!item.Status.Equals(AdvertisingSetStatus.Error))
            {
                item.Status = item.IsActive ? AdvertisingSetStatus.Running : AdvertisingSetStatus.Paused;
            }

            var advertisingSet = await _context.AdvertisingSets
                .Include(x => x.Advertisings)
                .Where(x => x.Id.Equals(request.AdvertisingSetId))
                .FirstOrDefaultAsync(cancellationToken);
            
            Expression<Func<Advertising, bool>> whereIsAdminExpression = x => true;
            if (!UserClaimsValue.IsAdmin)
            {
                whereIsAdminExpression = x => x.CreatedBy.Equals(currentUserId);
            }
                
            var totalAdvertising = await _context.Advertising
                .Where(whereIsAdminExpression)
                .Where(x => x.IsActive.Equals(true) && x.AdvertisingSetId.Equals(advertisingSet!.Id))
                .CountAsync(cancellationToken);

            var advertisingSetProductGroups = await _context.AdvertisingSetProductGroups
                .Include(x => x.ProductGroup)
                .ThenInclude(x => x!.ProductGroupEntities)!
                .ThenInclude(x => x.ProductEntity)
                .Where(x => x.AdvertisingSetId.Equals(request.AdvertisingSetId))
                .ToListAsync(cancellationToken);

            var runningProductGroups =
                advertisingSetProductGroups.Where(x => x.Status.Equals(AdvertisingSetStatus.Running)).ToList();

            if (advertisingSet != null && !advertisingSet.Status.Equals(AdvertisingSetStatus.Error))
            {
                if (runningProductGroups.Count.Equals(0))
                {
                    if (advertisingSet.Status != AdvertisingSetStatus.Unfinished) advertisingSet.Status = AdvertisingSetStatus.Paused;
                }
                else
                {
                    advertisingSet.Status = advertisingSet.Advertisings is {Count: 0} ? AdvertisingSetStatus.Unfinished : advertisingSet.IsActive && !totalAdvertising.Equals(0) ? AdvertisingSetStatus.Running : AdvertisingSetStatus.Paused;
                }   
            }

            item.ModifiedBy = currentUserId;
            var response = new UpdateAdvertisingSetProductGroup
            {
                AdvertisingSetId = item.AdvertisingSetId,
                IsActive = item.IsActive,
                ProductGroupId = item.ProductGroupId,
                Status = item.Status,
                AdvertisingSetStatus = advertisingSet?.Status
            };
            
            _context.AdvertisingSetProductGroups.Update(item);
            _context.AdvertisingSets.Update(advertisingSet);
            
            await _context.SaveChangesAsync(cancellationToken);
            
            // update cache
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetProductGroupCore>(item.SubId), item);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetProductGroupCore>(),
                new PatchCacheManagerSignal<AdvertisingSetProductGroupCore>
                {
                    Entity = item,
                    EntitySubId = item.SubId
                });

            CacheManager.AdvertisingSets.TryGetValue(advertisingSet.SubId, out var cachedAdvertisingSet);
            if (cachedAdvertisingSet is not null)
            {
                cachedAdvertisingSet.Status = advertisingSet.Status;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetCore>(cachedAdvertisingSet.SubId), cachedAdvertisingSet);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(),
                    new PatchCacheManagerSignal<AdvertisingSetCore>
                    {
                        Entity = cachedAdvertisingSet,
                        EntitySubId = cachedAdvertisingSet.SubId
                    });
            }
            
            using var scope = _serviceScopeFactory.CreateScope();

            var updateStatusService = scope.ServiceProvider.GetRequiredService<IUpdateStatusService>();
            
            await updateStatusService.UpdateCampaignStatus(item.ProductGroup?.ProductFeedId);

            return response;
        }
    }
}