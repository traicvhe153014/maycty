﻿using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class UpdateObjectGroupCommand : INovanetRequest<UpdateObjectGroupResponse>, IMapFrom<ProductGroup>
{
    public Guid Id { get; set; } = default!;

    public string Name { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<UpdateObjectGroupCommand, UpdateObjectGroupResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(
            ILogger<Handler> logger,
            SettingsContext context,
            IHttpContextAccessor httpContextAccessor, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) :
            base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<UpdateObjectGroupResponse> HandleAsync(UpdateObjectGroupCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var objectGroup = _context.ObjectGroups
                .FirstOrDefault(x => x.Id.Equals(request.Id));

            if (objectGroup == null) return new UpdateObjectGroupResponse(objectGroup!.Id, objectGroup.Name);
            objectGroup.Update(request.Name);
            objectGroup.UpdateModifiedAt();
            objectGroup.ModifiedBy = currentUserId;
            
            _context.ObjectGroups.Update(objectGroup);
            await _context.SaveChangesAsync(cancellationToken);

            // update cache
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingCore>(objectGroup.SubId), objectGroup);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<ObjectGroupCore>(), new PatchCacheManagerSignal<ObjectGroupCore>
            {
                Entity = objectGroup,
                EntitySubId = objectGroup.SubId
            });

            return new UpdateObjectGroupResponse(objectGroup.Id, objectGroup.Name);
        }
    }
}