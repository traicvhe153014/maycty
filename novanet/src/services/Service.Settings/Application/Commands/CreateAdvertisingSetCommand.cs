﻿using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class CreateAdvertisingSetCommand : INovanetRequest<AdvertisingSet>
{
    public string Name { get; set; } = default!;
    public Guid CampaignId { get; set; }

    public bool ApplyAllProductGroups { get; set; }
    public List<Guid> ProductGroupIds { get; set; } = default!;
    public bool HasProhibitedCategories { get; set; }
    public List<Guid> ProhibitedCategoryIds { get; set; } = default!;
    public bool HasObject { get; set; }
    public List<Guid> ObjectIds { get; set; } = default!;
    public List<Guid> LocationIds { get; set; } = default!;

    public List<Guid> CategoryIds { get; set; } = default;
    public List<Guid> GenderIds { get; set; } = default!;
    public List<Guid> AgeIds { get; set; } = default!;

    public List<Guid>? SelectedWebsites { get; set; }

    public List<Guid>? ExcludedWebsites { get; set; }

    public bool? Uniformed { get; set; }

    public double? UniformedPrice { get; set; }

    public UniformedUnit? UniformedUnit { get; set; }

    public RemarketingType? RemarketingType { get; set; }

    public int? AdImpressions { get; set; }

    public int? RemarketingTime { get; set; }

    public int? TimeOnDisplay { get; set; }

    public bool? TargetingMarketingByProduct { get; set; }

    public bool? ClickedAdvertising { get; set; }

    public bool? Viewer { get; set; }

    public int? ViewedMax { get; set; }

    public bool? ExcludedWebsite { get; set; }

    public List<AdvertisingSetScheduling>? Schedulings { get; set; }

    public bool? ScheduledAllTime { get; set; }

    internal class Handler : NovanetRequestHandler<CreateAdvertisingSetCommand, AdvertisingSet>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<AdvertisingSet> HandleAsync(CreateAdvertisingSetCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var websites = request.ExcludedWebsites
                ?.Select(requestExcludedWebsite => new AdvertisingSetWebsite
                    {WebsiteId = requestExcludedWebsite, WebsiteCondition = WebsiteCondition.Exclude})
                .ToList() ?? new List<AdvertisingSetWebsite>();
            websites.AddRange(request.SelectedWebsites?.Select(requestSelectedWebsite => new AdvertisingSetWebsite
            {
                WebsiteId = requestSelectedWebsite,
                WebsiteCondition = WebsiteCondition.Include,
            }) ?? new List<AdvertisingSetWebsite>());

            var advertisingSetStatus = AdvertisingSetStatus.Unfinished;
            var campaign = await _context.Campaigns
                .FirstOrDefaultAsync(x => x.Id == request.CampaignId, cancellationToken);
            if (campaign?.CampaignType == CampaignType.Ecommerce)
            {
                campaign = await _context.Campaigns
                    .Include(x => x.EcommerceProductFeed)
                    .ThenInclude(x => x!.ProductEntities)
                    .FirstOrDefaultAsync(x => x.Id == request.CampaignId, cancellationToken);
                if (campaign?.EcommerceProductFeed?.Deleted == true ||
                    campaign?.EcommerceProductFeed?.Permission is ProductFeedPermission.CannotAccess or ProductFeedPermission.CannotParse ||
                    (campaign?.EcommerceProductFeed?.ProductEntities.All(x => x.Status is ProductEntityStatus.OutOfStock or ProductEntityStatus.Deactivated) ?? false))
                {
                    advertisingSetStatus = AdvertisingSetStatus.Error;
                }
            }

            var advertisingSet = new AdvertisingSet
            {
                Name = request.Name,
                Status = advertisingSetStatus,
                IsActive = true,
                CampaignId = request.CampaignId,
                HasProhibitedCategories = request.HasProhibitedCategories,
                HasObject = request.HasObject,
                AdvertisingSetProductGroups = request.ProductGroupIds.Select(x => new AdvertisingSetProductGroup
                {
                    ProductGroupId = x,
                    Status = AdvertisingSetStatus.Running,
                    IsActive = true
                }).ToList(),
                AdvertisingSetAges = request.AgeIds.Select(x => new AdvertisingSetAge {AgeId = x}).ToList(),
                AdvertisingSetGenders = request.GenderIds.Select(x => new AdvertisingSetGender {GenderId = x}).ToList(),
                AdvertisingSetLocations = request.LocationIds.Select(x => new AdvertisingSetLocation {LocationId = x})
                    .ToList(),
                AdvertisingSetCategories = request.CategoryIds.Select(x => new AdvertisingSetCategory{CategoryId = x}).ToList(),
                AdvertisingSetProhibitedCategories = request.ProhibitedCategoryIds
                    .Select(x => new AdvertisingSetProhibitedCategory {ProhibitedCategoryId = x}).ToList(),
                AdvertisingSetObjects = request.ObjectIds.Select(x => new AdvertisingSetObject {ObjectId = x}).ToList(),
                Uniformed = request.Uniformed,
                UniformedPrice = request.UniformedPrice,
                UniformedUnit = request.UniformedUnit,
                RemarketingType = request.RemarketingType,
                AdImpressions = request.AdImpressions,
                RemarketingTime = request.RemarketingTime,
                TimeOnDisplay = request.TimeOnDisplay,
                TargetingMarketingByProduct = request.TargetingMarketingByProduct,
                ClickedAdvertising = request.ClickedAdvertising,
                Viewer = request.Viewer,
                ViewedMax = request.ViewedMax,
                AdvertisingSetWebsites = websites,
                ApplyAllProductGroups = request.ApplyAllProductGroups,
                ExcludedWebsite = request.ExcludedWebsite,
                ScheduledAllTime = request.ScheduledAllTime ?? default,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
            };

            var productGroupIds = advertisingSet.AdvertisingSetProductGroups.Select(x => x.ProductGroupId).ToList();
            
            var productGroupEntities = await _context.ProductGroupEntities
                .AsNoTracking()
                .Include(x => x.ProductEntity)
                .Include(x => x.ProductEntity!.ProductFeed)
                .Include(x => x.ProductEntity!.AttributeValues)!
                .ThenInclude(x => x.ProductAttribute)
                .Where(x => productGroupIds.Contains(x.ProductGroupId))
                .Where(x => x.ProductEntity != null && x.ProductEntity.AttributeValues!
                    .Where(y => !y.StringValue!.ToLower().Equals("hết hàng"))
                    .Select(y => y.AttributeId)
                    .Any(z => _context.ProductAttributes
                        .Where(n => n.Name.Equals("Availability"))
                        .Select(m => new {m.Id, m.DataType, m.Name})
                        .Select(t => t.Id).Contains(z)))
                .AsSplitQuery()
                .ToListAsync(cancellationToken);
            
            foreach (var advertisingSetAdvertisingSetProductGroup in advertisingSet.AdvertisingSetProductGroups)
            {
                var productsValid = productGroupEntities.Count(x =>
                    x.ProductGroupId.Equals(advertisingSetAdvertisingSetProductGroup.ProductGroupId));
                if (productsValid.Equals(0))
                {
                    advertisingSetAdvertisingSetProductGroup.Status = AdvertisingSetStatus.Error;
                }
            }
            if (request.HasProhibitedCategories)
            {
                advertisingSet.AdvertisingSetProhibitedCategories = request.ProhibitedCategoryIds
                    .Select(x => new AdvertisingSetProhibitedCategory {ProhibitedCategoryId = x}).ToList();
            }

            if (request.HasObject)
            {
                advertisingSet.AdvertisingSetObjects = request.ObjectIds
                    .Select(x => new AdvertisingSetObject {ObjectId = x}).ToList();

                foreach (var requestObjectId in request.ObjectIds)
                {
                    var getObject = await _context.ObjectGroups
                        .Where(x => x.Id.Equals(requestObjectId))
                        .FirstOrDefaultAsync(cancellationToken);

                    if (getObject is {ConnectionStatus: false}) {
                        getObject.ConnectionStatus = true;
                        _context.ObjectGroups.Update(getObject);
                    }
                }
            }

            if (campaign?.EcommerceScheduled == false)
            {
                advertisingSet.AdvertisingSetScheduling = request.Schedulings;
            }

            var entityEntry = await _context.AdvertisingSets.AddAsync(advertisingSet, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            // update cache
            entityEntry.Entity.LocationCores =
                entityEntry.Entity.AdvertisingSetLocations?.Cast<AdvertisingSetLocationCore>().ToList() ??
                new List<AdvertisingSetLocationCore>();
            entityEntry.Entity.CategoryCores =
                entityEntry.Entity.AdvertisingSetCategories?.Cast<AdvertisingSetCategoryCore>().ToList() ??
                new List<AdvertisingSetCategoryCore>();
            entityEntry.Entity.AdvertisingSetWebsiteCores =
                entityEntry.Entity.AdvertisingSetWebsites?.Cast<AdvertisingSetWebsiteCore>().ToList() ??
                new List<AdvertisingSetWebsiteCore>();
            entityEntry.Entity.SchedulingCore =
                entityEntry.Entity.AdvertisingSetScheduling?.Cast<AdvertisingSetSchedulingCore>().ToList() ??
                new List<AdvertisingSetSchedulingCore>();
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetCore>(entityEntry.Entity.SubId), entityEntry.Entity);
            _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(), new PatchCacheManagerSignal<AdvertisingSetCore>
            {
                Entity = entityEntry.Entity,
                EntitySubId = entityEntry.Entity.SubId,
            });

            foreach (var advertisingSetProductGroup in entityEntry.Entity.AdvertisingSetProductGroups!)
            {
                await _globalCacheService.SetAsync(
                    RedisKeys.KeySettings<AdvertisingSetProductGroupCore>(advertisingSetProductGroup.SubId),
                    advertisingSetProductGroup);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetProductGroupCore>(), new PatchCacheManagerSignal<AdvertisingSetProductGroupCore>
                {
                    Entity = advertisingSetProductGroup,
                    EntitySubId = advertisingSetProductGroup.SubId,
                });
            }

            foreach (var advertisingSetObject in entityEntry.Entity.AdvertisingSetObjects!)
            {
                await _globalCacheService.SetAsync(
                    RedisKeys.KeySettings<AdvertisingSetObjectCore>(advertisingSetObject.SubId),
                    advertisingSetObject);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetObjectCore>(), new PatchCacheManagerSignal<AdvertisingSetObjectCore>
                {
                    Entity = advertisingSetObject,
                    EntitySubId = advertisingSetObject.SubId,
                });
            }
            
            return advertisingSet;
        }
    }
}