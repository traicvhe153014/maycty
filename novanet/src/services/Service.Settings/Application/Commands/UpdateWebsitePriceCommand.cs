﻿using Novanet.Core.Exceptions;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Application.Commands;

public class UpdateWebsitePriceCommand : INovanetRequest<UpdateWebsitePriceResponse>
{
    public Guid Id { get; set; }

    public bool? IsActive { get; set; }
    
    public bool IsFullUpdate { get; set; }

    public DateTimeOffset? StartDate { get; set; }

    public List<TemplatePrice>? TemplatePrices { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<UpdateWebsitePriceCommand, UpdateWebsitePriceResponse>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<UpdateWebsitePriceResponse> HandleAsync(UpdateWebsitePriceCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var websitePrice = await _context.WebsitePrices
                .FirstOrDefaultAsync(x => x.Id.Equals(request.Id), cancellationToken);
            if (websitePrice is null) throw new NullReferenceException("Không tìm thấy thông tin chi phí của Website");
            if (websitePrice.Status != WebsitePriceStatus.NotStarted && request.IsFullUpdate)
            {
                throw new BadRequestException("Không thể chỉnh sửa bảng giá đang chạy");
            }
            websitePrice.ModifiedBy = currentUserId;
            websitePrice.ModifiedAt = DateTimeOffset.Now;
            if (request.IsActive is not null)
            {
                websitePrice.IsActive = request.IsActive.Value;
                websitePrice.Status = DateTimeOffset.Now < websitePrice.StartDate ? WebsitePriceStatus.NotStarted :
                    DateTimeOffset.Now > websitePrice.EndDate ? WebsitePriceStatus.Archived :
                    request.IsActive.Value ? WebsitePriceStatus.Running : WebsitePriceStatus.Paused;
            }

            var newWebsiteTemplatePrices = new List<WebsiteTemplatePrice>();
            if (request.IsFullUpdate)
            {
                if (request.StartDate is not null)
                {
                    request.StartDate = new DateTimeOffset(request.StartDate.Value.Year, request.StartDate.Value.Month,
                        request.StartDate.Value.Day, request.StartDate.Value.Hour, request.StartDate.Value.Minute, 0,
                        request.StartDate.Value.Offset);
                    var existingWebsitePrice = await _context.WebsitePrices
                        .AnyAsync(x =>
                                x.Id != websitePrice.Id &&
                                x.PublisherPriceType == websitePrice.PublisherPriceType &&
                                (websitePrice.PublisherPriceType == PublisherPriceType.Zone
                                    ? x.ZoneId == websitePrice.ZoneId
                                    : x.WebsiteId == websitePrice.WebsiteId) &&
                                      x.StartDate.Date == request.StartDate.Value.Date &&
                                      x.StartDate.Hour == request.StartDate.Value.Hour &&
                                      x.StartDate.Minute == request.StartDate.Value.Minute,
                            cancellationToken);
                    if (existingWebsitePrice)
                    {
                        throw new BadRequestException("Thời gian áp dụng bảng giá đã tồn tại");
                    }
                    var relatedWebsitePrices = await _context.WebsitePrices
                        .Where(x =>
                            x.PublisherPriceType == websitePrice.PublisherPriceType &&
                            websitePrice.PublisherPriceType == PublisherPriceType.Zone
                                ? x.ZoneId == websitePrice.ZoneId
                                : x.WebsiteId == websitePrice.WebsiteId)
                        .OrderBy(x => x.StartDate)
                        .ToListAsync(cancellationToken);
                    foreach (var currentPrice in relatedWebsitePrices)
                    {
                        if (currentPrice.Id == websitePrice.Id)
                        {
                            currentPrice.StartDate = request.StartDate.Value;
                        }
                    }

                    relatedWebsitePrices = relatedWebsitePrices.OrderBy(x => x.StartDate).ToList();
                    for (var i = 0; i < relatedWebsitePrices.Count; i++)
                    {
                        if (i == relatedWebsitePrices.Count - 1)
                        {
                            if (relatedWebsitePrices[i].EndDate is not null)
                            {
                                relatedWebsitePrices[i].EndDate = null;
                                _context.WebsitePrices.Update(relatedWebsitePrices[i]);
                            }
                        }
                        else
                        {
                            if (relatedWebsitePrices[i].EndDate != relatedWebsitePrices[i + 1].StartDate)
                            {
                                if (relatedWebsitePrices[i].Id == websitePrice.Id)
                                {
                                    websitePrice.EndDate = relatedWebsitePrices[i + 1].StartDate;
                                }
                                else
                                {
                                    relatedWebsitePrices[i].EndDate = relatedWebsitePrices[i + 1].StartDate;
                                    _context.WebsitePrices.Update(relatedWebsitePrices[i]);
                                }
                            }
                        }
                    }
                }
                if (request.TemplatePrices is not null)
                {
                    var currentWebsiteTemplatePrices = await _context.WebsiteTemplatePrices
                        .Where(x => x.WebsitePriceId == websitePrice.Id).ToListAsync(cancellationToken);
                    var advertisingTemplates = await _context.AdvertisingTemplates.ToListAsync(cancellationToken);
                    foreach (var templatePrice in request.TemplatePrices)
                    {
                        var template = advertisingTemplates.FirstOrDefault(x => x.TemplateType == templatePrice.TemplateType);
                        var templateId = template?.Id;
                        var websiteTemplatePrice = new WebsiteTemplatePrice
                        {
                            SellPrice = templatePrice.SellPrice,
                            SellPriceType = templatePrice.SellPriceType,
                            BuyPrice = templatePrice.BuyPrice,
                            BuyPriceType = templatePrice.BuyPriceType,
                            TemplateType = templatePrice.TemplateType,
                            CreatedBy = currentUserId,
                            AdvertisingTemplateId = templateId,
                            WebsitePriceId = websitePrice.Id
                        };
                        if (websitePrice.PublisherPriceType == PublisherPriceType.Zone)
                        {
                            var zoneTemplate = await _context.ZoneTemplates.FirstOrDefaultAsync(
                                x => x.ZoneId == websitePrice.ZoneId && x.AdvertisingTemplateId == templateId, cancellationToken);
                            websiteTemplatePrice.ZoneTemplateId = zoneTemplate?.Id;
                        }
                        newWebsiteTemplatePrices.Add(websiteTemplatePrice);
                    }
                    // Delete entities
                    foreach (var existingEntity in currentWebsiteTemplatePrices)
                    {
                        if (newWebsiteTemplatePrices.All(x => x.AdvertisingTemplateId != existingEntity.AdvertisingTemplateId))
                        {
                            _context.WebsiteTemplatePrices.Remove(existingEntity);
                        }
                    }

                    // Update and Insert entity
                    foreach (var newWebsiteTemplatePrice in newWebsiteTemplatePrices)
                    {
                        var existingEntity = currentWebsiteTemplatePrices.FirstOrDefault(x =>
                            x.AdvertisingTemplateId == newWebsiteTemplatePrice.AdvertisingTemplateId);

                        if (existingEntity is not null)
                        {
                            // Update entity
                            existingEntity.BuyPrice = newWebsiteTemplatePrice.BuyPrice;
                            existingEntity.BuyPriceType = newWebsiteTemplatePrice.BuyPriceType;
                            existingEntity.SellPrice = newWebsiteTemplatePrice.SellPrice;
                            existingEntity.SellPriceType = newWebsiteTemplatePrice.SellPriceType;
                        }
                        else
                        {
                            // Insert entity
                            await _context.WebsiteTemplatePrices.AddAsync(newWebsiteTemplatePrice, cancellationToken);
                        }
                    }
                }
            }

            _context.WebsitePrices.Update(websitePrice);
            await _context.SaveChangesAsync(cancellationToken);
            
            CacheManager.WebsitePrices.TryGetValue(websitePrice.SubId, out var cachedWebsitePrice);
            if (cachedWebsitePrice is not null)
            {
                cachedWebsitePrice.ModifiedBy = websitePrice.ModifiedBy;
                cachedWebsitePrice.IsActive = websitePrice.IsActive;
                cachedWebsitePrice.Status = websitePrice.Status;
                cachedWebsitePrice.StartDate = websitePrice.StartDate;
                cachedWebsitePrice.ModifiedAt = websitePrice.ModifiedAt;
                if (request.IsFullUpdate && request.TemplatePrices is not null)
                {
                    cachedWebsitePrice.WebsiteTemplatePriceCores =
                        newWebsiteTemplatePrices.Cast<WebsiteTemplatePriceCore>().ToList();
                }

                await _globalCacheService.SetAsync(RedisKeys.KeySettings<WebsitePriceCore>(cachedWebsitePrice.SubId), cachedWebsitePrice);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsitePriceCore>(), new PatchCacheManagerSignal<WebsitePriceCore>
                {
                    Entity = cachedWebsitePrice,
                    EntitySubId = cachedWebsitePrice.SubId
                });
            }

            return new UpdateWebsitePriceResponse
            {
                Id = websitePrice.Id,
                BuyPrice = websitePrice.BuyPrice,
                SellPrice = websitePrice.SellPrice,
                Status = websitePrice.Status,
                IsActive = websitePrice.IsActive,
                WebsiteId = websitePrice.WebsiteId,
                ZoneId = websitePrice.ZoneId
            };
        }
    }
}