﻿using AutoMapper;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;

namespace Service.Settings.Application.Commands;

public class CreateObjectGroupCommand : INovanetRequest<ObjectGroup>, IMap
{
    public string GroupName { get; set; } = default!;

    public string? CopyObject { get; set; } = default!;
    
    public bool ApplyAllProducts { get; set; }

    public ObjectGroupProductType? ProductType { get; set; }

    public string? ProductName { get; set; }

    public List<ObjectGroupWebsiteCondition>? WebsiteConditions { get; set; }

    public List<ObjectGroupConsumerBehavior>? ConsumerBehaviors { get; set; }

    internal class Handler : NovanetRequestHandler<CreateObjectGroupCommand, ObjectGroup>
    {
        private readonly SettingsContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            SettingsContext context, IGlobalCacheService globalCacheService, IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<ObjectGroup> HandleAsync(CreateObjectGroupCommand request,
            CancellationToken cancellationToken)
        {
            var currentUserId = UserClaimsValue.Id;
            var objectGroup = new ObjectGroup
            {
                Name = request.GroupName,
                ModifiedAt = DateTimeOffset.Now,
                CreatedAt = DateTimeOffset.Now,
                CreatedBy = currentUserId,
                ModifiedBy = currentUserId,
                ApplyAllProducts = request.ApplyAllProducts,
                ProductType = request.ProductType,
                ProductName = request.ProductName
            };
            var objectGroupEntry = await _context.ObjectGroups.AddAsync(objectGroup, cancellationToken);

            var websiteConditionEntries = new List<ObjectGroupWebsiteCondition>();
            var consumerBehaviorEntries = new List<ObjectGroupConsumerBehavior>();

            if (request.WebsiteConditions != null)
                foreach (var objectGroupWebsiteCondition in request.WebsiteConditions)
                {
                    objectGroupWebsiteCondition.ObjectGroupId = objectGroup.Id;
                    var entry = await _context.ObjectGroupWebsiteConditions.AddAsync(objectGroupWebsiteCondition,
                        cancellationToken);
                    websiteConditionEntries.Add(entry.Entity);
                }

            if (request.ConsumerBehaviors != null)
                foreach (var consumerBehavior in request.ConsumerBehaviors)
                {
                    consumerBehavior.ObjectGroupId = objectGroup.Id;
                    var entry = await _context.ObjectGroupConsumerBehavior.AddAsync(consumerBehavior, cancellationToken);
                    consumerBehaviorEntries.Add(entry.Entity);
                }

            await _context.SaveChangesAsync(cancellationToken);
            
            // update cache
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<ObjectGroupCore>(objectGroupEntry.Entity.SubId), objectGroupEntry.Entity);

            foreach (var websiteCondition in websiteConditionEntries)
            {
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ObjectGroupWebsiteConditionCore>(websiteCondition.SubId), websiteCondition);
            }
            
            foreach (var consumerBehavior in consumerBehaviorEntries)
            {
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<ObjectGroupConsumerBehaviorCore>(consumerBehavior.SubId), consumerBehavior);
            }

            _messageBusClient.Publish(MessageBusChannels.UpdateObjectGroupConditions, new UpdateObjectGroupConditionsConfig
            {
                ObjectGroupSubId = objectGroupEntry.Entity.SubId,
                ObjectGroupWebsiteConditionSubIds = websiteConditionEntries.Select(x => x.SubId).ToList(),
                ObjectGroupConsumerBehaviorSubIds = consumerBehaviorEntries.Select(x => x.SubId).ToList(),
            });

            return objectGroup;
        }
    }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CreateObjectGroupCommand, ObjectGroup>();
    }
}