namespace Service.Settings.Factories;

public class DbContextFactory : IDesignTimeDbContextFactory<SettingsContext>
{
    public SettingsContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.dev.json")
            .Build();

        var appSettings = new AppSettings();
        configuration.Bind(appSettings);

        var optionsBuilder = new DbContextOptionsBuilder<SettingsContext>();
        optionsBuilder.UseSqlServer(appSettings.ConnectionStrings.DefaultConnection, m =>
            {
                m.MigrationsAssembly(SettingsContext.Schema);
                m.MigrationsHistoryTable("__EFMigrationsHistory", SettingsContext.Schema);
            }
        );
        return new SettingsContext(optionsBuilder.Options, appSettings);
    }
}