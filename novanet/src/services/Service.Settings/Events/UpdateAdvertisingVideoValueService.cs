﻿using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessObjects;

namespace Service.Settings.Events;

public class UpdateAdvertisingVideoValueService : EventBusListenerService<UpdateAdvertisingVideoValueSignal>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateAdvertisingVideoValue;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<UpdateAdvertisingVideoValueService> _logger;
    private readonly IServiceScopeFactory _serviceScopeFactory;
    
    public UpdateAdvertisingVideoValueService(IServiceProvider serviceProvider,
        ILogger<UpdateAdvertisingVideoValueService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        _isProcessing = false;
    }

    protected override async Task Processing(UpdateAdvertisingVideoValueSignal signal)
    {
        _isProcessing = true;
        _logger.LogInformation("{Name}: started", nameof(UpdateAdvertisingVideoValueService));
        await using var scope = _serviceScopeFactory.CreateAsyncScope();
        var context = scope.ServiceProvider.GetRequiredService<SettingsContext>();
        var messageBusClient = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();
        
        try
        {
            var advertising = await context.Advertising
                .Include(x => x.AdvertisingAppliedTemplates)
                !.ThenInclude(x => x.AdvertisingTemplateAttributeValues)
                .FirstOrDefaultAsync(x => x.Id == signal.AdvertisingId);
            if (advertising is null)
            {
                return;
            }

            var templateAttributes = await context.AdvertisingTemplateAttributes.ToListAsync();
            var appliedTemplate = advertising.AdvertisingAppliedTemplates?.FirstOrDefault();
            var videoAttribute = templateAttributes.FirstOrDefault(x => x.Name == "video");

            var videoAttributeValue = appliedTemplate?.AdvertisingTemplateAttributeValues?.FirstOrDefault(x =>
                x.AdvertisingTemplateAttributeId == videoAttribute?.Id);
            if (videoAttributeValue is not null)
            {
                videoAttributeValue.StringValue = signal.VideoId;
                context.AdvertisingTemplateAttributeValues.Update(videoAttributeValue);
                await context.SaveChangesAsync();

                CacheManager.Advertising.TryGetValue(advertising.SubId, out var cachedAdvertising);
                var cachedVideoAttributeValue = cachedAdvertising?.AdvertisingAppliedTemplateCores.FirstOrDefault()
                    ?.AdvertisingTemplateAttributeValueCores
                    ?.FirstOrDefault(x => x.AdvertisingTemplateAttributeId == videoAttribute?.Id);
                if (cachedVideoAttributeValue != null)
                {
                    cachedVideoAttributeValue.StringValue = signal.VideoId;
                    messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingCore>(),
                        new PatchCacheManagerSignal<AdvertisingCore>
                        {
                            Entity = cachedAdvertising,
                            EntitySubId = cachedAdvertising?.SubId ?? default,
                        });
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError("{Name} error: {Error}", nameof(UpdateAdvertisingVideoValueService), e.ToString());
        }
        finally
        {
            _isProcessing = false;
            _logger.LogInformation("{Name}: finished", nameof(UpdateAdvertisingVideoValueService));
        }

     
    }
}