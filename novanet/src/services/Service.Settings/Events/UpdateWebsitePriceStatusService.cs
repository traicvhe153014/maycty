﻿using System.Diagnostics;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Events;

public class UpdateWebsitePriceStatusService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateWebsitePriceStatus;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<UpdateWebsitePriceStatusService> _logger;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public UpdateWebsitePriceStatusService(IServiceProvider serviceProvider,
        ILogger<UpdateWebsitePriceStatusService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        using var scope = _serviceScopeFactory.CreateScope();
        var globalCacheService = scope.ServiceProvider.GetRequiredService<IGlobalCacheService>();
        var messageBusClient = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();
        var context = scope.ServiceProvider.GetRequiredService<SettingsContext>();

        _logger.LogInformation("{Name} job started", nameof(UpdateWebsitePriceStatusService));
        try
        {
            var websitePrices = await context.WebsitePrices
                .Where(x => (x.Status == WebsitePriceStatus.Running || x.Status == WebsitePriceStatus.NotStarted) &&
                            x.PublisherPriceType == PublisherPriceType.Website)
                .ToListAsync();
            await UpdateWebsitePrice(context, messageBusClient, globalCacheService, websitePrices.GroupBy(x => x.WebsiteId));

            var zonePrices = await context.WebsitePrices
                .Where(x => (x.Status == WebsitePriceStatus.Running || x.Status == WebsitePriceStatus.NotStarted) &&
                            x.PublisherPriceType == PublisherPriceType.Zone)
                .ToListAsync();
            await UpdateWebsitePrice(context, messageBusClient, globalCacheService, zonePrices.GroupBy(x => x.ZoneId));

            stopwatch.Stop();
            _logger.LogInformation("{Name} job finished. Elapsed time: {Time}", nameof(UpdateWebsitePriceStatusService), stopwatch.ElapsedMilliseconds); 
            _isProcessing = false;
        }
        catch (Exception e)
        {
            stopwatch.Stop();
            _logger.LogError("{Error}", e.ToString()); 
            _isProcessing = false;
        }
    }

    private async Task UpdateWebsitePrice(
        SettingsContext context,
        IMessageBusClient messageBusClient,
        IGlobalCacheService globalCacheService,
        IEnumerable<IGrouping<Guid?, WebsitePrice>> websitePrices)
    {
        var now = DateTimeOffset.Now;
        foreach (var groupedWebsitePrice in websitePrices)
        {
            var runningWebsitePrice =
                groupedWebsitePrice.FirstOrDefault(x => x.Status == WebsitePriceStatus.Running);
            if (runningWebsitePrice is not null)
            {
                if (runningWebsitePrice.EndDate < now)
                {
                    runningWebsitePrice.Status = WebsitePriceStatus.Archived;
                    context.WebsitePrices.Update(runningWebsitePrice);
                    await UpdateWebsitePriceCache(messageBusClient, globalCacheService, runningWebsitePrice);

                    var scheduledWebsitePrice = groupedWebsitePrice
                        .Where(x => x.Id != runningWebsitePrice.Id)
                        .MinBy(x => x.StartDate);
                    if (scheduledWebsitePrice is not null)
                    {
                        scheduledWebsitePrice.Status = WebsitePriceStatus.Running;
                        context.WebsitePrices.Update(scheduledWebsitePrice);
                        await UpdateWebsitePriceCache(messageBusClient, globalCacheService, scheduledWebsitePrice);
                    }
                }
            }
            else
            {
                var scheduledWebsitePrice = groupedWebsitePrice
                    .Where(x => x.StartDate <= now)
                    .MinBy(x => x.StartDate);
                if (scheduledWebsitePrice is not null)
                {
                    scheduledWebsitePrice.Status = WebsitePriceStatus.Running;
                    context.WebsitePrices.Update(scheduledWebsitePrice);
                    await UpdateWebsitePriceCache(messageBusClient, globalCacheService, scheduledWebsitePrice);
                }
            }
        }
        await context.SaveChangesAsync();
    }
    
    private async Task UpdateWebsitePriceCache(IMessageBusClient messageBusClient, IGlobalCacheService globalCacheService, WebsitePrice websitePrice)
    {
        CacheManager.WebsitePrices.TryGetValue(websitePrice.SubId, out var cachedWebsitePrice);
        if (cachedWebsitePrice is not null)
        {
            cachedWebsitePrice.Status = websitePrice.Status;
            await globalCacheService.SetAsync(RedisKeys.KeySettings<WebsitePriceCore>(cachedWebsitePrice.SubId), cachedWebsitePrice);
            messageBusClient.Publish(MessageBusChannels.PatchCacheManager<WebsitePriceCore>(), new PatchCacheManagerSignal<WebsitePriceCore>
            {
                Entity = cachedWebsitePrice,
                EntitySubId = cachedWebsitePrice.SubId
            });
        }
    }

}