﻿using System.Diagnostics;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Events;

public class CheckCampaignBudgetService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.CheckCampaignBudget;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<CheckCampaignBudgetService> _logger;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public CheckCampaignBudgetService(IServiceProvider serviceProvider,
        ILogger<CheckCampaignBudgetService> logger, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        using var scope = _serviceScopeFactory.CreateScope();
        var globalCacheService = scope.ServiceProvider.GetRequiredService<IGlobalCacheService>();
        var redisCacheService = scope.ServiceProvider.GetRequiredService<IRedisCacheService>();
        var messageBusClient = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();
        var context = scope.ServiceProvider.GetRequiredService<SettingsContext>();

        _logger.LogInformation("{Name} job started", nameof(CheckCampaignBudgetService));
        try
        {
            var campaigns = await context.Campaigns
                .Where(x => x.Status == CampaignStatus.Running && x.IsActive)
                .ToListAsync();

            foreach (var campaign in campaigns)
            {
                var campaignPaid = await redisCacheService.HashGetAsync<double>(
                    RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Campaign, ReportMetric.Paid,
                        campaign.SubId),
                    "0");
                if (campaignPaid >= campaign.EcommerceAmountSpent)
                {
                    campaign.Status = CampaignStatus.Finished;
                    context.Campaigns.Update(campaign);
                    
                    // update cache
                    CacheManager.Campaigns.TryGetValue(campaign.SubId, out var cachedCampaign);
                    if (cachedCampaign is not null)
                    {
                        cachedCampaign.Status = campaign.Status;
                        await globalCacheService.SetAsync(RedisKeys.KeySettings<CampaignCore>(cachedCampaign.SubId), cachedCampaign);
                        messageBusClient.Publish(MessageBusChannels.PatchCacheManager<CampaignCore>(), new PatchCacheManagerSignal<CampaignCore>
                        {
                            Entity = cachedCampaign,
                            EntitySubId = cachedCampaign.SubId
                        });
                    }
                }
            }
            
            await context.SaveChangesAsync();

            stopwatch.Stop();
            _logger.LogInformation("{Name} job finished. Elapsed time: {Time}", nameof(CheckCampaignBudgetService), stopwatch.ElapsedMilliseconds); 
            _isProcessing = false;
        }
        catch (Exception e)
        {
            stopwatch.Stop();
            _logger.LogError("{Error}", e.ToString()); 
            _isProcessing = false;
        }
    }

}