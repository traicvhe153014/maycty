﻿using System.Diagnostics;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Settings.Events;

public class UpdateAdObjectStatusService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateAdObjectStatus;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<UpdateAdObjectStatusService> _logger;
    
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public UpdateAdObjectStatusService(IServiceProvider serviceProvider,
        ILogger<UpdateAdObjectStatusService> logger, 
        IServiceScopeFactory serviceScopeFactory
    ) : base(
        serviceProvider)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        var now = DateTimeOffset.Now.Date.AddDays(1).AddMilliseconds(-1);

        using var scope = _serviceScopeFactory.CreateScope();
        
        var globalCacheService = scope.ServiceProvider.GetRequiredService<IGlobalCacheService>();
        var messageBusClient = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();

        var context = scope.ServiceProvider.GetRequiredService<SettingsContext>();
        
        _logger.LogInformation("{Name} job started", nameof(UpdateAdObjectStatusService));
        try
        {  
            var updateStatusService = scope.ServiceProvider.GetRequiredService<IUpdateStatusService>();

            var advertisingSets = await context.AdvertisingSets
                .Include(x => x.AdvertisingSetProductGroups)
                .Where(x => x.Status != AdvertisingSetStatus.Finished && x.Status != AdvertisingSetStatus.Error)
                .ToListAsync();
            
            foreach (var advertisingSet in advertisingSets)
            {
                if (advertisingSet.ApplyAllProductGroups) continue;
                var runningProductGroups = advertisingSet.AdvertisingSetProductGroups
                    .Where(x => x.Status.Equals(AdvertisingSetStatus.Running)).ToList();
                if (runningProductGroups.Count != 0) continue;
                advertisingSet.Status = AdvertisingSetStatus.Error;
                context.AdvertisingSets.Update(advertisingSet);

                // update cache
                CacheManager.AdvertisingSets.TryGetValue(advertisingSet.SubId, out var cachedAdvertisingSet);
                if (cachedAdvertisingSet is null) continue;
                cachedAdvertisingSet.Status = advertisingSet.Status;
                await globalCacheService.SetAsync(RedisKeys.KeySettings<AdvertisingSetCore>(cachedAdvertisingSet.SubId), cachedAdvertisingSet);
                messageBusClient.Publish(MessageBusChannels.PatchCacheManager<AdvertisingSetCore>(), new PatchCacheManagerSignal<AdvertisingSetCore>
                {
                    Entity = cachedAdvertisingSet,
                    EntitySubId = cachedAdvertisingSet.SubId,
                });
            }

            await context.SaveChangesAsync();
            
            await updateStatusService.UpdateCampaignStatus(null);

            stopwatch.Stop();
            _logger.LogInformation("{Name} job finished. Elapsed time: {Time}", nameof(UpdateAdObjectStatusService), stopwatch.ElapsedMilliseconds); 
            _isProcessing = false;
        }
        catch (Exception e)
        {
            stopwatch.Stop();
            _logger.LogError("{Error}", e.ToString()); 
            _isProcessing = false;
        }        
    }
}