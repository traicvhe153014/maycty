﻿using System.Diagnostics;
using Novanet.Core.RedisCache;
using Novanet.EventBus;

namespace Service.Settings.Events;

public class CacheSettingsService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateServiceSettingsCache;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<CacheSettingsService> _logger;

    public CacheSettingsService(IServiceProvider serviceProvider,
        ILogger<CacheSettingsService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        // var context = ServiceProvider.GetRequiredService<SettingsContext>();
        var appSettings = ServiceProvider.GetRequiredService<AppSettings>();
        var connectionstring = appSettings.ConnectionStrings.DefaultConnection;

        var optionsBuilder = new DbContextOptionsBuilder<SettingsContext>();
        optionsBuilder.UseSqlServer(connectionstring);
        await using var context = new SettingsContext(optionsBuilder.Options, appSettings);

        _isProcessing = true;
        try
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _logger.LogInformation("Bắt đầu cập nhật dữ liệu cache 'Service.Settings'");
            var cacheFolderPath = Path.Combine(appSettings.CacheRootPath, "Settings");
            if (!Directory.Exists(cacheFolderPath))
            {
                Directory.CreateDirectory(cacheFolderPath);
            }
            await SyncProductFeedCache(context, appSettings);
            await SyncProductCache(context, appSettings);
            await SyncProductAttributeCache(context, appSettings);
            await SyncZoneCache(context, appSettings);
            await SyncWebsiteCache(context, appSettings);
            await SyncWebsitePriceCache(context, appSettings);
            await SyncAdvertisingCache(context, appSettings);
            await SyncAdvertisingSetCache(context, appSettings);
            await SyncAdvertisingSetProductGroupCache(context, appSettings);
            await SyncAdvertisingSetObjectCache(context, appSettings);
            await SyncAdvertisingTemplateCache(context, appSettings);
            await SyncAdvertisingTemplateAttributeCache(context, appSettings);
            await SyncCampaignCache(context, appSettings);
            await SyncProductGroupEntityCache(context, appSettings);
            await SyncProductAttributeValuesCache(context, appSettings);
            await SyncObjectGroups(context, appSettings);

            stopwatch.Stop();
            _logger.LogInformation("Cập nhật dữ liệu cache 'Service.Settings' thành công. Thời gian thực thi: {Time}",
                stopwatch.Elapsed.TotalMinutes);
        }
        catch (Exception e)
        {
            var errorMessage = e.Message == string.Empty ? "Có lỗi không xác định xảy ra" : e.Message;
            _logger.LogError("{Time}: {Message}", DateTimeOffset.Now, errorMessage);
        }
        finally
        {
            _isProcessing = false;
        }
    }

    private static async Task WriteCache<T>(List<T> data, AppSettings appSettings)
    {
        var cacheFolder = Path.Combine(appSettings.CacheRootPath, "Settings");
        if (!Directory.Exists(cacheFolder))
        {
            Directory.CreateDirectory(cacheFolder);
        }
        var cacheFilePath = Path.Combine(cacheFolder, $"{typeof(T).Name}.json");
        await using var fileStream = new FileStream(cacheFilePath, FileMode.OpenOrCreate, FileAccess.Write);
        await using var streamWriter = new StreamWriter(fileStream);
        await streamWriter.WriteAsync(data.Serialize());
    }

    private static async Task SyncProductFeedCache(SettingsContext context, AppSettings appSettings)
    {
        var productFeeds = await context.ProductFeeds
            .Where(x => !x.Deleted)
            .ToListAsync();
        await WriteCache(productFeeds.Cast<ProductFeedCore>().ToList(), appSettings);
    }
    
    private static async Task SyncProductCache(SettingsContext context, AppSettings appSettings)
    {
        var products = await context.ProductEntities
            .Include(x => x.AttributeValues)
            .Where(x => !x.Deleted)
            .ToListAsync();
        foreach (var product in products)
        {
            product.ProductAttributeValueCores = product.AttributeValues?.Cast<ProductAttributeValueCore>().ToList() ?? new List<ProductAttributeValueCore>();
        }
        await WriteCache(products.Cast<ProductEntityCore>().ToList(), appSettings);
    }
    
    private static async Task SyncProductAttributeCache(SettingsContext context, AppSettings appSettings)
    {
        var productAttributes = await context.ProductAttributes
            .ToListAsync();
        await WriteCache(productAttributes.Cast<ProductAttributeCore>().ToList(), appSettings);
    }
    // TODO: # Hòa NX - Cache không cần nested parent object vào - phình cache rất nhanh, nên truy vấn cache để lấy parent - reference object theo Id
    private static async Task SyncZoneCache(SettingsContext context, AppSettings appSettings)
    {
        var zones = await context.Zones
            .Include(x => x.ZoneTemplates)
            .ToListAsync();
        foreach (var zone in zones)
        {
            zone.ZoneTemplateCores = zone.ZoneTemplates?.Cast<ZoneTemplateCore>().ToList() ?? new List<ZoneTemplateCore>();
        }
        await WriteCache(zones.Cast<ZoneCore>().ToList(), appSettings);
    }
    
    private static async Task SyncWebsiteCache(SettingsContext context, AppSettings appSettings)
    {
        var websites = await context.Websites
            .Include(x => x.DomainAliases)
            .Include(x => x.WebsiteProhibitedCategories)
            .ToListAsync();
        foreach (var website in websites)
        {
            website.DomainAlias = website.DomainAliases?.Cast<DomainAliasCore>().ToList();
            website.WebsiteProhibitedCategoryCores = website.WebsiteProhibitedCategories?.Cast<WebsiteProhibitedCategoryCore>().ToList();
        }
        await WriteCache(websites.Cast<WebsiteCore>().ToList(), appSettings);
    }
    
    private static async Task SyncWebsitePriceCache(SettingsContext context, AppSettings appSettings)
    {
        var websitePrices = await context.WebsitePrices
            .Include(x => x.WebsiteTemplatePrices)
            .ToListAsync();
        foreach (var websitePrice in websitePrices)
        {
            websitePrice.WebsiteTemplatePriceCores =
                websitePrice.WebsiteTemplatePrices?.Cast<WebsiteTemplatePriceCore>().ToList() ??
                new List<WebsiteTemplatePriceCore>();
        }
        await WriteCache(websitePrices.Cast<WebsitePriceCore>().ToList(), appSettings);
    }
    
    private static async Task SyncAdvertisingCache(SettingsContext context, AppSettings appSettings)
    {
        var advertisings = await context.Advertising
            .Include(x => x.AdvertisingAppliedTemplates)
            !.ThenInclude(x => x.AdvertisingTemplateAttributeValues)
            .AsSplitQuery()
            .ToListAsync();
        foreach (var advertising in advertisings)
        {
            advertising.AdvertisingAppliedTemplateCores = advertising.AdvertisingAppliedTemplates?.Select(x => new AdvertisingAppliedTemplateCore
            {
                AdvertisingId = x.AdvertisingId,
                Id = x.Id,
                SubId = x.SubId,
                AdvertisingTemplateId = x.AdvertisingTemplateId,
                AdvertisingTemplateAttributeValueCores = x.AdvertisingTemplateAttributeValues?.Cast<AdvertisingTemplateAttributeValueCore>().ToList() ?? default!
            }).ToList() ?? default!;
        }
        await WriteCache(advertisings.Cast<AdvertisingCore>().ToList(), appSettings);
    }
    
    private static async Task SyncAdvertisingSetCache(SettingsContext context, AppSettings appSettings)
    {
        var advertisingSets = await context.AdvertisingSets
            .AsSplitQuery()
            .Include(x => x.AdvertisingSetLocations)
            .Include(x => x.AdvertisingSetWebsites)
            .Include(x => x.AdvertisingSetScheduling)
            .Include(x => x.AdvertisingSetCategories)
            .ToListAsync();
        foreach (var advertisingSet in advertisingSets)
        {
            advertisingSet.LocationCores =
                advertisingSet.AdvertisingSetLocations?.Cast<AdvertisingSetLocationCore>().ToList() ??
                new List<AdvertisingSetLocationCore>();
            advertisingSet.CategoryCores =
                advertisingSet.AdvertisingSetCategories?.Cast<AdvertisingSetCategoryCore>().ToList() ??
                new List<AdvertisingSetCategoryCore>();
            advertisingSet.AdvertisingSetWebsiteCores =
                advertisingSet.AdvertisingSetWebsites?.Cast<AdvertisingSetWebsiteCore>().ToList() ??
                new List<AdvertisingSetWebsiteCore>();
            advertisingSet.SchedulingCore =
                advertisingSet.AdvertisingSetScheduling?.Cast<AdvertisingSetSchedulingCore>().ToList() ??
                new List<AdvertisingSetSchedulingCore>();
        }
        await WriteCache(advertisingSets.Cast<AdvertisingSetCore>().ToList(), appSettings);
    }
    
    private static async Task SyncAdvertisingSetProductGroupCache(SettingsContext context, AppSettings appSettings)
    {
        var advertisingSetProductGroups = await context.AdvertisingSetProductGroups
            .ToListAsync();
        await WriteCache(advertisingSetProductGroups.Cast<AdvertisingSetProductGroupCore>().ToList(), appSettings);
    }

    private static async Task SyncAdvertisingSetObjectCache(SettingsContext context, AppSettings appSettings)
    {
        var advertisingSetObjects = await context.AdvertisingSetObjects
            .ToListAsync();
        await WriteCache(advertisingSetObjects.Cast<AdvertisingSetObjectCore>().ToList(), appSettings);
    }

    private static async Task SyncAdvertisingTemplateCache(SettingsContext context, AppSettings appSettings)
    {
        var advertisingTemplates = await context.AdvertisingTemplates
            .ToListAsync();
        await WriteCache(advertisingTemplates.Cast<AdvertisingTemplateCore>().ToList(), appSettings);
    }
    
    private static async Task SyncAdvertisingTemplateAttributeCache(SettingsContext context, AppSettings appSettings)
    {
        var advertisingTemplateAttributes = await context.AdvertisingTemplateAttributes
            .ToListAsync();
        await WriteCache(advertisingTemplateAttributes.Cast<AdvertisingTemplateAttributeCore>().ToList(), appSettings);
    }
    
    private static async Task SyncCampaignCache(SettingsContext context, AppSettings appSettings)
    {
        var campaigns = await context.Campaigns
            .Include(x => x.CampaignScheduling)
            .ToListAsync();
        foreach (var campaign in campaigns)
        {
            campaign.SchedulingCore = campaign.CampaignScheduling?.Select(x => new CampaignSchedulingCore
            {
                Id = x.Id,
                Timing = x.Timing,
                CampaignId = x.CampaignId,
                SubId = x.SubId,
                DayOfWeek = x.DayOfWeek
            }).ToList() ?? new List<CampaignSchedulingCore>();
        }
        await WriteCache(campaigns.Cast<CampaignCore>().ToList(), appSettings);
    }
    
    private static async Task SyncProductGroupEntityCache(SettingsContext context, AppSettings appSettings)
    {
        var productGroupEntities = await context.ProductGroupEntities
            .Where(x => !x.Deleted)
            .ToListAsync();
        await WriteCache(productGroupEntities.Cast<ProductGroupEntityCore>().ToList(), appSettings);
    }
    
    private static async Task SyncProductAttributeValuesCache(SettingsContext context, AppSettings appSettings)
    {
        var productAttributeValues = await context.ProductAttributeValues
            .ToListAsync();
        await WriteCache(productAttributeValues.Cast<ProductAttributeValueCore>().ToList(), appSettings);
    }

    private static async Task SyncObjectGroups(SettingsContext context, AppSettings appSettings)
    {
        var objectGroups = await context.ObjectGroups.ToListAsync();
        await WriteCache(objectGroups.Cast<ObjectGroupCore>().ToList(), appSettings);

        var objectGroupConsumerBehaviors = await context.ObjectGroupConsumerBehavior
            .ToListAsync();
        await WriteCache(objectGroupConsumerBehaviors.Cast<ObjectGroupConsumerBehaviorCore>().ToList(), appSettings);

        var objectWebsiteConditions = await context.ObjectGroupWebsiteConditions
            .ToListAsync();
        await WriteCache(objectWebsiteConditions.Cast<ObjectGroupWebsiteConditionCore>().ToList(), appSettings);
    }

}