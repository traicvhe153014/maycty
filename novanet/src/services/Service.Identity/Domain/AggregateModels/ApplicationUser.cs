using System.Text.Json.Serialization;
using NovanetCore.Business.BusinessDomain.Service.Identity;

namespace Service.Identity.Domain.AggregateModels;

public class ApplicationUser : UserCore
{
    public virtual ICollection<ApplicationUserClaim>? Claims { get; set; }
    public virtual ICollection<ApplicationUserLogin>? Logins { get; set; }
    public virtual ICollection<ApplicationUserToken>? Tokens { get; set; }
    [JsonIgnore] public virtual ICollection<ApplicationUserRole>? UserRoles { get; set; }

    public static IdentityUserValue ConvertToUserValue(ApplicationUser user)
    {
        var identityUserValue = new IdentityUserValue
        {
            Id = user.Id,
            SubId = user.SubId,
            Email = user.Email,
            FullName = user.FullName,
            PasswordHash = user.PasswordHash,
            UserName = user.UserName,
            Roles = user.UserRoles?.Select(x => (x.Role ?? default!).Name).ToList(),
            RoleClaims = user.UserRoles?.SelectMany(x => x.Role?.RoleClaims ?? default!)
                .Select(x => x.ClaimValue).ToList(),
            UserClaims = user.Claims?.Select(x => x.ClaimValue).ToList(),
            Amount = user.Amount,
            Status = user.Status,
            PhoneNumber = user.PhoneNumber,
            IsActive = user.IsActive
        };
        return identityUserValue;
    }
}