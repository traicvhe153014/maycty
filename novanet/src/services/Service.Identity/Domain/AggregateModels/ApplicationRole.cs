using NovanetCore.Business.BusinessDomain.Service.Identity;

namespace Service.Identity.Domain.AggregateModels;

public class ApplicationRole : RoleCore
{
    public string? Description { get; set; }
    
    public virtual ICollection<ApplicationUserRole>? UserRoles { get; set; }
    public virtual ICollection<ApplicationRoleClaim>? RoleClaims { get; set; }
}