namespace Service.Identity.Domain.AggregateModels;

public class ApplicationUserLogin : IdentityUserLogin<Guid>
{
    public virtual ApplicationUser? User { get; set; }
}