namespace Service.Identity.Domain.AggregateModels;

public class ApplicationRoleClaim : IdentityRoleClaim<Guid>
{
    public virtual ApplicationRole? Role { get; set; }
}