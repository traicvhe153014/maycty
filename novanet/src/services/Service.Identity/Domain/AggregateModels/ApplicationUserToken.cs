using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Service.Identity.Domain.AggregateModels;

public class ApplicationUserToken : IdentityUserToken<Guid>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    public DateTime Expires { get; set; }

    public virtual ApplicationUser? User { get; set; }

    public void Update(string accessToken, DateTime expires)
    {
        Value = accessToken;
        Expires = expires;
    }
}