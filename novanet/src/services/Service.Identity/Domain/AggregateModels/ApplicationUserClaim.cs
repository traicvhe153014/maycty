namespace Service.Identity.Domain.AggregateModels;

public class ApplicationUserClaim : IdentityUserClaim<Guid>
{
    public virtual ApplicationUser? User { get; set; }
}