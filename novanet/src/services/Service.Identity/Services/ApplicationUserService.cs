﻿using Novanet.Core.Enums;

namespace Service.Identity.Services;

public interface IApplicationUserService
{
    AccessTokenModel GenerateAccessToken(IdentityUserValue user);
}

public class ApplicationUserService : IApplicationUserService
{
    private readonly AppSettings _appSettings;

    public ApplicationUserService(AppSettings appSettings)
    {
        _appSettings = appSettings;
    }

    public AccessTokenModel GenerateAccessToken(IdentityUserValue user)
    {
        var policies = user.RoleClaims?.OrderBy(x => x)
            .Select(x => NovanetPolicies.Policies.IndexOf(x).ToString());

        var claims = new List<Claim>
        {
            new("id", user.Id.ToString()),
            new("fullname", user.FullName),
            new("username", user.UserName),
            new("email", user.Email),
            new("policies", policies.JoinString(",")),
            new("roles", user.Roles.JoinString(","))
        };

        var key = new SymmetricSecurityKey(
            Encoding.UTF8.GetBytes(_appSettings.TokenOption.ServerSigningPassword));
        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
        var expires = DateTime.Now.AddMinutes(_appSettings.TokenOption.AccessTokenDurationInMinutes);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Issuer = _appSettings.TokenOption.Issuer,
            Audience = _appSettings.TokenOption.Issuer,
            Subject = new ClaimsIdentity(claims),
            Expires = expires,
            SigningCredentials = credentials
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var accessToken = tokenHandler.WriteToken(token);
        return new AccessTokenModel
        {
            AccessToken = accessToken,
            Expires = expires
        };
    }
}