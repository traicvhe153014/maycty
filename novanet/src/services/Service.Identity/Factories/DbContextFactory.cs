namespace Service.Identity.Factories;

public class DbContextFactory : IDesignTimeDbContextFactory<IdentityContext>
{
    public IdentityContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.dev.json")
            .Build();

        var appSettings = new AppSettings();
        configuration.Bind(appSettings);

        var optionsBuilder = new DbContextOptionsBuilder<IdentityContext>();
        optionsBuilder.UseSqlServer(appSettings.ConnectionStrings.DefaultConnection, m =>
            {
                m.MigrationsAssembly(IdentityContext.Schema);
                m.MigrationsHistoryTable("__EFMigrationsHistory", IdentityContext.Schema);
            }
        );
        return new IdentityContext(optionsBuilder.Options);
    }
}