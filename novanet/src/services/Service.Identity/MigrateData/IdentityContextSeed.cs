﻿using Microsoft.Data.SqlClient;
using NovanetCore.Business.BusinessParallels;
using Polly;
using Polly.Retry;

namespace Service.Identity.MigrateData;

public class IdentityContextSeed
{
    public static async Task SeedAsync(IdentityContext context,
        ILogger<IdentityContextSeed>? logger,
        IServiceProvider services)
    {
        var policy = CreatePolicy(logger, nameof(IdentityContextSeed));

        await policy.ExecuteAsync(async () =>
        {
            foreach (var role in MigrateRoles.Roles())
            {
                if (!await context.Roles.AnyAsync(x => x.Name.Equals(role.Name)))
                    await context.Roles.AddAsync(role);
            }

            await context.SaveChangesAsync();
            
            var publisher = services.GetRequiredService<Publisher>();
            await publisher.Publish(new UpdateCacheManagerParallel(), PublishStrategy.ParallelNoWait);
        });
    }

    private static AsyncRetryPolicy CreatePolicy(ILogger? logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().WaitAndRetryAsync(
            retryCount: retries,
            sleepDurationProvider: _ => TimeSpan.FromSeconds(5),
            onRetry: (exception, _, retry, _) =>
            {
                logger?.LogWarning(exception,
                    "[{Prefix}] Exception {ExceptionType} with message {Message} detected on attempt {Retry} of {Retries}",
                    prefix, exception.GetType().Name, exception.Message, retry, retries);
            }
        );
    }
}