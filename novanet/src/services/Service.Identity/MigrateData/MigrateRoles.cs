namespace Service.Identity.MigrateData;

public static class MigrateRoles
{
    public static IEnumerable<ApplicationRole> Roles()
    {
        return new List<ApplicationRole>
        {
            new()
            {
                Id = Guid.NewGuid(),
                Name = "Admin",
                Description = "Admin",
                NormalizedName = "ADMIN",
                RoleClaims = new List<ApplicationRoleClaim>
                {
                    #region Identity

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Me",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:SignUp",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:UpdatePassword",
                    },

                    #endregion
                    
                    #region Advertising

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:ListTemplate",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:ReportTemplate",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:Update",
                    },

                    #endregion

                    #region AdvertisingSet

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:Update",
                    },

                    #endregion

                    #region Campaign

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:Update",
                    },

                    #endregion

                    #region ObjectGroup

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:Update",
                    },

                    #endregion

                    #region Payment

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Payment:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Payment:Create",
                    },

                    #endregion

                    #region ProductAttribute

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductAttribute:List",
                    },

                    #endregion

                    #region ProductAttributeValue

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductAttributeValue:List",
                    },

                    #endregion

                    #region Product

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Product:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Product:Overview",
                    },

                    #endregion

                    #region ProductFeed

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:HasAny",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Refresh",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Delete",
                    },

                    #endregion

                    #region ProductGroup

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:Get",
                    },
                    #endregion

                    #region Website

                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:Update",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:Update",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:Get",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:Update",
                    },

                    #endregion
                    
                    #region Bucket

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Delete",
                    },
                    

                    #endregion

                    #region File

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Upload",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Delete",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Avatar",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Stream",
                    },

                    #endregion

                    #region Report

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:FraudClick",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Publisher",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Website",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Zone",
                    },

                    #endregion
                    
                    #region Publisher

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Publisher:List",
                    }

                    #endregion
                    
                }
            },
            new()
            {
                Id = Guid.NewGuid(),
                Name = "Account",
                Description = "Account (Advertiser)",
                NormalizedName = "ACCOUNT",
                RoleClaims = new List<ApplicationRoleClaim>
                {
                    #region Identity

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Me",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:UpdatePassword",
                    },

                    #endregion
                    
                    #region Advertising

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:ListTemplate",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:ReportTemplate",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:Update",
                    },

                    #endregion

                    #region AdvertisingSet

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "AdvertisingSet:Update",
                    },

                    #endregion

                    #region Campaign

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Campaign:Update",
                    },

                    #endregion

                    #region ObjectGroup

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ObjectGroup:Update",
                    },

                    #endregion

                    #region ProductAttribute

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductAttribute:List",
                    },

                    #endregion

                    #region ProductAttributeValue

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductAttributeValue:List",
                    },

                    #endregion

                    #region Product

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Product:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Product:Overview",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Product:Update",
                    },

                    #endregion

                    #region ProductFeed

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Get",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:HasAny",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Delete",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductFeed:Refresh",
                    },

                    #endregion

                    #region ProductGroup

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:List",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "ProductGroup:Get",
                    },

                    #endregion
                    
                    #region Website

                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:List",
                    },

                    #endregion
                    
                    #region Bucket

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Delete",
                    },
                    

                    #endregion

                    #region File

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Upload",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Delete",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Avatar",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Stream",
                    },

                    #endregion
                }
            },
            new()
            {
                Id = Guid.NewGuid(),
                Name = "Accountant",
                Description = "Accountant",
                NormalizedName = "ACCOUNTANT",
                RoleClaims = new List<ApplicationRoleClaim>
                {
                    #region Identity

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Me",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:UpdatePassword",
                    },

                    #endregion
                    
                    #region Payment

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Payment:Create",
                    },

                    #endregion
                    
                    #region Bucket

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Create",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Bucket:Delete",
                    },
                    

                    #endregion

                    #region File

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Upload",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Delete",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Avatar",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "File:Stream",
                    },

                    #endregion
                }
            },
            new()
            {
                Id = Guid.NewGuid(),
                Name = "Publisher",
                Description = "Publisher",
                NormalizedName = "PUBLISHER",
                RoleClaims = new List<ApplicationRoleClaim>
                {
                    #region Identity

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Me",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:UpdatePassword",
                    },

                    #endregion
                    
                    #region Website
                    
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:Update",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:Update",
                    },

                    #endregion

                    #region Report

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Website",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Zone",
                    },

                    #endregion
                }
            },
            new()
            {
                Id = Guid.NewGuid(),
                Name = "PublisherManager",
                Description = "Publisher Manager",
                NormalizedName = "PUBLISHERMANAGER",
                RoleClaims = new List<ApplicationRoleClaim>
                {
                    #region Identity

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Me",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:Update",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Identity:UpdatePassword",
                    },

                    #endregion
                    
                    #region Website

                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Website:Update",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Zone:Update",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:Create",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:Get",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:List",
                    },
                    new ()
                    {
                        ClaimType = "Action",
                        ClaimValue = "WebsitePrice:Update",
                    },

                    #endregion

                    #region Report

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Publisher",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Website",
                    },
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Report:Zone",
                    },

                    #endregion
                    
                    #region Publisher

                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Publisher:List",
                    },

                    #endregion
                    
                    #region Advertising
                    
                    new()
                    {
                        ClaimType = "Action",
                        ClaimValue = "Advertising:ListTemplate",
                    },

                    #endregion
                }
            }
        };
    }
}