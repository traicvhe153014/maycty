﻿using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessServices;

namespace Service.Identity.Application.Queries;

public class MySelfQuery : INovanetRequest<UserResponse>
{
    internal class Handler : NovanetRequestHandler<MySelfQuery, UserResponse>
    {
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IReportService _reportService;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            IGlobalCacheService globalCacheService, IReportService reportService) : base(logger,
            httpContextAccessor)
        {
            _globalCacheService = globalCacheService;
            _reportService = reportService;
        }

        protected override async Task<UserResponse> HandleAsync(MySelfQuery request,
            CancellationToken cancellationToken)
        {
            var user = await _globalCacheService.GetAsync<IdentityUserValue>(
                RedisKeys.KeySettings<IdentityUserValue>(UserClaimsValue.Id));
            if (user == null) return default!;
            var paid = await _reportService.GetSingleAdvertiserReport(ReportMetric.Paid,
                ReportAdvertiserDimension.Advertiser, user.SubId);
            var remainingAmount = user.Amount - paid;
            return new UserResponse
            {
                Id = user.Id,
                Email = user.Email,
                FullName = user.FullName,
                UserName = user.UserName,
                Amount = remainingAmount,
                SubId = user.SubId
            };
        }
    }
}