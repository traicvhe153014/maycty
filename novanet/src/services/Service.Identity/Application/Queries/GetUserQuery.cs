﻿namespace Service.Identity.Application.Queries;

public class GetUserQuery : INovanetRequest<UserResponse>
{
    public string Email { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<GetUserQuery, UserResponse>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public Handler(ILogger<Handler> logger,
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _userManager = userManager;
        }

        protected override async Task<UserResponse> HandleAsync(GetUserQuery request,
            CancellationToken cancellationToken)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken);
            if (user is null)
            {
                throw new BadRequestException("Email không tồn tại");
            }

            return new UserResponse
            {
                Id = user.Id,
                Email = user.Email,
                FullName = user.FullName,
                UserName = user.UserName,
                Amount = user.Amount
            };
        }
    }
}