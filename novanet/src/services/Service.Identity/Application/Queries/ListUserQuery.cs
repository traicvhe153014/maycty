using System.Linq.Expressions;
using Novanet.Core.Specifications;

namespace Service.Identity.Application.Queries;

public class ListUserQuery : INovanetRequest<List<UserResponse>>
{
    public string? Keyword { get; set; }

    public string? QueryField { get; set; }

    public PredicateOperatorEnum? Operator { get; set; }

    public int Page { get; set; }

    public int PageSize { get; set; }
    
    public string? Roles { get; set; }

    public bool IsAccountantIgnored { get; set; }

    internal class Handler : NovanetRequestHandler<ListUserQuery, List<UserResponse>>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public Handler(ILogger<Handler> logger,
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _userManager = userManager;
        }

        protected override async Task<List<UserResponse>> HandleAsync(ListUserQuery request,
            CancellationToken cancellationToken)
        {
            Expression<Func<ApplicationUser, bool>> whereExpression = x => true;
            if (request.QueryField is not null && request.Keyword is not null && request.Operator is not null)
            {
                whereExpression = whereExpression.And(PredicateBuilder.Build<ApplicationUser>(request.QueryField,
                    Enum.GetName(typeof(PredicateOperatorEnum), request.Operator), request.Keyword));
            }

            if (request.IsAccountantIgnored)
            {
                whereExpression = whereExpression.And(x => x.UserRoles!.All(role => role.Role!.Name != "Accountant"));
            }

            if (request.Roles is not null)
            {
                var roles = request.Roles.Split(',');
                whereExpression = whereExpression.And(x => x.UserRoles!.Any(role => roles.Any(y => y == role.Role!.Name)));
            }

            var users = await _userManager.Users
                .Include(x => x.UserRoles)
                !.ThenInclude(x => x.Role)
                .Where(whereExpression)
                .Skip((request.Page - 1) * request.PageSize).Take(request.PageSize)
                .Select(x => new UserResponse
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                    UserName = x.UserName
                })
                .ToListAsync(cancellationToken);

            return users;
        }
    }
}