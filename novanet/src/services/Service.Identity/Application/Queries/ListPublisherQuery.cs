﻿using System.Linq.Expressions;
using Novanet.Core.Enums;
using Novanet.Core.Extensions.LinqExtensions;
using Novanet.Core.Specifications;
using NovanetCore.Business.BusinessManager;

namespace Service.Identity.Application.Queries;

public class ListPublisherQuery : INovanetRequest<ListPublisherResponse>
{
    public int Page { get; set; }

    public int PageSize { get; set; }
    
    public string? Keyword { get; set; }
    
    public string? Sorts { get; set; }
    
    public string? Filters { get; set; }
    
    public UserStatus? status { get; set; }

    internal class Handler : NovanetRequestHandler<ListPublisherQuery, ListPublisherResponse>
    {
        private readonly IdentityContext _context;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor, IdentityContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<ListPublisherResponse> HandleAsync(ListPublisherQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<ApplicationUser, bool>> whereExpression = x => x.UserRoles!.Any(role => role.Role!.Name == "Publisher");
            Expression<Func<IdentityUserValue, bool>> whereExpressionTotal = x => x.Roles!.Contains("Publisher");
            var listFilters = request.Filters.Deserialize<List<PredicateModel<FieldLevelEnum,string>>>();
            if (listFilters is not null&&listFilters.Count>0)
            {
                var listIdUser = new List<Guid>();
                foreach (var KeyWord in listFilters)
                {
                    var loweredKeyWord = KeyWord.Value.ToLower();
                    if (KeyWord.Field==FieldLevelEnum.Email)
                    {
                        whereExpression = whereExpression.And(x => x.Email.ToLower().Contains(loweredKeyWord));
                        whereExpressionTotal = whereExpressionTotal.And(x => x.Email.ToLower().Contains(loweredKeyWord));
                    }
                    if (KeyWord.Field==FieldLevelEnum.DomainWebsites)
                    {
                        var listidUserByWebsites = CacheManager.Websites.Values
                            .Where(website => website.Domain.ToLower().Contains(loweredKeyWord))
                            .Select(x => x.PublisherId).ToList();
                        listIdUser.AddRange(listidUserByWebsites);
                        if (listidUserByWebsites.Count==0)
                        {
                            whereExpression = whereExpression.And(x => x.Id.Equals(""));
                            whereExpressionTotal = whereExpressionTotal.And(x => x.Id.Equals(""));
                        }
                    }
                }
                if (listIdUser.Count>0)
                {
                    whereExpression = whereExpression.And(x => listIdUser.Distinct().Contains(x.Id));
                    whereExpressionTotal = whereExpressionTotal.And(x => listIdUser.Distinct().Contains(x.Id));
                }
            }

            if (request.status is not null)
            {
                whereExpression = whereExpression.And(x => x.Status==request.status);
                whereExpressionTotal = whereExpressionTotal.And(x => x.Status==request.status);
            }

            var sorts = request.Sorts?.Split(',') ?? Array.Empty<string>();
           
            var publishers = await _context.Users
                .Include(x => x.UserRoles)
                !.ThenInclude(x => x.Role)
                .Where(whereExpression)
                .SortBy(sorts)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new PublisherResponse
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    UserName = x.UserName,
                    Email = x.Email,
                    PhoneNumber = x.PhoneNumber,
                    Status = x.Status,
                    CreatedAt = x.CreatedAt,
                    IsActive = x.IsActive
                })
                .ToListAsync(cancellationToken);

            var response  = new ListPublisherResponse();
            response.Total = CacheManager.IdentityUsers.Values
                .Count(whereExpressionTotal.Compile());
            response.Data = publishers
                .Select(x =>
                {
                    x.DomainWebsites = CacheManager.Websites.Values
                        .Where(website => website.PublisherId == x.Id)
                        .Select(website => new DomainWebsite
                        {
                            Id = website.Id,
                            Domain = website.Domain
                        })
                        .ToList();
                    return x;
                })
                .ToList();
            return response;
        }
    }
}

public enum FieldLevelEnum
{
    Email,
    DomainWebsites,
}