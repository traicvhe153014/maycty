using System.Data;
using Novanet.Core.Constants;
using Novanet.Core.Enums;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessDomain.Service.Identity;
using NovanetCore.Business.BusinessEvents;

namespace Service.Identity.Application.Commands;

public sealed class SignUpCommand : INovanetRequest<ApplicationUser>
{
    public string FullName { get; set; } = default!;

    public string UserName { get; set; } = default!;

    public string Email { get; set; } = default!;

    public string Password { get; set; } = default!;

    public string ConfirmPassword { get; set; } = default!;

    public string Role { get; set; } = default!;

    public bool? IsActive { get; set; }

    public string? PhoneNumber { get; set; }

    internal class Handler : NovanetRequestHandler<SignUpCommand, ApplicationUser>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;
        private readonly IdentityContext _context;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            UserManager<ApplicationUser> userManager, IGlobalCacheService globalCacheService,
            IMessageBusClient messageBusClient, IdentityContext context) : base(logger, httpContextAccessor)
        {
            _userManager = userManager;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
            _context = context;
        }

        protected override async Task<ApplicationUser> HandleAsync(SignUpCommand request,
            CancellationToken cancellationToken)
        {
            var existingUser = await _userManager.FindByEmailAsync(request.Email);
            if (existingUser == null)
            {
                var user = new ApplicationUser
                {
                    FullName = request.FullName,
                    UserName = request.UserName,
                    Email = request.Email,
                    CreatedAt = DateTimeOffset.Now,
                    ModifiedAt = DateTimeOffset.Now,
                };
                if (request.PhoneNumber is not null)
                {
                    user.PhoneNumber = request.PhoneNumber;
                }

                if (request.IsActive is not null)
                {
                    user.IsActive = request.IsActive.Value;
                    user.Status = request.IsActive == true ? UserStatus.Running : UserStatus.Disabled;
                }

                var result = await _userManager.CreateAsync(user, request.Password);
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(request.Role))
                        await _userManager.AddToRoleAsync(user, request.Role);
                    // save to cache
                    var applicationUser = await _context.Users
                        .AsNoTracking()
                        .Include(x => x.UserRoles)!
                        .ThenInclude(x => x.Role).ThenInclude(x => x!.RoleClaims)
                        .Include(x => x.Tokens)
                        .Include(x => x.Claims)
                        .FirstOrDefaultAsync(x => x.Id == user.Id, cancellationToken);
                    if (applicationUser is not null)
                    {
                        var identityUserValue = ApplicationUser.ConvertToUserValue(applicationUser);
                        await _globalCacheService.SetAsync(
                            RedisKeys.KeySettings<IdentityUserValue>(identityUserValue.Id),
                            identityUserValue);
                        await _globalCacheService.SetAsync(
                            RedisKeys.KeySettings<IdentityUserValue>(identityUserValue.Email),
                            identityUserValue);
                        _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<IdentityUserValue>(),
                            new PatchCacheManagerSignal<IdentityUserValue>
                            {
                                Entity = identityUserValue,
                                EntitySubId = identityUserValue.SubId
                            });
                    }

                    return user;
                }
                else
                {
                    var errors = result.Errors;
                    if (errors.Any(x => x.Code.Equals(nameof(ErrorMessageValidate.PasswordRequiresLower))))
                        throw new ApplicationException("Mật khẩu phải có ít nhất một chữ thường ('a' - 'z').");
                    throw new BadRequestException(errors.ToString());
                }
            }
            else
            {
                throw new Exception($"Người dùng '{request.UserName}' đã tồn tại");
            }
        }
    }
}

public class SignUpCommandValidator : AbstractValidator<SignUpCommand>
{
    public SignUpCommandValidator()
    {
        RuleFor(x => x.UserName).NotEmpty();
        RuleFor(x => x.Email).NotEmpty();
        RuleFor(x => x.Password).NotEmpty();
        RuleFor(x => x.ConfirmPassword).Equal(x => x.Password);
    }
}