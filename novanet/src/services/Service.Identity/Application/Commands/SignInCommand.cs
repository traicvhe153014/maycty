using Novanet.Core.Constants;
using Novanet.Core.RedisCache;
using Service.Identity.Services;

namespace Service.Identity.Application.Commands;

public sealed class SignInCommand : INovanetRequest<SignInResponse>
{
    public string Email { get; set; } = default!;

    public string Password { get; set; } = default!;

    internal class Handler : NovanetRequestHandler<SignInCommand, SignInResponse>
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IdentityContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IApplicationUserService _applicationUserService;

        public Handler(ILogger<Handler> logger,
            SignInManager<ApplicationUser> signInManager,
            IdentityContext context,
            IGlobalCacheService globalCacheService,
            IApplicationUserService applicationUserService,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _signInManager = signInManager;
            _context = context;
            _globalCacheService = globalCacheService;
            _applicationUserService = applicationUserService;
        }

        protected override async Task<SignInResponse> HandleAsync(SignInCommand request,
            CancellationToken cancellationToken)
        {
            var user = await _context.Users.Include(x => x.UserRoles)!
                .ThenInclude(x => x.Role).ThenInclude(x => x!.RoleClaims)
                .Include(x => x.Tokens).Include(x => x.Claims)
                .FirstOrDefaultAsync(x => x.Email.Equals(request.Email), cancellationToken);

            if (user == null)
                throw new BadRequestException(ErrorMessageValidate.EmailNotFound);

            if (!user.IsActive)
            {
                throw new BadRequestException(ErrorMessageValidate.DeactivatedUser);
            }

            var identityUserValue = ApplicationUser.ConvertToUserValue(user);

            await _globalCacheService.SetAsync(RedisKeys.KeySettings<IdentityUserValue>(identityUserValue.Id),
                identityUserValue);
            await _globalCacheService.SetAsync(RedisKeys.KeySettings<IdentityUserValue>(identityUserValue.Email),
                identityUserValue);

            var passwordCheck = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

            if (!passwordCheck.Succeeded) throw new BadRequestException(ErrorMessageValidate.PasswordNotFound);

            var accessToken = _applicationUserService.GenerateAccessToken(identityUserValue);

            var userToken =
                await _context.UserTokens.FirstOrDefaultAsync(x => x.UserId.Equals(user.Id), cancellationToken);

            if (userToken == null)
            {
                userToken = new ApplicationUserToken
                {
                    Id = Guid.NewGuid(),
                    Name = user.FullName,
                    UserId = user.Id,
                    LoginProvider = LoginProvider.Jwt,
                    Expires = accessToken.Expires,
                    Value = accessToken.AccessToken
                };
                await _context.UserTokens.AddAsync(userToken, cancellationToken);
            }
            else
            {
                userToken.Value = accessToken.AccessToken;
                _context.UserTokens.Update(userToken);
            }

            await _context.SaveChangesAsync(cancellationToken);

            return new SignInResponse
            {
                Expires = accessToken.Expires,
                AccessToken = accessToken.AccessToken,
                RefreshToken = userToken.Id,
            };
        }
    }
}

public class SignInCommandValidator : AbstractValidator<SignInCommand>
{
    public SignInCommandValidator()
    {
        RuleFor(t => t.Email).NotEmpty();
        RuleFor(t => t.Password).NotEmpty();
    }
}