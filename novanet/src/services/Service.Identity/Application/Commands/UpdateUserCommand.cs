﻿using Novanet.Core.Enums;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessDomain.Service.Identity;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessManager;

namespace Service.Identity.Application.Commands;

public class UpdateUserCommand : INovanetRequest<object>
{
    public Guid Id { get; set; }
    public string? FullName { get; set; }
    public string? PhoneNumber { get; set; }
    
    public bool? IsActive { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateUserCommand, object>
    {
        private readonly IdentityContext _context;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IMessageBusClient _messageBusClient;

        public Handler(ILogger<NovanetRequestHandler<UpdateUserCommand, object>> logger,
            IHttpContextAccessor httpContextAccessor, IdentityContext context, IGlobalCacheService globalCacheService,
            IMessageBusClient messageBusClient) : base(logger, httpContextAccessor)
        {
            _context = context;
            _globalCacheService = globalCacheService;
            _messageBusClient = messageBusClient;
        }

        protected override async Task<object> HandleAsync(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (user is null)
            {
                return new BadRequestException("Id người dùng không tồn tại");
            }

            if (request.FullName is not null)
            {
                user.FullName = request.FullName;
            }
            if (request.PhoneNumber is not null)
            {
                user.PhoneNumber = request.PhoneNumber;
            }
            if (request.IsActive is not null)
            {
                user.IsActive = request.IsActive.Value;
                if (user.IsActive)
                {
                    user.Status = UserStatus.Running;
                }
                else
                {
                    user.Status = UserStatus.Disabled;
                }
            }
            user.ModifiedAt = DateTimeOffset.Now;

            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);

            CacheManager.IdentityUsers.TryGetValue(user.SubId, out var cachedUser);
            if (cachedUser is not null)
            {
                cachedUser.FullName = user.FullName;
                cachedUser.Status = user.Status;
                cachedUser.IsActive = user.IsActive;
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<IdentityUserValue>(user.Id),
                    cachedUser);
                await _globalCacheService.SetAsync(RedisKeys.KeySettings<IdentityUserValue>(user.Email),
                    cachedUser);
                _messageBusClient.Publish(MessageBusChannels.PatchCacheManager<IdentityUserValue>(), new PatchCacheManagerSignal<IdentityUserValue>
                {
                    Entity = cachedUser,
                    EntitySubId = cachedUser.SubId
                });
            }

            return user;
        }
    }
}