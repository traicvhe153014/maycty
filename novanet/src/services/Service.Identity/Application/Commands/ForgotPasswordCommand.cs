﻿using MediatR;

namespace Service.Identity.Application.Commands;

public class ForgotPasswordCommand : IRequest<bool>
{
    public string Email { get; set; } = default!;

    public string Password { get; set; } = default!;

    public string ConfirmPassword { get; set; } = default!;
    
    internal class Handler : IRequestHandler<ForgotPasswordCommand, bool>
    {
        public Task<bool> Handle(ForgotPasswordCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}