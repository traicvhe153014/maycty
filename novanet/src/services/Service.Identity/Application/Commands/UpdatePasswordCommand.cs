﻿using System.Data;
using Novanet.Core.RedisCache;

namespace Service.Identity.Application.Commands;

public class UpdatePasswordCommand : INovanetRequest<bool>
{
    public string Email { get; set; } = default!;
    public string? CurrentPassword { get; set; }
    public string? NewPassword { get; set; }
    public string? ConfirmNewPassword { get; set; }
    
    internal class Handler : NovanetRequestHandler<UpdatePasswordCommand, bool>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IdentityContext _context;

        public Handler(ILogger<NovanetRequestHandler<UpdatePasswordCommand, bool>> logger,
            IHttpContextAccessor httpContextAccessor, IdentityContext context,
            UserManager<ApplicationUser> userManager) : base(logger, httpContextAccessor)
        {
            _context = context;
            _userManager = userManager;
        }

        protected override async Task<bool> HandleAsync(UpdatePasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken);
            if (user is null)
            {
                throw new BadRequestException("Email người dùng không tồn tại");
            }
            if (!UserClaimsValue.IsAdmin && !UserClaimsValue.IsPublisherManager)
            {
                var checkPasswordResult =
                    await _userManager.CheckPasswordAsync(user, request.CurrentPassword);
                if (!checkPasswordResult)
                {
                    throw new BadRequestException("Mật khẩu hiện tại không hợp lệ");
                }
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, request.NewPassword);
            
            user.ModifiedAt = DateTimeOffset.Now;
            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}

public class UpdatePasswordCommandValidator : AbstractValidator<UpdatePasswordCommand>
{
    public UpdatePasswordCommandValidator()
    {
        RuleFor(x => x.Email)
            .NotEmpty();
        RuleFor(x => x.NewPassword)
            .NotEmpty();
        RuleFor(x => x.ConfirmNewPassword)
            .NotEmpty()
            .Equal(x => x.NewPassword);
    }
}