﻿using Novanet.Core.RedisCache;
using Novanet.Core.Specifications;
using Service.Identity.Services;

namespace Service.Identity.Application.Commands;

public class RefreshTokenQuery : INovanetRequest<AccessTokenModel>
{
    internal class Handler : NovanetRequestHandler<RefreshTokenQuery, AccessTokenModel>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IdentityContext _context;
        private readonly IApplicationUserService _applicationUserService;
        private readonly IGlobalCacheService _globalCacheService;

        public Handler(ILogger<Handler> logger,
            IHttpContextAccessor contextAccessor,
            IdentityContext context,
            IApplicationUserService applicationUserService,
            IGlobalCacheService globalCacheService) :
            base(logger, contextAccessor)
        {
            _contextAccessor = contextAccessor;
            _context = context;
            _applicationUserService = applicationUserService;
            _globalCacheService = globalCacheService;
        }

        protected override async Task<AccessTokenModel> HandleAsync(RefreshTokenQuery request,
            CancellationToken cancellationToken)
        {
            // Lấy thông tin user từ cookies
            var cookies = _contextAccessor.HttpContext?.Request.Cookies;
            var userId = cookies?.FirstOrDefault(x => x.Key.Equals(nameof(ApplicationUser.Id))).Value;
            var refreshToken = cookies?.FirstOrDefault(x => x.Key.Equals("RefreshToken")).Value;

            // Nếu không tồn tại 1 trong 2 trường hợp return null
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(refreshToken)) return default!;
            var user = await _globalCacheService.GetAsync<IdentityUserValue>(
                RedisKeys.KeySettings<IdentityUserValue>(userId));

            // Sinh thông tin access token mới
            var accessToken = _applicationUserService.GenerateAccessToken(user);
            var specification = new Specification<ApplicationUserToken>(x => x.UserId.Equals(userId.ToGuid()));
            var userToken = await _context.UserTokens.FirstOrDefaultAsync(specification.Predicate, cancellationToken);
            if (userToken == null) return default!;

            // Cập nhật thông tin refresh token
            userToken.Update(accessToken.AccessToken, accessToken.Expires);
            _context.UserTokens.Update(userToken);
            await _context.SaveChangesAsync(cancellationToken);
            return new AccessTokenModel
            {
                AccessToken = accessToken.AccessToken,
                Expires = accessToken.Expires
            };
        }
    }
}