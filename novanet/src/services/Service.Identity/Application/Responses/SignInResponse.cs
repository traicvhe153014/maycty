namespace Service.Identity.Application.Responses;

public class SignInResponse
{
    public DateTimeOffset Expires { get; set; }
    
    public string AccessToken { get; set; } = default!;

    public Guid RefreshToken { get; set; }
}