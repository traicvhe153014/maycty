using Novanet.Core.Enums;
using NovanetCore.Business.BusinessDomain.Service.Identity;

namespace Service.Identity.Application.Responses;

public class ListPublisherResponse
{
    public int Total { get; set; }
    public List<PublisherResponse> Data { get; set; } = default!;
}

public class PublisherResponse
{
    public Guid Id { get; set; }
    public string FullName { get; set; } = default!;
    public string UserName { get; set; } = default!;
    public string Email { get; set; } = default!;
    public string PhoneNumber { get; set; } = default!;
    public bool IsActive { get; set; }
    public UserStatus Status { get; set; }
    public DateTimeOffset? CreatedAt { get; set; }
    public List<DomainWebsite> DomainWebsites { get; set; } = default!;
}

public class DomainWebsite
{
    public Guid Id { get; set; }
    public string Domain { get; set; } = default!;
}