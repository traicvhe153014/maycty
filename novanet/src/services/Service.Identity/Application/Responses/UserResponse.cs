namespace Service.Identity.Application.Responses;

public class UserResponse
{
    public Guid Id { get; set; }

    public string FullName { get; set; } = default!;

    public string Email { get; set; } = default!;

    public string UserName { get; set; } = default!;
    
    public double? Amount { get; set; }
    
    public long SubId { get; set; }
    
}