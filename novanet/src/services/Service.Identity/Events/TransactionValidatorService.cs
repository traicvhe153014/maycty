﻿using Novanet.Core.Models.MessageBusModels;
using Novanet.Core.RedisCache;

namespace Service.Identity.Events;

public class TransactionValidatorService : EventBusListenerService<TransactionValidatorModel>
{
    private readonly ILogger<TransactionValidatorService> _logger;

    protected override string Channel => MessageBusChannels.TransactionValidator;
    protected override bool IsWorkQueue => false;

    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;

    public TransactionValidatorService(
        IServiceProvider serviceProvider,
        ILogger<TransactionValidatorService> logger) :
        base(serviceProvider)
    {
        _logger = logger;
    }

    protected override async Task Processing(TransactionValidatorModel transaction)
    {
        _isProcessing = true;
        _logger.LogInformation("{Name} event executed", nameof(TransactionValidatorService));
        try
        {
            var appSettings = ServiceProvider.GetRequiredService<AppSettings>();
            var connectionString = appSettings.ConnectionStrings.DefaultConnection;

            var optionsBuilder = new DbContextOptionsBuilder<IdentityContext>();
            optionsBuilder.UseSqlServer(connectionString);
            await using var context = new IdentityContext(optionsBuilder.Options);
            var globalCacheService = ServiceProvider.GetRequiredService<IGlobalCacheService>();

            var user = await context.Users.FirstOrDefaultAsync(x => x.Id.Equals(transaction.Id));
            if (user is null)
            {
                _isProcessing = false;
                return;
            }

            user.Amount += transaction.Amount;
            context.Users.Update(user);
            await context.SaveChangesAsync();

            var userRedisKey = RedisKeys.KeySettings<IdentityUserValue>(transaction.Id);
            var cachedUser = await globalCacheService.GetAsync<IdentityUserValue>(userRedisKey);
            if (cachedUser is not null)
            {
                cachedUser.Amount = user.Amount;
                await globalCacheService.SetAsync(userRedisKey, cachedUser);
            }
            
            var emailRedisKey = RedisKeys.KeySettings<IdentityUserValue>(user.Email);
            var cachedEmail = await globalCacheService.GetAsync<IdentityUserValue>(emailRedisKey);
            if (cachedEmail is not null)
            {
                cachedEmail.Amount = user.Amount;
                await globalCacheService.SetAsync(emailRedisKey, cachedEmail);
            }

            _isProcessing = false;
        }
        catch (Exception)
        {
            _isProcessing = false;
        }
    }
}