﻿using System.Diagnostics;
using Novanet.Core.RedisCache;

namespace Service.Identity.Events;

public class CacheIdentityService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateServiceIdentityCache;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<CacheIdentityService> _logger;

    public CacheIdentityService(IServiceProvider serviceProvider,
        ILogger<CacheIdentityService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        var appSettings = ServiceProvider.GetRequiredService<AppSettings>();
        var connectionString = appSettings.ConnectionStrings.DefaultConnection;

        var optionsBuilder = new DbContextOptionsBuilder<IdentityContext>();
        optionsBuilder.UseSqlServer(connectionString);
        await using var context = new IdentityContext(optionsBuilder.Options);
        var redisService = ServiceProvider.GetRequiredService<IGlobalCacheService>();

        _isProcessing = true;
        try
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _logger.LogInformation("{Time} Bắt đầu cập nhật dữ liệu cache 'Service.Identity'", DateTimeOffset.Now);

            var applicationUsers = await context.Users
                .AsNoTracking()
                .Include(x => x.UserRoles)!
                .ThenInclude(x => x.Role).ThenInclude(x => x!.RoleClaims)
                .Include(x => x.Tokens)
                .Include(x => x.Claims)
                .ToListAsync();
            
            // remove deleted or non existing keys
            var existingKeys = await redisService.ListKeyByPrefixAsync(RedisPrefixKey.PrefixSettings<IdentityUserValue>());
            foreach (var existingKey in existingKeys)
            {
                if (applicationUsers.All(x => $"Novanet.v6.{RedisDatabases.ServiceIdentity}.{nameof(IdentityUserValue)}.{x.Id}" != existingKey))
                {
                    await redisService.KeyDeleteAsync(new RedisKey
                    {
                        Database = (int) RedisDatabases.ServiceSettings,
                        KeyName = existingKey
                    });
                }
                if (applicationUsers.All(x => $"Novanet.v6.{RedisDatabases.ServiceIdentity}.{nameof(IdentityUserValue)}.{x.Email}" != existingKey))
                {
                    await redisService.KeyDeleteAsync(new RedisKey
                    {
                        Database = (int) RedisDatabases.ServiceSettings,
                        KeyName = existingKey
                    });
                }
            }

            var identityUsers = new List<IdentityUserValue>();
            foreach (var applicationUser in applicationUsers)
            {
                var identityUserValue = ApplicationUser.ConvertToUserValue(applicationUser);
                await redisService.SetAsync(RedisKeys.KeySettings<IdentityUserValue>(applicationUser.Id),
                    identityUserValue);
                await redisService.SetAsync(RedisKeys.KeySettings<IdentityUserValue>(applicationUser.Email),
                    identityUserValue);
                identityUsers.Add(identityUserValue);
            }

            await WriteCache(identityUsers, appSettings);

            stopwatch.Stop();
            _logger.LogInformation("{Time} Cập nhật dữ liệu cache 'Service.Identity' thành công",
                stopwatch.Elapsed.TotalMinutes);
        }
        catch (Exception e)
        {
            var errorMessage = e.Message == string.Empty ? "Có lỗi không xác định xảy ra" : e.Message;
            _logger.LogError("{Time} Error: {Message}", DateTimeOffset.Now, errorMessage);
        }
        finally
        {
            _isProcessing = false;
        }
    }
    
    private static async Task WriteCache<T>(List<T> data, AppSettings appSettings)
    {
        var cacheFolder = Path.Combine(appSettings.CacheRootPath, "Identity");
        if (!Directory.Exists(cacheFolder))
        {
            Directory.CreateDirectory(cacheFolder);
        }
        var cacheFilePath = Path.Combine(cacheFolder, $"{typeof(T).Name}.json");
        await using var fileStream = new FileStream(cacheFilePath, FileMode.OpenOrCreate, FileAccess.Write);
        await using var streamWriter = new StreamWriter(fileStream);
        await streamWriter.WriteAsync(data.Serialize());
    }
}