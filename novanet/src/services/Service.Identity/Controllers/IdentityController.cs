using Novanet.Core.Authorize;
using Novanet.Core.Constants;
using Service.Identity.Application.Queries;

namespace Service.Identity.Controllers;

[Route("identity/api/v1/[action]")]
public class IdentityController : NovanetController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.Identity, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListUserQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Identity, nameof(Get))]
    public async Task<IActionResult> Get([FromQuery] GetUserQuery query)
    {
        return Ok(await Mediator.Send(query));
    }

    [HttpGet]
    [NovanetAccessControl(Aggregates.Identity, nameof(Me))]
    public async Task<IActionResult> Me()
    {
        return Ok(await Mediator.Send(new MySelfQuery()));
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> SignIn(SignInCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Identity, nameof(SignUp))]
    public async Task<IActionResult> SignUp(SignUpCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Identity, nameof(Update))]
    public async Task<IActionResult> Update(UpdateUserCommand command)
    {
        return Ok(await Mediator.Send(command));
    }
    
    [HttpPut]
    [NovanetAccessControl(Aggregates.Identity, nameof(UpdatePassword))]
    public async Task<IActionResult> UpdatePassword(UpdatePasswordCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPost]
    public async Task<IActionResult> RefreshToken()
    {
        return Ok(await Mediator.Send(new RefreshTokenQuery()));
    }
}