using Novanet.Core.Authorize;
using Novanet.Core.Constants;
using Service.Identity.Application.Queries;

namespace Service.Identity.Controllers;

[Route("identity/api/v1/publisher/[action]")]
public class PublisherController : NovanetController
{
    [HttpGet]
    [NovanetAccessControl(Aggregates.Publisher, nameof(List))]
    public async Task<IActionResult> List([FromQuery] ListPublisherQuery query)
    {
        return Ok(await Mediator.Send(query));
    }
}