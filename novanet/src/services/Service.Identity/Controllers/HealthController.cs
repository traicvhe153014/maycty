namespace Service.Identity.Controllers;

[ApiController]
[Route("identity/health")]
public class HealthController : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> Health()
    {
        return Ok(await Task.FromResult("OK"));
    }
}