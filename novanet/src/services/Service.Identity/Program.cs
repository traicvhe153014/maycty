using NovanetCore.Business;
using NovanetCore.Business.BusinessServices;
using Service.Identity.Events;
using Service.Identity.MigrateData;
using Service.Identity.Services;

WebApplication.CreateBuilder(args).NovanetCore<IdentityContext>(IdentityContext.Schema,
    out var app, (services, appSettings) =>
    {
        services.AddIdentity<ApplicationUser, ApplicationRole>(o =>
            {
                o.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+/ ";
                o.User.RequireUniqueEmail = true;
                o.Password.RequireDigit = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequireUppercase = false;
                o.Password.RequiredLength = 6;
            })
            .AddEntityFrameworkStores<IdentityContext>()
            .AddDefaultTokenProviders();
        services.AddGrpc();
        services.AddCacheManagerServices();
        services.AddRabbitMQ(appSettings.RabbitMQ);
        services.AddHostedService<CacheIdentityService>();
        services.AddHostedService<TransactionValidatorService>();
        services.AddScoped<IApplicationUserService, ApplicationUserService>();
        services.AddScoped<IReportService, ReportService>();
    }, application =>
    {
        application.MapGet("/", () => $"{IdentityContext.Schema} - Hello World!");
        application.MigrateDatabase<IdentityContext>((context, services) =>
        {
            var logger = services.GetService<ILogger<IdentityContextSeed>>();
            IdentityContextSeed.SeedAsync(context, logger, services).Wait();
        });
        var appSettings = application.Services.GetRequiredService<AppSettings>();
        appSettings.Kestrel.Endpoints.TryGetValue("gRPC", out var kestrelConfiguration);
        var grpcUrl = kestrelConfiguration?.Url ?? string.Empty;
        var grpcUri = new Uri(grpcUrl);
        application.UseWhen(context => context.Connection.LocalPort == grpcUri.Port, builder =>
        {
            builder.UseRouting();
            builder.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GrpcCacheService>();
            });
        });
    });

app.Run();