#!/bin/bash
echo "Start migrating IPLocations"
for filename in MigrateData/IPLocations/*.txt; do
	cat "$filename" | redis-cli --pipe
	echo "File $filename migrated"
done
echo "Migrating IPLocations finished!"
