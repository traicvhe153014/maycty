using Novanet.EventBus;
using NovanetCore.Business.BusinessServices;
using Service.Sharing.Events;
using Service.Sharing.MigrateData;

WebApplication.CreateBuilder(args).NovanetCore<SharingContext>(SharingContext.Schema,
    out var app, (services, appSettings) =>
    {
        services.AddGrpc();
        services.AddSingleton(appSettings);
        services.AddRabbitMQ(appSettings.RabbitMQ);
        services.AddHostedService<CacheSharingService>();
        services.AddHostedService<SharingContextService>();
    }, application =>
    {
        application.MapGet("/", () => $"{SharingContext.Schema} - Hello World!");
        application.MigrateDatabase<SharingContext>((context, services) =>
        {
            var logger = services.GetService<ILogger<SharingContextSeed>>();
            SharingContextSeed.SeedAsync(context, logger).Wait();
        });
        var appSettings = application.Services.GetRequiredService<AppSettings>();
        appSettings.Kestrel.Endpoints.TryGetValue("gRPC", out var kestrelConfiguration);
        var grpcUrl = kestrelConfiguration?.Url ?? string.Empty;
        var grpcUri = new Uri(grpcUrl);
        application.UseWhen(context => context.Connection.LocalPort == grpcUri.Port, builder =>
        {
            builder.UseRouting();
            builder.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GrpcCacheService>();
            });
        });
    });
app.Run();