﻿namespace Service.Sharing.MigrateData;

public static class MigrateGender
{
    public static List<Gender> Genders => new()
    {
        new Gender {Name = "Nam"},
        new Gender {Name = "Nữ"},
        new Gender {Name = "Không xác đinh"},
    };
}