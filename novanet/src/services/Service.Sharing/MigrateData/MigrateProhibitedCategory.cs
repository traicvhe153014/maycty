﻿
namespace Service.Sharing.MigrateData;

public class MigrateProhibitedCategory
{
    public static List<ProhibitedCategory> ProhibitedCategories = new()
    {
        new ProhibitedCategory {Name = "Bia"},
        new ProhibitedCategory {Name = "Bikini"},
        new ProhibitedCategory {Name = "Đồ lót"},
        new ProhibitedCategory {Name = "Game có giấy phép"},
        new ProhibitedCategory {Name = "Game không có giấy phép"},
        new ProhibitedCategory {Name = "Giải thưởng"},
        new ProhibitedCategory {Name = "Hình ảnh nhạy cảm"},
        new ProhibitedCategory {Name = "Khuyến mại trên 50%"},
        new ProhibitedCategory {Name = "Nội dung banner là tiếng Anh"},
        new ProhibitedCategory {Name = "Rượu"},
        new ProhibitedCategory {Name = "Sextoy - Thuốc kích dục"},
        new ProhibitedCategory {Name = "Thẩm mỹ viện không có giấy phép"},
        new ProhibitedCategory {Name = "Thực phẩm chức năng"},
        new ProhibitedCategory {Name = "Thuốc lá"},
        new ProhibitedCategory {Name = "Thuốc lá điện tử"},
        new ProhibitedCategory {Name = "Thuốc sinh lý nam, nữ"},
        new ProhibitedCategory {Name = "Vũ khí"},
    };
}