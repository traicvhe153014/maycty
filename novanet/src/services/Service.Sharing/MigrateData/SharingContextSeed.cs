﻿namespace Service.Sharing.MigrateData;

public class SharingContextSeed
{
    public static async Task SeedAsync(SharingContext context,
        ILogger<SharingContextSeed>? logger)
    {
        var policy = CreatePolicy(logger, nameof(SharingContextSeed));

        await policy.ExecuteAsync(async () =>
        {
            if (!await context.Ages.AnyAsync())
                await context.Ages.AddRangeAsync(MigrateAge.Ages);

            if (!await context.Categories.AnyAsync())
                await context.Categories.AddRangeAsync(MigrateCategory.Categories);

            if (!await context.Locations.AnyAsync())
                await context.Locations.AddRangeAsync(MigrateLocation.Locations);

            if (!await context.Genders.AnyAsync())
                await context.Genders.AddRangeAsync(MigrateGender.Genders);
                
            if (!await context.ProhibitedCategories.AnyAsync())
                await context.ProhibitedCategories.AddRangeAsync(MigrateProhibitedCategory.ProhibitedCategories);

            await context.SaveChangesAsync(true);
        });
    }

    private static AsyncRetryPolicy CreatePolicy(ILogger? logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().WaitAndRetryAsync(
            retryCount: retries,
            sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
            onRetry: (exception, timeSpan, retry, ctx) =>
            {
                logger?.LogWarning(exception,
                    "[{Prefix}] Exception {ExceptionType} with message {Message} detected on attempt {Retry} of {Retries}",
                    prefix, exception.GetType().Name, exception.Message, retry, retries);
            }
        );
    }
}