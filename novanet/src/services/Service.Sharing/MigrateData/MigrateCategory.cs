﻿namespace Service.Sharing.MigrateData;

public static class MigrateCategory
{
    private static readonly Dictionary<string, List<CategoryVariety>> CategoryVarieties = new()
    {
        {
            "Media", new List<CategoryVariety>
            {
                new() {Name = "LAO ĐỘNG TV"},
                new() {Name = "HYPER TEXT"},
                new() {Name = "PHOTO PODCAST"},
                new() {Name = "INFOGRAPHICS"},
                new() {Name = "EMAGAGINZE"},
                new() {Name = "MULTIMEDIA"},
                new() {Name = "ẢNH-CLIP HAY"},
                new() {Name = "VISUAL STORY"}
            }
        },
        {
            "Xe", new List<CategoryVariety>
            {
                new() {Name = "XE360"},
                new() {Name = "XE MỚI"},
                new() {Name = "SAU TAY LÁI"},
                new() {Name = "ĐÁNH GIÁ XE"},
                new() {Name = "THỊ TRƯỜNG XE"},
                new() {Name = "LÁI XE AN TOÀN"},
                new() {Name = "SUPER CAR - SIÊU XE"},
                new() {Name = "KINH NGHIỆM CẦM LÁI"},
                new() {Name = "THỊ TRƯỜNG XE"},
                new() {Name = "XE ĐIỆN"}
            }
        },
        {
            "Thể thao", new List<CategoryVariety>
            {
                new() {Name = "TRONG NƯỚC"},
                new() {Name = "QUỐC TẾ"},
                new() {Name = "CÁC MÔN KHÁC"},
                new() {Name = "WOLD CUP QUATAR 2022"},
                new() {Name = "BÓNG ĐÁ VIỆT NAM"},
                new() {Name = "BÓNG ĐÁ QUỐC TẾ"},
                new() {Name = "TƯỜNG THUẬT BÓNG ĐÁ"},
                new() {Name = "DỮ LIỆU BÓNG ĐÁ"},
                new() {Name = "VIDEO THỂ THAO"},
                new() {Name = "GOFT"},
                new() {Name = "TENNIS"},
                new() {Name = "CHUYỂN NHƯỢNG"},
                new() {Name = "ESPORTS"},
                new() {Name = "CHAMPIONS LEAGUE"},
                new() {Name = "EUROPA LEAGUE"},
                new() {Name = "NHẬN ĐỊNH BÓNG ĐÁ"},
                new() {Name = "BÓNG CHUYỀN"},
                new() {Name = "PHÍA SAU SÂN CỎ"},
                new() {Name = "VÕ THUẬT"},
                new() {Name = "LỊCH THI ĐẤU"},
            }
        },
        {
            "Đô thị", new List<CategoryVariety>
            {
                new() {Name = "GIAO THÔNG"},
                new() {Name = "MÔI TRƯỜNG"},
                new() {Name = "ĐÔ THỊ & CUỘC SỐNG"}
            }
        },
        {
            "Bất động sản", new List<CategoryVariety>
            {
                new() {Name = "NHÀ ĐẸP"},
                new() {Name = "ĐỊA ỐC"},
                new() {Name = "TƯ VẤN ĐẦU TƯ"},
                new() {Name = "PHONG THỦY"},
                new() {Name = "KIẾN TRÚC-NỘI THẤT"}
            }
        },
        {
            "Thời sự", new List<CategoryVariety>
            {
                new() {Name = "THEO DÒNG THỜI SỰ"},
                new() {Name = "CHÍNH KIẾN"},
                new() {Name = "CÙNG LÊN TIẾNG"},
                new() {Name = "CHÍNH TRỊ"},
                new() {Name = "QUỐC HỘI"},
                new() {Name = "AN TOÀN GIAO THÔNG"},
                new() {Name = "BHYT-BHXH"},
                new() {Name = "CHỐNG THAM NHŨNG"},
                new() {Name = "QUỐC PHÒNG"},
                new() {Name = "MÔI TRƯỜNG"},
                new() {Name = "NGHỊ QUYẾT ĐẢNG VÀO CUỘC SỐNG"},
                new() {Name = "THÔNG TIN ĐỐI NGOẠI"},
                new() {Name = "KỶ CƯƠNG HÀNH CHÍNH"},
                new() {Name = "NHÂN SỰ"},
                new() {Name = "KÍNH ĐA TRÒNG"}
            }
        },
        {
            "Du lịch", new List<CategoryVariety>
            {
                new() {Name = "ĐI ĐÂU CHƠI ĐI"},
                new() {Name = "NGỦ NGỦ NGHỈ NGHỈ"},
                new() {Name = "SĂN TOUR"},
                new() {Name = "CHECK-IN"},
                new() {Name = "TOUR HAY-KHUYẾN MÃI"},
                new() {Name = "MÓN NGON-ĐIỂM ĐẸP"}
            }
        },
        {
            "Giáo dục", new List<CategoryVariety>
            {
                new() {Name = "TUYỂN SINH"},
                new() {Name = "VIỆC LÀM"},
                new() {Name = "NGƯỜI THẦY"},
                new() {Name = "DU HỌC"},
                new() {Name = "GƯƠNG MẶT TRẺ"},
                new() {Name = "GÓC PHỤ HUYNH"},
                new() {Name = "KHOA HỌC"},
                new() {Name = "CỔNG TRƯỜNG"},
                new() {Name = "KỂ CHUYỆN CHO BÉ"},
                new() {Name = "CỬA SỔ TÌNH YÊU"},
                new() {Name = "HẠT GIỐNG TÂM HỒN"},
                new() {Name = "ĐỌC TRUYỆN ĐÊM KHUYA"},
                new() {Name = "KHUYẾN HỌC"},
                new() {Name = "GƯƠNG SÁNG"},
                new() {Name = "GIÁO DỤC-NGHỀ NGHIỆP"},
                new() {Name = "THÔNG TIN UNESCO"}
            }
        },
        {
            "An ninh", new List<CategoryVariety>
            {
                new() {Name = "HỒ SƠ PHÁ ÁN"},
                new() {Name = "TRUY NÃ"},
                new() {Name = "QUÂN SỰ"},
                new() {Name = "CÔNG ĐOÀN"},
                new() {Name = "VŨ KHÍ"}
            }
        },
        {
            "Nhà nông", new List<CategoryVariety>
            {
                new() {Name = "TIN NÔNG NGHIỆP"},
                new() {Name = "NGON-SẠCH-LẠ"},
                new() {Name = "THỊ TRƯỜNG NÔNG SẢN"},
                new() {Name = "KHUYẾN NÔNG"},
                new() {Name = "NÔNG THÔN MỚI"},
                new() {Name = "CHỢ VIỆT XUA NAY"},
                new() {Name = "ĐỊA CHỈ XANH"},
                new() {Name = "MÔI TRƯỜNG XANH"},
                new() {Name = "NÔNG DÂN PHỐ"}
            }
        },
        {
            "Pháp luật", new List<CategoryVariety>
            {
                new() {Name = "CHÍNH SÁCH MỚI"},
                new() {Name = "LUẬT VÀ ĐỜI"},
                new() {Name = "VỤ ÁN"},
                new() {Name = "HỒ SƠ VỤ ÁN"},
                new() {Name = "TƯ VẤN PHÁP LUẬT"},
                new() {Name = "KÝ SỰ PHÁP ĐÌNH"}
            }
        },
        {
            "Nhịp sống trẻ", new List<CategoryVariety>
            {
                new() {Name = "GIỚI TRẺ"},
                new() {Name = "TÀI NĂNG TRẺ"},
                new() {Name = "HỌC ĐƯỜNG"},
                new() {Name = "NÓNG TRÊN MẠNG"},
                new() {Name = "NHỊP SỐNG"},
                new() {Name = "CẨM NANG TEEN"}
            }
        },
        {
            "Khoa học", new List<CategoryVariety>
            {
                new() {Name = "CÔNG NGHỆ QUÂN SỰ"},
                new() {Name = "CÔNG NGHỆ"},
                new() {Name = "THẾ GIỚI SỐ"},
                new() {Name = "VI TÍNH"},
                new() {Name = "ĐIỆN THOẠI"},
                new() {Name = "PHẦN MỀM-BẢO MẬT"},
                new() {Name = "DI ĐỘNG-VIỄN THÔNG"},
                new() {Name = "CỘNG ĐỒNG MẠNG"},
            }
        },
        {
            "Ẩm thực", new List<CategoryVariety>
            {
                new() {Name = "MÓN NGON NHÀ HÀNG"},
                new() {Name = "MÓN NGON GIA ĐÌNH"},
                new() {Name = "BÍ QUYẾT NẤU ĂN"}
            }
        },
        {
            "Kinh doanh", new List<CategoryVariety>
            {
                new() {Name = "KINH TẾ"},
                new() {Name = "PHÁP LÝ 4.0"},
                new() {Name = "QUẢN LÝ DOANH NGHIỆP - CỘNG ĐỒNG"},
                new() {Name = "DU LỊCH"},
                new() {Name = "THỊ TRƯỜNG"},
                new() {Name = "TÀI CHÍNH"},
                new() {Name = "BẤT ĐỘNG SẢN"},
                new() {Name = "DOANH NHÂN"},
                new() {Name = "TƯ VẤN TÀI CHÍNH"},
                new() {Name = "DOANH NGHIỆP"},
                new() {Name = "CHỨNG KHOÁN"},
                new() {Name = "CHUYỂN ĐỘNG THỊ TRƯỜNG"},
                new() {Name = "TÀI CHÍNH NGÂN HÀNG"},
                new() {Name = "CHUYỆN LÀM ĂN"},
                new() {Name = "ĐẦU TƯ-TÀI CHÍNH"}
            }
        },
        {
            "Văn hóa", new List<CategoryVariety>
            {
                new() {Name = "XEM-NGHE-ĐỌC"},
                new() {Name = "ĐỌC-XEM"},
                new() {Name = "ĐƯƠNG THỜI"},
                new() {Name = "HUYỀN THOẠI"},
                new() {Name = "XÃ HỘI"},
                new() {Name = "TIN VĂN HÓA"},
                new() {Name = "CÂU CHUYỆN VĂN HÓA"},
                new() {Name = "NHÂN VẬT-TÁC PHẨM"},
                new() {Name = "VẤN ĐỀ"},
                new() {Name = "SÁNG TÁC"},
                new() {Name = "RUBIK VĂN HÓA"},
                new() {Name = "SÂN KHẤU ĐIỆN ẢNH"},
                new() {Name = "DI SẢN"}
            }
        },
        {
            "Sức khỏe", new List<CategoryVariety>
            {
                new() {Name = "LÀM ĐẸP"},
                new() {Name = "THỜI TRANG"},
                new() {Name = "TƯ VẤN SỨC KHỎE"},
                new() {Name = "CÁC LOẠI BỆNH"},
                new() {Name = "ĐÀN ÔNG"},
                new() {Name = "KHỎE-ĐẸP"},
                new() {Name = "PHỤ NỮ ĐẸP"},
                new() {Name = "SỨC KHỎE VÀ GIỚI TÍNH"},
                new() {Name = "DINH DƯỠNG-ẨM THỰC"},
                new() {Name = "CÁC LOẠI BỆNH"},
                new() {Name = "BÁC SĨ ONLINE"},
                new() {Name = "THUỐC ĐÚNG-THUỐC TỐT"},
                new() {Name = "LÀM ĐẸP MỖI NGÀY"},
                new() {Name = "THÌ THẦM BÊN GỐI"},
                new() {Name = "TIN Y TẾ"},
                new() {Name = "ĐẸP HƠN MỖI NGÀY"},
                new() {Name = "VÀO BẾP"},
                new() {Name = "ĐẸP"},
                new() {Name = "TRANG ĐIỂM"},
                new() {Name = "SẢN PHỤ KHOA"},
                new() {Name = "NAM KHOA"},
                new() {Name = "NHI KHOA"},
                new() {Name = "LÀM ĐẸP-GIẢM CÂN"},
                new() {Name = "ĂN SẠCH SỐNG KHỎE"},
                new() {Name = "UNG THƯ"},
                new() {Name = "SỐNG KHỎE"},
            }
        },
        {
            "Đời sống", new List<CategoryVariety>
            {
                new() {Name = "GIA ĐÌNH"},
                new() {Name = "CHUYỆN LẠ"},
                new() {Name = "ẨM THỰC"},
                new() {Name = "GIỚI TRẺ"},
                new() {Name = "MẸO VẶT"},
                new() {Name = "TÂM SỰ"},
                new() {Name = "NHỊP SỐNG"},
                new() {Name = "ĐỜI THƯỜNG"},
                new() {Name = "THỜI TIẾT"},
                new() {Name = "CHUYỆN NHÀ"},
                new() {Name = "CÙNG CON TRƯỞNG THÀNH"},
                new() {Name = "LIFESTYLE"},
                new() {Name = "GENZ"},
                new() {Name = "TÌNH YÊU-HÔN NHÂN"},
                new() {Name = "CHUYỆN NHÀ"},
                new() {Name = "CHA MẸ VÀ CON"},
                new() {Name = "TÌNH VÀ LÝ"},
                new() {Name = "PHONG CÁCH SỐNG"},
                new() {Name = "NHỎ TO TÂM SỰ"},
                new() {Name = "CHIA NHỮNG LỖI BUỒN"}
            }
        },
        {
            "Công nghệ", new List<CategoryVariety>
            {
                new() {Name = "THÔNG TIN VÀ TRUYỀN THÔNG"},
                new() {Name = "CHUYỂN ĐỔI SỐ"},
                new() {Name = "SẢN PHẨM"},
                new() {Name = "BLOCKCHAIN"},
                new() {Name = "TIN ICT"},
                new() {Name = "GAME"},
                new() {Name = "NHỊP SỐNG SỐ"},
                new() {Name = "TABLET-LAPTOP-MÁY TÍNH"},
                new() {Name = "THỦ THUẬT CÔNG NGHỆ"}
            }
        },
        {
            "Giải trí", new List<CategoryVariety>
            {
                new() {Name = "THẾ GIỚI SAO"},
                new() {Name = "THỜI TRANG"},
                new() {Name = "NHẠC"},
                new() {Name = "PHIM"},
                new() {Name = "TRUYỀN HÌNH"},
                new() {Name = "SÁCH"},
                new() {Name = "DI SẢN-MỸ THUẬT-SÂN KHẤU"},
                new() {Name = "SAO"},
                new() {Name = "SHOWBIZ"},
                new() {Name = "ĂN-ĐI-CHILL"},
                new() {Name = "SAO THẾ GIỚI"},
                new() {Name = "NGHỆ SĨ"},
                new() {Name = "GAMESHOW"}
            }
        },
        {
            "Quốc tế", new List<CategoryVariety>
            {

                new() {Name = "SỰ KIỆN"},
                new() {Name = "KIỀU BÀO"},
                new() {Name = "CHUYÊN GIA"},
                new() {Name = "TƯ LIỆU"},
                new() {Name = "QUÂN SỰ"},
                new() {Name = "MUÔN MẶT"},
                new() {Name = "THẾ GIỚI"},
                new() {Name = "BÌNH LUẬN QUỐC TẾ"},
                new() {Name = "THẾ GIỚI ĐÓ ĐÂY"},
                new() {Name = "VIỆT NAM VÀ THẾ GIỚI"},
                new() {Name = "QUÂN SỰ"},
                new() {Name = "GƯƠNG MẶT QUỐC TẾ"},
                new() {Name = "ĐỐI NGOẠI"},
                new() {Name = "CỘNG ĐỒNG NGƯỜI VIỆT"},
                new() {Name = "THẾ GIỚI LẠ KỲ"},
                new() {Name = "CUỘC SỐNG ĐÓ ĐÂY"},
                new() {Name = "NÔNG SẢN ĐỘC-LẠ THẾ GIỚI"},
                new() {Name = "KIỀU BÀO"}
            }
        },
        {
            "Xã hội", new List<CategoryVariety>
            {
                new() {Name = "CHÍNH TRỊ"},
                new() {Name = "TIN TỨC"},
                new() {Name = "CHUYỆN HÔM NAY"},
                new() {Name = "PHÓNG SỰ"},
                new() {Name = "NHỊP SỐNG"},
                new() {Name = "BIỂN ĐẢO"},
                new() {Name = "HỌC TẬP BÁC"},
                new() {Name = "CHỢ ONLINE"},
                new() {Name = "CHUYỆN LẠ"},
                new() {Name = "THƯỢNG LƯU"}
            }
        }
    };

    public static List<Category> Categories => new()
    {
        new Category
        {
            Name = "Media",
            CategoryVarieties = CategoryVarieties["Media"],
            Slug = Category.GetCategorySlug("Media", CategoryVarieties["Media"])
        },
        new Category
        {
            Name = "Xe",
            CategoryVarieties = CategoryVarieties["Xe"],
            Slug = Category.GetCategorySlug("Xe", CategoryVarieties["Xe"])
        },
        new Category
        {
            Name = "Thể thao",
            CategoryVarieties = CategoryVarieties["Thể thao"],
            Slug = Category.GetCategorySlug("Thể thao", CategoryVarieties["Thể thao"])
        },
        new Category
        {
            Name = "Đô thị",
            CategoryVarieties = CategoryVarieties["Đô thị"],
            Slug = Category.GetCategorySlug("Đô thị", CategoryVarieties["Đô thị"])
        },
        new Category
        {
            Name = "Bất động sản",
            CategoryVarieties = CategoryVarieties["Bất động sản"],
            Slug = Category.GetCategorySlug("Bất động sản", CategoryVarieties["Bất động sản"])
        },
        new Category
        {
            Name = "Thời sự",
            CategoryVarieties = CategoryVarieties["Thời sự"],
            Slug = Category.GetCategorySlug("Thời sự", CategoryVarieties["Thời sự"])
        },
        new Category
        {
            Name = "Du lịch",
            CategoryVarieties = CategoryVarieties["Du lịch"],
            Slug = Category.GetCategorySlug("Du lịch", CategoryVarieties["Du lịch"])
        },
        new Category
        {
            Name = "Giáo dục",
            CategoryVarieties = CategoryVarieties["Giáo dục"],
            Slug = Category.GetCategorySlug("Giáo dục", CategoryVarieties["Giáo dục"])
        },
        new Category
        {
            Name = "An ninh",
            CategoryVarieties = CategoryVarieties["An ninh"],
            Slug = Category.GetCategorySlug("An ninh", CategoryVarieties["An ninh"])
        },
        new Category
        {
            Name = "Nhà nông",
            CategoryVarieties = CategoryVarieties["Nhà nông"],
            Slug = Category.GetCategorySlug("Nhà nông", CategoryVarieties["Nhà nông"])
        },
        new Category
        {
            Name = "Pháp luật",
            CategoryVarieties = CategoryVarieties["Pháp luật"],
            Slug = Category.GetCategorySlug("Pháp luật", CategoryVarieties["Pháp luật"])
        },
        new Category
        {
            Name = "Nhịp sống trẻ",
            CategoryVarieties = CategoryVarieties["Nhịp sống trẻ"],
            Slug = Category.GetCategorySlug("Nhịp sống trẻ", CategoryVarieties["Nhịp sống trẻ"])
        },
        new Category
        {
            Name = "Khoa học",
            CategoryVarieties = CategoryVarieties["Khoa học"],
            Slug = Category.GetCategorySlug("Khoa học", CategoryVarieties["Khoa học"])
        },
        new Category
        {
            Name = "Ẩm thực",
            CategoryVarieties = CategoryVarieties["Ẩm thực"],
            Slug = Category.GetCategorySlug("Ẩm thực", CategoryVarieties["Ẩm thực"])
        },
        new Category
        {
            Name = "Kinh doanh",
            CategoryVarieties = CategoryVarieties["Kinh doanh"],
            Slug = Category.GetCategorySlug("Kinh doanh", CategoryVarieties["Kinh doanh"])
        },
        new Category
        {
            Name = "Văn hóa",
            CategoryVarieties = CategoryVarieties["Văn hóa"],
            Slug = Category.GetCategorySlug("Văn hóa", CategoryVarieties["Văn hóa"])
        },
        new Category
        {
            Name = "Sức khỏe",
            CategoryVarieties = CategoryVarieties["Sức khỏe"],
            Slug = Category.GetCategorySlug("Sức khỏe", CategoryVarieties["Sức khỏe"])
        },
        new Category
        {
            Name = "Đời sống",
            CategoryVarieties = CategoryVarieties["Đời sống"],
            Slug = Category.GetCategorySlug("Đời sống", CategoryVarieties["Đời sống"])
        },
        new Category
        {
            Name = "Công nghệ",
            CategoryVarieties = CategoryVarieties["Công nghệ"],
            Slug = Category.GetCategorySlug("Công nghệ", CategoryVarieties["Công nghệ"])
        },
        new Category
        {
            Name = "Giải trí",
            CategoryVarieties = CategoryVarieties["Giải trí"],
            Slug = Category.GetCategorySlug("Giải trí", CategoryVarieties["Giải trí"])
        },
        new Category
        {
            Name = "Quốc tế",
            CategoryVarieties = CategoryVarieties["Quốc tế"],
            Slug = Category.GetCategorySlug("Quốc tế", CategoryVarieties["Quốc tế"])
        },
        new Category
        {
            Name = "Xã hội",
            CategoryVarieties = CategoryVarieties["Xã hội"],
            Slug = Category.GetCategorySlug("Xã hội", CategoryVarieties["Xã hội"])
        },
        new Category
        {
            Name = "Khác",
            CategoryVarieties = new List<CategoryVariety>(),
            Slug = ""
        }
    };
}