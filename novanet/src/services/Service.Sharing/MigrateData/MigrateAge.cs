namespace Service.Sharing.MigrateData;

public class MigrateAge
{
    public static List<Age> Ages => new()
    {
        new Age {Name = "<18", From = 0, To = 17},
        new Age {Name = "18-24", From = 18, To = 24},
        new Age {Name = "25-34", From = 25, To = 34},
        new Age {Name = "35-50", From = 35, To = 50},
        new Age {Name = ">50", From = 50, To = 100},
    };
}