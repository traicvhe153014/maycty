﻿namespace Service.Sharing.Factories;

public class DbContextFactory : IDesignTimeDbContextFactory<SharingContext>
{
    public SharingContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.dev.json")
            .Build();

        var appSettings = new AppSettings();
        configuration.Bind(appSettings);

        var optionsBuilder = new DbContextOptionsBuilder<SharingContext>();
        optionsBuilder.UseSqlServer(appSettings.ConnectionStrings.DefaultConnection, m =>
            {
                m.MigrationsAssembly(SharingContext.Schema);
                m.MigrationsHistoryTable("__EFMigrationsHistory", SharingContext.Schema);
            }
        );
        return new SharingContext(optionsBuilder.Options, appSettings);
    }
}