﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Sharing.Migrations
{
    public partial class UpdateIpLocationNullableField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "VnRegion",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "VnProvinceId",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "VnRegion",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VnProvinceId",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
