﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Sharing.Migrations
{
    public partial class UpdateIpLocationAndIpBlock : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Location",
                schema: "Service.Sharing",
                table: "IPBlocks",
                newName: "LocationId");

            migrationBuilder.AddColumn<string>(
                name: "CountryIsoCode",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "VnRegion",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountryIsoCode",
                schema: "Service.Sharing",
                table: "IPLocations");

            migrationBuilder.DropColumn(
                name: "VnRegion",
                schema: "Service.Sharing",
                table: "IPLocations");

            migrationBuilder.RenameColumn(
                name: "LocationId",
                schema: "Service.Sharing",
                table: "IPBlocks",
                newName: "Location");
        }
    }
}
