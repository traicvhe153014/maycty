﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Sharing.Migrations
{
    public partial class InitialDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_15E12262600E3912F4D7A92DF9D5FB86",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_3CD27E7375832CCD0D802BC80226D4C6",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_53FDCD38435350BA308D27F20273FE1D",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_7842E82DFFAE5F52C9A5D3A98B967765",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_9F907E5E92C584FD34A751246ABF2C1C",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_AFD4F60DE5D2FD2EFD61A0015E142D3C",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_B642E888A89FCD2D8E963CBAA8F312A4",
                schema: "Service.Sharing");

            migrationBuilder.CreateSequence<int>(
                name: "Sequence_FA0C76C7C26EE7BD645693D737018BED",
                schema: "Service.Sharing");

            migrationBuilder.CreateTable(
                name: "Ages",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_53FDCD38435350BA308D27F20273FE1D"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    From = table.Column<int>(type: "int", nullable: false),
                    To = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_3CD27E7375832CCD0D802BC80226D4C6"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Faqs",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_15E12262600E3912F4D7A92DF9D5FB86"),
                    Parent = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    ModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Priority = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faqs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genders",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_AFD4F60DE5D2FD2EFD61A0015E142D3C"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IPBlocks",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_B642E888A89FCD2D8E963CBAA8F312A4"),
                    Location = table.Column<long>(type: "bigint", nullable: false),
                    Start = table.Column<long>(type: "bigint", nullable: false),
                    End = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IPBlocks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IPLocations",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_7842E82DFFAE5F52C9A5D3A98B967765"),
                    LocationId = table.Column<long>(type: "bigint", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    VnProvinceId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IPLocations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_FA0C76C7C26EE7BD645693D737018BED"),
                    RegionKey = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    RegionName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProvinceName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProvinceId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProhibitedCategories",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_9F907E5E92C584FD34A751246ABF2C1C"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProhibitedCategories", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ages_SubId",
                schema: "Service.Sharing",
                table: "Ages",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_SubId",
                schema: "Service.Sharing",
                table: "Categories",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Faqs_SubId",
                schema: "Service.Sharing",
                table: "Faqs",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Genders_SubId",
                schema: "Service.Sharing",
                table: "Genders",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_IPBlocks_SubId",
                schema: "Service.Sharing",
                table: "IPBlocks",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_IPLocations_SubId",
                schema: "Service.Sharing",
                table: "IPLocations",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_SubId",
                schema: "Service.Sharing",
                table: "Locations",
                column: "SubId");

            migrationBuilder.CreateIndex(
                name: "IX_ProhibitedCategories_SubId",
                schema: "Service.Sharing",
                table: "ProhibitedCategories",
                column: "SubId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ages",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "Categories",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "Faqs",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "Genders",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "IPBlocks",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "IPLocations",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "Locations",
                schema: "Service.Sharing");

            migrationBuilder.DropTable(
                name: "ProhibitedCategories",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_15E12262600E3912F4D7A92DF9D5FB86",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_3CD27E7375832CCD0D802BC80226D4C6",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_53FDCD38435350BA308D27F20273FE1D",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_7842E82DFFAE5F52C9A5D3A98B967765",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_9F907E5E92C584FD34A751246ABF2C1C",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_AFD4F60DE5D2FD2EFD61A0015E142D3C",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_B642E888A89FCD2D8E963CBAA8F312A4",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_FA0C76C7C26EE7BD645693D737018BED",
                schema: "Service.Sharing");
        }
    }
}
