﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Sharing.Migrations
{
    public partial class UpdateSearchColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "ProhibitedCategories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "Locations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "IPBlocks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "Genders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "Faqs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "Categories",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Search",
                schema: "Service.Sharing",
                table: "Ages",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "ProhibitedCategories");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "IPLocations");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "IPBlocks");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "Genders");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "Faqs");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "Search",
                schema: "Service.Sharing",
                table: "Ages");
        }
    }
}
