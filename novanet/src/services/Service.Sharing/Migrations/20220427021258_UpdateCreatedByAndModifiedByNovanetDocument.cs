﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Sharing.Migrations
{
    public partial class UpdateCreatedByAndModifiedByNovanetDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "ProhibitedCategories",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "ProhibitedCategories",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Locations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Locations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "IPLocations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "IPBlocks",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "IPBlocks",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Genders",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Genders",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Faqs",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Faqs",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Categories",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Categories",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Ages",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Ages",
                type: "uniqueidentifier",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "ProhibitedCategories");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "ProhibitedCategories");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "IPLocations");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "IPLocations");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "IPBlocks");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "IPBlocks");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Genders");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Genders");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Faqs");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Faqs");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                schema: "Service.Sharing",
                table: "Ages");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                schema: "Service.Sharing",
                table: "Ages");
        }
    }
}
