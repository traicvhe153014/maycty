﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Sharing.Migrations
{
    public partial class AddCategoryVarietyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "Sequence_6275E574AAFF0DEEFC080C92251C37FD",
                schema: "Service.Sharing");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Service.Sharing",
                table: "Categories",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "CategoryVarieties",
                schema: "Service.Sharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Search = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SubId = table.Column<long>(type: "bigint", nullable: false, defaultValueSql: "NEXT VALUE FOR [Service.Sharing].Sequence_6275E574AAFF0DEEFC080C92251C37FD"),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryVarieties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryVarieties_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "Service.Sharing",
                        principalTable: "Categories",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryVarieties_CategoryId",
                schema: "Service.Sharing",
                table: "CategoryVarieties",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryVarieties_SubId",
                schema: "Service.Sharing",
                table: "CategoryVarieties",
                column: "SubId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CategoryVarieties",
                schema: "Service.Sharing");

            migrationBuilder.DropSequence(
                name: "Sequence_6275E574AAFF0DEEFC080C92251C37FD",
                schema: "Service.Sharing");

            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Service.Sharing",
                table: "Categories");
        }
    }
}
