﻿using Service.Sharing.Application.Queries;

namespace Service.Sharing.Controllers;

[Route("sharing/api/v1/[action]")]
[ResponseCache(Duration = 3600)]
public class SharingController : NovanetController
{
    [HttpGet]
    public async Task<IActionResult> Ages()
    {
        return Ok(await Mediator.Send(new ListAgeQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> Categories()
    {
        return Ok(await Mediator.Send(new ListCategoryQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> Cities()
    {
        return Ok(await Mediator.Send(new ListGenderQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> Faqs()
    {
        return Ok(await Mediator.Send(new ListGenderQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> Isps()
    {
        return Ok(await Mediator.Send(new ListGenderQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> ProductTypes()
    {
        return Ok(await Mediator.Send(new ListGenderQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> Genders()
    {
        return Ok(await Mediator.Send(new ListGenderQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> Locations()
    {
        return Ok(await Mediator.Send(new ListLocationQuery()));
    }

    [HttpGet]
    public async Task<IActionResult> ProhibitedCategories()
    {
        return Ok(await Mediator.Send(new ListProhibitedCategoryQuery()));
    }
}