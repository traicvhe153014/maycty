﻿using System.Diagnostics;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessConfigs;
using NovanetCore.Business.BusinessDomain.Service.Settings;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace Service.Sharing.Events;

public class CacheSharingService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateServiceSharingCache;
    protected override bool IsWorkQueue => true;

    private readonly ILogger<CacheSharingService> _logger;

    public CacheSharingService(IServiceProvider serviceProvider,
        ILogger<CacheSharingService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
    }


    protected override async Task Processing(object signal)
    {
        var context = ServiceProvider.GetRequiredService<SharingContext>();
        var redisService = ServiceProvider.GetRequiredService<IGlobalCacheService>();
        var appSettings = ServiceProvider.GetRequiredService<AppSettings>();

        _isProcessing = true;
        try
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _logger.LogInformation("Bắt đầu cập nhật dữ liệu cache 'Service.Sharing'");
            await Updating(context, redisService, _logger, appSettings);
            stopwatch.Stop();
            _logger.LogInformation("Cập nhật dữ liệu cache 'Service.Sharing' thành công. Thời gian thực thi: {Time}",
                stopwatch.Elapsed.TotalMinutes);
        }
        catch (Exception e)
        {
            var errorMessage = e.Message == string.Empty ? "Có lỗi không xác định xảy ra" : e.Message;
            _logger.LogError("{Time}: {Message}", DateTimeOffset.Now, errorMessage);
        }
        finally
        {
            _isProcessing = false;
        }
    }

    private static async Task Updating(SharingContext context, IGlobalCacheService globalCacheService, ILogger logger, AppSettings appSettings)
    {
        var ages = await context.Ages.ToListAsync();
        await WriteCache(ages.Cast<AgeCore>().ToList(), appSettings);

        var categories = await context.Categories.ToListAsync();
        await WriteCache(categories.Cast<CategoryCore>().ToList(), appSettings);

        var categoryVarieties = await context.CategoryVarieties.ToListAsync();
        await WriteCache(categoryVarieties.Cast<CategoryVarietyCore>().ToList(), appSettings);

        var faqs = await context.Faqs.ToListAsync();
        await WriteCache(faqs.Cast<FaqCore>().ToList(), appSettings);

        var locations = await context.Locations.ToListAsync();
        await WriteCache(locations.Cast<LocationCore>().ToList(), appSettings);

        var genders = await context.Genders.ToListAsync();
        await WriteCache(genders.Cast<GenderCore>().ToList(), appSettings);

        var prohibitedCategories = await context.ProhibitedCategories.ToListAsync();
        await WriteCache(prohibitedCategories.Cast<ProhibitedCategoryCore>().ToList(), appSettings);

        var ipLocations = await context.IPLocations.ToListAsync();
        await WriteCache(ipLocations.Cast<IPLocationCore>().ToList(), appSettings);
        
        var ipBlocks = await context.IPBlocks
            .Join(
                context.IPLocations,
                x => x.LocationId,
                x => x.LocationId,
                (block, location) => new {Block = block, Location = location})
            .Where(x => x.Location.CountryId == ConfigValues.VietnamCountryId)
            .Select(x => x.Block)
            .ToListAsync();
        await globalCacheService.RemoveNonExistingKeys<IPBlockCore, IPBlock>(ipBlocks, RedisDatabases.ServiceSharing);
        foreach (var ipBlock in ipBlocks)
        {
            await globalCacheService.SetAsync(RedisKeys.KeySharing<IPBlockCore>(ipBlock.SubId), ipBlock);
        
            var redisData = new Dictionary<RedisKey, string>();
            for (var i = ipBlock.Start; i <= ipBlock.End; i++)
            {
                redisData[RedisKeys.KeyIpBlock(i)] = ipBlock.SubId.ToString();
            }
            await globalCacheService.BatchSetAsync((int) RedisDatabases.ServiceIpBlock, redisData);
        }
    }
    
    private static async Task WriteCache<T>(List<T> data, AppSettings appSettings)
    {
        var cacheFolder = Path.Combine(appSettings.CacheRootPath, "Sharing");
        if (!Directory.Exists(cacheFolder))
        {
            Directory.CreateDirectory(cacheFolder);
        }
        var cacheFilePath = Path.Combine(cacheFolder, $"{typeof(T).Name}.json");
        await using var fileStream = new FileStream(cacheFilePath, FileMode.OpenOrCreate, FileAccess.Write);
        await using var streamWriter = new StreamWriter(fileStream);
        await streamWriter.WriteAsync(data.Serialize());
    }
}
