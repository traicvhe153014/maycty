﻿using Novanet.EventBus;

namespace Service.Sharing.Events;

public class SharingContextService : EventBusListenerService<List<object>>
{
    public SharingContextService(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }

    protected override string Channel => SharingContext.Schema;
    protected override bool IsWorkQueue => true;

    protected override Task Processing(List<object> signal)
    {
        foreach (var obj in signal)
        {
            
        }
        throw new NotImplementedException();
    }
}