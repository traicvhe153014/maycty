﻿namespace Service.Sharing.Domain;

public class SharingContext : NovanetContext<SharingContext>
{
    public static readonly string Schema = typeof(SharingContext).GetTypeInfo().Assembly.GetName().Name!;

    public SharingContext(DbContextOptions<SharingContext> options, AppSettings appSettings)
        : base(options, appSettings)
    {
    }

    public DbSet<Age> Ages { get; set; } = default!;

    public DbSet<Category> Categories { get; set; } = default!;
    
    public DbSet<CategoryVariety> CategoryVarieties { get; set; } = default!;

    public DbSet<Faq> Faqs { get; set; } = default!;

    public DbSet<IPBlock> IPBlocks { get; set; } = default!;
    
    public DbSet<IPLocation> IPLocations { get; set; } = default!;

    public DbSet<Location> Locations { get; set; } = default!;

    public DbSet<Gender> Genders { get; set; } = default!;
    
    public DbSet<ProhibitedCategory> ProhibitedCategories { get; set; } = default!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.HasDefaultSchema(Schema);

        new Age().ModelCreating<Age>(modelBuilder, Schema);
        new Category().ModelCreating<Category>(modelBuilder, Schema);
        new CategoryVariety().ModelCreating<CategoryVariety>(modelBuilder, Schema);
        new Faq().ModelCreating<Faq>(modelBuilder, Schema);
        new IPBlock().ModelCreating<IPBlock>(modelBuilder, Schema);
        new IPLocation().ModelCreating<IPLocation>(modelBuilder, Schema);
        new Location().ModelCreating<Location>(modelBuilder, Schema);
        new Gender().ModelCreating<Gender>(modelBuilder, Schema);
        new ProhibitedCategory().ModelCreating<ProhibitedCategory>(modelBuilder, Schema);
    }
}