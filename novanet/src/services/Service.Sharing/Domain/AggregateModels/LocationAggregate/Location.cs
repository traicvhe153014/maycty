using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace Service.Sharing.Domain.AggregateModels.LocationAggregate;

public class Location : LocationCore
{
    public Location()
    {
    }

    public Location(string regionName, string provinceName, int provinceId)
    {
        RegionKey = regionName.CreateMd5();
        RegionName = regionName;
        ProvinceName = provinceName;
        ProvinceId = provinceId;
    }
}