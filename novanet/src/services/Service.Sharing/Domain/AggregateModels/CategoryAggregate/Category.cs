﻿using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace Service.Sharing.Domain.AggregateModels.CategoryAggregate;

public class Category : CategoryCore
{
    [JsonIgnore]
    public List<CategoryVariety>? CategoryVarieties { get; set; }

    public static string GetCategorySlug(string name, List<CategoryVariety>? categoryVarieties)
    {
        var queryWords = Regex.Split(name.RemoveDiacritics(), @"\s+").Select(x => x.ToLower()).ToList();
        foreach (var variety in categoryVarieties ?? new List<CategoryVariety>())
        {
            var varietyWords = Regex.Split(variety.Name.RemoveDiacritics(), @"\s+").Select(x => x.ToLower()).ToList();
            queryWords.AddRange(varietyWords);
        }
        return queryWords.JoinString("-");
    }
}