﻿using System.Text.Json.Serialization;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace Service.Sharing.Domain.AggregateModels.CategoryAggregate;

public class CategoryVariety : CategoryVarietyCore
{
    [ForeignKey(nameof(CategoryId))]
    public Category? Category { get; set; }
}