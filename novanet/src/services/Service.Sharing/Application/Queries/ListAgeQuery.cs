﻿namespace Service.Sharing.Application.Queries;

public class ListAgeQuery : INovanetRequest<List<Age>>
{
    internal class Handler : NovanetRequestHandler<ListAgeQuery, List<Age>>
    {
        private readonly SharingContext _context;

        public Handler(
            ILogger<Handler> logger,
            SharingContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<Age>> HandleAsync(ListAgeQuery request, CancellationToken cancellationToken)
        {
            return await _context.Ages.ToListAsync(cancellationToken);
        }
    }
}