﻿namespace Service.Sharing.Application.Queries;

public class ListGenderQuery : INovanetRequest<List<GenderResponse>>
{
    internal class Handler : NovanetRequestHandler<ListGenderQuery, List<GenderResponse>>
    {
        private readonly SharingContext _context;

        public Handler(
            ILogger<Handler> logger,
            SharingContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<GenderResponse>> HandleAsync(ListGenderQuery request,
            CancellationToken cancellationToken)
        {
            var sexes = await _context.Genders.ToListAsync(cancellationToken);
            return sexes.MapTo<List<GenderResponse>>();
        }
    }
}