﻿namespace Service.Sharing.Application.Queries;

public class ListCategoryQuery : INovanetRequest<List<CategoryResponse>>
{
    internal class Handler : NovanetRequestHandler<ListCategoryQuery, List<CategoryResponse>>
    {
        private readonly SharingContext _context;

        public Handler(
            ILogger<Handler> logger,
            SharingContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<CategoryResponse>> HandleAsync(ListCategoryQuery request,
            CancellationToken cancellationToken)
        {
            var categories = await _context.Categories.ToListAsync(cancellationToken);
            return categories.MapTo<List<CategoryResponse>>();
        }
    }
}