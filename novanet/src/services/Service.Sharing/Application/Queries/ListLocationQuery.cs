﻿namespace Service.Sharing.Application.Queries;

public class ListLocationQuery : INovanetRequest<List<Location>>
{
    internal class Handler : NovanetRequestHandler<ListLocationQuery, List<Location>>
    {
        private readonly SharingContext _context;

        public Handler(ILogger<Handler> logger,
            SharingContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<Location>> HandleAsync(ListLocationQuery request,
            CancellationToken cancellationToken)
        {
            return await _context.Locations.ToListAsync(cancellationToken);
        }
    }
}