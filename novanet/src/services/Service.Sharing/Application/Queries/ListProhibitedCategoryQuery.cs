﻿namespace Service.Sharing.Application.Queries;

public class ListProhibitedCategoryQuery : INovanetRequest<List<ProhibitedCategory>>
{
    internal class Handler : NovanetRequestHandler<ListProhibitedCategoryQuery, List<ProhibitedCategory>>
    {
        private readonly SharingContext _context;

        public Handler(ILogger<Handler> logger,
            SharingContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<ProhibitedCategory>> HandleAsync(ListProhibitedCategoryQuery request,
            CancellationToken cancellationToken)
        {
            return await _context.ProhibitedCategories.ToListAsync(cancellationToken);
        }
    }
}