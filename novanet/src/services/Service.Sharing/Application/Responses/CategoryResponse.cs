﻿namespace Service.Sharing.Application.Responses;

public class CategoryResponse : IMapFrom<Category>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;
}