﻿namespace Service.Sharing.Application.Responses;

public class GenderResponse : IMapFrom<Gender>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;
}