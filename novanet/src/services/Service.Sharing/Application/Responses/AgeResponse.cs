﻿namespace Service.Sharing.Application.Responses;

public class AgeResponse : IMapFrom<Age>
{
    public string? Type { get; set; }
}