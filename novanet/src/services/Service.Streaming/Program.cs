using Novanet.Core.Registrations;
using Novanet.Core.ValueObjects;
using Novanet.EventBus;
using Service.Streaming.Helpers;
using Service.Streaming.Services.BackgroundServices;
using Service.Streaming.Services.CommonServices;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

var configuration = builder.Configuration;
var appSettings = new AppSettings();
configuration.Bind(appSettings);

services.AddCors(options =>
{
    options.AddPolicy("AllowAllOrigins", x => x
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials()
        .SetIsOriginAllowed(_ => true));
});
services.AddRazorPages();
services.AddServerSideBlazor();
services.AddControllers();
services.AddEndpointsApiExplorer();
services.AddSwaggerGen();
services.AddSingleton(appSettings);
services.AddScoped<IFileService, FileService>();
services.AddScoped<IVideoProcessorService, VideoProcessorService>();
services.AddHostedService<QueuedHostedService>();
services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
services.AddAntDesign();
services.RegisterRedis(appSettings);
services.AddRabbitMQ(appSettings.RabbitMQ);

var app = builder.Build();
var environment = app.Environment;
var lifetime = app.Lifetime;

if (!app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseExceptionHandler("/Error");
}

app.UseCors("AllowAllOrigins");
app.UseStaticFiles();
app.UseRouting();
app.UseSwagger();
app.UseSwaggerUI();
app.UseAuthentication();
app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapBlazorHub();
    endpoints.MapFallbackToPage("/_Host");
});
lifetime.ApplicationStarted.Register(() =>
{
    FFmpegHelper.Initialize(environment.ContentRootPath);
});
app.Run();