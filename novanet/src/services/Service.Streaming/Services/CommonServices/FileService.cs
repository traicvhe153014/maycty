﻿using Google.Protobuf;
using Grpc.Net.Client;
using Novanet.Core.ValueObjects;
using NovanetGrpcFileStreaming;

namespace Service.Streaming.Services.CommonServices;

public interface IFileService
{
    Task UploadFileAsync(string directory, string fileId);
}

public class FileService : IFileService, IDisposable
{
    private readonly Lazy<GrpcChannel> _channel;

    public FileService(AppSettings appSettings)
    {
        var httpHandler = new HttpClientHandler();
        httpHandler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
        _channel = new Lazy<GrpcChannel>(() => GrpcChannel.ForAddress(appSettings.GrpcEndpoints.ServiceStorage,
            new GrpcChannelOptions {HttpHandler = httpHandler}));
    }

    public async Task UploadFileAsync(string directory, string fileId)
    {
        var client = CreateClient();
        var fileInfo = new System.IO.FileInfo(directory);
        await using var fileStream = new FileStream(directory, FileMode.Open);
        var content = new BytesContent
        {
            FileSize = fileStream.Length,
            ReadedByte = 0,
            Info = new NovanetGrpcFileStreaming.FileInfo
            {
                FileName = Path.GetFileNameWithoutExtension(fileInfo.Name),
                FileExtension = fileInfo.Extension,
                FileId = fileId
            }
        };

        var upload = client.Upload();
        // sending file metadata
        await upload.RequestStream.WriteAsync(content);

        // sending file content
        var buffer = new byte[2048];
        while ((content.ReadedByte = fileStream.Read(buffer, 0, buffer.Length)) > 0)
        {
            content.Buffer = ByteString.CopyFrom(buffer);
            await upload.RequestStream.WriteAsync(content);
        }
        await upload.RequestStream.CompleteAsync();
    }
    
    public void Dispose()
    {
        if (_channel.IsValueCreated) _channel.Value.Dispose();
    }
    
    private FileStreamingService.FileStreamingServiceClient CreateClient()
    {
        return new FileStreamingService.FileStreamingServiceClient(_channel.Value);
    }
}