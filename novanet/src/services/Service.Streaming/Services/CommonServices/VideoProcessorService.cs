﻿using Novanet.Core.Extensions;
using Novanet.Core.RedisCache;
using Service.Streaming.Models;
using Service.Streaming.Services.BackgroundServices;
using Xabe.FFmpeg;

namespace Service.Streaming.Services.CommonServices;

public interface IVideoProcessorService
{
    Task ProcessVideo(string videoId, string fileName);

    Task ProcessVideo(string videoId, IFormFile file);
}

public class VideoProcessorService : IVideoProcessorService
{
    private readonly IBackgroundTaskQueue _taskQueue;
    private readonly IWebHostEnvironment _environment;
    private readonly ILogger<VideoProcessorService> _logger;
    private readonly IRedisPubSubService _redisPubSubService;
    private readonly IFileService _fileService;

    public VideoProcessorService(IBackgroundTaskQueue taskQueue,
        IWebHostEnvironment environment, ILogger<VideoProcessorService> logger,
        IRedisPubSubService redisPubSubService, IFileService fileService)
    {
        _taskQueue = taskQueue;
        _environment = environment;
        _logger = logger;
        _redisPubSubService = redisPubSubService;
        _fileService = fileService;
    }

    public async Task ProcessVideo(string videoId, IFormFile file)
    {
        await _redisPubSubService.HashSetAsync("VideoProcess", videoId, "true");
        var savedFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + file.FileName);
        if (file.Length == 0) return;

        //save file to temp folder
        await using (Stream fileStream = new FileStream(savedFile, FileMode.Create))
        {
            await file.CopyToAsync(fileStream);
        }

        await ProcessVideo(videoId, savedFile);
    }

    public async Task ProcessVideo(string videoId, string fileName)
    {
        await _taskQueue.EnqueueAsync((token) => CreateProcessVideoWorkItem(videoId, fileName, token));
    }

    private async ValueTask CreateProcessVideoWorkItem(string videoId, string fileName,
        CancellationToken cancellationToken)
    {
        try
        {
            if (cancellationToken.IsCancellationRequested) return;
            var mediaInfo = await FFmpeg.GetMediaInfo(fileName, cancellationToken);
            var height = mediaInfo.VideoStreams.FirstOrDefault()?.Height ?? 0;
            var tempFolder = Path.Combine(_environment.ContentRootPath, "FFmpeg", "conversions", videoId);
            if (!Directory.Exists(tempFolder)) Directory.CreateDirectory(tempFolder);
            var conversion = FFmpeg.Conversions.New().AddParameter($"-hide_banner -y -i \"{fileName}\"");

            AddParameters(conversion, height, tempFolder, videoId);

            conversion.OnProgress += async (sender, args) =>
            {
                var percent = (int)(Math.Round(args.Duration.TotalSeconds / args.TotalLength.TotalSeconds, 2) * 100);
                _logger.LogInformation("[{Duration} / {TotalLength}] {percent}%", args.Duration, args.TotalLength,
                    percent);
                await _redisPubSubService.PublishAsync(videoId.ToChannel(), new VideoProgressModel
                {
                    Id = videoId,
                    Percent = percent
                });
            };

            await conversion.SetOverwriteOutput(true).Start(cancellationToken);
            await UploadToFileServer(tempFolder, videoId);
            DeleteDirectory(tempFolder);
            await _redisPubSubService.HashRemoveAsync("VideoProcess", videoId);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "CreateProcessVideoWorkItem EXCEPTION");
        }
    }

    private static void AddParameters(IConversion conversion, int rootHeight, string directory, string videoId)
    {
        conversion.AddParameter(
            "-c:a aac -ar 48000 -c:v h264 -profile:v main -crf 20 -sc_threshold 0 -g 48 -keyint_min 48 -hls_time 4 -hls_playlist_type vod");

        if (rootHeight >= 240)
        {
            conversion.AddParameter(BuildParameters(240, directory, videoId));
        }

        if (rootHeight >= 360)
        {
            conversion.AddParameter(BuildParameters(360, directory, videoId));
        }

        if (rootHeight >= 480)
        {
            conversion.AddParameter(BuildParameters(480, directory, videoId));
        }

        if (rootHeight >= 720)
        {
            conversion.AddParameter(BuildParameters(720, directory, videoId));
        }

        if (rootHeight >= 1080)
        {
            conversion.AddParameter(BuildParameters(1080, directory, videoId));
        }
    }

    private static string BuildParameters(int height, string directory, string videoId)
    {
        var (w, bv, maxRate, bufSize, v) = height switch
        {
            240 => ("426", "400k", "428k", "600k", "0"),
            360 => ("640", "800k", "856k", "1200k", "0"),
            480 => ("842", "1400k", "1498k", "2100k", "1"),
            720 => ("1280", "2800k", "2996k", "4200k", "2"),
            1080 => ("1920", "5000k", "5350k", "7500k", "3"),
            _ => throw new NotImplementedException()
        };
        var command =
            $"-c:a aac -ar 48000 -c:v h264 -profile:v main -crf 19 -sc_threshold 0 -g 60 -keyint_min 60 -hls_time 1 -hls_playlist_type vod -vf scale=w={w}:h=-2 -b:v {bv} -maxrate {maxRate} -bufsize {bufSize} -b:a 0k -hls_segment_filename \"{Path.Combine(directory, $"{height}p_%03d.ts")}\" \"{Path.Combine(directory, $"{height}p.m3u8")}\"";
        return command;
    }

    private async Task UploadToFileServer(string directory, string id)
    {
        var files = Directory.GetFiles(directory, "*");

        foreach (var file in files)
        {
            await _fileService.UploadFileAsync(Path.Combine(directory, file), id);
        }
        // const int maxThread = 1; // 5
        // var semaphore = new SemaphoreSlim(maxThread);
        // var tasks = files.Select(async file =>
        // {
        //     await semaphore.WaitAsync();
        //     // var objectName = Path.Combine("file", "video-storage", id, Path.GetFileName(file));
        //     await _fileService.UploadFileAsync(directory, file);
        //     semaphore.Release();
        // });
        // await Task.WhenAll(tasks);
    }

    private static void DeleteDirectory(string directory)
    {
        Directory.Delete(directory, true);
    }
}