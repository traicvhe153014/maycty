﻿using Novanet.Core.ValueObjects;

namespace Service.Streaming.Services.BackgroundServices;

public class QueuedHostedService : BackgroundService
{
    private readonly IBackgroundTaskQueue _taskQueue;
    private readonly ILogger<QueuedHostedService> _logger;
    private readonly SemaphoreSlim _semaphore;

    public QueuedHostedService(IBackgroundTaskQueue taskQueue,
        ILogger<QueuedHostedService> logger,
        AppSettings appSettings)
    {
        _taskQueue = taskQueue;
        _logger = logger;
        _semaphore = new SemaphoreSlim(appSettings.VideoQueueConfigs.NumberOfProcessors);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Queued Hosted Service is running.");
        await BackgroundProcessing(stoppingToken);
    }

    private async Task BackgroundProcessing(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await _semaphore.WaitAsync(stoppingToken);
            var workItem = await _taskQueue.DequeueAsync(stoppingToken);
            try
            {
                _ = workItem(stoppingToken)
                    .AsTask()
                    .ContinueWith(_ => _semaphore.Release(), stoppingToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred executing {WorkItem}.", nameof(workItem));
            }
        }
    }

    public override async Task StopAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Queued Hosted Service is stopping.");
        await base.StopAsync(stoppingToken);
    }
}