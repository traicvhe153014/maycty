﻿using Microsoft.AspNetCore.Mvc;
using Novanet.EventBus;
using NovanetCore.Business.BusinessObjects;
using Service.Streaming.Services.CommonServices;

namespace Service.Streaming.Controllers;

[ApiController]
[Route("streaming/api/v1/media")]
public class MediaController : ControllerBase
{
    private readonly IVideoProcessorService _videoProcessorService;
    private readonly IMessageBusClient _messageBusClient;

    public MediaController(IVideoProcessorService videoProcessorService, IMessageBusClient messageBusClient)
    {
        _videoProcessorService = videoProcessorService;
        _messageBusClient = messageBusClient;
    }

    [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)] 
    [DisableRequestSizeLimit] 
    [Consumes("multipart/form-data")]
    [HttpPost("process-video")]
    public async Task<IActionResult> ProcessVideo(IFormFile file, Guid? advertisingId)
    {
        var videoId = Guid.NewGuid();
        await _videoProcessorService.ProcessVideo(videoId.ToString("N"), file);
        if (advertisingId is not null)
        {
            var fileExtension = new FileInfo(file.Name).Extension;
            _messageBusClient.Publish(MessageBusChannels.UpdateAdvertisingVideoValue,
                new UpdateAdvertisingVideoValueSignal
                {
                    AdvertisingId = advertisingId.Value,
                    VideoId = $"{videoId:N}.{fileExtension}"
                });
        }
        return Ok(videoId.ToString("N"));
    }
}