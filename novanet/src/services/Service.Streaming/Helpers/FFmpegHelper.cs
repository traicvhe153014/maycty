﻿using System.Runtime.InteropServices;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Downloader;

namespace Service.Streaming.Helpers;

public static class FFmpegHelper
{
    public static void Initialize(string rootPath)
    {
        var path = string.Empty;
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            path = Path.Combine(rootPath,"FFmpeg", "windows");
        }
        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
        {
            path = Path.Combine(rootPath, "FFmpeg", "osx");
        }
        if (!Directory.Exists(path)) Directory.CreateDirectory(path);
        FFmpeg.SetExecutablesPath(path);
        if (!File.Exists(Path.Combine(path, "ffmpeg.exe")) && !File.Exists(Path.Combine(path, "ffmpeg")))
        {
            FFmpegDownloader.GetLatestVersion(FFmpegVersion.Official, path).GetAwaiter().GetResult();
        }
    }
}