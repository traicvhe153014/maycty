﻿module.exports = {
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/aspect-ratio')
    ],
    content: [
        "./Areas/**/*.cshtml",
        "./Pages/**/*.razor",
        "./Pages/**/*.cshtml",
        "./Shared/**/*.razor",
        "./Shared/**/*.cshtml",
        "../../TechCore.Components/**/*.razor"
    ],
    theme: {
        extend: {},
        aspectRatio: {
        }
    },
}