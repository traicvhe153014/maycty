﻿namespace Service.Streaming.Models;

public class VideoProgressModel
{
    public string Id { get; set; } = default!;

    public double Percent { get; set; }
}