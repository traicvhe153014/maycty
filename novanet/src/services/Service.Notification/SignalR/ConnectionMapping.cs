﻿using System.Collections.Concurrent;

namespace Service.Notification.SignalR;

public class ConnectionMapping
{
    public ConcurrentDictionary<string, HashSet<string>> Connections { get; } = new();

    public int Count => Connections.Count;

    public void Add(string key, string connectionId)
    {
        lock (Connections)
        {
            if (!Connections.TryGetValue(key, out var connections))
            {
                connections = new HashSet<string>();
                Connections.TryAdd(key, connections);
            }

            lock (connections)
            {
                connections.Add(connectionId);
            }
        }
    }

    public IEnumerable<string> GetConnections(string key)
    {
        return Connections.TryGetValue(key, out var connections) ? connections : Enumerable.Empty<string>();
    }

    public void Remove(string key, string connectionId)
    {
        lock (Connections)
        {
            if (!Connections.TryGetValue(key, out var connections))
            {
                return;
            }

            lock (connections)
            {
                connections.Remove(connectionId);
                if (connections.Count == 0)
                {
                    Connections.TryRemove(key, out _);
                }
            }
        }
    }
}