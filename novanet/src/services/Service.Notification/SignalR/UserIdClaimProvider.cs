﻿using Microsoft.AspNetCore.SignalR;

namespace Service.Notification.SignalR;

public class UserIdClaimProvider : IUserIdProvider
{
    public string GetUserId(HubConnectionContext connection)
    {
        return connection.User?.FindFirst("id")?.Value!;
    }
}