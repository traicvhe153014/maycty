﻿using Microsoft.AspNetCore.SignalR;

namespace Service.Notification.SignalR;

[Authorize]
public class SignalRHub : Hub
{
    private readonly ConnectionMapping _connectionManager;
    private readonly IMessageBusClient _eventBus;

    public SignalRHub(
        ConnectionMapping connectionManager,
        IMessageBusClient eventBus)
    {
        _connectionManager = connectionManager;
        _eventBus = eventBus;
    }

    public override async Task OnConnectedAsync()
    {
        if (string.IsNullOrEmpty(Context.UserIdentifier)) return;
        _connectionManager.Add(Context.UserIdentifier, Context.ConnectionId);
        await base.OnConnectedAsync();
    }

    public override async Task OnDisconnectedAsync(Exception? exception)
    {
        if (string.IsNullOrEmpty(Context.UserIdentifier)) return;
        _connectionManager.Remove(Context.UserIdentifier, Context.ConnectionId);
        await base.OnDisconnectedAsync(exception);
    }

    public Task SendMessage(string userId, string message)
    {
        return Clients.All.SendAsync("Send", userId, message);
    }

    public virtual void JoinGroup(string groupName)
    {
        Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        Console.WriteLine($"Connection {Context.ConnectionId} joined group {groupName}");

        if (groupName != MessageBusChannels.UserLogin) return;

        Task.Run(() =>
        {
            _eventBus.Publish(groupName, new UserLoginModel
            {
                UserId = Context.UserIdentifier,
                ConnectionId = Context.ConnectionId,
                Status = true
            });
        });
    }

    public virtual void LeaveGroup(string groupName)
    {
        Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
    }
}