﻿namespace Service.Notification.SignalR;

public class UserLoginModel
{
    public string? UserId { get; set; }

    public string? ConnectionId { get; set; }

    public long ExpiresAt { get; set; }

    public bool Status { get; set; }
}
