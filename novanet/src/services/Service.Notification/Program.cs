using Microsoft.AspNetCore.SignalR;
using Service.Notification.Domain;
using Service.Notification.SignalR;

WebApplication.CreateBuilder(args).NovanetCore<NotificationContext>(NotificationContext.Schema, out var app,
    (services, appSettings) =>
    {
        services.AddSignalR();
        services.AddRabbitMQ(appSettings.RabbitMQ);
        services.AddSingleton<ConnectionMapping>();
        services.AddSingleton<IUserIdProvider, UserIdClaimProvider>();
    },
    application => { application.MapHub<SignalRHub>("/hub"); });

app.Run();