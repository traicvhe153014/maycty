using Microsoft.AspNetCore.SignalR;
using Service.Notification.SignalR;

namespace Service.Notification.Controllers;

[ApiController]
[Route("notification/[action]")]
public class HealthController : ControllerBase
{
    private readonly IHubContext<SignalRHub> _hub;

    public HealthController(IHubContext<SignalRHub> hub)
    {
        _hub = hub;
    }

    [HttpGet]
    public async Task<IActionResult> Health()
    {
        return Ok(await Task.FromResult("OK"));
    }

    [HttpGet]
    public async Task<IActionResult> Send()
    {
        await _hub.Clients.All.SendAsync("Novanet", "message here");
        return Ok("OK");
    }
}