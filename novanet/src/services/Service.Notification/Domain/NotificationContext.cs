namespace Service.Notification.Domain;

public class NotificationContext : DbContext
{
    public static readonly string? Schema = typeof(NotificationContext).GetTypeInfo().Assembly.GetName().Name;

    public NotificationContext(DbContextOptions<NotificationContext> options) : base(options)
    {
    }
}