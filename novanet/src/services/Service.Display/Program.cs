using AioCore.Mongo.Driver;
using MongoDB.Bson.Serialization.Conventions;
using Novanet.Core.RazorRenderer;
using Novanet.Core.Registrations;
using Novanet.EventBus;
using NovanetCore.Business;
using NovanetCore.Business.BusinessEvents;
using NovanetCore.Business.BusinessParallels;
using Service.Display.Domain;
using Service.Display.Events;
using Service.Display.Services;

var builder = WebApplication.CreateBuilder(args);
var environment = builder.Environment;
var services = builder.Services;
var appSettings = NovanetExtensions.Configuration(environment);

builder.LoggerBuilder(environment, appSettings);
services.AddHttpClient();
services.AddSingleton(appSettings);
services.AddSingleton(appSettings.RabbitMQ);
services.RegisterController();
services.AddControllersWithViews();
services.RegisterRedis(appSettings);
services.AddMediator(NovanetExtensions.GetAssemblies());
services.AddMongoContext<DisplayContext>(
    appSettings.MongoShortTime.Url,
    appSettings.MongoShortTime.Database);
var conventionPack = new ConventionPack { new IgnoreExtraElementsConvention(true) };
ConventionRegistry.Register("IgnoreExtraElements", conventionPack, type => true);
services.AddMapper();
services.AddBusinessCore();
services.AddRabbitMQ(appSettings.RabbitMQ);
services.AddCacheManagerServices();
services.AddHostedService<UpdateObjectGroupConditionsService>();
services.AddHostedService<AdvertisingLogService>();
services.AddHostedService<AdvertisingFraudClickService>();
services.AddScoped<IReportService, ReportService>();
services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
services.AddSingleton<IMessageBusClient, MessageBusClient>();
services.AddSingleton<IEventService, EventService>();
services.AddSingleton<RazorRenderer>();
services.AddSingleton<RedisLogService>();
services.AddSingleton<IDisplayQueueService, DisplayQueueService>();

var application = builder.Build();
var publisher = application.Services.GetRequiredService<Publisher>();
publisher.Publish(new UpdateCacheManagerParallel(), PublishStrategy.ParallelNoWait);
application.UseStaticFiles();
application.UseCors("AllowAllOrigins");
application.UseRouting();
application.MapControllers();

application.Run();