﻿# Display sdk

## Code nhúng hiển thị
```js
<script>
  window.Novanet.init({
    event: 'default',
    width: 640,
    height: 200,
    pubId: 7447,
    zoneId: 1
  });
</script>
```

## Code nhúng chuyển đổi
```js
<script>
  window.Novanet.init({
    event: 'add-to-card',
    data: {
      items: [
        {
          id: 1,
          name: "Product 1",
          description: "Product description 1",
          price: 100
        },
        {
          id: 2,
          name: "Product 2",
          description: "Product description 2",
          price: 200
        },
        {
          id: 3,
          name: "Product 3",
          description: "Product description 3",
          price: 300
        },
      ]
    }
  });
</script>
```