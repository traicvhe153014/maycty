﻿using NovanetCore.Business.BusinessConfigs;
using NovanetCore.Business.BusinessDomain.Service.Display;
using Service.Display.Domain;
using Service.Display.Parallels;
using Service.Display.Services;

namespace Service.Display.Application.Queries;

public class ClickQuery : INovanetRequest<ClickResponse>
{
    public ClickQuery(IQueryCollection? requestQuery, IRequestCookieCollection cookies)
    {
        QueryParams = requestQuery;
        Cookies = cookies;
    }

    private IQueryCollection? QueryParams { get; set; }
    private IRequestCookieCollection Cookies { get; set; }

    internal class Handler : NovanetRequestHandler<ClickQuery, ClickResponse>
    {
        private readonly IRequestParamsService _requestParamsService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Publisher _publisher;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Handler(ILogger<Handler> logger,
            IRequestParamsService requestParamsService, IHttpContextAccessor httpContextAccessor,
            Publisher publisher, IServiceScopeFactory serviceScopeFactory) : base(logger,
            httpContextAccessor)
        {
            _requestParamsService = requestParamsService;
            _httpContextAccessor = httpContextAccessor;
            _publisher = publisher;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task<ClickResponse> HandleAsync(ClickQuery request,
            CancellationToken cancellationToken)
        {
            // TODO: DONE # Hòa NX - nếu có exception thì điều gì xảy ra, anh cần nếu có bất kỳ vấn đề gì (lỗi redis, mongo...), vẫn phải redirect được tới trang đích của KH
            var @params = _requestParamsService.RequestParams(request.QueryParams);
            var hashContent = !string.IsNullOrEmpty(@params.Hash) ? @params.Hash.AesDecryptString() : string.Empty;
            var clientManager = new ClientManager(
                _httpContextAccessor, request.QueryParams,
                DateTimeOffset.Now, hashContent,
                Logger);
            try
            {
                var clientId = clientManager.ClientId;

                // xử lý click ảo real-time
                var clickedDictionary = request.Cookies[ConfigValues.Clicked]?.Deserialize<Dictionary<long, string>>();
                var isFraud = IsFraud(clickedDictionary, clientManager);

                var ip = clientManager.Ip;

                _ = Task.Run(async () =>
                {
                    using var scope = _serviceScopeFactory.CreateScope();
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<ClickParallel.Handler>>();
                    var redisQueueService = scope.ServiceProvider.GetRequiredService<IRedisQueueService>();
                    var globalCacheService = scope.ServiceProvider.GetRequiredService<IGlobalCacheService>();
                    var locationService = scope.ServiceProvider.GetRequiredService<ILocationService>();
                    var clickParallelHandler = new ClickParallel.Handler(logger, redisQueueService, globalCacheService, locationService);
                    var clickParallel = new ClickParallel
                    {
                        HttpContextAccessor = _httpContextAccessor,
                        ClientId = clientId,
                        ClientManager = clientManager,
                        IsFraud = isFraud,
                        Ip = ip
                    };
                    await clickParallelHandler.Handle(clickParallel, cancellationToken);
                }, cancellationToken);

                var urlRedirect = GetTargetUrl(clientManager.TargetUrl, clientManager.Campaign, clientManager.Advertising, clientManager.Product, clientManager.Domain);
                if (!string.IsNullOrEmpty(urlRedirect))
                    return new ClickResponse
                    {
                        RedirectTo = urlRedirect,
                        Clicked = clickedDictionary!
                    };
                return new ClickResponse
                {
                    RedirectTo = ConfigValues.Domain,
                    Clicked = clickedDictionary!
                };
            }
            catch (Exception e)
            {
                var urlRedirect = GetTargetUrl(clientManager.TargetUrl, clientManager.Campaign, clientManager.Advertising, clientManager.Product, clientManager.Domain);
                await _publisher.Publish(new ErrorParallel
                {
                    ClientManager = clientManager,
                    Error = e.ToString(),
                    TrackingType = TrackingType.Click
                }, cancellationToken);
                return new ClickResponse
                {
                    RedirectTo = urlRedirect,
                };
            }
        }

        private static bool IsFraud(Dictionary<long, string>? clickedDictionary, ClientManager clientManager)
        {
            var advertisingId = clientManager.Advertising;
            if (clickedDictionary != null &&
                (clickedDictionary.ContainsKey(advertisingId) ||
                 clickedDictionary.Count > 10 ||
                 !((DateTimeOffset.UtcNow - clickedDictionary[0].ToDateTime())
                     .TotalSeconds > 10))) return true;
            if (!((DateTimeOffset.UtcNow - clientManager.UrlCreatedOn).TotalMinutes < 60)) return true;
            clickedDictionary ??= new Dictionary<long, string>
            {
                {
                    advertisingId, DateTimeOffset.UtcNow.ToHexCode()
                }
            };
            clickedDictionary[0] = DateTimeOffset.UtcNow.ToHexCode();
            return false;
        }

        private static string GetTargetUrl(string adsUrl, long campaignId, long advertisingId, long productId, string domain)
        {
            var targetUrl = !string.IsNullOrWhiteSpace(adsUrl) ? adsUrl : ConfigValues.DefaultRedirectLink;

            if (targetUrl.Contains("utm_source") ||
                targetUrl.Contains("utm_campaign") ||
                targetUrl.Contains("utm_medium")) return targetUrl;
            const string utmSource = "novanet_ads";
            var utmCampaign = $"novanet-campaign-{campaignId}";
            var utmMedium = $"{domain}";

            var questionIndex = targetUrl.IndexOf("?", StringComparison.Ordinal);

            if (questionIndex > -1)
                targetUrl = targetUrl.Insert(questionIndex + 1,
                    $"utm_source={utmSource}&utm_campaign={utmCampaign}&utm_medium={utmMedium}&{ConfigValues.NovanetAdvertising}={advertisingId}&{ConfigValues.NovanetProduct}={productId}&");
            else
                targetUrl +=
                    $"?utm_source={utmSource}&utm_campaign={utmCampaign}&utm_medium={utmMedium}&{ConfigValues.NovanetAdvertising}={advertisingId}&{ConfigValues.NovanetProduct}={productId}";

            return targetUrl;
        }
    }
}