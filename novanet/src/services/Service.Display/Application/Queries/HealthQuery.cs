﻿using Novanet.Core.Constants;

namespace Service.Display.Application.Queries;

public class HealthQuery : INovanetRequest<object>
{
    public HealthType Type { get; set; }
    public Guid? Id { get; set; }

    internal class Handler : NovanetRequestHandler<HealthQuery, object>
    {
        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor) : base(logger,
            httpContextAccessor)
        {
        }

        protected override async Task<object> HandleAsync(HealthQuery request, CancellationToken cancellationToken)
        {
            if (request.Type == HealthType.Reset)
            {
                HealthCounter.HitToServer = 0;
                HealthCounter.HitToServerSuccess = 0;
                HealthCounter.HitToServerError = 0;
                HealthCounter.HitToServerDisplay = 0;
                HealthCounter.HitToServerDisplaySuccess = 0;
                HealthCounter.HitToServerDisplayError = 0;
                HealthCounter.HitToServerException = 0;
                HealthCounter.IncomingParallels = 0;
                HealthCounter.IncomingRequests = 0;
                HealthCounter.SuccessfulTrackings = 0;
                HealthCounter.FailedRequest = 0;
            }

            return request.Type switch
            {
                HealthType.LastUpdate => await Task.FromResult<object>(new
                {
                    CacheManager.LastUpdatedStart, CacheManager.LastUpdatedEnd
                }),
                HealthType.Campaign => await Task.FromResult(new
                {
                    Campaigns = CacheManager.Campaigns.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.AdvertisingSetLevel2 => await Task.FromResult(new
                {
                    AdvertisingSets = CacheManager.AdvertisingSets.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.Advertising => await Task.FromResult(new
                {
                    Advertising = CacheManager.Advertising.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.ProductFeed => await Task.FromResult(new
                {
                    ProductFeeds = CacheManager.ProductFeeds.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.Product => await Task.FromResult(new
                {
                    Products = CacheManager.Products.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.Website => await Task.FromResult(new
                {
                    Websites = CacheManager.Websites.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.Zone => await Task.FromResult(new
                {
                    Zones = CacheManager.Zones.Values.Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.WebsitePrice => await Task.FromResult(new
                {
                    WebsitePrices = CacheManager.WebsitePrices.Values
                        .Where(x => request.Id == null || x.CreatedBy == request.Id)
                        .ToList()
                }),
                HealthType.RequestTracking => await Task.FromResult(new
                {
                    HealthCounter.HitToServer,
                    HealthCounter.HitToServerSuccess,
                    HealthCounter.HitToServerError,
                    HealthCounter.HitToServerDisplay,
                    HealthCounter.HitToServerDisplaySuccess,
                    HealthCounter.HitToServerDisplayError,
                    HealthCounter.HitToServerException,
                    HealthCounter.IncomingRequests,
                    HealthCounter.IncomingParallels,
                    HealthCounter.SuccessfulTrackings,
                    HealthCounter.FailedRequest
                }),

                _ => await Task.FromResult("Server Healthy")
            };
        }
    }
}

public enum HealthType
{
    LastUpdate = 0,
    Campaign = 10,
    AdvertisingSetLevel1 = 111, // không lấy dữ liệu nhắm chọn
    AdvertisingSetLevel2 = 112, // lấy tất cả dữ liệu nhóm quảng cáo
    Advertising = 12,
    ProductFeed = 13,
    Product = 14,
    Website = 21,
    Zone = 22,
    WebsitePrice = 23,
    Reset = 98,
    RequestTracking = 99
}