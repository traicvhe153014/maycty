﻿using Novanet.Core.RazorRenderer;

namespace Service.Display.Application.Queries;

public class EventQuery : INovanetRequest<string>
{
    internal class Handler : NovanetRequestHandler<EventQuery, string>
    {
        private readonly RazorRenderer _razorRenderer;

        public Handler(ILogger<NovanetRequestHandler<EventQuery, string>> logger,
            IHttpContextAccessor httpContextAccessor, RazorRenderer razorRenderer) : base(logger, httpContextAccessor)
        {
            _razorRenderer = razorRenderer;
        }

        protected override async Task<string> HandleAsync(EventQuery request, CancellationToken cancellationToken)
        {
            var view = await _razorRenderer.RenderViewToString("Event/Index", new object());
            return view ?? string.Empty;
        }
    }
}