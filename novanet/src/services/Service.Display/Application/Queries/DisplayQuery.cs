﻿using System.Collections.Concurrent;
using Novanet.Core.Constants;
using Novanet.Core.Enums;
using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessDomain.Service.Settings;
using NovanetCore.Business.BusinessObjects;
using Service.Display.Domain;
using Service.Display.Parallels;
using Service.Display.Services;

namespace Service.Display.Application.Queries;

public class DisplayQuery : INovanetRequest<DisplayResponse>, IDisposable
{
    public DisplayQuery(IQueryCollection? requestQuery, IRequestCookieCollection cookies, string queryString)
    {
        QueryParams = requestQuery;
        Cookies = cookies;
        QueryString = queryString;
    }

    private IQueryCollection? QueryParams { get; set; }
    private IRequestCookieCollection Cookies { get; set; }
    private string QueryString { get; set; }

    internal class Handler : NovanetRequestHandler<DisplayQuery, DisplayResponse>, IDisposable
    {
        private readonly IAdvertisingService _advertisingService;
        private readonly ITemplateService _templateService;
        private readonly IRequestParamsService _requestParamsService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Publisher _publisher;
        private readonly IReportService _reportService;
        private readonly ICookieService _cookieService;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IRedisCacheService _redisCacheService;

        public Handler(ILogger<Handler> logger, IAdvertisingService advertisingService,
            ITemplateService templateService, IRequestParamsService requestParamsService,
            Publisher publisher, IHttpContextAccessor httpContextAccessor,
            IReportService reportService, ICookieService cookieService,
            IServiceScopeFactory serviceScopeFactory, IRedisCacheService redisCacheService) : base(logger, httpContextAccessor)
        {
            _advertisingService = advertisingService;
            _templateService = templateService;
            _requestParamsService = requestParamsService;
            _publisher = publisher;
            _httpContextAccessor = httpContextAccessor;
            _reportService = reportService;
            _cookieService = cookieService;
            _serviceScopeFactory = serviceScopeFactory;
            _redisCacheService = redisCacheService;
        }

        protected override async Task<DisplayResponse> HandleAsync(DisplayQuery request,
            CancellationToken cancellationToken)
        {
            try
            {
                Interlocked.Increment(ref HealthCounter.IncomingRequests);

                // TODO: DONE # Hòa NX - Function này không có try catch, nếu có exception thì điều gì xảy ra ? tương tự ở các xử lý khác, túm lại câu hỏi là khả năng chịu lỗi của hệ thống thế nào ?
                // Lấy danh sách thông tin phía client: zone, timestamp, referer, url, host
                var @params = _requestParamsService.RequestParams(request.QueryParams);

                // TODO: DONE #9 Hòa NX - đoạn này bắt client IP behind proxy ở đâu ?
                // anhlq1: xử lý client IP trong ClientManager
            
                var now = DateTimeOffset.Now;
                var nowDayOfYear = now.GetDayOfYear().ToString();

                var clientManager = new ClientManager(
                    _httpContextAccessor, request.QueryParams,
                    DateTimeOffset.Now, request.QueryString,
                    Logger);
                // get client click, view reports
                var clientDailyView = await _redisCacheService.HashGetAsync<double>(
                    RedisReportKeys.GetClientKey(ReportClientDimension.ClientId, ReportMetric.View,
                        clientManager.ClientId),
                    nowDayOfYear);
                var clientView = await _redisCacheService.HashGetAsync<double>(
                    RedisReportKeys.GetClientKey(ReportClientDimension.ClientId, ReportMetric.View,
                        clientManager.ClientId),
                    "0");
                var clientClick = await _redisCacheService.HashGetAsync<double>(
                    RedisReportKeys.GetClientKey(ReportClientDimension.ClientId, ReportMetric.Click,
                        clientManager.ClientId),
                    "0");
                clientManager.DailyView = clientDailyView;
                clientManager.TotalView = clientView;
                clientManager.TotalClick = clientClick;
            
                var clientId = clientManager.ClientId;

                CacheManager.Zones.TryGetValue(clientManager.Zone, out var zoneCore);
                var website = CacheManager.Websites.Values.FirstOrDefault(x => x.Id == zoneCore?.WebsiteId);

                try
                {
                    // Lấy danh sách advertising matched
                    // Match xuống từng level từ campaign -> adset -> ad
                    var runningCampaigns = CacheManager.Campaigns.Values
                        .Where(x => x.Status == CampaignStatus.Running)
                        .Select(x => new CampaignCore
                        {
                            Id = x.Id,
                            SubId = x.SubId,
                            EcommerceDailyBudget = x.EcommerceDailyBudget
                        })
                        .ToList();
                    Logger.LogInformation("runningCampaigns: {Count}", runningCampaigns.Count);

                    var targetedAdvertisingSets = CacheManager.AdvertisingSets.Values
                        .Where(x =>
                        {
                            if (x.Status != AdvertisingSetStatus.Running || runningCampaigns.All(y => y.Id != x.CampaignId))
                            {
                                return false;
                            }

                            var currentCampaign =
                                CacheManager.Campaigns.Values.FirstOrDefault(y => y.Id == x.CampaignId);
                            if (currentCampaign?.CampaignType == CampaignType.Display)
                            {
                                return true;
                            }

                            List<ProductEntityCore> productLists;
                            if (x.ApplyAllProductGroups)
                            {
                                var campaign =
                                    CacheManager.Campaigns.Values.FirstOrDefault(
                                        campaign => campaign.Id == x.CampaignId);
                                var productFeed = CacheManager.ProductFeeds.Values.FirstOrDefault(feed =>
                                    feed.Id == campaign?.EcommerceProductFeedId);
                                if (productFeed?.ValidProductsCount == 0)
                                {
                                    return false;
                                }
                                productLists =
                                    CacheManager.Products.Values.Where(product =>
                                        product.ProductFeedId == productFeed?.Id &&
                                        product.Status == ProductEntityStatus.Running).ToList();
                            }
                            else
                            {
                                var productGroupIds =
                                    CacheManager.AdvertisingSetProductGroups.Values
                                        .Where(y => y.AdvertisingSetId == x.Id &&
                                                    y.Status == AdvertisingSetStatus.Running)
                                        .Select(y => y.ProductGroupId)
                                        .ToList();
                                var productGroupEntities = CacheManager.ProductGroupEntities.Values
                                    .Where(y => productGroupIds.Contains(y.ProductGroupId))
                                    .ToList();
                                productLists = CacheManager.Products
                                    .Values
                                    .Where(y => y.Status == ProductEntityStatus.Running &&
                                                productGroupEntities.Any(group => group.ProductId == y.Id))
                                    .ToList();
                            }

                            return productLists.Count != 0;
                        })
                        .Select(x => x.Id)
                        .ToList();
                    Logger.LogInformation("targetedAdvertisingSets: {Count}", targetedAdvertisingSets.Count);

                    var advertising = new ConcurrentDictionary<long, AdvertisingCore>(
                        CacheManager.Advertising.Where(x =>
                            x.Value.Status == AdvertisingStatus.Running &&
                            targetedAdvertisingSets.Any(y => y == x.Value.AdvertisingSetId)));
                    // lọc ra danh sách QC có thỏa mãn điều kiện cài đặt của nhóm QC không (đối tượng, vị trí, website...)
                    var advertisingMatches = await _advertisingService.MatchesWithZone(advertising, zoneCore, website, @params, clientManager);

                    // Tính tiền chạy QC của các tin QC đã được match
                    // Và kiểm tra xem advertiser có đủ tiền trong tài khoản để chạy không
                    var advertiserIds = advertisingMatches
                        .Select(x =>
                        {
                            var advertiserGuid = x.Advertising?.CreatedBy;
                            var currentAdvertiser =
                                CacheManager.IdentityUsers.Values.FirstOrDefault(user => user.Id == advertiserGuid);
                            return currentAdvertiser?.SubId ?? default;
                        })
                        .Distinct()
                        .Cast<object>()
                        .ToList();
                    var advertiserPaids = await _reportService.GetTotalAdvertiserReport(ReportMetric.Paid,
                        ReportAdvertiserDimension.Advertiser,
                        advertiserIds);
                    foreach (var advertisingMatch in advertisingMatches)
                    {
                        CalculatePrice(advertisingMatch, zoneCore, website);
                    }
                    advertisingMatches = advertisingMatches.Where(x =>
                    {
                        if (x.UseDefault)
                        {
                            return false;
                        }
                        var currentAdvertiser =
                            CacheManager.IdentityUsers.Values.FirstOrDefault(
                                user => user.Id == x.Advertising?.CreatedBy);
                        advertiserPaids.TryGetValue(currentAdvertiser?.SubId ?? default, out var advertiserPaid);

                        var remainingAmount = currentAdvertiser?.Amount - advertiserPaid;
                        return x.SellPrice <= remainingAmount;
                    }).ToList();
                    
                    var matchedCampaignIds = runningCampaigns
                        .Where(x => advertisingMatches.Any(ad => ad.Advertising?.CampaignId == x.Id))
                        .Select(x => x.SubId)
                        .Cast<object>()
                        .ToList();
                    
                    // kiểm tra chiến dịch đã vượt quá ngân sách ngày chưa
                    var dailyCampaignPaids = await _reportService.GetAdvertiserReportByDay(
                        now, 
                        now,
                        ReportMetric.Paid,
                        ReportAdvertiserDimension.Campaign,
                        matchedCampaignIds);

                    advertisingMatches = advertisingMatches.Where(x =>
                    {
                        var campaign = CacheManager.Campaigns.Values.FirstOrDefault(campaign =>
                            campaign.Id.Equals(x.Advertising?.CampaignId));
                        if (campaign is null)
                        {
                            return false;
                        }

                        dailyCampaignPaids.TryGetValue(campaign.SubId, out var paid);
                        return campaign.EcommerceDailyBudget > paid;
                    }).ToList();

                    Logger.LogInformation("advertisingMatches after paid: {Count}", advertisingMatches.Count);

                    // Lấy thông tin template
                    var template = new AdvertisingTemplateCore();
                    if (advertisingMatches.Any())
                    {
                        var templateIds = advertisingMatches
                            .Select(x => x.Advertising)
                            .SelectMany(x => x?.AdvertisingAppliedTemplateCores ?? new List<AdvertisingAppliedTemplateCore>())
                            .Select(x => x.AdvertisingTemplateId)
                            .Where(x => x is not null && zoneCore?.ZoneTemplateCores.Any(y => y.AdvertisingTemplateId == x) == true)
                            .ToList();
                        var hasTemplate = new HashSet<Guid?>(templateIds);
                        var templateId = hasTemplate.Count != 0 ? DisplayTemplate(hasTemplate) : null;
                        template = TemplateCore(templateId);
                        advertisingMatches = advertisingMatches
                            .Where(t => t.Advertising?.AdvertisingAppliedTemplateCores?.Any(x => x.AdvertisingTemplateId == templateId) ?? false)
                            // .OrderByDescending(x => x.Score)
                            .ToList();
                        advertisingMatches = _advertisingService.GetRandomAdvertising(advertisingMatches, template, clientManager);
                    }
                    Logger.LogInformation("advertisingMatches: {Count}", advertisingMatches.Count);

                    // TODO: DONE #11 Hòa NX - Chưa có xử lý banner dự phòng  và banner mặc định
                    // xử lý trong TemplateService

                    var matchedAdvertising = advertisingMatches.FirstOrDefault() ?? new AdvertisingMatchScore
                    {
                        UseDefault = true
                    };

                    // lấy sản phẩm quảng cáo
                    // TODO: select products with algorithm instead of random

                    // TODO: DONE #10 Hòa NX - Xoay vòng sản phẩm chưa đúng, cần lấy sản phẩm phù hợp với tin QC trước - sản phẩm gắn với TQC
                    var matchedAdvertisingSet = CacheManager.AdvertisingSets.Values
                        .FirstOrDefault(x => x.Id == matchedAdvertising.Advertising?.AdvertisingSetId);
                    var matchedCampaign = CacheManager.Campaigns.Values
                        .FirstOrDefault(x => x.Id == matchedAdvertisingSet?.CampaignId);
                    var productMatches = GetRunningProducts(matchedCampaign, matchedAdvertisingSet, template,
                        out var takeProduct);
                    Logger.LogInformation("productMatches: {Count}", productMatches.Count);

                    if (matchedCampaign?.CampaignType == CampaignType.Ecommerce && 
                        productMatches.Count < GetMinimumProducts(template?.TemplateType, takeProduct))
                    {
                        matchedAdvertising.UseDefault = true;
                    }

                    // TODO: DONE #14 Hòa NX - Tách giá mua ra riêng giá mua luôn lấy theo giá mua của pub

                    // TODO: DONE #6 Hòa NX - Nhóm QC chạy đồng giá -> Giá bán là bằng nhau giữa các pub, giá mua thì tính theo giá mua của pub
                    // TODO: DONE #7 Hòa NX - Cần thêm 2 thuộc tính loại tính tiền (CPC hay CPM) cho giá mua giá bán riêng, CPC thì chỉ tính tiền cho click, CPM thì chỉ tính tiền lượt xem (BuyPriceType, SellPriceType)

                    var renderHtmlTemplate = await _templateService.RenderTemplate(matchedAdvertising, productMatches, template, clientManager);
                
                    matchedAdvertising.UseDefault = renderHtmlTemplate.RenderTemplateType.Equals(RenderTemplateTypeEnum.DefaultBanner);
                    matchedAdvertising.UseBackup =
                        renderHtmlTemplate.RenderTemplateType.Equals(RenderTemplateTypeEnum.Backup);

                    var cookieViewManager = new CookieViewManager(request.Cookies);
                    cookieViewManager.AddView(matchedAdvertising.Advertising?.SubId ?? default, clientManager.UrlCreatedOn);
                    
                    var histories = _cookieService.Histories();
                    if (matchedAdvertising.Advertising is not null && matchedAdvertising.UseDefault == false)
                    {
                        histories.Add(new CookieObject
                        {
                            Timestamp = DateTime.Now,
                            AdvertisingId = matchedAdvertising.Advertising.Id
                        });
                    }

                    if (histories.Count == 10)
                    {
                        histories.RemoveAt(0);
                    }

                    var ip = clientManager.Ip;

                    // Publish event nowait cho phép ghi dữ liệu vào mongodb
                    _ = Task.Run(async () =>
                    {
                        using var scope = _serviceScopeFactory.CreateScope();
                        var context = scope.ServiceProvider.GetRequiredService<DisplayContext>();
                        var logger = scope.ServiceProvider.GetRequiredService<ILogger<DisplayParallel.Handler>>();
                        var redisQueueService = scope.ServiceProvider.GetRequiredService<IRedisQueueService>();
                        var locationService = scope.ServiceProvider.GetRequiredService<ILocationService>();
                        var displayParallelHandler = new DisplayParallel.Handler(
                            context, logger, 
                            redisQueueService,
                            locationService);
                        var displayParallel = new DisplayParallel
                        {
                            HttpContextAccessor = _httpContextAccessor,
                            ClientManager = clientManager,
                            MatchedAdvertising = matchedAdvertising,
                            Zone = zoneCore,
                            MatchedTemplate = template,
                            MatchedProducts = productMatches,
                            Ip = ip
                        };
                        await displayParallelHandler.Handle(displayParallel, cancellationToken);
                    }, cancellationToken);

                    return new DisplayResponse
                    {
                        HtmlTemplate = renderHtmlTemplate.HtmlTemplate,
                        ContentType = "text/html",
                        ClientId = clientId,
                        CookieInfoItems = cookieViewManager.CookieInfoItems,
                        CookieHistories = histories
                    };
                }
                catch (Exception e)
                {
                    Interlocked.Increment(ref HealthCounter.FailedRequest);

                    await _publisher.Publish(new ErrorParallel
                    {
                        ClientManager = clientManager,
                        Error = e.ToString(),
                        TrackingType = TrackingType.Impression
                    }, cancellationToken);
                    // use default template
                    var matchedAdvertising = new AdvertisingMatchScore
                    {
                        UseDefault = true
                    };

                    var renderHtmlTemplate = await _templateService.RenderTemplate(matchedAdvertising, new List<ProductEntityMatchScore>(), null, clientManager);
                
                    matchedAdvertising.UseDefault = renderHtmlTemplate.RenderTemplateType.Equals(RenderTemplateTypeEnum.DefaultBanner);
                    matchedAdvertising.UseBackup =
                        renderHtmlTemplate.RenderTemplateType.Equals(RenderTemplateTypeEnum.Backup);
                
                    var cookieViewManager = new CookieViewManager(request.Cookies);
                    
                    _ = Task.Run(async () =>
                    {
                        using var scope = _serviceScopeFactory.CreateScope();
                        var context = scope.ServiceProvider.GetRequiredService<DisplayContext>();
                        var logger = scope.ServiceProvider.GetRequiredService<ILogger<DisplayParallel.Handler>>();
                        var redisQueueService = scope.ServiceProvider.GetRequiredService<IRedisQueueService>();
                        var locationService = scope.ServiceProvider.GetRequiredService<ILocationService>();
                        var displayParallelHandler = new DisplayParallel.Handler(
                            context, logger, 
                            redisQueueService,
                            locationService);
                        var displayParallel = new DisplayParallel
                        {
                            HttpContextAccessor = _httpContextAccessor,
                            ClientManager = clientManager,
                            MatchedAdvertising = matchedAdvertising,
                            Zone = zoneCore,
                            MatchedTemplate = null,
                            MatchedProducts = new List<ProductEntityMatchScore>()
                        };
                        await displayParallelHandler.Handle(displayParallel, cancellationToken);
                    }, cancellationToken);

                    return new DisplayResponse
                    {
                        HtmlTemplate = renderHtmlTemplate.HtmlTemplate,
                        ContentType = "text/html",
                        ClientId = clientId,
                        CookieInfoItems = cookieViewManager.CookieInfoItems
                    };
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.ToString());
                return default!;
            }
        }

        private static Guid? DisplayTemplate(IReadOnlyCollection<Guid?> hasTemplate)
        {
            var ticks = DateTime.Now.Ticks;
            var n = ticks % hasTemplate.Count;
            return hasTemplate.ElementAt((int) n);
        }

        private static AdvertisingTemplateCore TemplateCore(Guid? templateId)
        {
            var templates = CacheManager.Templates
                .Where(x => x.Value.Id.Equals(templateId));
            return templates.FirstOrDefault().Value;

            // var second = DateTime.Now.Second % 10;
            // var keyValuePairs = templates.ToList();
            // TODO: kiểm tra điều kiện timeToDisplay khi chọn template
            // return keyValuePairs.FirstOrDefault(x => x.Value.TimeToDisplay.Equals(second)).Value;
        }

        private static AdvertisingMatchScore CalculatePrice(
            AdvertisingMatchScore matchedAdvertising,
            ZoneCore? zoneCore,
            WebsiteCore? website)
        {
            // calculate price
            // TODO: DONE #8 Hòa NX - Logic tính giá này bị sai rồi, CPM thì mới log tiền cho Impression, CPC thì log tiền cho click thôi
            // tính giá mua
            // giá mua lấy theo giá trong bảng giá pub
            var now = DateTimeOffset.Now;
            var matchedAdvertisingSet = CacheManager.AdvertisingSets.Values
                .FirstOrDefault(x => x.Id == matchedAdvertising.Advertising?.AdvertisingSetId);
            var advertisingAppliedTemplate = matchedAdvertising.Advertising?.AdvertisingAppliedTemplateCores?.FirstOrDefault();
            var template = CacheManager.Templates.Values
                .FirstOrDefault(x => x.Id == advertisingAppliedTemplate?.AdvertisingTemplateId);

            var matchedWebsitePrice = CacheManager.WebsitePrices.Values
                .FirstOrDefault(x =>
                    x.WebsiteId == website?.Id && x.PublisherPriceType == PublisherPriceType.Website &&
                    x.Status == WebsitePriceStatus.Running)
                ?.WebsiteTemplatePriceCores
                ?.FirstOrDefault(x => x.TemplateType == template?.TemplateType);
            
            var matchedZonePrice = CacheManager.WebsitePrices.Values
                .FirstOrDefault(x =>
                    x.ZoneId == zoneCore?.Id && x.PublisherPriceType == PublisherPriceType.Zone &&
                    x.Status == WebsitePriceStatus.Running)
                ?.WebsiteTemplatePriceCores
                ?.FirstOrDefault(x => x.TemplateType == template?.TemplateType);
            if (matchedZonePrice is not null)
            {
                matchedWebsitePrice = matchedZonePrice;
            }
            
            switch (matchedWebsitePrice?.BuyPriceType)
            {
                case PriceType.CPM:
                    matchedAdvertising.BuyPriceType = PriceType.CPM;
                    matchedAdvertising.BuyPrice = matchedWebsitePrice.BuyPrice / 1000;
                    break;
                case PriceType.CPC:
                    matchedAdvertising.BuyPriceType = PriceType.CPC;
                    matchedAdvertising.BuyPrice = matchedWebsitePrice.BuyPrice;
                    break;
            }

            // tính giá bán
            // nếu nhóm quảng cáo bán đồng giá => giá bán lấy theo cấu hình nhóm quảng cáo
            // else lấy theo bảng giá pub
            if (matchedAdvertisingSet?.Uniformed ?? default)
            {
                switch (matchedAdvertisingSet?.UniformedUnit)
                {
                    case UniformedUnit.CPC:
                        matchedAdvertising.SellPriceType = PriceType.CPC; // TODO: DONE # Hòa NX - Bug - kiểu tính tiền CPC nhưng đang gán CPM
                        matchedAdvertising.SellPrice = matchedAdvertisingSet.UniformedPrice ?? default;
                        break;
                    case UniformedUnit.CPM:
                        matchedAdvertising.SellPriceType = PriceType.CPM;
                        matchedAdvertising.SellPrice = matchedAdvertisingSet.UniformedPrice / 1000 ?? default;
                        break;
                }
            }
            else
            {
                if (matchedWebsitePrice is null)
                {
                    matchedAdvertising.UseDefault = true;
                }
                else
                {
                    switch (matchedWebsitePrice.SellPriceType)
                    {
                        case PriceType.CPC:
                            matchedAdvertising.SellPriceType = PriceType.CPC;
                            matchedAdvertising.SellPrice = matchedWebsitePrice.SellPrice;
                            break;
                        case PriceType.CPM:
                            matchedAdvertising.SellPriceType = PriceType.CPM;
                            matchedAdvertising.SellPrice = matchedWebsitePrice.SellPrice / 1000;
                            break;
                    }
                }
            }
            return matchedAdvertising;
        }

        private static List<ProductEntityMatchScore> GetRunningProducts(
            CampaignCore? matchedCampaign,
            AdvertisingSetCore? matchedAdvertisingSet,
            AdvertisingTemplateCore? template,
            out int takeProduct)
        {
            if (matchedCampaign?.CampaignType == CampaignType.Display)
            {
                takeProduct = 0;
                return new List<ProductEntityMatchScore>();
            }
    
            takeProduct = template?.TemplateType switch
            {
                TemplateType.MobileBannerCard => 2,
                TemplateType.FlyingCarpet => 4,
                TemplateType.InteractiveBanner => 5,
                TemplateType.InReadEcommerce => 4,
                TemplateType.PostInRead => 4,
                TemplateType.CatfishEcom => 6,
                _ => 0
            };

            List<ProductEntityMatchScore> productMatches;
            if (matchedAdvertisingSet?.ApplyAllProductGroups == true)
            {
                productMatches = CacheManager.Products
                    .Values
                    .Where(x => x.Status == ProductEntityStatus.Running &&
                                x.ProductFeedId == matchedCampaign?.EcommerceProductFeedId)
                    .OrderBy(_ => Guid.NewGuid())
                    .Take(takeProduct)
                    .Select(x => new ProductEntityMatchScore
                    {
                        Score = 0,
                        ProductEntity = x
                    })
                    .ToList();
            }
            else
            {
                var productGroupIds =
                    CacheManager.AdvertisingSetProductGroups.Values
                        .Where(x => x.AdvertisingSetId == matchedAdvertisingSet?.Id &&
                                    x.Status == AdvertisingSetStatus.Running)
                        .Select(x => x.ProductGroupId)
                        .ToList();
                var productGroupEntities = CacheManager.ProductGroupEntities.Values
                    .Where(x => productGroupIds.Contains(x.ProductGroupId))
                    .ToList();
                // TODO: # Hòa NX - Chưa có giải thuật xoay vòng sản phẩm nhể ?
                productMatches = CacheManager.Products
                    .Values
                    .Where(x => x.Status == ProductEntityStatus.Running &&
                                productGroupEntities.Any(group => group.ProductId == x.Id))
                    .OrderBy(_ => Guid.NewGuid())
                    .Take(takeProduct)
                    .Select(x => new ProductEntityMatchScore
                    {
                        Score = 0,
                        ProductEntity = x
                    })
                    .ToList();
            }
            return productMatches;
        }

        private static int GetMinimumProducts(TemplateType? templateType, int takeProduct)
        {
            return templateType switch
            {
                TemplateType.CatfishEcom => 3,
                TemplateType.InteractiveBanner => 3,
                _ => takeProduct
            };
        }
        
        public void Dispose()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }

    public void Dispose()
    {
        GC.Collect();
        GC.WaitForPendingFinalizers();
    }
}