﻿using System.Web;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Mapper;
using NovanetCore.Business.BusinessDomain.Service.Display;
using Service.Display.Domain;
using Service.Display.Domain.AggregateModels.EventAggregate;
using Service.Display.Parallels;
using Service.Display.Services;

namespace Service.Display.Application.Commands;

public class EventCommand : INovanetRequest<EventResponse>
{
    [FromQuery]
    public Guid ClientId { get; set; }

    public EventType EventType { get; set; }

    public string EventTarget { get; set; } = default!;

    public DateTimeOffset EventTime { get; set; }

    public string? CartId { get; set; }
    
    public List<EventProductDetail>? ProductDetails { get; set; }
    
    public EventPurchaseTransaction? PurchaseTransaction { get; set; }
    
    public long AccountId { get; set; }

    internal class Handler : NovanetRequestHandler<EventCommand, EventResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Publisher _publisher;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            Publisher publisher, IServiceScopeFactory serviceScopeFactory) : base(logger, httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _publisher = publisher;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task<EventResponse> HandleAsync(EventCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var now = DateTimeOffset.Now;

                var clientManager = new ClientManager(
                    _httpContextAccessor,
                    _httpContextAccessor.HttpContext?.Request.Query,
                    now,
                    _httpContextAccessor.HttpContext?.Request.QueryString.ToString(),
                    Logger);

                var @event = request.MapTo<Event>();
                @event.EventTarget = HttpUtility.UrlDecode(@event.EventTarget);
                if (@event.ClientId == Guid.Empty)
                {
                    @event.ClientId = clientManager.ClientId;
                }
                @event.EventTime ??= now;

                _ = Task.Run(async () =>
                {
                    using var scope = _serviceScopeFactory.CreateScope();
                    var context = scope.ServiceProvider.GetRequiredService<DisplayContext>();
                    var eventService = scope.ServiceProvider.GetRequiredService<IEventService>();
                    var globalService = scope.ServiceProvider.GetRequiredService<IGlobalCacheService>();
                    var redisQueueService = scope.ServiceProvider.GetRequiredService<IRedisQueueService>();

                    var handler = new EventParallel.Handler(context, eventService, globalService, redisQueueService);
                    await handler.Handle(new EventParallel
                    {
                        ClientManager = clientManager,
                        Event = @event
                    }, cancellationToken);
                }, cancellationToken);

                return new EventResponse
                {
                    ClientId = @event.ClientId
                };
            }
            catch (Exception e)
            {
                Logger.LogError(e.ToString());
                return new EventResponse
                {
                    ClientId = Guid.Empty
                };
            }
        }
    }
}