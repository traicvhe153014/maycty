﻿using System.Web;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.Mapper;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessDomain.Service.Settings;
using Service.Display.Domain;
using Service.Display.Domain.AggregateModels.EventAggregate;
using Service.Display.Parallels;
using Service.Display.Services;

namespace Service.Display.Application.Commands;

public class VideoCommand : INovanetRequest<bool>
{
    public long ZoneId { get; set; }
    public Guid ClientId { get; set; }
    public long AdvertisingId { get; set; }
    public ReportMetric ReportMetric { get; set; }
    public long AdvertisingTemplateId { get; set; }

    internal class Handler : NovanetRequestHandler<VideoCommand, bool>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor,
            IServiceScopeFactory serviceScopeFactory) : base(logger, httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override async Task<bool> HandleAsync(VideoCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var now = DateTimeOffset.Now;

                var clientManager = new ClientManager(
                    _httpContextAccessor,
                    _httpContextAccessor.HttpContext?.Request.Query,
                    now,
                    _httpContextAccessor.HttpContext?.Request.QueryString.ToString(),
                    Logger);

                _ = Task.Run(async () =>
                {
                    using var scope = _serviceScopeFactory.CreateScope();
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<VideoParallel.Handler>>();
                    var redisQueueService = scope.ServiceProvider.GetRequiredService<IRedisQueueService>();
                    var locationService = scope.ServiceProvider.GetRequiredService<ILocationService>();

                    var handler = new VideoParallel.Handler(logger, redisQueueService, locationService);
                    await handler.Handle(new VideoParallel
                    {
                        ClientManager = clientManager,
                        ZoneId = request.ZoneId,
                        ClientId = request.ClientId,
                        AdvertisingId = request.AdvertisingId,
                        ReportMetric = request.ReportMetric,
                        AdvertisingTemplateId = request.AdvertisingTemplateId
                    }, cancellationToken);
                }, cancellationToken);

                return await Task.FromResult(true);
            }
            catch (Exception e)
            {
                Logger.LogError(e.ToString());
                return await Task.FromResult(false);
            }
        }
    }
}