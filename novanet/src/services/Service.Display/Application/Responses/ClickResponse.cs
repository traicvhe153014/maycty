﻿namespace Service.Display.Application.Responses;

public class ClickResponse
{
    public string RedirectTo { get; set; } = default!;

    public Dictionary<long, string> Clicked { get; set; } = default!;


}