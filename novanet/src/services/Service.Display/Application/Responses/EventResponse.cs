﻿namespace Service.Display.Application.Responses;

public class EventResponse
{
    public Guid ClientId { get; set; }
}