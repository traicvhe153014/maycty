﻿using NovanetCore.Business.BusinessObjects;

namespace Service.Display.Application.Responses;

public class DisplayResponse
{
    public string? HtmlTemplate { get; set; }

    public string ContentType { get; set; } = default!;

    public string? Message { get; set; }
    
    public Guid ClientId { get; set; }

    public Dictionary<long, CookieInfoItem>? CookieInfoItems { get; set; }
    public List<CookieObject>? CookieHistories { get; set; }

    public DisplayResponse()
    {
    }

    public DisplayResponse(string? htmlTemplate, string contentType)
    {
        HtmlTemplate = htmlTemplate;
        ContentType = contentType;
    }

    public DisplayResponse(string message)
    {
        Message = message;
    }
}