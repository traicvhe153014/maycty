﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Controllers;
using NovanetCore.Business.BusinessConfigs;
using Service.Display.Application.Commands;

namespace Service.Display.Controllers;

[Route("display/video")]
public class VideoController : BaseController
{
    public VideoController(IMediator mediator) : base(mediator)
    {
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Index([FromBody] VideoCommand command)
    {
        return Ok(await _mediator.Send(command));
    }
}