﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Controllers;
using NovanetCore.Business.BusinessConfigs;
using Service.Display.Application.Commands;

namespace Service.Display.Controllers;

[ApiController]
[Route("display/event")]
public class EventController : BaseController
{
    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Get([FromQuery] EventQuery query)
    {
        var response = await _mediator.Send(query);
        Response.Headers.AccessControlAllowOrigin = "*";

        return new ContentResult
        {
            ContentType = "text/html",
            Content = response.Data
        };
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Index([FromBody] EventCommand command)
    {
        var commandResponse = await _mediator.Send(command);
        Response.Cookies.Append(ConfigValues.ClientId, commandResponse.Data.ClientId.ToString(),
            new CookieOptions { SameSite = SameSiteMode.None, Secure = true });
        return Ok(commandResponse);
    }

    public EventController(IMediator mediator) : base(mediator)
    {
    }
}