﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Controllers;
using NovanetCore.Business.BusinessConfigs;

namespace Service.Display.Controllers;

[Route("display/[action]")]
public class DisplayController : BaseController
{
    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var query = new DisplayQuery(Request.Query, Request.Cookies, Request.QueryString.ToString());
        var response = await _mediator.Send(query);
        if (response.Data is null)
        {
            return new ContentResult
            {
                ContentType = "text/html",
                Content = string.Empty
            };
        }
        Response.Cookies.Append(ConfigValues.ClientId, response.Data.ClientId.ToString(),
            new CookieOptions {SameSite = SameSiteMode.None, Secure = true});
        if (response.Data.CookieInfoItems is not null)
        {
            Response.Cookies.Append(ConfigValues.CookieView, response.Data.CookieInfoItems.Serialize(),
                new CookieOptions {SameSite = SameSiteMode.None, Secure = true});
        }
        if (response.Data.CookieHistories is not null)
        {
            Response.Cookies.Append(ConfigValues.History, response.Data.CookieHistories.Serialize(),
                new CookieOptions {SameSite = SameSiteMode.None, Secure = true});
        }

        Response.Headers.CacheControl = "no-cache, no-store, must-revalidate";
        Response.Headers.Pragma = "no-cache";
        Response.Headers.Expires = "0";

        return new ContentResult
        {
            ContentType = response.Data.ContentType,
            Content = response.Data.HtmlTemplate,
        };
    }

    public DisplayController(IMediator mediator) : base(mediator)
    {
    }
}