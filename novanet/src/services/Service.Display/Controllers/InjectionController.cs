﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Controllers;
using Novanet.Core.RazorRenderer;
using Novanet.Core.ValueObjects;
using NovanetCore.Business.BusinessConfigs;

namespace Service.Display.Controllers;

[Route("injection/[action]")]
public class InjectionController : BaseController
{
    private readonly AppSettings _appSettings;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ILogger<InjectionController> _logger;
    private readonly RazorRenderer _razorRenderer;

    public InjectionController(IMediator mediator, RazorRenderer razorRenderer, AppSettings appSettings,
        IHttpContextAccessor httpContextAccessor, ILogger<InjectionController> logger) : base(mediator)
    {
        _razorRenderer = razorRenderer;
        _appSettings = appSettings;
        _httpContextAccessor = httpContextAccessor;
        _logger = logger;
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var clientManager = new ClientManager(
            _httpContextAccessor, Request.Query,
            DateTimeOffset.Now, Request.QueryString.ToString(), _logger);
        Response.Cookies.Append(ConfigValues.ClientId, clientManager.ClientId.ToString(),
            new CookieOptions { SameSite = SameSiteMode.None, Secure = true });
        var html = await _razorRenderer.RenderViewToString("Injection/Index", new InjectionViewModel
        {
            GoogleAnalyticProperty = _appSettings.GoogleAnalytics.Property,
            ClientId = clientManager.ClientId
        });
        return new ContentResult
        {
            ContentType = "text/html",
            Content = html
        };
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<int> Increment()
    {
        _appSettings.Data += 1;
        await Task.CompletedTask;
        return _appSettings.Data;
    }
}

public class InjectionViewModel
{
    public Guid ClientId { get; set; }
    public string GoogleAnalyticProperty { get; set; } = default!;
}