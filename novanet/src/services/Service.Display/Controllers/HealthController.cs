﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Novanet.Core.Controllers;
using Service.Display.Services;

namespace Service.Display.Controllers;

[ApiController]
[Route("display/Health/[action]")]
public class HealthController : BaseController
{
    private readonly IDisplayQueueService _displayQueueService;
    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Index([FromQuery] HealthQuery query)
    {
        return Ok(await _mediator.Send(query));
    }
    
    [HttpGet]
    [AllowAnonymous]
    public IActionResult CountQueue()
    {
        return Ok(_displayQueueService.CountQueue());
    }

    public HealthController(IMediator mediator, IDisplayQueueService displayQueueService) : base(mediator)
    {
        _displayQueueService = displayQueueService;
    }
}