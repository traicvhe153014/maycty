﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novanet.Core.Controllers;
using NovanetCore.Business.BusinessConfigs;

namespace Service.Display.Controllers;

[Route("display/Click")]
public class ClickController : BaseController
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IWebHostEnvironment _environment;

    public ClickController(IMediator mediator, IHttpContextAccessor httpContextAccessor, IWebHostEnvironment environment) : base(mediator)
    {
        _httpContextAccessor = httpContextAccessor;
        _environment = environment;
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Index()
    {
        var queryCollection = _httpContextAccessor.HttpContext?.Request.Query;
        var response = await _mediator.Send(new ClickQuery(queryCollection, Request.Cookies));
        Response.Cookies.Append(ConfigValues.Clicked, response.Data.Clicked.Serialize(),
            new CookieOptions {SameSite = SameSiteMode.None, Secure = _environment.IsProduction()});
        return Redirect(response.Data.RedirectTo);
    }
}