﻿using MediatR;
using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessDomain.Service.Settings;
using NovanetCore.Business.BusinessUtils;
using Service.Display.Domain;

namespace Service.Display.Parallels;

public class ClickParallel : INotification
{
    public IHttpContextAccessor HttpContextAccessor { get; set; } = default!;

    public Guid ClientId { get; set; }

    public ClientManager ClientManager { get; set; } = default!;

    public bool IsFraud { get; set; }

    public string Ip { get; set; } = default!;

    public class Handler : INotificationHandler<ClickParallel>
    {
        private readonly ILogger<Handler> _logger;
        private readonly IRedisQueueService _redisQueueService;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly ILocationService _locationService;

        public Handler(ILogger<Handler> logger,
            IRedisQueueService redisQueueService,
            IGlobalCacheService globalCacheService,
            ILocationService locationService)
        {
            _logger = logger;
            _redisQueueService = redisQueueService;
            _globalCacheService = globalCacheService;
            _locationService = locationService;
        }

        public async Task Handle(ClickParallel notification, CancellationToken cancellationToken)
        {
            try
            {
                var now = DateTimeOffset.UtcNow;
                var headers = new Dictionary<string, string>();
                try
                {
                    headers = notification.ClientManager.Headers?.SafeToDictionary(x => x.Key,
                                  x => x.Value.JoinString(","))
                              ?? new Dictionary<string, string>();
                }
                catch (Exception)
                {
                    // ignored
                }
                var xForwardedFor = notification.ClientManager.XForwardedFor;
                var xRealIp = notification.ClientManager.XRealIp;
                headers[RequestHeader.XForwardedFor] = xForwardedFor;
                headers[RequestHeader.XRealIp] = xRealIp;

                var advertising = CacheManager.Advertising.GetValueOrDefault(notification.ClientManager.Advertising);
                var advertisingSet =
                    CacheManager.AdvertisingSets.Values.FirstOrDefault(x => x.Id == advertising?.AdvertisingSetId);
                var campaign = CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == advertisingSet?.CampaignId);

                // trường hợp click sau khi quảng cáo đã tắt thì không lưu tracking
                if (campaign?.Status != CampaignStatus.Running ||
                    advertisingSet?.Status != AdvertisingSetStatus.Running ||
                    advertising?.Status != AdvertisingStatus.Running)
                {
                    return;
                }

                // trường hợp còn tiền mới log tracking
                var currentAdvertiser =
                    CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == advertising.CreatedBy);
                var advertiserPaid = await _redisQueueService.HashGetAsync<double>(
                    RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Advertiser, ReportMetric.Paid,
                        currentAdvertiser?.SubId ?? default),
                    "0");
                var totalCampaignPaid = await _redisQueueService.HashGetAsync<double>(
                    RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Campaign, ReportMetric.Paid,
                        campaign.SubId),
                    "0");
                var dailyCampaignPaid = await _redisQueueService.HashGetAsync<double>(
                    RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Campaign, ReportMetric.Paid,
                        campaign.SubId),
                    DateTimeOffset.Now.GetDayOfYear().ToString());
                var advertiserRemaining = currentAdvertiser?.Amount - advertiserPaid;
                var totalCampaignRemaining = campaign.EcommerceAmountSpent - totalCampaignPaid;
                var dailyCampaignRemaining = campaign.EcommerceDailyBudget - dailyCampaignPaid;

                var sellPrice = notification.ClientManager.SellPrice;
                if (sellPrice > advertiserRemaining ||
                    sellPrice > totalCampaignRemaining ||
                    sellPrice > dailyCampaignRemaining)
                {
                    return;
                }   
                
                var zone = CacheManager.Zones.Values.FirstOrDefault(x => x.SubId == notification.ClientManager.Zone);
                var website = CacheManager.Websites.Values.FirstOrDefault(x => x.Id == zone?.WebsiteId);

                var advertiser = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == advertising.CreatedBy);
                var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == website?.PublisherId);
                CacheManager.Products.TryGetValue(notification.ClientManager.Product, out var product);

                var productGroupIds = CacheManager.AdvertisingSetProductGroups.Values
                    .Where(x =>
                    {
                        if (x.AdvertisingSetId != advertising.AdvertisingSetId)
                        {
                            return false;
                        }

                        return CacheManager.ProductGroupEntities.Values.Any(group =>
                            group.ProductGroupId == x.ProductGroupId &&
                            product?.Id == group.ProductId);
                    })
                    .Select(x => x.SubId)
                    .ToList();
                var productIds = new List<long>();
                if (product is not null)
                {
                    productIds.Add(product.SubId);
                }
                var ipLocation = await _locationService.IPToLocation(notification.Ip);
                var location =
                    CacheManager.Locations.Values.FirstOrDefault(x => x.ProvinceId == ipLocation?.VnProvinceId);

                var tracking = new TrackingWeekPartition
                {
                    Headers = headers,
                    UserAgent = notification.ClientManager.UserAgent,
                    RefererUrl = notification.ClientManager.JsReferer,
                    Ip = ConvertUtils.ConvertIp(notification.Ip),
                    ClientUrl = notification.ClientManager.JsUrl,
                    IsEnableCookie = notification.ClientManager.IsEnableCookie,
                    IsCreateNewClientId = notification.ClientManager.IsCreateNewClientId,
                    HistoryLength = notification.ClientManager.JsHistoryLength,
                    Location = location?.SubId ?? default,
                    IpLocation = ipLocation?.SubId ?? default,

                    ClientId = notification.ClientManager.ClientId,
                    Domain = notification.ClientManager.Domain,
                    ZoneId = notification.ClientManager.Zone,
                    AdvertisingId = notification.ClientManager.Advertising,
                    AdvertisingSetId = advertisingSet.SubId,
                    CampaignId = campaign.SubId,
                    AdvertisingTemplateId = notification.ClientManager.Template,
                    ProductIds = productIds,
                    AdvertisingSetProductGroupIds = productGroupIds,
                    WebsiteId = website?.SubId ?? default,
                    AdvertiserId = advertiser?.SubId ?? default,
                    PublisherId = publisher?.SubId ?? default,
                    CreatedOn = now,
                    ViewTime = now,
                    IsDefault = notification.ClientManager.IsDefault,
                    TrackingType = TrackingType.Click,
                    IsFraud = notification.IsFraud,
                    SellPrice = notification.IsFraud ? 0 : notification.ClientManager.SellPrice,
                    BuyPrice = notification.IsFraud ? 0 : notification.ClientManager.BuyPrice,
                    CreatedFromMinute = now.GetMinuteOfYear()
                };

                await _redisQueueService.ListLeftPushAsync(
                    RedisReportKeys.QueueKey(ReportMetric.Click, tracking.CreatedFromMinute), tracking.Serialize());
                // await _context.TrackingWeekPartitions.AddAsync(tracking);
            }
            catch (Exception e)
            {
                _logger.LogError("{Time}: {Message}", DateTimeOffset.Now, e.Message);
            }
        }
    }
}