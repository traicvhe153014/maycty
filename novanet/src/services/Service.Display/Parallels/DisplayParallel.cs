﻿using System.Collections.Concurrent;
using MediatR;
using Novanet.Core.Constants;
using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessDomain.Service.Settings;
using NovanetCore.Business.BusinessObjects;
using NovanetCore.Business.BusinessUtils;
using Service.Display.Domain;

namespace Service.Display.Parallels;

public class DisplayParallel : INotification
{
    public IHttpContextAccessor HttpContextAccessor { get; set; } = default!;
    public ClientManager ClientManager { get; set; } = default!;
    public ZoneCore? Zone { get; set; }
    public AdvertisingMatchScore? MatchedAdvertising { get; set; }
    public AdvertisingTemplateCore? MatchedTemplate { get; set; }
    public List<ProductEntityMatchScore> MatchedProducts { get; set; } = default!;
    public string Ip { get; set; } = default!;

    public static ConcurrentDictionary<string, bool> FlyingCarpetTrackings = new();

    public class Handler : INotificationHandler<DisplayParallel>
    {
        private readonly DisplayContext _context;
        private readonly ILogger<Handler> _logger;
        private readonly IRedisQueueService _redisQueueService;
        private readonly ILocationService _locationService;

        public Handler(DisplayContext context,
            ILogger<Handler> logger, 
            IRedisQueueService redisQueueService, 
            ILocationService locationService)
        {
            _context = context;
            _logger = logger;
            _redisQueueService = redisQueueService;
            _locationService = locationService;
        }

        public async Task Handle(DisplayParallel notification, CancellationToken cancellationToken)
        {
            Interlocked.Increment(ref HealthCounter.IncomingParallels);
            try
            {
                if (notification.MatchedTemplate?.TemplateType == TemplateType.FlyingCarpet)
                {
                    var trackingKey =
                        $"{notification.Zone?.SubId}:{notification.ClientManager.Ip}:{notification.ClientManager.Time}";
                    if (!FlyingCarpetTrackings.ContainsKey(trackingKey))
                    {
                        FlyingCarpetTrackings[trackingKey] = true;
                    }
                    else
                    {
                        Interlocked.Increment(ref HealthCounter.SuccessfulTrackings);
                        FlyingCarpetTrackings.TryRemove(trackingKey, out _);
                        return;
                    }
                }

                var now = DateTimeOffset.UtcNow;
                var advertising = notification.MatchedAdvertising?.Advertising;
                var headers = new Dictionary<string, string>();
                try
                {
                    headers = notification.ClientManager.Headers?.SafeToDictionary(x => x.Key, x => x.Value.JoinString(",")) ??
                        new Dictionary<string, string>();
                }
                catch (Exception)
                {
                    // ignored
                }
                var xForwardedFor = notification.ClientManager.XForwardedFor;
                var xRealIp = notification.ClientManager.XRealIp;
                headers[RequestHeader.XForwardedFor] = xForwardedFor;
                headers[RequestHeader.XRealIp] = xRealIp;
                var advertiser = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == advertising?.CreatedBy);
                var website = CacheManager.Websites.Values.FirstOrDefault(x => x.Id == notification.Zone?.WebsiteId);
                var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == website?.PublisherId);
                var advertisingSet = CacheManager.AdvertisingSets.Values.FirstOrDefault(x => x.Id == advertising?.AdvertisingSetId);
                var campaign = CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == advertisingSet?.CampaignId);

                var productGroupIds = CacheManager.AdvertisingSetProductGroups.Values
                    .Where(x =>
                    {
                        if (x.AdvertisingSetId != advertising?.AdvertisingSetId)
                        {
                            return false;
                        }
                        return CacheManager.ProductGroupEntities.Values.Any(group =>
                            group.ProductGroupId == x.ProductGroupId &&
                            notification.MatchedProducts.Any(product => product.ProductEntity?.Id == group.ProductId));
                    })
                    .Select(x => x.SubId)
                    .ToList();
                var ipLocation = await _locationService.IPToLocation(notification.Ip);
                var location =
                    CacheManager.Locations.Values.FirstOrDefault(x => x.ProvinceId == ipLocation?.VnProvinceId);

                var tracking = new TrackingWeekPartition
                {
                    Headers = headers,
                    UserAgent = notification.ClientManager.UserAgent,
                    RefererUrl = notification.ClientManager.JsReferer,
                    Ip = ConvertUtils.ConvertIp(notification.Ip),
                    ClientUrl = notification.ClientManager.JsUrl,
                    IsEnableCookie = notification.ClientManager.IsEnableCookie,
                    IsCreateNewClientId = notification.ClientManager.IsCreateNewClientId,
                    HistoryLength = notification.ClientManager.JsHistoryLength,
                    Location = location?.SubId ?? default,
                    IpLocation = ipLocation?.SubId ?? default,

                    ClientId = notification.ClientManager.ClientId,
                    Domain = website?.Domain ?? string.Empty,
                    ZoneId = notification.Zone?.SubId ?? default,
                    AdvertisingId = advertising?.SubId ?? default,
                    AdvertisingSetId = advertisingSet?.SubId ?? default,
                    CampaignId = campaign?.SubId ?? default,
                    AdvertisingTemplateId = notification.MatchedTemplate?.SubId ?? default,
                    WebsiteId = website?.SubId ?? default,
                    CreatedOn = now,
                    ViewTime = DateTimeOffset.FromUnixTimeMilliseconds(notification.ClientManager.Time),
                    IsDefault = notification.MatchedAdvertising?.UseDefault ?? true,
                    IsBackup = notification.MatchedAdvertising?.UseBackup ?? true,
                    TrackingType = TrackingType.Impression,
                    IsPromotion = false,
                    AdvertiserId = advertiser?.SubId ?? default,
                    PublisherId = publisher?.SubId ?? default,
                    NumberOfImpression = 1,
                    ProductIds = notification.MatchedProducts.Select(x => x.ProductEntity?.SubId ?? default).ToList(),
                    AdvertisingSetProductGroupIds = productGroupIds,
                    CreatedFromMinute = now.GetMinuteOfYear()
                };

                if (notification.MatchedAdvertising?.BuyPriceType == PriceType.CPM)
                {
                    tracking.BuyPrice = notification.MatchedAdvertising?.BuyPrice ?? default;
                }
                if (notification.MatchedAdvertising?.SellPriceType == PriceType.CPM)
                {
                    tracking.SellPrice = notification.MatchedAdvertising?.SellPrice ?? default;
                }

                await _redisQueueService.ListLeftPushAsync(
                    RedisReportKeys.QueueKey(ReportMetric.View, tracking.CreatedFromMinute), tracking.Serialize());
                
                // await _context.TrackingWeekPartitions.AddAsync(tracking);
                Interlocked.Increment(ref HealthCounter.SuccessfulTrackings);
            }
            catch (Exception e)
            {
                var errorParallelRequest = new ErrorParallel
                {
                    ClientManager = notification.ClientManager,
                    Error = e.ToString(),
                    TrackingType = TrackingType.Impression
                };
                var errorParallelHandler = new ErrorParallel.Handler(_context);
                await errorParallelHandler.Handle(errorParallelRequest, cancellationToken);
                _logger.LogError("{Time}: {Message}", DateTimeOffset.Now, e.Message);
            }
        }
    }
}