﻿using MediatR;
using NovanetCore.Business.BusinessDomain.Service.Display;
using Service.Display.Domain;

namespace Service.Display.Parallels;

public class ErrorParallel : INotification
{
    public ClientManager ClientManager { get; set; } = default!;
    public string Error { get; set; } = default!;
    public TrackingType TrackingType { get; set; }

    public class Handler : INotificationHandler<ErrorParallel>
    {
        private readonly DisplayContext _context;

        public Handler(DisplayContext context)
        {
            _context = context;
        }

        public async Task Handle(ErrorParallel notification, CancellationToken cancellationToken)
        {
            try
            {
                var headers =
                    notification.ClientManager.Headers?.ToDictionary(x => x.Key, x => x.Value.JoinString(",")) ??
                    new Dictionary<string, string>();
                var tracking = new TrackingError
                {
                    Domain = notification.ClientManager.Domain,
                    Headers = headers,
                    AdvertisingId = notification.ClientManager.Advertising,
                    CampaignId = notification.ClientManager.Campaign,
                    AdvertisingTemplateId = notification.ClientManager.Template,
                    ClientId = notification.ClientManager.ClientId,
                    CreatedAt = DateTimeOffset.Now,
                    TrackingType = notification.TrackingType,
                    ZoneId = notification.ClientManager.Zone,
                    ErrorMessage = notification.Error,
                    UserAgent = notification.ClientManager.UserAgent
                };
                await _context.TrackingErrors.AddAsync(tracking);
            }
            catch
            {
                // ignored
            }
        }
    }
}