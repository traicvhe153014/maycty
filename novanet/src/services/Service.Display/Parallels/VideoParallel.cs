﻿using MediatR;
using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessDomain.Service.Settings;
using NovanetCore.Business.BusinessUtils;
using Service.Display.Domain;

namespace Service.Display.Parallels;

public class VideoParallel : INotification
{
    public Guid ClientId { get; set; }
    public ClientManager ClientManager { get; set; } = default!;
    public long ZoneId { get; set; }
    public long AdvertisingId { get; set; }
    public ReportMetric ReportMetric { get; set; } = default!;
    public string Ip { get; set; } = default!;
    public long AdvertisingTemplateId { get; set; }

    public class Handler : INotificationHandler<VideoParallel>
    {
        private readonly ILogger<Handler> _logger;
        private readonly IRedisQueueService _redisQueueService;
        private readonly ILocationService _locationService;

        public Handler(ILogger<Handler> logger,
            IRedisQueueService redisQueueService,
            ILocationService locationService)
        {
            _logger = logger;
            _redisQueueService = redisQueueService;
            _locationService = locationService;
        }

        public async Task Handle(VideoParallel notification, CancellationToken cancellationToken)
        {
            try
            {
                var now = DateTimeOffset.UtcNow;
                var headers = new Dictionary<string, string>();
                try
                {
                    headers = notification.ClientManager.Headers?.SafeToDictionary(x => x.Key,
                                  x => x.Value.JoinString(","))
                              ?? new Dictionary<string, string>();
                }
                catch (Exception)
                {
                    // ignored
                }
                var xForwardedFor = notification.ClientManager.XForwardedFor;
                var xRealIp = notification.ClientManager.XRealIp;
                headers[RequestHeader.XForwardedFor] = xForwardedFor;
                headers[RequestHeader.XRealIp] = xRealIp;

                var advertising = CacheManager.Advertising.GetValueOrDefault(notification.AdvertisingId);
                var advertisingSet =
                    CacheManager.AdvertisingSets.Values.FirstOrDefault(x => x.Id == advertising?.AdvertisingSetId);
                var campaign = CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == advertisingSet?.CampaignId);

                if (advertising is null || advertisingSet is null || campaign is null)
                {
                    return;
                }

                var zone = CacheManager.Zones.Values.FirstOrDefault(x => x.SubId == notification.ZoneId);
                var website = CacheManager.Websites.Values.FirstOrDefault(x => x.Id == zone?.WebsiteId);

                var advertiser = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == advertising.CreatedBy);
                var publisher = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == website?.PublisherId);
                // CacheManager.Products.TryGetValue(notification.ClientManager.Product, out var product);
                //
                // var productGroupIds = CacheManager.AdvertisingSetProductGroups.Values
                //     .Where(x =>
                //     {
                //         if (x.AdvertisingSetId != advertising.AdvertisingSetId)
                //         {
                //             return false;
                //         }
                //
                //         return CacheManager.ProductGroupEntities.Values.Any(group =>
                //             group.ProductGroupId == x.ProductGroupId &&
                //             product?.Id == group.ProductId);
                //     })
                //     .Select(x => x.SubId)
                //     .ToList();
                // var productIds = new List<long>();
                // if (product is not null)
                // {
                //     productIds.Add(product.SubId);
                // }

                var ipLocation = await _locationService.IPToLocation(notification.Ip);
                var location =
                    CacheManager.Locations.Values.FirstOrDefault(x => x.ProvinceId == ipLocation?.VnProvinceId);

                var tracking = new TrackingWeekPartition
                {
                    Headers = headers,
                    UserAgent = notification.ClientManager.UserAgent,
                    RefererUrl = notification.ClientManager.JsReferer,
                    Ip = ConvertUtils.ConvertIp(notification.Ip),
                    ClientUrl = notification.ClientManager.JsUrl,
                    IsEnableCookie = notification.ClientManager.IsEnableCookie,
                    IsCreateNewClientId = notification.ClientManager.IsCreateNewClientId,
                    HistoryLength = notification.ClientManager.JsHistoryLength,
                    Location = location?.SubId ?? default,
                    IpLocation = ipLocation?.SubId ?? default,

                    ClientId = notification.ClientId,
                    Domain = notification.ClientManager.Domain,
                    ZoneId = notification.ZoneId,
                    AdvertisingId = notification.AdvertisingId,
                    AdvertisingSetId = advertisingSet.SubId,
                    CampaignId = campaign.SubId,
                    AdvertisingTemplateId = notification.AdvertisingTemplateId,
                    // ProductIds = productIds,
                    // AdvertisingSetProductGroupIds = productGroupIds,
                    WebsiteId = website?.SubId ?? default,
                    AdvertiserId = advertiser?.SubId ?? default,
                    PublisherId = publisher?.SubId ?? default,
                    CreatedOn = now,
                    ViewTime = now,
                    IsDefault = false,
                    TrackingType = TrackingType.VideoView,
                    VideoViewMetric = notification.ReportMetric,
                    CreatedFromMinute = now.GetMinuteOfYear()
                };

                await _redisQueueService.ListLeftPushAsync(
                    RedisReportKeys.QueueKey(notification.ReportMetric, tracking.CreatedFromMinute), tracking.Serialize());
            }
            catch (Exception e)
            {
                _logger.LogError("{Time}: {Message}", DateTimeOffset.Now, e.Message);
            }
        }
    }
}