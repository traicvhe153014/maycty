﻿using System.Web;
using MediatR;
using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessConfigs;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessUtils;
using Service.Display.Domain;
using Service.Display.Domain.AggregateModels.EventAggregate;
using EventType = Novanet.Core.Enums.EventType;

namespace Service.Display.Parallels;

public class EventParallel: INotification
{
    public Event Event { get; set; } = default!;
    public ClientManager ClientManager { get; set; } = default!;

    public class Handler : INotificationHandler<EventParallel>
    {
        private readonly DisplayContext _context;
        private readonly IEventService _eventService;
        private readonly IGlobalCacheService _globalCacheService;
        private readonly IRedisQueueService _redisQueueService;

        public Handler(DisplayContext context, IEventService eventService, IGlobalCacheService globalCacheService, IRedisQueueService redisQueueService)
        {
            _context = context;
            _eventService = eventService;
            _globalCacheService = globalCacheService;
            _redisQueueService = redisQueueService;
        }

        public async Task Handle(EventParallel notification, CancellationToken cancellationToken)
        {
            // save to mongo event
            await _context.Events.AddAsync(notification.Event);
            
            // sinh event key trên redis theo cấu hình nhóm đối tượng, sử dụng để
            // check clientId này có match nhóm đối tượng không
            await _eventService.GenerateKeys(notification.Event, notification.ClientManager);
            
            // kiểm tra xem link target có advertisingId của novanet. nếu event là purchaseComplete thì ghi nhận
            // lượt chuyển đổi mua hàng của clientId
            var targetUri = new Uri(notification.Event.EventTarget);
            var advertisingId = HttpUtility.ParseQueryString(targetUri.Query).Get(ConfigValues.NovanetAdvertising);
            if (advertisingId is not null && notification.Event.EventType == EventType.PurchaseComplete)
            {
                long.TryParse(advertisingId, out var parsedAdvertisingId);
                var now = DateTimeOffset.UtcNow;
                var headers = new Dictionary<string, string>();
                try
                {
                    headers = notification.ClientManager.Headers?.SafeToDictionary(x => x.Key,
                                  x => x.Value.JoinString(","))
                              ?? new Dictionary<string, string>();
                }
                catch (Exception)
                {
                    // ignored
                }
                var advertising = CacheManager.Advertising.GetValueOrDefault(parsedAdvertisingId);
                var advertisingSet =
                    CacheManager.AdvertisingSets.Values.FirstOrDefault(x => x.Id == advertising?.AdvertisingSetId);
                var campaign = CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == advertisingSet?.CampaignId);
                var advertiser = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == advertising?.CreatedBy);
                // var publisher = await _globalCacheService.GetAsync<IdentityUserValue>(
                //     RedisKeys.KeySettings<IdentityUserValue>(website?.PublisherId));
                
                CacheManager.Products.TryGetValue(notification.ClientManager.Product, out var product);
                var productGroupIds = CacheManager.AdvertisingSetProductGroups.Values
                    .Where(x =>
                    {
                        if (x.AdvertisingSetId != advertising?.AdvertisingSetId)
                        {
                            return false;
                        }

                        return CacheManager.ProductGroupEntities.Values.Any(group =>
                            group.ProductGroupId == x.ProductGroupId &&
                            product?.Id == group.ProductId);
                    })
                    .Select(x => x.SubId)
                    .ToList();
                var productIds = new List<long>();
                if (product is not null)
                {
                    productIds.Add(product.SubId);
                }
                
                var tracking = new TrackingWeekPartition
                {
                    Headers = headers,
                    UserAgent = notification.ClientManager.UserAgent,
                    RefererUrl = notification.ClientManager.JsReferer,
                    Ip = ConvertUtils.ConvertIp(notification.ClientManager.Ip),
                    ClientUrl = notification.ClientManager.JsUrl,
                    IsEnableFlash = notification.ClientManager.IsEnableFlash,
                    IsEnableCookie = notification.ClientManager.IsEnableCookie,
                    IsCreateNewClientId = notification.ClientManager.IsCreateNewClientId,
                    HistoryLength = notification.ClientManager.JsHistoryLength,

                    ClientId = notification.Event.ClientId,
                    Domain = notification.Event.EventTarget,
                    // ZoneId = notification.ClientManager.Zone,
                    AdvertisingId = parsedAdvertisingId,
                    AdvertisingSetId = advertisingSet?.SubId ?? default,
                    CampaignId = campaign?.SubId ?? default,
                    ProductIds = productIds,
                    AdvertisingSetProductGroupIds = productGroupIds,
                    // WebsiteId = website?.SubId ?? default,
                    AdvertiserId = advertiser?.SubId ?? default,
                    // PublisherId = publisher?.SubId ?? default,
                    CreatedOn = now,
                    ViewTime = now,
                    IsDefault = notification.ClientManager.IsDefault,
                    TrackingType = TrackingType.Conversion,
                    CreatedFromMinute = now.GetMinuteOfYear()
                };
                await _redisQueueService.ListLeftPushAsync(
                    RedisReportKeys.QueueKey(ReportMetric.Conversion, tracking.CreatedFromMinute), tracking.Serialize());
                // await _context.TrackingWeekPartitions.AddAsync(tracking);

            }
        }
    }
}