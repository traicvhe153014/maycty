﻿using Novanet.Core.Mapper;

namespace Service.Display.Domain.AggregateModels.TrackingAggregate;

public class TrackingMonthPartition : Tracking, IMapFrom<TrackingWeekPartition>
{
}