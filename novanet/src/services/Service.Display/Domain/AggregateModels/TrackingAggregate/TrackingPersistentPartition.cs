﻿using Novanet.Core.Mapper;

namespace Service.Display.Domain.AggregateModels.TrackingAggregate;

public class TrackingPersistentPartition : Tracking, IMapFrom<TrackingMonthPartition>
{
}