﻿namespace Service.Display.Domain.AggregateModels.EventAggregate;

public enum EventType
{
    Default = 0,
    PageView = 1,
    ProductView = 2,
    AddToCart = 3,
    Purchase = 4,
    PurchaseComplete = 5
}