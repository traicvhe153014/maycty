﻿using Novanet.Core.Mapper;
using NovanetCore.Business.BusinessDomain.Service.Display;
using Service.Display.Application.Commands;

namespace Service.Display.Domain.AggregateModels.EventAggregate;

public class Event : EventCore, IMapFrom<EventCommand>
{
}