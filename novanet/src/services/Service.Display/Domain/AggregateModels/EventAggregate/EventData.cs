﻿namespace Service.Display.Domain.AggregateModels.EventAggregate;

public class EventData
{
    public string Id { get; set; } = default!;
}