﻿using System.Reflection;
using AioCore.Mongo.Driver;
using AioCore.Mongo.Driver.Abstracts;
using Service.Display.Domain.AggregateModels.EventAggregate;

namespace Service.Display.Domain;

public class DisplayContext : MongoContext
{
    public static readonly string? Schema = typeof(DisplayContext).GetTypeInfo().Assembly.GetName().Name;

    public DisplayContext(IMongoContextBuilder builder) : base(builder)
    {
    }

    public MongoSet<Event> Events { get; set; } = default!;

    public MongoSet<TrackingWeekPartition> TrackingWeekPartitions { get; set; } = default!;

    public MongoSet<TrackingMonthPartition> TrackingMonthPartitions { get; set; } = default!;

    public MongoSet<FraudClick> FraudClicks { get; set; } = default!;

    public MongoSet<FraudClickHistory> FraudClickHistories { get; set; } = default!;

    public MongoSet<TrackingError> TrackingErrors { get; set; } = default!;
}