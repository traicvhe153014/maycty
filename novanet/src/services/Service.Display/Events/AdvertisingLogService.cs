﻿using System.Diagnostics;
using System.IO.Compression;
using MongoDB.Driver;
using Novanet.Core.Mapper;
using Novanet.Core.ValueObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessObjects;
using Service.Display.Domain;
using Service.Display.Parallels;

namespace Service.Display.Events;

public class AdvertisingLogService : EventBusListenerService<object>
{
    private readonly ILogger<AdvertisingLogService> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;
    private bool _isProcessing;

    public AdvertisingLogService(IServiceProvider serviceProvider,
        IServiceScopeFactory serviceScopeFactory,
        ILogger<AdvertisingLogService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
        _serviceScopeFactory = serviceScopeFactory;
    }

    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.AdvertisingLog;
    protected override bool IsWorkQueue => false;

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var context = ServiceProvider.GetRequiredService<DisplayContext>();
        var appSettings = ServiceProvider.GetRequiredService<AppSettings>();
        var mongoLongTime = appSettings.MongoLongTime;

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("AdvertisingLog job started");

        var now = DateTimeOffset.UtcNow;

        foreach (var (trackingKey, _) in DisplayParallel.FlyingCarpetTrackings)
        {
            var keyParts = trackingKey.Split(':');
            var timePart = keyParts.LastOrDefault() ?? "0";
            _ = int.TryParse(timePart, out var timestamp);
            var originTime = DateTimeExtensions.OriginTime.AddMilliseconds(timestamp);
            if (DateTimeOffset.Now - originTime > TimeSpan.FromMinutes(1))
            {
                DisplayParallel.FlyingCarpetTrackings.TryRemove(trackingKey, out _);
            }
        }

        try
        {
            // move month partition to file
            var startOfMonth = now.AddMonths(-1);
            var getDayOfMonth = new DateTimeOffset(startOfMonth.Date);

            var monthTracking = await context.TrackingMonthPartitions
                .Where(x => x.CreatedOn <= getDayOfMonth.AddDays(1).AddMilliseconds(-1) && x.CreatedOn >= getDayOfMonth)
                .ToListAsync();
        
            var savedDirectory = Path.Combine(Path.GetTempPath(), DateTimeOffset.Now.ToString("ddMMyyyy"));
            var savedFile = DateTimeOffset.Now.ToString("ddMMyyyy") + ".txt";
            var savedZipFile = DateTimeOffset.Now.ToString("ddMMyyyy") + ".zip";
            
            _logger.LogInformation("monthTracking count: {StartOfWeek} {GetDayOfWeek}",
                startOfMonth, getDayOfMonth);
            
            if (monthTracking is not null && monthTracking.Count > 0)
            {
                if (File.Exists(Path.Combine(Path.GetTempPath(), savedZipFile)))
                    File.Delete(Path.Combine(Path.GetTempPath(), savedZipFile));
                if (!Directory.Exists(savedDirectory)) Directory.CreateDirectory(savedDirectory);
                (savedDirectory + "/" + savedFile).WriteFile(monthTracking.Serialize());

                ZipFile.CreateFromDirectory(savedDirectory, Path.GetTempPath() + "/" + savedZipFile,
                    CompressionLevel.SmallestSize, true);
                Directory.Delete(savedDirectory, true);

                using var scope = _serviceScopeFactory.CreateScope();
                var messageBus = scope.ServiceProvider.GetRequiredService<IMessageBusClient>();

                var byteFile = File.ReadAllBytes(Path.GetTempPath() + "/" + savedZipFile);

                var message = new CreateLongtimeBackupFile
                {
                    FileByte = byteFile,
                    PathFile = savedDirectory
                };


                messageBus.Publish(MessageBusChannels.CreateLongtimeBackupFile, message);
            }
            else
            {
                _logger.LogWarning("CreateLongtimeBackupFile event not running. Elapsed time: {Time}",
                    stopwatch.ElapsedMilliseconds.ToString());
            }

            // move week partition to month partition
            var startOfWeek = now.AddDays(-7);
            var getDayOfWeek = new DateTimeOffset(startOfWeek.Date);

            var weekTracking = await context.TrackingWeekPartitions
                .Where(x => x.CreatedOn <= getDayOfWeek.AddDays(1).AddMilliseconds(-1) && x.CreatedOn >= getDayOfWeek)
                .ToListAsync();
            
            _logger.LogInformation("weekTracking count: {StartOfWeek} {GetDayOfWeek}",
                startOfWeek, getDayOfWeek);

            if (weekTracking is not null && weekTracking.Count > 0)
            {
                _logger.LogInformation("weekTracking count: {Time} {Count}",
                    stopwatch.ElapsedMilliseconds.ToString(), weekTracking.Count);
                
                var tempTracking = weekTracking.Select(x => x.MapTo<TrackingMonthPartition>());
                await context.TrackingMonthPartitions.AddRangeAsync(tempTracking);
                await Parallel.ForEachAsync(weekTracking, new ParallelOptions { MaxDegreeOfParallelism = 3 },
                    async (tracking, _) => { await context.TrackingWeekPartitions.RemoveAsync(tracking.Id); });
            }
            
            stopwatch.Stop();
            _logger.LogInformation("AdvertisingLog job done. Elapsed time: {Time}",
                stopwatch.ElapsedMilliseconds.ToString());
            _isProcessing = false;
        }
        catch (Exception e)
        {
            stopwatch.Stop();
            _logger.LogError("AdvertisingLog job error. Elapsed time: {Time} {Exception}",
                stopwatch.ElapsedMilliseconds.ToString(), e.ToString());
            _isProcessing = false;
        }
    }
}