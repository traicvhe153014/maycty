﻿using System.Diagnostics;
using MongoDB.Driver;
using Novanet.Core.Enums;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessObjects;
using NovanetCore.Business.BusinessObjects.FraudClickObjects;
using Service.Display.Domain;
using Service.Display.Services.FraudClickServices;

namespace Service.Display.Events;

public class AdvertisingFraudClickService : EventBusListenerService<object>
{
    private const int ClickSourceDays = 15;

    private readonly ILogger<AdvertisingFraudClickService> _logger;
    private bool _isProcessing;

    public AdvertisingFraudClickService(IServiceProvider serviceProvider,
        ILogger<AdvertisingFraudClickService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
    }

    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.AdvertisingFraudClick;
    protected override bool IsWorkQueue => false;

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} started", nameof(AdvertisingFraudClickService));

        try
        {
            var context = ServiceProvider.GetRequiredService<DisplayContext>();
            var now = DateTimeOffset.UtcNow;

            var sourceStartRange = now.AddDays(-ClickSourceDays);
            var processStartRange = now.AddMinutes(-10);

            // dữ liệu click tính từ 15 ngày
            var sourceClicks = (await context.TrackingWeekPartitions
                    .Where(x => x.TrackingType == TrackingType.Click && x.CreatedOn < sourceStartRange)
                    .ToListAsync())
                ?.Cast<Tracking>()
                .ToList() ?? new List<Tracking>();

            // dữ liệu click cần được xử lý để xác định click ảo
            var processClicks = (await context.TrackingWeekPartitions
                    .Where(x => x.TrackingType == TrackingType.Click && x.CreatedOn < processStartRange)
                    .ToListAsync())
                ?.Cast<Tracking>()
                .ToList() ?? new List<Tracking>();

            // dữ liệu click ảo realtime
            var fraudClicksRealTime = processClicks
                .Where(x => x.IsFraud)
                .ToList();

            //Lấy danh sách FdClickId đã lọc trong các giờ trước
            var processedFraudClickIds = await ReadClickIdFormFile(context);

            var dicDataConfig = new Dictionary<string, List<FraudClickKeyConfigValue>>();

            var fraudClicks = new Dictionary<Guid, Tracking>();

            //TODO: set value for _runHistoryId
            long _runHistoryId = 0;

            //1 - loc theo IP
            var ruleIpCount = FraudClickServices.Ip.Process(sourceClicks, processClicks, fraudClicks, dicDataConfig);

            //2 - Loc theo ty le click / domain cua client id
            var ruleClientIdClickRate =
                FraudClickServices.ClientIdClickRates.Process(sourceClicks, processClicks, fraudClicks, dicDataConfig);

            //3 - loc theo ty le click client id/ publisher
            var doubleClick = FraudClickServices.DoubleClick.Process(processClicks, fraudClicks, dicDataConfig);

            //4 - loc theo ty le tao moi clientid
            var newClientClickeds =
                FraudClickServices.CreateTimeClientId.Process(sourceClicks, processClicks, fraudClicks, dicDataConfig);

            //5 - Log luật RealTime vào SQL         
            foreach (var c in fraudClicksRealTime)
                if (!fraudClicks.ContainsKey(c.Id))
                    fraudClicks.Add(c.Id, c);

            //6 - Lọc các click phát sinh ngoài thời gian lịch biểu(OutOfTimeSpan)
            var start = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            var end = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);

            var nowOutOfTimeSpan = now.AddHours(-1);
            var startTimeClickProcess = new DateTime(nowOutOfTimeSpan.Year, nowOutOfTimeSpan.Month,
                nowOutOfTimeSpan.Day, nowOutOfTimeSpan.Hour, 0,
                0); //Mốc lấy click để xử lý(chỉ xử lý các click của giờ trước)

            var outOfTimeSpan =
                FraudClickServices.OutOfTimeSpan.Process(processClicks, fraudClicks, start, end, startTimeClickProcess);

            //7 - Lọc các click  có CountryLocation không hợp lệ
            var dicCountryNotConfig =
                FraudClickServices.CountryNotConfig.Process(processClicks, fraudClicks, start, end);

            //8 - Lọc các click có Vnprovince không hợp lệ(không cấu hình chạy mà có click)
            var vnProvinceNotConfig =
                FraudClickServices.VNProvinceNotConfig.Process(processClicks, fraudClicks, start, end);

            //9 - PercentCutNovanet
            var dicPercentCutNovanet = new Dictionary<Guid, Tracking>();
            if (now.Hour == 0)
                dicPercentCutNovanet = FraudClickServices.PercentCutNovanet.Process(processClicks, fraudClicks);

            //10 - MaxCtr
            var maxCtr = FraudClickServices.MaxCtr.Process(processClicks, fraudClicks, processedFraudClickIds);

            //Lấy danh sách click ảo theo từng rule
            var fraudClicksIp = ruleIpCount.Select(t => t.Value).ToList();
            var fraudClicksClickRates = ruleClientIdClickRate.Select(t => t.Value).ToList();
            var fraudClicksDoubleClick = doubleClick.Select(t => t.Value).ToList();
            var fraudClicksCreateTime = newClientClickeds.Select(t => t.Value).ToList();
            var fraudClicksOutOfTimeSpan = outOfTimeSpan.Select(t => t.Value).ToList();
            var fraudClicksVnProvinceNotConfig = vnProvinceNotConfig.Select(t => t.Value).ToList();
            var fraudClicksMaxCtr = maxCtr.Select(t => t.Value).ToList();
            var fraudClicksPercentCutNovanet = dicPercentCutNovanet.Select(t => t.Value).ToList();
            var fraudClicksCountryNotConfig = dicCountryNotConfig.Select(t => t.Value).ToList();

            //loại bỏ các click đã lọc trong các giờ trước
            fraudClicksIp = fraudClicksIp.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksClickRates = fraudClicksClickRates.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksDoubleClick = fraudClicksDoubleClick.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksCreateTime = fraudClicksCreateTime.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksRealTime = fraudClicksRealTime.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksOutOfTimeSpan =
                fraudClicksOutOfTimeSpan.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksVnProvinceNotConfig = fraudClicksVnProvinceNotConfig
                .Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksMaxCtr = fraudClicksMaxCtr.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksPercentCutNovanet = fraudClicksPercentCutNovanet
                .Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();
            fraudClicksCountryNotConfig =
                fraudClicksCountryNotConfig.Where(t => !processedFraudClickIds.Contains(t.Id)).ToList();

            //loại bỏ các FdClick đã lọc lần trước
            foreach (var item in processedFraudClickIds.Where(item => fraudClicks.ContainsKey(item)))
                fraudClicks.Remove(item);

            //Lấy danh sách id lưu file txt để loại bỏ trong lần lọc sau
            //ghi danh sách clickId ra file txt
            await WriteClickId(context, GetFraudClickIds(fraudClicks, processedFraudClickIds));

            //Lưu dữ liệu click ảo theo từng rule riêng lẻ để monitor
            SaveFraudClickReports(fraudClicksIp, FraudClickTypeEnum.IP);
            SaveFraudClickReports(fraudClicksClickRates, FraudClickTypeEnum.ClientIdClickRates);
            SaveFraudClickReports(fraudClicksDoubleClick, FraudClickTypeEnum.DoubleClick);
            SaveFraudClickReports(fraudClicksCreateTime, FraudClickTypeEnum.CreateTimeClientId);
            SaveFraudClickReports(fraudClicksOutOfTimeSpan, FraudClickTypeEnum.OutOfTimeSpan);
            SaveFraudClickReports(fraudClicksVnProvinceNotConfig, FraudClickTypeEnum.VNProvinceNotConfig);
            SaveFraudClickReports(fraudClicksMaxCtr, FraudClickTypeEnum.MaxCtr);
            SaveFraudClickReports(fraudClicksRealTime, FraudClickTypeEnum.RealTime);
            SaveFraudClickReports(fraudClicksPercentCutNovanet, FraudClickTypeEnum.PercentCutNovanet);
            SaveFraudClickReports(fraudClicksCountryNotConfig, FraudClickTypeEnum.CountryNotConfig);

            //Dữ liệu click ảo cuối cùng = hợp của các rule=> lưu redis
            var lstDataSave = fraudClicks.Values.ToList();
            await SaveData(lstDataSave);

            #region Đánh dấu từng rule cho từng click xem bị loại bởi rule nào lưu log sql

            var fraudClickByRules = new Dictionary<Guid, FraudClick>();

            #region Rules IPS

            foreach (var c in fraudClicksIp)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleIp = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.IP);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules ClickRate

            foreach (var c in fraudClicksClickRates)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleClickRate = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.ClientIdClickRates);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules DoubleClick

            foreach (var c in fraudClicksDoubleClick)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleDoubleClick = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.DoubleClick);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules CreateTimeClientId

            foreach (var c in fraudClicksCreateTime)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleNewClient = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.CreateTimeClientId);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules RealTime

            foreach (var c in fraudClicksRealTime)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleRealTime = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.RealTime);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules OutOfTimeSpan

            foreach (var c in fraudClicksOutOfTimeSpan)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleOutOfTimeSpan = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.OutOfTimeSpan);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules VnProvinceNotConfig

            foreach (var c in fraudClicksVnProvinceNotConfig)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleVNProvinceNotConfig = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.VNProvinceNotConfig);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules CountryNotConfig

            foreach (var c in fraudClicksCountryNotConfig)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleCountryNotConfig = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.CountryNotConfig);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules MaxCtr

            foreach (var c in fraudClicksMaxCtr)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RuleMaxCtr = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.MaxCtr);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #region Rules PercentCutNovanet

            foreach (var c in fraudClicksPercentCutNovanet)
                if (fraudClickByRules.ContainsKey(c.Id))
                {
                    var ruleFlag = fraudClickByRules[c.Id];
                    ruleFlag.RulePercentCutNova = true;
                }
                else
                {
                    var ruleFlag = ToFraudClick(c, _runHistoryId, FraudClickTypeEnum.PercentCutNovanet);
                    fraudClickByRules.Add(c.Id, ruleFlag);
                }

            #endregion

            #endregion

            if (fraudClickByRules.Count > 0) await context.FraudClicks.AddRangeAsync(fraudClickByRules.Values);

            stopwatch.Stop();
            _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(AdvertisingFraudClickService),
                stopwatch.ElapsedMilliseconds.ToString());
            _isProcessing = false;
        }
        catch (Exception ex)
        {
            stopwatch.Stop();
            _logger.LogError("{Name} job error with message: {Message}", nameof(AdvertisingFraudClickService),
                ex.ToString());
            _isProcessing = false;
        }
    }

    private static FraudClick ToFraudClick(Tracking tracking, long runHistoryId, FraudClickTypeEnum fraudClickType)
    {
        var fraudClick = new FraudClick
        {
            ClickId = tracking.Id,
            Ip = tracking.Ip,
            ClientId = tracking.ClientId,
            ZoneId = tracking.ZoneId,
            WebsiteId = tracking.WebsiteId,
            PublisherId = tracking.PublisherId,
            AdvertisingId = tracking.AdvertisingId,
            AdvertisingSetId = tracking.AdvertisingSetId,
            CampaignId = tracking.CampaignId,
            AdvertiserId = tracking.AdvertiserId,
            ClientUrl = tracking.ClientUrl,
            ViewTime = tracking.ViewTime,
            ClickedOn = tracking.CreatedOn,
            RunHistoryId = runHistoryId,
            CountryId = tracking.CountryId,
            RuleClickRate = fraudClickType == FraudClickTypeEnum.ClientIdClickRates,
            RuleCountryNotConfig = fraudClickType == FraudClickTypeEnum.CountryNotConfig,
            RuleDoubleClick = fraudClickType == FraudClickTypeEnum.DoubleClick,
            RuleMaxCtr = fraudClickType == FraudClickTypeEnum.MaxCtr,
            RuleOutOfTimeSpan = fraudClickType == FraudClickTypeEnum.OutOfTimeSpan,
            RulePercentCutNova = fraudClickType == FraudClickTypeEnum.PercentCutNovanet,
            RuleRealTime = fraudClickType == FraudClickTypeEnum.RealTime,
            RuleVNProvinceNotConfig = fraudClickType == FraudClickTypeEnum.VNProvinceNotConfig,
            RuleNewClient = fraudClickType == FraudClickTypeEnum.CreateTimeClientId,
            RuleIp = fraudClickType == FraudClickTypeEnum.IP,
            CreateDate = DateTimeOffset.UtcNow
        };
        return fraudClick;
    }

    private static async Task WriteClickId(DisplayContext context, List<Guid> clickIds)
    {
        await context.Database.DropCollectionAsync(nameof(context.FraudClickHistories));
        await context.FraudClickHistories.AddRangeAsync(clickIds.Select(x => new FraudClickHistory
        {
            ClickId = x
        }));
    }

    private static async Task<List<Guid>> ReadClickIdFormFile(DisplayContext context)
    {
        try
        {
            return await context.FraudClickHistories.Where(x => true).Project(x => x.ClickId).ToListAsync();
        }
        catch (Exception)
        {
            return new List<Guid>();
        }
    }

    private List<Guid> GetFraudClickIds(Dictionary<Guid, Tracking> fraudClicks, List<Guid> processedFraudClickIds)
    {
        //Lấy list FdClickId có trong file nhưng ko có trong KQ lọc
        var fraudClickIds = processedFraudClickIds.Where(item => !fraudClicks.ContainsKey(item)).ToList();

        //lấy list FdClickId mới lọc
        var lstFdClick = fraudClicks.Select(t => t.Value).ToList();
        fraudClickIds.AddRange(lstFdClick.Select(item => item.Id));

        return fraudClickIds;
    }

    private void SaveFraudClickReports(List<Tracking> fraudClicks, FraudClickTypeEnum objStatistics)
    {
        var redisCacheService = ServiceProvider.GetRequiredService<IRedisCacheService>();
        if (fraudClicks is not { Count: > 0 }) return;

        //save redis
        var day = fraudClicks.First().CreatedOn.ToHexCode();
        redisCacheService.ExecuteTransaction(RedisDatabases.ServiceReport, transaction =>
        {
            var groups = fraudClicks.GroupBy(t => t.WebsiteId).ToList();
            foreach (var item in groups)
            {
                var websiteId = item.Key;
                var numberClick = item.Count();
                var key = RedisReportKeys.FraudClickPubKey(objStatistics, ReportPublisherDimension.Website, websiteId);
                transaction.HashIncrementAsync(key.KeyName, "0", numberClick);
                transaction.HashIncrementAsync(key.KeyName, day, numberClick);
            }

            return Task.CompletedTask;
        });
    }

    //Lưu dữ liệu thật để trừ tiền
    private async Task SaveData(IEnumerable<Tracking> lstFdClick)
    {
        //Save redis
        var arrMoney = new[]
        {
            ReportMetric.GetBack, ReportMetric.PromotionGetBack, ReportMetric.Refund, ReportMetric.PromotionRefund
        };

        var logConfig = new LogConfig
        {
            EAdverForSingleObjLog = new List<ReportAdvertiserDimension>
            {
                ReportAdvertiserDimension.Advertising,
                ReportAdvertiserDimension.AdvertisingSet,
                ReportAdvertiserDimension.Campaign,
                ReportAdvertiserDimension.Advertiser,
                ReportAdvertiserDimension.Product,
                ReportAdvertiserDimension.ProductFeed,
                ReportAdvertiserDimension.ProductGroup,
                ReportAdvertiserDimension.AdvertisingSetProductGroup
            },
            EPubForSingleObjLog = new List<ReportPublisherDimension>
            {
                ReportPublisherDimension.Zone,
                ReportPublisherDimension.Website,
                ReportPublisherDimension.Publisher
            },
            EClientSingleObjLog = new List<ReportClientDimension>
            {
                ReportClientDimension.ClientId
            },

            EAdverForAdverTargeting = new List<ReportAdvertiserDimension>
                { ReportAdvertiserDimension.AdvertisingSet, ReportAdvertiserDimension.Campaign },
            // ETargetForAdverTargeting = new List<ETarget> {ETarget.VNPROVINCE, ETarget.COUNTRY},

            EAdverForAdverPub = new List<ReportAdvertiserDimension>
                { ReportAdvertiserDimension.Campaign, ReportAdvertiserDimension.AdvertisingSet },
            EPubForAdverPub = new List<ReportPublisherDimension> { ReportPublisherDimension.Website },

            EPubForPubTargeting = new List<ReportPublisherDimension>
            {
                ReportPublisherDimension.Website,
                ReportPublisherDimension.Zone,
                ReportPublisherDimension.Publisher
            },
            IsLogAdverMoney = true,
            IsLogPubMoney = true
        };

        var redisLogService = ServiceProvider.GetRequiredService<RedisLogService>();
        // var processService = new RedisLogService(lstFdClick, _platform, EObjStatistics.FDCLICK, dbs, arrMoney, logConfig);
        await redisLogService.Save(lstFdClick.Cast<TrackingCore>().ToList(), arrMoney, ReportMetric.FraudClick,
            logConfig);
    }
}