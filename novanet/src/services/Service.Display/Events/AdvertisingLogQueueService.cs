﻿using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Web;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.EventBus;
using NovanetCore.Business.BusinessConfigs;

namespace Service.Display.Events;

[Obsolete("Not used anymore")]
public class AdvertisingLogQueueService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.AdvertisingLogQueue;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<AdvertisingLogQueueService> _logger;

    public AdvertisingLogQueueService(IServiceProvider serviceProvider,
        ILogger<AdvertisingLogQueueService> logger) : base(
        serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        var redisCacheService = ServiceProvider.GetRequiredService<IRedisCacheService>();

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(AdvertisingLogQueueService));

        const string botPattern = "";

        const int timeToGetKey = ConfigValues.TotalMinuteGetData;
        var currentTime = DateTimeOffset.UtcNow.GetMinuteOfYear();
        var endTime = currentTime - 1;
        var startTime = endTime - timeToGetKey;
        var queuedMetrics = new[]
        {
            ReportMetric.Click,
            ReportMetric.View
        };
        foreach (var metric in queuedMetrics)
        {
            for (var i = startTime; i <= endTime; i++)
            {
                var key = RedisReportKeys.QueueKey(metric, i);
                var queuedItems = await redisCacheService.ListAsync(key);
                if (queuedItems is null || queuedItems.Length <= 0) continue;
                var idDictionary = new Dictionary<Guid, int>();

                foreach (var item in queuedItems)
                {
                    var trackingItem = item.ToString().Deserialize<Tracking>();
                    // đánh dấu bot/crawler
                    if (trackingItem.UserAgent != null &&
                        Regex.IsMatch(trackingItem.UserAgent, botPattern, RegexOptions.IgnoreCase))
                    {
                        trackingItem.IsCrawlerBot = true;
                    }
                    else
                    {
                        trackingItem.IsCrawlerBot = false;
                    }

                    // encode url để tránh lỗi khi decode
                    trackingItem.ClientUrl = HttpUtility.UrlEncode(trackingItem.ClientUrl);
                    trackingItem.RefererUrl = HttpUtility.UrlEncode(trackingItem.RefererUrl);

                    //kiểm tra xem Id có bị trùng ko ?
                    if (!idDictionary.ContainsKey(trackingItem.Id))
                        idDictionary.Add(trackingItem.Id, 0);
                    else // bị trùng clickId -> +1 vào
                    {
                        var repairOk = false;
                        while (!repairOk)
                        {
                            trackingItem.CreatedOn = trackingItem.CreatedOn.AddMilliseconds(1);
                            if (!idDictionary.ContainsKey(trackingItem.Id))
                            {
                                repairOk = true;
                                idDictionary.Add(trackingItem.Id, 0);
                            }
                        }
                    }
                }

                //Remove Queue Key + Add Flag
                await redisCacheService.KeyDeleteAsync(key);
                var flagKey = RedisReportKeys.FlagKey(metric, i);
                await redisCacheService.SetAsync(flagKey, queuedItems.Length);
            }
        }

        stopwatch.Stop();
        _logger.LogInformation("{Name} job done. Elapsed time: {Time}", nameof(AdvertisingLogQueueService),
            stopwatch.ElapsedMilliseconds.ToString());
        _isProcessing = false;
    }
}