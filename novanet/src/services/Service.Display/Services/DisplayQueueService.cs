﻿using System.Collections.Concurrent;

namespace Service.Display.Services;

public interface IDisplayQueueService
{
    void Enqueue(Func<Task> task);
    int CountQueue();
}

public class DisplayQueueService : IDisplayQueueService
{
    private readonly ConcurrentQueue<Func<Task>> Tasks = new();
    private readonly ILogger<DisplayQueueService> _logger;

    public DisplayQueueService(ILogger<DisplayQueueService> logger)
    {
        _logger = logger;
        _ = Task.Run(async () =>
        {
            while (true)
            {
                var count = 0;
                const int maxCount = 50;
                while (count < maxCount)
                {
                    var hasTask = Tasks.TryDequeue(out var task);
                    if (hasTask && task is not null)
                    {
                        try
                        {
                            await task();
                        }
                        catch (Exception e)
                        {
                            _logger.LogError("DisplayQueue: {Message}", e.ToString());
                            Tasks.Enqueue(task);
                        }
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                await Task.Delay(500);
            }
        });
    }

    public void Enqueue(Func<Task> task)
    {
        Tasks.Enqueue(task);
    }

    public int CountQueue()
    {
        return Tasks.Count;
    }
}