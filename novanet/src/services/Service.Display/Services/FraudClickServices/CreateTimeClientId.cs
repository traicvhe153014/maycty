﻿using NovanetCore.Business.BusinessControllers;
using NovanetCore.Business.BusinessObjects.FraudClickObjects;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public static class CreateTimeClientId
    {
        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> sourceClicks,
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            var dicClickedOfNewClientIds =
                new Dictionary<Guid, Tracking>(); //ClickId; ClickLog: Danh sách các Click có Thời gian CLick - thời gian tạo CLientId < 300s.

            var dicPubHasClick =
                new Dictionary<long, List<Tracking>>(); //pubId; List Click: Danh sách các Click của các Pub.

            foreach (var c in sourceClicks)
            {
                if (c.ClientId == Guid.Empty) continue;
                //Tổng hợp các Click có thời gian tạo click- tạo client < 300s.
                var timeCreateClientId = c.ClientCreatedOn;
                var ts = c.CreatedOn - timeCreateClientId;
                var timeSetIsNewClient =
                    PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, c.PublisherId, c.AdvertiserId, (int) FraudClickRules.CreateTimeClientId,
                        (int) FraudClickKeyConfig.TimeSetIsNewClient);

                if (ts.TotalSeconds < timeSetIsNewClient)
                {
                    dicClickedOfNewClientIds.Add(c.Id, c);
                }

                //Tổng hợp các Click về theo từng PublisherId.
                if (dicPubHasClick.ContainsKey(c.PublisherId))
                {
                    dicPubHasClick[c.PublisherId].Add(c);
                }
                else
                {
                    dicPubHasClick.Add(c.PublisherId, new List<Tracking> {c});
                }
            }

            var tileClickCuaClientMoiTaoTrenTongClick = (double) dicClickedOfNewClientIds.Count / sourceClicks.Count;

            var dicClickedOnNewClientOnPubInSourceList =
                new Dictionary<long, double>(); //Danh sách chứa: PublisherId; Tỉ lệ click của newClient của 1 Pub / Tổng số Click của Pub đó.

            foreach (var pubHasClick in dicPubHasClick)
            {
                var lstClickOfNewClientOfOnePub =
                    pubHasClick.Value.Where(t => dicClickedOfNewClientIds.ContainsKey(t.Id)).ToList();
                //Tỉ lệ click của newClient của 1 Pub / Tổng số Click của Pub đó.
                var rate = (double) lstClickOfNewClientOfOnePub.Count /
                           pubHasClick.Value.Count; //d.Value.Count: Tổng Click của 1 Pub.
                var filterRate =
                    PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, pubHasClick.Key, 0, (int) FraudClickRules.CreateTimeClientId,
                        (int) FraudClickKeyConfig.FilterRate);
                //Nếu tỉ lệ này > 3 lần tỉ lệ trung bình của 15 ngày => Coi Pubid là Click ảo.
                if (rate >= filterRate * tileClickCuaClientMoiTaoTrenTongClick)
                {
                    dicClickedOnNewClientOnPubInSourceList.Add(pubHasClick.Key,
                        rate); //PublisherId; Tỉ lệ click của newClient của 1 Pub / Tổng số Click của Pub đó.
                }
            }


            var lstClickOfNewClientInProcessList =
                processClicks.Where(t => dicClickedOfNewClientIds.ContainsKey(t.Id)).ToList();
            var lstClickedOnPubHasNewClientInProcessList = lstClickOfNewClientInProcessList
                .Where(c => dicClickedOnNewClientOnPubInSourceList.ContainsKey(c.PublisherId))
                .ToList(); //PublisherId; Tỉ lệ click của newClient của 1 Pub / Tổng số Click của Pub đó trong danh sách Click cần xử lý.

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();
            foreach (var c in lstClickedOnPubHasNewClientInProcessList)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }

            return fraudClickResults;
        }
    }
}