﻿using NovanetCore.Business.BusinessDomain.Service.Settings;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public class CountryLocationChangeHistory
    {
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset ChangeTime { get; set; }
        public HashSet<long> HasCountry { get; set; } = default!;
        public string SessionHashCode { get; set; } = default!;
    }

    public class LogsessionByHashCode
    {
        public DateTimeOffset Time { get; set; }
        public long GroupId { get; set; }
        public HashSet<long> HasCountry { get; set; } = default!;
    }

    public static class CountryNotConfig
    {
        private static List<long> _processClickAdvertisingSetIds = default!;
        private static Dictionary<long, List<CountryLocationChangeHistory>> _countryChangeHistories = default!;

        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            DateTimeOffset startTime,
            DateTimeOffset endTime)
        {
            _processClickAdvertisingSetIds = new List<long>();
            _countryChangeHistories = new Dictionary<long, List<CountryLocationChangeHistory>>();

            //Lấy danh sách AdvertisingSetId trong danh sách click cần xử lý
            GetProcessClickAdvertisingSetIds(processClicks);

            //Lấy lịch sử thay đổi Country của nhóm
            LoadAllCountryChangeHistory(startTime, endTime, _processClickAdvertisingSetIds,
                out _countryChangeHistories);

            //Loc ra danh sách click ảo
            var fraudList = new List<Tracking>();
            foreach (var clickLogMessage in processClicks)
            {
                if (IsFraud(clickLogMessage))
                {
                    fraudList.Add(clickLogMessage);
                }
            }

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();
            foreach (var c in fraudList)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }
            
            // clear memory
            _processClickAdvertisingSetIds = default!;
            _countryChangeHistories = default!;

            return fraudClickResults;
        }

        private static bool IsFraud(Tracking click)
        {
            if (!_countryChangeHistories.ContainsKey(click.AdvertisingSetId)) return true;
            var countryId = click.CountryId;
            var lstCountryChange = _countryChangeHistories[click.AdvertisingSetId];
            foreach (var vnProvinceChange in lstCountryChange)
            {
                if (click.CreatedOn <= vnProvinceChange.StartTime ||
                    click.CreatedOn > vnProvinceChange.ChangeTime.AddMinutes(10)) continue;
                if (vnProvinceChange.HasCountry.Contains(countryId))
                {
                    return false;
                }
            }

            return true;
        }

        //ghép cấu hình hiện tại với lịch sử thay đổi cấu hình
        private static void LoadAllCountryChangeHistory(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            List<long> advertisingSetIds,
            out Dictionary<long, List<CountryLocationChangeHistory>> dicAllCountryChangeHistory)
        {
            //lấy cấu hình location hiện tại
            var dicCurrentCountry = new Dictionary<long, HashSet<long>>();
            LoadCurrentCountryConfig(advertisingSetIds, dicCurrentCountry);

            //Lấy lịch sử các lần thay đổi location
            var dicCountryChangeHistory = new Dictionary<long, List<CountryLocationChangeHistory>>();
            LoadCountryChangeHistoryOfGroup(startTime, endTime, dicCountryChangeHistory);

            //ghép lịch sử thay đổi với cấu hình hiện tại
            foreach (var advertisingSet in dicCurrentCountry)
            {
                //- Nếu nhóm không có lần thay đổi cấu hình nào thì thêm nhóm đó với cấu hình hiện tại, starttime=0h đầu ngày
                //- Nếu nhóm có thay đổi cấu hình thì thêm cấu hình hiện tại với starttime = changeTime của lần cấu hình trước
                if (!dicCountryChangeHistory.ContainsKey(advertisingSet.Key))
                {
                    dicCountryChangeHistory.Add(advertisingSet.Key, new List<CountryLocationChangeHistory>());
                }

                var currentTimeSpanOfAdvGroup = new CountryLocationChangeHistory()
                {
                    ChangeTime = endTime,
                    HasCountry = advertisingSet.Value
                };

                var numberChangeHistory = dicCountryChangeHistory[advertisingSet.Key].Count;
                currentTimeSpanOfAdvGroup.StartTime = numberChangeHistory == 0
                    ? new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0)
                    : dicCountryChangeHistory[advertisingSet.Key][numberChangeHistory - 1].ChangeTime;

                dicCountryChangeHistory[advertisingSet.Key].Add(currentTimeSpanOfAdvGroup);
            }

            dicAllCountryChangeHistory = dicCountryChangeHistory;
        }

        //Lấy cấu hình location hiện tại
        private static void LoadCurrentCountryConfig(List<long> advertisingSetIds,
            Dictionary<long, HashSet<long>> dicCurrentLocation)
        {
            var countries = CacheManager.AdvertisingSets.Values
                .Where(t => advertisingSetIds.Contains(t.SubId))
                .SelectMany(x => x.LocationCores ?? new List<AdvertisingSetLocationCore>())
                .DistinctBy(x => x.SubId)
                .ToList();
            var advertisingSets = CacheManager.AdvertisingSets.Values
                .Where(t => advertisingSetIds.Contains(t.SubId))
                .ToList();
            foreach (var advertisingSet in advertisingSets)
            {
                var lstCountryLocationId = countries
                    .Where(t => t.AdvertisingSetCore?.SubId == advertisingSet.SubId)
                    .Select(t => t.Location?.SubId ?? default)
                    .ToList();
                dicCurrentLocation.Add(advertisingSet.SubId, new HashSet<long>());
                dicCurrentLocation[advertisingSet.SubId].UnionWith(lstCountryLocationId);
            }
        }

        //Lấy lịch sử thay đổi location startTime, endTime
        private static void LoadCountryChangeHistoryOfGroup(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            Dictionary<long, List<CountryLocationChangeHistory>> dicLocationChangeHistory)
        {
            var lstLocationChangeHistory = new Dictionary<long, List<CountryLocationChangeHistory>>();
            var lstLogSessionInDay = CacheManager.LogSessions.Values
                .Where(t => t.ModifiedType == 1
                            && t.Time >= startTime && t.Time <= endTime
                            && t.TableName == "Service.Settings.AdvertisingSetLocations")
                .OrderBy(t => t.Time)
                .ToList();

            var dicTemp = new Dictionary<string, LogsessionByHashCode>();
            foreach (var logSession in lstLogSessionInDay)
            {
                var objLog = $"{{{logSession.OldObject}}}".Deserialize<AdvertisingSetLocationCore>();
                var sessionHashCode = logSession.SessionHashCode;
                if (!dicTemp.ContainsKey(sessionHashCode))
                {
                    dicTemp.Add(sessionHashCode, new LogsessionByHashCode
                    {
                        Time = logSession.Time ?? DateTimeOffset.UtcNow,
                        GroupId = objLog.AdvertisingSetCore?.SubId ?? default,
                        HasCountry = new HashSet<long>()
                    });
                }

                dicTemp[sessionHashCode].HasCountry.Add(objLog.Location?.SubId ?? default);
            }

            var lstDataTem = dicTemp.Values.OrderBy(t => t.Time).ToList();
            foreach (var logSessionAdvertisingSet in lstDataTem)
            {
                var groupId = logSessionAdvertisingSet.GroupId;
                if (!lstLocationChangeHistory.ContainsKey(groupId))
                {
                    lstLocationChangeHistory.Add(groupId, new List<CountryLocationChangeHistory>());
                }

                var countryLocationChangeHistory = new CountryLocationChangeHistory
                {
                    ChangeTime = logSessionAdvertisingSet.Time,
                    HasCountry = logSessionAdvertisingSet.HasCountry
                };

                lstLocationChangeHistory[groupId].Add(countryLocationChangeHistory);
            }

            //tính mốc startTime của các lần thay đổi
            //+ Starttime bằng changetime của lần thay đổi trước
            //+ nếu lần thay đổi trước không có thì gán starttime là đầu ngày
            foreach (var advertisingSet in lstLocationChangeHistory)
            {
                var advGroupId = advertisingSet.Key;
                if (!dicLocationChangeHistory.ContainsKey(advGroupId))
                {
                    dicLocationChangeHistory.Add(advGroupId, new List<CountryLocationChangeHistory>());
                }

                var lstChangeLocation = advertisingSet.Value;
                lstChangeLocation = lstChangeLocation.OrderBy(t => t.ChangeTime).ToList();

                for (var i = 0; i < lstChangeLocation.Count; i++)
                {
                    var previousIndex = i - 1;
                    var start = previousIndex >= 0
                        ? lstChangeLocation[previousIndex].ChangeTime
                        : new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                    lstChangeLocation[i].StartTime = start;
                }

                dicLocationChangeHistory[advGroupId].AddRange(lstChangeLocation);
            }
        }

        private static void GetProcessClickAdvertisingSetIds(List<Tracking> processClicks)
        {
            foreach (var click in processClicks.Where(click =>
                         !_processClickAdvertisingSetIds.Contains(click.AdvertisingSetId)))
            {
                _processClickAdvertisingSetIds.Add(click.AdvertisingSetId);
            }
        }
    }
}