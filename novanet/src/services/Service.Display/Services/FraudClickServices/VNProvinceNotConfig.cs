﻿using NovanetCore.Business.BusinessDomain.Service.Settings;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public class LocationChangeHistory
    {
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset ChangeTime { get; set; }
        public HashSet<int> HasVnProvince { get; set; } = default!;
        public string SessionHashCode { get; set; } = default!;
    }

    public class ObjGroupLogSessionByHashCode
    {
        public DateTimeOffset Time { get; set; }
        public long GroupId { get; set; }
        public HashSet<int> HasVnProvince { get; set; } = default!;
    }

    public static class VNProvinceNotConfig
    {
        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            DateTimeOffset startTime,
            DateTimeOffset endTime)
        {
            //Lấy danh sách AdvertisingSetId trong danh sách click cần xử lý
            var clickProcessAdvertisingSetIds = GetGroupIdInLstClickProcess(processClicks);

            //Lấy lịch sử thay đổi location của nhóm
            // LoadAllLocationChangeHistory(startTime, endTime, _lstAdvgroupIdInLstClickProcess, ref _dicLocationChangeHistory);

            //Lấy lịch sử thay đổi VnProvince của nhóm
            LoadAllVnProvinceChangeHistory(startTime, endTime, clickProcessAdvertisingSetIds,
                out var vnProvinceChangeHistories);

            //Loc ra danh sách click ảo
            var fraudClickList = processClicks
                .Where(clickLogMessage => IsFraud(clickLogMessage, vnProvinceChangeHistories)).ToList();

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();
            foreach (var c in fraudClickList)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }

            return fraudClickResults;
        }

        private static bool IsFraud(
            Tracking click,
            Dictionary<long, List<LocationChangeHistory>> vnProvinceChangeHistories)
        {
            if (!vnProvinceChangeHistories.ContainsKey(click
                    .AdvertisingSetId)) return true;
            var locationIdOfClick = click.VnProvinceId;
            //if (click.IP == CoreUtils.ConvertIP("127.0.0.1")) locationIdOfClick = 1;
            if (locationIdOfClick != 1 && locationIdOfClick != 2 && locationIdOfClick != 4 &&
                locationIdOfClick != 15)
            {
                //var lstLocationChange = _dicLocationChangeHistory[click.AdvertisingSetId];
                var lstVnProvinceChange = vnProvinceChangeHistories[click.AdvertisingSetId];
                return lstVnProvinceChange
                    .Where(vnProvinceChange => click.CreatedOn > vnProvinceChange.StartTime &&
                                               click.CreatedOn <= vnProvinceChange.ChangeTime.AddMinutes(10))
                    .All(vnProvinceChange => !vnProvinceChange.HasVnProvince.Contains(locationIdOfClick));
            }

            return false;
        }

        //ghép cấu hình hiện tại với lịch sử thay đổi cấu hình
        private static void LoadAllVnProvinceChangeHistory(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            List<long> advertisingSetIds,
            out Dictionary<long, List<LocationChangeHistory>> vnProvinceChangeHistories)
        {
            //lấy cấu hình location hiện tại
            var dicCurrentVnProvince = new Dictionary<long, HashSet<int>>();
            LoadCurrentVnProvinceConfig(advertisingSetIds, dicCurrentVnProvince);

            //Lấy lịch sử các lần thay đổi location
            var dicVnProvinceChangeHistory = new Dictionary<long, List<LocationChangeHistory>>();
            LoadVnProvinceChangeHistoryOfGroup(startTime, endTime, dicVnProvinceChangeHistory);

            //ghép lịch sử thay đổi với cấu hình hiện tại
            foreach (var advertisingSet in dicCurrentVnProvince)
            {
                //- Nếu nhóm không có lần thay đổi cấu hình nào thì thêm nhóm đó với cấu hình hiện tại, starttime=0h đầu ngày
                //- Nếu nhóm có thay đổi cấu hình thì thêm cấu hình hiện tại với starttime = changeTime của lần cấu hình trước
                if (!dicVnProvinceChangeHistory.ContainsKey(advertisingSet.Key))
                {
                    dicVnProvinceChangeHistory.Add(advertisingSet.Key, new List<LocationChangeHistory>());
                }

                var currentTimeSpanOfAdvertisingSet = new LocationChangeHistory
                {
                    ChangeTime = endTime,
                    HasVnProvince = advertisingSet.Value
                };

                var numberChangeHistory = dicVnProvinceChangeHistory[advertisingSet.Key].Count;
                currentTimeSpanOfAdvertisingSet.StartTime = numberChangeHistory == 0
                    ? new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0)
                    : dicVnProvinceChangeHistory[advertisingSet.Key][numberChangeHistory - 1].ChangeTime;

                dicVnProvinceChangeHistory[advertisingSet.Key].Add(currentTimeSpanOfAdvertisingSet);
            }

            vnProvinceChangeHistories = dicVnProvinceChangeHistory;
        }

        //Lấy cấu hình location hiện tại
        private static void LoadCurrentVnProvinceConfig(
            List<long> advertisingSetIds,
            Dictionary<long, HashSet<int>> dicCurrentLocation)
        {
            var provinces = CacheManager.AdvertisingSets.Values
                .Where(t => advertisingSetIds.Contains(t.SubId))
                .SelectMany(x => x.LocationCores ?? new List<AdvertisingSetLocationCore>())
                .DistinctBy(x => x.SubId)
                .ToList();
            var advertisingSets = CacheManager.AdvertisingSets.Values
                .Where(t => advertisingSetIds.Contains(t.SubId))
                .ToList();
            foreach (var advGroup in advertisingSets)
            {
                var provinceIds = provinces
                    .Where(t => t.AdvertisingSetId == advGroup.Id)
                    .Select(t => t.Location?.ProvinceId ?? default)
                    .ToList();
                dicCurrentLocation.Add(advGroup.SubId, new HashSet<int>());
                dicCurrentLocation[advGroup.SubId].UnionWith(provinceIds);
            }
        }

        //Lấy lịch sử thay đổi location startTime, endTime
        private static void LoadVnProvinceChangeHistoryOfGroup(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            Dictionary<long, List<LocationChangeHistory>> dicBitLocationChangeHistory)
        {
            var locationChangeHistories = new Dictionary<long, List<LocationChangeHistory>>();
            var lstLogSessionInDay = CacheManager.LogSessions
                .Values
                .Where(t => t.ModifiedType == 1 && t.Time >= startTime && t.Time <= endTime &&
                            t.TableName == "Service.Settings.AdvertisingSetLocations")
                .OrderBy(t => t.Time)
                .ToList();

            var dicTemp = new Dictionary<string, ObjGroupLogSessionByHashCode>();
            foreach (var logSession in lstLogSessionInDay)
            {
                var objLog = $"{{{logSession.OldObject}}}".Deserialize<AdvertisingSetLocationCore>();
                var sessionHashCode = logSession.SessionHashCode;
                if (!dicTemp.ContainsKey(sessionHashCode))
                {
                    dicTemp.Add(sessionHashCode, new ObjGroupLogSessionByHashCode
                    {
                        Time = logSession.Time ?? DateTimeOffset.UtcNow,
                        GroupId = objLog.AdvertisingSetCore?.SubId ?? default,
                        HasVnProvince = new HashSet<int>()
                    });
                }

                dicTemp[sessionHashCode].HasVnProvince.Add(objLog.Location?.ProvinceId ?? default);
            }


            var lstDataTem = dicTemp.Values.OrderBy(t => t.Time).ToList();
            foreach (var logSessionAdvertisingSet in lstDataTem)
            {
                var groupId = logSessionAdvertisingSet.GroupId;
                if (!locationChangeHistories.ContainsKey(groupId))
                {
                    locationChangeHistories.Add(groupId, new List<LocationChangeHistory>());
                }

                var objLocationChange = new LocationChangeHistory
                {
                    ChangeTime = logSessionAdvertisingSet.Time,
                    HasVnProvince = logSessionAdvertisingSet.HasVnProvince
                };

                locationChangeHistories[groupId].Add(objLocationChange);
            }

            //tính mốc startTime của các lần thay đổi
            //+ Starttime bằng changetime của lần thay đổi trước
            //+ nếu lần thay đổi trước không có thì gán starttime là đầu ngày
            foreach (var advertisingSet in locationChangeHistories)
            {
                var advertisingSetId = advertisingSet.Key;
                if (!dicBitLocationChangeHistory.ContainsKey(advertisingSetId))
                {
                    dicBitLocationChangeHistory.Add(advertisingSetId, new List<LocationChangeHistory>());
                }

                var changeLocations = advertisingSet.Value;
                changeLocations = changeLocations.OrderBy(t => t.ChangeTime).ToList();

                for (var i = 0; i < changeLocations.Count; i++)
                {
                    var previousIndex = i - 1;
                    var start = previousIndex >= 0
                        ? changeLocations[previousIndex].ChangeTime
                        : new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                    changeLocations[i].StartTime = start;
                }

                dicBitLocationChangeHistory[advertisingSetId].AddRange(changeLocations);
            }
        }

        private static List<long> GetGroupIdInLstClickProcess(List<Tracking> processClicks)
        {
            return processClicks.Select(x => x.AdvertisingSetId).Distinct().ToList();
        }
    }
}