﻿using NovanetCore.Business.BusinessControllers;
using NovanetCore.Business.BusinessObjects.FraudClickObjects;
using NovanetCore.Business.BusinessUtils;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public static class ClientIdClickRates
    {
        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> sourceClicks,
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            var fraudClientIds = new Dictionary<Guid, int>();
            var clientIds = new Dictionary<Guid, int>();
            var clientIdClickDomains =
                new Dictionary<Guid, Dictionary<string, int>>(); //Danh sách chứa số Click của 1 ClientId ứng với từng Domain. long: ClientId, String: Domain, int: Số Click. 

            foreach (var click in sourceClicks)
            {
                if (!clientIds.ContainsKey(click.ClientId))
                {
                    clientIds.Add(click.ClientId, 1);
                }
                else
                {
                    clientIds[click.ClientId]++;
                }

                //dic clientid - click - domain
                var domain = UrlUtils.GetDomainByUrl(click.ClientUrl);
                if (!clientIdClickDomains.ContainsKey(click.ClientId))
                {
                    clientIdClickDomains.Add(click.ClientId,
                        new Dictionary<string, int> {{domain, 1}});
                }
                else
                {
                    var dicDomainClick = clientIdClickDomains[click.ClientId];
                    if (dicDomainClick.ContainsKey(domain))
                    {
                        clientIdClickDomains[click.ClientId][domain]++;
                    }
                    else
                    {
                        clientIdClickDomains[click.ClientId].Add(domain, 1);
                    }
                }
            }

            //Tinh ty le click trung binh/ 1 client id/ 1 domain
            var tongDomainCuaTatCaClientId = 0; //Tổng số Domain có Click của tất cả ClientId
            var tongClickCuaTatCaClientId = 0; //Tổng số Click của tất cả các Domain của tất cả ClientId.
            foreach (var dicClickDomain in clientIdClickDomains)
            {
                tongDomainCuaTatCaClientId += dicClickDomain.Value.Count;
                tongClickCuaTatCaClientId += dicClickDomain.Value.Sum(t => t.Value);
            }

            var tyleClickTrungBinhMoiDomainCuaTatCaClientId =
                (double) tongClickCuaTatCaClientId /
                tongDomainCuaTatCaClientId; //Tỉ lệ Click t/bình trên mỗi Domain của tất cả ClientId. VD: 1 Domain có 2 click.

            //Tinh ty le click trung binh 1 clientid. VD: 1 ClientId có 3,5 Click.
            var tongSoClickCuaTatCaClientId =
                sourceClicks.Count(t => t.ClientId != Guid.Empty); //Tổng số Click của tất cả ClientId.
            var tongSoClientId = clientIds.Count(t => t.Key != Guid.Empty); //Tổng số ClientId.
            var soClickTrungBinhTren1ClientId = (double) tongSoClickCuaTatCaClientId / tongSoClientId;

            //Nếu số CLick của 1 ClienId > 3 lần số Click trung bình của 1 ClientId => Loai bo click
            foreach (var i in clientIds)
            {
                //Nếu số Click của 1 ClienId > 3 lần số Click trung bình của 1 ClientId và Số click trung bình/1 domain > 3 lần số Click trung bình / 1 Domain của tất cả Client => Loai bo click
                var clientIdClickAverageRates =
                    PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, 0, 0, (int) FraudClickRules.ClientIdClickRates,
                        (int) FraudClickKeyConfig.ClientIdClickAverageRates);
                if (!(soClickTrungBinhTren1ClientId * clientIdClickAverageRates < i.Value) ||
                    i.Key == Guid.Empty) continue;
                var clientIdClickDomain = clientIdClickDomains[i.Key]; //Domain;AllClick của Domain.
                var tongSoDomainCoClickCua1ClientId = clientIdClickDomain.Count;
                var tongClickCuaClientId =
                    clientIdClickDomain.Sum(t => t.Value); // Tổng số Click của tất cả ClientId trên tất cả Domain.

                var tyleClickTrungBinhMoiDomainCua1ClientId =
                    (double) tongClickCuaClientId / tongSoDomainCoClickCua1ClientId;

                //Neu ty le click trung binh cua client do tren 1 domain > 3 lan so voi ty le click trung binh
                var domainClickAverageRates = PublisherAdvertiserRulesConfigController
                    .GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, 0, 0, (int) FraudClickRules.ClientIdClickRates,
                        (int) FraudClickKeyConfig.DomainClickAverageRates);
                if (tyleClickTrungBinhMoiDomainCua1ClientId >
                    tyleClickTrungBinhMoiDomainCuaTatCaClientId * domainClickAverageRates)
                {
                    fraudClientIds.Add(i.Key, 1);
                }
            }

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();
            foreach (var c in processClicks.Where(c => fraudClientIds.ContainsKey(c.ClientId)))
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }

            return fraudClickResults;
        }
    }
}