﻿using NovanetCore.Business.BusinessDomain.Service.Settings;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public class TimeSpanChangeHistoryOfCampaign
    {
        public TimeSpanChangeHistoryOfCampaign()
        {
            LstCampaignTimeSpan = new List<CampaignSchedulingCore>();
        }

        public List<CampaignSchedulingCore> LstCampaignTimeSpan { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset ChangeTime { get; set; }
    }

    public class TimeSpanChangeHistoryOfAdvertisingSet
    {
        public TimeSpanChangeHistoryOfAdvertisingSet()
        {
            LstAdvGroupTimeSpan = new List<AdvertisingSetSchedulingCore>();
        }

        public List<AdvertisingSetSchedulingCore> LstAdvGroupTimeSpan { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset ChangeTime { get; set; }
    }

    public class ShareTimeSpanHistoryOfCampaign
    {
        public bool ShareTimeSpan { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset ChangeTime { get; set; }
    }

    public static class OutOfTimeSpan
    {
        private static List<long> _lstCampaignIdOfCampaignShareTimeSpan = default!,
            _clickProcessCampaignIds = default!,
            _clickProcessAdvertisingSetIds = default!;

        private static Dictionary<long, List<ShareTimeSpanHistoryOfCampaign>> _dicShareTimeSpanChangeHistoryOfCampaign =
            default!;

        private static Dictionary<long, List<TimeSpanChangeHistoryOfCampaign>> _dicTimeSpanChangeHistoryCampaign =
            default!;

        private static Dictionary<long, List<TimeSpanChangeHistoryOfAdvertisingSet>> _dicTimeSpanChangeHistoryAdvGroup =
            default!;

        //Tư tưởng: Lọc các click phát sinh ngoài thời gian cấu hình lịch biểu chạy của chiến dịch hoặc nhóm
        // - Tạo danh sách các lần thay đổi timespan của chiến dịch(1)
        // - Tạo danh sách các lần thay đổi timespan của nhóm(2)
        // - Tạo danh sách các lần thay đổi ShareTimeSpan của chiến dịch
        // - Xét ClickedOn của click với các khoảng trong danh sách ShareTimeSpan của chiến dịch,
        // tùy theo cấu hình tại khoảng đó để check tiếp trong danh sách (1) hoặc (2)
        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            DateTimeOffset startTimeClickProcess)
        {
            //Nếu là 0 giờ thì không chạy rule này(0h mốc chuyển giao ngày)
            if (DateTimeOffset.UtcNow.Hour == 0)
            {
                return new Dictionary<Guid, Tracking>();
            }

            //khởi tạo biến
            _clickProcessCampaignIds = new List<long>();
            _lstCampaignIdOfCampaignShareTimeSpan = new List<long>();
            _clickProcessAdvertisingSetIds = new List<long>();
            _dicTimeSpanChangeHistoryCampaign = new Dictionary<long, List<TimeSpanChangeHistoryOfCampaign>>();
            _dicTimeSpanChangeHistoryAdvGroup = new Dictionary<long, List<TimeSpanChangeHistoryOfAdvertisingSet>>();
            _dicShareTimeSpanChangeHistoryOfCampaign = new Dictionary<long, List<ShareTimeSpanHistoryOfCampaign>>();

            //Lấy danh sách CampaignId và danh sách AdvertisingSetId có click
            //chỉ lấy các click của giờ trước để xử lý, các click của các giờ trước nữa đã xử lý không cần xử lý lại
            GetCampaignIdAndGroupIdInLstClickProcess(processClicks);

            //Lấy lịch sử thay đổi lịch biểu của chiến dịch và nhóm
            LoadAllTimeSpanChangeHistoryOfCampaignAndGroup(
                startTime,
                endTime,
                _clickProcessCampaignIds,
                _clickProcessAdvertisingSetIds,
                out _dicTimeSpanChangeHistoryCampaign,
                out _dicTimeSpanChangeHistoryAdvGroup);

            //lấy lịch sử thay đổi cấu hình ShareTimeSpan
            LoadShareTimeSpanChangeHistoryOfCampaign(startTime, endTime, _dicShareTimeSpanChangeHistoryOfCampaign);

            var lstFd = (from clickLogMessage in processClicks
                let check = IsFraud(clickLogMessage)
                where check
                select clickLogMessage).ToList();

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();

            foreach (var c in lstFd)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }

            //giải phóng bộ nhớ
            _dicTimeSpanChangeHistoryCampaign = default!;
            _dicTimeSpanChangeHistoryAdvGroup = default!;
            _dicShareTimeSpanChangeHistoryOfCampaign = default!;
            _clickProcessCampaignIds = default!;
            _lstCampaignIdOfCampaignShareTimeSpan = default!;
            _clickProcessAdvertisingSetIds = default!;

            return fraudClickResults;
        }

        //Lấy danh sách CampaignId và AdvertisingSetId trong danh sách click cần xử lý
        private static void GetCampaignIdAndGroupIdInLstClickProcess(List<Tracking> processClicks)
        {
            _clickProcessCampaignIds = processClicks.Select(x => x.CampaignId).Distinct().ToList();
            _clickProcessAdvertisingSetIds = processClicks.Select(x => x.AdvertisingSetId).Distinct().ToList();

            _lstCampaignIdOfCampaignShareTimeSpan = CacheManager.Campaigns
                .Values
                .Where(c => (c.EcommerceScheduled ?? default) && _clickProcessCampaignIds.Contains(c.SubId))
                .Select(t => t.SubId)
                .ToList();

            _clickProcessAdvertisingSetIds = CacheManager.AdvertisingSets.Values
                .Where(c => !_lstCampaignIdOfCampaignShareTimeSpan.Contains(
                                CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == c.CampaignId)?.SubId ??
                                default) &&
                            _clickProcessAdvertisingSetIds.Contains(c.SubId))
                .Select(t => t.SubId)
                .ToList();
        }

        //Lấy lịch sử thay đổi time span của chiến dịch và nhóm trong khoảng startTime, endTime 
        private static void LoadTimeSpanChangeHistoryOfCampaignAndGroup(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            Dictionary<long, List<TimeSpanChangeHistoryOfCampaign>> dicTimeSpanChangeHistoryCampaign,
            Dictionary<long, List<TimeSpanChangeHistoryOfAdvertisingSet>> dicTimeSpanChangeHistoryAdvGroup)
        {
            var lstTimeSpanChangeHistoryCampaign =
                new Dictionary<long, Dictionary<string, TimeSpanChangeHistoryOfCampaign>>();
            var lstTimeSpanChangeHistoryAdvGroup =
                new Dictionary<long, Dictionary<string, TimeSpanChangeHistoryOfAdvertisingSet>>();
            var lstLogSessionInDay = CacheManager.LogSessions.Values
                .Where(t => t.ModifiedType == 3 && t.Time >= startTime && t.Time <= endTime &&
                            (t.TableName == "Service.Settings.CampaignScheduling" ||
                             t.TableName == "Service.Settings.AdvertisingSetScheduling"))
                .ToList();

            #region tổng hợp lịch sử thay đổi của chiến dịch

            var lstLogSessionOfCampaign = lstLogSessionInDay
                .Where(t => t.TableName == "Service.Settings.CampaignScheduling")
                .OrderBy(t => t.Time)
                .ToList();

            foreach (var logSessionCampaign in lstLogSessionOfCampaign)
            {
                var timespan = $"{{{logSessionCampaign.OldObject}}}".Deserialize<CampaignSchedulingCore>();
                var timespanCampaignId = timespan.CampaignCore?.SubId ?? default;

                if (!lstTimeSpanChangeHistoryCampaign.ContainsKey(timespanCampaignId))
                {
                    lstTimeSpanChangeHistoryCampaign.Add(timespanCampaignId,
                        new Dictionary<string, TimeSpanChangeHistoryOfCampaign>());
                }

                if (!lstTimeSpanChangeHistoryCampaign[timespanCampaignId]
                        .ContainsKey(logSessionCampaign.SessionHashCode))
                {
                    lstTimeSpanChangeHistoryCampaign[timespanCampaignId].Add(logSessionCampaign.SessionHashCode,
                        new TimeSpanChangeHistoryOfCampaign());
                }

                lstTimeSpanChangeHistoryCampaign[timespanCampaignId][logSessionCampaign.SessionHashCode]
                    .LstCampaignTimeSpan.Add(timespan);
                lstTimeSpanChangeHistoryCampaign[timespanCampaignId][logSessionCampaign.SessionHashCode].ChangeTime =
                    logSessionCampaign.Time ?? DateTimeOffset.UtcNow;
            }


            foreach (var campaign in lstTimeSpanChangeHistoryCampaign)
            {
                if (!dicTimeSpanChangeHistoryCampaign.ContainsKey(campaign.Key))
                {
                    dicTimeSpanChangeHistoryCampaign.Add(campaign.Key, new List<TimeSpanChangeHistoryOfCampaign>());
                }

                var campaignChangedTimeSpans = campaign.Value.Values
                    .OrderBy(t => t.ChangeTime)
                    .ToList();

                //tính mốc bắt đầu của khoảng
                for (var i = 0; i < campaignChangedTimeSpans.Count; i++)
                {
                    var previousIndex = i - 1;
                    var start = previousIndex >= 0
                        ? campaignChangedTimeSpans[previousIndex].ChangeTime
                        : new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                    campaignChangedTimeSpans[i].StartTime = start;
                }

                dicTimeSpanChangeHistoryCampaign[campaign.Key].AddRange(campaignChangedTimeSpans);
            }

            #endregion

            #region tổng hợp lịch sử thay đổi của nhóm

            var lstLogSessionOfAdvGroup = lstLogSessionInDay
                .Where(t => t.TableName == "Service.Settings.AdvertisingSetScheduling")
                .OrderBy(t => t.Time)
                .ToList();

            foreach (var logSessionAdSet in lstLogSessionOfAdvGroup)
            {
                var timespan = $"{{{logSessionAdSet.OldObject}}}".Deserialize<AdvertisingSetSchedulingCore>();
                var timespanAdvertisingSetId = timespan.AdvertisingSetCore?.SubId ?? default;

                if (!lstTimeSpanChangeHistoryAdvGroup.ContainsKey(timespanAdvertisingSetId))
                {
                    lstTimeSpanChangeHistoryAdvGroup.Add(timespanAdvertisingSetId,
                        new Dictionary<string, TimeSpanChangeHistoryOfAdvertisingSet>());
                }

                if (!lstTimeSpanChangeHistoryAdvGroup[timespanAdvertisingSetId]
                        .ContainsKey(logSessionAdSet.SessionHashCode))
                {
                    lstTimeSpanChangeHistoryAdvGroup[timespanAdvertisingSetId].Add(logSessionAdSet.SessionHashCode,
                        new TimeSpanChangeHistoryOfAdvertisingSet());
                }

                lstTimeSpanChangeHistoryAdvGroup[timespanAdvertisingSetId][logSessionAdSet.SessionHashCode]
                    .LstAdvGroupTimeSpan.Add(timespan);
                lstTimeSpanChangeHistoryAdvGroup[timespanAdvertisingSetId][logSessionAdSet.SessionHashCode].ChangeTime =
                    logSessionAdSet.Time ?? DateTimeOffset.UtcNow;
            }

            foreach (var advertisingSet in lstTimeSpanChangeHistoryAdvGroup)
            {
                if (!dicTimeSpanChangeHistoryAdvGroup.ContainsKey(advertisingSet.Key))
                {
                    dicTimeSpanChangeHistoryAdvGroup.Add(advertisingSet.Key,
                        new List<TimeSpanChangeHistoryOfAdvertisingSet>());
                }

                var advertisingSetChangedTimeSpans = advertisingSet.Value.Values
                    .OrderBy(t => t.ChangeTime)
                    .ToList();

                for (var i = 0; i < advertisingSetChangedTimeSpans.Count; i++)
                {
                    var previousIndex = i - 1;
                    var start = previousIndex >= 0
                        ? advertisingSetChangedTimeSpans[previousIndex].ChangeTime
                        : new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                    advertisingSetChangedTimeSpans[i].StartTime = start;
                }

                dicTimeSpanChangeHistoryAdvGroup[advertisingSet.Key].AddRange(advertisingSetChangedTimeSpans);
            }

            #endregion
        }

        //ghép cấu hình hiện tại với lịch sử thay đổi cấu hình
        private static void LoadAllTimeSpanChangeHistoryOfCampaignAndGroup(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            List<long> lstCampaignId,
            List<long> lstAdvGroupId,
            out Dictionary<long, List<TimeSpanChangeHistoryOfCampaign>> dicAllTimeSpanChangeHistoryCampaign,
            out Dictionary<long, List<TimeSpanChangeHistoryOfAdvertisingSet>> dicAllTimeSpanChangeHistoryAdvertisingSet)
        {
            //lấy lịch biểu hiện tại
            var dicCurrentCampaignTimeSpan = CacheManager.Campaigns
                .Where(x => lstCampaignId.Contains(x.Key))
                .ToDictionary(i => i.Key, i => i.Value.SchedulingCore);

            var dicCurrentAdvertisingSetTimeSpan = CacheManager.AdvertisingSets
                .Where(x => lstAdvGroupId.Contains(x.Key))
                .ToDictionary(i => i.Key, i => i.Value.SchedulingCore);

            //Lấy lịch sử các lần thay đổi lịch biểu
            var dicTimeSpanChangeHistoryCampaign = new Dictionary<long, List<TimeSpanChangeHistoryOfCampaign>>();
            var dicTimeSpanChangeHistoryAdvertisingSet =
                new Dictionary<long, List<TimeSpanChangeHistoryOfAdvertisingSet>>();
            LoadTimeSpanChangeHistoryOfCampaignAndGroup(startTime, endTime, dicTimeSpanChangeHistoryCampaign,
                dicTimeSpanChangeHistoryAdvertisingSet);

            //ghép lịch sử thay đổi với cấu hình hiện tại

            #region chiến dịch

            //- duyệt cấu hình hiện tại
            foreach (var campaign in dicCurrentCampaignTimeSpan)
            {
                //- Nếu chiến dịch không có lần thay đổi cấu hình nào thì thêm chiến dịch đó với cấu hình hiện tại, starttime=0h đầu ngày
                //- Nếu chiến dịch thay đổi cấu hình nào thì thêm cấu hình hiện tại với starttime = changeTime của lần cấu hình trước
                if (!dicTimeSpanChangeHistoryCampaign.ContainsKey(campaign.Key))
                {
                    dicTimeSpanChangeHistoryCampaign.Add(campaign.Key, new List<TimeSpanChangeHistoryOfCampaign>());
                }

                var currentTimeSpanOfCampaign = new TimeSpanChangeHistoryOfCampaign
                {
                    ChangeTime = endTime,
                    LstCampaignTimeSpan = campaign.Value ?? new List<CampaignSchedulingCore>()
                };

                var numberChangeOfCampaign = dicTimeSpanChangeHistoryCampaign[campaign.Key].Count;
                if (numberChangeOfCampaign == 0)
                {
                    currentTimeSpanOfCampaign.StartTime =
                        new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                }
                else
                {
                    currentTimeSpanOfCampaign.StartTime =
                        dicTimeSpanChangeHistoryCampaign[campaign.Key][numberChangeOfCampaign - 1].ChangeTime;
                }

                dicTimeSpanChangeHistoryCampaign[campaign.Key].Add(currentTimeSpanOfCampaign);
            }

            #endregion

            #region nhóm

            //- duyệt cấu hình hiện tại
            foreach (var advertisingSet in dicCurrentAdvertisingSetTimeSpan)
            {
                //- Nếu nhóm không có lần thay đổi cấu hình nào thì thêm nhóm đó với cấu hình hiện tại, starttime=0h đầu ngày
                //- Nếu nhóm thay đổi cấu hình nào thì thêm cấu hình hiện tại với starttime = changeTime của lần cấu hình trước
                if (!dicTimeSpanChangeHistoryAdvertisingSet.ContainsKey(advertisingSet.Key))
                {
                    dicTimeSpanChangeHistoryAdvertisingSet.Add(advertisingSet.Key,
                        new List<TimeSpanChangeHistoryOfAdvertisingSet>());
                }

                var currentTimeSpanOfAdvGroup = new TimeSpanChangeHistoryOfAdvertisingSet
                {
                    ChangeTime = endTime,
                    LstAdvGroupTimeSpan = advertisingSet.Value ?? new List<AdvertisingSetSchedulingCore>()
                };

                var numberChangeOfAdvertisingSet = dicTimeSpanChangeHistoryAdvertisingSet[advertisingSet.Key].Count;
                if (numberChangeOfAdvertisingSet == 0)
                {
                    currentTimeSpanOfAdvGroup.StartTime =
                        new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                }
                else
                {
                    currentTimeSpanOfAdvGroup.StartTime =
                        dicTimeSpanChangeHistoryAdvertisingSet[advertisingSet.Key][numberChangeOfAdvertisingSet - 1]
                            .ChangeTime;
                }

                dicTimeSpanChangeHistoryAdvertisingSet[advertisingSet.Key].Add(currentTimeSpanOfAdvGroup);
            }

            #endregion

            dicAllTimeSpanChangeHistoryCampaign = dicTimeSpanChangeHistoryCampaign;
            dicAllTimeSpanChangeHistoryAdvertisingSet = dicTimeSpanChangeHistoryAdvertisingSet;
        }

        //Lấy lịch sử thay đổi Sharetimespan của chiến dịch trong khoảng startTime, endTime 
        private static void LoadShareTimeSpanChangeHistoryOfCampaign(
            DateTimeOffset startTime,
            DateTimeOffset endTime,
            Dictionary<long, List<ShareTimeSpanHistoryOfCampaign>> dicShareTimeSpanChangeHistoryOfCampaign)
        {
            var lstShareTimeSpanChangeHistoryOfCampaign = new Dictionary<long, List<ShareTimeSpanHistoryOfCampaign>>();
            var lstLogSessionInDay = CacheManager.LogSessions
                .Values
                .Where(t => t.ModifiedType == 2 && t.Time >= startTime && t.Time <= endTime &&
                            t.TableName == "Service.Settings.Campaigns" && t.OldObject.Contains("EcommerceScheduling"))
                .OrderBy(T => T.Time)
                .ToList();

            //Lấy danh sách các lần thay đổi ShareTimeSpan của chiến dịch
            foreach (var logSessionCampaign in lstLogSessionInDay)
            {
                var campaign = $"{{{logSessionCampaign.OldObject}}}".Deserialize<CampaignCore>();
                if (!lstShareTimeSpanChangeHistoryOfCampaign.ContainsKey(campaign.SubId))
                {
                    lstShareTimeSpanChangeHistoryOfCampaign.Add(campaign.SubId,
                        new List<ShareTimeSpanHistoryOfCampaign>());
                }

                var shareTimeSpanChange = new ShareTimeSpanHistoryOfCampaign
                {
                    ChangeTime = logSessionCampaign.Time ?? DateTimeOffset.UtcNow,
                    ShareTimeSpan = campaign.EcommerceScheduled ?? default
                };
                lstShareTimeSpanChangeHistoryOfCampaign[campaign.SubId].Add(shareTimeSpanChange);
            }

            //tính mốc startTime của mỗi khoảng
            foreach (var item in lstShareTimeSpanChangeHistoryOfCampaign)
            {
                if (!dicShareTimeSpanChangeHistoryOfCampaign.ContainsKey(item.Key))
                {
                    dicShareTimeSpanChangeHistoryOfCampaign.Add(item.Key, new List<ShareTimeSpanHistoryOfCampaign>());
                }

                var lstChangeShareTimeSpanOfCampaign = item.Value.OrderBy(t => t.ChangeTime).ToList();

                for (var i = 0; i < lstChangeShareTimeSpanOfCampaign.Count; i++)
                {
                    var previousIndex = i - 1;
                    var start = previousIndex >= 0
                        ? lstChangeShareTimeSpanOfCampaign[previousIndex].ChangeTime
                        : new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                    lstChangeShareTimeSpanOfCampaign[i].StartTime = start;
                }

                dicShareTimeSpanChangeHistoryOfCampaign[item.Key].AddRange(lstChangeShareTimeSpanOfCampaign);
            }

            //ghép với cấu hình hiện tại
            foreach (var campaignId in _clickProcessCampaignIds)
            {
                if (!dicShareTimeSpanChangeHistoryOfCampaign.ContainsKey(campaignId))
                {
                    dicShareTimeSpanChangeHistoryOfCampaign.Add(campaignId, new List<ShareTimeSpanHistoryOfCampaign>());
                }

                var numberChange = dicShareTimeSpanChangeHistoryOfCampaign[campaignId].Count;
                var currentShareTimeSpanChange = new ShareTimeSpanHistoryOfCampaign
                {
                    ChangeTime = endTime,
                    ShareTimeSpan = _lstCampaignIdOfCampaignShareTimeSpan.Contains(campaignId),
                    StartTime = numberChange == 0
                        ? new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0)
                        : dicShareTimeSpanChangeHistoryOfCampaign[campaignId][numberChange - 1].ChangeTime
                };

                dicShareTimeSpanChangeHistoryOfCampaign[campaignId].Add(currentShareTimeSpanChange);
            }
        }

        private static bool IsFraud(Tracking click)
        {
            //check click thuộc chiến dịch nào
            if (!_dicShareTimeSpanChangeHistoryOfCampaign.ContainsKey(click.CampaignId)) return true;
            //tìm xem click thuộc khoảng ShareTimeSpan nào
            foreach (var historyShareTimeSpan in _dicShareTimeSpanChangeHistoryOfCampaign[click.CampaignId])
            {
                if (click.CreatedOn > historyShareTimeSpan.StartTime &&
                    click.CreatedOn.AddMinutes(-10) <= historyShareTimeSpan.ChangeTime)
                {
                    //Nếu trong khoảng sharetimespan chiến dịch cấu hình dùng chung lịch biểu thì
                    //- Check trong lịch sử thay đổi lịch biểu của chiến dich
                    //- ngược lại Check trong lịch sử thay đổi lịch biểu của nhóm
                    return historyShareTimeSpan.ShareTimeSpan
                        ? IsFraudByTimeSpanOfCampaign(click)
                        : IsFraudByTimeSpanOfAdvGroup(click);
                }
            }

            return true;
        }

        private static bool IsFraudByTimeSpanOfCampaign(Tracking click)
        {
            if (!_dicTimeSpanChangeHistoryCampaign.ContainsKey(click.CampaignId)) return true;
            var campaignChangeTimeSpans = _dicTimeSpanChangeHistoryCampaign[click.CampaignId];
            //duyệt danh sách các lần thay đổi timespan của chiến dịch
            foreach (var changeTimeSpan in campaignChangeTimeSpans)
            {
                //nếu click thuộc khoảng thay đổi nào thì check cấu hình lịch biểu của khoảng đó
                if (click.CreatedOn <= changeTimeSpan.StartTime ||
                    click.CreatedOn.AddMinutes(-10) > changeTimeSpan.ChangeTime) continue;
                var lstTimeSpan = changeTimeSpan.LstCampaignTimeSpan;
                foreach (var timespan in lstTimeSpan)
                {
                    var clickOn = click.CreatedOn;
                    if (click.CreatedOn.DayOfWeek == timespan.DayOfWeek &&
                        click.CreatedOn >= new DateTime(clickOn.Year, clickOn.Month, clickOn.Day,
                            timespan.Timing.Hour, 0, 0, 0) &&
                        click.CreatedOn.AddMinutes(-10) <= new DateTime(clickOn.Year, clickOn.Month, clickOn.Day,
                            timespan.Timing.Hour, 0, 0, 0).AddHours(1))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static bool IsFraudByTimeSpanOfAdvGroup(Tracking click)
        {
            if (!_dicTimeSpanChangeHistoryAdvGroup.ContainsKey(click.AdvertisingSetId)) return true;
            var advertisingSetChangeTimeSpans = _dicTimeSpanChangeHistoryAdvGroup[click.AdvertisingSetId];
            //duyệt danh sách các lần thay đổi timespan của nhóm
            foreach (var changeTimeSpan in advertisingSetChangeTimeSpans)
            {
                //nếu click thuộc khoảng thay đổi nào thì check cấu hình lịch biểu của khoảng đó
                if (click.CreatedOn <= changeTimeSpan.StartTime ||
                    click.CreatedOn > changeTimeSpan.ChangeTime.AddMinutes(10)) continue;
                var lstTimeSpan = changeTimeSpan.LstAdvGroupTimeSpan;
                foreach (var timespan in lstTimeSpan)
                {
                    var clickOn = click.CreatedOn;
                    if (click.CreatedOn.DayOfWeek == timespan.DayOfWeek &&
                        click.CreatedOn > new DateTime(clickOn.Year, clickOn.Month, clickOn.Day,
                            timespan.Timing.Hour, 0, 0, 0) &&
                        click.CreatedOn <=
                        new DateTime(clickOn.Year, clickOn.Month, clickOn.Day, timespan.Timing.Hour, 0, 0, 0)
                            .AddHours(1).AddMinutes(10))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}