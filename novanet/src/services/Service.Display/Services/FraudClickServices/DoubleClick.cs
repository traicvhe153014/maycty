﻿using NovanetCore.Business.BusinessControllers;
using NovanetCore.Business.BusinessObjects.FraudClickObjects;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public static class DoubleClick
    {
        /// <summary>
        /// Tư tưởng chính: Tính toán trên dữ liệu của 15 ngày trước đưa ra danh sách các Click bị coi là click ảo với quy tắc: 
        ///     + Đối với các Pub có tỉ lệ: (ClientId có > 2 Click/Tổng số ClientId) >= giá trị cho phép(mặc định 0.2)
        ///             Loại bỏ toàn bộ Click của ClientId có Click ảo (2 click cùng click vào 1 link đích hoặc thời gian giữa 2 click nhỏ hơn 1 giới hạn cho phép. mặc định 300s)
        ///     + Đối với các Pub có tỉ lệ: (ClientId có > 2 Click/Tổng số ClientId) nhỏ hơn giá trị cho phép(mặc định 0.2)
        ///             Giữ lại Click cuối trong các click bị coi là ảo.(2 click cùng click vào 1 link đích hoặc thời gian giữa 2 click nhỏ hơn 1 giới hạn cho phép. mặc định 300s)
        /// </summary>
        /// <param name="processClicks"></param>
        /// <param name="fraudClicks"></param>
        /// <param name="dicDataConfig"></param>
        /// <returns></returns>
        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            var dicPubIdClientIdClick = new Dictionary<long, Dictionary<Guid, List<Tracking>>>();//pubid,clientid, list click: Danh sách Các Click của Từng ClientId của từng Pubid.
            var dicPubIdRateDoubleClick = new Dictionary<long, bool>();//pubid; clientIdDoubleClick/CountClient >= highrate (cac client id co >2 click / tong so clientid)
            
            #region Tổng hợp dữ liệu Click theo PublisherId;ClientId;ListClick.

            foreach (var c in processClicks)
            {
                if (c.ClientId == Guid.Empty) continue;
                if (dicPubIdClientIdClick.ContainsKey(c.PublisherId))
                {
                    if (dicPubIdClientIdClick[c.PublisherId].ContainsKey(c.ClientId))
                    {
                        dicPubIdClientIdClick[c.PublisherId][c.ClientId].Add(c);
                    }
                    else
                    {
                        dicPubIdClientIdClick[c.PublisherId].Add(c.ClientId, new List<Tracking> {c});
                    }
                }
                else
                {
                    var dicClientClick = new Dictionary<Guid, List<Tracking>>
                        {{c.ClientId, new List<Tracking> {c}}};
                    dicPubIdClientIdClick.Add(c.PublisherId, dicClientClick);
                }
            }
            #endregion
            
            foreach (var danhsachClickCuaMoiClientIdCua1Pub in dicPubIdClientIdClick)
            {
                var soClientIdCoHon2Click = danhsachClickCuaMoiClientIdCua1Pub.Value.Where(t => t.Value.Count >= 2).ToList();
                //Tỉ lệ của ClientId có hơn 2 Click / tổng số ClientId. Vd: 10Client có 3 Client có > 2Click => tỉ lệ = 3/10 = 0.3
                var rate = (double)soClientIdCoHon2Click.Count / danhsachClickCuaMoiClientIdCua1Pub.Value.Count;
                var highRate =
                    PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, danhsachClickCuaMoiClientIdCua1Pub.Key, 0, (int) FraudClickRules.DoubleClick,
                        (int) FraudClickKeyConfig.HighRate);
                dicPubIdRateDoubleClick.Add(danhsachClickCuaMoiClientIdCua1Pub.Key,
                    rate >= highRate); //PublisherId: tỉ lệ Double Click.
            }
            
            //lay danh sach cac publisher co ty le double click cao 
            var highRateDoubleClickPublisherIds = dicPubIdRateDoubleClick.Where(t => t.Value).ToList();
            var doubleClicks = new List<Tracking>(); //Danh sách các Click bị coi là Click ảo vì là DoubleClick.

            //Duyệt qua danh sách các PublisherId có Hight_Rate cao => bỏ tất cả các Click của ClientId có Double Click. 
            foreach (var highRateDoubleClickPublisherId in highRateDoubleClickPublisherIds)
            {
                //danh sach cac ClientId có double click cua 1 publisher 
                var lstClientIdCoDoubleClickCua1PubId = GetAllDoubleClickOfPub(dicPubIdClientIdClick[highRateDoubleClickPublisherId.Key], dicDataConfig);
                foreach (var clientIdCoDoubleClickCua1PubId in lstClientIdCoDoubleClickCua1PubId)
                {
                    doubleClicks.AddRange(clientIdCoDoubleClickCua1PubId.Value);
                }
            }
            
            //Danh sách các PublisherId có tỉ lệ Double Click bình thường (dưới HIGH_RATE)
            var normalRateDoubleClickPublisherIds = dicPubIdRateDoubleClick.Where(t => !t.Value).ToList();

            //Duyệt qua danh sách các PublisherId có Hight_Rate bình thường => chỉ giữ lại Click ko vi phạm rule. 
            foreach (var normalRateDoubleClickPublisherId in normalRateDoubleClickPublisherIds)
            {
                //danh sach cac double click cua 1 publisher 
                var groupedDoubleClicksByClientId =
                    GetAllDoubleClickOfPubNormalRate(dicPubIdClientIdClick[normalRateDoubleClickPublisherId.Key],
                        dicDataConfig);
                foreach (var clicks in groupedDoubleClicksByClientId)
                {
                    doubleClicks.AddRange(clicks.Value);
                }
            }

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();
            foreach (var c in doubleClicks)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }
            return fraudClickResults;
        }

        /// <summary>
        /// lay toan bo danh sach cac clientid:lstClicks có Double cua 1 publisher.
        /// </summary>
        /// <param name="allClientIdClicked">ClientId:ListClick. của 1 PublisherId</param>
        /// <param name="dicDataConfig"></param>
        /// <returns></returns>
        private static Dictionary<Guid, List<Tracking>> GetAllDoubleClickOfPubNormalRate(
            Dictionary<Guid, List<Tracking>> allClientIdClicked, 
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            var groupedClicksByClientId = new Dictionary<Guid, List<Tracking>>();
            var dicClientIdHasDoubleClick = allClientIdClicked.Where(t => t.Value.Count >= 2).ToList();
            foreach (var clientIdHasDoubleClick in dicClientIdHasDoubleClick)
            {
                var currentClientIdClicks = clientIdHasDoubleClick.Value.ToList();

                var doubleClicks = GetDoubleClickOfClientId(currentClientIdClicks, dicDataConfig);
                if(doubleClicks.Count>0)
                {
                    groupedClicksByClientId.Add(clientIdHasDoubleClick.Key, doubleClicks);
                }
            }
            return groupedClicksByClientId;
        }

        /// <summary>
        /// lay toan bo danh sach cac clientid co double click cua 1 publisher
        /// </summary>
        /// <param name="allClientIdClicked">ClientId:ListClicks của các PublisherId có tỉ lệ DoubleClick cao.</param>
        /// <param name="dicDataConfig"></param>
        /// <returns>ClientId; ListClick</returns>
        private static Dictionary<Guid, List<Tracking>> GetAllDoubleClickOfPub(
            Dictionary<Guid, List<Tracking>> allClientIdClicked, 
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            var groupedClicksByClientId = new Dictionary<Guid, List<Tracking>>();
            var dicClientIdHasDoubleClick = allClientIdClicked.Where(t => t.Value.Count >= 2).ToList();
            foreach (var clientIdHasDoubleClick in dicClientIdHasDoubleClick)
            {
                var currentClientIdClicks = clientIdHasDoubleClick.Value.ToList();

                //clientid co double click
                if (CheckClientIdDoubleClick(currentClientIdClicks, dicDataConfig))
                {
                    groupedClicksByClientId.Add(clientIdHasDoubleClick.Key, clientIdHasDoubleClick.Value);
                }
            }
            return groupedClicksByClientId;
        }

        /// <summary>
        /// kiem tra xem trong so cac click cua 1 client id co hay khong viec double click
        /// </summary>
        /// <param name="clicks"></param>
        /// <param name="dicDataConfig"></param>
        /// <returns></returns>
        private static bool CheckClientIdDoubleClick(
            List<Tracking> clicks, 
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            clicks = clicks.OrderByDescending(t => t.CreatedOn).ToList();

            //set by time 
            for (var i = 0; i < clicks.Count - 1; i++)
            {
                var ts = clicks[i].CreatedOn - clicks[i + 1].CreatedOn;
                var timeSetDoubleClick =
                    PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, clicks[i].PublisherId, clicks[i].AdvertiserId, (int)FraudClickRules.DoubleClick,
                        (int) FraudClickKeyConfig.TimeSetDoubleClick);

                if (ts.TotalSeconds < timeSetDoubleClick)
                {
                    return true;
                }
            }

            //set by target url
            clicks = clicks.OrderByDescending(t => t.TargetUrl).ToList();
            for (var i = 0; i < clicks.Count - 1; i++)
            {
                if (string.Equals(clicks[i].TargetUrl?.Trim().ToLower(), clicks[i + 1].TargetUrl?.Trim().ToLower(),
                        StringComparison.Ordinal))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Lấy ra Danh sách các Click (Trừ click cuối cùng) bị coi là DoubleCLick của 1 Clientid vì lý do vi phạm luật: 
        ///     + Thời gian giữa 2 lần Click của 1 ClientId nhỏ hơn 1 giới hạn cho phép.
        ///     + 2 Click của 1 ClientId cùng Click vào 1 Link đích.
        /// </summary>
        /// <param name="clicks"></param>
        /// <param name="dicDataConfig"></param>
        /// <returns></returns>
        private static List<Tracking> GetDoubleClickOfClientId(
            List<Tracking> clicks,
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            //Danh sách Click bị coi là Click ảo. ClickId: Click.
            var doubleClickResults = new Dictionary<Guid, Tracking>();

            clicks = clicks.OrderByDescending(t => t.CreatedOn).ToList();
            //set by time
            for (int i = 0; i < clicks.Count - 1; i++)
            {
                var timeBetween2Click = clicks[i].CreatedOn - clicks[i + 1].CreatedOn;

                var timeSetDoubleClick =
                    PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                        dicDataConfig, clicks[i].PublisherId, clicks[i].AdvertiserId, (int)FraudClickRules.DoubleClick,
                        (int)FraudClickKeyConfig.TimeSetDoubleClick);

                if (timeBetween2Click.TotalSeconds <= timeSetDoubleClick)
                {
                    if (!doubleClickResults.ContainsKey(clicks[i].Id))
                    {
                        doubleClickResults.Add(clicks[i].Id, clicks[i]);
                    }
                }
            }

            //set by target url
            clicks = clicks.OrderByDescending(t => t.TargetUrl).ToList();
            for (var i = 0; i < clicks.Count - 1; i++)
            {
                if (string.Equals(clicks[i].TargetUrl?.Trim().ToLower(), clicks[i + 1].TargetUrl?.Trim().ToLower(),
                        StringComparison.Ordinal))
                {
                    if (!doubleClickResults.ContainsKey(clicks[i].Id))
                    {
                        doubleClickResults.Add(clicks[i].Id, clicks[i]);
                    }
                }
            }
            return doubleClickResults.Values.ToList();
        }
    }
}
