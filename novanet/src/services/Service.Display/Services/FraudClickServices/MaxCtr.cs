﻿using NovanetCore.Business.BusinessObjects.FraudClickObjects;
using NovanetCore.Business.BusinessUtils;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public static class MaxCtr
    {
        private const double MaxCTR = 0.1;
        private const double MinCTR = 0.85;

        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            List<Guid> processedFraudClickIds)
        {
            //loại bỏ các click đã bị coi là click ảo + bị lọc trong các giờ trước
            processClicks = processClicks.Where(p =>
                !fraudClicks.ContainsKey(p.Id) && !p.IsFraud && !processedFraudClickIds.Contains(p.Id)).ToList();

            //chỉ cắt click các site hạng B,C
            var websitePrices = CacheManager.WebsitePrices
                .Values
                .Where(p => p.StartDate <= DateTimeOffset.Now)
                .OrderByDescending(q => q.StartDate)
                .GroupBy(p => p.WebsiteId)
                .Select(g => g.First())
                .ToList();

            var processWebsiteIds = websitePrices
                .Select(p => p.WebsiteCore?.SubId ?? default)
                .ToList();
            processClicks = processClicks.Where(t => processWebsiteIds.Contains(t.WebsiteId)).ToList();

            //Lấy danh sách CampaignId có click
            var lstCampaignIdInLstClickProcess = processClicks.Select(t => t.CampaignId).Distinct().ToList();

            //tính CTR của mỗi domain trên từng chiến dịch
            //+ Nếu ctr > MaxCTR thì cắt click
            var fraudClickList = new List<Tracking>();

            //TODO: set value for lstReport
            var reportItems = new List<ReportItem>();
            foreach (var domainReport in reportItems)
            {
                var websiteId = domainReport.WEBSITE_ID;
                var campaignId = domainReport.CAMPAIGN_ID;
                var lstClickOfDomainByCampaign = processClicks
                    .Where(t => t.WebsiteId == websiteId && t.CampaignId == campaignId).ToList();
                var numberClickOfDomainByCampaign = lstClickOfDomainByCampaign.Count;

                var ctr = domainReport.ACTIVEVIEW != 0
                    ? (float) (numberClickOfDomainByCampaign * 100) / domainReport.ACTIVEVIEW
                    : 0;

                if (numberClickOfDomainByCampaign <= 5 || (!(ctr > MaxCTR))) continue;
                {
                    var ctrStandard = MathUtils.GetRandomNumber(MinCTR, MaxCTR);
                    var numberClickValid = (int) (domainReport.ACTIVEVIEW * ctrStandard) / 100;

                    var clickStandard =
                        lstClickOfDomainByCampaign.OrderByDescending(p => (p.CreatedOn - p.ViewTime).TotalSeconds)
                            .Take(numberClickValid)
                            .LastOrDefault();

                    if (clickStandard == null) continue;
                    var timeStandard = (clickStandard.CreatedOn - clickStandard.ViewTime).TotalSeconds;
                    var lstFdClickOfDomainByCampaign = lstClickOfDomainByCampaign.Where(
                        t => (t.CreatedOn - t.ViewTime).TotalSeconds < timeStandard).ToList();

                    if (lstFdClickOfDomainByCampaign.Count > 0)
                    {
                        fraudClickList.AddRange(lstFdClickOfDomainByCampaign);
                    }
                }
            }

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();
            foreach (var c in fraudClickList)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }

            return fraudClickResults;
        }
    }
}