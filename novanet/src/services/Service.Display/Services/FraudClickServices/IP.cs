﻿using NovanetCore.Business.BusinessControllers;
using NovanetCore.Business.BusinessObjects.FraudClickObjects;

namespace Service.Display.Services.FraudClickServices;

public partial class FraudClickServices
{
    public static class Ip
    {
        private class ClickAndUrlByWebAndIp
        {
            public ClickAndUrlByWebAndIp()
            {
                TotalClick = 0;
                ListClientUrl = new HashSet<string>();
            }

            public int TotalClick { get; set; }

            public HashSet<string> ListClientUrl { get; set; }

            public bool IsFraudClick
            {
                get
                {
                    var clickUrlRate = ListClientUrl.Count / (double) TotalClick;
                    var maxClickUrlRate =
                        PublisherAdvertiserRulesConfigController.GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
                            _dicDataConfig, 0, 0, (int) FraudClickRules.Ips, (int) FraudClickKeyConfig.ClickUrlRate);
                    return clickUrlRate < maxClickUrlRate;
                }
            }
        }

        /// <summary>
        /// Phân nhóm tạo từ điển click và url theo WebsiteId và IP
        /// </summary>
        private static Dictionary<long, Dictionary<long, ClickAndUrlByWebAndIp>> GroupSourceClickData(
            List<Tracking> lstAllSourceClick)
        {
            var dicClickAndUrlByWebAndIp = new Dictionary<long, Dictionary<long, ClickAndUrlByWebAndIp>>();

            if (lstAllSourceClick.Count <= 0) return dicClickAndUrlByWebAndIp;
            foreach (var logClickDto in lstAllSourceClick)
            {
                if (!dicClickAndUrlByWebAndIp.ContainsKey(logClickDto.WebsiteId))
                {
                    dicClickAndUrlByWebAndIp.Add(logClickDto.WebsiteId, new Dictionary<long, ClickAndUrlByWebAndIp>());
                }

                if (!dicClickAndUrlByWebAndIp[logClickDto.WebsiteId].ContainsKey(logClickDto.Ip))
                {
                    dicClickAndUrlByWebAndIp[logClickDto.WebsiteId].Add(logClickDto.Ip, new ClickAndUrlByWebAndIp());
                }

                dicClickAndUrlByWebAndIp[logClickDto.WebsiteId][logClickDto.Ip].TotalClick += 1;

                if (!dicClickAndUrlByWebAndIp[logClickDto.WebsiteId][logClickDto.Ip].ListClientUrl
                        .Contains(logClickDto.ClientUrl))
                {
                    dicClickAndUrlByWebAndIp[logClickDto.WebsiteId][logClickDto.Ip].ListClientUrl
                        .Add(logClickDto.ClientUrl);
                }
            }

            return dicClickAndUrlByWebAndIp;
        }

        /// <summary>
        /// lọc ra các IP có click ảo 
        /// </summary>
        /// <param name="dicClickAndUrlByWebAndIp"></param>
        /// <returns></returns>
        private static Dictionary<long, HashSet<long>> FilterFdClick(
            Dictionary<long, Dictionary<long, ClickAndUrlByWebAndIp>> dicClickAndUrlByWebAndIp)
        {
            var dicIpBlackListByWebsite = new Dictionary<long, HashSet<long>>();
            foreach (var clickAndUrlByWebsiteAndIp in dicClickAndUrlByWebAndIp)
            {
                foreach (var clickAndUrlByIp in clickAndUrlByWebsiteAndIp.Value.Where(clickAndUrlByIp =>
                             clickAndUrlByIp.Value.IsFraudClick))
                {
                    if (!dicIpBlackListByWebsite.ContainsKey(clickAndUrlByWebsiteAndIp.Key))
                    {
                        dicIpBlackListByWebsite.Add(clickAndUrlByWebsiteAndIp.Key, new HashSet<long>());
                    }

                    if (!dicIpBlackListByWebsite[clickAndUrlByWebsiteAndIp.Key].Contains(clickAndUrlByIp.Key))
                    {
                        dicIpBlackListByWebsite[clickAndUrlByWebsiteAndIp.Key].Add(clickAndUrlByIp.Key);
                    }
                }
            }

            return dicIpBlackListByWebsite;
        }

        private static Dictionary<string, List<FraudClickKeyConfigValue>> _dicDataConfig = default!;

        //Tư tưởng chính: Tổng hợp dữ liệu trong 15 ngày gần nhất đưa ra danh sách các Ip có Click ảo của từng Website từ ngày đến ngày với quy tắc: 1IP không có quá 2 Click/ 1 Url.
        // Duyệt qua các website đưa ra danh sách các Click ảo : Click có Ip và WebsiteId trùng với danh sách click ảo đã tổng hợp bên trên.
        public static Dictionary<Guid, Tracking> Process(
            List<Tracking> sourceClicks,
            List<Tracking> processClicks,
            Dictionary<Guid, Tracking> fraudClicks,
            Dictionary<string, List<FraudClickKeyConfigValue>> dicDataConfig)
        {
            //Nạp Config:
            _dicDataConfig = dicDataConfig;

            //1 - Phân nhóm dữ liệu SourceClick theo WebsiteId và IP
            var dicClickAndUrlByWebAndIp = GroupSourceClickData(sourceClicks);

            //2 - lọc click ảo theo điều kiện url/số click nhỏ hơn 0.3
            var lstSourceFdClick = FilterFdClick(dicClickAndUrlByWebAndIp);

            //3 - duyệt lstClickProcess: nếu click có webId trong danh sách lọc + chưa check là click ảo do bộ realtime + IP nằm trong danh sách lọc
            var fraudClickList = new List<Tracking>();
            foreach (var click in processClicks)
            {
                var isFraud = lstSourceFdClick.ContainsKey(click.WebsiteId) && click.IsFraud == false &&
                               lstSourceFdClick[click.WebsiteId].Contains(click.Ip);

                //hard code IP này do Pass qua rule IP=> hỏi lại a Thành rule này
                var isFixIp = click.Ip == 1934295165 && click.CreatedOn >= new DateTime(2016, 11, 29, 0, 15, 0, 0) &&
                              click.IsFraud == false;
                if (isFraud || isFixIp)
                {
                    fraudClickList.Add(click);
                }
            }

            //Thuc hien loc Click
            var fraudClickResults = new Dictionary<Guid, Tracking>();

            foreach (var c in fraudClickList)
            {
                //Chua dc xet la click ao
                if (!fraudClicks.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClicks.Add(c.Id, c);
                }

                if (!fraudClickResults.ContainsKey(c.Id) && !c.IsFraud)
                {
                    fraudClickResults.Add(c.Id, c);
                }
            }

            return fraudClickResults;
        }
    }
}