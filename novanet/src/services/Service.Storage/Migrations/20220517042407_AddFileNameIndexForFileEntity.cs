﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Storage.Migrations
{
    public partial class AddFileNameIndexForFileEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FileName",
                schema: "Service.Storage",
                table: "Files",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateIndex(
                name: "IX_Files_FileName",
                schema: "Service.Storage",
                table: "Files",
                column: "FileName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Files_FileName",
                schema: "Service.Storage",
                table: "Files");

            migrationBuilder.AlterColumn<string>(
                name: "FileName",
                schema: "Service.Storage",
                table: "Files",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");
        }
    }
}
