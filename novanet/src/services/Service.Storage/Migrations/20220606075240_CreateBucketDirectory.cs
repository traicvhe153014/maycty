﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Service.Storage.Migrations
{
    public partial class CreateBucketDirectory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Path",
                schema: "Service.Storage",
                table: "Files",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Bucket",
                schema: "Service.Storage",
                table: "Files",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Files_Bucket",
                schema: "Service.Storage",
                table: "Files",
                column: "Bucket");

            migrationBuilder.CreateIndex(
                name: "IX_Files_Path",
                schema: "Service.Storage",
                table: "Files",
                column: "Path");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Files_Bucket",
                schema: "Service.Storage",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_Path",
                schema: "Service.Storage",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "Bucket",
                schema: "Service.Storage",
                table: "Files");

            migrationBuilder.AlterColumn<string>(
                name: "Path",
                schema: "Service.Storage",
                table: "Files",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);
        }
    }
}
