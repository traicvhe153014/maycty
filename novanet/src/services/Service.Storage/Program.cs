using Service.Storage.Events;

WebApplication.CreateBuilder(args).NovanetCore<StorageContext>(StorageContext.Schema,
    out var app, (services, configuration) =>
    {
        services.AddGrpc();
        services.AddRabbitMQ(configuration.RabbitMQ);
        services.AddScoped<IStorageService, StorageService>();
        services.AddHostedService<CreateLongtimeBackupFileService>();
    }, application =>
    {
        application.MapGet("/", () => $"{StorageContext.Schema} - Hello World!");
        application.MigrateDatabase<StorageContext>((context, services) =>
        {
            var logger = services.GetService<ILogger<StorageContextSeed>>();
            StorageContextSeed.SeedAsync(context, logger, services).Wait();
        });
        var appSettings = application.Services.GetRequiredService<AppSettings>();
        appSettings.Kestrel.Endpoints.TryGetValue("gRPC", out var kestrelConfiguration);
        var grpcUrl = kestrelConfiguration?.Url ?? string.Empty;
        var grpcUri = new Uri(grpcUrl);
        application.UseWhen(context => context.Connection.LocalPort == grpcUri.Port, builder =>
        {
            builder.UseRouting();
            builder.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GrpcStreamingService>();
            });
        });
    });

app.Run();