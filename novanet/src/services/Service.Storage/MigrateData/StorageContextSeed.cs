﻿using Microsoft.Data.SqlClient;
using Polly;
using Polly.Retry;
using Service.Storage.Parallels;

namespace Service.Storage.MigrateData;

public class StorageContextSeed
{
    public static async Task SeedAsync(StorageContext context, ILogger<StorageContextSeed>? logger, IServiceProvider services)
    {
        var policy = CreatePolicy(logger, nameof(StorageContextSeed));
        
        await policy.ExecuteAsync(async () =>
        {
            if (!await context.Buckets.AnyAsync())
            {
                // TODO: Migrate buckets here
            }
            // load zip file content when startup
            var publisher = services.GetRequiredService<Publisher>();
            await publisher.Publish(new InitializeArchiveContentsParallel(), PublishStrategy.ParallelNoWait);
        });
    }
    
    private static AsyncRetryPolicy CreatePolicy(ILogger? logger, string prefix, int retries = 3)
    {
        return Policy.Handle<SqlException>().
            WaitAndRetryAsync(
                retryCount: retries,
                sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                onRetry: (exception, timeSpan, retry, ctx) =>
                {
                    logger?.LogWarning(exception,
                        "[{Prefix}] Exception {ExceptionType} with message {Message} detected on attempt {Retry} of {Retries}",
                        prefix, exception.GetType().Name, exception.Message, retry, retries);
                }
            );
    }
}