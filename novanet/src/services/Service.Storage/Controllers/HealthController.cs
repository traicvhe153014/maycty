namespace Service.Storage.Controllers;

[ApiController]
[Route("health")]
public class HealthController : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> Health()
    {
        return Ok(await Task.FromResult("OK"));
    }
}