using Novanet.Core.Constants;
using Service.Storage.Application.Queries;

namespace Service.Storage.Controllers;

[Route("storage/api/v1/[action]")]
public class StorageController : NovanetController
{
    private readonly AppSettings _appSettings;

    public StorageController(AppSettings appSettings)
    {
        _appSettings = appSettings;
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.Bucket, nameof(Create))]
    public async Task<IActionResult> Create(CreateBucketCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpPut]
    [NovanetAccessControl(Aggregates.Bucket, nameof(Update))]
    public async Task<IActionResult> Update(UpdateBucketCommand command)
    {
        return Ok(await Mediator.Send(command));
    }

    [HttpDelete]
    [NovanetAccessControl(Aggregates.Bucket, nameof(Delete))]
    public async Task<IActionResult> Delete(Guid id)
    {
        return Ok(await Mediator.Send(new DeleteBucketCommand(id)));
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Upload(IFormFile file)
    {
        return Ok(await Mediator.Send(new UploadFileCommand(file)));
    }

    [HttpPost]
    [NovanetAccessControl(Aggregates.File, nameof(Upload))]
    public async Task<IActionResult> UploadMultiple(List<IFormFile> files)
    {
        return Ok(await Mediator.Send(new UploadFilesCommand(files)));
    }

    [HttpDelete]
    [NovanetAccessControl(Aggregates.File, "Delete")]
    public async Task<IActionResult> DeleteFile(Guid id)
    {
        return Ok(await Mediator.Send(new DeleteFileCommand(id)));
    }

    [AllowAnonymous]
    [HttpGet("{dimension:range(10,500)=50}/{text?}")]
    public async Task<IActionResult> Avatar(int dimension, string text)
    {
        var response = await Mediator.Send(new GenerateRandomAvatarQuery(dimension, text));
        return File(response.Data, "image/png");
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Stream(string name)
    {
        var response = await Mediator.Send(new StreamFileQuery(name));
        return PhysicalFile(
            Path.Combine(_appSettings.StorageSettings.Saved, response.Data.FilePath.Split("\\").JoinString("/")),
            response.Data.ContentType);
    }
    
    [HttpGet("{videoId}/{fileName}")]
    [AllowAnonymous]
    public async Task<IActionResult> Video(string videoId, string fileName)
    {
        var response = await Mediator.Send(new StreamVideoQuery(videoId, fileName));
        return PhysicalFile(
            Path.Combine(_appSettings.StorageSettings.Saved, response.Data.FilePath.Split("\\").JoinString("/")),
            response.Data.ContentType);
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> UploadZip(IFormFile file)
    {
        return Ok(await Mediator.Send(new UploadZipCommand(file)));
    }
}