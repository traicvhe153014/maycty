﻿using Novanet.Core.Mapper;

namespace Service.Storage.Application.Queries;

public class ListBucketQuery : INovanetRequest<List<BucketResponse>>
{
    public int Page { get; set; }

    public int PageSize { get; set; }

    public ListBucketQuery(int page, int pageSize)
    {
        Page = page;
        PageSize = pageSize;
    }

    internal class Handler : NovanetRequestHandler<ListBucketQuery, List<BucketResponse>>
    {
        private readonly StorageContext _context;

        public Handler(
            ILogger<Handler> logger,
            StorageContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<List<BucketResponse>> HandleAsync(ListBucketQuery request,
            CancellationToken cancellationToken)
        {
            var entities = await _context.Buckets.Where(x => true)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize).ToListAsync(cancellationToken);
            return entities.MapTo<List<BucketResponse>>();
        }
    }
}