﻿using Novanet.Core.Mapper;

namespace Service.Storage.Application.Queries;

public class GetBucketQuery : INovanetRequest<GetBucketResponse>
{
    public Guid Id { get; set; }

    public GetBucketQuery(Guid id)
    {
        Id = id;
    }

    internal class Handler : NovanetRequestHandler<GetBucketQuery, GetBucketResponse>
    {
        private readonly StorageContext _context;

        public Handler(
            ILogger<Handler> logger,
            StorageContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<GetBucketResponse> HandleAsync(GetBucketQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _context.Buckets.FirstOrDefaultAsync(x => x.Id.Equals(request.Id),
                cancellationToken: cancellationToken);
            return entity.MapTo<GetBucketResponse>();
        }
    }
}