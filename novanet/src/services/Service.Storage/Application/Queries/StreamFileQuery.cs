﻿using Microsoft.Extensions.Caching.Memory;
using Novanet.Core.Constants;

namespace Service.Storage.Application.Queries;

public class StreamFileQuery : INovanetRequest<StreamFileResponse>
{
    public string ObjectName { get; set; }

    public StreamFileQuery(string objectName)
    {
        ObjectName = objectName;
    }

    internal class Handler : NovanetRequestHandler<StreamFileQuery, StreamFileResponse>
    {
        private readonly StorageContext _storageContext;
        private readonly IMemoryCache _memoryCache;
        private readonly AppSettings _appSettings;

        public Handler(ILogger<Handler> logger,
            StorageContext storageContext,
            IHttpContextAccessor httpContextAccessor,
            IMemoryCache memoryCache, AppSettings appSettings) : base(logger, httpContextAccessor)
        {
            _storageContext = storageContext;
            _memoryCache = memoryCache;
            _appSettings = appSettings;
        }

        /// <summary>
        /// Cache file
        /// Key: FileName (MD5)
        /// Value: {FilePath}|{ContentType}
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        protected override async Task<StreamFileResponse> HandleAsync(StreamFileQuery request,
            CancellationToken cancellationToken)
        {
            var cached = _memoryCache.Get<string>(request.ObjectName.CreateMd5());
            if (cached != null)
            {
                Logger.LogInformation("{Time} {Path}", DateTimeOffset.Now, cached.Split("|").First());
                return new StreamFileResponse
                {
                    FilePath = cached.Split("|").First(),
                    ContentType = cached.Split("|").Last(),
                };
            }
            var file = await _storageContext.Files.AsNoTracking()
                .FirstOrDefaultAsync(x => x.FileName.Equals(request.ObjectName),
                    cancellationToken: cancellationToken);
            if (file == null) throw new ApplicationException(ErrorMessageValidate.FileNotFound);

            var fileBucket = !string.IsNullOrWhiteSpace(file.Bucket)
                ? file.Bucket
                : _appSettings.StorageSettings.Bucket;
            var filePath = Path.Combine(fileBucket, file.Path);
            Logger.LogInformation("{Time} {Path}",
                DateTimeOffset.Now,
                filePath);
            _memoryCache.Set(request.ObjectName.CreateMd5(),
                $"{filePath}|{file.ContentType}",
                TimeSpan.FromHours(1));
            return new StreamFileResponse
            {
                FilePath = filePath,
                ContentType = file.ContentType!,
            };
        }
    }
}