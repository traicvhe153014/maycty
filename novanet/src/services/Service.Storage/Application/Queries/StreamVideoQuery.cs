﻿using System.Text.RegularExpressions;
using Microsoft.Extensions.Caching.Memory;
using Novanet.Core.Constants;

namespace Service.Storage.Application.Queries;

public class StreamVideoQuery : INovanetRequest<StreamFileResponse>
{
    public string VideoId { get; set; }

    public string FileName { get; set; }

    public StreamVideoQuery(string videoId, string fileName)
    {
        VideoId = videoId;
        FileName = fileName;
    }

    internal class Handler : NovanetRequestHandler<StreamVideoQuery, StreamFileResponse>
    {
        private readonly StorageContext _storageContext;
        private readonly IMemoryCache _memoryCache;
        private readonly AppSettings _appSettings;

        public Handler(ILogger<Handler> logger,
            StorageContext storageContext,
            IHttpContextAccessor httpContextAccessor,
            IMemoryCache memoryCache, AppSettings appSettings) : base(logger, httpContextAccessor)
        {
            _storageContext = storageContext;
            _memoryCache = memoryCache;
            _appSettings = appSettings;
        }

        /// <summary>
        /// Cache file
        /// Key: FileName (MD5)
        /// Value: {FilePath}|{ContentType}
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        protected override async Task<StreamFileResponse> HandleAsync(StreamVideoQuery request,
            CancellationToken cancellationToken)
        {
            const string fileNamePattern = @"(\d+)p(_\d+)?.(\w+)";
            var matches = Regex.Matches(request.FileName, fileNamePattern);
            if (matches.Count == 0)
            {
                throw new BadRequestException("Tên video không hợp lệ");
            }
            var match = matches.FirstOrDefault()!;
            var videoSize = int.TryParse(match.Groups[1].Value, out var tempVideoSize) ? tempVideoSize : 240;
            var videoPart = match.Groups[2].Value;
            var videoExtension = match.Groups[3].Value;
            var videoContentType = videoExtension == "m3u8" ? "application/x-mpegURL" : "video/mp2t";

            var cached = _memoryCache.Get<string>($"{request.VideoId}|{request.FileName}".CreateMd5());
            if (cached != null)
            {
                Logger.LogInformation("{Time} {Path}", DateTimeOffset.Now, cached.Split("|").First());
                return new StreamFileResponse
                {
                    FilePath = cached.Split("|").First(),
                    ContentType = cached.Split("|").Last(),
                };
            }

            var videoGuidId = request.VideoId.ToGuid();
            var file = await _storageContext.Files.AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id.Equals(videoGuidId),
                    cancellationToken: cancellationToken);
            if (file == null) throw new ApplicationException(ErrorMessageValidate.FileNotFound);

            var fileBucket = !string.IsNullOrWhiteSpace(file.Bucket)
                ? file.Bucket
                : _appSettings.StorageSettings.Bucket;
            var filePath = Path.Combine(fileBucket, string.Format(file.Path, videoSize, videoPart, videoExtension));
            Logger.LogInformation("{Time} {Path}",
                DateTimeOffset.Now,
                filePath);
            _memoryCache.Set($"{request.VideoId}|{request.FileName}".CreateMd5(),
                $"{filePath}|{videoContentType}",
                TimeSpan.FromHours(1));
            return new StreamFileResponse
            {
                FilePath = filePath,
                ContentType = videoContentType
            };
        }
    }
}