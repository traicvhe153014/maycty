﻿using System.Drawing;
using System.Drawing.Imaging;
using Devcorner.NIdenticon;
using Devcorner.NIdenticon.BrushGenerators;

namespace Service.Storage.Application.Queries;

public class GenerateRandomAvatarQuery : INovanetRequest<MemoryStream>
{
    public int Dimension { get; set; }

    public string Text { get; set; }

    public GenerateRandomAvatarQuery(int dimension, string text)
    {
        Dimension = dimension;
        Text = text;
    }

    internal class Handler : NovanetRequestHandler<GenerateRandomAvatarQuery, MemoryStream>
    {
        public Handler(
            ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
        }

        protected override async Task<MemoryStream> HandleAsync(GenerateRandomAvatarQuery request,
            CancellationToken cancellationToken)
        {
            var identiconGenerator = new IdenticonGenerator
            {
                DefaultBrushGenerator =
                    new StaticColorBrushGenerator(StaticColorBrushGenerator.ColorFromText(request.Text)),
                DefaultBackgroundColor = Color.Transparent
            };
            var qrCodeImage = identiconGenerator.Create(request.Text, new Size(request.Dimension, request.Dimension));
            var outputStream = new MemoryStream();
            qrCodeImage.Save(outputStream, ImageFormat.Png);
            outputStream.Seek(0, SeekOrigin.Begin);
            return await Task.FromResult(outputStream);
        }
    }
}