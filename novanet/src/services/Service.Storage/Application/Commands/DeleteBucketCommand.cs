using Novanet.Core.Mapper;

namespace Service.Storage.Application.Commands;

public class DeleteBucketCommand : INovanetRequest<DeleteBucketResponse>
{
    public Guid Id { get; set; }

    public DeleteBucketCommand(Guid id)
    {
        Id = id;
    }

    internal class Handler : NovanetRequestHandler<DeleteBucketCommand, DeleteBucketResponse>
    {
        private readonly StorageContext _context;

        public Handler(
            ILogger<Handler> logger,
            StorageContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<DeleteBucketResponse> HandleAsync(DeleteBucketCommand request,
            CancellationToken cancellationToken)
        {
            var bucket = await _context.Buckets.FirstOrDefaultAsync(x => x.Id.Equals(request.Id), cancellationToken);

            if (bucket != null) _context.Buckets.Remove(bucket);

            return bucket.MapTo<DeleteBucketResponse>();
        }
    }
}