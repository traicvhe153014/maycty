using Novanet.Core.Mapper;

namespace Service.Storage.Application.Commands;

public class CreateBucketCommand : INovanetRequest<CreateBucketResponse>
{
    public string Name { get; set; } = default!;

    public string? Description { get; set; }

    internal class Handler : NovanetRequestHandler<CreateBucketCommand, CreateBucketResponse>
    {
        private readonly StorageContext _context;
        private readonly IStorageService _storageService;

        public Handler(ILogger<Handler> logger,
            StorageContext context,
            IStorageService storageService,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _context = context;
            _storageService = storageService;
        }

        protected override async Task<CreateBucketResponse> HandleAsync(CreateBucketCommand request,
            CancellationToken cancellationToken)
        {
            var entityEntry = await _context.Buckets.AddAsync(new Bucket
            {
                Name = request.Name,
                Description = request.Description
            }, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);
            _storageService.MakeBucket(entityEntry.Entity.Id.ToString());

            return entityEntry.Entity.MapTo<CreateBucketResponse>();
        }
    }
}