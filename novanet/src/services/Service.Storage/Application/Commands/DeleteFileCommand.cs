﻿namespace Service.Storage.Application.Commands;

public class DeleteFileCommand : INovanetRequest<DeleteFileResponse>
{
    public Guid Id { get; set; }

    public DeleteFileCommand(Guid id)
    {
        Id = id;
    }

    internal class Handler : NovanetRequestHandler<DeleteFileCommand, DeleteFileResponse>
    {
        public Handler(
            ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
        }

        protected override Task<DeleteFileResponse> HandleAsync(DeleteFileCommand request,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}