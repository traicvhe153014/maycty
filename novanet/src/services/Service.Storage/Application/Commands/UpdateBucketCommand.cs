using Novanet.Core.Mapper;

namespace Service.Storage.Application.Commands;

public class UpdateBucketCommand : INovanetRequest<UpdateBucketResponse>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public string? Description { get; set; }

    internal class Handler : NovanetRequestHandler<UpdateBucketCommand, UpdateBucketResponse>
    {
        private readonly StorageContext _context;

        public Handler(
            ILogger<Handler> logger,
            StorageContext context,
            IHttpContextAccessor httpContextAccessor) :
            base(logger, httpContextAccessor)
        {
            _context = context;
        }

        protected override async Task<UpdateBucketResponse> HandleAsync(UpdateBucketCommand request,
            CancellationToken cancellationToken)
        {
            var bucket = await _context.Buckets.FirstOrDefaultAsync(x => x.Id.Equals(request.Id), cancellationToken);

            bucket?.Update(request.Name, request.Description);

            if (bucket != null) _context.Buckets.Update(bucket);

            await _context.SaveChangesAsync(cancellationToken);

            return bucket.MapTo<UpdateBucketResponse>();
        }
    }
}