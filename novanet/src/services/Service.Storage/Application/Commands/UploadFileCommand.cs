﻿using File = Service.Storage.Domain.AggregateModels.File;
using SkiaSharp;

namespace Service.Storage.Application.Commands;

public class UploadFileCommand : INovanetRequest<string>
{
    public IFormFile File { get; set; }

    public UploadFileCommand(IFormFile file)
    {
        File = file;
    }

    internal class Handler : NovanetRequestHandler<UploadFileCommand, string>
    {
        private readonly int[] _imageSizes = {100, 200, 400, 800, 1600};

        private readonly IStorageService _fileServerService;
        private readonly StorageContext _context;
        private readonly AppSettings _appSettings;

        public Handler(ILogger<Handler> logger,
            IStorageService fileServerService,
            StorageContext context,
            IHttpContextAccessor httpContextAccessor, 
            AppSettings appSettings) :
            base(logger, httpContextAccessor)
        {
            _fileServerService = fileServerService;
            _context = context;
            _appSettings = appSettings;
        }

        protected override async Task<string> HandleAsync(UploadFileCommand request,
            CancellationToken cancellationToken)
        {
            var file = request.File;

            var fileId = Guid.NewGuid().ToString("N").ToLower();

            var fullPath = GetPhysicalPath(file.FileName);

            var fileExtension = Path.GetExtension(file.FileName).ToLower();

            var buffer = file.OpenReadStream().ReadToEnd();

            var filePath = $"{fileId}";

            if (request.File.ContentType.EndsWith(".bak"))
            {
                var fileName = fileId + fileExtension;

                var filePathExtension = filePath + fileExtension;

                await _fileServerService.UploadFileAsync(fullPath, filePathExtension, buffer);

                var binary = new File
                {
                    SourceName = file.FileName,

                    FileName = fileName,

                    Path = Path.Combine(fullPath, filePathExtension),

                    Timestamp = DateTimeOffset.Now,

                    Size = file.Length,

                    ContentType = file.ContentType,
                    
                    Bucket = _appSettings.StorageSettings.Bucket
                };

                await _context.Files.AddAsync(binary, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return fileName;
            }

            if (request.File.FileName.StartsWith("Original"))
            {
                return await SaveFileAsync(fullPath, file.FileName, file.Length, file.ContentType,
                    filePath + fileExtension, fileId, fileExtension, buffer);
            }

            if (request.File.ContentType.StartsWith("image") && !request.File.ContentType.Contains("gif"))
            {
                var files = new List<File>();

                var sourceBitmap = SKBitmap.Decode(buffer);

                var ratio = (float) sourceBitmap.Width / (float) sourceBitmap.Height;

                foreach (var size in _imageSizes)
                {
                    var filePathResize = filePath + $"-x{size}" + fileExtension;

                    var targetHeight = (int) (size / ratio);

                    var skImageInfo = new SKImageInfo(size, targetHeight);

                    const SKFilterQuality quality = SKFilterQuality.High;

                    var scaledBitmap = sourceBitmap.Resize(skImageInfo, quality);

                    var scaledImage = SKImage.FromBitmap(scaledBitmap);

                    var data = scaledImage.Encode();

                    var fileResize = data.ToArray();

                    await _fileServerService.UploadFileAsync(fullPath, filePathResize, fileResize);

                    files.Add(new File
                    {
                        SourceName = file.FileName,

                        FileName = fileId + $"-x{size}" + fileExtension,

                        Path = Path.Combine(fullPath, filePathResize),

                        Timestamp = DateTimeOffset.Now,

                        Size = fileResize.Length,

                        SizeType = ConvertToSizeType(size),

                        ContentType = file.ContentType,
                        
                        Bucket = _appSettings.StorageSettings.Bucket
                    });
                }

                await _context.Files.AddRangeAsync(files, cancellationToken);

                await _context.SaveChangesAsync(cancellationToken);

                return fileId + "-x{0}" + fileExtension;
            }
            else
            {
                return await SaveFileAsync(fullPath, file.FileName, file.Length, file.ContentType,
                    filePath + fileExtension, fileId, fileExtension, buffer);
            }
        }

        private async Task<string> SaveFileAsync(string fullPath, string sourceName, long size, string contentType,
            string filePath, string fileId, string fileExtension, byte[] buffer)
        {
            var fileName = fileId + fileExtension;

            var filePathExtension = filePath + fileExtension;

            await _fileServerService.UploadFileAsync(fullPath, filePathExtension, buffer);

            var binary = new File
            {
                SourceName = sourceName,

                FileName = fileName,

                Path = Path.Combine(fullPath, filePathExtension),

                Timestamp = DateTimeOffset.Now,

                Size = size,

                ContentType = contentType,
                
                Bucket = _appSettings.StorageSettings.Bucket
            };

            await _context.Files.AddAsync(binary);

            await _context.SaveChangesAsync();

            return fileName;
        }

        private static string GetPhysicalPath(string fileName)
        {
            var fileExtension = Path.GetExtension(fileName)?.Replace(".", string.Empty).ToLower();
            return $"{DirectoryExtensions.GenerateDirectory()}\\{fileExtension}";
        }

        private static SizeType ConvertToSizeType(int imageSize)
        {
            return imageSize switch
            {
                25 => SizeType._25,
                50 => SizeType._50,
                100 => SizeType._100,
                200 => SizeType._200,
                400 => SizeType._400,
                800 => SizeType._800,
                1600 => SizeType._1600,
                _ => throw new KeyNotFoundException("Could not found size type"),
            };
        }
    }
}