﻿using File = Service.Storage.Domain.AggregateModels.File;

namespace Service.Storage.Application.Commands;

public class UploadVideoCommand : INovanetRequest<string>
{
    public IFormFile File { get; set; }
    public string FileId { get; set; }

    public UploadVideoCommand(IFormFile file, string fileId)
    {
        File = file;
        FileId = fileId;
    }
    
    internal class Handler : NovanetRequestHandler<UploadVideoCommand, string>
    {
        private readonly IStorageService _fileServerService;
        private readonly StorageContext _context;
        private readonly AppSettings _appSettings;

        public Handler(ILogger<Handler> logger, IHttpContextAccessor httpContextAccessor, AppSettings appSettings,
            StorageContext context, IStorageService fileServerService) : base(logger, httpContextAccessor)
        {
            _appSettings = appSettings;
            _context = context;
            _fileServerService = fileServerService;
        }

        protected override async Task<string> HandleAsync(UploadVideoCommand request, CancellationToken cancellationToken)
        {
            var file = request.File;
            var fileId = request.FileId;

            var fullPath = GetPhysicalPath(fileId);
            var fileExtension = Path.GetExtension(file.FileName).ToLower();
            var buffer = file.OpenReadStream().ReadToEnd();
            var fileName = fileId + fileExtension;

            await _fileServerService.UploadFileAsync(fullPath, file.FileName, buffer);

            var currentFileExist = await _context.Files.AnyAsync(x => x.Id == fileId.ToGuid(), cancellationToken);
            if (!currentFileExist)
            {
                var binary = new File
                {
                    Id = fileId.ToGuid(),
                    SourceName = file.FileName,
                    FileName = fileName,
                    Path = Path.Combine(fullPath, "{0}p{1}.{2}"),
                    Timestamp = DateTimeOffset.Now,
                    Size = file.Length,
                    ContentType = "video",
                    Bucket = _appSettings.StorageSettings.Bucket
                };

                await _context.Files.AddAsync(binary, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
            }

            return fileName;
        }
        
        private static string GetPhysicalPath(string fileId)
        {
            return $"{DirectoryExtensions.GenerateDirectory()}\\streams\\{fileId}";
        }
    }
}