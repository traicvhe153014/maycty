﻿using System.IO.Compression;
using File = Service.Storage.Domain.AggregateModels.File;

namespace Service.Storage.Application.Commands;

public class UploadZipCommand : INovanetRequest<string>
{
    public IFormFile File { get; set; }

    public UploadZipCommand(IFormFile file)
    {
        File = file;
    }

    internal class Handler : NovanetRequestHandler<UploadZipCommand, string>
    {
        private readonly IStorageService _fileServerService;
        private readonly StorageContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly AppSettings _appSettings;

        public Handler(
            ILogger<Handler> logger,
            IHttpContextAccessor httpContextAccessor,
            StorageContext context,
            IStorageService fileServerService, IWebHostEnvironment webHostEnvironment, AppSettings appSettings) : base(logger,
            httpContextAccessor)
        {
            _context = context;
            _fileServerService = fileServerService;
            _webHostEnvironment = webHostEnvironment;
            _appSettings = appSettings;
        }

        protected override async Task<string> HandleAsync(UploadZipCommand request, CancellationToken cancellationToken)
        {
            var file = request.File;
            var fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (fileExtension.ToLower() != ".zip")
            {
                throw new BadRequestException("Định dạng không hợp lệ");
            }
            
            var zipFolderId = Guid.NewGuid().ToString("N").ToLower();

            // save to minio
            var fullPath = GetPhysicalPath(file.FileName);
            var fileName = zipFolderId + fileExtension;
            var filePath = $"{zipFolderId}";
            var filePathExtension = filePath + fileExtension;

            await _fileServerService.UploadFileAsync(fullPath, filePathExtension, file.OpenReadStream().ReadToEnd());

            // save entity to db
            var fileEntity = new File
            {
                SourceName = file.FileName,
                FileName = fileName,
                Path = Path.Combine(fullPath, filePathExtension),
                Timestamp = DateTimeOffset.Now,
                Size = file.Length,
                ContentType = request.File.ContentType,
                Bucket = _appSettings.StorageSettings.Bucket
            };
            await _context.Files.AddAsync(fileEntity, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            
            file.OpenReadStream().Seek(0, SeekOrigin.Begin);
            using var archive = new ZipArchive(file.OpenReadStream());
            // save zip file content to wwwroot for static hosting with relative path
            var archivePath = Path.Combine(_webHostEnvironment.WebRootPath, $"Archives/{zipFolderId}");
            if (!Directory.Exists(archivePath))
            {
                Directory.CreateDirectory(archivePath);
            }

            foreach (var entry in archive.Entries)
            {
                var stream = entry.Open();
                var archiveFilePath = Path.Combine(archivePath, entry.FullName);
                if (entry.FullName.EndsWith("/")) // is directory
                {
                    continue;
                }
                var archiveFileDirectory = Path.GetDirectoryName(archiveFilePath);
                if (archiveFileDirectory is not null && !Directory.Exists(archiveFileDirectory))
                {
                    Directory.CreateDirectory(archiveFileDirectory);
                }
                await using var writeStream =
                    new FileStream(archiveFilePath, FileMode.Create);
                await stream.CopyToAsync(writeStream, cancellationToken);
            }

            return fileName;
        }

        private static string GetPhysicalPath(string fileName)
        {
            var fileExtension = Path.GetExtension(fileName)?.Replace(".", string.Empty).ToLower();
            return $"{DirectoryExtensions.GenerateDirectory()}\\{fileExtension}";
        }
    }
}