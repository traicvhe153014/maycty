﻿using File = Service.Storage.Domain.AggregateModels.File;
using SkiaSharp;

namespace Service.Storage.Application.Commands;

public class UploadFilesCommand : INovanetRequest<List<string>>
{
    public List<IFormFile> Files { get; set; }

    public UploadFilesCommand(List<IFormFile> files)
    {
        Files = files;
    }

    internal class Handler : NovanetRequestHandler<UploadFilesCommand, List<string>>
    {
        private readonly int[] _imageSizes = {100, 200, 400, 800, 1600};

        private readonly IStorageService _fileServerService;
        private readonly StorageContext _context;

        public Handler(ILogger<Handler> logger,
            IStorageService fileServerService,
            StorageContext context,
            IHttpContextAccessor httpContextAccessor) : base(logger, httpContextAccessor)
        {
            _fileServerService = fileServerService;
            _context = context;
        }

        protected override async Task<List<string>> HandleAsync(UploadFilesCommand request,
            CancellationToken cancellationToken)
        {
            var index = 0;
            var fileNames = new List<string>();
            foreach (var file in request.Files)
            {
                index++;

                var fileId = index + "_" + file.FileName.CreateMd5().ToLower();

                var fullPath = GetPhysicalPath(file.FileName);

                var fileExtension = Path.GetExtension(file.FileName).ToLower();

                var buffer = file.OpenReadStream().ReadToEnd();

                var filePath = $"{fullPath}\\{fileId}";

                if (file.ContentType.StartsWith("image"))
                {
                    var files = new List<File>();

                    var sourceBitmap = SKBitmap.Decode(buffer);

                    var ratio = (float) sourceBitmap.Width / (float) sourceBitmap.Height;

                    foreach (var size in _imageSizes)
                    {
                        var filePathResize = filePath + $"-x{size}" + fileExtension;

                        var targetHeight = (int) (size / ratio);

                        var skImageInfo = new SKImageInfo(size, targetHeight);

                        const SKFilterQuality quality = SKFilterQuality.High;

                        var scaledBitmap = sourceBitmap.Resize(skImageInfo, quality);

                        var scaledImage = SKImage.FromBitmap(scaledBitmap);

                        var data = scaledImage.Encode();

                        var fileResize = data.ToArray();

                        await _fileServerService.UploadFileAsync(fullPath, filePathResize, fileResize);

                        files.Add(new File
                        {
                            SourceName = file.FileName,

                            FileName = fileId + $"-x{size}" + fileExtension,

                            Path = filePathResize,

                            Timestamp = DateTimeOffset.Now,

                            Size = fileResize.Length,

                            SizeType = ConvertToSizeType(size),

                            ContentType = file.ContentType
                        });
                    }

                    await _context.Files.AddRangeAsync(files, cancellationToken);
                }
                else
                {
                    var filePathExtension = filePath + fileExtension;

                    await _fileServerService.UploadFileAsync(fullPath, filePathExtension, buffer);

                    var binary = new File
                    {
                        SourceName = file.FileName,

                        FileName = file.FileName,

                        Path = filePathExtension,

                        Timestamp = DateTimeOffset.Now,

                        Size = file.Length,

                        ContentType = file.ContentType
                    };

                    await _context.Files.AddAsync(binary, cancellationToken);
                }

                fileNames.Add(fileId + "-x{0}" + fileExtension);
            }

            await _context.SaveChangesAsync(cancellationToken);

            return fileNames;
        }

        private static string GetPhysicalPath(string fileName)
        {
            var fileExtension = Path.GetExtension(fileName)?.Replace(".", string.Empty).ToLower();
            return $"{DirectoryExtensions.GenerateDirectory()}\\{fileExtension}";
        }

        private static SizeType ConvertToSizeType(int imageSize)
        {
            return imageSize switch
            {
                25 => SizeType._25,
                50 => SizeType._50,
                100 => SizeType._100,
                200 => SizeType._200,
                400 => SizeType._400,
                800 => SizeType._800,
                1600 => SizeType._1600,
                _ => throw new KeyNotFoundException("Could not found size type"),
            };
        }
    }
}