﻿namespace Service.Storage.Application.Responses;

public class DeleteFileResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;
}