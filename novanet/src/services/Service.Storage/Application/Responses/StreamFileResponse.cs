﻿namespace Service.Storage.Application.Responses;

public class StreamFileResponse
{
    public string FilePath { get; set; } = default!;

    public string ContentType { get; set; } = default!;
}