using Novanet.Core.Mapper;

namespace Service.Storage.Application.Responses;

public class DeleteBucketResponse : IMapFrom<Bucket>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;
}