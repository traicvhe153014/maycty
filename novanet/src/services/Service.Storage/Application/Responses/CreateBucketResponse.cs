using Novanet.Core.Mapper;

namespace Service.Storage.Application.Responses;

public class CreateBucketResponse : IMapFrom<Bucket>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public string? Description { get; set; }
}