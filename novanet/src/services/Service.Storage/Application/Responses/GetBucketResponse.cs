﻿using Novanet.Core.Mapper;

namespace Service.Storage.Application.Responses;

public class GetBucketResponse : IMapFrom<Bucket>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;
}