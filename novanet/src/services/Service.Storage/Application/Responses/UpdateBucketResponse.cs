using Novanet.Core.Mapper;

namespace Service.Storage.Application.Responses;

public class UpdateBucketResponse: IMapFrom<Bucket>
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public string? Description { get; set; }
}