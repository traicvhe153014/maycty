using File = Service.Storage.Domain.AggregateModels.File;

namespace Service.Storage.Domain;

public class StorageContext : DbContext
{
    public static readonly string? Schema = typeof(StorageContext).GetTypeInfo().Assembly.GetName().Name;

    public StorageContext(DbContextOptions<StorageContext> options) : base(options)
    {
    }

    public DbSet<Bucket> Buckets { get; set; } = default!;

    public DbSet<File> Files { get; set; } = default!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.HasDefaultSchema(Schema);

        modelBuilder.Entity<File>().HasKey(x => x.Id);
        modelBuilder.Entity<File>().HasIndex(x => x.FileName);
        modelBuilder.Entity<File>().Property(x => x.Path)
            .IsRequired().HasMaxLength(255);
        modelBuilder.Entity<File>().Property(x => x.Bucket)
            .IsRequired().HasMaxLength(255);
        modelBuilder.Entity<File>().HasIndex(x => x.Path);
        modelBuilder.Entity<File>().HasIndex(x => x.Bucket);

        modelBuilder.Entity<Bucket>().HasKey(x => x.Id);
    }
}