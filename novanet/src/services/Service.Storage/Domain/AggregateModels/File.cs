namespace Service.Storage.Domain.AggregateModels;

public class File
{
    public Guid Id { get; set; }

    public string SourceName { get; set; } = default!;

    public string FileName { get; set; } = default!;

    public string? ContentType { get; set; }

    public long Size { get; set; }

    public string Path { get; set; } = default!;

    public SizeType SizeType { get; set; }

    public string Bucket { get; set; } = default!;

    public DateTimeOffset Timestamp { get; set; }
}

public enum SizeType
{
    _25 = 1,
    _50,
    _100,
    _200,
    _400,
    _800,
    _1600
}