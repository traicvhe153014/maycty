namespace Service.Storage.Domain.AggregateModels;

public class Bucket
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public string? Description { get; set; }

    public ICollection<File>? Files { get; set; }

    public void Update(string name, string? description)
    {
        Name = !string.IsNullOrEmpty(name) ? name : Name;
        Description = !string.IsNullOrEmpty(description) ? description : Description;
    }
}