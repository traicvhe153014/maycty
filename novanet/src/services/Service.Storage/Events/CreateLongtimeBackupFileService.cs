﻿using System.Diagnostics;
using NovanetCore.Business.BusinessObjects;

namespace Service.Storage.Events;

public class CreateLongtimeBackupFileService : EventBusListenerService<CreateLongtimeBackupFile>
{
    private readonly ILogger<CreateLongtimeBackupFileService> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;
    private bool _isProcessing;

    public CreateLongtimeBackupFileService(
        IServiceProvider serviceProvider,
        IServiceScopeFactory serviceScopeFactory,
        ILogger<CreateLongtimeBackupFileService> logger) : base(serviceProvider)
    {
        _logger = logger;
        _isProcessing = false;
        _serviceScopeFactory = serviceScopeFactory;
    }

    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.CreateLongtimeBackupFile;
    protected override bool IsWorkQueue => false;

    protected override async Task Processing(CreateLongtimeBackupFile signal)
    {
        _isProcessing = true;
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _logger.LogInformation("{Name} job started", nameof(CreateLongtimeBackupFileService));

        using var scope = _serviceScopeFactory.CreateScope();
        var storageService = scope.ServiceProvider.GetRequiredService<IStorageService>();

        var fileName = DateTimeOffset.Now.ToString("yyyyMMdd") + ".zip";

        await storageService.UploadFileAsync("backups", fileName, signal.FileByte);

        stopwatch.Stop();
        _logger.LogInformation("Create Longtime BackupFile Service job done. Elapsed time: {Time}",
            stopwatch.ElapsedMilliseconds.ToString());

        _isProcessing = false;
    }
}