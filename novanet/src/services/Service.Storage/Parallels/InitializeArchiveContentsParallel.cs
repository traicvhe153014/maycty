﻿using System.IO.Compression;
using MediatR;

namespace Service.Storage.Parallels;

public class InitializeArchiveContentsParallel : INotification
{
    internal class Handler : INotificationHandler<InitializeArchiveContentsParallel>
    {
        private readonly StorageContext _context;
        private readonly IStorageService _storageService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public Handler(StorageContext context, IStorageService storageService, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _storageService = storageService;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task Handle(InitializeArchiveContentsParallel notification, CancellationToken cancellationToken)
        {
            var archiveRootPath = Path.Combine(_webHostEnvironment.WebRootPath, "Archives");
            var zipFiles = await _context.Files.Where(x => x.ContentType == "application/x-zip-compressed")
                .ToListAsync(cancellationToken);
            foreach (var zipFile in zipFiles)
            {
                var zipStream = await _storageService.DownloadFileStreamAsync(zipFile.Path);
                using var archive = new ZipArchive(zipStream);
               
                var folderPath = Path.Combine(
                    archiveRootPath,
                    zipFile.FileName.Split('.').FirstOrDefault() ?? string.Empty);
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                    foreach (var entry in archive.Entries)
                    {
                        var stream = entry.Open();
                        var archiveFilePath = Path.Combine(folderPath, entry.FullName);
                        if (entry.FullName.EndsWith("/")) // is directory
                        {
                            continue;
                        }
                        var archiveFileDirectory = Path.GetDirectoryName(archiveFilePath);
                        if (archiveFileDirectory is not null && !Directory.Exists(archiveFileDirectory))
                        {
                            Directory.CreateDirectory(archiveFileDirectory);
                        }
                        await using var writeStream = new FileStream(archiveFilePath, FileMode.Create);
                        await stream.CopyToAsync(writeStream, cancellationToken);
                    }
                }
            }
        }
    }
}