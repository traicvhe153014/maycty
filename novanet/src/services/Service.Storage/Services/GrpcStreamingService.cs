﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using MediatR;
using NovanetGrpcFileStreaming;

namespace Service.Storage.Services;

public class GrpcStreamingService : FileStreamingService.FileStreamingServiceBase
{
    private readonly IMediator _mediator;

    public GrpcStreamingService(IMediator mediator)
    {
        _mediator = mediator;
    }

    public override async Task<Empty> Upload(IAsyncStreamReader<BytesContent> requestStream, ServerCallContext context)
    {
        await using var memoryStream = new MemoryStream();
        await requestStream.MoveNext();
        var fileName = requestStream.Current.Info.FileName;
        var fileId = requestStream.Current.Info.FileId;
        var fileExtension = requestStream.Current.Info.FileExtension;

        var formFile = new FormFile(memoryStream, 0, requestStream.Current.FileSize,
            fileName, $"{fileName}{fileExtension}");
        while (await requestStream.MoveNext())
        {
            var buffer = requestStream.Current.Buffer.ToByteArray();
            await memoryStream.WriteAsync(buffer);
        }
        await _mediator.Send(new UploadVideoCommand(formFile, fileId));
        return new Empty();
    }
}