﻿using Novanet.Core.Constants;
using File = System.IO.File;

namespace Service.Storage.Services;

public interface IStorageService
{
    string MakeBucket(string name, string bucket = "");

    Task UploadFileAsync(string subPath, string? objectName, byte[] fileBytes);

    Task<Stream> DownloadFileStreamAsync(string? fileName);

    Task<byte[]> DownloadFileByteAsync(string? fileName);
}

public class StorageService : IStorageService
{
    private readonly string _filesBucket;

    public StorageService(AppSettings appSettings)
    {
        _filesBucket = Path.Combine(appSettings.StorageSettings.Saved, appSettings.StorageSettings.Bucket);
        MakeBucket(_filesBucket);
    }

    public string MakeBucket(string name, string bucket = "")
    {
        var path = Path.Combine(bucket, name);
        var existed = Directory.Exists(name);
        if (existed) return null;
        var subPathItems = path.Split("\\");
        var subPath = string.Empty;

        foreach (var item in subPathItems)
        {
            subPath = Path.Combine(subPath, item);
            existed = Directory.Exists(subPath);
            if (existed) continue;
            Directory.CreateDirectory(subPath);
        }
        
        return path;
    }

    public async Task UploadFileAsync(string subPath, string? objectName, byte[] fileBytes)
    {
        await UploadAsync(MakeBucket(subPath, _filesBucket), objectName, fileBytes);
    }

    public async Task<Stream> DownloadFileStreamAsync(string? fileName)
    {
        if (string.IsNullOrEmpty(fileName))
            throw new ApplicationException(ErrorMessageValidate.FileNotFound);
        var byteArray = await File.ReadAllBytesAsync(fileName);
        Stream stream = new MemoryStream(byteArray);
        return stream;
    }

    public async Task<byte[]> DownloadFileByteAsync(string? fileName)
    {
        if (string.IsNullOrEmpty(fileName))
            throw new ApplicationException(ErrorMessageValidate.FileNotFound);
        var byteArray = await File.ReadAllBytesAsync(fileName);
        return byteArray;
    }

    private static async Task UploadAsync(string bucketName, string? objectName, byte[] fileBytes)
    {
        var fileName = NormalFileName(objectName);
        if (string.IsNullOrEmpty(fileName))
            throw new ApplicationException("Không tìm thấy tên tập tin");
        await using var memoryStream = new MemoryStream();
        memoryStream.Write(fileBytes, 0, fileBytes.Length);
        memoryStream.Seek(0, SeekOrigin.Begin);
        await using var file = new FileStream(Path.Combine(bucketName, fileName), FileMode.Create, FileAccess.Write);
        var bytes = new byte[memoryStream.Length];
        _ = await memoryStream.ReadAsync(bytes.AsMemory(0, (int) memoryStream.Length));
        file.Write(bytes, 0, bytes.Length);
        memoryStream.Close();
    }

    private static string? NormalFileName(string? fileName)
    {
        while (fileName != null && fileName.Contains('\\'))
            fileName = fileName.Replace("\\", "/");
        return fileName;
    }
}