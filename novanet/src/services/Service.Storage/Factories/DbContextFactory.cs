namespace Service.Storage.Factories;

public class DbContextFactory : IDesignTimeDbContextFactory<StorageContext>
{
    public StorageContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.dev.json")
            .Build();

        var appSettings = new AppSettings();
        configuration.Bind(appSettings);

        var optionsBuilder = new DbContextOptionsBuilder<StorageContext>();
        optionsBuilder.UseSqlServer(appSettings.ConnectionStrings.DefaultConnection, m =>
            {
                m.MigrationsAssembly(StorageContext.Schema);
                m.MigrationsHistoryTable("__EFMigrationsHistory", StorageContext.Schema);
            }
        );
        return new StorageContext(optionsBuilder.Options);
    }
}