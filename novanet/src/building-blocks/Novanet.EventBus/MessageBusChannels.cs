﻿namespace Novanet.EventBus;

public class MessageBusChannels
{
    public const string UpdateServiceIdentityCache = nameof(UpdateServiceIdentityCache);
    public const string UpdateServiceSettingsCache = nameof(UpdateServiceSettingsCache);
    public const string UpdateServiceSharingCache = nameof(UpdateServiceSharingCache);
    public const string UpdateCacheManager = nameof(UpdateCacheManager);
    public const string AdvertisingLog = nameof(AdvertisingLog);
    public const string AdvertisingFraudClick = nameof(AdvertisingFraudClick);
    public const string AdvertisingLogQueue = nameof(AdvertisingLogQueue);
    public const string AdvertisingLogFlag = nameof(AdvertisingLogFlag);
    public const string AdvertisingConversionLog = nameof(AdvertisingConversionLog);
    public const string UpdateAdObjectStatus = nameof(UpdateAdObjectStatus);
    public const string TransactionValidator = nameof(TransactionValidator);
    public const string CheckCampaignBudget = nameof(CheckCampaignBudget);
    public const string UpdateObjectGroupConditions = nameof(UpdateObjectGroupConditions);
    public const string UpdateMetricReportManager = nameof(UpdateMetricReportManager);
    public const string CreateLongtimeBackupFile = nameof(CreateLongtimeBackupFile);
    public const string UploadExcelFile = nameof(UploadExcelFile);
    public const string UserLogin = nameof(UserLogin);
    public const string SyncReport = nameof(SyncReport);
    public const string SyncWebsitePublisherReport = nameof(SyncWebsitePublisherReport);
    public const string BatchSyncWebsitePublisherReport = nameof(BatchSyncWebsitePublisherReport);
    public const string SyncTrackingLocation = nameof(SyncTrackingLocation);
    public const string UpdateWebsitePriceStatus = nameof(UpdateWebsitePriceStatus);
    public const string UpdateAdvertisingVideoValue = nameof(UpdateAdvertisingVideoValue);

    public static string PatchCacheManager<T>()
    {
        return $"{nameof(PatchCacheManager)}<{typeof(T).Name}>";
    }
}