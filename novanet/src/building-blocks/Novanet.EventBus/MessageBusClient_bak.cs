﻿using System.Collections.Concurrent;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace Novanet.EventBus;

public class MessageBusClientBak // : IMessageBusClient
{ 
    private readonly IConfiguration _config;
    private readonly ILogger<MessageBusClient> _logger;
    private readonly Lazy<ConnectionMultiplexer> _lazyConnection;
    private readonly int _maxQueue = 50;
    private readonly ConcurrentDictionary<string, DateTime> _timeDict;
    private ISubscriber Subscriber => _lazyConnection.Value.GetSubscriber();
    public MessageBusClientBak(IConfiguration config, ILogger<MessageBusClient> logger)
    {
        _config = config;
        _logger = logger;
        _timeDict = new ConcurrentDictionary<string, DateTime>();
        _lazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(_config["PubSubServer:Url"]));
    }
    
    public void Publish(string channel, object data)
    {
        Subscriber.Publish(channel, JsonSerializer.Serialize(data));
    }

    public async Task PublishAsync(string channel, object data)
    {
        await Subscriber.PublishAsync(channel, JsonSerializer.Serialize(data));
    }
    
    public void Subscribe<T>(string channel, Func<T, Task> func)
    {
        Subscriber.Subscribe(channel, (ch, msg) =>
        {
            func(Convert(msg));
        });

        static T Convert(string msg)
        {
            if (typeof(T) == typeof(string))
                return (T)(object)msg;
            return
                JsonSerializer.Deserialize<T>(msg);
        }
    }

    public async Task SubscribeAsync<T>(string channel, Func<T, Task> func)
    {
        await Subscriber.SubscribeAsync(channel, (ch, msg) =>
        {
            func(Convert(msg));
        });
        static T Convert(string msg)
        {
            if (typeof(T) == typeof(string))
                return (T)(object)msg;
            return
                JsonSerializer.Deserialize<T>(msg);
        }
    }
    
    public void SubscribeWorkQueue<T>(string channel, Func<T, Task> func)
    {
        var queue = Subscriber.Subscribe(channel);
        queue.OnMessage(handler =>
        {
            if (_config.GetValue<bool>("PubSubServer:Warning"))
            {
                queue.TryGetCount(out int count);
                if (count >= _maxQueue)
                {
                    _ = LogWarning(); Task LogWarning()
                    {
                        var now = DateTime.Now;
                        var timeCheck = _timeDict.GetOrAdd(channel, now);
                        if (now - timeCheck > TimeSpan.FromSeconds(15))
                        {
                            _timeDict[channel] = now;
                            _logger.LogWarning("Channel [{0}] has exceeded the number of items. The number of items is {1}", channel, count);
                        }
                        return Task.CompletedTask;
                    }
                }
            }
            return func(JsonSerializer.Deserialize<T>(handler.Message));
        });
    }

    public async Task SubscribeWorkQueueAsync<T>(string channel, Func<T, Task> func)
    {
        var queue = await Subscriber.SubscribeAsync(channel);
        queue.OnMessage(handler =>
        {
            if (_config.GetValue<bool>("PubSubServer:Warning"))
            {
                queue.TryGetCount(out int count);
                if (count >= _maxQueue)
                {
                    _ = LogWarning(); Task LogWarning()
                    {
                        var now = DateTime.Now;
                        var timeCheck = _timeDict.GetOrAdd(channel, now);
                        if (now - timeCheck > TimeSpan.FromSeconds(15))
                        {
                            _timeDict[channel] = now;
                            _logger.LogWarning("Channel [{0}] has exceeded the number of items. The number of items is {1}", channel, count);
                        }
                        return Task.CompletedTask;
                    }
                }
            }
            return func(JsonSerializer.Deserialize<T>(handler.Message));
        });
    }

    public Task UnsubscribeAsync(string channel)
    {
        return Subscriber.UnsubscribeAsync(channel);
    }

    public Task UnsubscribeAsync(params string[] channels)
    {
        return Task.WhenAll(channels.Select(async channel => await Subscriber.UnsubscribeAsync(channel)));
    }

    public void Unsubscribe(string channel)
    {
        Subscriber.Unsubscribe(channel);
    }

    public void Unsubscribe(params string[] channels)
    {
        if (channels?.Any() != true) return;
        foreach (var channel in channels)
            Subscriber.Unsubscribe(channel);
    }
}