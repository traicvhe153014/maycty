﻿using CliFx;
using Microsoft.Extensions.DependencyInjection;
using Novanet.CLI.Commands;

var exitCode = 0;
exitCode = await new CliApplicationBuilder()
    .AddCommandsFromThisAssembly()
    .UseTypeActivator(Providers().GetService)
    .SetExecutableName("novanet")
    .Build()
    .RunAsync(args);
return exitCode;

ServiceProvider Providers()
{
    var services = new ServiceCollection()
        .AddTransient<InstallCommand>()
        .AddTransient<ServiceCommand>();
    return services.BuildServiceProvider();
}