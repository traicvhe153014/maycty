﻿using System.Runtime.InteropServices;
using CliFx;
using CliFx.Attributes;
using CliFx.Infrastructure;

namespace Novanet.CLI.Commands;

[Command("install")]
public class InstallCommand : ICommand
{
    [CommandParameter(0, Description = "Name of extension (redis, rabbitmq, pm2 ...)")]
    public string Extension { get; init; } = default!;

    public static bool IsWindows() =>
        RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

    public static bool IsMacOs() =>
        RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

    public static bool IsLinux() =>
        RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

    public async ValueTask ExecuteAsync(IConsole console)
    {
        switch (Extension)
        {
            case "redis":
                await DownloadRedis(console);
                break;
        }

        await console.Output.WriteLineAsync();
    }

    private static async Task<string> DownloadRedis(IConsole console)
    {
        const string fileInfo = "Redis-x64-3.0.504.msi";
        const string redisMsi = $"https://github.com/microsoftarchive/redis/releases/download/win-3.0.504/{fileInfo}";
        var httpClient = new HttpClient();
        await console.Output.WriteLineAsync($"{fileInfo} downloading...");
        var response = await httpClient.GetAsync(redisMsi);
        response.EnsureSuccessStatusCode();
        await using var ms = await response.Content.ReadAsStreamAsync();
        await using var fs = File.Create(fileInfo);
        ms.Seek(0, SeekOrigin.Begin);
        await ms.CopyToAsync(fs);
        fs.Close();
        ms.Close();
        await console.Output.WriteLineAsync($"{fileInfo} downloaded.");
        return fileInfo;
    }
}