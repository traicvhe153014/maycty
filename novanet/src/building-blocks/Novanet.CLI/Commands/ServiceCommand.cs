﻿using CliFx;
using CliFx.Attributes;
using CliFx.Infrastructure;

namespace Novanet.CLI.Commands;

[Command("service")]
public class ServiceCommand : ICommand
{
    [CommandParameter(0, Description = "Name of microservice")]
    public string Service { get; init; } = default!;

    [CommandOption("prefix", 'p', Description = "Parameters for executable")]
    public string Prefix { get; init; } = default!;

    public async ValueTask ExecuteAsync(IConsole console)
    {
        var service = $"Service.{Service}";
        var projectPath = $"./src/services/{service}/{service}.csproj";
        await console.Output.WriteLineAsync(Service);
    }
}