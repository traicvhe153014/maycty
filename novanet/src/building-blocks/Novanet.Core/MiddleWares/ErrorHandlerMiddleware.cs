using Microsoft.AspNetCore.Http.Extensions;
using Novanet.Core.Constants;

namespace Novanet.Core.MiddleWares;

public class ErrorHandlerMiddleware
{
    private readonly RequestDelegate _next;

    public ErrorHandlerMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var urlRequest = context.Request.GetDisplayUrl();
        try
        {
            Interlocked.Increment(ref HealthCounter.HitToServer);
            if (urlRequest.Contains("/display/") && !urlRequest.Contains("/display/health?type="))
                Interlocked.Increment(ref HealthCounter.HitToServerDisplay);
            await _next(context);
            Interlocked.Increment(ref HealthCounter.HitToServerSuccess);
            if (urlRequest.Contains("/display/") && !urlRequest.Contains("/display/health?type="))
                Interlocked.Increment(ref HealthCounter.HitToServerDisplaySuccess);
            if (HasError(context.Response.StatusCode, out var err))
            {
                if (urlRequest.Contains("/display/") && !urlRequest.Contains("/display/health?type="))
                    Interlocked.Increment(ref HealthCounter.HitToServerDisplayError);
                await WriteResponseAsync(context, context.Response.StatusCode, err);
            }
        }
        catch (Exception ex)
        {
            Interlocked.Increment(ref HealthCounter.HitToServerError);
            if (urlRequest.Contains("/display/") && !urlRequest.Contains("/display/health?type="))
                Interlocked.Increment(ref HealthCounter.HitToServerException);
            await HandleExceptionAsync(context, ex);
        }
    }

    private static Task HandleExceptionAsync(HttpContext context, Exception exception)
    {
        return exception switch
        {
            UnauthorizedAccessException => WriteResponseAsync(context, 401, "401 Unauthorized"),
            BadRequestException => WriteResponseAsync(context, 400, GetErrors(exception)),
            ValidationException => WriteResponseAsync(context, 400, GetErrors(exception)),
            SlientException fEx => WriteResponseAsync(context, fEx.Status, GetErrors(exception)),
            _ => WriteResponseAsync(context, 500, GetErrors(exception))
        };
    }

    private static async Task WriteResponseAsync(HttpContext context, int status, params string[] errors)
    {
        if (context.Response.HasStarted) return;
        context.Response.StatusCode = status == 0 ? 500 : status;
        var json = true;
        var contentType = "application/json; charset=utf-8";
        if (context.Request.Headers["Accept"].Contains("application/xml"))
        {
            contentType = "application/xml; charset=utf-8";
            json = false;
        }

        context.Response.ContentType = contentType;
        await context.Response.WriteAsync(Serialize(new NovanetResponse(status, false, errors), json));
    }

    private static string[] GetErrors(Exception ex)
    {
        if (ex is ValidationException fEx)
        {
            return fEx.Errors.Select(t => t.ErrorMessage).Distinct().ToArray();
        }

        return _().Distinct().ToArray();

        IEnumerable<string> _()
        {
            yield return ex.Message;
            var innerEx = ex.InnerException;
            var count = 5; //Maximum 5 levels of inner-exceptions
            while (innerEx != null && count-- > 0)
            {
                yield return innerEx.Message;
                innerEx = innerEx.InnerException;
            }
        }
    }

    private static string Serialize(object @object, bool json)
    {
        if (json)
        {
            var jsonOption = new JsonSerializerOptions();
            jsonOption.Converters.Add(new JsonStringEnumConverter());
            jsonOption.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            return JsonSerializer.Serialize(@object, jsonOption);
        }

        var stringWriter = new StringWriter();
        var serializer = new XmlSerializer(@object.GetType());
        serializer.Serialize(stringWriter, @object);
        return stringWriter.ToString();
    }

    private static bool HasError(int status, out string err)
    {
        switch (status)
        {
            case >= 400 and <= 511:
                err = $"{status} {(HttpStatusCode) status}";
                return true;
            default:
                err = null;
                return false;
        }
    }
}

public static class ErrorHandlerMiddlewareExtension
{
    public static void UseErrorHandlerMiddleware(this WebApplication app)
    {
        app.UseMiddleware<ErrorHandlerMiddleware>();
    }
}