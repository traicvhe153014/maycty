﻿namespace Novanet.Core.SqlScripts;

public static class FulltextSearchBuilder
{
    public static string CatalogExisted<TDbContext>() where TDbContext : DbContext
    {
        var schema = typeof(TDbContext).GetTypeInfo().Assembly.GetName().Name;
        var catalogName = $"{schema}.Catalog";
        const string fulltextCatalogs = " sys.fulltext_catalogs ";
        return "select count(1) from" + fulltextCatalogs + $"where [name] = '{catalogName}'";
    }

    public static string FulltextSearchScript<TDbContext>() where TDbContext : DbContext
    {
        var schema = typeof(TDbContext).GetTypeInfo().Assembly.GetName().Name;
        ;
        var catalogName = $"{schema}.Catalog";
        var properties = typeof(TDbContext).GetProperties();
        var fulltextSearchBuilder = new StringBuilder()
            .AppendLine($"CREATE FULLTEXT CATALOG [{catalogName}] WITH ACCENT_SENSITIVITY = OFF AS DEFAULT;");

        foreach (var property in properties.Where(x => x.CanWrite))
        {
            fulltextSearchBuilder.AppendLine(
                $"CREATE FULLTEXT INDEX ON [{schema}].[{property.Name}] KEY INDEX [PK_{property.Name}] ON ([{catalogName}]) WITH (CHANGE_TRACKING AUTO)");
            fulltextSearchBuilder.AppendLine(
                $"ALTER FULLTEXT INDEX ON [{schema}].[{property.Name}] ADD ([Search] LANGUAGE [Vietnamese])");
            fulltextSearchBuilder.AppendLine($"ALTER FULLTEXT INDEX ON [{schema}].[{property.Name}] ENABLE");
        }

        return fulltextSearchBuilder.ToString();
    }
}