﻿namespace Novanet.Core.Extensions.LinqExtensions
{
    internal interface IValueComparer<in T>
    {
        bool Compare(T x, T y);
    }
}
