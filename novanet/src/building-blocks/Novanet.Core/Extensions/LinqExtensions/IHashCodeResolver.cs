﻿namespace Novanet.Core.Extensions.LinqExtensions
{
    internal interface IHashCodeResolver<in T>
    {
        int GetHashCodeFor(T obj);
    }
}
