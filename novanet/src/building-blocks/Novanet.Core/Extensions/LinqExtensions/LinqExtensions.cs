﻿using System.ComponentModel;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore.Query;
using MongoDB.Driver;
using Novanet.Core.Specifications;

namespace Novanet.Core.Extensions.LinqExtensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> source)
        {
            return source.Select((item, index) => (item, index));
        }
        
        public static IQueryable<T> SortBy<T>(this IQueryable<T> source, params string[] sortExpression) where T : class
        {
            if (sortExpression == null || sortExpression.Length == 0)
                return source;

            IOrderedQueryable<T> orderedQuery = null;
            for (var index = 0; index < sortExpression.Length; index++)
            {
                var exp = sortExpression[index];
                if (string.IsNullOrEmpty(exp))
                {
                    continue;
                }

                var sortField = Regex.Replace(sortExpression[index], @"[\+\-]", string.Empty);
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? source.OrderByDescending(sortField)
                        : orderedQuery.ThenByDescending(sortField);
                }
                else
                {
                    orderedQuery = index == 0 ? source.OrderBy(sortField) : orderedQuery.ThenBy(sortField);
                }
            }

            return orderedQuery ?? source;
        }

        public static IQueryable<T> SortBy<T>(this IQueryable<T> query, SortDirection sortDirection,
            params Expression<Func<T, object>>[] sortExpressions)
        {
            if (sortExpressions == null || sortExpressions.All(t => t == null)) return query;
            IOrderedQueryable<T> orderedQuery = null;
            for (var i = 0; i < sortExpressions.Length; i++)
            {
                var sortExpItem = sortExpressions[i];
                if (sortExpItem == null) continue;
                if (sortDirection == SortDirection.Descending)
                {
                    orderedQuery = i == 0
                        ? query.OrderByDescending(sortExpItem)
                        : orderedQuery?.ThenByDescending(sortExpItem);
                }
                else
                {
                    orderedQuery = i == 0 ? query.OrderBy(sortExpItem) : orderedQuery?.ThenBy(sortExpItem);
                }
            }

            return orderedQuery ?? query;
        }

        public static IQueryable<T> SortBy<T>(this IQueryable<T> query, SortDirection sortDirection,
            IEnumerable<Expression<Func<T, object>>> sortExpressions)
        {
            return query.SortBy(sortDirection, sortExpressions?.ToArray());
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string fieldName) where T : class
        {
            var resultExp = GenerateMethodCall(source, "OrderBy", fieldName);
            return source.Provider.CreateQuery<T>(resultExp) as IOrderedQueryable<T>;
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string fieldName)
            where T : class
        {
            var resultExp = GenerateMethodCall(source, "OrderByDescending", fieldName);
            return source.Provider.CreateQuery<T>(resultExp) as IOrderedQueryable<T>;
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string fieldName) where T : class
        {
            var resultExp = GenerateMethodCall(source, "ThenBy", fieldName);
            return source.Provider.CreateQuery<T>(resultExp) as IOrderedQueryable<T>;
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string fieldName)
            where T : class
        {
            var resultExp = GenerateMethodCall(source, "ThenByDescending", fieldName);
            return source.Provider.CreateQuery<T>(resultExp) as IOrderedQueryable<T>;
        }

        public static IEnumerable<T> SortBy<T>(this IEnumerable<T> source, params string[] sortExpression)
            where T : class
        {
            if (sortExpression == null || sortExpression.Length == 0)
                return source;
            IEnumerable<T> query = from item in source select item;
            IOrderedEnumerable<T> orderedQuery = null;
            for (var index = 0; index < sortExpression.Length; index++)
            {
                var exp = sortExpression[index];
                if (string.IsNullOrEmpty(exp))
                {
                    continue;
                }

                var sortField = Regex.Replace(sortExpression[index], @"[\+\-]", string.Empty);
                PropertyDescriptor sortProperty = TypeDescriptor.GetProperties(typeof(T)).Find(sortField, true);
                if (sortExpression[index].StartsWith("-"))
                {
                    orderedQuery = index == 0
                        ? query.OrderByDescending(a => sortProperty?.GetValue(a))
                        : orderedQuery!.ThenByDescending(a => sortProperty?.GetValue(a));
                }
                else
                {
                    orderedQuery = index == 0
                        ? query.OrderBy(a => sortProperty?.GetValue(a))
                        : orderedQuery!.ThenBy(a => sortProperty?.GetValue(a));
                }
            }
            query = orderedQuery;

            return query;
        }

        public static MethodCallExpression GenerateMethodCall<T>(IQueryable<T> source, string methodName,
            string fieldName) where T : class
        {
            var type = typeof(T);
            var selector = GenerateSelector<T>(fieldName, out var selectorResultType);
            var resultExp = Expression.Call(typeof(Queryable), methodName,
                new[] {type, selectorResultType},
                source.Expression, Expression.Quote(selector));
            return resultExp;
        }

        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> first,
            Expression<Func<T, bool>> second)
        {
            if (second == null) return first;
            return first.Compose(second, Expression.OrElse);
        }

        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> first,
            Expression<Func<T, bool>> second)
        {
            if (second == null) return first;
            return first.Compose(second, Expression.AndAlso);
        }

        public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> predicate)
        {
            return Expression.Lambda<Func<T, bool>>(Expression.Not(predicate.Body), predicate.Parameters[0]);
        }


        public static Expression<Func<T, bool>> Compose<T>(
            this Expression<Func<T, bool>> first,
            Expression<Func<T, bool>> second,
            Func<Expression, Expression, Expression> merge)
        {
            if (first.IsEquals(t => true)) return second;
            if (second.IsEquals(t => true)) return first;
            return ComposeExpression(first, second, merge);
        }

        public static Expression<T> ComposeExpression<T>(this Expression<T> first, Expression<T> second,
            Func<Expression, Expression, Expression> merge)
        {
            if (first.IsEquals(second)) return first;
            var map = first.Parameters.Select((f, i) => new {f, s = second.Parameters[i]})
                .ToDictionary(p => p.s, p => p.f);
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }

        public static Specification<T> ToSpec<T>(this Expression<Func<T, bool>> expression)
        {
            return new Specification<T>(expression);
        }

        private static LambdaExpression GenerateSelector<T>(string propertyName, out Type resultType) where T : class
        {
            // Create a parameter to pass into the Lambda expression (Entity => Entity.OrderByField).
            var parameter = Expression.Parameter(typeof(T), "Entity");
            //  create the selector part, but support child properties
            PropertyInfo property;
            Expression propertyAccess;
            if (propertyName.Contains('.'))
            {
                // support to be sorted on child fields.
                var childProperties = propertyName.Split('.');
                property = typeof(T).GetProperty(childProperties[0],
                    BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
                for (var i = 1; i < childProperties.Length; i++)
                {
                    property = property.PropertyType.GetProperty(childProperties[i],
                        BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                    propertyAccess = Expression.MakeMemberAccess(propertyAccess, property);
                }
            }
            else
            {
                property = typeof(T).GetProperty(propertyName,
                    BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
            }

            resultType = property.PropertyType;
            // Create the order by expression.
            return Expression.Lambda(propertyAccess, parameter);
        }

        public static IEnumerable<IEnumerable<TSource>> FetchInChunk<TSource>(this IQueryable<TSource> source,
            int chunkSize)
        {
            var iter = 0;
            while (true)
            {
                var queryable = source.Skip(iter).Take(chunkSize).ToList();
                if (!queryable.Any())
                {
                    break;
                }

                iter += chunkSize;
                yield return queryable;
            }
        }

        /// <summary>
        /// Iterate through queryable with prefetch some item
        /// </summary>
        /// <param name="source"></param>
        /// <param name="prefetch">Number of items load each database query</param>
        /// <typeparam name="TSource"></typeparam>
        /// <returns></returns>
        public static IEnumerable<TSource> TakeOneByOne<TSource>(this IQueryable<TSource> source, int prefetch = 10)
        {
            var iter = 0;
            while (true)
            {
                var items = source.Skip(iter).Take(prefetch).ToList();
                if (!items.Any())
                {
                    yield break;
                }

                iter += items.Count;
                foreach (var item in items)
                {
                    yield return item;
                }
            }
        }
        
        public static IQueryable<TResult> LeftJoin<TOuter, TInner, TKey, TResult>(
            this IQueryable<TOuter> outer,
            IQueryable<TInner> inner,
            Expression<Func<TOuter, TKey>> outerKeySelector,
            Expression<Func<TInner, TKey>> innerKeySelector,
            Expression<Func<TOuter, TInner, TResult>> resultSelector)
        {
            var groupJoin = typeof (Queryable).GetMethods()
                                                     .Single(m => m.ToString() == "System.Linq.IQueryable`1[TResult] GroupJoin[TOuter,TInner,TKey,TResult](System.Linq.IQueryable`1[TOuter], System.Collections.Generic.IEnumerable`1[TInner], System.Linq.Expressions.Expression`1[System.Func`2[TOuter,TKey]], System.Linq.Expressions.Expression`1[System.Func`2[TInner,TKey]], System.Linq.Expressions.Expression`1[System.Func`3[TOuter,System.Collections.Generic.IEnumerable`1[TInner],TResult]])")
                                                     .MakeGenericMethod(typeof (TOuter), typeof (TInner), typeof (TKey), typeof (LeftJoinIntermediate<TOuter, TInner>));
            var selectMany = typeof (Queryable).GetMethods()
                                                      .Single(m => m.ToString() == "System.Linq.IQueryable`1[TResult] SelectMany[TSource,TCollection,TResult](System.Linq.IQueryable`1[TSource], System.Linq.Expressions.Expression`1[System.Func`2[TSource,System.Collections.Generic.IEnumerable`1[TCollection]]], System.Linq.Expressions.Expression`1[System.Func`3[TSource,TCollection,TResult]])")
                                                      .MakeGenericMethod(typeof (LeftJoinIntermediate<TOuter, TInner>), typeof (TInner), typeof (TResult));

            var groupJoinResultSelector = (Expression<Func<TOuter, IEnumerable<TInner>, LeftJoinIntermediate<TOuter, TInner>>>)
                                          ((oneOuter, manyInners) => new LeftJoinIntermediate<TOuter, TInner> {OneOuter = oneOuter, ManyInners = manyInners});

            var exprGroupJoin = Expression.Call(groupJoin, outer.Expression, inner.Expression, outerKeySelector, innerKeySelector, groupJoinResultSelector);

            var selectManyCollectionSelector = (Expression<Func<LeftJoinIntermediate<TOuter, TInner>, IEnumerable<TInner>>>)
                                               (t => t.ManyInners.DefaultIfEmpty());

            var paramUser = resultSelector.Parameters.First();

            var paramNew = Expression.Parameter(typeof (LeftJoinIntermediate<TOuter, TInner>), "t");
            var propExpr = Expression.Property(paramNew, "OneOuter");

            var selectManyResultSelector = Expression.Lambda(new Replacer(paramUser, propExpr).Visit(resultSelector.Body), paramNew, resultSelector.Parameters.Skip(1).First());

            var exprSelectMany = Expression.Call(selectMany, exprGroupJoin, selectManyCollectionSelector, selectManyResultSelector);

            return outer.Provider.CreateQuery<TResult>(exprSelectMany); 
        }
          
        public static IEnumerable<T> IfThenElse<T>(
            this IEnumerable<T> elements,
            Func<bool> condition,
            Func<IEnumerable<T>, IEnumerable<T>> thenPath,
            Func<IEnumerable<T>, IEnumerable<T>> elsePath)
        {
            return condition()
                ? thenPath(elements)
                : elsePath(elements);
        }
        
        public static IQueryable<T> If<T>(
            this IQueryable<T> source,
            bool condition,
            Func<IQueryable<T>, IQueryable<T>> transform
        )
        { 
            return condition? transform(source) : source;
        }
        
        public static IQueryable<T> If<T, TP>(
            this IIncludableQueryable<T, TP> source,
            bool condition,
            Func<IIncludableQueryable<T, TP>, IQueryable<T>> transform
        )
            where T : class
        {
            return condition ? transform(source) : source;
        }

        public static IQueryable<T> If<T, TP>(
            this IIncludableQueryable<T, IEnumerable<TP>> source,
            bool condition,
            Func<IIncludableQueryable<T, IEnumerable<TP>>, IQueryable<T>> transform
        )
            where T : class
        {
            return condition ? transform(source) : source;
        }
        
        public static Expression<Func<TSource, int>> DescriptionOrder<TSource, TEnum>(this Expression<Func<TSource, TEnum>> source)
            where TEnum : struct
        {
            var enumType = typeof(TEnum);
            if (!enumType.IsEnum) throw new InvalidOperationException();

            var body = ((TEnum[])Enum.GetValues(enumType))
                .OrderBy(value => value.GetDescription())
                .Select((value, ordinal) => new { value, ordinal })
                .Reverse()
                .Aggregate((Expression)null, (next, item) => next == null ? (Expression)
                    Expression.Constant(item.ordinal) :
                    Expression.Condition(
                        Expression.Equal(source.Body, Expression.Constant(item.value)),
                        Expression.Constant(item.ordinal),
                        next));

            return Expression.Lambda<Func<TSource, int>>(body, source.Parameters[0]);
        }

        public static string GetDescription<TEnum>(this TEnum value)
            where TEnum : struct
        {
            var enumType = typeof(TEnum);
            if (!enumType.IsEnum) throw new InvalidOperationException();

            var name = Enum.GetName(enumType, value);
            var field = typeof(TEnum).GetField(name!, BindingFlags.Static | BindingFlags.Public);
            return field!.GetCustomAttribute<DescriptionAttribute>()?.Description ?? name;
        }

        private class LeftJoinIntermediate<TOuter, TInner>
        {
            public TOuter OneOuter { get; set; }
            public IEnumerable<TInner> ManyInners { get; set; }
        }

        private class Replacer : ExpressionVisitor
        {
            private readonly ParameterExpression _oldParam;
            private readonly Expression _replacement;

            public Replacer(ParameterExpression oldParam, Expression replacement)
            {
                _oldParam = oldParam;
                _replacement = replacement;
            }

            public override Expression Visit(Expression exp)
            {
                if (exp == _oldParam)
                {
                    return _replacement;
                }

                return base.Visit(exp);
            }
        }
    }
}