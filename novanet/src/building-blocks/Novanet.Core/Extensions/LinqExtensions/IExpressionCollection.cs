﻿namespace Novanet.Core.Extensions.LinqExtensions
{
    internal interface IExpressionCollection : IEnumerable<Expression>
    {
        void Fill();
    }
}
