﻿namespace Novanet.Core.Extensions;

public static class DictionaryExtension
{
    public static TValue GetValue<TValue>(this Dictionary<string, TValue> input, string key)
    {
        return input.FirstOrDefault(x => x.Key.Equals(key)).Value;
    }
    
    public static Dictionary<TKey, TElement> SafeToDictionary<TSource, TKey, TElement>(
        this IEnumerable<TSource> source, 
        Func<TSource, TKey> keySelector, 
        Func<TSource, TElement> elementSelector, 
        IEqualityComparer<TKey> comparer = null)
    {
        var dictionary = new Dictionary<TKey, TElement>(comparer);

        if (source == null)
        {
            return dictionary;
        }

        foreach (var element in source)
        {
            dictionary[keySelector(element)] = elementSelector(element);
        }

        return dictionary; 
    }
    
    public static Dictionary<TKey, TSource> SafeToDictionary<TSource, TKey>(
        this IEnumerable<TSource> source, 
        Func<TSource, TKey> keySelector, 
        IEqualityComparer<TKey> comparer = null)
    {
        var dictionary = new Dictionary<TKey, TSource>(comparer);

        if (source == null)
        {
            return dictionary;
        }

        foreach (var element in source)
        {
            dictionary[keySelector(element)] = element;
        }

        return dictionary; 
    }
}