using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Hosting;
using Novanet.Core.Authorize;
using Novanet.Core.Registrations;
using Serilog;
using Serilog.Events;

namespace Novanet.Core.Extensions;

public static class NovanetExtensions
{
    public static void NovanetCore(this WebApplicationBuilder builder, string schema,
        out WebApplication application, Action<IServiceCollection, AppSettings> actionServices = null,
        Action<WebApplication> actionApplication = null)
    {
        var environment = builder.Environment;
        var services = builder.Services;
        var appSettings = Configuration(environment);

        builder.LoggerBuilder(environment, appSettings);
        services.AddCustomSwagger(appSettings.MetaData);
        services.AddHttpClient();
        services.AddSingleton(appSettings);
        services.AddSingleton(appSettings.RabbitMQ);
        services.RegisterController();
        services.AddControllersWithViews();
        services.RegisterRedis(appSettings);
        services.AddApplicationAuthorize(appSettings);
        services.AddMediator(GetAssemblies());
        services.AddMapper();
        actionServices?.Invoke(services, appSettings);

        application = builder.Build();
        actionApplication?.Invoke(application);
        application.UseStaticFiles();
        application.UseErrorHandlerMiddleware();
        application.UseSwagger();
        application.UseSwaggerUI();
        application.UseCors("AllowAllOrigins");
        application.UseAuthentication();
        application.UseRouting();
        application.MapControllers();
        application.UseAuthorization();
    }

    public static void NovanetCore<TDbContext>(this WebApplicationBuilder builder, string schema,
        out WebApplication application, Action<IServiceCollection, AppSettings> actionServices = null,
        Action<WebApplication> actionApplication = null)
        where TDbContext : DbContext
    {
        var environment = builder.Environment;
        var services = builder.Services;
        var appSettings = Configuration(environment);

        builder.LoggerBuilder(environment, appSettings);
        services.AddCustomSwagger(appSettings.MetaData);
        services.AddDbContext<TDbContext>(options =>
            {
                var connectionString = appSettings.ConnectionStrings.DefaultConnection;
                options.UseSqlServer(connectionString, m =>
                    {
                        m.MigrationsAssembly(schema);
                        m.MigrationsHistoryTable("__EFMigrationsHistory", schema);
                    }
                );
            })
            .AddScoped<IDbConnection>(sp =>
            {
                var connectionString = appSettings.ConnectionStrings.DefaultConnection;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                return connection;
            });
        ;

        services.AddHttpClient();
        services.AddSingleton(appSettings);
        services.AddSingleton(appSettings.RabbitMQ);
        services.RegisterController();
        services.AddMemoryCache();
        services.ConfigureResponseCaching();
        services.RegisterRedis(appSettings);
        services.AddApplicationAuthorize(appSettings);
        services.AddMediator(GetAssemblies());
        services.AddMapper();
        actionServices?.Invoke(services, appSettings);

        application = builder.Build();
        application.UseStaticFiles();
        application.UseResponseCaching();
        application.UseErrorHandlerMiddleware();
        application.UseSwagger();
        application.UseSwaggerUI();
        application.UseCors("AllowAllOrigins");
        application.UseAuthentication();
        application.UseRouting();
        application.MapControllers();
        actionApplication?.Invoke(application);
        application.UseAuthorization();
    }

    public static AppSettings Configuration(IHostEnvironment environment)
    {
        var environmentName = environment.EnvironmentName;
        var assemblyPath = Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location);
        var appSettingsPath = Path.Combine(assemblyPath!, $"appsettings.{environmentName}.json");
        var appSettings = new AppSettings();
        new ConfigurationBuilder()
            .AddJsonFile(appSettingsPath)
            .Build().Bind(appSettings);
        return appSettings;
    }

    private static void AddCustomSwagger(this IServiceCollection services, MetaData metaData)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc(metaData.Name,
                new OpenApiInfo
                {
                    Title = metaData.Title,
                    Version = metaData.Version
                });
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = @"JWT Authorization header using the Bearer scheme.<br><br>
                      Enter 'Bearer' [space] and then your token in the text input below.
                      <br><br>Example: 'Bearer ...'",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer"
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header
                    },
                    new List<string>()
                }
            });
        });
    }

    public static void AddMapper(this IServiceCollection services, params Assembly[] assemblies)
    {
        assemblies = GetAssemblies(assemblies);
        var mapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new MappingProfile(assemblies));
            cfg.AddMaps(assemblies);
        }).CreateMapper();

        services.AddSingleton(mapper).AddMapperInstance(mapper);
    }

    private static void ConfigureResponseCaching(this IServiceCollection services)
    {
        services.AddResponseCaching();
    }

    public static Assembly[] GetAssemblies(params Assembly[] assemblies)
    {
        var assemblyEntries = assemblies?.Any() == true ? assemblies : new[] { Assembly.GetEntryAssembly() };

        IEnumerable<Assembly> _()
        {
            foreach (var asm in assemblyEntries)
            {
                yield return asm;
                foreach (var a in asm?.GetReferencedAssemblies()!)
                    yield return Assembly.Load(a);
            }
        }

        var getAssemblies = _()
            .Where(t => t != null && (t.FullName!.StartsWith("Service") || t.FullName!.StartsWith("Novanet")))
            .Distinct()
            .ToArray();
        return getAssemblies;
    }

    public static void LoggerBuilder(this WebApplicationBuilder builder, IHostEnvironment environment,
        AppSettings appSettings)
    {
        var logPath = Path.Combine(environment.ContentRootPath, "Logs", $"log.{appSettings.MetaData.Title}.txt");
        builder.Host.UseSerilog((_, loggerConfiguration) =>
        {
            loggerConfiguration.WriteTo.Console()
                .WriteTo.File(logPath, LogEventLevel.Warning,
                    rollingInterval: RollingInterval.Day);
        });
        builder.Logging.ClearProviders();
        builder.Logging.AddSerilog();
    }
}