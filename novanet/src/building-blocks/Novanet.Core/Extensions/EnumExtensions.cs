﻿using System.ComponentModel;

namespace Novanet.Core.Extensions;

public static class EnumExtensions
{
    public static string GetEnumDescription(this Enum @enum)
    {
        return @enum.GetType()
            .GetMember(@enum.ToString())
            .First()
            .GetCustomAttribute<DescriptionAttribute>()?
            .Description ?? string.Empty;
    }
}