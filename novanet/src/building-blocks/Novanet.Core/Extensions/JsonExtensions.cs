﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Novanet.Core.Extensions;

public static class JsonExtensions
{
    public static string Serialize<T>(this T input)
    {
        return System.Text.Json.JsonSerializer.Serialize(input);
    }

    public static T Deserialize<T>(this string input)
    {
        return !string.IsNullOrEmpty(input) ? System.Text.Json.JsonSerializer.Deserialize<T>(input) : default!;
    }
    
    public static async IAsyncEnumerable<JObject> StreamJsonList(string fileDestination)
    {
        await using var fs = new FileStream(fileDestination, FileMode.Open, FileAccess.Read);
        using var sr = new StreamReader(fs);
        using var reader = new JsonTextReader(sr);
        while (await reader.ReadAsync())
        {
            if (reader.TokenType != JsonToken.StartObject) continue;
            // Load each object from the stream and do something with it
            var obj = await JObject.LoadAsync(reader);
            yield return obj;
        }
    }
    
    public static async IAsyncEnumerable<T> StreamJsonList<T>(StreamReader sr)
    {
        using var reader = new JsonTextReader(sr);
        while (await reader.ReadAsync())
        {
            if (reader.TokenType != JsonToken.StartObject) continue;
            // Load each object from the stream and do something with it
            var obj = await JObject.LoadAsync(reader);
            yield return obj.ToObject<T>();
        }
    }
}