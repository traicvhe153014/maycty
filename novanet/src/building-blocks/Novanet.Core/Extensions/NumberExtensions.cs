﻿namespace Novanet.Core.Extensions;

public static class NumberExtensions
{
    public static long ToLong(this Guid input)
    {
        var bytes = input.ToByteArray();
        return BitConverter.ToInt64(bytes, 0);
    }
}