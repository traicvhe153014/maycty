using System.Security.Cryptography;

namespace Novanet.Core.Extensions;

public static class AesCryptoExtension
{
    private const string DefaultKey = "64f4244b5f2744858cfdbaf053b3213d";
    private const string DefaultIv = "18ff0f015475497d";

    public static string AesEncryptString(this string plainText, string key = null, string iv = null)
    {
        using var aes = Aes.Create();
        aes.Key = Convert.FromBase64String(key ?? DefaultKey);
        aes.IV = Encoding.UTF8.GetBytes(iv ?? DefaultIv);
        using var encryptor = aes.CreateEncryptor();
        using var memoryStream = new MemoryStream();
        using var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        using (var streamWriter = new StreamWriter(cryptoStream))
        {
            streamWriter.Write(plainText);
        }

        return Convert.ToBase64String(memoryStream.ToArray());
    }

    public static string AesDecryptString(this string encrypted, string key = null, string iv = null)
    {
        using var aes = Aes.Create();
        aes.Key = Convert.FromBase64String(key ?? DefaultKey);
        aes.IV = Encoding.UTF8.GetBytes(iv ?? DefaultIv);
        using var cryptoTransform = aes.CreateDecryptor();
        using var memoryStream = new MemoryStream(Convert.FromBase64String(encrypted));
        using var cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read);
        using var streamReader = new StreamReader(cryptoStream);
        return streamReader.ReadToEnd();
    }
}