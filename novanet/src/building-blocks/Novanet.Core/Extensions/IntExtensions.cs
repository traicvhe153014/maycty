﻿namespace Novanet.Core.Extensions;

public static class IntExtensions
{
    public static string ToSheetColumn(this int number)
    {
        var columnName = "";
        var columnNumber = number + 1;
        while (columnNumber > 0)
        {
            var modulo = (columnNumber - 1) % 26;
            columnName = Convert.ToChar('A' + modulo) + columnName;
            columnNumber = (columnNumber - modulo) / 26;
        }
        return columnName;
    }
}