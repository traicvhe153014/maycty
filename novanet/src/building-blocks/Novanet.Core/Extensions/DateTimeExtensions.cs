﻿using System.Globalization;

namespace Novanet.Core.Extensions;

public static class DateTimeExtensions
{
    public static DateTimeOffset OriginTime
    {
        get
        {
            var timezone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            var date = new DateTime(2012, 1, 1, 0, 0, 0);
            return TimeZoneInfo.ConvertTimeToUtc(date, timezone);
        }
    }

    public static DateTimeOffset ToDateTime(this string hexCode)
    {
        var dec = long.Parse(hexCode, NumberStyles.HexNumber);
        return dec.ToDateTime();
    }

    public static string ToHexCode(this DateTimeOffset dateTime)
    {
        var unix = dateTime.ToUnixTimeSeconds();
        return unix.ToString("X");
    }

    private static DateTimeOffset ToDateTime(this long ticks)
    {
        return DateTimeOffset.FromUnixTimeSeconds(ticks);
    }
    
    public static int GetHourOfWeek()
    {
        var timeOfDay = DateTime.Now.Hour;
        return DateTime.Now.DayOfWeek switch
        {
            DayOfWeek.Monday => timeOfDay,
            DayOfWeek.Tuesday => 24 + timeOfDay,
            DayOfWeek.Wednesday => 48 + timeOfDay,
            DayOfWeek.Thursday => 72 + timeOfDay,
            DayOfWeek.Friday => 96 + timeOfDay,
            DayOfWeek.Saturday => 120 + timeOfDay,
            DayOfWeek.Sunday => 144 + timeOfDay,
            _ => 0
        };
    }
    
    public static int GetMinuteOfYear(this DateTimeOffset time)
    {
        var timeSpan = time - OriginTime;
        return timeSpan.Days * 24 * 60 + time.Hour * 60 + time.Minute;   
    }
    
    public static int GetHourOfYear(this DateTimeOffset time)
    {            
        var timeSpan = time - OriginTime;
        return timeSpan.Days * 24 + time.Hour;          
    }
    
    public static int GetDayOfYear(this DateTimeOffset time)
    {
        var timeSpan = time - OriginTime;                        
        return timeSpan.Days + 1;
    }
    
    public static DateTimeOffset GetDateFromOrigin(this long days)
    {
        return OriginTime.AddDays(days);
    }
    
    //To Get The First Day of the Week in C#
    public static DateTime GetFirstDayOfWeek(this DateTime date)
    {
        var culture = Thread.CurrentThread.CurrentCulture;
        var diff = date.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
        if (diff < 0)
            diff += 7;
        return date.AddDays(-diff).Date;
    }

    //To Get The Last Day of the Week in C#
    public static DateTime GetLastDayOfWeek(this DateTime date)
    {
        var culture = Thread.CurrentThread.CurrentCulture;
        var diff = date.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
        if (diff < 0)
            diff += 7;
        DateTime start = date.AddDays(-diff).Date;
        return start.AddDays(6).Date;
    }
    
    public static string[] GetHourParts(int t)
    {
        var sArray = new string[2];
        // list-max-ziplist-entries 512
        sArray[0] = (t / 500).ToString();
        sArray[1] = (t % 500).ToString();
        return sArray;
    }

    public static DateTimeOffset ConvertByTimeZone(this DateTimeOffset time, string timeZone = "SE Asia Standard Time")
    {
        return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(time, timeZone);
    }
}