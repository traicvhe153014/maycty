﻿namespace Novanet.Core.Extensions;

public static class PriceExtension
{
    public static double? ParsePrice(this string price)
    {
        return (from s in Suffixes
            where price.EndsWith(s)
            select price[..^s.Length]
            into priceStr
            select double.Parse(priceStr ?? "0")).FirstOrDefault();
    }

    public static string TrimPrice(this string price)
    {
        return Suffixes.Aggregate(price, (result, s) => price.EndsWith(s) ? result[..^s.Length] : result);
    }

    private static readonly string[] Suffixes = {"VND", "VNĐ", "vnd", "vnđ"};
}