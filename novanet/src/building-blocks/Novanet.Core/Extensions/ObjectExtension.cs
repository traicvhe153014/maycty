﻿using Novanet.Core.Enums;

namespace Novanet.Core.Extensions;

public static class ObjectExtension
{
    public static Dictionary<string, TValue> ToDictionary<TValue>(this object input)
    {
        return input.Serialize().Deserialize<Dictionary<string, TValue>>();
    }

    public static bool TryCast<T>(this object input, out T output)
    {
        try
        {
            output = (T) input;
            return true;
        }
        catch
        {
            output = default;
            return false;
        }
    }

    public static object GetValue(this INovanetAttributeValue input, NovanetDataType? dataType)
    {
        switch (dataType)
        { 
            case NovanetDataType.String:
            case NovanetDataType.Url:
                return input.StringValue;
            case NovanetDataType.Number:
                return input.NumberValue;
            case NovanetDataType.Double:
                return input.DoubleValue;
            case NovanetDataType.DateTime:
                return input.DateTimeValue;
            case NovanetDataType.Boolean:
                return input.BooleanValue;
            default:
                return null;
        }
    }
    
    public static INovanetAttributeValue SetValue(this INovanetAttributeValue input, NovanetDataType? dataType,
        object value)
    {
        bool _;
        switch (dataType)
        { 
            case NovanetDataType.String:
            case NovanetDataType.Url: 
                input.StringValue = value.ToString(); 
                break;
            case NovanetDataType.Number:
                _ = int.TryParse(value.ToString(), out var valueNumber);
                input.NumberValue = _ ? valueNumber : null;
                break;
            case NovanetDataType.Double:
                _ = double.TryParse(value.ToString(), out var valueDouble);
                input.DoubleValue = _ ? valueDouble : null;
                break;
            case NovanetDataType.DateTime:
                _ = DateTimeOffset.TryParse(value.ToString(), out var valueDateTime);
                input.DateTimeValue = _ ? valueDateTime : null;
                break;
            case NovanetDataType.Boolean:
                _ = bool.TryParse(value.ToString(), out var booleanValue);
                input.BooleanValue = _ ? booleanValue : null;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(dataType), dataType, null);
        }

        return input;
    }
}