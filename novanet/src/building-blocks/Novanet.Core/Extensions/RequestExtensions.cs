﻿namespace Novanet.Core.Extensions;

public static class RequestExtensions
{
    public static string ToRawString(this IHeaderDictionary input)
    {
        return string.Join(";", input.Select(x => x.Key + "=" + x.Value.JoinString(",")).ToArray());
    }
}