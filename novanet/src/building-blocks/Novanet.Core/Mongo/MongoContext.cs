﻿using Humanizer;
using MongoDB.Driver;
using Novanet.Core.Domain;

namespace Novanet.Core.Mongo;

public interface IMongoContext
{
    IMongoCollection<T> Collection<T>() where T : INovanetDocument;

    Task<T> GetAsync<T>(Guid? id) where T : INovanetDocument, new();

    Task<List<T>> ListAsync<T>(int page, int pageSize,
        Expression<Func<T, bool>> predicate = null, SortDefinition<T> sort = null) where T : INovanetDocument;

    Task<T> CreateAsync<T>(T document) where T : INovanetDocument;

    Task<bool> CreateAsync<T>(List<T> documents) where T : INovanetDocument;

    Task<bool> UpdateAsync<T>(T document) where T : INovanetDocument;

    Task<bool> DeleteAsync<T>(Guid id) where T : INovanetDocument;
}

public class MongoContext : IMongoContext
{
    private readonly IMongoDatabase _database;

    public MongoContext(AppSettings settings)
    {
        var mongoSettings = settings.MongoShortTime;
        var client = new MongoClient(mongoSettings.Url);
        _database = client.GetDatabase(mongoSettings.Database);
    }

    public IMongoCollection<T> Collection<T>() where T : INovanetDocument =>
        _database.GetCollection<T>(typeof(T).Name.Pluralize());

    public async Task<T> GetAsync<T>(Guid? id) where T : INovanetDocument, new()
    {
        if (id.Equals(Guid.Empty)) return new T();
        var document = await Collection<T>().FindAsync(x => x.Id.Equals(id));
        return await document.FirstOrDefaultAsync();
    }

    public async Task<List<T>> ListAsync<T>(int page, int pageSize,
        Expression<Func<T, bool>> predicate = null, SortDefinition<T> sort = null)
        where T : INovanetDocument
    {
        var filter = Builders<T>.Filter.Where(predicate ?? (x => true));
        var fluent = Collection<T>().Find(filter);
        if (sort != null)
            fluent.Options.Sort = sort;
        return await fluent.Skip((page - 1) * pageSize).Limit(pageSize).ToListAsync();
    }

    public async Task<T> CreateAsync<T>(T document) where T : INovanetDocument
    {
        await Collection<T>().InsertOneAsync(document);
        return document;
    }

    public async Task<bool> CreateAsync<T>(List<T> documents) where T : INovanetDocument
    {
        try
        {
            await Collection<T>().InsertManyAsync(documents);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public async Task<bool> UpdateAsync<T>(T document) where T : INovanetDocument
    {
        var record = await Collection<T>().ReplaceOneAsync(x => x.Id.Equals(document.Id), document);
        return record.IsAcknowledged;
    }

    public async Task<bool> DeleteAsync<T>(Guid id) where T : INovanetDocument
    {
        var deleteResult = await Collection<T>().DeleteOneAsync(x => x.Id.Equals(id));
        return deleteResult.IsAcknowledged;
    }
}