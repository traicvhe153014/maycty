﻿using System.ComponentModel;
using Novanet.Core.Enums;
using Novanet.Core.Enums.ReportKeyObjects;

namespace Novanet.Core.RedisCache;

public struct RedisKey
{
    public int Database { get; set; }

    public string KeyName { get; set; }
}

public static class RedisKeys
{
    public static RedisKey KeyDisplay<T>(object key) => new()
    {
        Database = (int) RedisDatabases.ServiceDisplay,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceDisplay)}.{typeof(T).Name}.{key}"
    };

    public static RedisKey KeyIdentity<T>(object key) => new()
    {
        Database = (int) RedisDatabases.ServiceIdentity,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceIdentity)}.{typeof(T).Name}.{key}"
    };

    public static RedisKey KeySettings<T>(object key) => new()
    {
        Database = (int) RedisDatabases.ServiceSettings,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceSettings)}.{typeof(T).Name}.{key}"
    };

    public static RedisKey KeySharing<T>(object key) => new()
    {
        Database = (int) RedisDatabases.ServiceSharing,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceSharing)}.{typeof(T).Name}.{key}"
    };
    
    public static RedisKey KeyIpBlock(long key) => new()
    {
        Database = (int) RedisDatabases.ServiceIpBlock,
        KeyName = key.ToString()
    };
}

public static class RedisPrefixKey
{
    public static RedisKey PrefixDisplay<T>() => new()
    {
        Database = (int) RedisDatabases.ServiceDisplay,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceDisplay)}.{typeof(T).Name}"
    };
    public static RedisKey PrefixIdentity<T>() => new()
    {
        Database = (int) RedisDatabases.ServiceIdentity,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceIdentity)}.{typeof(T).Name}"
    };

    public static RedisKey PrefixSettings<T>() => new()
    {
        Database = (int) RedisDatabases.ServiceSettings,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceSettings)}.{typeof(T).Name}"
    };
    public static RedisKey PrefixSharing<T>() => new()
    {
        Database = (int) RedisDatabases.ServiceSharing,
        KeyName = $"Novanet.v6.{nameof(RedisDatabases.ServiceSharing)}.{typeof(T).Name}"
    };
}

public static class RedisReportKeys
{
    public static RedisKey QueueKey(ReportMetric metric, long minute) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"QUEUE_{metric}_{minute}"
    };
    
    public static RedisKey FlagKey(ReportMetric metric, int minute) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"FLAG_{metric}_{minute}"
    };
    public static RedisKey PublisherKey(object obj, object objectId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"Novanet.v6.{obj}_{objectId}"
    };

    public static RedisKey FraudClickPubKey(FraudClickTypeEnum fraudClickType, ReportPublisherDimension pubObj, long objectId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"FRAUD_{fraudClickType}_{pubObj}_{objectId}"
    };

    #region Render for single obj Report Key
    //Publisher Key
    public static RedisKey GetPubKey(ReportPublisherDimension pubObj, ReportMetric obj, long objId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{pubObj}_{obj}_{objId}"
    };

    public static RedisKey GetPubKey(ReportPublisherDimension pubObj, ReportMetric obj, string hourPart0, long objId) =>
        new()
        {
            Database = (int) RedisDatabases.ServiceReport,
            KeyName = $"{pubObj}_{obj}_{objId}_{hourPart0}"
        };

    ///hàm get key cho bao cao
    public static RedisKey GetPubKey(ReportPublisherDimension pubObj, ReportMetric obj) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{pubObj}_{obj}_"
    };

    //Adver key
    public static RedisKey GetAdverKey(ReportAdvertiserDimension adverObj, ReportMetric obj, long objId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{adverObj}_{obj}_{objId}"
    };

    public static RedisKey GetAdverKey(ReportAdvertiserDimension adverObj, ReportMetric obj, string hourPart0,
        long objId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{adverObj}_{obj}_{objId}_{hourPart0}"
    };
    
    public static RedisKey GetAdverKey(List<ReportAdvertiserDimension> adverObj, ReportMetric obj, List<long> objIds) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{string.Join("_", adverObj.ToArray())}_{obj}_{string.Join("_", objIds.ToArray())}"
    };
    
    //Client Key
    public static RedisKey GetClientKey(ReportClientDimension clientObj, ReportMetric obj, object objId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{clientObj}_{obj}_{objId}"
    };
    
    public static RedisKey GetClientKey(ReportClientDimension clientObj, ReportMetric obj, string hourPart0,
        object objId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{clientObj}_{obj}_{objId}_{hourPart0}"
    };

    // //Target key
    // public static string GetTargetKey(EPlatform platform,/* EPaymentType paymentType,*/ ETarget targetObj, EObjStatistics obj, int objId)
    // {
    //     return string.Format("{0}_{1}_{2}_{3}", platform, /*paymentType,*/ targetObj, obj, objId);
    // }
    //
    // public static string GetTargetKey(EPlatform platform,/* EPaymentType paymentType, */ETarget targetObj, EObjStatistics obj, string hourPart0, int objId)
    // {
    //     return string.Format("{0}_{1}_{2}_{3}_{4}", platform, /*paymentType,*/ targetObj, obj, objId, hourPart0);
    // }

    public static RedisKey GetAdver_PubSetKey(ReportAdvertiserDimension adverObj, ReportPublisherDimension pubObj,
        ReportMetric obj, long adverObjId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"SET_{adverObj}_{pubObj}_{obj}_{adverObjId}"
    };

    public static RedisKey GetPub_AdverSetKey(ReportAdvertiserDimension adverObj, ReportPublisherDimension pubObj,
        ReportMetric obj, long pubObjId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"SET_{pubObj}_{adverObj}_{obj}_{pubObjId}"
    };

    #endregion

    #region Render for Obj By Obj Report

    public static RedisKey GetAdver_TargetingKey(ReportAdvertiserDimension adverObj, ReportTargetingDimension targetObj,
        ReportMetric obj, long adverObjId, long targetObjId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{adverObj}_{targetObj}_{obj}_{adverObjId}_{targetObjId}"
    };

    public static RedisKey GetAdver_PubKey(ReportAdvertiserDimension adverObj, ReportPublisherDimension pubObj,
        ReportMetric obj, long adverObjId, long pubObjId) => new()
    {
        Database = (int) RedisDatabases.ServiceReport,
        KeyName = $"{adverObj}_{pubObj}_{obj}_{adverObjId}_{pubObjId}"
    };
    #endregion
}

public static class RedisEventKeys
{
    public static RedisKey PageView(long accountId, Guid clientId, UrlConditionType conditionType, string website) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.PageView}:{accountId}:{conditionType}:{website}"
    };
    
    public static RedisKey ProductView(Guid clientId) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.ProductView}"
    };
    
    public static RedisKey ProductView(Guid clientId, ObjectGroupProductType productType, string productName) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.ProductView}:{productType}:{productName?.ToLower()}"
    };

    public static RedisKey AddToCart(Guid clientId) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.AddToCart}"
    };
    
    public static RedisKey AddToCart(Guid clientId, ObjectGroupProductType productType, string productName) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.AddToCart}:{productType}:{productName?.ToLower()}"
    };

    public static RedisKey Purchase(Guid clientId) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.Purchase}"
    };
    
    public static RedisKey Purchase(Guid clientId, ObjectGroupProductType productType, string productName) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.Purchase}:{productType}:{productName?.ToLower()}"
    };

    public static RedisKey PurchaseComplete(Guid clientId) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.PurchaseComplete}"
    };
    
    public static RedisKey PurchaseComplete(Guid clientId, ObjectGroupProductType productType, string productName) => new()
    {
        Database = (int) RedisDatabases.ServiceEvent,
        KeyName = $"{clientId}:{EventType.PurchaseComplete}:{productType}:{productName?.ToLower()}"
    };
}

public enum RedisDatabases
{
    [Description("Service.Display")] ServiceDisplay = 1,

    [Description("Service.Identity")] ServiceIdentity = 2,

    [Description("Service.Notification")] ServiceNotification = 3,

    [Description("Service.Schedule")] ServiceSchedule = 4,

    [Description("Service.Settings")] ServiceSettings = 5,

    [Description("Service.Sharing")] ServiceSharing = 6,

    [Description("Service.Storage")] ServiceStorage = 7,
    
    [Description("Service.Report")] ServiceReport = 8,
    
    [Description("Service.Event")] ServiceEvent = 9,

    [Description("Service.IpBlock")] ServiceIpBlock = 10
}