﻿namespace Novanet.Core.RedisCache;

public enum CacheModes
{
    Sliding = 1,
    Absolute = 2
}
