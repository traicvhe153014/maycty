﻿using Novanet.Core.Domain;
using StackExchange.Redis;

namespace Novanet.Core.RedisCache;

public interface IRedisCacheService
{
    public ConnectionMultiplexer Connection();
    
    Task<T> GetAsync<T>(RedisKey key);
    
    Task<string> GetAsync(RedisKey key);

    Task<bool> SetAsync<T>(int databaseId, object key, T value, bool overrideKey);

    Task<bool> SetAsync<T>(RedisKey key, T value);
    
    Task<bool> SetAsync(RedisKey key, string value);

    Task<long> QueuePushAsync(RedisKey key, byte[] value);

    Task<long> QueuePushAsync(RedisKey key, string value);

    Task<bool> SetPushAsync(RedisKey key, byte[] value);

    Task<bool> SetPushAsync(RedisKey key, string value);

    Task<RedisValue[]> ListAsync(RedisKey key);

    Task<RedisValue[]> ListAsync(RedisKey key, int start, int end);
    
    Task<List<T>> ListByPrefixAsync<T>(RedisKey prefix);

    Task<Dictionary<string, string>> HashGetAllAsync(RedisKey key);

    Task<List<string>> ListKeyByPrefixAsync(RedisKey prefix);

    IAsyncEnumerable<string> EnumerateKeyByPatternAsync(RedisKey pattern);

    Task<bool> ExistsAsync(RedisKey key);

    Task<string> ExecuteLuaScript(int databaseId, string script, StackExchange.Redis.RedisKey[] keys = null, RedisValue[] values = null);
    Task ExecuteTransaction(RedisDatabases databaseKey, Func<ITransaction, Task> callback);
    
    Task HashIncrementAsync(RedisKey key, string field, long value);
    
    Task IncrementAsync(RedisKey key, long value);

    Task<T> HashGetAsync<T>(RedisKey key, string field);

    Task ListLeftPushAsync(RedisKey key, string value);
    
    Task KeyDeleteAsync(RedisKey key);

    Task<bool> PipelineSetAsync<T>(int databaseId, Dictionary<RedisKey, T> data);

    Task<bool> BatchSetAsync<T>(int databaseId, Dictionary<RedisKey, T> data);

    Task<bool> FlushDatabaseAsync(int databaseId);

    Task RemoveNonExistingKeys<TRedis, TEntity>(List<TEntity> entities, RedisDatabases database)
        where TRedis : NovanetDocument
        where TEntity : TRedis;
}
