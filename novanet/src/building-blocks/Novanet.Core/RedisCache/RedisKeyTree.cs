﻿namespace Novanet.Core.RedisCache;

public class RedisKeyTree
{
    public List<RedisKeyNode> Nodes { get; set; } = default!;
}

public class RedisKeyNode
{
    public List<RedisKey> Includes { get; set; } = default!;
    public List<RedisKey> Excludes { get; set; } = default!;
}
