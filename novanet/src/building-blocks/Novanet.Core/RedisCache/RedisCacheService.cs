﻿using Novanet.Core.Domain;
using Novanet.Core.Extensions;
using StackExchange.Redis;

namespace Novanet.Core.RedisCache;

public class RedisCacheService : IRedisCacheService, IGlobalCacheService, IRedisQueueService, IDisposable
{
    private readonly Lazy<ConnectionMultiplexer> _lazyConnection;

    public ConnectionMultiplexer Connection() => _lazyConnection.Value;
    private IDatabase Database(int id) => Connection().GetDatabase(id);

    public RedisCacheService(string connection)
    {
        _lazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(connection));
    }

    public async Task<T> GetAsync<T>(RedisKey key)
    {
        var database = Database(key.Database);
        string redisValue = await database.StringGetAsync(key.KeyName);
        return redisValue.Deserialize<T>();
    }

    public async Task<string> GetAsync(RedisKey key)
    {
        var database = Database(key.Database);
        return await database.StringGetAsync(key.KeyName);
    }

    public async Task<bool> SetAsync<T>(int databaseId, object key, T value, bool overrideKey)
    {
        var redisKey = key.ToString();
        if (overrideKey) redisKey = Key<T>(key);
        var serialize = value.Serialize();
        if (serialize is not {Length: > 1})
            return false;
        var database = Database(databaseId);
        await database.StringSetAsync(redisKey, serialize);
        return true;
    }

    public async Task<bool> SetAsync<T>(RedisKey key, T value)
    {
        var database = Database(key.Database);
        await database.StringSetAsync(key.KeyName, value.Serialize());
        return true;
    }

    public async Task<bool> SetAsync(RedisKey key, string value)
    {
        if (value is not {Length: > 1})
            return false;
        var database = Database(key.Database);
        await database.StringSetAsync(key.KeyName, value);
        return true;
    }

    public async Task<long> QueuePushAsync(RedisKey key, byte[] value)
    {
        var database = Database(key.Database);
        return await database.ListLeftPushAsync(key.KeyName, value);
    }

    public async Task<long> QueuePushAsync(RedisKey key, string value)
    {
        var database = Database(key.Database);
        return await database.ListLeftPushAsync(key.KeyName, value);
    }

    public async Task<bool> SetPushAsync(RedisKey key, byte[] value)
    {
        var database = Database(key.Database);
        return await database.SetAddAsync(key.KeyName, value);
    }

    public async Task<bool> SetPushAsync(RedisKey key, string value)
    {
        var database = Database(key.Database);
        return await database.SetAddAsync(key.KeyName, value);
    }

    public async Task<RedisValue[]> ListAsync(RedisKey key)
    {
        var database = Database(key.Database);
        return await database.ListRangeAsync(key.KeyName);
    }

    public async Task<RedisValue[]> ListAsync(RedisKey key, int start, int end)
    {
        var database = Database(key.Database);
        return await database.ListRangeAsync(key.KeyName, start, end);
    }

    public async Task<List<T>> ListByPrefixAsync<T>(RedisKey prefix)
    {
        var records = new List<T>();
        var server = Server();
        var keys = server.Keys(pattern: $"{prefix.KeyName}*", database: prefix.Database);
        foreach (var key in keys)
        {
            var record = await GetAsync<T>(new RedisKey
            {
                Database = prefix.Database,
                KeyName = key
            });
            records.Add(record);
        }

        return records;
    }

    public async Task<List<string>> ListKeyByPrefixAsync(RedisKey prefix)
    {
        var server = Server();
        var keys = server.KeysAsync(pattern: $"{prefix.KeyName}*", database: prefix.Database);
        var list = new List<string>();
        await foreach (var key in keys)
        {
            list.Add(key.ToString());
        }
        return list;
    }
    
    public async IAsyncEnumerable<string> EnumerateKeyByPatternAsync(RedisKey pattern)
    {
        var server = Server();
        var keys = server.KeysAsync(pattern: pattern.KeyName, database: pattern.Database);
        await foreach (var key in keys)
        {
            yield return key;
        }
    }

    public async Task<bool> ExistsAsync(RedisKey key)
    {
        var database = Database(key.Database);
        return await database.KeyExistsAsync(key.KeyName);
    }

    public async Task<string> ExecuteLuaScript(
        int databaseId,
        string script,
        StackExchange.Redis.RedisKey[] keys = null,
        RedisValue[] values = null)
    {
        var database = Database(databaseId);
        var json = await database.ScriptEvaluateAsync(script, keys, values);
        return json.ToString();
    }

    private static string Key<T>(object key) => typeof(T).GetTypeInfo().Assembly.GetName().Name + ":" + key;

    public static string Key(Type type, object key) => type.GetTypeInfo().Assembly.GetName().Name + ":" + key;

    public async Task ExecuteTransaction(RedisDatabases databaseKey, Func<ITransaction, Task> callback)
    {
        var database = Database((int) databaseKey);
        var transaction = database.CreateTransaction();
        await callback(transaction);
        await transaction.ExecuteAsync();
    }

    public async Task HashIncrementAsync(RedisKey key, string field, long value)
    {
        var database = Database(key.Database);
        await database.HashIncrementAsync(key.KeyName, field, value);
    }

    public async Task IncrementAsync(RedisKey key, long value)
    {
        var database = Database(key.Database);
        await database.StringIncrementAsync(key.KeyName, value);
    }

    public async Task ListLeftPushAsync(RedisKey key, string value)
    {
        var database = Database(key.Database);
        await database.ListLeftPushAsync(key.KeyName, value);
    }

    public async Task KeyDeleteAsync(RedisKey key)
    {
        var database = Database(key.Database);
        await database.KeyDeleteAsync(key.KeyName);
    }

    private IServer Server()
    {
        foreach (var endpoint in _lazyConnection.Value.GetEndPoints())
        {
            var server = _lazyConnection.Value.GetServer(endpoint);
            if (!server.IsReplica) return server;
        }

        throw new InvalidOperationException("Requires a master endpoint (found none)");
    }
    
    public async Task RemoveNonExistingKeys<TRedis, TEntity>(List<TEntity> entities, RedisDatabases database)
        where TRedis : NovanetDocument
        where TEntity : TRedis
    {
        var existingKeys = await ListKeyByPrefixAsync(RedisPrefixKey.PrefixSettings<TRedis>());
        foreach (var existingKey in existingKeys)
        {
            if (entities.All(x => $"Novanet.v6.{database}.{typeof(TRedis).Name}.{x.SubId}" != existingKey))
            {
                await KeyDeleteAsync(new RedisKey
                {
                    Database = (int) database,
                    KeyName = existingKey
                });
            }
        }
    }

    public async Task<Dictionary<string, string>> HashGetAllAsync(RedisKey key)
    {
        var database = Database(key.Database);
        var hashEntries = await database.HashGetAllAsync(key.KeyName);
        return hashEntries
            .ToDictionary()
            .ToDictionary(
                x => x.Key.ToString(),
                x => x.Value.ToString());
    }
    
    public async Task<T> HashGetAsync<T>(RedisKey key, string field)
    {
        var database = Database(key.Database);
        var hashEntry = await database.HashGetAsync(key.KeyName, field);
        return hashEntry.ToString().Deserialize<T>();
    }

    public async Task<bool> PipelineSetAsync<T>(int databaseId, Dictionary<RedisKey, T> data)
    {
        var addTasks = new List<Task>();
        var database = Database(databaseId);
        foreach (var item in data)
        {
            var addAsync = database.StringSetAsync(item.Key.KeyName, item.Value.Serialize());;
            addTasks.Add(addAsync);
        }
        var tasks = addTasks.ToArray();
        Task.WaitAll(tasks);
        return await Task.FromResult(true);
    }

    public async Task<bool> BatchSetAsync<T>(int databaseId, Dictionary<RedisKey, T> data)
    {
        var addTasks = new List<Task>();
        var database = Database(databaseId);
        var batch = database.CreateBatch();
        foreach (var item in data)
        {
            var addAsync = batch.StringSetAsync(item.Key.KeyName, item.Value.Serialize());;
            addTasks.Add(addAsync);
        }
        batch.Execute();
        var tasks = addTasks.ToArray();
        Task.WaitAll(tasks);
        return await Task.FromResult(true);
    }

    public async Task<bool> FlushDatabaseAsync(int databaseId)
    {
        var database = Database(databaseId);
        await database.ExecuteAsync("FLUSHDB");
        return true;
    }

    public void Dispose()
    {
        GC.Collect();
        GC.WaitForPendingFinalizers();
    }
}