﻿using MongoDB.Bson;
using Novanet.Core.Extensions;
using StackExchange.Redis;

namespace Novanet.Core.RedisCache;

public interface IRedisPubSubService
{
    public ConnectionMultiplexer Connection();

    Task PublishAsync<TData>(string channel, TData data);

    Task SubscribeAsync<T>(string channel, Action<T> action);

    Task HashSetAsync(string key, string hashKey, string value);

    Task<string> HashGetAsync(string key, string hashKey);
    
    Task<bool> HashExistAsync(string key, string hashKey);

    Task HashRemoveAsync(string key, string hashKey);
}

public class RedisPubSubService : IRedisPubSubService, IDisposable
{
    private readonly Lazy<ConnectionMultiplexer> _lazyConnection;
    private ISubscriber Subscriber => _lazyConnection.Value.GetSubscriber();
    public ConnectionMultiplexer Connection() => _lazyConnection.Value;

    public RedisPubSubService(AppSettings appSettings)
    {
        _lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            ConnectionMultiplexer.Connect(appSettings.RedisPubSub.Url));
    }

    public async Task PublishAsync<TData>(string channel, TData data)
    {
        await Subscriber.PublishAsync(channel, data.Serialize());
    }

    public async Task SubscribeAsync<T>(string channel, Action<T> action)
    {
        await Subscriber.SubscribeAsync(channel, (ch, msg) => { action(msg.ToString().Deserialize<T>()); });
    }

    public async Task HashSetAsync(string key, string hashKey, string value)
    {
        var connection = Connection();
        var database = connection.GetDatabase(0);
        await database.HashSetAsync(key, new[] {new HashEntry(hashKey, value)});
    }

    public async Task<string> HashGetAsync(string key, string hashKey)
    {
        var connection = Connection();
        var database = connection.GetDatabase(0);
        return await database.HashGetAsync(key, hashKey);
    }
    
    public async Task<bool> HashExistAsync(string key, string hashKey)
    {
        var connection = Connection();
        var database = connection.GetDatabase(0);
        return await database.HashExistsAsync(key, hashKey);
    }

    public async Task HashRemoveAsync(string key, string hashKey)
    {
        var connection = Connection();
        var database = connection.GetDatabase(0);
        await database.HashDeleteAsync(key, hashKey);
    }

    public void Dispose()
    {
        GC.Collect();
        GC.WaitForPendingFinalizers();
    }
    
    private IServer Server()
    {
        foreach (var endpoint in _lazyConnection.Value.GetEndPoints())
        {
            var server = _lazyConnection.Value.GetServer(endpoint);
            if (!server.IsReplica) return server;
        }

        throw new InvalidOperationException("Requires a master endpoint (found none)");
    }
}