﻿namespace Novanet.Core.Mediator
{
    public interface INovanetRequest : IRequest<NovanetResponse>
    {

    }

    public interface INovanetRequest<TResponse> : IRequest<NovanetResponse<TResponse>>
    {
    }
}
