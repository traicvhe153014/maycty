﻿using System.IdentityModel.Tokens.Jwt;
using Novanet.Core.Extensions;

namespace Novanet.Core.Mediator;

public abstract class NovanetRequestHandler<TRequest> : NovanetExceptionHandler<TRequest, NovanetResponse>,
    IRequestHandler<TRequest, NovanetResponse>
    where TRequest : IRequest<NovanetResponse>
{
    protected NovanetRequestHandler(ILogger<NovanetRequestHandler<TRequest>> logger) : base(logger)
    {
    }

    public virtual async Task<NovanetResponse> Handle(TRequest request, CancellationToken cancellationToken)
    {
        try
        {
            await HandleAsync(request, cancellationToken);
            return new NovanetResponse((int) HttpStatusCode.OK, true);
        }
        catch (Exception ex)
        {
            return await HandleExceptionAsync(ex, request, cancellationToken);
        }
    }

    protected abstract Task HandleAsync(TRequest request, CancellationToken cancellationToken);
}

public abstract class NovanetRequestHandler<TRequest, TResponse> :
    NovanetExceptionHandler<TRequest, NovanetResponse<TResponse>>,
    IRequestHandler<TRequest, NovanetResponse<TResponse>>
    where TRequest : IRequest<NovanetResponse<TResponse>>
{
    protected readonly UserClaimsValue UserClaimsValue;

    protected NovanetRequestHandler(
        ILogger<NovanetRequestHandler<TRequest, TResponse>> logger,
        IHttpContextAccessor httpContextAccessor) : base(logger)
    {
        UserClaimsValue = UserClaims(httpContextAccessor);
    }

    public virtual async Task<NovanetResponse<TResponse>> Handle(TRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var result = await HandleAsync(request, cancellationToken);
            return new NovanetResponse<TResponse>
            {
                Data = result,
                Status = (int) HttpStatusCode.OK,
                Success = true
            };
        }
        catch (Exception ex)
        {
            return await HandleExceptionAsync(ex, request, cancellationToken);
        }
    }

    protected abstract Task<TResponse> HandleAsync(TRequest request, CancellationToken cancellationToken);

    private static UserClaimsValue UserClaims(IHttpContextAccessor httpContextAccessor)
    {
        var accessTokenKeyValue = httpContextAccessor.HttpContext?
            .Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value;
        if (accessTokenKeyValue.ToString() == null) return default!;
        var accessToken = accessTokenKeyValue?.ToString().Split(" ").LastOrDefault();
        if (string.IsNullOrEmpty(accessToken)) return default!;
        var handler = new JwtSecurityTokenHandler();
        var jwtSecurityToken = handler.ReadJwtToken(accessToken);
        return new UserClaimsValue
        {
            Id = jwtSecurityToken.Claims.FirstOrDefault(x => x.Type.Equals("id"))?.Value.ToGuid(),
            FullName = jwtSecurityToken.Claims.FirstOrDefault(x => x.Type.Equals("fullname"))?.Value,
            UserName = jwtSecurityToken.Claims.FirstOrDefault(x => x.Type.Equals("username"))?.Value,
            Email = jwtSecurityToken.Claims.FirstOrDefault(x => x.Type.Equals("email"))?.Value,
            Policies = jwtSecurityToken.Claims.FirstOrDefault(x => x.Type.Equals("policies"))?.Value.Split(","),
            Roles = jwtSecurityToken.Claims.FirstOrDefault(x => x.Type.Equals("roles"))?.Value.Split(","),
        };
    }
}

public abstract class NovanetExceptionHandler<TRequest, TResponse>
    where TResponse : NovanetResponse, new()
    where TRequest : IRequest<NovanetResponse>
{
    protected readonly ILogger<NovanetExceptionHandler<TRequest, TResponse>> Logger;

    protected NovanetExceptionHandler(ILogger<NovanetExceptionHandler<TRequest, TResponse>> logger)
    {
        Logger = logger;
    }

    // TODO: Log trong ngày và clear sau ngày mới, custom thông tin ex để đọc ra
    protected virtual Task<TResponse> HandleExceptionAsync(Exception ex, TRequest request,
        CancellationToken cancellationToken)
    {
        switch (ex)
        {
            case UnauthorizedAccessException:
            case BadRequestException:
            // TODO: Cần chia nhỏ validation zone, template, advertising item,...
            case ValidationException:
            // TODO: Cần log default advertising
            case SlientException:
                break;
            default:
                Logger.LogError(ex, "{HandleException} EXCEPTION", request.GetType().FullName);
                break;
        }

        throw ex;
    }
}