﻿namespace Novanet.Core.Utils;

public class AdvertisingUtils
{
    public static double SumField(Dictionary<Guid, Dictionary<string, object>> data, string field)
    {
        var sum = 0d;
        foreach (var item in data)
        {
            var currentObject = item.Value.TryGetValue(field, out var currentItem) ? currentItem : null;
            if (currentObject is not null)
            {
                sum += (double) currentObject;
            }
        }

        return sum;
    }
}