﻿using System.Text.RegularExpressions;

namespace Novanet.Core.Utils;

public static class RegexUtils
{
    public static readonly Regex SlugRegex = new(@"(^[a-z0-9])([a-z0-9_-]+)*([a-z0-9])$");

    public static readonly Regex SlugWithSegmentsRegex = new(@"^(?!-)[a-z0-9_-]+(?<!-)(/(?!-)[a-z0-9_-]+(?<!-))*$");

    public static readonly Regex IpAddressRegex =
        new(@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    public static readonly Regex EmailRegex = new(@"^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$");

    public static readonly Regex
        UrlRegex = new(@"[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");

    public static readonly Regex PositiveNumberRegex = new(@"^[1-9]+[0-9]*$");
}