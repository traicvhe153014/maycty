using MimeKit;

namespace Novanet.Core.Services;

public class MailSettings
{
    public string Host { get; set; } = default!;

    public int Port { get; set; }

    public string DisplayName { get; set; } = default!;

    public string Email { get; set; } = default!;

    public string Password { get; set; } = default!;

    public List<MailReceiver> Receivers { get; set; }
}

public class MailReceiver
{
    public string Email { get; set; }

    public string Name { get; set; }
}

public interface IMailService
{
    Task<bool> SendAsync(MailSettings mailSettings,
        string receiverEmail, string receiverName, string subject, string body);
}

public class MailService : IMailService
{
    private readonly ILogger<MailService> _logger;

    public MailService(ILogger<MailService> logger)
    {
        _logger = logger;
    }

    public async Task<bool> SendAsync(MailSettings mailSettings,
        string receiverEmail, string receiverName, string subject, string body)
    {
        try
        {
            _logger.LogInformation("Sending to: {To}", receiverEmail);

            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(mailSettings.DisplayName, mailSettings.Email));
            mimeMessage.To.Add(new MailboxAddress(receiverName, receiverEmail));
            mimeMessage.Subject = subject;
            mimeMessage.Body = new TextPart("html") { Text = body };

            using var client = new MailKit.Net.Smtp.SmtpClient();
            await client.ConnectAsync(mailSettings.Host, mailSettings.Port, false);

            await client.AuthenticateAsync(mailSettings.Email, mailSettings.Password);

            await client.SendAsync(mimeMessage);

            await client.DisconnectAsync(true);
            
            _logger.LogInformation("Sent to: {To}", receiverEmail);

            return true;
        }
        catch (Exception e)
        {
            _logger.LogError("Error: {Error}", e.Message);
            return false;
        }
    }
}