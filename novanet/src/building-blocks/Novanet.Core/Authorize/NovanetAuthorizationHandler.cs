﻿using Novanet.Core.RedisCache;

namespace Novanet.Core.Authorize
{
    public class NovanetAuthorizationHandler : AuthorizationHandler<NovanetPolicyRequirement>
    {
        private readonly IGlobalCacheService _globalCacheService;

        public NovanetAuthorizationHandler(
            IGlobalCacheService globalCacheService)
        {
            _globalCacheService = globalCacheService;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
            NovanetPolicyRequirement requirement)
        {
            if (context.User.Identity is {IsAuthenticated: false})
            {
                context.Fail();
                return;
            }

            var policy = $"{requirement.Aggregate}:{requirement.Action}";
            var userId = context.User.Claims.FirstOrDefault(x => x.Type.Equals("id"))?.Value;
            var parsed = Guid.TryParse(userId, out _);
            if (!parsed) context.Fail();

            var identityUserValue = await _globalCacheService.GetAsync<IdentityUserValue>(
                RedisKeys.KeySettings<IdentityUserValue>(userId));
            if (identityUserValue.Roles?.Contains("Publisher") == true && !identityUserValue.IsActive)
            {
                context.Fail();
                return;
            }

            var accessed = identityUserValue.RoleClaims?.Any(x => x.Equals(policy));
            if (accessed == false)
            {
                context.Fail();
                return;
            }
            context.Succeed(requirement);
        }
    }
}