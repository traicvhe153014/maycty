﻿using System.Text.RegularExpressions;
using Microsoft.Extensions.Options;

namespace Novanet.Core.Authorize
{
    public class NovanetPolicyProvider : IAuthorizationPolicyProvider
    {
        private static readonly Regex Pattern = new Regex(@"^Novanet|([^|]+)|([^|]+)$");

        public DefaultAuthorizationPolicyProvider FallbackPolicyProvider { get; }

        public NovanetPolicyProvider(IOptions<AuthorizationOptions> options)
        {
            FallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
        {
            return FallbackPolicyProvider.GetDefaultPolicyAsync();
        }

        public Task<AuthorizationPolicy> GetFallbackPolicyAsync()
        {
            return FallbackPolicyProvider.GetFallbackPolicyAsync();
        }

        public async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            var match = Pattern.Matches(policyName);

            if (!match.Any() && match.Count != 3)
            {
                return await FallbackPolicyProvider.GetPolicyAsync(policyName);
            }

            var aggregate = match[1].Groups[0].ToString();
            var action = match[2].Groups[0].ToString();

            var policy = new AuthorizationPolicyBuilder();
            policy.AddRequirements(new NovanetPolicyRequirement
            {
                Aggregate = aggregate,
                Action = action
            });
            await Task.CompletedTask;
            return policy.Build();
        }
    }
}