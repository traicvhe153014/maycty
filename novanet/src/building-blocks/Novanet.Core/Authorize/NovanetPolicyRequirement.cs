﻿namespace Novanet.Core.Authorize
{
    public class NovanetPolicyRequirement : IAuthorizationRequirement
    {
        public string Aggregate { get; set; }

        public string Action { get; set; }
    }
}