﻿namespace Novanet.Core.Authorize;

public class NovanetAccessControlAttribute : AuthorizeAttribute
{
    private const string PolicyPrefix = "Novanet";

    private string _aggregate;
    private string _action;

    public NovanetAccessControlAttribute()
    {
        AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
    }

    public NovanetAccessControlAttribute(string aggregate, string action) : this()
    {
        Aggregate = aggregate;
        Action = action;
    }

    public string Aggregate
    {
        get => _aggregate;
        set
        {
            _aggregate = value;
            UpdatePolicy();
        }
    }

    public string Action
    {
        get => _action;
        set
        {
            _action = value;
            UpdatePolicy();
        }
    }

    private void UpdatePolicy()
    {
        Policy = $"{PolicyPrefix}|{_aggregate}|{_action}";
    }
}