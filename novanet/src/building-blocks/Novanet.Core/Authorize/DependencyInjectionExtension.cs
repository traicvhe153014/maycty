﻿namespace Novanet.Core.Authorize;

public static class DependencyInjectionExtension
{
    public static void AddApplicationAuthorize(this IServiceCollection services, AppSettings appSettings)
    {
        services.AddSingleton<IAuthorizationPolicyProvider, NovanetPolicyProvider>();
        services.AddSingleton<IAuthorizationHandler, NovanetAuthorizationHandler>();

        services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = appSettings.TokenOption.Issuer,
                    ValidAudiences = appSettings.TokenOption.AudienceList,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(appSettings.TokenOption.ServerSigningPassword)),
                    ClockSkew = TimeSpan.Zero
                };
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var path = context.HttpContext.Request.Path;
                        if (!path.StartsWithSegments("/hub") ||
                            !context.Request.Query.TryGetValue("access_token", out var token))
                            return Task.CompletedTask;
                        context.Token = token;
                        context.Request.Headers.Authorization = token;

                        return Task.CompletedTask;
                    }
                };
            });
    }
}