﻿using Novanet.Core.Domain;
using Novanet.Core.Extensions;

namespace Novanet.Core.DataContext;

public class NovanetContext<TDbContext> : DbContext where TDbContext : DbContext
{
    public NovanetContext(DbContextOptions<TDbContext> options, AppSettings appSettings)
        : base(options)
    {
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        OverrideFulltextSearch();
        return base.SaveChangesAsync(cancellationToken);
    }

    public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
        CancellationToken cancellationToken = new())
    {
        OverrideFulltextSearch();
        return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }

    private void OverrideFulltextSearch()
    {
        var modified = ChangeTracker.Entries()
            .Where(e => e.State is EntityState.Modified or EntityState.Added);
        foreach (var item in modified)
        {
            if (item.Entity is not NovanetDocument changedOrAddedItem) continue;
            changedOrAddedItem.Search = item.Entity.FullTextSearch();
        }
    }
}