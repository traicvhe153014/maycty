﻿using System.ComponentModel.DataAnnotations;
using Novanet.Core.Extensions;

namespace Novanet.Core.Domain;

public class NovanetDocument : INovanetDocument
{
    [Key] public Guid Id { get; set; } = Guid.NewGuid();

    public string Search { get; set; } = default!;

    public long SubId { get; set; }

    public Guid? CreatedBy { get; set; }
    
    public Guid? ModifiedBy { get; set; }

    public virtual void ModelCreating<T>(ModelBuilder modelBuilder, string schema) where T : NovanetDocument
    {
        var sequenceName = typeof(T).FullName.CreateMd5("Sequence");
        modelBuilder.HasSequence<int>(sequenceName);
        modelBuilder.Entity<T>().Property(o => o.SubId)
            .HasDefaultValueSql($"NEXT VALUE FOR [{schema}].{sequenceName}");
        modelBuilder.Entity<T>().HasIndex(x => x.SubId);
    }
}