﻿namespace Novanet.Core.Domain;

public interface INovanetDocument
{
    Guid Id { get; set; }

    public long SubId { get; set; }
}