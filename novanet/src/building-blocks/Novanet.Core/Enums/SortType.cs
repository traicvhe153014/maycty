﻿namespace Novanet.Core.Enums;

public enum SortType
{
    Ascending = 1,
    Descending = 2
}