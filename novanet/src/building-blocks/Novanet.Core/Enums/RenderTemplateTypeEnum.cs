﻿namespace Novanet.Core.Enums;

public enum RenderTemplateTypeEnum
{
    HtmlTemplate = 1,
    DefaultBanner = 2,
    Backup = 3
}