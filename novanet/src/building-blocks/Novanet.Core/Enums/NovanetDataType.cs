﻿namespace Novanet.Core.Enums;

public enum NovanetDataType
{
    String = 1,
    Number = 2,
    Double = 3,
    DateTime = 4,
    Boolean = 5,
    Url = 6
}