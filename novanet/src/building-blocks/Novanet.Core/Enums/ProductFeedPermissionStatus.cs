﻿namespace Novanet.Core.Enums;

public enum ProductFeedPermissionStatus
{
    Granted = 1,
    Processing = 2,
    CannotAccess = 3,
    Denied = 4
}