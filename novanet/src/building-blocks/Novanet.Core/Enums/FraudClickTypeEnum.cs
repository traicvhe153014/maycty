﻿namespace Novanet.Core.Enums;

public enum FraudClickTypeEnum
{
     RealTime = 0,
     ClientIdClickRates = 1,
     IP = 2,
     DoubleClick = 3,
     CreateTimeClientId = 4,
     OutOfTimeSpan = 5,
     VNProvinceNotConfig = 6,
     MaxCtr = 7,
     CountryNotConfig = 8,
     PercentCutNovanet = 9
}