namespace Novanet.Core.Enums;

public enum UserStatus
{
    Running = 1,
    Disabled = 2
}