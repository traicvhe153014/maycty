﻿namespace Novanet.Core.Enums;

public enum ConditionType
{
    Equal = 1,
    Contains = 2,
    And = 3,
    Or = 4
}