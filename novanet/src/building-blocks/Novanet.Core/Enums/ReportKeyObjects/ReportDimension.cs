﻿namespace Novanet.Core.Enums.ReportKeyObjects;

public enum ReportAdvertiserDimension 
{
    Advertiser = 1,
    Campaign = 2,
    AdvertisingSet = 3,
    Advertising = 4,
    ProductFeed = 5,
    ProductGroup = 6,
    Product = 7,
    AdvertisingSetProductGroup = 14,
    AdvertisingTemplate = 16,
}

public enum ReportPublisherDimension 
{
    Publisher = 8,
    Website = 9,
    Zone = 10
}

public enum ReportTargetingDimension 
{
    Location = 11,
    DayOfWeek = 12,
    HourOfDay = 13
}

public enum ReportClientDimension
{
    ClientId = 15
}