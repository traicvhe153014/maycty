﻿namespace Novanet.Core.Enums.ReportKeyObjects;

public enum ReportDimensionPrefix
{
    Advertiser = 1,
    Publisher = 2,
    Targeting = 3,
    Format = 4
}