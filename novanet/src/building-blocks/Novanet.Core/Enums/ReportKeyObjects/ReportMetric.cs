﻿namespace Novanet.Core.Enums.ReportKeyObjects;

public enum ReportMetric
{
    View = 1,
    Users = 2,
    Click = 3,
    Cost = 4,
    Revenue = 5,
    Conversion = 6,
    PageView = 7,
    AddToCart = 8,
    RemoveCart = 9,
    BeginCheckout = 10,
    PaymentInfo = 11,
    Purchase = 12,

    // money
    Earned = 13,
    PromotionEarned = 14,
    Paid = 15,
    PromotionPaid = 16,
    GetBack = 17,
    PromotionGetBack = 18,
    Refund = 19,
    PromotionRefund = 20,
    
    // metric
    BannerClick = 21,
    FraudClick = 22,
    DefaultBannerView = 23,
    BackupView = 24,
    
    // video metric
    Video25Percent = 25,
    Video50Percent = 26,
    Video75Percent = 27,
    Video100Percent = 28,
    VideoView3s = 29
}