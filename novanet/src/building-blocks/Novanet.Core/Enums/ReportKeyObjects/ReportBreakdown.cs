﻿namespace Novanet.Core.Enums.ReportKeyObjects;

public enum ReportBreakdown
{
    Total = 1,
    Day = 2,
    Hour = 3,
    Month = 4,
    Year = 5
}