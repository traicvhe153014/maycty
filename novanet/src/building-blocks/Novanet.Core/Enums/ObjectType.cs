﻿namespace Novanet.Core.Enums;

public enum ObjectType
{
    OrderPrice = 1,
    PurchaseFrequency = 2,
    PurchasedWithin = 3,
    RemovedFromCart = 4,
    SelectedNotCompleted = 5,
    ViewedNotAddToCart = 6,
    ViewedNotPurchased = 7,
    AddedNotPurchased = 8,
    All = 9
}