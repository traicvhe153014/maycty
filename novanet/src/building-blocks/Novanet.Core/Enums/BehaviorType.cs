﻿namespace Novanet.Core.Enums;

public enum BehaviorType
{
    ViewProduct = 1,
    AddToCart = 2,
    Checkout = 3,
    Purchases = 4,
    All = 5
}