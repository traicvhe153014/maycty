﻿namespace Novanet.Core.Enums;

public enum ObjectGroupProductType
{
    Product = 1,
    ProductGroup = 2
}