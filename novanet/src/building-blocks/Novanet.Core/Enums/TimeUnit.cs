﻿namespace Novanet.Core.Enums;

public enum TimeUnit
{
    Month = 1,
    Year = 2
}