﻿namespace Novanet.Core.Enums;

public enum UrlConditionType
{
    Equal = 0,
    Contains = 1
}