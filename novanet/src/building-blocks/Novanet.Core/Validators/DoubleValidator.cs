﻿namespace Novanet.Core.Validators;

public static class DoubleValidator
{
    public static IRuleBuilderOptions<T, string> IsDouble<T>(this IRuleBuilder<T, string> ruleBuilder)
    {
        return ruleBuilder.Must(x => double.TryParse(x, out _))
            .WithMessage("'{PropertyName}' is not a Double Data Type");
    }
}