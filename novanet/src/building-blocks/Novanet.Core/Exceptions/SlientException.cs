namespace Novanet.Core.Exceptions;

public class SlientException : Exception
{
    public SlientException(string message) : base(message)
    {
        Status = StatusCodes.Status400BadRequest;
    }

    public SlientException(int status)
    {
        Status = status;
    }

    public SlientException(string message, int status) : base(message)
    {
        Status = status;
    }

    public SlientException(string message, int status, Exception innerException)
        : base(message, innerException)
    {
        Status = status;
    }

    public int Status { get; set; }
}