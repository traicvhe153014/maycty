﻿namespace Novanet.Core.Models;

public class ResponseData
{
    public HttpStatusCode StatusCode;
    public string Url;
}