﻿namespace Novanet.Core.Models;

public class MappingAttribute
{
    public string DisplayName { get; set; }

    public string Attribute { get; set; }

    public List<string> Mappings { get; set; }

    public List<string> SupportedValues { get; set; }
}