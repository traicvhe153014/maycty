namespace Novanet.Core.Models;

public class NovanetResponse
{
    public NovanetResponse()
    {

    }

    public NovanetResponse(int status, bool success, params string[] errors)
    {
        Status = status;
        Success = success;
        Errors = errors?.Length > 0 ? errors : null;
    }

    public bool Success { get; set; }
    public int Status { get; set; }
    public string[] Errors { get; set; }
}

public class NovanetResponse<T> : NovanetResponse
{
    public T Data { get; set; }
}

public class NovanetPagedResponse<T> : NovanetResponse, IPaged
{
    public int Page { get; set; }
    public int PageSize { get; set; }
    public long TotalRecords { get; set; }
    public long TotalPage => (TotalRecords - 1) / PageSize + 1;
    public bool HasNextPage => Page < TotalPage;
    public bool HasPreviousPage => Page > 1;
    public List<T> Data { get; set; }
}