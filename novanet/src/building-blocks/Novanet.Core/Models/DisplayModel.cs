﻿namespace Novanet.Core.Models;

public class DisplayAbstract
{
    public string Key { get; set; }

    public string[] Value { get; set; }
}

public class DisplayModel
{
    public string Zone { get; set; }

    public string Width { get; set; }

    public string Height { get; set; }

    public string Timestamp { get; set; }

    public string Referer { get; set; }

    public string Url { get; set; }

    public string Host { get; set; }
}