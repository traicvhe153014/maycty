﻿namespace Novanet.Core.Models
{
    public class TokenOption
    {
        public string ServerSigningPassword { get; set; }

        public double AccessTokenDurationInMinutes { get; set; }

        public int RefreshTokenDurationInDays { get; set; }

        public string Issuer { get; set; }

        public List<string> AudienceList { get; set; }
    }
}