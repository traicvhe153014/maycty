﻿using Novanet.Core.Domain;
using Novanet.Core.Enums;

#nullable enable

namespace Novanet.Core.Models;

public class IdentityUserValue : INovanetDocument
{
    public Guid Id { get; set; }
    
    public long SubId { get; set; }

    public string Email { get; set; } = default!;

    public string FullName { get; set; } = default!;

    public string PasswordHash { get; set; } = default!;

    public string UserName { get; set; } = default!;

    public List<string>? Roles { get; set; }
    
    public List<string>? RoleClaims { get; set; }

    public List<string>? UserClaims { get; set; } = default!;
    
    public double Amount { get; set; }
    
    public UserStatus? Status { get; set; }
    
    public string? PhoneNumber { get; set; }
    
    public bool IsActive { get; set; }
}

// public enum UserStatus
// {
//     Running = 1,
//     Disabled = 2
// }