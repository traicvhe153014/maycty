﻿using Newtonsoft.Json;

namespace Novanet.Core.Models;

public class UserClaimsValue
{
    [JsonProperty("id")]
    public Guid? Id { get; set; }

    [JsonProperty("fullname")]
    public string FullName { get; set; }

    [JsonProperty("username")]
    public string UserName { get; set; }

    [JsonProperty("email")]
    public string Email { get; set; }

    [JsonProperty("policies")]
    public string[] Policies { get; set; }

    [JsonProperty("roles")]
    public string[] Roles { get; set; }

    public bool IsAdmin => Roles.Contains("Admin");
    
    public bool IsPublisher => Roles.Contains("Publisher");
    
    public bool IsPublisherManager => Roles.Contains("PublisherManager");
}