﻿namespace Novanet.Core.Models
{
    public class VirtualPathConfig
    {
        public string RealPath { get; set; }

        public string RequestPath { get; set; }

        public string Alias { get; set; }
    }
}
