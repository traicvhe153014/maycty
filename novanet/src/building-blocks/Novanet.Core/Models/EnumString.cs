﻿namespace Novanet.Core.Models;

public class EnumString
{
    protected EnumString(string value)
    {
        Value = value;
    }
    private string Value { get; }
    
    public static implicit operator string(EnumString item) { return item.Value; }

    public override string ToString()
    {
        return Value;
    }
}