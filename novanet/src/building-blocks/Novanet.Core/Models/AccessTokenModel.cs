﻿namespace Novanet.Core.Models;

public class AccessTokenModel
{
    public string AccessToken { get; set; }

    public DateTime Expires { get; set; }
}