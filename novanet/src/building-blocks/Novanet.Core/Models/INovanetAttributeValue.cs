﻿namespace Novanet.Core.Models;

public interface INovanetAttributeValue
{
    public string StringValue { get; set; }

    public DateTimeOffset? DateTimeValue { get; set; }

    public long? NumberValue { get; set; }

    public bool? BooleanValue { get; set; }

    public double? DoubleValue { get; set; }
}