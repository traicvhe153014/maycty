﻿namespace Novanet.Core.Models;

public class PredicateModel
{
    public string Field { get; set; }
    public PredicateOperatorEnum Operator { get; set; }
    public string Value { get; set; }
}

public class PredicateModel<TField,TValue>
{
    [JsonPropertyName("field")]
    public TField Field { get; set; }
    [JsonPropertyName("operator")]
    public PredicateOperatorEnum Operator { get; set; }
    [JsonPropertyName("value")]
    public TValue Value { get; set; }
}

public enum PredicateOperatorEnum
{
    Contains,
    NotContain,
    StartsWith,
    EndsWith,
    Equals
}