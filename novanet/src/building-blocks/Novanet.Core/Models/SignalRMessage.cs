﻿namespace Novanet.Core.Models
{
    public class SignalRMessage<T>
    {
        public string Chanel { get; set; }
        public T Data { get; set; }

        public SignalRMessage()
        {
        }

        public SignalRMessage(string channel, T data)
        {
            Chanel = channel;
            Data = data;
        }
    }
}