﻿namespace Novanet.Core.Models.MessageBusModels;

public class TransactionValidatorModel
{
    public Guid Id { get; set; }

    public double Amount { get; set; }
}