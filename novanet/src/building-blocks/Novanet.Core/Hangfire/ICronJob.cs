﻿using Hangfire;

namespace Novanet.Core.Hangfire;

[AutomaticRetry(Attempts = 0)]
public interface ICronJob
{
    Task<string> Run();
}