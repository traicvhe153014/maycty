﻿namespace Novanet.Core.Constants;

public static class ProductFeedConstants
{
    public static readonly List<MappingAttribute> RequiredAttributes = new()
    {
        new MappingAttribute
        {
            DisplayName = "Id",
            Attribute = "Id",
            Mappings = new List<string> {"ID", "Id", "id"}
        },
        new MappingAttribute
        {
            DisplayName = "Tiêu đề",
            Attribute = "Title",
            Mappings = new List<string> {"Title", "title", "tiêu đề", "Tiêu đề"}
        },
        new MappingAttribute
        {
            DisplayName = "Mô tả",
            Attribute = "Description",
            Mappings = new List<string> {"Description", "description", "mô tả", "nội dung mô tả"}
        },
        new MappingAttribute
        {
            DisplayName = "Liên kết",
            Attribute = "Link",
            Mappings = new List<string> {"Link", "link", "liên kết", "đường liên kết"}
        },
        new MappingAttribute
        {
            DisplayName = "Liên kết hình ảnh",
            Attribute = "ImageLink",
            Mappings = new List<string> {"ImageLink", "imageLink", "image_link", "liên kết hình ảnh", "hình ảnh", "Liên kết hình ảnh", "Hình ảnh"}
        },
        new MappingAttribute
        {
            DisplayName = "Tình trạng còn hàng",
            Attribute = "Availability",
            Mappings = new List<string> {"Availability", "availability", "còn hàng", "Còn hàng"},
            SupportedValues = new List<string>
            {
                "Còn hàng", "còn hàng",
                "Hết hàng", "hết hàng",
                "Đặt hàng trước", "đặt hàng trước",
                "Giao sau", "giao sau"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Giá",
            Attribute = "Price",
            Mappings = new List<string> {"Price", "price", "giá", "Giá"}
        },
        new MappingAttribute
        {
            DisplayName = "Tình trạng",
            Attribute = "Condition",
            Mappings = new List<string>
                {"Condition", "condition", "tình trạng", "Tình trạng"},
            SupportedValues = new List<string>
                {"Mới", "mới", "Cũ", "cũ", "Đã được tân trang", "đã được tân trang", "Đã qua sử dụng", "đã qua sử dụng"}
        }
    };

    private static readonly List<MappingAttribute> NotRequiredAttributes = new()
    {
        new MappingAttribute
        {
            DisplayName = "Mã số linh kiện nhà sản xuất",
            Attribute = "Mpn",
            Mappings = new List<string> {"MPN", "Mpn", "mpn", "Mã số linh kiện của nhà sản xuất", "mã số linh kiện của nhà sản xuất"}
        },
        new MappingAttribute
        {
            DisplayName = "Lô sản phẩm",
            Attribute = "MultiPack",
            Mappings = new List<string> {"MultiPack", "multiPack", "multipack", "lô sản phẩm", "Lô sản phẩm"}
        },
        new MappingAttribute
        {
            DisplayName = "Gói",
            Attribute = "IsBundle",
            Mappings = new List<string> {"IsBundle", "isBundle", "is_bundle", "gói", "Gói"}
        },
        new MappingAttribute
        {
            DisplayName = "Nhóm tuổi",
            Attribute = "AgeGroup",
            Mappings = new List<string> {"AgeGroup", "ageGroup", "age_group", "nhóm tuổi", "Nhóm tuổi"}
        },
        new MappingAttribute
        {
            DisplayName = "Màu sắc",
            Attribute = "Color",
            Mappings = new List<string> {"Color", "color", "màu sắc", "Màu sắc"}
        },
        new MappingAttribute
        {
            DisplayName = "Giới tính",
            Attribute = "Gender",
            Mappings = new List<string> {"Gender", "gender", "giới tính", "Giới tính"}
        },
        new MappingAttribute
        {
            DisplayName = "Chất liệu",
            Attribute = "Material",
            Mappings = new List<string> {"Material", "material", "chất liệu", "Chất liệu"}
        },
        new MappingAttribute
        {
            DisplayName = "Họa tiết",
            Attribute = "Pattern",
            Mappings = new List<string> {"Pattern", "pattern", "họa tiết", "Họa tiết", "Hoa văn", "hoa văn"}
        },
        new MappingAttribute
        {
            DisplayName = "Kích thước",
            Attribute = "Size",
            Mappings = new List<string> {"Size", "size", "kích thước", "Kích thước", "kích cỡ", "Kích cỡ"}
        },
        new MappingAttribute
        {
            DisplayName = "Mã nhóm mặt hàng",
            Attribute = "ItemGroupId",
            Mappings = new List<string> {"ItemGroupId", "itemGroupId", "item_group_id", "mã nhóm mặt hàng", "Mã nhóm mặt hàng", "Nhóm mặt hàng", "nhóm mặt hàng"}
        },
        new MappingAttribute
        {
            DisplayName = "Phí vận chuyển",
            Attribute = "Shipping",
            Mappings = new List<string> {"Shipping", "shipping", "Phí vận chuyển", "phí vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Thuế",
            Attribute = "Tax",
            Mappings = new List<string> {"Tax", "tax", "thuế", "Thuế"}
        },
        new MappingAttribute
        {
            DisplayName = "Chiều dài sản phẩm",
            Attribute = "ProductLength",
            Mappings = new List<string> {"ProductLength", "Chiều dài sản phẩm", "chiều dài sản phẩm"}
        },
        new MappingAttribute
        {
            DisplayName = "Mã số sản phẩm thương mại toàn cầu",
            Attribute = "Gtin",
            Mappings = new List<string>
            {
                "Gtin", "gtin",
                "Mã số sản phẩm thương mại toàn cầu", 
                "mã số sản phẩm thương mại toàn cầu", 
                "Mã số sản phẩm thương mại toàn cầu (Gtin)",
                "mã số sản phẩm thương mại toàn cầu (gtin)"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Đường liên kết dành cho thiết bị di động",
            Attribute = "MobileLink",
            Mappings = new List<string>
                {"MobileLink", "mobilelink", "mobile_link", "đường liên kết dành cho thiết bị di động", "Đường liên kết dành cho thiết bị di động"}
        },
        new MappingAttribute
        {
            DisplayName = "Có mã nhận dạng",
            Attribute = "IdentifierExists",
            Mappings = new List<string>
                {"IdentifierExists", "identifierexists", "identifier_exists", "có mã nhận dạng", "Có mã nhận dạng"}
        },
        new MappingAttribute
        {
            DisplayName = "Có mã nhận dạng",
            Attribute = "ShipsFromCountry",
            Mappings = new List<string>
                {"IdentifierExists", "identifierexists", "identifier_exists", "có mã nhận dạng", "Có mã nhận dạng"}
        },
        new MappingAttribute
        {
            DisplayName = "Quốc gia gửi hàng",
            Attribute = "ShipsFromCountry",
            Mappings = new List<string>
                {"ShipsFromCountry", "shipsfromcountry", "ships_from_country", "quốc gia gửi hàng", "Quốc gia gửi hàng"}
        },
        new MappingAttribute
        {
            DisplayName = "Quốc gia gửi hàng",
            Attribute = "ProductDetail",
            Mappings = new List<string>
                {"ProductDetail", "productdetail", "product_detail]", "quốc gia gửi hàng", "Quốc gia gửi hàng"}
        },
        new MappingAttribute
        {
            DisplayName = "Người lớn",
            Attribute = "Adult",
            Mappings = new List<string>
                {"Adult", "adult", "adult", "người lớn", "Người lớn"}
        },
        new MappingAttribute
        {
            DisplayName = "Vị trí được phép xuất hiện",
            Attribute = "IncludedDestination",
            Mappings = new List<string>
                {"IncludedDestination", "includeddestination", "included_destination", "vị trí được phép xuất hiện", "Vị trí được phép xuất hiện"}
        },
        new MappingAttribute
        {
            DisplayName = "Quốc gia bị loại trừ khỏi Quảng cáo mua sắm",
            Attribute = "ShoppingAdsExcludedCountry",
            Mappings = new List<string>
            {
                "ShoppingAdsExcludedCountry", "shoppingadsexcludedcountry", "shopping_ads_excluded_country",
                "quốc gia bị loại trừ khỏi Quảng cáo mua sắm"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Nhãn tùy chỉnh",
            Attribute = "CustomLabel04",
            Mappings = new List<string>
                {"CustomLabel04", "customlabel", "custom_label", "nhãn tùy chỉnh"}
        },
        new MappingAttribute
        {
            DisplayName = "Chiều dài vận chuyển",
            Attribute = "ShippingLength",
            Mappings = new List<string>
                {"ShippingLength", "shippinglength", "shipping_length", "chiều dài vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Chiều dài vận chuyển",
            Attribute = "ShippingLength",
            Mappings = new List<string>
                {"ShippingLength", "shippinglength", "shipping_length", "chiều dài vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Chiều cao vận chuyển",
            Attribute = "ProductHeight",
            Mappings = new List<string>
                {"ShippingLength", "shippinglength", "product_height", "chiều cao sản phẩm"}
        },
        new MappingAttribute
        {
            DisplayName = "Ngày hết hạn",
            Attribute = "ExpirationDate",
            Mappings = new List<string>
                {"ExpirationDate", "expirationdate", "expiration_date", "ngày hết hạn", "Ngày hết hạn"}
        },
        new MappingAttribute
        {
            DisplayName = "Ngày hết hạn",
            Attribute = "ExpirationDate",
            Mappings = new List<string>
                {"ExpirationDate", "expirationdate", "expiration_date", "ngày hết hạn", "Ngày hết hạn"}
        },
        new MappingAttribute
        {
            DisplayName = "Trọng lượng sản phẩm",
            Attribute = "ProductWeight",
            Mappings = new List<string>
                {"ProductWeight", "productweight", "product_weight", "trọng lượng sản phẩm"}
        },
        new MappingAttribute
        {
            DisplayName = "Chiều rộng sản phẩm",
            Attribute = "ProductWidth",
            Mappings = new List<string>
                {"ProductWidth", "productwidth", "product_width", "trọng lượng sản phẩm"}
        },
        new MappingAttribute
        {
            DisplayName = "Số lượng đo lường để định giá theo đơn vị",
            Attribute = "ProductWidth",
            Mappings = new List<string>
                {"ProductWidth", "productwidth", "unit_pricing_measure", "số lượng đo lường để định giá theo đơn vị"}
        },
        new MappingAttribute
        {
            DisplayName = "Số lượng đo lường để định giá theo đơn vị",
            Attribute = "ProductWidth",
            Mappings = new List<string>
                {"ProductWidth", "productwidth", "unit_pricing_measure", "số lượng đo lường để định giá theo đơn vị"}
        },
        new MappingAttribute
        {
            DisplayName = "Số lượng đo lường để định giá theo đơn vị",
            Attribute = "ProductWidth",
            Mappings = new List<string>
                {"ProductWidth", "productwidth", "unit_pricing_measure", "số lượng đo lường để định giá theo đơn vị"}
        },
        new MappingAttribute
        {
            DisplayName = "Nhãn thời gian vận chuyển",
            Attribute = "TransitTimeLabel",
            Mappings = new List<string>
                {"TransitTimeLabel", "transit_time_label", "transit_time_label", "nhãn thời gian vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Nhãn thời gian vận chuyển",
            Attribute = "TransitTimeLabel",
            Mappings = new List<string>
                {"TransitTimeLabel", "transit_time_label", "transit_time_label", "nhãn thời gian vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Cấp hiệu suất năng lượng",
            Attribute = "EnergyEfficiencyClass",
            Mappings = new List<string>
            {
                "EnergyEfficiencyClass", "energyffficiencyclass", "energy_efficiency_class", "cấp hiệu suất năng lượng"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Giá vốn hàng bán",
            Attribute = "CostOfGoodsSold",
            Mappings = new List<string>
                {"CostOfGoodsSold", "costofgoodssold", "cost_of_goods_sold", "giá vốn hàng bán"}
        },
        new MappingAttribute
        {
            DisplayName = "Giá vốn hàng bán",
            Attribute = "CostOfGoodsSold",
            Mappings = new List<string>
                {"CostOfGoodsSold", "costofgoodssold", "cost_of_goods_sold", "giá vốn hàng bán"}
        },
        new MappingAttribute
        {
            DisplayName = "Hệ thống kích thước",
            Attribute = "SizeSystem",
            Mappings = new List<string>
                {"SizeSystem", "sizesystem", "size_system", "hệ thống kích thước"}
        },
        new MappingAttribute
        {
            DisplayName = "Trọng lượng vận chuyển",
            Attribute = "ShippingWeight",
            Mappings = new List<string>
                {"ShippingWeight", "shippingweight", "shipping_weight", "trọng lượng vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Cấp hiệu suất năng lượng tối thiểu",
            Attribute = "MinEnergyEfficiencyClass",
            Mappings = new List<string>
            {
                "MinEnergyEfficiencyClass", "minenergyefficiencyclass", "min_energy_efficiency_class",
                "trọng lượng vận chuyển"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Thương hiệu",
            Attribute = "Brand",
            Mappings = new List<string>
                {"Brand", "brand", "thương hiệu", "Thương hiệu", "nhãn hiệu", "Nhãn hiệu"}
        },
        new MappingAttribute
        {
            DisplayName = "Số lượng đo lường cơ sở để định giá theo đơn vị",
            Attribute = "UnitPricingBaseMeasure",
            Mappings = new List<string>
            {
                "UnitPricingBaseMeasure", "unitpricingbasemeasure", "unit_pricing_base_measure",
                "Số lượng đo lường cơ sở để định giá theo đơn vị"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Vị trí không được phép xuất hiện",
            Attribute = "UnitPricingBaseMeasure",
            Mappings = new List<string>
            {
                "ExcludedDestination", "excludeddestination", "excluded_destination", "vị trí không được phép xuất hiện"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Chiều cao vận chuyển",
            Attribute = "ShippingHeight",
            Mappings = new List<string>
                {"ShippingHeight", "shippingheight", "shipping_height", "chiều cao vận chuyển"}
        },
        new MappingAttribute
        {
            DisplayName = "Đường liên kết của hình ảnh bổ sung",
            Attribute = "AdditionalImageLink",
            Mappings = new List<string>
            {
                "AdditionalImageLink", "additionalimagelink", "additional_image_link",
                "đường liên kết của hình ảnh bổ sung",
                "Đường liên kết của hình ảnh bổ sung",
                "Hình ảnh bổ sung",
                "hình ảnh bổ sung",
                "liên kết hình ảnh bổ sung",
                "Liên kết hình ảnh bổ sung"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Ngày có hàng",
            Attribute = "AvailabilityDate",
            Mappings = new List<string>
            {
                "AvailabilityDate", "availabilitydate", "availability_date",
                "ngày có hàng"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Mã khuyến mãi",
            Attribute = "PromotionId",
            Mappings = new List<string>
            {
                "PromotionId", "promotionid", "promotion_id",
                "mã khuyến mãi"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Mã khuyến mãi",
            Attribute = "PromotionId",
            Mappings = new List<string>
            {
                "PromotionId", "promotionid", "promotion_id",
                "mã khuyến mãi"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Điểm khách hàng thân thiết",
            Attribute = "LoyaltyPoints",
            Mappings = new List<string>
            {
                "LoyaltyPoints", "loyaltypoints", "loyalty_points",
                "điểm khách hàng thân thiết"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Điểm khách hàng thân thiết",
            Attribute = "LoyaltyPoints",
            Mappings = new List<string>
            {
                "LoyaltyPoints", "loyaltypoints", "loyalty_points",
                "điểm khách hàng thân thiết"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Ngày giá ưu đãi có hiệu lực",
            Attribute = "SalePriceEffectiveDate",
            Mappings = new List<string>
            {
                "SalePriceEffectiveDate", "salepriceeffectivedate", "sale_price_effective_date",
                "ngày giá ưu đãi có hiệu lực"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Thời gian xử lý tối thiểu",
            Attribute = "MinHandlingTime",
            Mappings = new List<string>
            {
                "MinHandlingTime", "minhandlingtime", "sale_price_effective_date",
                "thời gian xử lý tối thiểu"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Nhãn vận chuyển",
            Attribute = "ShippingLabel",
            Mappings = new List<string>
            {
                "ShippingLabel", "shippinglabel", "shipping_label",
                "nhãn vận chuyển"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Nhãn vận chuyển",
            Attribute = "ShippingLabel",
            Mappings = new List<string>
            {
                "ShippingLabel", "shippinglabel", "shipping_label",
                "nhãn vận chuyển"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Thời gian xử lý tối đa",
            Attribute = "MaxHandlingTime",
            Mappings = new List<string>
            {
                "MaxHandlingTime", "maxhandlingtime", "max_handling_time",
                "nhãn vận chuyển"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Loại sản phẩm",
            Attribute = "ProductType",
            Mappings = new List<string>
            {
                "ProductType", "producttype", "product_type",
                "loại sản phẩm", "Loại sản phẩm"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Điểm nổi bật của sản phẩm",
            Attribute = "ProductHighlight",
            Mappings = new List<string>
            {
                "ProductHighlight", "producthighlight", "product_highlight",
                "điểm nổi bật của sản phẩm"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Trả góp",
            Attribute = "Installment",
            Mappings = new List<string>
            {
                "Installment", "installment",
                "trả góp"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Chiều rộng vận chuyển",
            Attribute = "ShippingWidth",
            Mappings = new List<string>
            {
                "ShippingWidth", "shippingwidth", "shipping_width",
                "chiều rộng vận chuyển"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Loại kích thước",
            Attribute = "SizeType",
            Mappings = new List<string>
            {
                "SizeType", "sizetype", "size_type",
                "loại kích thước"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Loại kích thước",
            Attribute = "SizeType",
            Mappings = new List<string>
            {
                "SizeType", "sizetype", "size_type",
                "loại kích thước"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Danh mục sản phẩm của Google",
            Attribute = "GoogleProductCategory",
            Mappings = new List<string>
            {
                "GoogleProductCategory", "googleproductcategory",
                "google_product_category", "danh mục sản phẩm của Google", "Danh mục sản phẩm", "danh mục sản phẩm"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Danh mục sản phẩm của Google",
            Attribute = "GoogleProductCategory",
            Mappings = new List<string>
            {
                "GoogleProductCategory", "googleproductcategory",
                "google_product_category", "danh mục sản phẩm của google"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Phí thuê bao",
            Attribute = "SubscriptionCost",
            Mappings = new List<string>
            {
                "SubscriptionCost", "googleproductcategory",
                "subscription_cost", "phí thuê bao"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Vị trí không được phép xuất hiện",
            Attribute = "ExcludedDestination",
            Mappings = new List<string>
            {
                "ExcludedDestination", "excludeddestination",
                "excluded_destination", "vị trí không được phép xuất hiện"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Quảng cáo chuyển hướng",
            Attribute = "AdsRedirect",
            Mappings = new List<string>
            {
                "AdsRedirect", "adsredirect",
                "ads_redirect", "quảng cáo chuyển hướng"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Danh mục thuế",
            Attribute = "TaxCategory",
            Mappings = new List<string>
            {
                "TaxCategory", "taxcategory",
                "tax_category", "danh mục thuế"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Số lượng đo lường để định giá theo đơn vị",
            Attribute = "UnitPricingMeasure",
            Mappings = new List<string>
            {
                "UnitPricingMeasure", "unitpricingmeasure",
                "unit_pricing_measure", "số lượng đo lường để định giá theo đơn vị"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Giá ưu đãi",
            Attribute = "SalePrice",
            Mappings = new List<string>
            {
                "SalePrice", "saleprice",
                "sale_price", "giá ưu đãi",
                "Giá ưu đãi"
            }
        },
        new MappingAttribute
        {
            DisplayName = "Trạng thái",
            Attribute = "IsActivated",
            Mappings = new List<string>
            {
                "Trạng thái", "status", "trạng thái", "Status", "IsActivated", "isActivated"
            }
        },
    };

    public static List<MappingAttribute> MappingAttributes()
    {
        var mapping = new List<MappingAttribute>();
        mapping.AddRange(RequiredAttributes);
        mapping.AddRange(NotRequiredAttributes);
        return mapping;
    }

}