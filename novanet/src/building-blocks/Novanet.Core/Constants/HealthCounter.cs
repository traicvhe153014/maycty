﻿namespace Novanet.Core.Constants;

public static class HealthCounter
{
    public static int HitToServer = 0;
    public static int HitToServerSuccess = 0;
    public static int HitToServerError = 0;
    public static int HitToServerDisplay = 0;
    public static int HitToServerDisplaySuccess = 0;
    public static int HitToServerDisplayError = 0;
    public static int HitToServerException = 0;
    public static int IncomingRequests = 0;
    public static int IncomingParallels = 0;
    public static int SuccessfulTrackings = 0;
    public static int FailedRequest = 0;
}