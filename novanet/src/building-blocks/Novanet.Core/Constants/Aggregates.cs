﻿namespace Novanet.Core.Constants;

public static class Aggregates
{
    public const string Identity = nameof(Identity);
    public const string Advertising = nameof(Advertising);
    public const string AdvertisingSet = nameof(AdvertisingSet);
    public const string Campaign = nameof(Campaign);
    public const string ObjectGroup = nameof(ObjectGroup);
    public const string Payment = nameof(Payment);
    public const string Product = nameof(Product);
    public const string ProductAttribute = nameof(ProductAttribute);
    public const string ProductAttributeValue = nameof(ProductAttributeValue);
    public const string ProductFeed = nameof(ProductFeed);
    public const string ProductGroup = nameof(ProductGroup);
    public const string Website = nameof(Website);
    public const string Zone = nameof(Zone);
    public const string WebsitePrice = nameof(WebsitePrice);
    public const string File = nameof(File);
    public const string Bucket = nameof(Bucket);
    public const string Report = nameof(Report);
    public const string Publisher = nameof(Publisher);
}