﻿using Novanet.Core.Enums.ReportKeyObjects;

namespace Novanet.Core.Constants;

public class ReportConstants
{
    public static readonly Dictionary<string, MetricExpression> MetricSorts = new()
    {
        // budget (ignored)
        {
            "dailyBudget", new MetricExpression
            {
                Ignored = true,
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalBudget", new MetricExpression
            {
                Ignored = true,
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid},
                MetricDuration = MetricDuration.Total
            }
        },
        // cost
        {
            "dailyCost", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalCost", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid},
                MetricDuration = MetricDuration.Total
            }
        },
        // impressions
        {
            "dailyImpressions", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.View},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalImpressions", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.View},
                MetricDuration = MetricDuration.Total
            }
        },
        // clicks
        {
            "dailyClicks", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Click},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalClicks", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Click},
                MetricDuration = MetricDuration.Total
            }
        },
        // ctr
        {
            "dailyCtr", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Divide,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Click, ReportMetric.View},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalCtr", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Divide,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Click, ReportMetric.View},
                MetricDuration = MetricDuration.Total
            }
        },
        // cpc
        {
            "dailyCpc", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Divide,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid, ReportMetric.Click},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalCpc", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Divide,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid, ReportMetric.Click},
                MetricDuration = MetricDuration.Total
            }
        },
        // purchase
        {
            "dailyPurchase", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Purchase},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalPurchase", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Purchase},
                MetricDuration = MetricDuration.Total
            }
        },
        // purchaseConversion
        {
            "dailyPurchaseConversion", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Purchase},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalPurchaseConversion", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Purchase},
                MetricDuration = MetricDuration.Total
            }
        },
        // cps
        {
            "dailyCps", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Divide,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid, ReportMetric.Purchase},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalCps", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Divide,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Paid, ReportMetric.Purchase},
                MetricDuration = MetricDuration.Total
            }
        },
        // video25Percent
        {
            "dailyVideo25Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video25Percent},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalVideo25Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video25Percent},
                MetricDuration = MetricDuration.Total
            }
        },
        // video50Percent
        {
            "dailyVideo50Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video50Percent},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalVideo50Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video50Percent},
                MetricDuration = MetricDuration.Total
            }
        },
        // video75Percent
        {
            "dailyVideo75Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video75Percent},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalVideo75Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video75Percent},
                MetricDuration = MetricDuration.Total
            }
        },
        // video100Percent
        {
            "dailyVideo100Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video100Percent},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalVideo100Percent", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.Video100Percent},
                MetricDuration = MetricDuration.Total
            }
        },
        // videoView3s
        {
            "dailyVideoView3s", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.VideoView3s},
                MetricDuration = MetricDuration.Daily
            }
        },
        {
            "totalVideoView3s", new MetricExpression
            {
                ExpressionType = MetricExpressionType.Equal,
                ReportMetrics = new List<ReportMetric> {ReportMetric.VideoView3s},
                MetricDuration = MetricDuration.Total
            }
        },
    };

    public static MetricExpression GetMetricExpression(string key)
    {
        var processedKey = key.ToLower();
        if (processedKey.StartsWith("daily") || processedKey.StartsWith("total"))
        {
            return MetricSorts.TryGetValue(key, out var result) ? result : null;
        }

        var totalKey = $"total{key[0].ToString().ToUpper()}{key.AsSpan(1)}";
        return MetricSorts.TryGetValue(totalKey, out var resultTotal) ? resultTotal : null;
    }
}

public class MetricExpression
{
    public bool Ignored { get; set; }
    public MetricExpressionType ExpressionType { get; set; }
    public List<ReportMetric> ReportMetrics { get; set; }
    public MetricDuration MetricDuration { get; set; }
}

public enum MetricExpressionType
{
    Equal = 1,
    Add = 2,
    Subtract = 3,
    Multiple = 4,
    Divide = 5
}

public enum MetricDuration
{
    Daily = 1,
    Total = 2
}