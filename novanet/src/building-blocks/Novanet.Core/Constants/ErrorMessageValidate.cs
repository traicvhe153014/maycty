﻿namespace Novanet.Core.Constants;

public static class ErrorMessageValidate
{
    public const string ValidateMaxLength = "Trường dữ liệu {0} đã vượt quá độ dài cho phép.";

    public const string ValidateCouldNotParse = "Trường dữ liệu {0} có giá trị không đúng kiểu dữ liệu {1}.";
    
    public const string ValidateCouldNotParseRequired = "Trường dữ liệu bắt buộc {0} có giá trị không đúng kiểu dữ liệu {1}.";

    public const string ValidateMissingAttribute = "Dữ liệu trong nguồn thiếu thuộc tính bắt buộc {0}.";

    public const string ValidateUnauthorized = "URL nguồn dữ liệu không truy cập được vì đã bị hủy cấp quyền.";
    
    public const string ValidateMalformedUrl = "Trường dữ liệu {0} không đúng định dạng URL";
    
    public const string ValidateCannotBlank = "Trường dữ liệu {0} không được để trống";
    
    public const string ValidateDuplicatedProduct = "Sản phẩm bị trùng các thuộc tính “Product ID” và “Product Title” và “Nội dung mô tả” và “Đường liên kết” và “Đường liên kết của hình ảnh”.";

    public const string ValidateValueCouldNotSupport = "Trường dữ liệu {0} có giá trị không được hỗ trợ. Danh sách giá trị hỗ trợ ({1})";
    
    public const string ValidateUnknownErrorOccurs = "Xuất hiện lỗi không xác định";

    public const string PasswordNotFound = "Mật khẩu không chính xác vui lòng kiểm tra lại.";

    public const string EmailNotFound = "Email bị khoá hoặc không tồn tại vui lòng kiểm tra lại.";

    public const string PasswordRequiresLower = "Mật khẩu phải có ít nhất một chữ thường ('a' - 'z').";

    public const string FileNotFound = "Không tìm thấy tệp tin";

    public const string DeactivatedUser =
        "Tài khoản của bạn đã bị khóa, vui lòng liên hệ tới Quản lý Publisher và thử lại!";
    
    public static class ValidateDuplicateColumn
    {
        public const string ValidateDuplicateColumnId = "An item with the same key has already been added. Key: Id";
        public const string ValidateDuplicateColumnIdVi = "Cột dữ liệu 'Id' xuất hiện nhiều hơn 1 lần";
    }
}