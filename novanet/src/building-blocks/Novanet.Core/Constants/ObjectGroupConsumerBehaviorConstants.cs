﻿using Novanet.Core.Enums;

namespace Novanet.Core.Constants;

public static class ObjectGroupConsumerBehaviorConstants
{
    public static readonly List<BehaviorType> BehaviorTypes = Enum.GetValues(typeof(BehaviorType))
        .Cast<BehaviorType>()
        .Where(x => x != BehaviorType.All)
        .ToList();

    public static readonly Dictionary<BehaviorType, EventType> MappingEventTypes = new()
    {
        {BehaviorType.ViewProduct, EventType.ProductView},
        {BehaviorType.AddToCart, EventType.AddToCart},
        {BehaviorType.Checkout, EventType.Purchase},
        {BehaviorType.Purchases, EventType.PurchaseComplete}
    };
}