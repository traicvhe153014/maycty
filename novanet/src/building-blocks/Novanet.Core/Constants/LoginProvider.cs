namespace Novanet.Core.Constants;

public static class LoginProvider
{
    public static string Jwt { get; set; } = nameof(Jwt);

    public static string Google { get; set; } = nameof(Google);
}