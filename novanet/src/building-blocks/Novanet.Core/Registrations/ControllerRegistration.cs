namespace Novanet.Core.Registrations;

public static class ControllerRegistration
{
    public static IServiceCollection RegisterController(this IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddPolicy("AllowAllOrigins", x => x
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
                .SetIsOriginAllowed(_ => true));
        });
        services.AddControllers().AddJsonOptions(opt =>
            {
                opt.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            })
            .AddValidationBehavior();

        return services;
    }
}

public static class ControllerRegistrationExtension
{
    public static void AddValidationBehavior(this IMvcBuilder builder)
    {
        builder
            .AddFluentValidation()
            .ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = c =>
                {
                    var errors = c.ModelState.Values.Where(v => v.Errors.Count > 0)
                        .SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage)
                        .ToArray();
                    return new BadRequestObjectResult(new NovanetResponse(400, false, errors));
                };
            });
    }
}