using Novanet.Core.RedisCache;

namespace Novanet.Core.Registrations;

public static class RedisRegistration
{
    public static void RegisterRedis(this IServiceCollection services, AppSettings appSettings)
    {
        if (appSettings.GlobalCache != null)
            services.AddSingleton<IGlobalCacheService>(new RedisCacheService(appSettings.GlobalCache.Url));

        if (appSettings.RedisTracking != null)
            services.AddSingleton<IRedisCacheService>(new RedisCacheService(appSettings.RedisTracking.Url));

        if (appSettings.RedisPubSub != null)
            services.AddSingleton<IRedisPubSubService, RedisPubSubService>();
            
        if (appSettings.RedisQueue != null)
            services.AddSingleton<IRedisQueueService>(new RedisCacheService(appSettings.RedisQueue.Url));
    }
}