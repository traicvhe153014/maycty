﻿namespace Novanet.Core.Mapper
{
    public interface IMap
    {
        void Mapping(Profile profile);
    }
}
