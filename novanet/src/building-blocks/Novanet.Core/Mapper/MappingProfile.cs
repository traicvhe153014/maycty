﻿namespace Novanet.Core.Mapper
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {

        }

        public MappingProfile(params Assembly[] assemblies)
        {
            var types = assemblies.SelectMany(x => x.GetExportedTypes()).ToList();
            ApplyMappingsFromIMap(types);
            ApplyMappingsFromIMapFrom(types, typeof(IMapFrom<>), false);
        }

        private void ApplyMappingsFromIMap(IEnumerable<Type> types)
        {
            var instances = types
                .Where(t => typeof(IMap).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
                .Select(Activator.CreateInstance)
                .Cast<IMap>();

            foreach (var instance in instances)
            {
                instance.Mapping(this);
            }
        }

        private void ApplyMappingsFromIMapFrom(IEnumerable<Type> types, Type fromType, bool reverseMap)
        {
            foreach (var type in types)
            {
                foreach (var imapType in type.GetInterfaces().Where(t => t.IsGenericType && t.GetGenericTypeDefinition() == fromType))
                {
                    var sourceType = imapType.GetGenericArguments()[0];
                    if (reverseMap)
                    {
                        CreateMap(sourceType, type).ReverseMap();
                    }
                    else
                    {
                        CreateMap(sourceType, type);
                    }
                }
            }
        }
    }
}
