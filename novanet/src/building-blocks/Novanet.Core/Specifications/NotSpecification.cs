﻿using Novanet.Core.Extensions.LinqExtensions;

namespace Novanet.Core.Specifications
{
    public class NotSpecification<T> : Specification<T>
    {
        public NotSpecification(ISpecification<T> spec) : base(spec.Predicate.Not())
        {
        }

    }
}
