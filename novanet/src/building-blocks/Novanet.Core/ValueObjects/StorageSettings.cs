﻿namespace Novanet.Core.ValueObjects;

public class StorageSettings
{
    public string Saved { get; set; }
        
    public string Bucket { get; set; }
}