namespace Novanet.Core.ValueObjects;

public class ElasticsearchSettings
{
    public string Url { get; set; }
    
    public string Index { get; set; }
    
    public string UserName { get; set; }
    
    public string Password { get; set; }
}