﻿namespace Novanet.Core.ValueObjects;

public class MongoSettings
{
    public string Url { get; set; } = default!;

    public string Database { get; set; } = default!;
}