using Microsoft.AspNetCore.Server.Kestrel.Core;
using Novanet.Core.Hangfire;
using Novanet.Core.Services;
using Novanet.EventBus;

namespace Novanet.Core.ValueObjects;

public class AppSettings
{
    public MailSettings Smtp { get; set; }
    
    public MetaData MetaData { get; set; } = default!;

    public ConnectionStrings ConnectionStrings { get; set; } = default!;

    public RedisSettings RedisTracking { get; set; }
    
    public RedisSettings GlobalCache { get; set; }
    
    public RedisSettings RedisPubSub { get; set; }

    public RedisSettings RedisQueue { get; set; }

    public RabbitMQSettings RabbitMQ { get; set; } = default!;

    public TokenOption TokenOption { get; set; }

    public string GoogleSheetCredentials { get; set; }

    public StorageSettings StorageSettings { get; set; }

    public MongoSettings MongoShortTime { get; set; }
    
    public MongoSettings MongoLongTime { get; set; }
    
    public MongoSettings MongoConversion { get; set; }

    public List<ScheduledTask> ScheduledTasks { get; set; }

    public Configurations Configurations { get; set; }

    public Hangfire Hangfire { get; set; }

    public string RootUrl { get; set; }
    
    public string WebsiteUrl { get; set; }
    
    public GoogleAnalyticsConfig GoogleAnalytics { get; set; }

    public int Data { get; set; }

    public List<HealthUrl> HealthUrls { get; set; }

    public VideoQueueConfigs VideoQueueConfigs { get; set; }
    
    public string CacheRootPath { get; set; }
    
    public GrpcEndpointConfigs GrpcEndpoints { get; set; }
    
    public KestrelConfiguration Kestrel { get; set; }

}

public class ConnectionStrings
{
    public string DefaultConnection { get; set; } = default!;
}

public class MetaData
{
    public string Name { get; set; }

    public string Title { get; set; }

    public string Version { get; set; }
}

public class Hangfire
{
    public string UserName { get; set; }

    public string Password { get; set; }
}

public class Configurations
{
    public string GoogleSheetSourcePath { get; set; }
}

public class GoogleAnalyticsConfig
{
    public string Property { get; set; }
}

public class HealthUrl
{
    public string ServiceName { get; set; }

    public string Url { get; set; }

    public bool Error { get; set; }

    public DateTimeOffset ErrorTime { get; set; }
}

public class VideoQueueConfigs
{
    public int QueueCapacity { get; set; }

    public int NumberOfProcessors { get; set; }
}

public class GrpcEndpointConfigs
{
    public string ServiceIdentity { get; set; }
    public string ServiceSettings { get; set; }
    public string ServiceSharing { get; set; }
    public string ServiceStorage { get; set; }
}

public class KestrelConfiguration
{
    public Dictionary<string, KestrelEndpointConfiguration> Endpoints { get; set; } 
}

public class KestrelEndpointConfiguration
{
    public string Url { get; set; }
    public string Protocols { get; set; }

}