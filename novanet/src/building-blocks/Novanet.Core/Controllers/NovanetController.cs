namespace Novanet.Core.Controllers;

[ApiController]
[Authorize]
public class NovanetController : ControllerBase
{
    private IMediator _mediator;
    protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetRequiredService<IMediator>();

    [NonAction]
    protected virtual ObjectResult Ok<T>(T value) where T : NovanetResponse
    {
        return value.Status == 204
            ? StatusCode(204, null)
            : StatusCode((int) value?.Status, value);
    }
}