#!/bin/sh

sudo kill -9 `sudo lsof -t -i:5001`
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5001

sudo pm2 kill
sudo pm2 start -n Service.Display.5101 "dotnet src/publish/Service.Display/Service.Display.dll --environment=prod --urls=http://0.0.0.0:5101/"
sudo pm2 start -n Service.Display.5102 "dotnet src/publish/Service.Display/Service.Display.dll --environment=prod --urls=http://0.0.0.0:5102/"
sudo pm2 start -n Service.Display.5103 "dotnet src/publish/Service.Display/Service.Display.dll --environment=prod --urls=http://0.0.0.0:5103/"

sudo pm2 start -n Service.Display.5104 "dotnet src/publish/Service.Display/Service.Display.dll --environment=prod --urls=http://0.0.0.0:5104/"
sudo pm2 start -n Service.Display.5105 "dotnet src/publish/Service.Display/Service.Display.dll --environment=prod --urls=http://0.0.0.0:5105/"
sudo pm2 start -n Service.Display.5106 "dotnet src/publish/Service.Display/Service.Display.dll --environment=prod --urls=http://0.0.0.0:5106/"