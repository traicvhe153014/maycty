#!/bin/sh

sudo rm -rf /var/www/html/*

cp -a "../publish/Client.Manager/." "/var/www/html"

sudo kill -9 `sudo lsof -t -i:5004`
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5004

sudo pm2 kill
sudo pm2 start Service.Report "dotnet src/publish/Service.Report/Service.Report.dll --environment=prod --urls=http://0.0.0.0:5004/"
