#!/bin/sh

sudo kill -9 `sudo lsof -t -i:5002`
sudo kill -9 `sudo lsof -t -i:5005`
sudo kill -9 `sudo lsof -t -i:5006`
sudo kill -9 `sudo lsof -t -i:5007`
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5002
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5005
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5006
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5007

sudo pm2 kill
sudo pm2 start -n Service.Identity "dotnet src/publish/Service.Identity/Service.Identity.dll --environment=prod --urls=http://0.0.0.0:5002/"
sudo pm2 start -n Service.Schedule "dotnet src/publish/Service.Schedule/Service.Schedule.dll --environment=prod --urls=http://0.0.0.0:5005/"
sudo pm2 start -n Service.Settings "dotnet src/publish/Service.Settings/Service.Settings.dll --environment=prod --urls=http://0.0.0.0:5006/"
sudo pm2 start -n Service.Sharing "dotnet src/publish/Service.Sharing/Service.Sharing.dll --environment=prod --urls=http://0.0.0.0:5007/"
