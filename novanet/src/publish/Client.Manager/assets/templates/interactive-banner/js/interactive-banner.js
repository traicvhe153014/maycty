const localData = {
    productName: !0,
    bannerImageUrl:
      'https://gamek.mediacdn.vn/133514250583805952/2021/11/15/ti1-1636961824231608526672.jpg',
    redirectUrl: '/xxxxx',
    promotion: !0,
    freeDelivery: !0,
    templateType: 0,
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 3 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 26e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  url = new URL(window.location.href),
  apiGetData = url.searchParams.get('api_get_data');

const AdvertisingSetting = 'AdvertisingSetting';
const data = JSON.parse(sessionStorage.getItem(AdvertisingSetting)).find(
  (x) => x.templateId === 2
);

function getData() {
  (document.body.style.background =
    'url(' + data.imageLink + ') no-repeat center'),
    renderItems(data);
}

async function getDataAsync() {
  const e = await fetch(dataFromApi);
  let r = await e.json();
  (document.body.style.background =
    'url(' + r.bannerImageUrl + ') no-repeat center'),
    console.log(window.location.href),
    renderItems(r);
}

function renderItems(e) {
  let r = '';
  for (let t = 0; t < 5; t++)
    r +=
      '<a target="_blank" href=' +
      e.products[t].redirectUrl +
      " class='swiper-slide'>\n        <div class='relative'>\n          <img src='" +
      e.products[t].productImageUrl +
      "'/>\n" +
      (e.freeDelivery ? "<div\nclass='absolute trapezoid-up'>\n</div>\n" : '') +
      (e.promotion
        ? "<div class='absolute trapezoid-down'>\n<span class='absolute sale-text'>sale " +
          formatNumber(getDiscount(e.products[t])) +
          '%</span>\n</div>\n'
        : '') +
      (e.freeDelivery
        ? "<span class='absolute free-delivery-text'>FREESHIP</span>\n"
        : '') +
      '        </div>\n        <div class="description">\n          <div class="title">\n' +
      (e.productName ? e.products[t].title : '') +
      '          </div>\n          <div class=\'detail-price\'>\n            <span class="price">' +
      (e.promotion
        ? formatNumber(e.products[t].discountedPrice)
        : formatNumber(e.products[t].price)) +
      'đ</span>\n' +
      (e.promotion
        ? '<span class="promotion">' +
          formatNumber(e.products[t].price) +
          'đ</span>\n'
        : '') +
      '          </div>\n        </div></a>';
  (document.getElementsByClassName('swiper-wrapper')[0].innerHTML = r),
    (document.getElementById('container').href = e.redirectUrl);
  new Swiper('.mySwiper', {
    effect: 'coverflow',
    grabCursor: !0,
    centeredSlides: !0,
    slidesPerView: 'auto',
    loop: 'true',
    coverflowEffect: {
      rotate: 0,
      stretch: 223,
      depth: 100,
      modifier: 1,
      slideShadows: !0,
    },
  });
  return r;
}

function formatNumber(e) {
  return new Intl.NumberFormat('en-EN', { maximumSignificantDigits: 2 }).format(
    e
  );
}

function getDiscount(e) {
  return ((e.price - e.discountedPrice) / e.price) * 100;
}

getData();
