﻿using System.Text.Json.Serialization;

namespace NovanetCore.ValueObject;

public class CookieObject
{
    [JsonPropertyName("a")] public Guid AdvertisingId { get; set; }
    [JsonPropertyName("t")] public DateTime Timestamp { get; set; }
}