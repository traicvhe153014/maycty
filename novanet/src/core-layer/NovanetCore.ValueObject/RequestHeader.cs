﻿namespace NovanetCore.ValueObject;

public class RequestHeader
{
    public const string XForwardedFor = "X-Forwarded-For";
    public const string XRealIp = "X-Real-IP";
}