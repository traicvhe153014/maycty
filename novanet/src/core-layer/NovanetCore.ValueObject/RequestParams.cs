﻿using System.Text.Json.Serialization;

namespace NovanetCore.ValueObject;

public class RequestParams
{
    [JsonPropertyName(ZoneKey)] public string Zone { get; set; } = default!;

    public const string ZoneKey = "zone";

    [JsonPropertyName(TimestampKey)] public string Timestamp { get; set; } = default!;

    public const string TimestampKey = "timestamp";

    [JsonPropertyName(AdvertisingKey)] public string Advertising { get; set; } = default!;

    public const string AdvertisingKey = "advertising";

    [JsonPropertyName(RefererKey)] public string Referer { get; set; } = default!;

    public const string RefererKey = "referer";

    [JsonPropertyName(UrlKey)] public string Url { get; set; } = default!;

    public const string UrlKey = "url";

    [JsonPropertyName(DomainKey)] public string Domain { get; set; } = default!;

    public const string DomainKey = "domain";

    [JsonPropertyName(IpKey)] public string Ip { get; set; } = default!;

    public const string IpKey = "ip";

    [JsonPropertyName(HashKey)] public string Hash { get; set; } = default!;

    public const string HashKey = "q";

    [JsonPropertyName(TargetUrlKey)] public string TargetUrl { get; set; } = default!;

    public const string TargetUrlKey = "target_url";

    [JsonPropertyName(CampaignKey)] public string Campaign { get; set; } = default!;

    public const string CampaignKey = "campaign";
    
    [JsonPropertyName(ProductKey)] public string Product { get; set; } = default!;

    public const string ProductKey = "product";
    
    [JsonPropertyName(ProductGroupKey)] public string ProductGroup { get; set; } = default!;

    public const string ProductGroupKey = "product_group";
    
    [JsonPropertyName(UrlCreateOnKey)] public string UrlCreateOn { get; set; } = default!;

    public const string UrlCreateOnKey = "urlcreateon";
    
    [JsonPropertyName(JsRefererKey)] public string JsReferer { get; set; } = default!;
    public const string JsRefererKey = "js_referer";
    
    [JsonPropertyName(JsHistoryLengthKey)] public string JsHistoryLength { get; set; } = default!;
    public const string JsHistoryLengthKey = "hl";
    
    [JsonPropertyName(IsEnableCookieKey)] public string IsEnableCookie { get; set; } = default!;
    public const string IsEnableCookieKey = "ce";
    
    [JsonPropertyName(IsEnableFlashKey)] public string IsEnableFlash { get; set; } = default!;
    public const string IsEnableFlashKey = "fe";
    
    [JsonPropertyName(LogoStatusKey)] public string LogoStatus { get; set; } = default!;

    public const string LogoStatusKey = "logo-status";
    
    public const string SellPriceKey = "sellprice";
    
    public const string BuyPriceKey = "buyprice";
    
    public const string PriceKey = "price";
    
    public const string AdvertisingTemplateKey = "tmpl";
    
    public const string IsDefaultKey = "default";
    
    public const string CloseSizeKey = "closeSize";

    public const string TimeKey = "time";
}