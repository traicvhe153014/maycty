﻿using Novanet.Core.Enums.ReportKeyObjects;

namespace NovanetCore.Business.BusinessObjects;

public class LogConfig
{
    #region Single log
    public List<ReportAdvertiserDimension>? EAdverForSingleObjLog { get; set; }
    public List<ReportPublisherDimension>? EPubForSingleObjLog { get; set; }
    public List<ReportClientDimension>? EClientSingleObjLog { get; set; }
    #endregion
    
    #region Multiple key log
    public List<List<ReportAdvertiserDimension>>? EAdverForMultipleObjLog { get; set; }
    #endregion

    #region AdverTargeting
    public List<ReportAdvertiserDimension>? EAdverForAdverTargeting { get; set; }
    public List<ReportTargetingDimension>? ETargetForAdverTargeting { get; set; }
    #endregion

    #region PubTargeting
    public List<ReportPublisherDimension>? EPubForPubTargeting { get; set; }
    public List<ReportTargetingDimension>? ETargetForPubTargeting { get; set; }
    #endregion

    #region Adver Pub log
    public List<ReportAdvertiserDimension>? EAdverForAdverPub { get; set; }
    public List<ReportPublisherDimension>? EPubForAdverPub { get; set; }
    #endregion

    public bool IsLogPubMoney { get; set; }
    public bool IsLogAdverMoney { get; set; }

    public LogConfig()
    {
        //var allEAdver= Enum.GetValues(typeof(EAdvertiserObject)).Cast<EAdvertiserObject>().ToList();
        //var allEPub = Enum.GetValues(typeof(EPublisherObject)).Cast<EPublisherObject>().ToList();
        //var allETargeting = Enum.GetValues(typeof(ETarget)).Cast<ETarget>().ToList();

        EAdverForSingleObjLog = new List<ReportAdvertiserDimension>();
        EPubForSingleObjLog = new List<ReportPublisherDimension>();

        EAdverForAdverTargeting = new List<ReportAdvertiserDimension>();
        ETargetForAdverTargeting =  new List<ReportTargetingDimension>();

        EAdverForAdverPub = new List<ReportAdvertiserDimension>();
        EPubForAdverPub = new List<ReportPublisherDimension>();

        EPubForPubTargeting = new List<ReportPublisherDimension>();
        ETargetForPubTargeting = new List<ReportTargetingDimension>();

        EClientSingleObjLog = new List<ReportClientDimension>();

        IsLogAdverMoney = false;
        IsLogPubMoney = false;
    }
}