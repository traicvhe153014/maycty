﻿using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessObjects;

public class ObjectPageViewItem
{
    public UrlConditionType Type { get; set; }
    public string Value { get; set; } = default!;
}