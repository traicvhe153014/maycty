﻿namespace NovanetCore.Business.BusinessObjects;

public class ProductEntityMatchScore
{
    public int Score { get; set; }

    public ProductEntityCore? ProductEntity { get; set; }
}