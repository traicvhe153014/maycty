﻿using Novanet.Core.Enums.ReportKeyObjects;

namespace NovanetCore.Business.BusinessObjects;

public class MetricReport
{
    // key: SubId
    public Dictionary<long, MetricReportByMetric> Data { get; set; } = default!;
}

public class MetricReportByMetric
{
    public Dictionary<ReportMetric, MetricReportValue> Metrics { get; set; } = default!;
}

public class MetricReportValue
{
    // key: dayOfYear tính từ DateTimeExtensions.OriginTime
    // value: giá trị của metric theo dayOfYear
    public Dictionary<long, double> Values { get; set; } = default!;
}