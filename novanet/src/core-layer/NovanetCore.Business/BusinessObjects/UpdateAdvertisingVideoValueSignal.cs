﻿namespace NovanetCore.Business.BusinessObjects;

public class UpdateAdvertisingVideoValueSignal
{
    public Guid AdvertisingId { get; set; }

    public string VideoId { get; set; } = default!;
}