﻿using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessUtils;

namespace NovanetCore.Business.BusinessObjects;

public class ValueItem
{
    public int Count { get; set; } //Imp, click...
    public double Earned { get; set; }
    public double Paid { get; set; }
    public double PromotionPaid { get; set; }
    public double PromotionEarned { get; set; }

    public ValueItem()
    {
        Count = 0;
        Earned = 0;
        Paid = 0;
        PromotionPaid = 0;
        PromotionEarned = 0;
    }

    public void Add(ValueItem item)
    {
        Count += item.Count;
        Earned += item.Earned;
        Paid += item.Paid;
        PromotionPaid += item.PromotionPaid;
        PromotionEarned += item.PromotionEarned;
    }
}

public class BaseStatistics
{
    protected ValueItem GetValueStatistic(double earned, double paid, bool isPromotion, bool isFraud)
    {
        var rtData = new ValueItem();
        rtData.Count++;

        if (!isFraud)
        {
            if (isPromotion)
            {
                rtData.PromotionEarned += earned;
                rtData.PromotionPaid += paid;
            }
            else
            {
                rtData.Earned += earned;
                rtData.Paid += paid;
            }
        }

        return rtData;
    }
}

public interface IStatistic
{
    void Add(TrackingCore imp);
}

public class SumByPublisher : BaseStatistics, IStatistic
{
    //Dictionary<ObjId,ValueItem>
    public Dictionary<ReportPublisherDimension, Dictionary<long, ValueItem>> StatisticByPublisherObj;

    public SumByPublisher()
    {
        StatisticByPublisherObj = new Dictionary<ReportPublisherDimension, Dictionary<long, ValueItem>>();
    }

    public void Add(TrackingCore imp)
    {
        var zoneId = imp.ZoneId;
        var websiteId = imp.WebsiteId;
        var publisherId = imp.PublisherId;

        var earned = imp.BuyPrice;
        var paid = imp.SellPrice;
        var isPromotion = imp.IsPromotion;
        var statisticValue = GetValueStatistic(earned, paid, isPromotion, false);

        AddLog(zoneId, websiteId, publisherId, statisticValue);
    }

    private void AddLog(long zoneId, long websiteId, long publisherId, ValueItem logValue)
    {
        #region init by Publisher Obj

        var lstObj = Enum.GetValues(typeof(ReportPublisherDimension)).Cast<ReportPublisherDimension>().ToList();
        foreach (var eObj in lstObj)
        {
            if (!StatisticByPublisherObj.ContainsKey(eObj))
            {
                StatisticByPublisherObj.Add(eObj, new Dictionary<long, ValueItem>());
            }
        }

        #endregion

        //var templateStatistic = StatisticByPublisherObj[EPublisherObject.TEMPLATE];
        var zoneStatistic = StatisticByPublisherObj[ReportPublisherDimension.Zone];
        var webStatistic = StatisticByPublisherObj[ReportPublisherDimension.Website];
        var publisherStatistic = StatisticByPublisherObj[ReportPublisherDimension.Publisher];

        //if (!templateStatistic.ContainsKey(tempateId))
        //{
        //    templateStatistic.Add(tempateId, new ValueItem());
        //}

        if (!zoneStatistic.ContainsKey(zoneId))
        {
            zoneStatistic.Add(zoneId, new ValueItem());
        }

        if (!webStatistic.ContainsKey(websiteId))
        {
            webStatistic.Add(websiteId, new ValueItem());
        }

        if (!publisherStatistic.ContainsKey(publisherId))
        {
            publisherStatistic.Add(publisherId, new ValueItem());
        }

        //templateStatistic[tempateId].Add(logValue);
        zoneStatistic[zoneId].Add(logValue);
        webStatistic[websiteId].Add(logValue);
        publisherStatistic[publisherId].Add(logValue);
    }
}

public class SumByAdvertiser : BaseStatistics, IStatistic
{
    public Dictionary<ReportAdvertiserDimension, Dictionary<long, ValueItem>> StatisticByAdverObj;

    public SumByAdvertiser()
    {
        StatisticByAdverObj = new Dictionary<ReportAdvertiserDimension, Dictionary<long, ValueItem>>();
    }

    public void Add(TrackingCore imp)
    {
        var advitemId = imp.AdvertisingId;
        var advgroupId = imp.AdvertisingSetId;
        var campaignId = imp.CampaignId;
        var advertiserId = imp.AdvertiserId;
        var conversionCodeId = imp.ConversionId;
        var productIds = imp.ProductIds ?? new List<long>();
        var advertisingSetProductGroupIds = imp.AdvertisingSetProductGroupIds ?? new List<long>();
        var advertisingTemplateId = imp.AdvertisingTemplateId;

        var earned = imp.BuyPrice;
        var paid = imp.SellPrice;
        var isPromotion = imp.IsPromotion;
        var statisticValue = GetValueStatistic(earned, paid, isPromotion, false);

        AddLog(advitemId, advgroupId, campaignId, advertiserId, productIds, advertisingSetProductGroupIds,
            advertisingTemplateId, conversionCodeId, statisticValue);
    }

    private void AddLog(
        long advitemId,
        long advgroupId,
        long campaignId,
        long advertiserId,
        List<long> productIds,
        List<long> advertisingSetProductGroupIds,
        long advertisingTemplateId,
        long conversionCodeId,
        ValueItem logValue)
    {
        #region init

        var lstObj = Enum.GetValues(typeof(ReportAdvertiserDimension)).Cast<ReportAdvertiserDimension>().ToList();
        foreach (var eObj in lstObj)
        {
            if (!StatisticByAdverObj.ContainsKey(eObj))
            {
                StatisticByAdverObj.Add(eObj, new Dictionary<long, ValueItem>());
            }
        }

        #endregion

        var advitemStatistic = StatisticByAdverObj[ReportAdvertiserDimension.Advertising];
        var advgroupStatistic = StatisticByAdverObj[ReportAdvertiserDimension.AdvertisingSet];
        var campaignStatistic = StatisticByAdverObj[ReportAdvertiserDimension.Campaign];
        var advertiserStatistic = StatisticByAdverObj[ReportAdvertiserDimension.Advertiser];
        var productStatistic = StatisticByAdverObj[ReportAdvertiserDimension.Product];
        var advertisingSetProductGroupStatistic =
            StatisticByAdverObj[ReportAdvertiserDimension.AdvertisingSetProductGroup];
        var advertisingTemplateStatistic = StatisticByAdverObj[ReportAdvertiserDimension.AdvertisingTemplate];

        // var conversionStatistic = StatisticByAdverObj[ReportAdvertiserDimension.Conversion];

        if (!advitemStatistic.ContainsKey(advitemId))
        {
            advitemStatistic.Add(advitemId, new ValueItem());
        }

        if (!advgroupStatistic.ContainsKey(advgroupId))
        {
            advgroupStatistic.Add(advgroupId, new ValueItem());
        }

        if (!campaignStatistic.ContainsKey(campaignId))
        {
            campaignStatistic.Add(campaignId, new ValueItem());
        }

        if (!advertiserStatistic.ContainsKey(advertiserId))
        {
            advertiserStatistic.Add(advertiserId, new ValueItem());
        }
        
        if (!advertisingTemplateStatistic.ContainsKey(advertisingTemplateId))
        {
            advertisingTemplateStatistic.Add(advertisingTemplateId, new ValueItem());
        }

        foreach (var productId in productIds.Where(productId => !productStatistic.ContainsKey(productId)))
        {
            productStatistic.Add(productId, new ValueItem());
        }

        foreach (var advertisingSetProductGroupId in advertisingSetProductGroupIds.Where(advertisingSetProductGroupId =>
                     !advertisingSetProductGroupStatistic.ContainsKey(advertisingSetProductGroupId)))
        {
            advertisingSetProductGroupStatistic.Add(advertisingSetProductGroupId, new ValueItem());
        }

        // if (!conversionStatistic.ContainsKey(conversionCodeId))
        // {
        //     conversionStatistic.Add(conversionCodeId, new ValueItem());
        // }

        advitemStatistic[advitemId].Add(logValue);
        advgroupStatistic[advgroupId].Add(logValue);
        campaignStatistic[campaignId].Add(logValue);
        advertiserStatistic[advertiserId].Add(logValue);
        advertisingTemplateStatistic[advertisingTemplateId].Add(logValue);
        foreach (var productId in productIds)
        {
            productStatistic[productId].Add(logValue);
        }

        foreach (var advertisingSetProductGroupId in advertisingSetProductGroupIds)
        {
            advertisingSetProductGroupStatistic[advertisingSetProductGroupId].Add(logValue);
        }
        // conversionStatistic[conversionCodeId].Add(logValue);
    }
}

public class SumByClient : BaseStatistics, IStatistic
{
    public readonly Dictionary<ReportClientDimension, Dictionary<Guid, ValueItem>> _statisticByClientObj;

    public SumByClient()
    {
        _statisticByClientObj = new Dictionary<ReportClientDimension, Dictionary<Guid, ValueItem>>();
    }

    public void Add(TrackingCore imp)
    {
        var clientId = imp.ClientId;

        var earned = imp.BuyPrice;
        var paid = imp.SellPrice;
        var isPromotion = imp.IsPromotion;
        var statisticValue = GetValueStatistic(earned, paid, isPromotion, false);

        AddLog(clientId, statisticValue);
    }

    private void AddLog(Guid clientId, ValueItem logValue)
    {
        #region init

        var lstObj = Enum.GetValues(typeof(ReportClientDimension)).Cast<ReportClientDimension>().ToList();
        foreach (var eObj in lstObj.Where(eObj => !_statisticByClientObj.ContainsKey(eObj)))
        {
            _statisticByClientObj.Add(eObj, new Dictionary<Guid, ValueItem>());
        }

        #endregion

        var clientIdStatistic = _statisticByClientObj[ReportClientDimension.ClientId];

        if (!clientIdStatistic.ContainsKey(clientId))
        {
            clientIdStatistic.Add(clientId, new ValueItem());
        }

        clientIdStatistic[clientId].Add(logValue);
    }
}

public class SumByTargeting : BaseStatistics, IStatistic
{
    public SumByTargeting()
    {
        StatisticByTargetingObj = new Dictionary<ReportTargetingDimension, Dictionary<long, ValueItem>>();
    }
    
    public Dictionary<ReportTargetingDimension, Dictionary<long, ValueItem>> StatisticByTargetingObj;

    public void Add(TrackingCore imp)
    {
        var countryId = imp.CountryId;
        var vnprovinceId = imp.VnProvinceId;
        var locationId = imp.Location;
        var frequency = imp.NumberOfImpression;
        var ids = new ETargetObjId
        {
            CountryId = countryId,
            VnProvinceId = vnprovinceId,
            LocationId = locationId,
            Frequency = frequency
        };

        var earned = imp.BuyPrice;
        var paid = imp.SellPrice;
        var isPromotion = imp.IsPromotion;
        var statisticValue = GetValueStatistic(earned, paid, isPromotion, false);
        AddLog(ids, statisticValue);
    }

   private void AddLog(ETargetObjId ids, ValueItem logValue)
    {
        var lstTargetObj = Enum.GetValues(typeof(ReportTargetingDimension)).Cast<ReportTargetingDimension>().ToList();
        foreach (var eTarget in lstTargetObj)
        {
            if (!StatisticByTargetingObj.ContainsKey(eTarget))
            {
                StatisticByTargetingObj.Add(eTarget, new Dictionary<long, ValueItem>());
            }
        }

        // var countryStatistic = StatisticByTargetingObj[ReportTargetingDimension.COUNTRY];
        // var vnprovinceIdStatistic = StatisticByTargetingObj[ReportTargetingDimension.VNPROVINCE];
        var locationStatistic = StatisticByTargetingObj[ReportTargetingDimension.Location];

        #region Init by Id
        // if (!countryStatistic.ContainsKey(ids.CountryId))
        // {
        //     countryStatistic.Add(ids.CountryId, new ValueItem());
        // }
        // if (!vnprovinceIdStatistic.ContainsKey(ids.VnProvinceId))
        // {
        //     vnprovinceIdStatistic.Add(ids.VnProvinceId, new ValueItem());
        // }
        if (!locationStatistic.ContainsKey(ids.LocationId))
        {
            locationStatistic.Add(ids.LocationId, new ValueItem());
        }

        #endregion

        // countryStatistic[ids.CountryId].Add(logValue);
        // vnprovinceIdStatistic[ids.VnProvinceId].Add(logValue);
        locationStatistic[ids.LocationId].Add(logValue);
    }
}

// public class SumByTargetingOnlyLog : BaseStatistics, IStatistic
// {
//     public SumByTargetingOnlyLog()
//     {
//         StatisticByTargetingObj = new Dictionary<ETargetOnlyLog, Dictionary<string, ValueItem>>();
//     }
//     public Dictionary<ETargetOnlyLog, Dictionary<string, ValueItem>> StatisticByTargetingObj;
//
//     public void Add(TrackingCore imp)
//     {
//         var hardwareFamilyId = string.Empty;//imp.MobileInfo.HardwareFamilyId;
//         var hardwareModelId = string.Empty;//imp.MobileInfo.HardwareModelId;
//         var ids = new ETargetOnlyLogId
//             {
//                 HardwareFamilyId = hardwareFamilyId,
//                 HardwareModelId = hardwareModelId
//             };
//         var earned = imp.BuyPrice;
//         var paid = imp.SellPrice;
//         var isPromotion = imp.IsPromotion;
//         var statisticValue = GetValueStatistic(earned, paid, isPromotion, false);
//         AddLog(ids, statisticValue);
//     }                
//
//     private void AddLog(ETargetOnlyLogId ids, ValueItem logValue)
//     {
//         var lstTargetObj = Enum.GetValues(typeof(ETargetOnlyLog)).Cast<ETargetOnlyLog>().ToList();
//         foreach (var eTarget in lstTargetObj)
//         {
//             if (!StatisticByTargetingObj.ContainsKey(eTarget))
//             {
//                 StatisticByTargetingObj.Add(eTarget, new Dictionary<string, ValueItem>());
//             }
//         }
//
//         var hardwareFamilyStatistic = StatisticByTargetingObj[ETargetOnlyLog.HARDWAREFAMILY];
//         var hardwareModelStatistic = StatisticByTargetingObj[ETargetOnlyLog.HARDWAREMODEL];
//         
//         #region Init by Id
//         if (!hardwareFamilyStatistic.ContainsKey(ids.HardwareFamilyId))
//         {
//             hardwareFamilyStatistic.Add(ids.HardwareFamilyId, new ValueItem());
//         }
//
//         if (!hardwareModelStatistic.ContainsKey(ids.HardwareModelId))
//         {
//             hardwareModelStatistic.Add(ids.HardwareModelId, new ValueItem());
//         }
//         #endregion
//
//         hardwareFamilyStatistic[ids.HardwareFamilyId].Add(logValue);
//         hardwareModelStatistic[ids.HardwareModelId].Add(logValue);
//     }
// }

public class StatisticByHourItem : IStatistic
{
    public int DayOfYear { get; set; }

    public SumByPublisher Publisher { get; set; }
    public SumByAdvertiser Advertiser { get; set; }

    public SumByClient Client { get; set; }
    // public SumByTargeting Targeting;
    // public SumByTargetingOnlyLog TargetingOnlyLog;

    // public Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByTargetingOnlyLog>> StatisticTargetingOnlyLog;
    public Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByTargeting>> StatisticAdverTargeting { get; set; }
    // public Dictionary<ReportPublisherDimension, Dictionary<long, SumByTargeting>> StatisticPublisherTargeting;
    public Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByPublisher>> StatisticAdverPublisher { get; set; }
    
    public Dictionary<string, Dictionary<string, SumByAdvertiser>> StatisticMutlipleAdver { get; set; }

    public StatisticByHourItem()
    {
        Publisher = new SumByPublisher();
        Advertiser = new SumByAdvertiser();
        Client = new SumByClient();
        // Targeting = new SumByTargeting();
        // StatisticTargetingOnlyLog = new Dictionary<ReportAdvertiserDimension, Dictionary<int, SumByTargetingOnlyLog>>();
        StatisticAdverTargeting = new Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByTargeting>>();
        // StatisticPublisherTargeting = new Dictionary<ReportPublisherDimension, Dictionary<int, SumByTargeting>>();
        StatisticAdverPublisher = new Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByPublisher>>();
        StatisticMutlipleAdver = new Dictionary<string, Dictionary<string, SumByAdvertiser>>();
    }

    public void Add(TrackingCore imp)
    {
        DayOfYear = imp.CreatedOn.GetDayOfYear();
        Publisher.Add(imp);
        Advertiser.Add(imp);
        Client.Add(imp);
        // Targeting.Add(imp);

        #region Init

        var lstAdverObj = Enum.GetValues(typeof(ReportAdvertiserDimension)).Cast<ReportAdvertiserDimension>().ToList();
        foreach (var eObj in lstAdverObj)
        {
            if (!StatisticAdverTargeting.ContainsKey(eObj))
            {
                StatisticAdverTargeting.Add(eObj, new Dictionary<long, SumByTargeting>());
            }
            if (!StatisticAdverPublisher.ContainsKey(eObj))
            {
                StatisticAdverPublisher.Add(eObj, new Dictionary<long, SumByPublisher>());
            }
        }

        // var lstPublisherObj = Enum.GetValues(typeof(ReportPublisherDimension)).Cast<ReportPublisherDimension>().ToList();
        // foreach (var eObj in lstPublisherObj)
        // {
        //     if (!StatisticPublisherTargeting.ContainsKey(eObj))
        //     {
        //         StatisticPublisherTargeting.Add(eObj, new Dictionary<int, SumByTargeting>());
        //     }
        // }

        #endregion

        #region Advertiser - Targeting

        var advItemByTargeting = StatisticAdverTargeting[ReportAdvertiserDimension.Advertising];
        var advGroupByTargeting = StatisticAdverTargeting[ReportAdvertiserDimension.AdvertisingSet];
        var campaignByTargeting = StatisticAdverTargeting[ReportAdvertiserDimension.Campaign];
        var advertiserByTargeting = StatisticAdverTargeting[ReportAdvertiserDimension.Advertiser];
        
        if (!advItemByTargeting.ContainsKey(imp.AdvertisingId))
        {
            advItemByTargeting.Add(imp.AdvertisingId, new SumByTargeting());
        }
        
        if (!advGroupByTargeting.ContainsKey(imp.AdvertisingSetId))
        {
            advGroupByTargeting.Add(imp.AdvertisingSetId, new SumByTargeting());
        }
        
        if (!campaignByTargeting.ContainsKey(imp.CampaignId))
        {
            campaignByTargeting.Add(imp.CampaignId, new SumByTargeting());
        }
        
        if (!advertiserByTargeting.ContainsKey(imp.AdvertiserId))
        {
            advertiserByTargeting.Add(imp.AdvertiserId, new SumByTargeting());
        }
        
        advItemByTargeting[imp.AdvertisingId].Add(imp);
        advGroupByTargeting[imp.AdvertisingSetId].Add(imp);
        campaignByTargeting[imp.CampaignId].Add(imp);
        advertiserByTargeting[imp.AdvertiserId].Add(imp);

        #endregion

        #region Publisher - Targeting

        // //var templateByTargeting = StatisticPublisherTargeting[EPublisherObject.TEMPLATE];
        // var zoneByTargeting = StatisticPublisherTargeting[EPublisherObject.ZONE];
        // var websiteByTargeting = StatisticPublisherTargeting[EPublisherObject.WEBSITE];
        // var publisherByTargeting = StatisticPublisherTargeting[EPublisherObject.PUBLISHER];
        //
        // //if (!templateByTargeting.ContainsKey(imp.TemplateId))
        // //{
        // //    templateByTargeting.Add(imp.TemplateId, new SumByTargeting());
        // //}
        //
        // if (!zoneByTargeting.ContainsKey(imp.ZoneId))
        // {
        //     zoneByTargeting.Add(imp.ZoneId, new SumByTargeting());
        // }
        //
        // if (!websiteByTargeting.ContainsKey(imp.WebsiteId))
        // {
        //     websiteByTargeting.Add(imp.WebsiteId, new SumByTargeting());
        // }
        //
        // if (!publisherByTargeting.ContainsKey(imp.PublisherId))
        // {
        //     publisherByTargeting.Add(imp.PublisherId, new SumByTargeting());
        // }
        //
        // //templateByTargeting[imp.TemplateId].AddImp(imp);
        // zoneByTargeting[imp.ZoneId].Add(imp);
        // websiteByTargeting[imp.WebsiteId].Add(imp);
        // publisherByTargeting[imp.PublisherId].Add(imp);

        #endregion

        #region Advertiser - Publisher

        var advItemPublisherStatistic = StatisticAdverPublisher[ReportAdvertiserDimension.Advertising];
        var advGroupPublisherStatistic = StatisticAdverPublisher[ReportAdvertiserDimension.AdvertisingSet];
        var campaignPublisherStatistic = StatisticAdverPublisher[ReportAdvertiserDimension.Campaign];
        var advertiserPublisherStatistic = StatisticAdverPublisher[ReportAdvertiserDimension.Advertiser];
        var productPublisherStatistic = StatisticAdverPublisher[ReportAdvertiserDimension.Product];
        var advertisingSetProductGroupPublisherStatistic =
            StatisticAdverPublisher[ReportAdvertiserDimension.AdvertisingSetProductGroup];

        if (!advItemPublisherStatistic.ContainsKey(imp.AdvertisingId))
        {
            advItemPublisherStatistic.Add(imp.AdvertisingId, new SumByPublisher());
        }

        if (!advGroupPublisherStatistic.ContainsKey(imp.AdvertisingSetId))
        {
            advGroupPublisherStatistic.Add(imp.AdvertisingSetId, new SumByPublisher());
        }

        if (!campaignPublisherStatistic.ContainsKey(imp.CampaignId))
        {
            campaignPublisherStatistic.Add(imp.CampaignId, new SumByPublisher());
        }

        if (!advertiserPublisherStatistic.ContainsKey(imp.AdvertiserId))
        {
            advertiserPublisherStatistic.Add(imp.AdvertiserId, new SumByPublisher());
        }

        foreach (var productId in (imp.ProductIds ?? new List<long>()).Where(productId =>
                     !productPublisherStatistic.ContainsKey(productId)))
        {
            productPublisherStatistic.Add(productId, new SumByPublisher());
        }

        foreach (var advertisingSetProductGroupId in (imp.AdvertisingSetProductGroupIds ?? new List<long>()).Where(
                     advertisingSetProductGroupId =>
                         !advertisingSetProductGroupPublisherStatistic.ContainsKey(advertisingSetProductGroupId)))
        {
            advertisingSetProductGroupPublisherStatistic.Add(advertisingSetProductGroupId, new SumByPublisher());
        }

        advItemPublisherStatistic[imp.AdvertisingId].Add(imp);
        advGroupPublisherStatistic[imp.AdvertisingSetId].Add(imp);
        campaignPublisherStatistic[imp.CampaignId].Add(imp);
        advertiserPublisherStatistic[imp.AdvertiserId].Add(imp);
        foreach (var productId in imp.ProductIds ?? new List<long>())
        {
            productPublisherStatistic[productId].Add(imp);
        }

        foreach (var advertisingSetProductGroupId in imp.AdvertisingSetProductGroupIds ?? new List<long>())
        {
            advertisingSetProductGroupPublisherStatistic[advertisingSetProductGroupId].Add(imp);
        }

        #endregion

        #region TargetingOnlyLog - Adver

        // var advGroupByTargetingOnlyLog = StatisticTargetingOnlyLog[EAdvertiserObject.ADVGROUP];
        // var campaignByTargetingOnlyLog = StatisticTargetingOnlyLog[EAdvertiserObject.CAMPAIGN];
        //
        // if (!advGroupByTargetingOnlyLog.ContainsKey(imp.AdvGroupId))
        // {
        //     advGroupByTargetingOnlyLog.Add(imp.AdvGroupId, new SumByTargetingOnlyLog());
        // }
        //
        // if (!campaignByTargetingOnlyLog.ContainsKey(imp.CampaignId))
        // {
        //     campaignByTargetingOnlyLog.Add(imp.CampaignId, new SumByTargetingOnlyLog());
        // }
        //
        // advGroupByTargetingOnlyLog[imp.AdvGroupId].Add(imp);
        // campaignByTargetingOnlyLog[imp.CampaignId].Add(imp);

        #endregion

        #region Multiple Advertiser

        foreach (var advertisers in SupportedMultipleAdvertisers)
        {
            var advertisersKey = string.Join('_', advertisers);
            if (!StatisticMutlipleAdver.ContainsKey(advertisersKey))
            {
                StatisticMutlipleAdver.Add(advertisersKey, new Dictionary<string, SumByAdvertiser>());
            }

            foreach (var advertiserValues in advertisers.Select(imp.GetAdvertiserValues).GetPermutations())
            {
                var advertisersValueList = advertiserValues.ToList();
                var advertisersValueString = string.Join('_', advertisersValueList);

                if (!StatisticMutlipleAdver[advertisersKey].ContainsKey(advertisersValueString))
                {
                    StatisticMutlipleAdver[advertisersKey].Add(advertisersValueString, new SumByAdvertiser());
                }
                StatisticMutlipleAdver[advertisersKey][advertisersValueString].Add(imp);
            }
        }

        #endregion
    }

    public readonly List<List<ReportAdvertiserDimension>> SupportedMultipleAdvertisers =
        new()
        {
            new List<ReportAdvertiserDimension>
            {
                ReportAdvertiserDimension.Advertising,
                ReportAdvertiserDimension.AdvertisingTemplate
            },
            new List<ReportAdvertiserDimension>
            {
                ReportAdvertiserDimension.Campaign,
                ReportAdvertiserDimension.Product
            }
        };
}

public class ETargetObjId
{        
    public long CountryId { get; set; }
    public long VnProvinceId { get; set; }
    public long LocationId { get; set; }
    public int Frequency { get; set; }
}
