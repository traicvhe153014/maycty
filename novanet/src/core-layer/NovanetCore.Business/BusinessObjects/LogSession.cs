﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessObjects;

public class LogSession : NovanetDocument
{
    public long UserId { get; set; }
    public string OldObject { get; set; } = default!;
    public string NewObject { get; set; } = default!;
    public DateTimeOffset? Time { get; set; }
    public string TableName { get; set; } = default!;
    public string SessionHashCode { get; set; } = default!;
    public int? ModifiedType { get; set; }
    public bool? IsProcessed { get; set; }
    public bool? IsAPI { get; set; }
}