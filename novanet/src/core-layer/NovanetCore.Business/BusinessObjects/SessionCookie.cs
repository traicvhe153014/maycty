﻿using System.Text.Json.Serialization;

namespace NovanetCore.Business.BusinessObjects;

public class CookieRemarketing
{
    [JsonPropertyName("c")] public Guid CampaignId { get; set; } = default!;

    [JsonPropertyName("t")] public string Timestamp { get; set; } = default!;

    [JsonPropertyName("v")] public int TotalViews { get; set; }
}