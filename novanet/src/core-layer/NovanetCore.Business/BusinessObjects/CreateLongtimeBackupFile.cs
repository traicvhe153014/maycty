﻿namespace NovanetCore.Business.BusinessObjects;

public class CreateLongtimeBackupFile
{
    public byte[] FileByte { get; set; }

    public string PathFile { get; set; }
}