﻿using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessObjects;

public class ObjectBehaviorWithProductItem
{
    public ObjectGroupProductType? ProductType { get; set; }

    public string? ProductName { get; set; }
    
    public BehaviorType BehaviorType { get; set; }
}