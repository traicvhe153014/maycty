﻿namespace NovanetCore.Business.BusinessObjects;

public class TemplateObject
{
    public string TemplateHtml { get; set; } = default!;
    public bool IsBackUp { get; set; }
}