﻿namespace NovanetCore.Business.BusinessObjects;

public class SyncWebsitePublisherReportSignal
{
    public long WebsiteSubId { get; set; }
    public Guid OldPublisherId { get; set; }
    public Guid NewPublisherId { get; set; }
}