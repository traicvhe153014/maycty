﻿namespace NovanetCore.Business.BusinessObjects;

public class AdvertisingMatchScore
{
    public int Score { get; set; }
    
    public double BuyPrice { get; set; }
    
    public double SellPrice { get; set; }
    
    public PriceType SellPriceType { get; set; }

    // TODO: # Hòa NX - Biến này không được dùng để tính tiền ở tầng log ?
    public PriceType BuyPriceType { get; set; }

    public AdvertisingCore? Advertising { get; set; }
    
    public bool UseDefault { get; set; }
    
    public bool UseBackup { get; set; }
}