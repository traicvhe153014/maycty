﻿namespace NovanetCore.Business.BusinessObjects.FraudClickObjects;

public enum FraudClickKeyConfig
{
    ClickUrlRate = 1,
    TimeSetDoubleClick = 2,
    HighRate = 3,
    /// <summary>
    /// Số lần giới hạn của (Tỉ lệ CLick của NewClient của 1 Pub / tỉ lệ Click của newClient của toàn bộ Pub).
    /// </summary>
    FilterRate = 4,
    /// <summary>
    /// Thời gian quy định newclient = thời gian phát sinh Click - thời gian tạo Client.
    /// </summary>
    TimeSetIsNewClient = 5,
    ClientIdClickAverageRates = 6,
    DomainClickAverageRates = 7,
    TimeSpanViewToClicked = 8   
}