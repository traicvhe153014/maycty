﻿namespace NovanetCore.Business.BusinessObjects.FraudClickObjects;

public class FraudClickKeyConfigValue
{
    public int KeyConfigId { get; set; }
    public int RulesId { get; set; }
    public double Value { get; set; }
}