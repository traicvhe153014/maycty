﻿namespace NovanetCore.Business.BusinessObjects.FraudClickObjects;

public enum FraudClickRules
{
    Ips = 1,
    DoubleClick = 2,
    CreateTimeClientId = 3,
    ClientIdClickRates = 4,
    TimeSpanViewToClicked = 5
}