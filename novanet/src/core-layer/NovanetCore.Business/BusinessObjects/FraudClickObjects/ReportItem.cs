﻿namespace NovanetCore.Business.BusinessObjects.FraudClickObjects;

public class ReportItem
{ 
    //statistics
    public long IMP { get; set; }
    public long CLICK { get; set; }
    public long ACTIVEVIEW { get; set; }
    public long FDCLICK { get; set; }
    public int CT1N { get; set; }
    public int CT11 { get; set; }
    public long PROMOTIONPAID { get; set; }
    public long PROMOTIONEARNED { get; set; }
    public long PROMOTIONGETBACK { get; set; }
    public long PROMOTIONREFUND { get; set; }
    public long PAID { get; set; }
    public long EARNED { get; set; }
    public long GETBACK { get; set; }
    public long REFUND { get; set; }
    public long DEFAULTIMP { get; set; }
    public long VIEWRATE25 { get; set; }
    public long VIEWRATE50 { get; set; }
    public long VIEWRATE75 { get; set; }
    public long VIEWRATE100 { get; set; }

    public long FDCLICK_REALTIME { get; set; }
    public long FDCLICK_IP { get; set; }
    public long FDCLICK_MAXCTR { get; set; }
    public long FDCLICK_CLICKRATE { get; set; }
    public long FDCLICK_DOUBLECLICK { get; set; }
    public long FDCLICK_CLIENTCREATE { get; set; }
    public long FDCLICK_OUT_OF_TIMESPAN { get; set; }
    public long FDCLICK_VNPROVINCE_NOTCONFIG { get; set; }
    public long FDCLICK_PERCENTCUTNOVANET { get; set; }
    public long FDCLICK_COUNTRY_NOTCONFIG { get; set; }        

    #region ID
    //advertiser
    public int ADVERTISER_ID { get; set; }
    public int CAMPAIGN_ID { get; set; }
    public int ADVGROUP_ID { get; set; }
    public int ADVITEM_ID { get; set; }        
    //pub
    public int PUBLISHER_ID { get; set; }
    public int WEBSITE_ID { get; set; }
    public int ZONE_ID { get; set; }
    
    //target
    public int COUNTRY_ID { get; set; }
    public int VNPROVINCE_ID { get; set; }
    public int ADVPACKET_ID { get; set; }
    #endregion
    
    public long TIME { get; set; }
    
    //TOP VISIT URL 
    public double ORDER { get; set; }
    public string? URL { get; set; }

    #region thuộc tính thêm của báo cáo mobile
    //public int OsId { get; set; }
    //public int BrowserId { get; set; }
    //public int ManufacturerId { get; set; }
    //public int IspId { get; set; }
    //public int NetworkTypeId { get; set; }
    //public int DeviceId { get; set; }
    #endregion
}