﻿using System.Text.Json.Serialization;

namespace NovanetCore.Business.BusinessObjects;

public class CookieInfoItem
{
    [JsonPropertyName("A")] public long AdvertisingId { get; set; }

    [JsonPropertyName("B")] public int CountAll { get; set; }

    [JsonPropertyName("C")] public int CountInDay { get; set; }

    [JsonPropertyName("D")] public string LastViewTime { get; set; } = string.Empty;
    
    [JsonPropertyName("E")] public int ClickCount { get; set; }

    [JsonPropertyName("F")] public int ConversionCount { get; set; }
}