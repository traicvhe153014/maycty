﻿using Microsoft.AspNetCore.Http;
using Novanet.Core.RedisCache;

namespace NovanetCore.Business.BusinessServices;

public interface ICookieService
{
    List<CookieObject> Histories();

    List<CookieRemarketing> CampaignMatches();

    int MaximumTimeOnSite(Guid? campaignId);

    Task<bool> VerifySession<T>(Guid id);
}

public class CookieService : ICookieService
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IGlobalCacheService _globalCacheService;

    public CookieService(IHttpContextAccessor httpContextAccessor,
        IGlobalCacheService globalCacheService)
    {
        _httpContextAccessor = httpContextAccessor;
        _globalCacheService = globalCacheService;
    }

    public List<CookieObject> Histories()
    {
        var cookieJson = _httpContextAccessor.HttpContext?.Request.Cookies[ConfigValues.History];
        if (cookieJson is null)
        {
            return new List<CookieObject>();
        }
        var cookies = cookieJson.Deserialize<List<CookieObject>>();
        return !cookies.Any() ? new List<CookieObject>() : cookies;
    }

    public List<CookieRemarketing> CampaignMatches()
    {
        var cookieJson = _httpContextAccessor.HttpContext?.Request.Cookies[ConfigValues.Remarketing];
        if (cookieJson is null)
        {
            return new List<CookieRemarketing>();
        }
        var cookies = cookieJson.Deserialize<List<CookieRemarketing>>();
        return !cookies.Any() ? new List<CookieRemarketing>() : cookies;
    }

    public int MaximumTimeOnSite(Guid? campaignId)
    {
        var cookieCheckDomain =
            _httpContextAccessor.HttpContext?.Request.Cookies[ConfigValues.RemarketingDomain + campaignId];
        var cookieMaxTimeOnSite =
            _httpContextAccessor.HttpContext?.Request.Cookies[ConfigValues.MaximumTimeOnSite + campaignId];
        return cookieCheckDomain != null && cookieMaxTimeOnSite != null ? int.Parse(cookieMaxTimeOnSite) : 0;
    }

    public Task<bool> VerifySession<T>(Guid id)
    {
        return _globalCacheService.ExistsAsync(RedisKeys.KeyDisplay<T>(id));
    }
}