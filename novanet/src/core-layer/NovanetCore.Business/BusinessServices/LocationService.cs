﻿using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessDomain.Service.Sharing;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessUtils;

namespace NovanetCore.Business.BusinessServices;

public interface ILocationService
{
    Task<IPLocationCore?> IPToLocation(string ip);
}

public class LocationService : ILocationService
{
    private static readonly string[] LocalhostIps = { "127.0.0.1", "0.0.0.0" };
    private readonly IGlobalCacheService _globalCacheService;

    public LocationService(IGlobalCacheService globalCacheService)
    {
        _globalCacheService = globalCacheService;
    }

    public async Task<IPLocationCore?> IPToLocation(string ip)
    {
        var locationId = await FindLocation(ip);
        if (locationId <= 0)
            return new IPLocationCore
            {
                CountryId = ConfigValues.UnknownCountryId,
                VnProvinceId = ConfigValues.UnknownProvinceId
            };

        var location =
            await _globalCacheService.GetAsync<IPLocationCore>(RedisKeys.KeySharing<IPLocationCore>(locationId));
        return location ?? new IPLocationCore
        {
            CountryId = ConfigValues.UnknownCountryId,
            VnProvinceId = ConfigValues.UnknownProvinceId
        };
    }

    public static bool IsPrivate(string? ip)
    {
        if (string.IsNullOrEmpty(ip)) return true;

        if (LocalhostIps.Contains(ip)) return true;

        var ipParts = ip
            .Split('.').Select(x => string.IsNullOrWhiteSpace(x) ? 0 : int.TryParse(x, out var value) ? value : 0)
            .ToArray();

        var ipPart0 = ipParts.ElementAtOrDefault(0);
        var ipPart1 = ipParts.ElementAtOrDefault(1);
        // in private ip range
        return ipPart0 == 10 ||
               (ipPart0 == 192 && ipPart1 == 168) ||
               (ipPart0 == 172 && ipPart1 is >= 16 and <= 31);
    }

    private async Task<long> FindLocation(string ip)
    {
        if (string.IsNullOrEmpty(ip))
            return 0;
        var ipValue = ConvertUtils.ConvertIp(ip);
        if (ipValue == 0)
            return 0;

        var ipBlockId = await _globalCacheService.GetAsync(RedisKeys.KeyIpBlock(ipValue));
        ipBlockId = ipBlockId?.Trim('"');
        var ipBlock = await _globalCacheService.GetAsync<IPBlockCore>(RedisKeys.KeySharing<IPBlockCore>(ipBlockId));
        return ipBlock?.LocationId ?? default;
    }
}