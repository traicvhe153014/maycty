﻿using System.Globalization;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessUtils;
using StackExchange.Redis;

namespace NovanetCore.Business.BusinessServices;

public class RedisLogService
{
    private readonly IRedisCacheService _redisCacheService;

    public RedisLogService(IRedisCacheService redisCacheService)
    {
        _redisCacheService = redisCacheService;
    }

    public async Task Save(List<TrackingCore> clicks, ReportMetric[] arrMoney, ReportMetric metric, LogConfig logConfig)
    {
        var dicStatisticByHour = new Dictionary<int, StatisticByHourItem>();
        foreach (var item in clicks)
        {
            //+nếu là click ảo thì ko xử lý (Application FdClick sẽ xử lý) 
            //+ Cờ ko log redis chỉ log SQL
            if (!item.IsFraud) // TODO: # Hòa NX - không còn application FdClick nữa (chạy job) thì còn cờ này không nhỉ ?
            {
                var hour = item.CreatedOn.GetHourOfYear();
                if (!dicStatisticByHour.ContainsKey(hour))
                {
                    dicStatisticByHour.Add(hour, new StatisticByHourItem());
                    dicStatisticByHour[hour].DayOfYear = (int) item.CreatedOn.DayOfWeek;
                }

                dicStatisticByHour[hour].Add(item);
            }
        }

        //Save log
        foreach (var statisticByHourItem in dicStatisticByHour)
        {
            var day = statisticByHourItem.Value.DayOfYear.ToString(CultureInfo.InvariantCulture);
            await _redisCacheService.ExecuteTransaction(RedisDatabases.ServiceReport, transaction =>
            {
                SingleObjLog(transaction, statisticByHourItem, day, logConfig, metric, arrMoney);
                AdverTargetingLog(transaction, statisticByHourItem, day, logConfig, metric, arrMoney);
                AdverPubLog(transaction, statisticByHourItem, day, logConfig, metric, arrMoney);

                // await PubTargetingLog(transaction, statisticByHourItem, day);
                //await AdverTargetingOnlyLog(trans, statisticByHourItem);
                return Task.CompletedTask;
            });
        }
    }

    private void SingleObjLog(
        ITransaction trans,
        KeyValuePair<int, StatisticByHourItem> statisticByHourItem,
        string day,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        var hourParts = DateTimeExtensions.GetHourParts(statisticByHourItem.Key);

        //Cas enum to list
        //var lstPubObj = Enum.GetValues(typeof(EPublisherObject)).Cast<EPublisherObject>().ToList();
        //var lstAdverObj = Enum.GetValues(typeof(EAdvertiserObject)).Cast<EAdvertiserObject>().ToList();
        //var lstTargetObj = Enum.GetValues(typeof(ETarget)).Cast<ETarget>().ToList();

        var lstAdverObj = logConfig.EAdverForSingleObjLog;
        var lstAdverMultipleObj = logConfig.EAdverForMultipleObjLog;
        var lstPubObj = logConfig.EPubForSingleObjLog;
        var lstClientObj = logConfig.EClientSingleObjLog;

        #region publisher

        if (lstPubObj != null)
        {
            var pubStatistics = statisticByHourItem.Value.Publisher.StatisticByPublisherObj;
            foreach (var ePublisherObject in lstPubObj)
            {
                if (pubStatistics.ContainsKey(ePublisherObject))
                {
                    var log = pubStatistics[ePublisherObject];
                    RenderTransactionForSinglePubObj(trans, log, day, hourParts, ePublisherObject, logConfig,
                        metric, arrMoney);
                }
            }
        }

        #endregion

        #region Advertiser

        if (lstAdverObj != null)
        {
            var adverStatistic = statisticByHourItem.Value.Advertiser.StatisticByAdverObj;
            foreach (var eAdverObj in lstAdverObj)
            {
                if (adverStatistic.ContainsKey(eAdverObj))
                {
                    var log = adverStatistic[eAdverObj];
                    RenderTransactionForSingleAdverObj(trans, log, day, hourParts, eAdverObj, logConfig, metric, arrMoney);
                }
            }
        }

        if (lstAdverMultipleObj is not null)
        {
            var multipleAdverStatistic = statisticByHourItem.Value.StatisticMutlipleAdver;
            foreach (var eAdverObj in lstAdverMultipleObj)
            {
                var multipleAdvertiserDimensionKey = string.Join('_', eAdverObj);
                if (multipleAdverStatistic.ContainsKey(multipleAdvertiserDimensionKey))
                {
                    var logs = multipleAdverStatistic[multipleAdvertiserDimensionKey];
                    RenderTransactionForMultipleAdverObj(trans, logs, day, hourParts, eAdverObj, logConfig, metric, arrMoney);
                }
            }
        }

        #endregion
        
        #region Client

        if (lstClientObj != null)
        {
            var clientStatistic = statisticByHourItem.Value.Client._statisticByClientObj;
            foreach (var eClientObj in lstClientObj)
            {
                if (clientStatistic.ContainsKey(eClientObj))
                {
                    var log = clientStatistic[eClientObj];
                    RenderTransactionForSingleClientObj(trans, log, day, hourParts, eClientObj, logConfig, metric, arrMoney);
                }
            }
        }

        #endregion
    }

    private void AdverPubLog(
        ITransaction trans,
        KeyValuePair<int, StatisticByHourItem> statisticByHourItem,
        string day,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        var logs = statisticByHourItem.Value.StatisticAdverPublisher;
        RenderTransactionForAdverPublisherReport(trans, logs, day, logConfig, metric, arrMoney);
    }
    
    private void AdverTargetingLog(
        ITransaction trans,
        KeyValuePair<int, StatisticByHourItem> statisticByHourItem,
        string day,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        var logs = statisticByHourItem.Value.StatisticAdverTargeting;
        RenderTransactionForAdverTargetingReport(trans, logs, day, logConfig, metric, arrMoney);
    }

    #region common func for single obj report

    private void RenderTransactionForSinglePubObj(
        ITransaction trans,
        Dictionary<long, ValueItem> logs,
        string day,
        IList<string> hourParts, ReportPublisherDimension pubObj,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        foreach (var log in logs)
        {
            if (log.Value.Count <= 0 || log.Key <= 0) continue;
            trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, metric, log.Key).KeyName, "0",
                    log.Value.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, metric, log.Key).KeyName, day,
                log.Value.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, metric, hourParts[0], log.Key).KeyName,
                hourParts[1], log.Value.Count);

            if (logConfig.IsLogPubMoney) // TODO: # Hòa NX - anh chưa thấy đoạn nào check BuyPriceType, SellPriceType truyền từ DisplayQuery xuống nhỉ,IsLogPubMoney và IsLogAdverMoney đều gán mặc định true hết từ log service ? 
            {
                if (log.Value.Earned > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, Earned(arrMoney), log.Key).KeyName, "0", log.Value.Earned);
                if (log.Value.PromotionEarned > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, PromotionEarned(arrMoney), log.Key).KeyName, "0", log.Value.PromotionEarned);
                //if (log.Value.Paid > 0) trans.HashIncrementAsync(RedisKeyRender.GetPubKey(_platform,  pubObj, _paid, log.Key), "0", log.Value.Paid);
                //if (log.Value.PromotionPaid > 0) trans.HashIncrementAsync( RedisKeyRender.GetPubKey(_platform,  pubObj, _promotionPaid, log.Key), "0", log.Value.PromotionPaid);
                

                if (log.Value.Earned > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, Earned(arrMoney), log.Key).KeyName, day, log.Value.Earned);
                if (log.Value.PromotionEarned > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, PromotionEarned(arrMoney), log.Key).KeyName, day, log.Value.PromotionEarned);
                //if (log.Value.Paid > 0) trans.HashIncrementAsync( RedisKeyRender.GetPubKey(_platform,  pubObj, _paid, log.Key), day, log.Value.Paid);
                //if (log.Value.PromotionPaid > 0) trans.HashIncrementAsync( RedisKeyRender.GetPubKey(_platform,  pubObj, _promotionPaid, log.Key), day, log.Value.PromotionPaid);


                if (log.Value.Earned > 0) 
                    trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, Earned(arrMoney), hourParts[0], log.Key).KeyName, hourParts[1], log.Value.Earned);
                if (log.Value.PromotionEarned > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetPubKey(pubObj, PromotionEarned(arrMoney), hourParts[0], log.Key).KeyName, hourParts[1], log.Value.PromotionEarned);
                //if (log.Value.Paid > 0) trans.HashIncrementAsync( RedisKeyRender.GetPubKey(_platform,  pubObj, _paid, hourParts[0], log.Key), hourParts[1], log.Value.Paid);
                //if (log.Value.PromotionPaid > 0) trans.HashIncrementAsync( RedisKeyRender.GetPubKey(_platform,  pubObj, _promotionPaid, hourParts[0], log.Key), hourParts[1], log.Value.PromotionPaid);
            }
        }
    }

    private void RenderTransactionForSingleClientObj
    (
        ITransaction trans,
        Dictionary<Guid, ValueItem> logs,
        string day,
        IList<string> hourParts, ReportClientDimension clientObj,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        foreach (var log in logs)
        {
            if (log.Value.Count <= 0) continue;
            trans.HashIncrementAsync(RedisReportKeys.GetClientKey(clientObj, metric, log.Key).KeyName, "0",
                    log.Value.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetClientKey(clientObj, metric, log.Key).KeyName, day,
                log.Value.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetClientKey(clientObj, metric, hourParts[0], log.Key).KeyName,
                hourParts[1], log.Value.Count);
        }
    }
        
    private void RenderTransactionForSingleAdverObj(
        ITransaction trans,
        Dictionary<long, ValueItem> logs,
        string day,
        IList<string> hourParts,
        ReportAdvertiserDimension adverObj,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        foreach (var log in logs)
        {
            if (log.Value.Count <= 0 || log.Key <= 0) continue;

            trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, metric, log.Key).KeyName, "0",
                log.Value.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, metric, log.Key).KeyName, day,
                log.Value.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, metric, hourParts[0], log.Key).KeyName,
                hourParts[1], log.Value.Count);

            if (logConfig.IsLogAdverMoney) // TODO: # Hòa NX - Tương tự log tiền pub, chưa tính theo cờ đánh dấu loại tính tiền mua/bán CPC/CPM
            {
                if (log.Value.Paid > 0) 
                    trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, Paid(arrMoney), log.Key).KeyName, "0", log.Value.Paid);
                if (log.Value.PromotionPaid > 0) 
                    trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, PromotionPaid(arrMoney), log.Key).KeyName, "0", log.Value.PromotionPaid);
                //if (log.Value.Earned > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(_platform,  adverObj, _earned, log.Key), "0", log.Value.Earned);
                //if (log.Value.PromotionEarned > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(_platform,  adverObj, _promotionEarned, log.Key), "0", log.Value.PromotionEarned);
            
                if (log.Value.Paid > 0) 
                    trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, Paid(arrMoney), log.Key).KeyName, day, log.Value.Paid);
                if (log.Value.PromotionPaid > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, PromotionPaid(arrMoney), log.Key).KeyName, day, log.Value.PromotionPaid);
                //if (log.Value.Earned > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(_platform,  adverObj, _earned, log.Key), day, log.Value.Earned);
                //if (log.Value.PromotionEarned > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(_platform,  adverObj, _promotionEarned, log.Key), day, log.Value.PromotionEarned);
            
                if (log.Value.Paid > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, Paid(arrMoney), hourParts[0], log.Key).KeyName, hourParts[1], log.Value.Paid);
                if (log.Value.PromotionPaid > 0) trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, PromotionPaid(arrMoney), hourParts[0], log.Key).KeyName, hourParts[1], log.Value.PromotionPaid);
                //if (log.Value.Earned > 0) trans.HashIncrementAsync( RedisKeyRender.GetAdverKey(_platform,  adverObj, _earned, hourParts[0], log.Key), hourParts[1], log.Value.Earned);
                //if (log.Value.PromotionEarned > 0) trans.HashIncrementAsync( RedisKeyRender.GetAdverKey(_platform,  adverObj, _promotionEarned, hourParts[0], log.Key), hourParts[1], log.Value.PromotionEarned);
            }
        }
    }

    private void RenderTransactionForMultipleAdverObj(
        ITransaction trans,
        Dictionary<string, SumByAdvertiser> logs,
        string day,
        IList<string> hourParts,
        List<ReportAdvertiserDimension> adverObj,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        foreach (var log in logs)
        {
            if (log.Value.StatisticByAdverObj.Count <= 0) continue;
            var logKeys = log.Key.Split('_').Select(x => long.TryParse(x, out var value) ? value : default).ToList();
            var logItem = adverObj.Select((dimension, index) =>
            {
                var key = logKeys.ElementAtOrDefault(index);
                if (!log.Value.StatisticByAdverObj.ContainsKey(dimension))
                {
                    return new ValueItem();
                }
                return log.Value.StatisticByAdverObj[dimension].ContainsKey(key) ? log.Value.StatisticByAdverObj[dimension][key] : new ValueItem();
            }).MinBy(x => x.Count) ?? new ValueItem();
            trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, metric, logKeys).KeyName, "0",
                logItem.Count);
            trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, metric, logKeys).KeyName, day,
                logItem.Count);
            if (logConfig.IsLogAdverMoney)
            {
                if (logItem.Paid > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, Paid(arrMoney), logKeys).KeyName,
                        "0", logItem.Paid);
                if (logItem.PromotionPaid > 0)
                    trans.HashIncrementAsync(
                        RedisReportKeys.GetAdverKey(adverObj, PromotionPaid(arrMoney), logKeys).KeyName, "0",
                        logItem.PromotionPaid);

                if (logItem.Paid > 0)
                    trans.HashIncrementAsync(RedisReportKeys.GetAdverKey(adverObj, Paid(arrMoney), logKeys).KeyName,
                        day, logItem.Paid);
                if (logItem.PromotionPaid > 0)
                    trans.HashIncrementAsync(
                        RedisReportKeys.GetAdverKey(adverObj, PromotionPaid(arrMoney), logKeys).KeyName, day,
                        logItem.PromotionPaid);
            }
        }
    }

    //Advertiser - Publisher
    private void RenderTransactionForAdverPublisherReport(
        ITransaction trans,
        Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByPublisher>> logs,
        string day,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        //var lstAdverObj = Enum.GetValues(typeof(EAdvertiserObject)).Cast<EAdvertiserObject>().ToList();
        //var lstPublisherObj = Enum.GetValues(typeof(EPublisherObject)).Cast<EPublisherObject>().ToList();            
        var lstAdverObj = logConfig.EAdverForAdverPub;
        var lstPublisherObj = logConfig.EPubForAdverPub;

        if (lstAdverObj != null && lstPublisherObj != null)
        {
            //Browse all advertiser obj
            foreach (var adverObject in lstAdverObj)
            {
                // if statistic of "advertiser obj by targeting" has this publisher obj
                if (logs.ContainsKey(adverObject))
                {
                    //Get log of targeting for this publisher obj
                    var logOfObj = logs[adverObject];
                    //Browse all targeting log
                    foreach (var sumByTargeting in logOfObj)
                    {
                        var allLogByPublisher = sumByTargeting.Value.StatisticByPublisherObj;
                        foreach (var ePublisher in lstPublisherObj)
                        {
                            if (allLogByPublisher.ContainsKey(ePublisher))
                            {
                                var log = allLogByPublisher[ePublisher];
                                if (sumByTargeting.Key > 0)
                                {
                                    AdverPublisherTrans(trans, log, day, adverObject, ePublisher,
                                        sumByTargeting.Key, logConfig, metric, arrMoney);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void AdverPublisherTrans(
        ITransaction trans,
        Dictionary<long, ValueItem> logs,
        string day,
        ReportAdvertiserDimension advertiserObject,
        ReportPublisherDimension pubObj,
        long adverObjId,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        foreach (var log in logs)
        {
            if (log.Value.Count <= 0 || log.Key <= 0) continue;
            if (log.Value.Count > 0)
            {
                trans.HashIncrementAsync(
                    RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, metric, adverObjId, log.Key).KeyName, "0",
                    log.Value.Count);
                trans.HashIncrementAsync(
                    RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, metric, adverObjId, log.Key).KeyName, day,
                    log.Value.Count);

                if (logConfig.IsLogPubMoney)
                {
                    //total
                    if (log.Value.Earned > 0) 
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, Earned(arrMoney), adverObjId, log.Key).KeyName, "0", log.Value.Earned);
                    if (log.Value.PromotionEarned > 0)
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, PromotionEarned(arrMoney), adverObjId, log.Key).KeyName, "0", log.Value.PromotionEarned);
                
                    //day
                    if (log.Value.Earned > 0)
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, Earned(arrMoney), adverObjId, log.Key).KeyName, day, log.Value.Earned);
                    if (log.Value.PromotionEarned > 0) 
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, PromotionEarned(arrMoney), adverObjId, log.Key).KeyName, day, log.Value.PromotionEarned);   
                }
                
                if (logConfig.IsLogAdverMoney)
                {
                    //total
                    if (log.Value.Paid > 0) 
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, Paid(arrMoney), adverObjId, log.Key).KeyName, "0", log.Value.Paid);
                    if (log.Value.PromotionPaid > 0)
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, PromotionPaid(arrMoney), adverObjId, log.Key).KeyName, "0", log.Value.PromotionPaid);
                
                    //day
                    if (log.Value.Paid > 0)
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, Paid(arrMoney), adverObjId, log.Key).KeyName, day, log.Value.Paid);
                    if (log.Value.PromotionPaid > 0) 
                        trans.HashIncrementAsync(RedisReportKeys.GetAdver_PubKey(advertiserObject, pubObj, PromotionPaid(arrMoney), adverObjId, log.Key).KeyName, day, log.Value.PromotionPaid);
                }

                #region set key

                trans.SetAddAsync(
                    RedisReportKeys.GetAdver_PubSetKey(advertiserObject, pubObj, ReportMetric.View, adverObjId).KeyName,
                    log.Key.ToString());
                trans.SetAddAsync(
                    RedisReportKeys.GetPub_AdverSetKey(advertiserObject, pubObj, ReportMetric.View, log.Key).KeyName,
                    adverObjId.ToString());

                #endregion
            }
        }
    }

    #endregion

    #region Common func for obj by obj report

    //Advertiser - Targeting
    private void RenderTransactionForAdverTargetingReport(
        ITransaction trans,
        Dictionary<ReportAdvertiserDimension, Dictionary<long, SumByTargeting>> logs,
        string day,
        LogConfig logConfig,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        var lstAdverObj = logConfig.EAdverForAdverTargeting;
        var lstTargetObj = logConfig.ETargetForAdverTargeting;

        if (lstAdverObj != null && lstTargetObj != null)
        {
            //Browse all advertiser obj
            foreach (var adverObject in lstAdverObj)
            {
                // if statistic of "advertiser obj by targeting" has this publisher obj
                if (logs.ContainsKey(adverObject))
                {
                    //Get log of targeting for this publisher obj
                    var logOfObj = logs[adverObject];
                    //Browse all targeting log
                    foreach (var sumByTargeting in logOfObj)
                    {
                        var allLogByTarget = sumByTargeting.Value.StatisticByTargetingObj;
                        foreach (var eTarget in lstTargetObj)
                        {
                            if (allLogByTarget.ContainsKey(eTarget))
                            {
                                var log = allLogByTarget[eTarget];
                                if (sumByTargeting.Key > 0)
                                {
                                    AdverTargetingTrans(trans, log, day, logConfig, adverObject, eTarget, sumByTargeting.Key, metric, arrMoney);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void AdverTargetingTrans(
        ITransaction trans,
        Dictionary<long, ValueItem> logs,
        string day,
        LogConfig logConfig,
        ReportAdvertiserDimension advertiserObject,
        ReportTargetingDimension targetObj,
        long adverObjId,
        ReportMetric metric,
        ReportMetric[] arrMoney)
    {
        foreach (var log in logs)
        {
            //if (targetObj == ETarget.HARDWAREFAMILY || targetObj == ETarget.HARDWAREMODEL) continue;
            if (log.Value.Count <= 0 || log.Key <= 0) continue;

            if (log.Value.Count > 0)
            {
                trans.HashIncrementAsync(
                    RedisReportKeys.GetAdver_TargetingKey(advertiserObject, targetObj, metric, adverObjId,
                        log.Key).KeyName, "0", log.Value.Count);
                trans.HashIncrementAsync(
                    RedisReportKeys.GetAdver_TargetingKey(advertiserObject, targetObj, metric, adverObjId,
                        log.Key).KeyName, day, log.Value.Count);

                if (logConfig.IsLogAdverMoney)
                {
                    //if (log.Value.Earned > 0) trans.HashIncrementAsync( RedisKeyRender.GetAdver_TargetingKey(_platform,  advertiserObject, targetObj, _earned, adverObjId, log.Key), "0", log.Value.Earned);
                    //if (log.Value.PromotionEarned > 0) trans.HashIncrementAsync(RedisKeyRender.GetAdver_TargetingKey(_platform,  advertiserObject, targetObj, _promotionEarned, adverObjId, log.Key), "0", log.Value.PromotionEarned);

                    if (log.Value.Paid > 0)
                        trans.HashIncrementAsync(
                            RedisReportKeys.GetAdver_TargetingKey(advertiserObject, targetObj, Paid(arrMoney),
                                adverObjId, log.Key).KeyName, "0", log.Value.Paid);
                    if (log.Value.PromotionPaid > 0)
                        trans.HashIncrementAsync(
                            RedisReportKeys.GetAdver_TargetingKey(advertiserObject, targetObj, PromotionPaid(arrMoney),
                                adverObjId, log.Key).KeyName, "0", log.Value.PromotionPaid);

                    //if (log.Value.Earned > 0) trans.HashIncrementAsync( RedisKeyRender.GetAdver_TargetingKey(_platform,  advertiserObject, targetObj, _earned, adverObjId, log.Key), day, log.Value.Earned);
                    //if (log.Value.PromotionEarned > 0) trans.HashIncrementAsync(RedisKeyRender.GetAdver_TargetingKey(_platform,  advertiserObject, targetObj, _promotionEarned, adverObjId, log.Key), day, log.Value.PromotionEarned);

                    if (log.Value.Paid > 0)
                        trans.HashIncrementAsync(
                            RedisReportKeys.GetAdver_TargetingKey(advertiserObject, targetObj, Paid(arrMoney),
                                adverObjId, log.Key).KeyName, day, log.Value.Paid);
                    if (log.Value.PromotionPaid > 0)
                        trans.HashIncrementAsync(
                            RedisReportKeys.GetAdver_TargetingKey(advertiserObject, targetObj, PromotionPaid(arrMoney),
                                adverObjId, log.Key).KeyName, day, log.Value.PromotionPaid);
                }
            }
        }
    }
    
    #endregion
    
    private static ReportMetric Earned(ReportMetric[] arrMoney)
    {
        return arrMoney.ElementAtOrDefault(0);
    }
    
    private static ReportMetric PromotionEarned(ReportMetric[] arrMoney)
    {
        return arrMoney.ElementAtOrDefault(1);
    }
    
    private static ReportMetric Paid(ReportMetric[] arrMoney)
    {
        return arrMoney.ElementAtOrDefault(2);
    }
    
    private static ReportMetric PromotionPaid(ReportMetric[] arrMoney)
    {
        return arrMoney.ElementAtOrDefault(3);
    }
}