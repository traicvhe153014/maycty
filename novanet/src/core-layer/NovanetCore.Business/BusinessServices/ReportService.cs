﻿using System.Text;
using Novanet.Core.Constants;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessManager;
using NovanetCore.Business.BusinessUtils;

namespace NovanetCore.Business.BusinessServices;

public interface IReportService
{
    Task<Dictionary<long, double>> GetAdvertiserReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds);
    
    Task<Dictionary<string, double>> GetMultipleAdvertiserReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        List<ReportAdvertiserDimension> dimensions,
        List<List<object>> objectIds);
    
    Task<Dictionary<string, double>> GetTotalMultipleAdvertiserReport(
        ReportMetric metric,
        List<ReportAdvertiserDimension> dimensions,
        List<List<object>> objectIds);

    Task<Dictionary<long, double>> GetTotalAdvertiserReport(
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds);

    Task<double> GetSingleAdvertiserReport(
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        object objectId);
        
    Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserReportByHour(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds);

    Task<Dictionary<long, double>> GetPublisherReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportPublisherDimension dimension,
        List<object> objectIds);
    
    Task<Dictionary<long, double>> GetTotalPublisherReport(
        ReportMetric metric,
        ReportPublisherDimension dimension,
        List<object> objectIds);

    Task<Dictionary<Guid, double>> GetClientReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportClientDimension dimension,
        List<object> objectIds);

    Task<double> GetSingleClientReport(
        ReportMetric metric,
        ReportClientDimension dimension,
        object objectId);
    
    Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserTargetingReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportTargetingDimension targetingDimension,
        List<object> advertiserIds,
        List<object> targetingIds);
    
    Task<Dictionary<long, Dictionary<string, object>>> GetTotalAdvertiserTargetingReport(
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportTargetingDimension targetingDimension,
        List<object> advertiserIds,
        List<object> targetingIds);
    
    Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserPublisherReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportPublisherDimension publisherDimension,
        List<object> advertiserIds,
        List<object> publisherIds);
    
    Task<Dictionary<long, Dictionary<string, object>>> GetTotalAdvertiserPublisherReport(
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportPublisherDimension publisherDimension,
        List<object> advertiserIds,
        List<object> publisherIds);

    Task<bool> MatchAllKeys(int database, List<RedisKey> redisKeys);

    Task<bool> MatchAllKeys(int database, RedisKeyTree redisKeyTree);

    List<long> GetSortedAdvertiserIds(
        DateTimeOffset? fromDate,
        DateTimeOffset? toDate,
        ReportAdvertiserDimension dimension,
        MetricExpression metricExpression,
        bool isDescending = false);

    Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserReport(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds);
}

public class ReportService : IReportService
{
    private readonly IRedisCacheService _redisCacheService;

    public ReportService(IRedisCacheService redisCacheService)
    {
        _redisCacheService = redisCacheService;
    }

    public async Task<Dictionary<long, double>> GetAdvertiserReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds)
    {
        var from = fromDate.GetDayOfYear();
        var to = toDate.GetDayOfYear();

        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine($"for j={from},{to} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, j) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, double>>() ?? new Dictionary<long, double>();
        return result;
    }

    public async Task<Dictionary<string, double>> GetMultipleAdvertiserReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        List<ReportAdvertiserDimension> dimensions,
        List<List<object>> objectIds)
    {
        if (dimensions.Count == 0 || objectIds.Count == 0)
        {
            return new Dictionary<string, double>();
        }
        var from = fromDate.GetDayOfYear(); 
        var to = toDate.GetDayOfYear();

        var dimensionString = string.Join('_', dimensions.ToArray());
        var combinedObjectIds = MathUtils.GetAllPossibleCombos(objectIds, '_');
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", combinedObjectIds.Select(x => $"'{x}'").ToArray())}}};");
        script.AppendLine($"for i=1,{combinedObjectIds.Count} do");
        script.AppendLine($"for j={from},{to} do");
        script.AppendLine("local objectId = objectIds[i]");
        script.AppendLine($"local metric = redis.call('hget', '{dimensionString}_{metric}_' .. objectId, j) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");
        
        var scriptString = script.ToString();
            
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<string, double>>() ?? new Dictionary<string, double>();
        return result;
    }

    public async Task<Dictionary<string, double>> GetTotalMultipleAdvertiserReport(
        ReportMetric metric,
        List<ReportAdvertiserDimension> dimensions,
        List<List<object>> objectIds)
    {
        if (dimensions.Count == 0 || objectIds.Count == 0)
        {
            return new Dictionary<string, double>();
        }

        var dimensionString = string.Join('_', dimensions.ToArray());
        var combinedObjectIds = MathUtils.GetAllPossibleCombos(objectIds, '_');
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", combinedObjectIds.Select(x => $"'{x}'").ToArray())}}};");
        script.AppendLine($"for i=1,{combinedObjectIds.Count} do");
        script.AppendLine("local objectId = objectIds[i]");
        script.AppendLine($"local metric = redis.call('hget', '{dimensionString}_{metric}_' .. objectId, 0) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");
        
        var scriptString = script.ToString();
            
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<string, double>>() ?? new Dictionary<string, double>();
        return result;
    }

    public async Task<Dictionary<long, double>> GetTotalAdvertiserReport(
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds)
    {
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, 0) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, double>>() ?? new Dictionary<long, double>();
        return result;
    }

    public async Task<double> GetSingleAdvertiserReport(
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        object objectId)
    {
        var script = new StringBuilder();
        script.AppendLine($"local objectId = tostring({objectId})");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, 0) or 0;");
        script.AppendLine("return metric;");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        return double.TryParse(executeResult, out var result) ? result : default;
    }

    public async Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserReportByHour(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension dimension,
        List<object> objectIds)
    {
        var from = fromDate.GetDayOfYear(); 
        var to = toDate.GetDayOfYear();

        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine("table[objectId] = {}");
        script.AppendLine($"for j={from},{to} do");
        script.AppendLine($"for k={0},{23} do");
        script.AppendLine("local hourOfYear = (j - 1) * 24 + (k - 7);");
        script.AppendLine("local firstPart = math.floor(hourOfYear / 500);");
        script.AppendLine("local secondPart = hourOfYear % 500;");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId .. '_' .. firstPart, secondPart) or 0;");
        script.AppendLine("table[objectId][tostring(k)] = (table[objectId][tostring(k)] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");
        
        var scriptString = script.ToString();
            
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, object>>() ??
                     new Dictionary<long, object>();
        var returnValue =
            result.ToDictionary(x => x.Key, x => x.Value.ToString().Deserialize<Dictionary<string, object>>());

        return returnValue;
    }

    public async Task<Dictionary<long, double>> GetPublisherReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportPublisherDimension dimension,
        List<object> objectIds)
    {
        var from = fromDate.GetDayOfYear();
        var to = toDate.GetDayOfYear();

        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine($"for j={from},{to} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, j) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, double>>() ?? new Dictionary<long, double>();
        return result;
    }

    public async Task<Dictionary<long, double>> GetTotalPublisherReport(
        ReportMetric metric,
        ReportPublisherDimension dimension,
        List<object> objectIds)
    {
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, 0) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, double>>() ?? new Dictionary<long, double>();
        return result;
    }

    public async Task<Dictionary<Guid, double>> GetClientReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportClientDimension dimension,
        List<object> objectIds)
    {
        var from = fromDate.GetDayOfYear();
        var to = toDate.GetDayOfYear();

        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.Select(x => $"'{x}'").ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine($"for j={from},{to} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, j) or 0;");
        script.AppendLine("table[objectId] = (table[objectId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<Guid, double>>() ?? new Dictionary<Guid, double>();
        return result;
    }

    public async Task<double> GetSingleClientReport(
        ReportMetric metric,
        ReportClientDimension dimension,
        object objectId)
    {
        var script = new StringBuilder();
        script.AppendLine($"local objectId = '{objectId}'");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, 0) or 0;");
        script.AppendLine("return metric;");

        var scriptString = script.ToString();

        var executeResult = await _redisCacheService.ExecuteLuaScript((int)RedisDatabases.ServiceReport, scriptString);
        return double.TryParse(executeResult, out var result) ? result : default;
    }

    public async Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserTargetingReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportTargetingDimension targetingDimension,
        List<object> advertiserIds,
        List<object> targetingIds)
    {
        var from = fromDate.GetDayOfYear(); 
        var to = toDate.GetDayOfYear();

        var dimensionString = $"{advertiserDimension}_{targetingDimension}";
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local advertiserIds = {{{string.Join(", ", advertiserIds.ToArray())}}};");
        script.AppendLine($"local targetingIds = {{{string.Join(", ", targetingIds.ToArray())}}};");
        script.AppendLine($"for i=1,{advertiserIds.Count} do");
        script.AppendLine("local advertiserId = tostring(advertiserIds[i])");
        script.AppendLine("table[advertiserId] = {}");
        script.AppendLine($"for j=1,{targetingIds.Count} do");
        script.AppendLine("local targetingId = tostring(targetingIds[j])");
        script.AppendLine($"for k={from},{to} do");
        script.AppendLine($"local metric = redis.call('hget', '{dimensionString}_{metric}_' .. advertiserId .. '_' .. targetingId, k) or 0;");
        script.AppendLine("table[advertiserId][targetingId] = (table[advertiserId][targetingId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, object>>() ??
                     new Dictionary<long, object>();
        var returnValue = result.ToDictionary(x => x.Key, x => x.Value.ToString().Deserialize<Dictionary<string, object>>());

        return returnValue;
    }

    public async Task<Dictionary<long, Dictionary<string, object>>> GetTotalAdvertiserTargetingReport(
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportTargetingDimension targetingDimension,
        List<object> advertiserIds,
        List<object> targetingIds)
    {
        var dimensionString = $"{advertiserDimension}_{targetingDimension}";
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local advertiserIds = {{{string.Join(", ", advertiserIds.ToArray())}}};");
        script.AppendLine($"local targetingIds = {{{string.Join(", ", targetingIds.ToArray())}}};");
        script.AppendLine($"for i=1,{advertiserIds.Count} do");
        script.AppendLine("local advertiserId = tostring(advertiserIds[i])");
        script.AppendLine("table[advertiserId] = {}");
        script.AppendLine($"for j=1,{targetingIds.Count} do");
        script.AppendLine("local targetingId = tostring(targetingIds[j])");
        script.AppendLine($"local metric = redis.call('hget', '{dimensionString}_{metric}_' .. advertiserId .. '_' .. targetingId, 0) or 0;");
        script.AppendLine("table[advertiserId][targetingId] = (table[advertiserId][targetingId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, object>>() ??
                     new Dictionary<long, object>();
        var returnValue = result.ToDictionary(x => x.Key, x => x.Value.ToString().Deserialize<Dictionary<string, object>>());

        return returnValue;
    }
    
    public async Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserPublisherReportByDay(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportPublisherDimension publisherDimension,
        List<object> advertiserIds,
        List<object> publisherIds)
    {
        var from = fromDate.GetDayOfYear(); 
        var to = toDate.GetDayOfYear();

        var dimensionString = $"{advertiserDimension}_{publisherDimension}";
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local advertiserIds = {{{string.Join(", ", advertiserIds.ToArray())}}};");
        script.AppendLine($"local publisherIds = {{{string.Join(", ", publisherIds.ToArray())}}};");
        script.AppendLine($"for i=1,{advertiserIds.Count} do");
        script.AppendLine("local advertiserId = tostring(advertiserIds[i])");
        script.AppendLine("table[advertiserId] = {}");
        script.AppendLine($"for j=1,{publisherIds.Count} do");
        script.AppendLine("local publisherId = tostring(publisherIds[j])");
        script.AppendLine($"for k={from},{to} do");
        script.AppendLine($"local metric = redis.call('hget', '{dimensionString}_{metric}_' .. advertiserId .. '_' .. publisherId, k) or 0;");
        script.AppendLine("table[advertiserId][publisherId] = (table[advertiserId][publisherId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, object>>() ??
                     new Dictionary<long, object>();
        var returnValue = result.ToDictionary(x => x.Key, x => x.Value.ToString().Deserialize<Dictionary<string, object>>());

        return returnValue;
    }

    public async Task<Dictionary<long, Dictionary<string, object>>> GetTotalAdvertiserPublisherReport(
        ReportMetric metric,
        ReportAdvertiserDimension advertiserDimension,
        ReportPublisherDimension publisherDimension,
        List<object> advertiserIds,
        List<object> publisherIds)
    {
        var dimensionString = $"{advertiserDimension}_{publisherDimension}";
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local advertiserIds = {{{string.Join(", ", advertiserIds.ToArray())}}};");
        script.AppendLine($"local publisherIds = {{{string.Join(", ", publisherIds.ToArray())}}};");
        script.AppendLine($"for i=1,{advertiserIds.Count} do");
        script.AppendLine("local advertiserId = tostring(advertiserIds[i])");
        script.AppendLine("table[advertiserId] = {}");
        script.AppendLine($"for j=1,{publisherIds.Count} do");
        script.AppendLine("local publisherId = tostring(publisherIds[j])");
        script.AppendLine($"local metric = redis.call('hget', '{dimensionString}_{metric}_' .. advertiserId .. '_' .. publisherId, 0) or 0;");
        script.AppendLine("table[advertiserId][publisherId] = (table[advertiserId][publisherId] or 0) + metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");

        var scriptString = script.ToString();
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, object>>() ??
                     new Dictionary<long, object>();
        var returnValue = result.ToDictionary(x => x.Key, x => x.Value.ToString().Deserialize<Dictionary<string, object>>());

        return returnValue;
    }

    public async Task<bool> MatchAllKeys(int database, List<RedisKey> redisKeys)
    {
        var script = new StringBuilder();
        var redisKeysObject = redisKeys.Select(x => $"'{x.KeyName}'").JoinString(",") ?? string.Empty;

        script.AppendLine($"local keys = {{{redisKeysObject}}}");
        script.AppendLine($"for i=1,{redisKeys.Count} do");
        script.AppendLine("local count = redis.call('get', keys[i]) or 0");
        script.AppendLine("if count == 0 then return false end");
        script.AppendLine("end");
        script.AppendLine("return true");

        var scriptString = script.ToString();
        var executeResult = await _redisCacheService.ExecuteLuaScript(database, scriptString);
        return executeResult == "1";
    }

    public async Task<bool> MatchAllKeys(int database, RedisKeyTree redisKeyTree)
    {
        var script = new StringBuilder();
        script.AppendLine("local keys = {}");
        for (var i = 0; i < redisKeyTree.Nodes.Count; i++)
        {
            var node = redisKeyTree.Nodes[i];
            var includeTable = node.Includes.Select(x => $"'{x.KeyName}'").JoinString(",") ?? string.Empty;
            var excludeTable = node.Excludes.Select(x => $"'{x.KeyName}'").JoinString(",") ?? string.Empty;
            script.AppendLine($"keys[{i + 1}] = {{ includes = {{{includeTable}}}, excludes = {{{excludeTable}}} }}");
        }

        script.AppendLine($"for i=1,{redisKeyTree.Nodes.Count} do");
        script.AppendLine("local check = true");

        script.AppendLine("local checkIncludes = false");
        script.AppendLine("for k,v in ipairs(keys[i]['includes']) do");
        script.AppendLine("local checkIncludeItem = redis.call('get', v) or 0");
        script.AppendLine("if checkIncludeItem ~= 0 then");
        script.AppendLine("checkIncludes = true");
        script.AppendLine("break");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("if checkIncludes then");
        script.AppendLine("local checkExcludes = true");
        script.AppendLine("for k,v in ipairs(keys[i]['excludes']) do");
        script.AppendLine("local checkExcludeItem = redis.call('get', v) or 0");
        script.AppendLine("if checkExcludeItem ~= 0 then");
        script.AppendLine("checkExcludes = false");
        script.AppendLine("break");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("if checkExcludes then return true end");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return false");

        var scriptString = script.ToString();
        var executeResult = await _redisCacheService.ExecuteLuaScript(database, scriptString);
        return executeResult == "1";
    }

    public List<long> GetSortedAdvertiserIds(
        DateTimeOffset? fromDate,
        DateTimeOffset? toDate,
        ReportAdvertiserDimension dimension,
        MetricExpression metricExpression,
        bool isDescending = false)
    {
        if (MetricReportManager.AdvertiserReport is null) return new List<long>();
        MetricReportManager.AdvertiserReport.TryGetValue(dimension, out var dimensionReports);
        if (dimensionReports?.Data is null) return new List<long>();
        var result = new Dictionary<long, double>();

        var hasDateRange = fromDate is not null && toDate is not null;

        var from = hasDateRange ? fromDate!.Value.GetDayOfYear() : 0;
        var to = hasDateRange ? toDate!.Value.GetDayOfYear() : 0;

        var firstMetric = metricExpression.ReportMetrics.First();
        var secondMetric = metricExpression.ReportMetrics.ElementAtOrDefault(1);

        foreach (var reportByMetric in dimensionReports.Data)
        {
            var totalFirstMetricValue = 0d;
            var totalSecondMetricValue = 0d;
            reportByMetric.Value.Metrics.TryGetValue(firstMetric, out var firstMetricValues);
            if (firstMetricValues?.Values is not null)
                for (var i = from; i <= to; i++)
                {
                    firstMetricValues.Values.TryGetValue(i, out var firstMetricValue);
                    totalFirstMetricValue += firstMetricValue;
                }

            if (metricExpression.ExpressionType != MetricExpressionType.Equal && secondMetric != default)
            {
                reportByMetric.Value.Metrics.TryGetValue(secondMetric, out var secondMetricValues);
                if (secondMetricValues?.Values is not null)
                    for (var i = from; i <= to; i++)
                    {
                        secondMetricValues.Values.TryGetValue(i, out var secondMetricValue);
                        totalSecondMetricValue += secondMetricValue;
                    }
            }

            var finalResult = MathUtils.CalculateMetric(metricExpression.ExpressionType, totalFirstMetricValue, totalSecondMetricValue);
            result[reportByMetric.Key] = finalResult;
        }

        if (isDescending)
            return result
                .OrderByDescending(x => x.Value)
                .Select(x => x.Key)
                .ToList();

        return result
            .OrderBy(x => x.Value)
            .Select(x => x.Key)
            .ToList();
    }
    
    public async Task<Dictionary<long, Dictionary<string, object>>> GetAdvertiserReport(
        DateTimeOffset fromDate,
        DateTimeOffset toDate,
        ReportMetric metric, 
        ReportAdvertiserDimension dimension,
        List<object> objectIds)
    {
        var from = fromDate.GetDayOfYear(); 
        var to = toDate.GetDayOfYear();
        
        var script = new StringBuilder();
        script.AppendLine("local table = {};");
        script.AppendLine($"local objectIds = {{{string.Join(", ", objectIds.ToArray())}}};");
        script.AppendLine($"for i=1,{objectIds.Count} do");
        script.AppendLine("local objectId = tostring(objectIds[i])");
        script.AppendLine("table[objectId] = {}");
        script.AppendLine($"for j={from},{to} do");
        script.AppendLine($"local metric = redis.call('hget', '{dimension}_{metric}_' .. objectId, j) or 0;");
        script.AppendLine("table[objectId][tostring(j)] = metric;");
        script.AppendLine("end");
        script.AppendLine("end");
        script.AppendLine("return cjson.encode(table);");
        
        var scriptString = script.ToString();
            
        var executeResult = await _redisCacheService.ExecuteLuaScript((int) RedisDatabases.ServiceReport, scriptString);
        var result = executeResult?.Deserialize<Dictionary<long, object>>() ??
                     new Dictionary<long, object>();
        var returnValue = result.ToDictionary(x => x.Key, x => x.Value.ToString().Deserialize<Dictionary<string, object>>());

        
        return returnValue;
    }
}