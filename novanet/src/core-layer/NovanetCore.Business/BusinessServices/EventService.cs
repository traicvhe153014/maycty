﻿using System.Text.RegularExpressions;
using Novanet.Core.Enums;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessDomain.Service.Display;
using NovanetCore.Business.BusinessManager;

namespace NovanetCore.Business.BusinessServices;

public interface IEventService
{
    Task GenerateKeys(EventCore @event, ClientManager clientManager);
}

public class EventService : IEventService
{
    private readonly IRedisCacheService _redisCacheService;

    public EventService(IRedisCacheService redisCacheService)
    {
        _redisCacheService = redisCacheService;
    }

    public async Task GenerateKeys(EventCore @event, ClientManager clientManager)
    {
        switch (@event.EventType)
        {
            case EventType.PageView:
                await HandlePageViewKeys(@event, clientManager);
                break;
            case EventType.ProductView:
                await HandleProductViewKeys(@event, clientManager);
                break;
            case EventType.AddToCart:
                await HandleAddToCartKeys(@event, clientManager);
                break;
            case EventType.Purchase:
                await HandlePurchaseKeys(@event, clientManager);
                break;
            case EventType.PurchaseComplete:
                await HandlePurchaseCompleteKeys(@event, clientManager);
                break;
        }
    }

    private async Task HandlePageViewKeys(EventCore @event, ClientManager clientManager)
    {
        foreach (var (conditionKey, conditionValue) in CacheManager.ObjectPageViewItems)
        {
            var pageViewKey = RedisEventKeys.PageView(@event.AccountId, @event.ClientId, conditionValue.Type, @event.EventTarget.Normalize().ToLower()).KeyName;
            var conditionRegex = conditionKey.Replace("*", ".*");
            var match = Regex.Match(pageViewKey, conditionRegex);
            if (match.Success)
            {
                var redisKey = RedisEventKeys.PageView(@event.AccountId, @event.ClientId, conditionValue.Type, conditionValue.Value.Normalize().ToLower());
                await _redisCacheService.IncrementAsync(redisKey, 1);
            }
        }
    }
    
    private async Task HandleProductViewKeys(EventCore @event, ClientManager clientManager)
    {
        var redisKey = RedisEventKeys.ProductView(@event.ClientId);
        await _redisCacheService.IncrementAsync(redisKey, 1);

        await HandleObjectWithProductKeys(@event, BehaviorType.ViewProduct, RedisEventKeys.ProductView);
    }

    private async Task HandleAddToCartKeys(EventCore @event, ClientManager clientManager)
    {
        var redisKey = RedisEventKeys.AddToCart(@event.ClientId);
        await _redisCacheService.IncrementAsync(redisKey, 1);

        await HandleObjectWithProductKeys(@event, BehaviorType.AddToCart, RedisEventKeys.AddToCart);
    }
    
    private async Task HandlePurchaseKeys(EventCore @event, ClientManager clientManager)
    {
        var redisKey = RedisEventKeys.Purchase(@event.ClientId);
        await _redisCacheService.IncrementAsync(redisKey, 1);
        
        await HandleObjectWithProductKeys(@event, BehaviorType.Checkout, RedisEventKeys.Purchase);
    }
    
    private async Task HandlePurchaseCompleteKeys(EventCore @event, ClientManager clientManager)
    {
        var redisKey = RedisEventKeys.PurchaseComplete(@event.ClientId);
        await _redisCacheService.IncrementAsync(redisKey, 1);

        await HandleObjectWithProductKeys(@event, BehaviorType.Purchases, RedisEventKeys.PurchaseComplete);
    }

    private async Task HandleObjectWithProductKeys(
        EventCore @event,
        BehaviorType behaviorType,
        Func<Guid, ObjectGroupProductType, string?, RedisKey> getRedisKey)
    {
        foreach (var eventProduct in @event.ProductDetails ?? new List<EventProductDetail>())
        {
            var objectBehaviors = CacheManager.ObjectBehaviorWithProductItems
                .Where(x => x.Value.BehaviorType == behaviorType);
            foreach (var (conditionKey, conditionValue) in objectBehaviors)
            {
                var productName = conditionValue.ProductType == ObjectGroupProductType.Product
                    ? eventProduct.Product?.ProductName
                    : eventProduct.Product?.ProductGroup;
                var processedProductName = productName?.Normalize().ToLower() ?? string.Empty;
                var eventKey = getRedisKey(@event.ClientId, conditionValue.ProductType ?? default, processedProductName).KeyName;
                var conditionRegex = conditionKey.Replace("*", ".*");
                var match = Regex.Match(eventKey, conditionRegex);
                if (match.Success)
                {
                    var redisKey = getRedisKey(@event.ClientId,
                        conditionValue.ProductType ?? default, conditionValue.ProductName?.Normalize().ToLower());
                    await _redisCacheService.IncrementAsync(redisKey, 1);
                }
            }
        }
    }
}