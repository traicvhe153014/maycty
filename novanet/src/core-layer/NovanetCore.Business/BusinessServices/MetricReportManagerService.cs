﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Novanet.Core.Domain;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessManager;

namespace NovanetCore.Business.BusinessServices;

public class MetricReportManagerService
{
    private readonly ILogger<MetricReportManagerService> _logger;
    private readonly IRedisCacheService _redisCacheService;

    public MetricReportManagerService(ILogger<MetricReportManagerService> logger, IRedisCacheService redisCacheService)
    {
        _logger = logger;
        _redisCacheService = redisCacheService;
    }

    public async Task Processing()
    {
        try
        {
            var advertiserMetricReports = new ConcurrentDictionary<ReportAdvertiserDimension, MetricReport>();

            // get campaign reports
            await GetReports<CampaignCore>(advertiserMetricReports, ReportAdvertiserDimension.Campaign);
            // get advertisingSet reports
            await GetReports<AdvertisingSetCore>(advertiserMetricReports, ReportAdvertiserDimension.AdvertisingSet);
            // get advertising reports
            await GetReports<AdvertisingCore>(advertiserMetricReports, ReportAdvertiserDimension.Advertising);
            // get product reports
            await GetReports<ProductEntityCore>(advertiserMetricReports, ReportAdvertiserDimension.Product);

            MetricReportManager.AdvertiserReport = advertiserMetricReports;
        }
        catch (Exception e)
        {
            _logger.LogError(e.ToString());
        }
    }
    
    private async Task GetReports<T>(
        ConcurrentDictionary<ReportAdvertiserDimension, MetricReport> advertiserMetricReports,
        ReportAdvertiserDimension dimension)
        where T : NovanetDocument
    {
        var cachedSet = CacheManager.Set<T>(_logger);
        if (cachedSet is null)
        {
            return;
        }
        var dimensionReports = new MetricReport
        {
            Data = new Dictionary<long, MetricReportByMetric>()
        };
        var subIds = cachedSet.Values.Select(x => x.SubId);
        foreach (var subId in subIds)
        {
            dimensionReports.Data[subId] = new MetricReportByMetric
            {
                Metrics = new Dictionary<ReportMetric, MetricReportValue>()
            };
            foreach (var reportMetric in MetricsToCache)
            {
                var reportKey = RedisReportKeys.GetAdverKey(dimension, reportMetric, subId);
                var reportData = (await _redisCacheService
                        .HashGetAllAsync(reportKey))
                    .ToDictionary(
                        x => long.TryParse(x.Key, out var parsedKey) ? parsedKey : default,
                        x => double.TryParse(x.Value, out var parsedValue) ? parsedValue : default);
                dimensionReports.Data[subId].Metrics[reportMetric] = new MetricReportValue
                {
                    Values = reportData
                };
            }
        }

        advertiserMetricReports[dimension] = dimensionReports;
    }

    private static readonly ReportMetric[] MetricsToCache =
    {
        ReportMetric.Click,
        ReportMetric.View,
        ReportMetric.Paid
    };
}