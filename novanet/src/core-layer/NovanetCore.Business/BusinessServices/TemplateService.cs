﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Novanet.Core.Enums;
using Novanet.Core.RazorRenderer;
using Novanet.Core.ValueObjects;
using NovanetCore.Business.BusinessAdvertisingViewModel;
using NovanetCore.Business.BusinessManager;

namespace NovanetCore.Business.BusinessServices;

public class RenderTemplateData
{
    public string? HtmlTemplate { get; set; }

    public RenderTemplateTypeEnum RenderTemplateType { get; set; }
}

public interface ITemplateService
{
    Task<RenderTemplateData> RenderTemplate(AdvertisingMatchScore? matchedAdvertising,
        List<ProductEntityMatchScore> productMatches,
        AdvertisingTemplateCore? template, ClientManager clientManager);
}

public class TemplateService : ITemplateService
{
    private readonly RazorRenderer _razorRenderer;
    private readonly AppSettings _appSettings;

    public TemplateService(RazorRenderer razorRenderer, AppSettings appSettings)
    {
        _razorRenderer = razorRenderer;
        _appSettings = appSettings;
    }

    public async Task<RenderTemplateData> RenderTemplate(
        AdvertisingMatchScore? matchedAdvertising,
        List<ProductEntityMatchScore> productMatches,
        AdvertisingTemplateCore? template,
        ClientManager clientManager)
    {
        //TODO: DONE Hòa NX - Có exception thì điều gì xảy ra ?
        var viewTime = DateTimeOffset.UtcNow.ToHexCode();
        try
        {
            double sellPrice = 0, buyPrice = 0;

            // anhlq1: ở trong TemplateService, buyPrice, sellPrice được lấy ra nếu theo CPC để gán vào tham số của link click url,
            // khi click, clickQuery sẽ lấy giá này đê track
            // nếu mua theo CPM hay bán theo CPM thì khi click vào link, giá = 0
            if (matchedAdvertising?.BuyPriceType ==
                PriceType.CPC) // TODO: DONE # Hòa NX - tại sao mua kiểu CPC lại gán giá mua nhỉ ? nếu mua CPM thì không gán giá mua à ?
            {
                buyPrice = matchedAdvertising.BuyPrice;
            }

            if (matchedAdvertising?.SellPriceType == PriceType.CPC)
            {
                sellPrice = matchedAdvertising.SellPrice;
            }

            if (matchedAdvertising?.UseDefault == true || template is null)
            {
                return await GetDefaultTemplateHtml(clientManager, viewTime, template?.TemplateType);
            }

            var matchedTemplate = matchedAdvertising?.Advertising?.AdvertisingAppliedTemplateCores
                .FirstOrDefault(x => x.AdvertisingTemplateId == template.Id);
            if (matchedTemplate is null)
            {
                return await GetDefaultTemplateHtml(clientManager, viewTime, template.TemplateType);
            }

            var displayedProducts = new List<AdvertisedProduct>();
            foreach (var product in productMatches)
            {
                var productPrice = double.TryParse(
                    GetProductAttributeValue(product.ProductEntity?.ProductAttributeValueCores, "Price")?.ToString(),
                    out var valuePrice)
                    ? valuePrice
                    : default;
                var productDiscountedPrice = double.TryParse(
                    GetProductAttributeValue(product.ProductEntity?.ProductAttributeValueCores, "SalePrice")
                        ?.ToString(),
                    out var valueDiscounted)
                    ? valueDiscounted
                    : default;
                var showDiscount =
                    GetProductAttributeValue(product.ProductEntity?.ProductAttributeValueCores, "SalePrice") != null &&
                    productDiscountedPrice < productPrice;
                var displayedProduct = new AdvertisedProduct
                {
                    Id = product.ProductEntity?.SubId,
                    Link = GetProductAttributeValue(product.ProductEntity?.ProductAttributeValueCores, "Link")
                        ?.ToString(),
                    Name = GetProductAttributeValue(product.ProductEntity?.ProductAttributeValueCores, "Title")
                        ?.ToString(),
                    Price = productPrice.ToString("N0"),
                    ImageLink = GetProductAttributeValue(product.ProductEntity?.ProductAttributeValueCores, "ImageLink")
                        ?.ToString(),
                    DiscountedPrice = productDiscountedPrice.ToString("N0"),
                    DiscountPercentage =
                        Math.Round((productPrice != 0 ? (productPrice - productDiscountedPrice) / productPrice : 0) *
                                   100),
                    ShowDiscount = showDiscount
                };
                displayedProducts.Add(displayedProduct);
            }

            var templateName = Enum.GetName(typeof(TemplateType), template.TemplateType) ?? "Default";

            var attributeValues = matchedTemplate.AdvertisingTemplateAttributeValueCores ??
                                  new List<AdvertisingTemplateAttributeValueCore>();
            AdvertisingViewModel? model;

            switch (template.TemplateType)
            {
                case TemplateType.PostInRead:
                    model = new PostInReadViewModel
                    {
                        AvatarUrl = GetAvatarUrl(GetTemplateAttributeValue(attributeValues, "avatarUrl")?.ToString() ??
                                                 string.Empty),
                        AvatarName = GetTemplateAttributeValue(attributeValues, "avatarName")?.ToString(),
                        PostContent = GetTemplateAttributeValue(attributeValues, "postContent")?.ToString()
                    };
                    await UpdateEcommerceViewModel((BaseEcommerceViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template, displayedProducts);
                    break;
                case TemplateType.FlyingCarpet:
                case TemplateType.InteractiveBanner:
                case TemplateType.InReadEcommerce:
                case TemplateType.MobileBannerCard:
                    model = new BannerCardViewModel
                    {
                        BackgroundImageUrl = GetBannerUrl(template.TemplateType,
                            (bool?) GetTemplateAttributeValue(attributeValues, "useBannerTemplate") ?? true,
                            GetTemplateAttributeValue(attributeValues, "bannerImage")?.ToString()),
                        UseBannerTemplate =
                            (bool?) GetTemplateAttributeValue(attributeValues, "useBannerTemplate") ?? true
                    };
                    await UpdateEcommerceViewModel((BaseEcommerceViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template, displayedProducts);
                    break;
                case TemplateType.CatfishEcom:
                    model = new CatfishEcomViewModel
                    {
                        BackgroundImageUrl = GetBannerUrl(template.TemplateType,
                            (bool?) GetTemplateAttributeValue(attributeValues, "useBannerTemplate") ?? true,
                            GetTemplateAttributeValue(attributeValues, "bannerImage")?.ToString()),
                        ExpandedContentUrl = GetExpandedUrl(
                            (bool?) GetTemplateAttributeValue(attributeValues, "useBannerTemplate") ?? true,
                            GetTemplateAttributeValue(attributeValues, "expandedContentUrl")?.ToString()),
                        UseBannerTemplate =
                            (bool?) GetTemplateAttributeValue(attributeValues, "useBannerTemplate") ?? true
                    };
                    await UpdateEcommerceViewModel((BaseEcommerceViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template, displayedProducts);
                    break;
                case TemplateType.NotificationBanner:
                    model = new NotificationBannerViewModel
                    {
                        ShortcutNotification = GetTemplateAttributeValue(attributeValues, "shortcutNotification")?.ToString(),
                        ExtendNotification = GetTemplateAttributeValue(attributeValues, "extendNotification")?.ToString(),
                        IsCustomImage = (bool?) GetTemplateAttributeValue(attributeValues, "isCustomImage") ??
                                        true,
                        Images = GetTemplateAttributeValue(attributeValues, "images")?.ToString()
                            ?.Deserialize<List<string>>()
                            .Select(x => GetBannerUrl(template.TemplateType, false, x))
                            .ToList(),
                        IsDelay = (bool?) GetTemplateAttributeValue(attributeValues, "isDelay") ?? false,
                        Delay =
                            int.TryParse(GetTemplateAttributeValue(attributeValues, "delay")?.ToString(),
                                out var value)
                                ? value
                                : default
                    };
                    await UpdateDisplayAdsViewModel((BaseDisplayAdsViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template);
                    break;
                case TemplateType.CatfishCollabBranding:
                    model = new CatfishCollabBrandingViewModel
                    {
                        CollapseBanner = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "collapseBanner")?.ToString()),
                        ExtendBanner = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "extendBanner")?.ToString())
                    };
                    await UpdateDisplayAdsViewModel((BaseDisplayAdsViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template);
                    break;
                case TemplateType.PopupBannerBranding:
                    model = new PopupBannerBrandingViewModel
                    {
                        ExtendBanner = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "extendBanner")?.ToString()),
                    };
                    await UpdateDisplayAdsViewModel((BaseDisplayAdsViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template);
                    break;
                case TemplateType.CatfishCollabVideo:
                    model = new CatfishCollabVideoViewModel
                    {
                        CollapseBanner = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "collapseBanner")?.ToString()),
                        Video = GetVideoUrl(GetTemplateAttributeValue(attributeValues, "video")?.ToString()),
                        ThumbnailVideo = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "thumbnailVideo")?.ToString())
                    };
                    await UpdateDisplayAdsViewModel((BaseDisplayAdsViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template);
                    break;
                case TemplateType.PopupBannerVideo:
                    model = new PopupBannerVideoViewModel
                    {
                        Video = GetVideoUrl(GetTemplateAttributeValue(attributeValues, "video")?.ToString()),
                        ThumbnailVideo = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "thumbnailVideo")?.ToString())
                    };
                    await UpdateDisplayAdsViewModel((BaseDisplayAdsViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template);
                    break;
                case TemplateType.CatfishEcomVideo:
                    model = new CatfishEcomVideoViewModel
                    {
                        BackgroundImageUrl = GetBannerUrl(template.TemplateType, false,
                            GetTemplateAttributeValue(attributeValues, "bannerImage")?.ToString()),
                        Video = GetVideoUrl(GetTemplateAttributeValue(attributeValues, "video")?.ToString()),
                        ThumbnailVideo = GetExpandedUrl(false,
                            GetTemplateAttributeValue(attributeValues, "thumbnailVideo")?.ToString()),
                    };
                    await UpdateEcommerceViewModel((BaseEcommerceViewModel) model, buyPrice, sellPrice, viewTime,
                        attributeValues,
                        clientManager,
                        matchedAdvertising, template, displayedProducts);
                    break;
                default:
                    model = null;
                    break;
            }

            var htmlTemplate = await _razorRenderer.RenderViewToString($"Templates/{templateName}/Index", model);

            return new RenderTemplateData
            {
                HtmlTemplate = htmlTemplate,
                RenderTemplateType = RenderTemplateTypeEnum.HtmlTemplate
            };
        }
        catch (Exception)
        {
            return await GetDefaultTemplateHtml(clientManager, viewTime, template?.TemplateType);
        }
    }

    private static object? GetTemplateAttributeValue(List<AdvertisingTemplateAttributeValueCore> attributeValues,
        string attributeName)
    {
        var templateAttributes = CacheManager.AdvertisingTemplateAttributes;
        var (_, attribute) = templateAttributes.FirstOrDefault(x => x.Value.Name == attributeName);
        var attributeValue = attributeValues.FirstOrDefault(x =>
            x.AdvertisingTemplateAttributeId == attribute?.Id);
        return attributeValue?.GetValue(attribute.DataType);
    }

    private static object? GetProductAttributeValue(List<ProductAttributeValueCore>? attributeValues,
        string attributeName)
    {
        attributeValues ??= new List<ProductAttributeValueCore>();
        var templateAttributes = CacheManager.ProductAttributes;
        var (_, attribute) = templateAttributes.FirstOrDefault(x => x.Value.Name == attributeName);
        var attributeValue = attributeValues.FirstOrDefault(x =>
            x.AttributeId == attribute?.Id);
        return attributeValue?.GetValue(attribute.DataType);
    }

    private static string GetHashedQueryParams(AdvertisingViewModel model, ClientManager clientManager, string? targetUrl = null)
    {
        var queryString = HttpUtility.ParseQueryString(string.Empty);
        queryString.Add(RequestParams.AdvertisingTemplateKey, model.AdvertisingTemplateId.ToString());
        queryString.Add(RequestParams.TargetUrlKey,
            !string.IsNullOrWhiteSpace(targetUrl) ? targetUrl : model.RedirectLink);
        queryString.Add(RequestParams.CampaignKey, model.CampaignId?.ToString());
        queryString.Add(RequestParams.AdvertisingKey, model.AdvertisingId?.ToString());
        queryString.Add(RequestParams.SellPriceKey, model.SellPrice.ToString(CultureInfo.InvariantCulture));
        queryString.Add(RequestParams.BuyPriceKey, model.BuyPrice.ToString(CultureInfo.InvariantCulture));
        queryString.Add(RequestParams.PriceKey, model.Price.ToString(CultureInfo.InvariantCulture));
        queryString.Add(RequestParams.TimestampKey, model.Timestamp);
        queryString.Add(RequestParams.ZoneKey, clientManager.Zone.ToString());
        queryString.Add(RequestParams.JsRefererKey, clientManager.JsReferer);
        queryString.Add(RequestParams.IpKey, clientManager.Ip);
        queryString.Add(RequestParams.IsDefaultKey, model.IsDefault.ToString());
        var encrypted = (queryString.ToString() ?? string.Empty).AesEncryptString();
        return HttpUtility.UrlEncode(encrypted);
    }

    private static string GetProductHashedQueryParams(AdvertisingViewModel model, AdvertisedProduct product,
        ClientManager clientManager)
    {
        var queryString = HttpUtility.ParseQueryString(string.Empty);
        queryString.Add(RequestParams.ProductKey, product.Id.ToString());
        queryString.Add(RequestParams.AdvertisingTemplateKey, model.AdvertisingTemplateId.ToString());
        queryString.Add(RequestParams.TargetUrlKey, product.Link);
        queryString.Add(RequestParams.CampaignKey, model.CampaignId.ToString());
        queryString.Add(RequestParams.AdvertisingKey, model.AdvertisingId.ToString());
        queryString.Add(RequestParams.SellPriceKey, model.SellPrice.ToString(CultureInfo.InvariantCulture));
        queryString.Add(RequestParams.BuyPriceKey, model.BuyPrice.ToString(CultureInfo.InvariantCulture));
        queryString.Add(RequestParams.PriceKey, model.Price.ToString(CultureInfo.InvariantCulture));
        queryString.Add(RequestParams.TimestampKey, model.Timestamp);
        queryString.Add(RequestParams.ZoneKey, clientManager.Zone.ToString());
        queryString.Add(RequestParams.JsRefererKey, clientManager.JsReferer);
        queryString.Add(RequestParams.IpKey, clientManager.Ip);
        queryString.Add(RequestParams.IsDefaultKey, model.IsDefault.ToString());
        var encrypted = (queryString.ToString() ?? string.Empty).AesEncryptString();
        return HttpUtility.UrlEncode(encrypted);
    }

    private DefaultViewModel GetDefaultViewModel(ClientManager clientManager, string viewTime,
        TemplateType? templateType, bool isBackUp)
    {
        var zone = CacheManager.Zones.Values.FirstOrDefault(x => x.SubId == clientManager.Zone);
        var defaultModel = new DefaultViewModel
        {
            BackgroundImageUrl = zone is not null ? $"{zone.Width}x{zone.Height}.jpg" : "375x600.jpg",
            RedirectLink = ConfigValues.DefaultRedirectLink,
            DisplayRootUrl = _appSettings.RootUrl,
            WebsiteRootUrl = _appSettings.WebsiteUrl,
            SellPrice = 0,
            BuyPrice = 0,
            Timestamp = viewTime,
            ClientId = clientManager.ClientId,
            GoogleAnalyticsProperty = _appSettings.GoogleAnalytics.Property,
            ZoneId = clientManager.Zone,
            IsDefault = true,
            CloseSize = clientManager.CloseSize ?? ConfigValues.DefaultCloseSize,
            LogoStatus = clientManager.LogoStatus ?? ConfigValues.DefaultLogoStatus,
            IsBackUp = isBackUp
        };
        if (templateType is not null)
        {
            defaultModel.TemplateType = templateType;
        }
        else
        {
            var templateTypes = zone?.ZoneTemplateCores
                ?.Select(x =>
                {
                    var template = CacheManager.Templates.Values.FirstOrDefault(y => y.Id == x.AdvertisingTemplateId);
                    return template?.TemplateType;
                })
                .ToList() ?? new List<TemplateType?>();
            defaultModel.TemplateType = templateTypes.FirstOrDefault();
        }

        defaultModel.HashedQueryParams = GetHashedQueryParams(defaultModel, clientManager);
        return defaultModel;
    }

    private async Task UpdateCommonViewModel(
        AdvertisingViewModel model,
        double buyPrice,
        double sellPrice,
        string viewTime,
        ClientManager clientManager,
        AdvertisingMatchScore? matchedAdvertising,
        AdvertisingTemplateCore template)
    {
        var campaign =
            CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == matchedAdvertising?.Advertising?.CampaignId);
        CacheManager.Zones.TryGetValue(clientManager.Zone, out var zone);
        model.ZoneId = clientManager.Zone;
        model.AdvertisingId = matchedAdvertising?.Advertising?.SubId;
        model.CampaignId = campaign?.SubId;
        model.DisplayRootUrl = _appSettings.RootUrl;
        model.WebsiteRootUrl = _appSettings.WebsiteUrl;
        model.BuyPrice = buyPrice;
        model.SellPrice = sellPrice;
        model.Timestamp = viewTime;
        model.AdvertisingTemplateId = template.SubId;
        model.Height = template.Height;
        model.Width = template.Width;
        model.ClientId = clientManager.ClientId;
        model.GoogleAnalyticsProperty = _appSettings.GoogleAnalytics.Property;
        model.IsDefault = false;
        model.UsingBackup = zone?.UsingBackup ?? true;
        model.BackUpCode =
            (await GetDefaultTemplateHtml(clientManager, viewTime, template.TemplateType, true)).HtmlTemplate?.Replace("\n",
                string.Empty);
        model.CloseSize = clientManager.CloseSize ?? ConfigValues.DefaultCloseSize;
        model.LogoStatus = clientManager.LogoStatus ?? ConfigValues.DefaultLogoStatus;
        model.RedirectLink = matchedAdvertising?.Advertising?.RedirectLink;
        model.UseRedirectLink = !string.IsNullOrWhiteSpace(matchedAdvertising?.Advertising?.RedirectLink);
        model.HashedQueryParams = GetHashedQueryParams(model, clientManager);
    }

    private async Task UpdateEcommerceViewModel(
        BaseEcommerceViewModel model,
        double buyPrice,
        double sellPrice,
        string viewTime,
        List<AdvertisingTemplateAttributeValueCore> attributeValues,
        ClientManager clientManager,
        AdvertisingMatchScore? matchedAdvertising,
        AdvertisingTemplateCore template,
        List<AdvertisedProduct> displayedProducts)
    {
        await UpdateCommonViewModel(model, buyPrice, sellPrice, viewTime, clientManager, matchedAdvertising, template);
        model.ShowFreeDelivery = (bool?) GetTemplateAttributeValue(attributeValues, "showFreeDelivery") ?? false;
        model.ProductName = (bool?) GetTemplateAttributeValue(attributeValues, "productName") ?? false;
        model.ShowDiscountedPrice = (bool?) GetTemplateAttributeValue(attributeValues, "showDiscountedPrice") ?? false;
        model.Products = displayedProducts.Select(x =>
        {
            x.HashedQueryParams = GetProductHashedQueryParams(model, x, clientManager);
            return x;
        }).ToList();
    }

    private async Task UpdateDisplayAdsViewModel(
        BaseDisplayAdsViewModel model,
        double buyPrice,
        double sellPrice,
        string viewTime,
        List<AdvertisingTemplateAttributeValueCore> attributeValues,
        ClientManager clientManager,
        AdvertisingMatchScore? matchedAdvertising,
        AdvertisingTemplateCore template)
    {
        await UpdateCommonViewModel(model, buyPrice, sellPrice, viewTime, clientManager, matchedAdvertising, template);
        model.Title = GetTemplateAttributeValue(attributeValues, "title")?.ToString() ?? string.Empty;
        model.Description = GetTemplateAttributeValue(attributeValues, "description")?.ToString() ?? string.Empty;
        model.IsCta = (bool?) GetTemplateAttributeValue(attributeValues, "isCta") ?? default;
        model.Cta = GetTemplateAttributeValue(attributeValues, "cta")?.ToString() ?? string.Empty;
        model.CtaUrl = GetTemplateAttributeValue(attributeValues, "ctaUrl")?.ToString() ?? string.Empty;
        model.Logo = GetAvatarUrl(GetTemplateAttributeValue(attributeValues, "logo")?.ToString() ?? string.Empty);
        model.HashedCtaQueryParams = GetHashedQueryParams(model, clientManager, model.CtaUrl);
    }

    #region GetUrls

     private string GetBannerUrl(TemplateType templateType, bool useBannerTemplate, string? bannerUrl)
    {
        if (!useBannerTemplate && bannerUrl is not null)
        {
            return
                $"{_appSettings.RootUrl}/storage/api/v1/stream?name={string.Format(bannerUrl, 800)}&v={DateTimeOffset.Now.ToHexCode()}";
        }

        return templateType switch
        {
            TemplateType.MobileBannerCard => $"{_appSettings.WebsiteUrl}/{ConfigValues.DefaultMobileBanner}",
            TemplateType.InteractiveBanner => $"{_appSettings.WebsiteUrl}/{ConfigValues.DefaultInteractiveBanner}",
            TemplateType.FlyingCarpet => $"{_appSettings.WebsiteUrl}/{ConfigValues.DefaultFlyingCarpet}",
            TemplateType.InReadEcommerce => $"{_appSettings.WebsiteUrl}/{ConfigValues.DefaultInReadEcommerce}",
            TemplateType.CatfishEcom => $"{_appSettings.WebsiteUrl}/{ConfigValues.DefaultCatfishEcomBanner}",
            _ => string.Empty
        };
    }

    private string GetExpandedUrl(bool useBannerTemplate, string? expandedContentUrl)
    {
        if (useBannerTemplate || expandedContentUrl is null) return string.Empty;
        if (!expandedContentUrl.EndsWith("zip"))
            return $"{_appSettings.RootUrl}/storage/api/v1/stream?name={string.Format(expandedContentUrl, 800)}";
        var fileName = expandedContentUrl.Split('.').FirstOrDefault() ?? string.Empty;
        return $"{_appSettings.RootUrl}/archives/{fileName}/index.html";
    }
    
    private string GetVideoUrl(string? videoUrl)
    {
        if (videoUrl is null)
        {
            return string.Empty;
        }
        const string fileNamePattern = @"(\w+).(\w+)";
        var matches = Regex.Matches(videoUrl, fileNamePattern);
        if (matches.Count == 0)
        {
            return string.Empty;
        }
        var match = matches.FirstOrDefault()!;
        var videoPart = match.Groups[1].Value;
        return videoPart;
    }

    private string GetAvatarUrl(string avatarUrl)
    {
        return $"{_appSettings.RootUrl}/storage/api/v1/stream?name={string.Format(avatarUrl, 200)}";
    }

    #endregion

    private async Task<RenderTemplateData> GetDefaultTemplateHtml(ClientManager clientManager, string viewTime,
        TemplateType? templateType, bool isBackUp = false)
    {
        var zone = CacheManager.Zones.Values.FirstOrDefault(x => x.SubId == clientManager.Zone);
        // nếu zone có back up => dùng backup code của zone
        // không thì dùng default template
        if (zone is not null && zone.UsingBackup)
        {
            switch (zone.BackupType)
            {
                case ZoneBackupType.BackupCode:
                    var htmlTemplate = await _razorRenderer.RenderViewToString("Templates/BannerBackUp/Index",
                        new BackUpViewModel
                        {
                            ZoneId = zone.SubId,
                            BackUpCode = zone.BackupCode ?? string.Empty,
                            ClientId = clientManager.ClientId,
                        });
                    return new RenderTemplateData
                    {
                        HtmlTemplate = htmlTemplate,
                        RenderTemplateType = RenderTemplateTypeEnum.Backup
                    };
                case ZoneBackupType.BackupFile:
                    var htmlTemplateBackupFile = await _razorRenderer.RenderViewToString("Templates/BannerBackUp/Index",
                        new BackUpViewModel
                        {
                            ZoneId = zone.SubId,
                            BackUpCode = GetBannerBackUpCode(zone, viewTime, clientManager),
                            ClientId = clientManager.ClientId
                        });

                    return new RenderTemplateData
                    {
                        HtmlTemplate = htmlTemplateBackupFile,
                        RenderTemplateType = RenderTemplateTypeEnum.Backup
                    };
                default:
                    break;
            }
        }

        var defaultModel =
            GetDefaultViewModel(clientManager, viewTime,
                templateType, isBackUp); // TODO: DONE # Hòa NX - banner mặc định thì không tính tiền nhé (cả mua lẫn bán), ngoài ra log số view mặc định ở đâu nhỉ ?
        // TODO: DONE # Hòa NX - chưa thấy có xử lý hiển thị backup script của publisher
        var defaultBannerTemplate = await _razorRenderer.RenderViewToString("Templates/Default/Index", defaultModel);

        return new RenderTemplateData
        {
            HtmlTemplate = defaultBannerTemplate,
            RenderTemplateType = RenderTemplateTypeEnum.DefaultBanner
        };
    }

    private string GetBannerBackUpCode(ZoneCore zone, string viewTime, ClientManager clientManager)
    {
        var content = zone.BackupCode?.EndsWith("html") == true
            ? $@"<iframe src='{_appSettings.RootUrl}/storage/api/v1/stream?name={zone.BackupCode}' style='width: 100%;'></iframe>"
            : $@"<img src='{_appSettings.RootUrl}/storage/api/v1/stream?name={string.Format(zone.BackupCode ?? string.Empty, 800)}' alt='' style='width: 100%;'/>";
        if (string.IsNullOrWhiteSpace(zone.RedirectLink))
        {
            return content;
        }
        var defaultModel = new DefaultViewModel
        {
            RedirectLink = zone.RedirectLink,
            DisplayRootUrl = _appSettings.RootUrl,
            WebsiteRootUrl = _appSettings.WebsiteUrl,
            SellPrice = 0,
            BuyPrice = 0,
            Timestamp = viewTime,
            ClientId = clientManager.ClientId,
            ZoneId = zone.SubId,
            IsDefault = true,
            IsBackUp = true
        };
        var hashedQueryParams = GetHashedQueryParams(defaultModel, clientManager);
        var stringBuilder = new StringBuilder();
        stringBuilder.AppendLine("<div style='position: relative;'>");
        stringBuilder.AppendLine(content);
        stringBuilder.AppendLine(
            $@"<a style='display:block;width:100%;height:100%;position:absolute;top:0' href='{_appSettings.RootUrl}/display/Click?q={hashedQueryParams}' target='_blank'></a>");
        stringBuilder.AppendLine("</div>");
        return stringBuilder.ToString();
    }
}