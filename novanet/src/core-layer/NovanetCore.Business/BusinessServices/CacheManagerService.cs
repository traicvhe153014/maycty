﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.Text;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using Novanet.Core.Constants;
using Novanet.Core.Domain;
using Novanet.Core.Enums;
using Novanet.Core.Models;
using Novanet.Core.RedisCache;
using Novanet.Core.ValueObjects;
using NovanetCore.Business.BusinessDomain.Service.Sharing;
using NovanetCore.Business.BusinessManager;
using NovanetGrpcCache;

namespace NovanetCore.Business.BusinessServices;

public class CacheManagerService
{
    private readonly ILogger<CacheManagerService> _logger;
    private readonly IGlobalCacheService _globalCacheService;
    private readonly AppSettings _appSettings;

    public CacheManagerService(
        ILogger<CacheManagerService> logger,
        IGlobalCacheService globalCacheService, AppSettings appSettings)
    {
        _logger = logger;
        _globalCacheService = globalCacheService;
        _appSettings = appSettings;
    }

    public async Task Processing()
    {
        CacheManager.LastUpdatedStart = DateTimeOffset.Now;
        _logger.LogInformation("Update CacheManager Started");
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        var debugStr = string.Empty;
        try
        {
            await LoadCacheIdentity();
            await LoadCacheSettings();
            await LoadCacheSharing();

            LoadObjectWebsiteConditions();
            LoadObjectBehaviorWithProductItems();

            CacheManager.LastUpdatedEnd = DateTimeOffset.Now;
        }
        catch (Exception e)
        {
            _logger.LogError("{Time} Error: {Error}", DateTimeOffset.Now, debugStr + e.Message);
        }
        finally
        {
            stopwatch.Stop();
            _logger.LogInformation("Update CacheManager Finished. Elapsed Time: {Time}",
                stopwatch.Elapsed.TotalMinutes);
        }
    }

    /// <summary>
    /// Lưu danh sách các điều kiện truy cập website tổng hợp từ các nhóm đối tượng trên hệ thống.
    /// Khi visitor truy cập 1 trang web có gán code remarketing, core sẽ check nếu url đấy match
    /// với điều kiện nào, thì +1 giá trị cho key đó trong redis.
    /// </summary>
    public void LoadObjectWebsiteConditions()
    {
        var conditions = CacheManager.ObjectGroupWebsiteConditions.Values;
        var cachingConditions = new ConcurrentDictionary<string, ObjectPageViewItem>();
        foreach (var condition in conditions)
        {
            var conditionValue = condition.ConditionType switch
            {
                UrlConditionType.Equal => condition.Value.Normalize().ToLower(),
                UrlConditionType.Contains => $"*{condition.Value.Normalize().ToLower()}*",
                _ => string.Empty
            };
            cachingConditions[$"*:{EventType.PageView}:*:{condition.ConditionType}:{conditionValue}"] =
                new ObjectPageViewItem
                {
                    Type = condition.ConditionType,
                    Value = condition.Value.Normalize().ToLower()
                };
        }

        CacheManager.ObjectPageViewItems = cachingConditions;
    }

    /// <summary>
    /// Tương tự như LoadObjectWebsiteConditions, nhưng chỉ lưu các điều kiện hành vi khách hàng
    /// khi đặt điều kiện chứa từ khóa sản phẩm/nhóm sản phẩm
    /// </summary>
    public void LoadObjectBehaviorWithProductItems()
    {
        var cachingConditions = new ConcurrentDictionary<string, ObjectBehaviorWithProductItem>();

        foreach (var objectGroup in CacheManager.ObjectGroups.Values
                     .Where(x => !x.ApplyAllProducts))
        {
            var conditions = CacheManager.ObjectGroupConsumerBehaviors.Values
                .Where(x => x.ObjectGroupId == objectGroup.Id)
                .ToList();

            foreach (var condition in conditions)
            {
                var processedProductName = objectGroup.ProductName?.Normalize().ToLower();
                if (condition.BehaviorType == BehaviorType.All)
                {
                    ObjectGroupConsumerBehaviorConstants.BehaviorTypes.ForEach(item =>
                    {
                        var conditionValue =
                            $"*:{ObjectGroupConsumerBehaviorConstants.MappingEventTypes[item]}:{objectGroup.ProductType}:*{processedProductName}*";
                        cachingConditions[conditionValue] = new ObjectBehaviorWithProductItem
                        {
                            ProductName = processedProductName,
                            ProductType = objectGroup.ProductType,
                            BehaviorType = item
                        };
                    });
                }
                else
                {
                    var conditionValue =
                        $"*:{ObjectGroupConsumerBehaviorConstants.MappingEventTypes[condition.BehaviorType]}:{objectGroup.ProductType}:*{processedProductName}*";
                    cachingConditions[conditionValue] = new ObjectBehaviorWithProductItem
                    {
                        ProductName = processedProductName,
                        ProductType = objectGroup.ProductType,
                        BehaviorType = condition.BehaviorType
                    };
                }
            }
        }

        CacheManager.ObjectBehaviorWithProductItems = cachingConditions;
    }

    private async Task LoadCacheEntity<T>(string serviceName) where T : INovanetDocument
    {
        var cacheSet = CacheManager.Set<T>(_logger);
        if (cacheSet is null)
        {
            return;
        }
        var filePath = Path.Combine(_appSettings.CacheRootPath, serviceName, $"{typeof(T).Name}.json");
        var httpHandler = new HttpClientHandler();
        httpHandler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
        var grpcAddress = (string) _appSettings.GrpcEndpoints.GetType()
            .GetProperty($"Service{serviceName}")!.GetValue(_appSettings.GrpcEndpoints, null)!;
        using var channel = GrpcChannel.ForAddress(grpcAddress, new GrpcChannelOptions { HttpHandler = httpHandler });
        var client = new GrpcCache.GrpcCacheClient(channel);
        var fileInfo = new NovanetGrpcCache.FileInfo
        {
            FilePath = filePath
        };
        var request = client.StreamFile(fileInfo);
        var cancellationTokenSource = new CancellationTokenSource();
        while (await request.ResponseStream.MoveNext(cancellationTokenSource.Token))
        {
            var itemString = request.ResponseStream.Current.ReadedContent;
            var item = itemString.Deserialize<T>();
            cacheSet[item.SubId] = item;
        }
    }

    private async Task LoadCacheIdentity()
    {
        await LoadCacheEntity<IdentityUserValue>("Identity");
    }

    private async Task LoadCacheSettings()
    {
        // advertising
        await LoadCacheEntity<AdvertisingCore>("Settings");
        await LoadCacheEntity<AdvertisingSetCore>("Settings");
        await LoadCacheEntity<AdvertisingSetProductGroupCore>("Settings");
        await LoadCacheEntity<AdvertisingSetObjectCore>("Settings");
        await LoadCacheEntity<AdvertisingTemplateCore>("Settings");
        await LoadCacheEntity<AdvertisingTemplateAttributeCore>("Settings");
        await LoadCacheEntity<CampaignCore>("Settings");
        // publisher
        await LoadCacheEntity<ZoneCore>("Settings");
        await LoadCacheEntity<WebsiteCore>("Settings");
        await LoadCacheEntity<WebsitePriceCore>("Settings");
        // product
        await LoadCacheEntity<ProductAttributeCore>("Settings");
        await LoadCacheEntity<ProductAttributeValueCore>("Settings");
        await LoadCacheEntity<ProductEntityCore>("Settings");
        await LoadCacheEntity<ProductFeedCore>("Settings");
        await LoadCacheEntity<ProductGroupEntityCore>("Settings");
        // object
        await LoadCacheEntity<ObjectGroupCore>("Settings");
        await LoadCacheEntity<ObjectGroupConsumerBehaviorCore>("Settings");
        await LoadCacheEntity<ObjectGroupWebsiteConditionCore>("Settings");
    }

    private async Task LoadCacheSharing()
    {
        await LoadCacheEntity<CategoryCore>("Sharing");
        await LoadCacheEntity<ProhibitedCategoryCore>("Sharing");
        await LoadCacheEntity<LocationCore>("Sharing");
    }
}