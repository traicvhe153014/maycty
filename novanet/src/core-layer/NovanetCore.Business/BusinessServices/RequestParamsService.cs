﻿using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using Novanet.Core.Models;

namespace NovanetCore.Business.BusinessServices;

public interface IRequestParamsService
{
    RequestParams RequestParams(IQueryCollection? queryCollection);
}

public class RequestParamsService : IRequestParamsService
{
    public RequestParams RequestParams(IQueryCollection? queryCollection)
    {
        var abstracts = queryCollection.Serialize().Deserialize<List<DisplayAbstract>>();

        // Lấy danh sách thông tin phía client: zone, timestamp, referer, url, host
        var @params = abstracts.ToDictionary(displayAbstract => displayAbstract.Key,
            displayAbstract => displayAbstract.Value?.FirstOrDefault()).ToJson().Deserialize<RequestParams>();

        return @params;
    }
}