﻿using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NovanetGrpcCache;
using FileInfo = NovanetGrpcCache.FileInfo;
using StringContent = NovanetGrpcCache.StringContent;

namespace NovanetCore.Business.BusinessServices;

public class GrpcCacheService : GrpcCache.GrpcCacheBase
{
    private readonly ILogger<GrpcCacheService> _logger;

    public GrpcCacheService(ILogger<GrpcCacheService> logger)
    {
        _logger = logger;
    }

    public override async Task StreamFile(FileInfo request, IServerStreamWriter<StringContent> responseStream, ServerCallContext context)
    {
        try
        {
            var fileName = Path.GetFileName(request.FilePath);
            var fileExtension = Path.GetExtension(request.FilePath);

            var content = new StringContent
            {
                FileSize = 0,
                Info = new FileInfo {FileName = fileName, FileExtension = fileExtension},
                ReadedContent = ""
            };
            await foreach (var obj in JsonExtensions.StreamJsonList(request.FilePath))
            {
                var currentObject = obj.ToString(Formatting.None);
                content.ReadedContent = currentObject;
                await responseStream.WriteAsync(content);
            }
        }
        catch (Exception)
        {
            // ignored
        }
    }
}