﻿using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Novanet.Core.Constants;
using Novanet.Core.Enums;
using Novanet.Core.Enums.ReportKeyObjects;
using Novanet.Core.RedisCache;
using NovanetCore.Business.BusinessDomain.Service.Sharing;
using NovanetCore.Business.BusinessManager;
using BehaviorType = Novanet.Core.Enums.BehaviorType;

namespace NovanetCore.Business.BusinessServices;

public interface IAdvertisingService
{
    Task<bool> IsFraud();

    Task<List<AdvertisingMatchScore>> MatchesWithZone(
        ConcurrentDictionary<long, AdvertisingCore> advertisingCores,
        ZoneCore? zoneCore, WebsiteCore? website, RequestParams @params, ClientManager clientManager);

    List<AdvertisingMatchScore> GetRandomAdvertising(List<AdvertisingMatchScore>? advertising,
        AdvertisingTemplateCore? templateCore, ClientManager clientManager);
}

public class AdvertisingService : IAdvertisingService
{
    private readonly ICookieService _cookieService;
    private readonly ILocationService _locationService;
    private readonly IReportService _reportService;
    private readonly IRedisCacheService _redisCacheService;
    private readonly ILogger<AdvertisingService> _logger;

    /// <summary>
    /// Sử dụng để lưu kết quả check điều kiện thỏa mãn nhóm đối tượng của client => hạn chế roundtrip vào redis, tăng tốc độ xử lý.
    /// Biến có vòng đời trong scope của request nên object sẽ bị hủy khi có request mới.
    /// </summary>
    private readonly Dictionary<string, bool> _objectGroupExecutedValues;

    public AdvertisingService(ICookieService cookieService, ILocationService locationService,
        IReportService reportService, IRedisCacheService redisCacheService, ILogger<AdvertisingService> logger)
    {
        _cookieService = cookieService;
        _locationService = locationService;
        _reportService = reportService;
        _logger = logger;
        _objectGroupExecutedValues = new Dictionary<string, bool>();
        _redisCacheService = redisCacheService;
    }

    public Task<bool> IsFraud()
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Date: 05/13/2022
    /// Author: hoanx
    /// -------------------
    /// Developer: anhlq
    /// Version: 1
    /// Before: 
    /// After: Không sử dụng cơ chế điểm, sử dụng cơ chế xoay vòng (GetRandomAdvertisings) theo v5
    /// -------------------
    /// </summary>
    /// <param name="advertisingCores"></param>
    /// <param name="zoneCore"></param>
    /// <param name="website"></param>
    /// <param name="params"></param>
    /// <param name="clientManager"></param>
    /// <returns></returns>
    public async Task<List<AdvertisingMatchScore>> MatchesWithZone(
        ConcurrentDictionary<long, AdvertisingCore> advertisingCores,
        ZoneCore? zoneCore,
        WebsiteCore? website,
        RequestParams @params,
        ClientManager clientManager)
    {
        var matches = new List<AdvertisingMatchScore>();
        foreach (var (_, advertisingCore) in advertisingCores)
        {
            if (!await Matched(zoneCore, website, advertisingCore, @params, clientManager)) continue;

            // Lấy danh sách campaigns matched
            var campaignRetargets = _cookieService.CampaignMatches();

            // Kiểm tra danh sách site người dùng quan tâm chứa chiến dịch vẫn trong ngày cho phép retarget
            if (campaignRetargets.Any(x => x.CampaignId.Equals(advertisingCore.CampaignId)))
            {
                var campaignRetarget =
                    campaignRetargets.FirstOrDefault(x => x.CampaignId.Equals(advertisingCore.CampaignId));
                var totalViews = campaignRetarget?.TotalViews;

                // Thêm điều kiện remarketing - thời gian xem trang
                if (advertisingCore.MinimumTimeOnSiteRemarketing > 0)
                {
                    var maximumTimeOnSite = _cookieService.MaximumTimeOnSite(campaignRetarget?.CampaignId);
                    if (maximumTimeOnSite >= advertisingCore.MinimumTimeOnSiteRemarketing)
                        advertisingCore.RetargetScore = totalViews * 3000;
                }
                else
                {
                    advertisingCore.RetargetScore = totalViews * 3000;
                }
            }
            else
            {
                advertisingCore.RetargetScore = 0;
            }

            var adClick = await _redisCacheService.HashGetAsync<double>(
                RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Advertising, ReportMetric.Click,
                    advertisingCore.SubId),
                "0");
            var adView = await _redisCacheService.HashGetAsync<double>(
                RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Advertising, ReportMetric.View,
                    advertisingCore.SubId),
                "0");
            var adCtr = adView != 0 ? adClick / adView : 0;
            var adPaid = await _redisCacheService.HashGetAsync<double>(
                RedisReportKeys.GetAdverKey(ReportAdvertiserDimension.Advertising, ReportMetric.Paid,
                    advertisingCore.SubId),
                DateTimeOffset.Now.GetDayOfYear().ToString());

            advertisingCore.Reach = adView;
            advertisingCore.Click = adClick;
            advertisingCore.Ctr = adCtr;
            advertisingCore.DailyCost = adPaid;

            matches.Add(new AdvertisingMatchScore
            {
                Advertising = advertisingCore,
                Score = 0
            });
        }

        return matches;
    }

    public async Task<List<AdvertisingCore?>?> MatchesAsync(
        ConcurrentDictionary<long, AdvertisingCore> advertisingCores,
        ZoneCore zoneCore,
        WebsiteCore website,
        RequestParams @params,
        AdvertisingTemplateCore? templateCore,
        ClientManager clientManager)
    {
        var matchedWithZone = await MatchesWithZone(advertisingCores, zoneCore, website, @params, clientManager);
        var matchedCampaigns = matchedWithZone.Select(x => x.Advertising)
            .Select(y => y?.CampaignId).Distinct();
        if (!matchedCampaigns.Any()) return null;

        // Xử dụng cơ chế xoay vòng quảng cáo
        var advertising = GetRandomAdvertising(matchedWithZone, templateCore, clientManager);
        return advertising.Select(x => x.Advertising).ToList();
    }

    private static int AdvertisingOnBox(AdvertisingTemplateCore? templateCore)
    {
        if (templateCore != null)
            return templateCore.TemplateType switch
            {
                TemplateType.FlyingCarpet => 1,
                TemplateType.InReadEcommerce => 4,
                TemplateType.InteractiveBanner => 5,
                TemplateType.MobileBannerCard => 1,
                TemplateType.PostInRead => 4,
                TemplateType.CatfishEcom => 5,
                _ => 1
            };
        return 1;
    }

    private async Task<bool> Matched(
        ZoneCore? zoneCore,
        WebsiteCore? website,
        AdvertisingCore advertisingCore,
        RequestParams @params,
        ClientManager clientManager)
    {
        try
        {
            var location = await _locationService.IPToLocation(clientManager.Ip);
            return HasPrice(website, advertisingCore)
                   && MatchedLocation(location, advertisingCore)
                   && MatchedTimeSpan(advertisingCore)
                   && MatchedZone(zoneCore, @params)
                   && MatchedReMarketing(website, advertisingCore, clientManager)
                   && MatchedDomain(website, @params)
                   && await MatchedObjectGroup(advertisingCore, clientManager, _reportService, _objectGroupExecutedValues);
        }
        catch
        {
            return false;
        }
    }

    // Kiểm tra tính active của bảng giá
    private static bool HasPrice(WebsiteCore? website, AdvertisingCore advertisingCore)
    {
        var advertisingSet = CacheManager.AdvertisingSets.Values
            .FirstOrDefault(x => x.Id == advertisingCore?.AdvertisingSetId);
        // Kiểm tra giá đồng giá
        return advertisingSet?.AdvertisingSetWebsiteCores?.Any(x =>
            x.WebsiteCondition == WebsiteCondition.Include && x.WebsiteId == website?.Id) == true;
    }

    private static bool MatchedLocation(IPLocationCore? location, AdvertisingCore advertisingCore)
    {
        var advertisingSet = CacheManager.AdvertisingSets.Values
            .FirstOrDefault(x => x.Id == advertisingCore.AdvertisingSetId);
        // TODO: DONE # Hòa NX - chưa xong phần match location rồi
        if (location is null)
        {
            return false;
        }
        if (location.CountryId == ConfigValues.VietnamCountryId)//nếu là VN khớp tỉnh thành
        {
            return advertisingSet?.LocationCores?.Any(x =>
            {
                var locationCore = CacheManager.Locations.Values.FirstOrDefault(y => y.Id == x.LocationId);
                return locationCore?.ProvinceId == location.VnProvinceId;
            }) ?? false;
        }
        return advertisingSet?.LocationCores?.Any(x =>
        {
            var locationCore = CacheManager.Locations.Values.FirstOrDefault(y => y.Id == x.LocationId);
            return locationCore?.RegionName == "Nước ngoài";
        }) ?? false;
    }

    private static bool MatchedTimeSpan(AdvertisingCore advertisingCore)
    {
        var campaign = CacheManager.Campaigns.Values.FirstOrDefault(x => x.Id == advertisingCore.CampaignId);
        if (campaign is null)
        {
            return false;
        }
        var currentTime = DateTimeOffset.UtcNow;
        if (campaign.EcommerceScheduled == false)
        {
            var advertisingSet = CacheManager.AdvertisingSets.Values.FirstOrDefault(x => x.Id == advertisingCore.AdvertisingSetId);
            if (advertisingSet?.ScheduledAllTime == true)
            {
                return true;
            }
            var adSetResult = advertisingSet?.SchedulingCore == null || advertisingSet.SchedulingCore.Any(x =>
                x.DayOfWeek == currentTime.DayOfWeek && x.Timing.Hour == currentTime.Hour);
            return adSetResult;
        }
        if (campaign.EcommerceScheduledAllTime)
        {
            return true;
        }

        var result = campaign.SchedulingCore == null || campaign.SchedulingCore.Any(x =>
            x.DayOfWeek == currentTime.DayOfWeek && x.Timing.Hour == currentTime.Hour);
        return result;
    }

    private static bool MatchedZone(ZoneCore? zoneCore, RequestParams @params)
    {
        return zoneCore?.SubId == long.Parse(@params.Zone);
    }

    private static bool MatchedReMarketing(WebsiteCore? website, AdvertisingCore advertisingCore, ClientManager clientManager)
    {
        var advertisingSet = CacheManager.AdvertisingSets.Values
            .FirstOrDefault(x => x.Id == advertisingCore.AdvertisingSetId);
        var isRemarketingWebsite = advertisingSet?.AdvertisingSetWebsiteCores?.Any(item =>
                item.WebsiteCondition == WebsiteCondition.Include && item.WebsiteId == website?.Id) == true;

        var isRemarketingBehavior = clientManager.DailyView < advertisingSet?.AdImpressions;
        if (advertisingSet?.ClickedAdvertising == true)
        {
            isRemarketingBehavior = isRemarketingBehavior && clientManager.TotalClick == 0;
        }
        if (advertisingSet?.Viewer == true)
        {
            isRemarketingBehavior = isRemarketingBehavior && clientManager.TotalView < advertisingSet.ViewedMax;
        }
        if (advertisingSet?.ExcludedWebsite == true)
        {
            isRemarketingBehavior = isRemarketingBehavior && advertisingSet.AdvertisingSetWebsiteCores?.Any(item =>
                item.WebsiteCondition == WebsiteCondition.Exclude && item.WebsiteId == website?.Id) == false;
        }

        return advertisingSet?.RemarketingType switch
        {
            RemarketingType.OnlyRemarketing => isRemarketingBehavior,
            RemarketingType.NoMoreRemarketing => isRemarketingWebsite,
            RemarketingType.MoreRemarketing => isRemarketingBehavior && isRemarketingWebsite,
            _ => false
        };
    }

    private static bool MatchedDomain(WebsiteCore? websiteCore, RequestParams @params)
    {
        if (@params?.Domain is null)
        {
            return false;
        }
        if (websiteCore?.Active != true)
        {
            return false;
        }
        var isMatchDomain = false;
        if (websiteCore?.Domain == @params.Domain)
        {
            isMatchDomain = true;
        }
        else if (websiteCore?.DomainAlias != null &&
                 websiteCore.DomainAlias.Select(x => x.Name).Contains(@params.Domain))
        {
            isMatchDomain = true;
        }
        else
        {
            // xử lý t/h accept mọi sub domain nếu có domain alias *.domain.com
            var domain = @params.Domain.ToLower();
            if (websiteCore?.DomainAlias == null) return isMatchDomain;
            foreach (var domainAlias in websiteCore.DomainAlias)
            {
                if (!domainAlias.Name.Contains('*')) continue;

                var regexPattern = domainAlias.Name.Replace("*", @"[A-Za-z0-9\-]+") + "$";
                var match = Regex.Match(domain, regexPattern, RegexOptions.IgnoreCase);
                if (!match.Success) continue;

                isMatchDomain = true;
                break;
            }
        }

        return isMatchDomain;
    }

    private static async Task<bool> MatchedObjectGroup(
        AdvertisingCore advertising,
        ClientManager clientManager,
        IReportService reportService,
        Dictionary<string, bool> objectGroupExecutedValues)
    {
        var advertisingSet = CacheManager.AdvertisingSets.Values
            .FirstOrDefault(x => x.Id == advertising.AdvertisingSetId);
        if (advertisingSet is null)
        {
            return false;
        }
        if (!advertisingSet.HasObject)
        {
            return true;
        }

        var advertiser = CacheManager.IdentityUsers.Values.FirstOrDefault(x => x.Id == advertising.CreatedBy);

        var advertisingSetGroups = CacheManager.AdvertisingSetObjects.Values
            .Where(x => x.AdvertisingSetId == advertisingSet.Id)
            .ToList();
        foreach (var advertisingSetGroup in advertisingSetGroups)
        {
            var objectGroup =
                CacheManager.ObjectGroups.Values.FirstOrDefault(x => x.Id == advertisingSetGroup.ObjectId);
            var objectGroupExecutedKey =
                ObjectGroupExecutedKey(advertiser?.Id, objectGroup?.Id, clientManager.ClientId);
            if (objectGroupExecutedValues.ContainsKey(objectGroupExecutedKey))
            {
                var executedValue = objectGroupExecutedValues[objectGroupExecutedKey];
                if (executedValue)
                {
                    return true;
                }
                continue;
            }
            // check website condition
            var websiteConditions = CacheManager.ObjectGroupWebsiteConditions.Values
                .Where(x => x.ObjectGroupId == objectGroup?.Id)
                .ToList();
            var websiteConditionTree = GenerateWebsiteConditionTree(websiteConditions);

            var websiteConditionMatch = false;
            foreach (var websiteConditionItems in websiteConditionTree)
            {
                var websiteKeys = websiteConditionItems
                    .Select(websiteCondition =>
                        RedisEventKeys.PageView(advertiser?.SubId ?? default, clientManager.ClientId, websiteCondition.ConditionType, websiteCondition.Value))
                    .ToList();
                var websiteMatched = await reportService.MatchAllKeys((int) RedisDatabases.ServiceEvent, websiteKeys);
                websiteConditionMatch = websiteConditionMatch || websiteMatched;
            }

            if (!websiteConditionMatch)
            {
                SetObjectGroupExecutedValues(objectGroupExecutedValues, advertiser?.Id, objectGroup, clientManager, false);
                continue;
            }

            // check behavior
            var behaviors = CacheManager.ObjectGroupConsumerBehaviors.Values
                .Where(x => x.ObjectGroupId == objectGroup?.Id)
                .ToList();
            var behaviorTree = GenerateConsumerBehaviorTree(objectGroup, behaviors, clientManager);
            var behaviorMatched = await reportService.MatchAllKeys((int) RedisDatabases.ServiceEvent, behaviorTree);
            if (!behaviorMatched)
            {
                SetObjectGroupExecutedValues(objectGroupExecutedValues, advertiser?.Id, objectGroup, clientManager, false);
                continue;
            }

            SetObjectGroupExecutedValues(objectGroupExecutedValues, advertiser?.Id, objectGroup, clientManager, true);
            return true;
        }
        return false;
    }
    
    /// <summary>
    /// Date: 05/14/2022
    /// Author: hoanx
    /// -------------------
    /// Developer: anhlq
    /// Version: 1
    /// Before: 
    /// After: Giảm trọng số nhân số lần lặp lại của ad từ 8 -> 3
    /// -------------------
    /// </summary>
    /// <param name="advertising"></param>
    /// <param name="templateCore"></param>
    /// <param name="clientManager"></param>
    /// <returns></returns>
    // advertisings những tin quảng cáo phù hợp với lượt xem này 
    // number of item: số lượng tin cần lấy => lấy từ cấu hình template ra (Fix cứng number of item nên có db để cấu hình trong bảng template tối thiểu = 1)
    public List<AdvertisingMatchScore> GetRandomAdvertising(
        List<AdvertisingMatchScore>? advertising,
        AdvertisingTemplateCore? templateCore,
        ClientManager clientManager)
    {
        if (advertising is not {Count: > 0}) return new List<AdvertisingMatchScore>();
        var numberOfItem = AdvertisingOnBox(templateCore);
        
        var url = clientManager.JsUrl;
        var urlSegments = url.Split('/').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
        var matchedCategories =
            CacheManager.Categories.Values
                .Where(x => x.Name != "Khác" && urlSegments.Any(segment => x.Slug.Contains(segment))).ToList();
        if (matchedCategories.Count == 0)
        {
            var otherCategory = CacheManager.Categories.Values.FirstOrDefault(x => x.Name == "Khác");
            if (otherCategory is not null)
            {
                matchedCategories.Add(otherCategory);
            }
        }

        List<AdvertisingMatchScore> advertisingMatchScores;

        // tính tổng lượt xem
        var totalImp = advertising.Sum(t => t.Advertising?.Reach ?? default);

        // tính tổng click
        var totalClick = advertising.Sum(t => t.Advertising?.Click ?? default);
        var averageCtr = totalImp > 0 ? totalClick / totalImp : 1;

        // số lượt xem trung bình
        var averageImp = totalImp / advertising.Count;

        // 20% số lượt xem
        var imp20PercentAverage = averageImp * 0.2;

        // 75% CTR
        var ctr75Percent = averageCtr * 0.75;

        // CTR max của list
        var maxCtr = advertising.Max(t => t.Advertising?.Ctr ?? default) > 0
            ? advertising.Max(t => t.Advertising?.Ctr ?? default)
            : 1;

        var divisibleAverageCtr = averageCtr == 0 ? 1 : averageCtr;
        
        var lstAdvItemId = new List<Guid>(); // List ID AdvItem để random
        var itemHasChangeToRun = 0;
        foreach (var advItem in advertising)
        {
            var numAdded = 0; //biến lưu số item clone
         
            // Tin mới setup -> CTR fix 1.0
            if (advItem.Advertising?.Reach == 0)
            {
                numAdded = (int)Math.Ceiling((1.0 / divisibleAverageCtr) * 3);
                itemHasChangeToRun += 1;
            }
            // Tin có Imp < 20% Imp TB -> fix CTR = max
            else if (advItem.Advertising?.Reach < imp20PercentAverage)
            {
                numAdded = (int)Math.Ceiling((maxCtr / divisibleAverageCtr) * 3);
                itemHasChangeToRun += 1;
            }
            // Tin có Imp > 20% và CTR > 75% CTR TB -> x theo CTR
            else if (advItem.Advertising?.Reach >= imp20PercentAverage && advItem.Advertising?.Ctr > ctr75Percent)
            {
                numAdded = (int)Math.Ceiling(((advItem.Advertising?.Ctr ?? default) / divisibleAverageCtr) * 3);
                itemHasChangeToRun += 1;
            }
            // Tin có Imp > 20% và Ctr <= 75% CTR TB và số tin đang có ít hơn 3 lần số tin cần 
            else if (advItem.Advertising?.Reach >= imp20PercentAverage && advItem.Advertising?.Ctr <= ctr75Percent)
            {
                numAdded = 1;
                itemHasChangeToRun += 1;
            }
            
            var advertisingSet =
                CacheManager.AdvertisingSets.Values.FirstOrDefault(x => x.Id == advItem.Advertising?.AdvertisingSetId);
            if (advertisingSet is not null)
            {
                var advertisingSetCategories = advertisingSet.CategoryCores ?? new List<AdvertisingSetCategoryCore>();
                if (matchedCategories.Any(x => advertisingSetCategories.Any(y => y.CategoryId == x.Id)))
                {
                    numAdded *= 2;
                }
            }
                
            for (var i = 0; i < numAdded; i++)
            {
                lstAdvItemId.Add(advItem.Advertising?.Id ?? Guid.Empty);
            }
        }

        // random danh sách tin 
        if (lstAdvItemId.Count > 0)
        {
            var lstNumber = new List<Guid>();
            var random = new Random();
            var maxIteration = numberOfItem > 100 ? numberOfItem : 100;
            var iterationCount = 0;
            for (var i = 0; i < numberOfItem; i++)
            {
                if (iterationCount == maxIteration)
                {
                    break;
                }
                var randomIndex = random.Next(0, lstAdvItemId.Count - 1);
                if (lstNumber.Contains(lstAdvItemId.ElementAt(randomIndex)) && i < itemHasChangeToRun - 1)
                {
                    i -= 1;
                }
                else
                {
                    lstNumber.Add(lstAdvItemId.ElementAt(randomIndex));
                }

                iterationCount += 1;
            }

            advertisingMatchScores = lstNumber
                .Select(x => advertising.FirstOrDefault(item => item.Advertising?.Id == x)!)
                .ToList();
        }
        else
        {
            advertisingMatchScores = advertising.OrderBy(_ => Guid.NewGuid()).Take(numberOfItem).ToList();
        }

        //Nếu số Item nhỏ hơn số Item cần lấy => add từ trên xuống đến khi đủ
        var index = 0;
        while (advertisingMatchScores.Count < numberOfItem)
        {
            advertisingMatchScores.Add(advertisingMatchScores[index]);
            index++;
        }

        return advertisingMatchScores;
    }

    #region Object Group Process Methods

    private static List<List<ObjectGroupWebsiteConditionCore>> GenerateWebsiteConditionTree(
        List<ObjectGroupWebsiteConditionCore> websiteConditions)
    {
        var websiteConditionTree = new List<List<ObjectGroupWebsiteConditionCore>>();
        var firstWebsiteNode = websiteConditions.FirstOrDefault(x => x.BeforeConditionId == null);
        if (firstWebsiteNode is not null)
        {
            websiteConditionTree.Add(new List<ObjectGroupWebsiteConditionCore>
            {
                firstWebsiteNode
            });
            var currentWebsiteNode = firstWebsiteNode;
            var iterCount = 1;
            while (currentWebsiteNode.AfterConditionId is not null && iterCount < websiteConditions.Count)
            {
                if (currentWebsiteNode.AfterConditionType == ConditionType.Or)
                {
                    websiteConditionTree.Add(new List<ObjectGroupWebsiteConditionCore>());
                }
                currentWebsiteNode = websiteConditions.FirstOrDefault(x => x.Id == currentWebsiteNode.AfterConditionId);
                if (currentWebsiteNode is null)
                {
                    break;
                }
                websiteConditionTree.Last().Add(currentWebsiteNode);
                iterCount += 1;
            }
        }

        return websiteConditionTree;
    }

    private static RedisKeyTree GenerateConsumerBehaviorTree(
        ObjectGroupCore? objectGroup,
        List<ObjectGroupConsumerBehaviorCore> behaviors,
        ClientManager clientManager)
    {
        var tree = new RedisKeyTree
        {
            Nodes = new List<RedisKeyNode>()
        };
        var roots = behaviors.Where(x => x.ExcludeId == null);
        foreach (var item in roots)
        {
            var includeKeys = item.BehaviorType == BehaviorType.All
                ? ObjectGroupConsumerBehaviorConstants.BehaviorTypes
                    .Select(x => GetConsumerBehaviorRedisKey(objectGroup, x, clientManager)).ToList()
                : new List<RedisKey> {GetConsumerBehaviorRedisKey(objectGroup, item.BehaviorType, clientManager)};
            var excludedKeys = behaviors
                .Where(x => x.ExcludeId == item.BehaviorGroupId)
                .Select(x => GetConsumerBehaviorRedisKey(objectGroup, x.BehaviorType, clientManager))
                .ToList();

            var treeNode = new RedisKeyNode
            {
                Includes = includeKeys,
                Excludes = excludedKeys
            };
            tree.Nodes.Add(treeNode);
        }

        return tree;
    }

    private static RedisKey GetConsumerBehaviorRedisKey(
        ObjectGroupCore? objectGroup,
        BehaviorType behaviorType,
        ClientManager clientManager)
    {
        var productType = objectGroup?.ProductType ?? default;
        return objectGroup?.ApplyAllProducts switch
        {
            true => behaviorType switch
            {
                BehaviorType.ViewProduct => RedisEventKeys.ProductView(clientManager.ClientId),
                BehaviorType.AddToCart => RedisEventKeys.AddToCart(clientManager.ClientId),
                BehaviorType.Checkout => RedisEventKeys.Purchase(clientManager.ClientId),
                BehaviorType.Purchases => RedisEventKeys.PurchaseComplete(clientManager.ClientId),
                _ => new RedisKey {Database = (int) RedisDatabases.ServiceEvent, KeyName = ""}
            },
            _ => behaviorType switch
            {
                BehaviorType.ViewProduct => RedisEventKeys.ProductView(clientManager.ClientId, productType,
                    objectGroup?.ProductName?.Normalize().ToLower()),
                BehaviorType.AddToCart => RedisEventKeys.AddToCart(clientManager.ClientId, productType,
                    objectGroup?.ProductName?.Normalize().ToLower()),
                BehaviorType.Checkout => RedisEventKeys.Purchase(clientManager.ClientId, productType,
                    objectGroup?.ProductName?.Normalize().ToLower()),
                BehaviorType.Purchases => RedisEventKeys.PurchaseComplete(clientManager.ClientId, productType,
                    objectGroup?.ProductName?.Normalize().ToLower()),
                _ => new RedisKey {Database = (int) RedisDatabases.ServiceEvent, KeyName = ""}
            }
        };
    }

    private static string ObjectGroupExecutedKey(Guid? userId, Guid? objectGroupId, Guid? clientId) =>
        $"{userId}:{objectGroupId}:{clientId}";

    private static void SetObjectGroupExecutedValues(
        Dictionary<string, bool> objectGroupExecutedValues,
        Guid? userId,
        ObjectGroupCore? objectGroup,
        ClientManager clientManager,
        bool value)
    {
        objectGroupExecutedValues[ObjectGroupExecutedKey(userId, objectGroup?.Id, clientManager.ClientId)] = value;
    }

    #endregion
}