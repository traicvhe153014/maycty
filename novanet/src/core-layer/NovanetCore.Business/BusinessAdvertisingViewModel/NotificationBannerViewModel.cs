﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class NotificationBannerViewModel : BaseDisplayAdsViewModel
{
    public string? ShortcutNotification { get; set; }
    public string? ExtendNotification { get; set; }
    public bool IsCustomImage { get; set; }
    public List<string>? Images { get; set; }
    public bool IsDelay { get; set; }
    public int Delay { get; set; }
}