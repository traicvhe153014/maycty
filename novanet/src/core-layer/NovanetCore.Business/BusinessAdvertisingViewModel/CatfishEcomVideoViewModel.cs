﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class CatfishEcomVideoViewModel : BaseEcommerceViewModel
{
    public string BackgroundImageUrl { get; set; } = default!;
    public string Video { get; set; } = default!;
    public string ThumbnailVideo { get; set; } = default!;
}