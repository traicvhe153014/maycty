﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class DefaultViewModel : AdvertisingViewModel
{
    public string? BackgroundImageUrl { get; set; }
    
    public TemplateType? TemplateType { get; set; }
    
    public bool IsBackUp { get; set; }
}