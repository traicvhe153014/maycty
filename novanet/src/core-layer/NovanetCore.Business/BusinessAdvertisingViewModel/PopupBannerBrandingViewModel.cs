﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class PopupBannerBrandingViewModel : BaseDisplayAdsViewModel
{
    public string ExtendBanner { get; set; } = default!;
}