﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class CatfishEcomViewModel : BaseEcommerceViewModel
{
    public string? ExpandedContentUrl { get; set; }
    public string? BackgroundImageUrl { get; set; }
    public bool UseBannerTemplate { get; set; }
}