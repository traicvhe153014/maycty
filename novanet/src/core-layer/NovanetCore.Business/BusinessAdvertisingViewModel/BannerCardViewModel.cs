﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class BannerCardViewModel : BaseEcommerceViewModel
{
    public string? BackgroundImageUrl { get; set; }
    public bool UseBannerTemplate { get; set; }
}