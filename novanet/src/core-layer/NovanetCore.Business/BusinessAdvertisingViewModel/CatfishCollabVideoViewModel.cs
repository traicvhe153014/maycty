﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class CatfishCollabVideoViewModel : BaseDisplayAdsViewModel
{
    public string CollapseBanner { get; set; } = default!;
    public string Video { get; set; } = default!;
    public string ThumbnailVideo { get; set; } = default!;
}