﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class PopupBannerVideoViewModel : BaseDisplayAdsViewModel
{
    public string Video { get; set; } = default!;
    public string ThumbnailVideo { get; set; } = default!;
}