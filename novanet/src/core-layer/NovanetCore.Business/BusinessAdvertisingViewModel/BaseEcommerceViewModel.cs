﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class BaseEcommerceViewModel : AdvertisingViewModel
{
    public bool ProductName { get; set; }
    public bool ShowDiscountedPrice { get; set; }
    public bool ShowFreeDelivery { get; set; }
    public List<AdvertisedProduct> Products { get; set; } = default!;
}