﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class PostInReadViewModel : BaseEcommerceViewModel
{
    public string? AvatarUrl { get; set; }
    public string? AvatarName { get; set; }
    public string? PostContent { get; set; }
}