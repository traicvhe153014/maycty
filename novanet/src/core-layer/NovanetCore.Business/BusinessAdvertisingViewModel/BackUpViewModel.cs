﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class BackUpViewModel
{
    public long ZoneId { get; set; }
    public string BackUpCode { get; set; } = default!;
    public Guid ClientId { get; set; }
}