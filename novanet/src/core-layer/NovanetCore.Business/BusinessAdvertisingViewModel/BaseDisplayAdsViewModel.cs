﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class BaseDisplayAdsViewModel : AdvertisingViewModel
{
    public string Title { get; set; } = default!;
    public string Description { get; set; } = default!;
    public bool IsCta { get; set; }
    public string? Cta { get; set; }
    public string? CtaUrl { get; set; }
    public string Logo { get; set; } = default!;
    public string? HashedCtaQueryParams { get; set; }
}