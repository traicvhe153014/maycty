﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class AdvertisingViewModel
{
    public long ZoneId { get; set; }
    public long? CampaignId { get; set; }
    public long? AdvertisingId { get; set; }
    public long? AdvertisingTemplateId { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public string DisplayRootUrl { get; set; } = default!;
    public string WebsiteRootUrl { get; set; } = default!;
    public double SellPrice { get; set; }
    public double BuyPrice { get; set; }
    public double Price { get; set; }
    public string Timestamp { get; set; } = default!;
    public string HashedQueryParams { get; set; } = string.Empty;
    public Guid ClientId { get; set; }
    public string GoogleAnalyticsProperty { get; set; } = default!;
    public bool IsDefault { get; set; }
    public int CloseSize { get; set; }
    public bool UsingBackup { get; set; }
    public string? BackUpCode { get; set; }
    public string? LogoStatus { get; set; }
    public bool UseRedirectLink { get; set; }
    public string? RedirectLink { get; set; }
}

public class AdvertisedProduct
{
    public long? Id { get; set; }
    public string? Name { get; set; }
    public string? Price { get; set; }
    public string? DiscountedPrice { get; set; }
    public double DiscountPercentage { get; set; }
    public string? Link { get; set; }
    public string? ImageLink { get; set; }
    public bool ShowDiscount { get; set; }
    public string? HashedQueryParams { get; set; }
}