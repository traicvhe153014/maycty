﻿namespace NovanetCore.Business.BusinessAdvertisingViewModel;

public class CatfishCollabBrandingViewModel : BaseDisplayAdsViewModel
{
    public string CollapseBanner { get; set; } = default!;
    public string ExtendBanner { get; set; } = default!;
}