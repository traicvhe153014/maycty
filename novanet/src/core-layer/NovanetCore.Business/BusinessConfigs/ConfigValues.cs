﻿namespace NovanetCore.Business.BusinessConfigs;

public class ConfigValues
{
    public const int DefaultCloseSize = 14;

    public const int TotalMinuteGetData = 10;
    
    public const string Domain = "ads.novanet.vn";

    public const string Remarketing = "nvn_rmkt_";

    public const string RemarketingDomain = "nvn_domain_rmkt_";

    public const string MaximumTimeOnSite = "nvn_max_time_onsite";
    
    public const string History = "nvn_his_";

    public const string ClientId = "nvn_client_id";

    public const string CookieView = "nvn_cookie_view";

    public const string Clicked = "nvn_clicked";
    
    public const string NovanetAdvertising = "nvn_ad";
    
    public const string NovanetProduct = "nvn_product";

    public const int VietnamCountryId = 241;

    // HardCode để ghi chú cho việc số này ko map với giá trị trong location IP
    public const int UnknownProvinceId = 15;
    
    // HardCode để ghi chú cho việc số này ko map với giá trị trong location IP
    public const int UnknownCountryId = 500;

    public const string DefaultRedirectLink = "https://novanet.vn";
    
    public const string DefaultLogoStatus = "Block";

    public const string DefaultMobileBanner = "assets/images/default-banner/banner-Mobile-Banner-Card.jpg";
    public const string DefaultInteractiveBanner = "assets/images/default-banner/banner-Interactive-Banner.jpg";
    public const string DefaultFlyingCarpet = "assets/images/default-banner/banner-Flying-Carpet.jpg";
    public const string DefaultInReadEcommerce = "assets/images/default-banner/banner-Inread-Ecommerce.jpg";
    public const string DefaultCatfishEcomBanner = "assets/images/default-banner/banner-Catfish-Ecom.jpg";

}