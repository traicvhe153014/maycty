﻿using Microsoft.Extensions.DependencyInjection;
using Novanet.Core.Models;
using NovanetCore.Business.BusinessEvents;

namespace NovanetCore.Business;

public static class BusinessCoreExtension
{
    public static void AddBusinessCore(this IServiceCollection services)
    {
        services.AddSingleton<ICookieService, CookieService>();
        services.AddScoped<IAdvertisingService, AdvertisingService>();
        services.AddSingleton<ITemplateService, TemplateService>();
        services.AddSingleton<IRequestParamsService, RequestParamsService>();
        services.AddSingleton<ILocationService, LocationService>();
    }

    public static void AddCacheManagerServices(this IServiceCollection services)
    {
        services.AddScoped<CacheManagerService>();
        services.AddHostedService<UpdateCacheManagerService>();
        services.AddHostedService<PatchCacheManagerService<AdvertisingCore>>();
        services.AddHostedService<PatchCacheManagerService<AdvertisingSetCore>>();
        services.AddHostedService<PatchCacheManagerService<AdvertisingSetObjectCore>>();
        services.AddHostedService<PatchCacheManagerService<AdvertisingSetProductGroupCore>>();
        services.AddHostedService<PatchCacheManagerService<AdvertisingTemplateCore>>();
        services.AddHostedService<PatchCacheManagerService<AdvertisingTemplateAttributeCore>>();
        services.AddHostedService<PatchCacheManagerService<CampaignCore>>();
        services.AddHostedService<PatchCacheManagerService<IdentityUserValue>>();
        services.AddHostedService<PatchCacheManagerService<ObjectGroupCore>>();
        services.AddHostedService<PatchCacheManagerService<ObjectGroupConsumerBehaviorCore>>();
        services.AddHostedService<PatchCacheManagerService<ObjectGroupWebsiteConditionCore>>();
        services.AddHostedService<PatchCacheManagerService<ProductAttributeCore>>();
        services.AddHostedService<PatchCacheManagerService<ProductAttributeValueCore>>();
        services.AddHostedService<PatchCacheManagerService<ProductEntityCore>>();
        services.AddHostedService<PatchCacheManagerService<ProductFeedCore>>();
        services.AddHostedService<PatchCacheManagerService<ProductGroupEntityCore>>();
        services.AddHostedService<PatchCacheManagerService<WebsiteCore>>();
        services.AddHostedService<PatchCacheManagerService<WebsitePriceCore>>();
        services.AddHostedService<PatchCacheManagerService<WebsiteProhibitedCategoryCore>>();
        services.AddHostedService<PatchCacheManagerService<ZoneCore>>();
    }
}