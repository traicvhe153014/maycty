﻿global using NovanetCore.Business.BusinessServices;
global using NovanetCore.Business.BusinessDomain.Service.Settings;
global using Novanet.Core.Extensions;
global using NovanetCore.Business.BusinessConfigs;
global using NovanetCore.Business.BusinessObjects;
global using NovanetCore.ValueObject;