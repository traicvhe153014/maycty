﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Novanet.Core.Domain;
using Novanet.Core.Models;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace NovanetCore.Business.BusinessManager;

public class CacheManager
{
    public static DateTimeOffset? LastUpdatedStart { get; set; }

    public static DateTimeOffset? LastUpdatedEnd { get; set; }

    public static ConcurrentDictionary<long, ZoneCore> Zones { get; set; } = new();

    public static ConcurrentDictionary<long, CampaignCore> Campaigns { get; set; } = new();

    public static ConcurrentDictionary<long, WebsiteCore> Websites { get; set; } = new();

    public static ConcurrentDictionary<long, WebsitePriceCore> WebsitePrices { get; set; } = new();

    public static ConcurrentDictionary<long, AdvertisingCore> Advertising { get; set; } = new();

    public static ConcurrentDictionary<long, AdvertisingSetCore> AdvertisingSets { get; set; } = new();

    public static ConcurrentDictionary<long, AdvertisingSetProductGroupCore> AdvertisingSetProductGroups { get; set; } =
        new();

    public static ConcurrentDictionary<long, AdvertisingSetObjectCore> AdvertisingSetObjects { get; set; } = new();

    public static ConcurrentDictionary<long, ObjectGroupCore> ObjectGroups { get; set; } = new();

    public static ConcurrentDictionary<long, ObjectGroupWebsiteConditionCore>
        ObjectGroupWebsiteConditions { get; set; } = new();

    public static ConcurrentDictionary<long, ObjectGroupConsumerBehaviorCore>
        ObjectGroupConsumerBehaviors { get; set; } = new();

    public static ConcurrentDictionary<long, AdvertisingTemplateCore> Templates { get; set; } = new();

    public static ConcurrentDictionary<long, AdvertisingTemplateAttributeCore> AdvertisingTemplateAttributes
    {
        get;
        set;
    } = new();

    public static ConcurrentDictionary<long, IPBlockCore> IpBlocks { get; set; } = new();

    public static ConcurrentDictionary<long, IPLocationCore> IpLocations { get; set; } = new();

    public static ConcurrentDictionary<long, LogSession> LogSessions { get; set; } = new();

    public static ConcurrentDictionary<long, ProductEntityCore> Products { get; set; } = new();

    public static ConcurrentDictionary<long, ProductFeedCore> ProductFeeds { get; set; } = new();

    public static ConcurrentDictionary<long, ProductAttributeCore> ProductAttributes { get; set; } = new();

    public static ConcurrentDictionary<long, ProductAttributeValueCore> ProductAttributeValues { get; set; } = new();

    public static ConcurrentDictionary<long, ProductGroupEntityCore> ProductGroupEntities { get; set; } = new();

    public static ConcurrentDictionary<long, LocationCore> Locations { get; set; } = new();

    public static ConcurrentDictionary<long, IdentityUserValue> IdentityUsers { get; set; } = new();


    public static ConcurrentDictionary<string, ObjectPageViewItem> ObjectPageViewItems { get; set; } = new();

    public static ConcurrentDictionary<string, ObjectBehaviorWithProductItem> ObjectBehaviorWithProductItems
    {
        get;
        set;
    } = new();

    public static ConcurrentDictionary<long, ProhibitedCategoryCore> ProhibitedCategories { get; set; } = new();
    
    public static ConcurrentDictionary<long, CategoryCore> Categories { get; set; } = new();

    public static ConcurrentDictionary<long, T>? Set<T>(ILogger logger) where T : INovanetDocument
    {
        logger.LogInformation("{Time} Start recache {Type}", DateTimeOffset.Now, typeof(T).Name);
        try
        {
            foreach (var propertyInfo in typeof(CacheManager).GetProperties())
            {
                if (propertyInfo.PropertyType != typeof(ConcurrentDictionary<long, T>)) continue;
                var value = propertyInfo.GetValue(null, null);
                return (ConcurrentDictionary<long, T>)value!;
            }

            return null;
        }
        catch (Exception e)
        {
            logger.LogError("{Time} Error: {Message}", DateTimeOffset.Now, e.ToString());
            return null;
        }
    }
}