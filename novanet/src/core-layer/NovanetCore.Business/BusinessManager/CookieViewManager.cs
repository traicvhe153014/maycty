﻿using Microsoft.AspNetCore.Http;

namespace NovanetCore.Business.BusinessManager;

public class CookieViewManager
{
    public Dictionary<long, CookieInfoItem> CookieInfoItems { get; }

    public CookieViewManager(IRequestCookieCollection cookies)
    {
        CookieInfoItems = cookies[ConfigValues.CookieView]?.Deserialize<Dictionary<long, CookieInfoItem>>() ??
                          new Dictionary<long, CookieInfoItem>();
    }
    
    public void AddView(long advertisingId, DateTimeOffset viewTime)
    {            
        if (CookieInfoItems.ContainsKey(advertisingId))
        {              
            CookieInfoItems[advertisingId].CountInDay += 1;
            CookieInfoItems[advertisingId].LastViewTime = viewTime.ToHexCode();
            CookieInfoItems[advertisingId].CountAll += 1;
        }
        else
        {
            var objAdvViewCookieObj = new CookieInfoItem
            {
                AdvertisingId = advertisingId,
                CountAll = 1,
                CountInDay = 1,
                LastViewTime = viewTime.ToHexCode(),
                ClickCount = 0,
                ConversionCount = 0
            };
            CookieInfoItems.Add(objAdvViewCookieObj.AdvertisingId, objAdvViewCookieObj);
        }
    }
    
    public void AddClick(long advertisingId)
    {
        if (CookieInfoItems.ContainsKey(advertisingId))
        {
            CookieInfoItems[advertisingId].ClickCount += 1;
        }
    }
}