﻿using System.Collections.Specialized;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace NovanetCore.Business.BusinessManager;

public class ClientManager
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly NameValueCollection _paramQueryString;
    private readonly ILogger _logger;

    public ClientManager(
        IHttpContextAccessor httpContextAccessor,
        IQueryCollection? queryCollection,
        DateTimeOffset timestamp, string? queryString,
        ILogger logger)
    {
        _paramQueryString = HttpUtility.ParseQueryString((queryString ?? queryCollection?.ToString()) ?? string.Empty);
        _httpContextAccessor = httpContextAccessor;
        _logger = logger;
    }

    public ZoneCore ZoneCore => CacheManager.Zones.ContainsKey(Zone) ? CacheManager.Zones[Zone] : new ZoneCore();

    public long Zone => long.TryParse(_paramQueryString[RequestParams.ZoneKey], out var value) ? value : default;
    public string Timestamp => _paramQueryString[RequestParams.TimestampKey] ?? string.Empty;

    public long Advertising =>
        long.TryParse(_paramQueryString[RequestParams.AdvertisingKey], out var value) ? value : default;

    public long Campaign =>
        long.TryParse(_paramQueryString[RequestParams.CampaignKey], out var value) ? value : default;

    public long Product => long.TryParse(_paramQueryString[RequestParams.ProductKey], out var value) ? value : default;

    public long Template => long.TryParse(_paramQueryString[RequestParams.AdvertisingTemplateKey], out var value)
        ? value
        : default;

    private Guid Referer => _paramQueryString[RequestParams.RefererKey].ToGuid();
    private Guid Hash => _paramQueryString[RequestParams.HashKey].ToGuid();

    public DateTimeOffset UrlCreatedOn =>
        _paramQueryString[RequestParams.UrlCreateOnKey]?.ToDateTime() ?? DateTimeOffset.UtcNow;

    // public string Header => HttpUtility.UrlDecode(_httpContextAccessor.HttpContext?.Request.Headers.ToRawString() ?? string.Empty);
    public IHeaderDictionary? Headers
    {
        get
        {
            try
            {
                return _httpContextAccessor.HttpContext?.Request.Headers;
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return null;
            }
        }
    }
    public string UserAgent 
    {
        get
        {
            try
            {
                return _httpContextAccessor.HttpContext?.Request.Headers.UserAgent.ToString() ?? string.Empty;
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return string.Empty;
            }
        }
    }

   /// <summary>
    /// Lấy IP public của client theo độ ưu tiên các dữ liệu có tồn tại trên request
    /// - X-Forwarded-For (public IP)
    /// - X-Real-IP (public IP)
    /// - Remote Address (public IP)
    /// - query parameter ip trên request
    /// </summary>
    public string Ip
    {
        get
        {
            try
            {
                var ip = "";
                if (_httpContextAccessor.HttpContext is not null)
                {
                    _httpContextAccessor.HttpContext.Request.Headers.TryGetValue(RequestHeader.XForwardedFor, out var xForwardedForIp);
                    if (!string.IsNullOrEmpty(xForwardedForIp))
                    {
                        ip = xForwardedForIp;
                    }
                }
                ip ??= string.Empty;
                ip = ip.TrimEnd(',')
                    .Split(',')
                    .Select(s => s.Trim())
                    .FirstOrDefault() ?? string.Empty;
                if (!string.IsNullOrWhiteSpace(ip) && !LocationService.IsPrivate(ip)) return ip;

                // X-Real-IP header 
                if (_httpContextAccessor.HttpContext is not null)
                {
                    _httpContextAccessor.HttpContext.Request.Headers.TryGetValue(RequestHeader.XRealIp, out var realIp);
                    if (string.IsNullOrWhiteSpace(realIp))
                    {
                        ip = realIp;
                    }
                }
                if (!string.IsNullOrWhiteSpace(ip) && !LocationService.IsPrivate(ip)) return ip;

                // RemoteIpAddress is always null in DNX RC1 Update1.
                if (_httpContextAccessor.HttpContext?.Connection?.RemoteIpAddress != null)
                {
                    var remoteIpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    if (string.IsNullOrWhiteSpace(remoteIpAddress))
                    {
                        ip = remoteIpAddress;
                    }
                }
                if (!string.IsNullOrWhiteSpace(ip) && !LocationService.IsPrivate(ip)) return ip;
                
                if (_httpContextAccessor.HttpContext is not null)
                {
                    _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("REMOTE_ADDR", out var remoteAddrIp);
                    if (string.IsNullOrWhiteSpace(remoteAddrIp))
                    {
                        ip = remoteAddrIp;
                    }
                }
                if (!string.IsNullOrWhiteSpace(ip) && !LocationService.IsPrivate(ip)) return ip;
                
                if (_paramQueryString[RequestParams.IpKey] is not null)
                {
                    return _paramQueryString[RequestParams.IpKey]!;
                }

                ip ??= string.Empty;
                return ip;
            }
            catch (Exception e) 
            {
                _logger.LogError(e.ToString());
                return string.Empty;
            }
        }
    }

    public string JsReferer => _paramQueryString[RequestParams.JsRefererKey] ?? string.Empty;

    public string JsUrl => _paramQueryString[RequestParams.UrlKey] ?? string.Empty;

    public int JsHistoryLength => int.TryParse(_paramQueryString[RequestParams.JsHistoryLengthKey], out var value)
        ? value
        : default;

    public bool IsEnableCookie => bool.TryParse(_paramQueryString[RequestParams.IsEnableCookieKey], out var value)
        ? value
        : default;

    public bool IsEnableFlash => bool.TryParse(_paramQueryString[RequestParams.IsEnableFlashKey], out var value)
        ? value
        : default;

    public bool IsCreateNewClientId
    {
        get
        {
            try
            {
                return _httpContextAccessor.HttpContext is null ||
                       !_httpContextAccessor.HttpContext.Request.Cookies.TryGetValue(ConfigValues.ClientId,
                           out var value) ||
                       value is null;
            }
            catch (Exception e) 
            {
                _logger.LogError(e.ToString());
                return true;
            }
        }
    }

    public double SellPrice =>
        double.TryParse(_paramQueryString[RequestParams.SellPriceKey], out var value) ? value : default;

    public double BuyPrice =>
        double.TryParse(_paramQueryString[RequestParams.BuyPriceKey], out var value) ? value : default;

    public double Price => double.TryParse(_paramQueryString[RequestParams.PriceKey], out var value) ? value : default;

    public string TargetUrl => _paramQueryString[RequestParams.TargetUrlKey] ?? string.Empty;

    public string Domain => _paramQueryString[RequestParams.DomainKey] ?? string.Empty;

    public bool IsDefault =>
        bool.TryParse(_paramQueryString[RequestParams.IsDefaultKey], out var value) ? value : default;

    public long Time =>
        long.TryParse(_paramQueryString[RequestParams.TimeKey], out var value) ? value : default;

    public int? CloseSize => int.TryParse(_paramQueryString[RequestParams.CloseSizeKey], out var value) ? value : null;
    
    public string? LogoStatus => _paramQueryString[RequestParams.LogoStatusKey];

    private readonly Guid _clientId = Guid.NewGuid();
    public Guid ClientId
    {
        get
        {
            try
            {
                if (_httpContextAccessor.HttpContext is null)
                {
                    return _clientId;
                }

                _httpContextAccessor.HttpContext.Request.Cookies.TryGetValue(ConfigValues.ClientId, out var clientId);
                var parsedClientId = clientId?.ToGuid();
                if (parsedClientId is not null && parsedClientId != Guid.Empty)
                {
                    return parsedClientId.Value;
                }
                return _clientId;
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return _clientId;
            }
        }
    }

    public string XForwardedFor
    {
        get
        {
            try
            {
                if (_httpContextAccessor.HttpContext is null) return string.Empty;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue(RequestHeader.XForwardedFor,
                    out var xForwardedForIp);
                return xForwardedForIp;
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return string.Empty;
            }
        }
    }
    
    public string XRealIp
    {
        get
        {
            try
            {
                if (_httpContextAccessor.HttpContext is null) return string.Empty;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue(RequestHeader.XRealIp,
                    out var xRealIp);
                return xRealIp;
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return string.Empty;
            }
        }
    }

    public double TotalClick { get; set; }
    public double DailyView { get; set; }
    public double TotalView { get; set; }
}