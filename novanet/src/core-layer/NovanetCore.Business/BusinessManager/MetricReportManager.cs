﻿using System.Collections.Concurrent;
using Novanet.Core.Enums.ReportKeyObjects;

namespace NovanetCore.Business.BusinessManager;

public class MetricReportManager
{
    public static ConcurrentDictionary<ReportAdvertiserDimension, MetricReport>? AdvertiserReport { get; set; }
}