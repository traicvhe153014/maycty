﻿using MediatR;

namespace NovanetCore.Business.BusinessParallels;

public class UpdateCacheManagerParallel : INotification
{
    internal class Handler : INotificationHandler<UpdateCacheManagerParallel>
    {
        private readonly CacheManagerService _cacheManagerService;

        public Handler(CacheManagerService cacheManagerService)
        {
            _cacheManagerService = cacheManagerService;
        }
        
        public async Task Handle(UpdateCacheManagerParallel notification, CancellationToken cancellationToken)
        {
            await _cacheManagerService.Processing();
        }
    }
}