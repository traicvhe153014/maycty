﻿using MediatR;

namespace NovanetCore.Business.BusinessParallels;

public class UpdateMetricReportManagerParallel : INotification
{
    internal class Handler : INotificationHandler<UpdateMetricReportManagerParallel>
    {
        private readonly MetricReportManagerService _metricReportManagerService;

        public Handler(MetricReportManagerService metricReportManagerService)
        {
            _metricReportManagerService = metricReportManagerService;
        }

        public async Task Handle(UpdateMetricReportManagerParallel notification, CancellationToken cancellationToken)
        {
            await _metricReportManagerService.Processing();
        }
    }
}