﻿using NovanetCore.Business.BusinessObjects.FraudClickObjects;

namespace NovanetCore.Business.BusinessControllers;

public static class PublisherAdvertiserRulesConfigController
{
    /// <summary>
        /// Hàm lấy giá trị config trong dicData, tham số truyền vào là PublisherId, AdvertiserId,Type:(1: Pub, 2: Web), RulesId, KeyConfigId,
        /// </summary>
        /// <param name="dicData"></param>
        /// <param name="publisherId"></param>
        /// <param name="advertiserId"></param>
        /// <param name="rulesId"></param>
        /// <param name="keyConfig"></param>
        /// <returns></returns>
        public static double GetConfigValueByWebsiteAdvertiserRulesKeyConfig(
            Dictionary<string, List<FraudClickKeyConfigValue>>? dicData,
            long publisherId,
            long advertiserId,
            long rulesId,
            int keyConfig)
        {

            #region Trường hợp bảng Config = null => trả về các giá trị mặc định.

            if (dicData == null)
            {
                switch (rulesId)
                {
                    case (int)FraudClickRules.Ips when keyConfig == (int)FraudClickKeyConfig.ClickUrlRate:
                        return 0.3;
                    case (int)FraudClickRules.DoubleClick when keyConfig == (int)FraudClickKeyConfig.TimeSetDoubleClick:
                        return 300;
                    case (int)FraudClickRules.DoubleClick when keyConfig == (int)FraudClickKeyConfig.HighRate:
                        return 0.2;
                    case (int)FraudClickRules.CreateTimeClientId when keyConfig == (int)FraudClickKeyConfig.FilterRate:
                        return 3;
                    case (int)FraudClickRules.CreateTimeClientId when keyConfig == (int)FraudClickKeyConfig.TimeSetIsNewClient:
                        return 300;
                    case (int)FraudClickRules.ClientIdClickRates when keyConfig == (int)FraudClickKeyConfig.ClientIdClickAverageRates:
                    case (int)FraudClickRules.ClientIdClickRates when keyConfig == (int)FraudClickKeyConfig.DomainClickAverageRates:
                        return 3;
                    case (int)FraudClickRules.TimeSpanViewToClicked when keyConfig == (int)FraudClickKeyConfig.TimeSpanViewToClicked:
                        return 0;
                }
            }

            #endregion

            List<FraudClickKeyConfigValue> value;
            //Truyền đầy đủ các thông số để lấy config.
            var stringKey = $"{publisherId};{advertiserId};{rulesId}";
            var key = stringKey.Base64Encode();
            if (dicData is not null && dicData.ContainsKey(key))
            {
                value = dicData[key].Where(p => p.KeyConfigId == keyConfig).ToList();
                if (!value.Any()) return 0;
                var firstOrDefault = value.FirstOrDefault();
                if (firstOrDefault != null)
                    return firstOrDefault.Value;
                return 0;
            }
            //Lấy Config của tất cả Website áp dụng cho toàn chiến dịch.
            stringKey = $"{publisherId};{0};{rulesId}";
            key = stringKey.Base64Encode();
            if (dicData is not null && dicData.ContainsKey(key))
            {
                value = dicData[key].Where(p => p.KeyConfigId == keyConfig).ToList();
                if (!value.Any()) return 0;
                var firstOrDefault = value.FirstOrDefault();
                if (firstOrDefault != null)
                    return firstOrDefault.Value;
                return 0;
            }
            //Lấy Config của Campaign áp dụng cho toàn bộ Website.
            stringKey = $"{0};{advertiserId};{rulesId}";
            key = stringKey.Base64Encode();
            if (dicData is not null && dicData.ContainsKey(key))
            {
                value = dicData[key].Where(p => p.KeyConfigId == keyConfig).ToList();
                if (!value.Any()) return 0;
                var firstOrDefault = value.FirstOrDefault();
                if (firstOrDefault != null)
                    return firstOrDefault.Value;
                return 0;
            }
            //Lấy config của tất cả các Website đối với tất cả Campaign (Config mặc định.)
            stringKey = $"{0};{0};{rulesId}";
            key = stringKey.Base64Encode();
            if (dicData is null || !dicData.ContainsKey(key)) return 0;
            {
                value = dicData[key].Where(p => p.KeyConfigId == keyConfig).ToList();
                if (!value.Any()) return 0;
                var firstOrDefault = value.FirstOrDefault();
                if (firstOrDefault != null)
                    return firstOrDefault.Value;
                return 0;
            }

        }
}