﻿namespace NovanetCore.Business.BusinessConstants
{
    public static class TemplateValueProducts
    {
        public static readonly Dictionary<TemplateType, int> TemplateRequiredProducts = new()
        {
            { TemplateType.MobileBannerCard, 2 },
            { TemplateType.InteractiveBanner, 3 },
            { TemplateType.FlyingCarpet, 4 },
            { TemplateType.InReadEcommerce, 4 },
            { TemplateType.PostInRead, 4 },
            { TemplateType.CatfishEcom, 3 },
        };
    }
}