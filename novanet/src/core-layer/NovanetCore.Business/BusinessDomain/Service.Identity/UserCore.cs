﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Novanet.Core.Domain;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Identity;

public class UserCore : IdentityUser<Guid>, INovanetDocument
{
    public double Amount { get; set; }

    public long SubId { get; set; }
    
    public bool IsActive { get; set; }

    public UserStatus Status { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }
    
    public string FullName { get; set; } = default!;

    public virtual void ModelCreating<T>(ModelBuilder modelBuilder, string? schema) where T : class, INovanetDocument
    {
        var sequenceName = typeof(T).FullName.CreateMd5("Sequence");
        modelBuilder.HasSequence<int>(sequenceName);
        modelBuilder.Entity<T>().Property(o => o.SubId)
            .HasDefaultValueSql($"NEXT VALUE FOR [{schema}].{sequenceName}");
        modelBuilder.Entity<T>().HasIndex(x => x.SubId);
    }
}
