﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Identity;

public class RoleCore : IdentityRole<Guid>, INovanetDocument
{
    public long SubId { get; set; }

    public virtual void ModelCreating<T>(ModelBuilder modelBuilder, string? schema) where T : class, INovanetDocument
    {
        var sequenceName = typeof(T).FullName.CreateMd5("Sequence");
        modelBuilder.HasSequence<int>(sequenceName);
        modelBuilder.Entity<T>().Property(o => o.SubId)
            .HasDefaultValueSql($"NEXT VALUE FOR [{schema}].{sequenceName}");
        modelBuilder.Entity<T>().HasIndex(x => x.SubId);
    }
}