using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetCategoryCore : NovanetDocument
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid CategoryId { get; set; }
    
    [NotMapped] public CategoryCore? Category { get; set; }
    
    [NotMapped] public AdvertisingSetCore? AdvertisingSetCore { get; set; }
}