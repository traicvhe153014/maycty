﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ZoneTemplateCore : NovanetDocument
{
    public Guid? ZoneId { get; set; }
    public Guid? AdvertisingTemplateId { get; set; }
    [NotMapped] public AdvertisingTemplateCore? AdvertisingTemplateCore { get; set; }
}