﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetProductGroupCore : NovanetDocument
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid? ProductGroupId { get; set; }
    
    public bool IsActive { get; set; }
    
    public AdvertisingSetStatus Status { get; set; } 
}