﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetLocationCore : NovanetDocument
{
    public Guid LocationId { get; set; }

    public Guid? AdvertisingSetId { get; set; }

    [NotMapped] public LocationCore? Location { get; set; }
    
    [NotMapped] public AdvertisingSetCore? AdvertisingSetCore { get; set; }
}