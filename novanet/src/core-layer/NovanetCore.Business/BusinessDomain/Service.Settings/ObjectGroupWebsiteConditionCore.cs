﻿using Novanet.Core.Domain;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ObjectGroupWebsiteConditionCore : NovanetDocument
{
    public Guid? ObjectGroupId { get; set; }

    public ConditionType BeforeConditionType { get; set; }
    
    public ConditionType AfterConditionType { get; set; }
    
    public Guid? BeforeConditionId { get; set; }
    
    public Guid? AfterConditionId { get; set; }

    public MatchingType MatchingType { get; set; } = MatchingType.Url;

    public UrlConditionType ConditionType { get; set; } = UrlConditionType.Contains;

    public string Value { get; set; } = default!;
}