﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductTypeCore : NovanetDocument
{
    public string Name { get; set; } = default!;
}