﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetProhibitedCategoryCore : NovanetDocument
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid ProhibitedCategoryId { get; set; }
    
    [NotMapped] public ProhibitedCategoryCore? ProhibitedCategory { get; set; }
}