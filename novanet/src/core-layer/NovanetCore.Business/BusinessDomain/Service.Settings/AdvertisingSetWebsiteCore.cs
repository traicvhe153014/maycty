﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetWebsiteCore : NovanetDocument
{
    public Guid AdvertisingSetId { get; set; }

    public Guid WebsiteId { get; set; }
    
    public WebsiteCondition WebsiteCondition { get; set; }

    [NotMapped] public WebsiteCore? Website { get; set; }
}

public enum WebsiteCondition
{
    Include = 1,
    Exclude = 2
}
