﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetGenderCore : NovanetDocument
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid GenderId { get; set; }
    
    [NotMapped] public GenderCore? Gender { get; set; }
}