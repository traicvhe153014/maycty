﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class PaymentCore: NovanetDocument
{
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
}