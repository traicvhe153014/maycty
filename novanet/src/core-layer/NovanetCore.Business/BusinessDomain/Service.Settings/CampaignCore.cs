﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class CampaignCore : NovanetDocument
{
    [MaxLength(150)] public string Name { get; set; } = default!;

    public CampaignType CampaignType { get; set; } = CampaignType.Ecommerce;

    public bool IsActive { get; set; }

    public CampaignStatus Status { get; set; }

    public int ShowSpeed { get; set; }

    public double EcommerceAmountSpent { get; set; }

    public double EcommerceDailyBudget { get; set; }

    public Guid? EcommerceProductFeedId { get; set; }

    public DateTimeOffset EcommerceStartDate { get; set; }

    public DateTimeOffset? EcommerceEndDate { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; } = DateTimeOffset.Now;
    
    public DateTimeOffset ModifiedAt { get; set; } = DateTimeOffset.Now;

    public bool? EcommerceScheduled { get; set; }
    
    public bool EcommerceScheduledAllTime { get; set; }

    [NotMapped] public List<CampaignSchedulingCore>? SchedulingCore { get; set; }
    
    [NotMapped] public int OrderedCampaignStatus { get; set; }
}

public enum CampaignType
{
    Ecommerce,
    Display,
    Native
}

public enum CampaignStatus
{
    [Description("1")]
    NotStarted = 0,
    [Description("0")]
    Running,
    [Description("2")]
    Paused,
    [Description("3")]
    Error,
    [Description("4")]
    Finished,
    [Description("5")]
    Unfinished
}