﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductGroupCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public int Amount { get; set; }

    public Guid? ProductFeedId { get; set; }

    public Guid ProductAttributeId { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset ModifiedAt { get; set; } = DateTimeOffset.Now;
}