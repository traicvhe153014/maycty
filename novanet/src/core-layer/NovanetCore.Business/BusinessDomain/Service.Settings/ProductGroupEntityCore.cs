﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductGroupEntityCore : NovanetDocument
{
    public Guid? ProductGroupId { get; set; }

    public Guid? ProductAttributeValueId { get; set; }

    public Guid? ProductId { get; set; }

    public string? ProductAttributeValueHashCode { get; set; }
    
    public bool Deleted { get; set; }
}