﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using NovanetCore.Business.BusinessDomain.Service.Identity;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class WebsiteCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public string? Description { get; set; }

    public Guid PublisherId { get; set; }

    public string Domain { get; set; } = default!;
    
    public string Url { get; set; } = default!;

    public string? Keywords { get; set; }

    public int? AlexaRank { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset ModifiedAt { get; set; }

    [NotMapped] public UserCore? Publisher { get; set; }
    
    [NotMapped] public List<DomainAliasCore>? DomainAlias { get; set; }

    [NotMapped] public List<WebsitePriceCore>? WebsitePriceCores { get; set; }
    
    [NotMapped] public List<WebsiteProhibitedCategoryCore>? WebsiteProhibitedCategoryCores { get; set; }

    public WebsiteStatus Status { get; set; }
    
    public bool Active { get; set; } 
}
public enum WebsiteStatus
{
    Running = 1,
    Disabled = 2
}