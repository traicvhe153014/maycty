﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

[NotMapped]
public class DomainAliasCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public Guid WebsiteId { get; set; }

    [NotMapped] public WebsiteCore WebsiteCore { get; set; } = default!;
}