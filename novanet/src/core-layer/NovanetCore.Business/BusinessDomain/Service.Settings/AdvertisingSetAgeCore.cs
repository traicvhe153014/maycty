﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using NovanetCore.Business.BusinessDomain.Service.Sharing;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetAgeCore : NovanetDocument
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid AgeId { get; set; }
    
    [NotMapped] public AgeCore? Age { get; set; }
}