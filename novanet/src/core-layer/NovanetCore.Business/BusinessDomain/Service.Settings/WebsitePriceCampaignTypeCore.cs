﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class WebsitePriceCampaignTypeCore : NovanetDocument
{
    public Guid? WebsitePriceId { get; set; }

    public CampaignType CampaignType { get; set; }
}