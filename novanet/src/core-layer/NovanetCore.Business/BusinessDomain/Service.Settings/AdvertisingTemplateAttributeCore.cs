﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingTemplateAttributeCore : NovanetDocument
{
    [MaxLength(150)] public string Name { get; set; } = default!;
    
    [MaxLength(150)] public string DisplayedName { get; set; } = default!;
    
    public NovanetDataType DataType { get; set; }
    
    [NotMapped]
    public CampaignType CampaignType { get; set; }
}