﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetCore : NovanetDocument
{
    [MaxLength(150)] public string Name { get; set; } = default!;

    public bool IsActive { get; set; }
    public AdvertisingSetStatus Status { get; set; }

    public Guid AdvertisingSetMappingId { get; set; }
    public Guid? CampaignId { get; set; }

    public bool ApplyAllProductGroups { get; set; }

    public bool HasProhibitedCategories { get; set; }
    
    public bool HasObject { get; set; }
    
    public RemarketingType? RemarketingType { get; set; }

    public int? RemarketingTime { get; set; }
    
    public int? TimeOnDisplay { get; set; }
    
    public int? AdImpressions { get; set; }
    
    public bool? TargetingMarketingByProduct { get; set; }

    public bool? Uniformed { get; set; }

    public double? UniformedPrice { get; set; }

    public UniformedUnit? UniformedUnit { get; set; }

    public bool? ClickedAdvertising { get; set; }
    
    public bool? Viewer { get; set; }
    
    public int? ViewedMax { get; set; }

    public bool? ExcludedWebsite { get; set; }
    
    public bool ScheduledAllTime { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; } = DateTimeOffset.Now;
    
    public DateTimeOffset ModifiedAt { get; set; } = DateTimeOffset.Now;

    [NotMapped] 
    public List<ProductEntityCore>? IncludedProducts { get; set; }

    [NotMapped] 
    public List<ProductEntityCore>? ExcludedProducts { get; set; }
    
    [NotMapped] public CampaignCore? CampaignCore { get; set; }
    
    [NotMapped] public List<AdvertisingSetLocationCore>? LocationCores { get; set; }
    
    [NotMapped] public List<AdvertisingSetSchedulingCore>? SchedulingCore { get; set; }
    
    [NotMapped] public List<ProductGroupCore>? ProductGroupCores { get; set; }
    
    [NotMapped]
    public List<AdvertisingSetWebsiteCore>? AdvertisingSetWebsiteCores { get; set; }
    
    [NotMapped] public List<AdvertisingSetCategoryCore>? CategoryCores { get; set; }
}

public enum AdvertisingSetStatus
{
    [Description("1")]
    NotStarted = 0,
    [Description("0")]
    Running,
    [Description("2")]
    Paused,
    [Description("3")]
    Error,
    [Description("4")]
    Finished,
    [Description("5")]
    Unfinished
}

public enum UniformedUnit
{
    CPC = 0,
    CPM = 1
}

public enum RemarketingType
{
    MoreRemarketing = 1,
    NoMoreRemarketing = 2,
    OnlyRemarketing = 3
}