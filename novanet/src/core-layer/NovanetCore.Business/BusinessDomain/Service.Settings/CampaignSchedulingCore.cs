﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class CampaignSchedulingCore : NovanetDocument
{
    public Guid CampaignId { get; set; }

    public DayOfWeek DayOfWeek { get; set; }

    public string Timer => Timing.ToString("t");

    public DateTimeOffset Timing { get; set; }
    
    [NotMapped] public CampaignCore? CampaignCore { get; set; }

}