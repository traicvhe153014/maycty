﻿using Novanet.Core.Domain;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ObjectGroupCore : NovanetDocument
{
    public string Name { get; set; } = default!;
    
    public bool ConnectionStatus { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; }
    
    public DateTimeOffset ModifiedAt { get; set; }
    
    public bool ApplyAllProducts { get; set; }
    
    public ObjectGroupProductType? ProductType { get; set; }
    
    public string? ProductName { get; set; }
}