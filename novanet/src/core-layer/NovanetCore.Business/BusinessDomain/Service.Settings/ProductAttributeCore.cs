﻿using System.ComponentModel.DataAnnotations;
using Novanet.Core.Domain;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductAttributeCore : NovanetDocument
{
    [MaxLength(150)] public string Name { get; set; } = default!;

    public string? Description { get; set; }

    public bool Required { get; set; }

    public int MaxLength { get; set; }

    public NovanetDataType DataType { get; set; }

    public int Order { get; set; }
}