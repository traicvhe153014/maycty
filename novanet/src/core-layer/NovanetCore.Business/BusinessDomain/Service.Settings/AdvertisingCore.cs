﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public double DailyBudget { get; set; }

    public double AmountSpent { get; set; }
    
    public bool IsActive { get; set; }
    
    public AdvertisingStatus Status { get; set; }

    [NotMapped] public double Ctr { get; set; }

    [NotMapped] public double Reach { get; set; }

    [NotMapped] public double DailyCost { get; set; }

    public Guid? CampaignId { get; set; }
    
    public Guid? AdvertisingSetId { get; set; }

    public Guid Location { get; set; }

    public int MinimumTimeOnSiteRemarketing { get; set; }
    public int? RetargetScore { get; set; }

    public Guid TemplateId { get; set; }

    public string? RedirectLink { get; set; }
    
    public DateTimeOffset CreatedAt { get; set; } = DateTimeOffset.Now;
    
    public DateTimeOffset ModifiedAt { get; set; } = DateTimeOffset.Now;

    [NotMapped] public double Click { get; set; }

    [NotMapped] public CampaignCore? Campaign { get; set; }
    
    [NotMapped] public AdvertisingSetCore? AdvertisingSetCore { get; set; }

    [NotMapped] public List<AdvertisingAppliedTemplateCore> AdvertisingAppliedTemplateCores { get; set; } = default!;
}

public enum AdvertisingStatus
{
    [Description("1")]
    NotStarted = 0,
    [Description("0")]
    Running,
    [Description("2")]
    Paused,
    [Description("3")]
    Error,
    [Description("4")]
    Finished,
    [Description("5")]
    Unfinished
}