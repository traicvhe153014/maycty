﻿using Novanet.Core.Domain;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ObjectGroupConsumerBehaviorCore : NovanetDocument
{
    public Guid? BehaviorGroupId { get; set; }
    public Guid? ObjectGroupId { get; set; }

    public BehaviorType BehaviorType { get; set; }
    
    public Guid? BeforeBehaviorId { get; set; }
    
    public Guid? AfterBehaviorId { get; set; }
    
    public Guid? ExcludeId { get; set; }

    public ObjectType ObjectType { get; set; }

    public int? From { get; set; }

    public int? To { get; set; }

    public TimeUnit? Unit { get; set; }
}