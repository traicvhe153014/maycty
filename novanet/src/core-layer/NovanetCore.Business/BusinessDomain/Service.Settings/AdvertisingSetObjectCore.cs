﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingSetObjectCore : NovanetDocument
{
    public Guid? AdvertisingSetId { get; set; }

    public Guid? ObjectId { get; set; }
}