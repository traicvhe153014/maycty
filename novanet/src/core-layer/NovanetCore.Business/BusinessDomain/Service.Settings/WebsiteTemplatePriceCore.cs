﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class WebsiteTemplatePriceCore : NovanetDocument
{
    public TemplateType TemplateType { get; set; }

    public PriceType BuyPriceType { get; set; }

    public double BuyPrice { get; set; }
    
    public PriceType SellPriceType { get; set; }

    public double SellPrice { get; set; }
    
    public Guid? WebsitePriceId { get; set; }
    
    public Guid? AdvertisingTemplateId { get; set; }
    
    public Guid? ZoneTemplateId { get; set; }
}