﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ZoneCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public int Width { get; set; }

    public int Height { get; set; }
    
    public CampaignType CampaignType { get; set; }

    public bool UsingBackup { get; set; }

    public ZoneBackupType? BackupType { get; set; }
    
    public string? BackupCode { get; set; }

    public string Code { get; set; } = default!;

    public Guid WebsiteId { get; set; }
    
    public string? RedirectLink { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }

    [NotMapped] public WebsiteCore? WebsiteCore { get; set; }

    [NotMapped] public List<ZoneTemplateCore> ZoneTemplateCores { get; set; } = default!;
}

public enum ZoneBackupType
{
    BackupCode = 0,
    BackupFile = 1
}