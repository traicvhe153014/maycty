﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class PriceCore : NovanetDocument
{
    public Guid? WebsiteId { get; set; }

    public DateTimeOffset Timestamp { get; set; }
}