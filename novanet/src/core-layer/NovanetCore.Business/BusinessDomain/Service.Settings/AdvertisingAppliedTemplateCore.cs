﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingAppliedTemplateCore : NovanetDocument
{
    public Guid? AdvertisingId { get; set; }
    public Guid? AdvertisingTemplateId { get; set; }

    [NotMapped]
    public List<AdvertisingTemplateAttributeValueCore>? AdvertisingTemplateAttributeValueCores { get; set; }
}