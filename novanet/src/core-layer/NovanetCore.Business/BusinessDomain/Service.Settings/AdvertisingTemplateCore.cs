﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingTemplateCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public int Width { get; set; }

    public int Height { get; set; }

    public TemplateType TemplateType { get; set; }
    
    public int TimeToDisplay { get; set; }

    public CampaignType CampaignType { get; set; }

    public string TemplatePosition { get; set; } = default!;
}

public enum TemplateType
{
    // w375 x h180
    MobileBannerCard = 1,
    
    // w375 x h810
    InteractiveBanner = 2,
    
    // w375 x h810
    FlyingCarpet = 3,
    
    // w375 x h600
    InReadEcommerce = 4,
    
    // w375 x h600
    PostInRead = 5,
    
    // w375 x h120
    CatfishEcom = 6,

    // w359 x h70
    NotificationBanner = 7,

    // w375 x h120
    CatfishCollabBranding = 8,

    // w375 x h810
    PopupBannerBranding = 9,
    
    // w375 x h120
    CatfishCollabVideo = 10,

    // w375 x h810
    PopupBannerVideo = 11,
    
    // w375 x h120
    CatfishEcomVideo = 12,
}