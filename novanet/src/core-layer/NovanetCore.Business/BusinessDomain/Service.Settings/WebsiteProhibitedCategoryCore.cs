using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class WebsiteProhibitedCategoryCore : NovanetDocument
{
    public Guid ProhibitedCategoryId { get; set; }

    public Guid WebsiteId { get; set; }
    
    [NotMapped] public WebsiteCore? WebsiteCore { get; set; }
}

