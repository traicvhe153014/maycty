﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using Novanet.Core.Models;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class AdvertisingTemplateAttributeValueCore : NovanetDocument, INovanetAttributeValue
{
    public Guid? AdvertisingTemplateAttributeId { get; set; }
    
    public Guid? AdvertisingAppliedTemplateId { get; set; }
    
    public string? StringValue { get; set; }
    
    public DateTimeOffset? DateTimeValue { get; set; }
    
    public long? NumberValue { get; set; }
    
    public bool? BooleanValue { get; set; } 
    
    public double? DoubleValue { get; set; }
    
    [NotMapped]
    public object OutputValue => StringValue + DateTimeValue + NumberValue + BooleanValue + DoubleValue;
}