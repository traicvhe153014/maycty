﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class WebsitePriceCore : NovanetDocument
{
    public Guid? WebsiteId { get; set; }
    
    public Guid? AdvertisingSetId { get; set; }

    public DateTimeOffset Timestamp { get; set; }
    
    public DateTimeOffset StartDate { get; set; }
    
    public DateTimeOffset? EndDate { get; set; }
    
    public TemplateType TemplateType { get; set; }

    public PriceType BuyPriceType { get; set; }

    public double BuyPrice { get; set; }
    
    public PriceType SellPriceType { get; set; }

    public double SellPrice { get; set; }
    
    public Guid? ZoneId { get; set; }

    public PublisherPriceType PublisherPriceType { get; set; }
    
    public WebsitePriceStatus Status { get; set; }
    
    public bool IsActive { get; set; }
    
    public DateTimeOffset? CreatedAt { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }

    [NotMapped] public WebsiteCore? WebsiteCore { get; set; }

    [NotMapped] public ZoneCore? ZoneCore { get; set; }
    
    [NotMapped] public List<WebsiteTemplatePriceCore>? WebsiteTemplatePriceCores { get; set; }
    
    [NotMapped] public List<WebsitePriceCampaignTypeCore>? WebsitePriceCampaignTypeCores { get; set; }
}

public enum PriceType
{
    CPM,
    CPC,
    CPA
}

public enum PublisherPriceType
{
    Publisher = 1,
    Website,
    Zone
}

public enum WebsitePriceStatus
{
    Running = 1,
    Paused,
    NotStarted,
    Archived
}