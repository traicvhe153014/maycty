﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductEntityCore : NovanetDocument
{
    public Guid ProductFeedId { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public bool IsValid { get; set; }
    
    public bool IsActive { get; set; }
    
    public ProductEntityStatus? Status { get; set; }
    
    public DateTimeOffset? ModifiedAt { get; set; }

    [NotMapped] public List<ProductAttributeValueCore>? ProductAttributeValueCores { get; set; }
    
    public bool Deleted { get; set; }
}

public enum ProductEntityStatus
{
    Running,
    OutOfStock,
    Deactivated
}