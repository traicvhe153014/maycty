﻿using System.ComponentModel.DataAnnotations.Schema;
using Novanet.Core.Domain;
using Novanet.Core.Models;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductAttributeValueCore : NovanetDocument, INovanetAttributeValue
{
    public Guid ProductId { get; set; }

    public Guid AttributeId { get; set; }

    public string? StringValue { get; set; }

    public DateTimeOffset? DateTimeValue { get; set; }

    public long? NumberValue { get; set; }

    public bool? BooleanValue { get; set; }

    public double? DoubleValue { get; set; }

    [NotMapped]
    public object OutputValue => StringValue + DateTimeValue + NumberValue + BooleanValue + DoubleValue;

    public string? HashKey { get; set; }
    
    public bool Deleted { get; set; }
}