﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Settings;

public class ProductFeedCore : NovanetDocument
{
    public bool Deleted { get; set; } = false;

    public string Name { get; set; } = default!;

    public SourceType SourceType { get; set; }

    public string SourcePath { get; set; } = default!;

    public ProductFeedPermission Permission { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    // public Guid CreatedBy { get; set; }

    public DateTimeOffset ModifiedAt { get; set; } = DateTimeOffset.Now;
    
    public int ValidProductsCount { get; set; }

    public int InvalidProductsCount { get; set; }
}

public enum ProductFeedPermission
{
    Granted = 1,
    CannotParse = 2,
    Processing = 3,
    CannotAccess = 4,
}

public enum SourceType
{
    GoogleSheet = 1
}