﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class IPBlockCore : NovanetDocument
{
    public long LocationId { get; set; }

    public long Start { get; set; }

    public long End { get; set; }
}