﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class FaqCore : NovanetDocument
{
    public Guid Parent { get; set; }

    public string Name { get; set; } = default!;

    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset ModifiedAt { get; set; }

    public string? Content { get; set; }

    public int Priority { get; set; }
}