﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class AgeCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public int From { get; set; }

    public int To { get; set; }
}