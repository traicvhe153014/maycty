﻿using System.ComponentModel.DataAnnotations;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class CategoryCore : NovanetDocument
{
    [MaxLength(150)] public string Name { get; set; } = default!;
    
    public string Slug { get; set; } = default!;
}