using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class ProhibitedCategoryCore : NovanetDocument
{
    public string Name { get; set; } = default!;
}