﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class GenderCore : NovanetDocument
{
    public string Name { get; set; } = default!;
}