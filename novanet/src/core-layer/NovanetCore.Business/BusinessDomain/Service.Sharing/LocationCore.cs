﻿using System.ComponentModel.DataAnnotations;
using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class LocationCore : NovanetDocument
{
    [StringLength(100)] public string RegionKey { get; set; } = default!;

    public string RegionName { get; set; } = default!;

    public string ProvinceName { get; set; } = default!;
    
    public int ProvinceId { get; set; }

}