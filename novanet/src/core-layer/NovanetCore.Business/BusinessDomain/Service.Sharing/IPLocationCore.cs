﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class IPLocationCore : NovanetDocument
{
    public long LocationId { get; set; }

    public string CountryIsoCode { get; set; } = default!;

    public int? VnRegion { get; set; }

    public int? VnProvinceId { get; set; }

    public int CountryId { get; set; }
}