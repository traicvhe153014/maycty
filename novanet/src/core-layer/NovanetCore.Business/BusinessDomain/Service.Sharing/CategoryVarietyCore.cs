﻿using Novanet.Core.Domain;

namespace NovanetCore.Business.BusinessDomain.Service.Sharing;

public class CategoryVarietyCore : NovanetDocument
{
    public string Name { get; set; } = default!;

    public Guid? CategoryId { get; set; }
}