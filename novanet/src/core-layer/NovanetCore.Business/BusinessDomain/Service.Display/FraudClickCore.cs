﻿using AioCore.Mongo.Driver;

namespace NovanetCore.Business.BusinessDomain.Service.Display;

public class FraudClickCore : MongoDocument
{
    public Guid ClickId { get; set; }
    public long Ip { get; set; }
    public Guid ClientId { get; set; }
    public long ZoneId { get; set; }
    public long WebsiteId { get; set; }
    public long PublisherId { get; set; }
    public long AdvertisingId { get; set; }
    public long AdvertisingSetId { get; set; }
    public long CampaignId { get; set; }
    public long AdvertiserId { get; set; }
    public long AdvertisingTemplateId { get; set; }
    public int AgentId { get; set; }
    public DateTimeOffset ClickedOn { get; set; }
    public string ClientUrl { get; set; } = default!;
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? ViewTime { get; set; }
    public bool? RuleIp { get; set; }
    public bool? RuleClickRate { get; set; }
    public bool? RuleDoubleClick { get; set; }
    public bool? RuleNewClient { get; set; }
    public int? ConfigId { get; set; }
    public long? RunHistoryId { get; set; }
    public bool? RuleRealTime { get; set; }
    public bool? RuleOutOfTimeSpan { get; set; }
    public bool? RuleMaxCtr { get; set; }
    public bool? RulePercentCutNova { get; set; }
    public bool? RuleCountryNotConfig { get; set; }
    public long? CountryId { get; set; }
    public bool? RuleVNProvinceNotConfig { get; set; }
}