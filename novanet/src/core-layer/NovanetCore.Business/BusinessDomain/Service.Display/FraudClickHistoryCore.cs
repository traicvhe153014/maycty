﻿using AioCore.Mongo.Driver;

namespace NovanetCore.Business.BusinessDomain.Service.Display;

public class FraudClickHistoryCore : MongoDocument
{
    public Guid ClickId { get; set; }
}
