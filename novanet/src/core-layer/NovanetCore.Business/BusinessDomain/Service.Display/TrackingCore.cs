﻿using AioCore.Mongo.Driver;
using MongoDB.Bson.Serialization.Attributes;
using Novanet.Core.Enums.ReportKeyObjects;

namespace NovanetCore.Business.BusinessDomain.Service.Display;

[BsonIgnoreExtraElements]
public class TrackingCore : MongoDocument
{
    public long Ip { get; set; }
    public long Location { get; set; }
    public long IpLocation { get; set; }
    public string? UserAgent { get; set; }
    public bool IsEnableCookie { get; set; }
    public bool IsCreateNewClientId { get; set; }
    public string ClientUrl { get; set; } = default!;
    public Guid ClientId { get; set; }
    public string RefererUrl { get; set; } = default!;
    public int HistoryLength { get; set; }
    public long ZoneId { get; set; }
    public long WebsiteId { get; set; }
    public long PublisherId { get; set; }
    public long AdvertisingId { get; set; }
    public long AdvertisingSetId { get; set; }
    public long CampaignId { get; set; }
    public long AdvertisingTemplateId { get; set; }
    public long AdvertiserId { get; set; }
    public List<long>? AdvertisingSetProductGroupIds { get; set; }
    public Dictionary<string, string> Headers { get; set; } = default!;
    public string Domain { get; set; } = default!;
    public string SessionId { get; set; } = default!;
    public int SessionTimeOut { get; set; }
    public bool IsNewSession { get; set; }
    public double SellPrice { get; set; }
    public double BuyPrice { get; set; }
    public double Price { get; set; }
    public string MobileInfo { get; set; } = default!;
    public bool IsPromotion { get; set; }
    public int VnProvinceId { get; set; }
    public string? TargetUrl { get; set; }
    public bool IsFraud { get; set; }
    public bool IsRemarketing { get; set; }
    public int NumberOfImpression { get; set; }
    public DateTimeOffset CreatedOn { get; set; }
    public DateTimeOffset ViewTime { get; set; } = default!;
    public bool IsDefault { get; set; }
    public bool IsBackup { get; set; }
    public bool IsEnableFlash { get; set; }
    public List<long> CampaignIds { get; set; } = default!;
    public long CountryId { get; set; }
    public long ConversionId { get; set; }
    public bool IsCrawlerBot { get; set; }
    public TrackingType TrackingType { get; set; }
    public DateTimeOffset ClientCreatedOn { get; set; }
    public long ProductId { get; set; }
    public long ProductGroupId { get; set; }
    public long ProductFeedId { get; set; }
    public List<long>? ProductIds { get; set; }
    public bool IsProcessed { get; set; }
    public long CreatedFromMinute { get; set; }
    public ReportMetric? VideoViewMetric { get; set; }

    public List<long> GetAdvertiserValues(ReportAdvertiserDimension dimension)
    {
        switch (dimension)
        {
            case ReportAdvertiserDimension.Advertiser:
                return new List<long> { AdvertiserId };
            case ReportAdvertiserDimension.Advertising:
                return new List<long> { AdvertisingId };
            case ReportAdvertiserDimension.Campaign:
                return new List<long> { CampaignId };
            case ReportAdvertiserDimension.Product:
                return ProductIds ?? new List<long>();
            case ReportAdvertiserDimension.AdvertisingSet:
                return new List<long> { AdvertisingSetId };
            case ReportAdvertiserDimension.AdvertisingTemplate:
                return new List<long> { AdvertisingTemplateId };
            case ReportAdvertiserDimension.ProductFeed:
                return new List<long> { ProductFeedId };
            case ReportAdvertiserDimension.AdvertisingSetProductGroup:
                return AdvertisingSetProductGroupIds ?? new List<long>();
            default:
                return new List<long>();
        }
    }
}

public enum TrackingType
{
    Impression = 1,
    Click = 2,
    Conversion = 3,
    VideoView = 4
}