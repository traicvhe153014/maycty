﻿using AioCore.Mongo.Driver;

namespace NovanetCore.Business.BusinessDomain.Service.Display;

public class TrackingErrorCore : MongoDocument
{
    public string ErrorMessage { get; set; } = default!;
    public TrackingType TrackingType { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public Dictionary<string, string> Headers { get; set; } = default!;
    public string Domain { get; set; } = default!;
    public long ZoneId { get; set; }
    public long WebsiteId { get; set; }
    public long PublisherId { get; set; }
    public long AdvertisingId { get; set; }
    public long AdvertisingSetId { get; set; }
    public long CampaignId { get; set; }
    public long AdvertisingTemplateId { get; set; }
    public long AdvertiserId { get; set; }
    public Guid ClientId { get; set; }
    public string? UserAgent { get; set; }
}