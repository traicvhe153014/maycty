﻿using AioCore.Mongo.Driver;
using Novanet.Core.Enums;

namespace NovanetCore.Business.BusinessDomain.Service.Display;

public class EventCore : MongoDocument
{
    public Guid ClientId { get; set; }
    
    public EventType EventType { get; set; }
    
    public DateTimeOffset? EventTime { get; set; }

    public string EventTarget { get; set; } = default!;

    public string? CartId { get; set; }
    
    public long AccountId { get; set; }
    
    public List<EventProductDetail>? ProductDetails { get; set; }
    
    public EventPurchaseTransaction? PurchaseTransaction { get; set; }
}

public class EventProductDetail
{
    public EventProduct? Product { get; set; }
    public int Quantity { get; set; }
}

public class EventProduct
{
    public string? Id { get; set; }
    public string? ProductName { get; set; }
    public string? ProductGroup { get; set; }

}

public class EventPurchaseTransaction
{
    public double Revenue { get; set; }
    public string? CurrencyCode { get; set; }
}