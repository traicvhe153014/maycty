﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Novanet.Core.RedisCache;
using Novanet.EventBus;
using NovanetCore.Business.BusinessManager;

namespace NovanetCore.Business.BusinessEvents;

public class UpdateObjectGroupConditionsService : EventBusListenerService<UpdateObjectGroupConditionsConfig>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateObjectGroupConditions;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<UpdateObjectGroupConditionsService> _logger;
    private readonly CacheManagerService _cacheManagerService;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public UpdateObjectGroupConditionsService(IServiceProvider serviceProvider,
        ILogger<UpdateObjectGroupConditionsService> logger, CacheManagerService cacheManagerService, IServiceScopeFactory serviceScopeFactory) : base(
        serviceProvider)
    {
        _logger = logger;
        _cacheManagerService = cacheManagerService;
        _serviceScopeFactory = serviceScopeFactory;
        _isProcessing = false;
    }

    protected override async Task Processing(UpdateObjectGroupConditionsConfig signal)
    {
        _isProcessing = true;
        _logger.LogInformation("{Name} started", nameof(UpdateObjectGroupConditionsService));
        try
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var globalCacheService = scope.ServiceProvider.GetRequiredService<IGlobalCacheService>();
            var objectGroup = await globalCacheService.GetAsync<ObjectGroupCore>(
                RedisKeys.KeySettings<ObjectGroupCore>(signal.ObjectGroupSubId));
            CacheManager.ObjectGroups[signal.ObjectGroupSubId] = objectGroup;
            
            foreach (var websiteConditionSubId in signal.ObjectGroupWebsiteConditionSubIds)
            {
                var websiteCondition = await globalCacheService.GetAsync<ObjectGroupWebsiteConditionCore>(
                    RedisKeys.KeySettings<ObjectGroupWebsiteConditionCore>(websiteConditionSubId));
                CacheManager.ObjectGroupWebsiteConditions[websiteConditionSubId] = websiteCondition;
            }
            
            foreach (var consumerBehaviorSubId in signal.ObjectGroupConsumerBehaviorSubIds)
            {
                var consumerBehavior = await globalCacheService.GetAsync<ObjectGroupConsumerBehaviorCore>(
                    RedisKeys.KeySettings<ObjectGroupConsumerBehaviorCore>(consumerBehaviorSubId));
                CacheManager.ObjectGroupConsumerBehaviors[consumerBehaviorSubId] = consumerBehavior;
            }

            _cacheManagerService.LoadObjectBehaviorWithProductItems();
            _cacheManagerService.LoadObjectWebsiteConditions();
        }
        catch (Exception e)
        {
            _logger.LogError(e.ToString());
        }
        finally
        {
            _logger.LogInformation("{Name} finished", nameof(UpdateObjectGroupConditionsService));
            _isProcessing = false;
        }

        await Task.CompletedTask;
    }
}

public class UpdateObjectGroupConditionsConfig
{
    public long ObjectGroupSubId { get; set; }
    public List<long> ObjectGroupWebsiteConditionSubIds { get; set; } = default!;
    public List<long> ObjectGroupConsumerBehaviorSubIds { get; set; } = default!;
}
