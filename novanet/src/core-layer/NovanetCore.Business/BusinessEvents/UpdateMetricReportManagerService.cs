﻿using System.Diagnostics;
using Microsoft.Extensions.Logging;
using Novanet.EventBus;

namespace NovanetCore.Business.BusinessEvents;

public class UpdateMetricReportManagerService : EventBusListenerService<object>
{
    private readonly ILogger<UpdateMetricReportManagerService> _logger;
    private readonly MetricReportManagerService _metricReportManagerService;
    private bool _isProcessing;

    public UpdateMetricReportManagerService(IServiceProvider serviceProvider,
        ILogger<UpdateMetricReportManagerService> logger, MetricReportManagerService metricReportManagerService) : base(
        serviceProvider)
    {
        _logger = logger;
        _metricReportManagerService = metricReportManagerService;
        _isProcessing = false;
    }

    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateMetricReportManager;
    protected override bool IsWorkQueue => false;

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        _logger.LogInformation("{Name} started", nameof(UpdateMetricReportManagerService));

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        try
        {
            await _metricReportManagerService.Processing();
        }
        catch (Exception e)
        {
            _logger.LogError(e.ToString());
        }
        finally
        {
            _logger.LogInformation("{Name} finished. Elapsed time: {Time}", nameof(UpdateMetricReportManagerService),
                stopwatch.ElapsedMilliseconds);
            _isProcessing = false;
        }
    }
}