﻿using Microsoft.Extensions.Logging;
using Novanet.EventBus;

namespace NovanetCore.Business.BusinessEvents;

public class UpdateCacheManagerService : EventBusListenerService<object>
{
    private bool _isProcessing;
    protected override bool Disabled => _isProcessing;
    protected override string Channel => MessageBusChannels.UpdateCacheManager;
    protected override bool IsWorkQueue => false;

    private readonly ILogger<UpdateCacheManagerService> _logger;
    private readonly CacheManagerService _cacheManagerService;

    public UpdateCacheManagerService(IServiceProvider serviceProvider,
        ILogger<UpdateCacheManagerService> logger, CacheManagerService cacheManagerService) : base(
        serviceProvider)
    {
        _logger = logger;
        _cacheManagerService = cacheManagerService;
        _isProcessing = false;
    }

    protected override async Task Processing(object signal)
    {
        _isProcessing = true;
        try
        {
            await _cacheManagerService.Processing();
        }
        catch (Exception e)
        {
            _logger.LogError("{Time} Error: {Error}", DateTimeOffset.Now, e.Message.ToString());
        }
        finally
        {
            _isProcessing = false;
        }
    }
}