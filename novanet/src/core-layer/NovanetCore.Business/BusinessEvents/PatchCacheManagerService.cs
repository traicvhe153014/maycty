﻿using Microsoft.Extensions.Logging;
using Novanet.Core.Domain;
using Novanet.EventBus;
using NovanetCore.Business.BusinessManager;

namespace NovanetCore.Business.BusinessEvents;

public class PatchCacheManagerService<T> : EventBusListenerService<PatchCacheManagerSignal<T>>
    where T : INovanetDocument
{
    private readonly ILogger<PatchCacheManagerService<T>> _logger;

    public PatchCacheManagerService(IServiceProvider serviceProvider, ILogger<PatchCacheManagerService<T>> logger) :
        base(serviceProvider)
    {
        _logger = logger;
    }

    protected override string Channel => MessageBusChannels.PatchCacheManager<T>();
    protected override bool IsWorkQueue => false;

    protected override async Task Processing(PatchCacheManagerSignal<T> signal)
    {
        _logger.LogInformation("{Name} started. Entity: {EntityName}.{Id}", nameof(PatchCacheManagerSignal<T>),
            typeof(T).Name, signal.EntitySubId);
        var cacheSet = CacheManager.Set<T>(_logger);
        if (cacheSet is null)
        {
            return;
        }

        if (signal.Entity is null)
        {
            cacheSet.TryRemove(signal.EntitySubId, out _);
        }
        else
        {
            cacheSet[signal.EntitySubId] = signal.Entity;
        }

        _logger.LogInformation("{Name} Finished. Entity: {EntityName}.{Id}", nameof(PatchCacheManagerSignal<T>),
            typeof(T).Name, signal.EntitySubId);
        await Task.CompletedTask;
    }
}

public class PatchCacheManagerSignal<T> where T : INovanetDocument
{
    public long EntitySubId { get; set; }
    public T? Entity { get; set; }
}