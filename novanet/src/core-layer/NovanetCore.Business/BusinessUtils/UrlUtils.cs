﻿using System.Web;

namespace NovanetCore.Business.BusinessUtils;

public static class UrlUtils
{
    public static string GetDomainByUrl(string url)
    {
        try
        {
            var urlTmp = HttpUtility.UrlDecode(url);
            var uri = new Uri(urlTmp);
            return uri.Host;
        }
        catch
        {
            return string.Empty;
        }
    }
}