﻿using Novanet.Core.Enums.ReportKeyObjects;
using NovanetCore.Business.BusinessDomain.Service.Display;

namespace NovanetCore.Business.BusinessUtils;

public static class ConvertUtils
{
    public static int ConvertCtrScore(double ctr)
    {
        return ctr switch
        {
            < 0.00015 => 1,
            < 0.00018 => 2,
            < 0.00021 => 3,
            < 0.00024 => 4,
            < 0.00027 => 5,
            < 0.0003 => 6,
            < 0.00035 => 7,
            < 0.0005 => 8,
            _ => ctr < 0.0007 ? 9 : 10
        };
    }
    
    public static long ConvertIp(string ip)
    {
        try
        {
            var ipList = ip.Split('.');
            var ipNumber = Convert.ToInt64(ipList[0]) * 16777216 + Convert.ToInt64(ipList[1]) * 65536 + Convert.ToInt64(ipList[2]) * 256 + Convert.ToInt64(ipList[3]);
            return ipNumber;
        }
        catch (Exception)
        {
            return 0;
        }
    }
    
    public static string ConvertIp(long ip)
    {
        try
        {
            return $"{ip % 256}.{ip / 256 % 256}.{ip / 256 / 256 % 256}.{ip / 256 / 256 / 256}";
        }
        catch (Exception)
        {
            return "0.0.0.0";
        }
    }
    
    public static TrackingType? GetTrackingType(ReportMetric metric)
    {
        return metric switch
        {
            ReportMetric.Click => TrackingType.Click,
            ReportMetric.View => TrackingType.Impression,
            ReportMetric.Conversion => TrackingType.Conversion,
            _ => null
        };
    }
    
    public static ReportMetric? GetMetric(TrackingType trackingType)
    {
        return trackingType switch
        {
            TrackingType.Click => ReportMetric.Click,
            TrackingType.Impression => ReportMetric.View,
            TrackingType.Conversion => ReportMetric.Conversion,
            _ => null
        };
    }
}