﻿using Novanet.Core.Constants;

namespace NovanetCore.Business.BusinessUtils;

public static class MathUtils
{
    public static double GetRandomNumber(double minimum, double maximum)
    {
        var random = new Random();
        return random.NextDouble() * (maximum - minimum) + minimum;
    }
    
    public static List<string> GetAllPossibleCombos(List<List<object>> input, char separator = '\0')
    {
        IEnumerable<string> combos = new [] { "" };

        foreach (var inner in input)
            combos = from c in combos
                from i in inner
                select c + separator + i;

        return combos.Select(x => x.Trim(separator)).ToList();
    }

    
    public static IEnumerable<IEnumerable<T>> GetPermutations<T>(this IEnumerable<IEnumerable<T>> lists)
    {
        IEnumerable<IEnumerable<T>> result = new List<IEnumerable<T>> { new List<T>() };
        return lists.Aggregate(result,
            (current, list) => current.SelectMany(o => list.Select(s => o.Union(new[] {s}))));
    }
    
    public static double CalculateMetric(MetricExpressionType type, double first, double second)
    {
        return type switch
        {
            MetricExpressionType.Add => first + second,
            MetricExpressionType.Subtract => first - second,
            MetricExpressionType.Multiple => first * second,
            MetricExpressionType.Divide => second != 0 ? first / second : 0,
            MetricExpressionType.Equal => first,
            _ => default
        };
    }
}