export {};

type GroupByFn<T> = (t: T) => string | number | symbol;

declare global {
  interface Array<T> {
    groupBy(key: keyof T | GroupByFn<T>): { [key: string]: Array<T> };
  }
}

Array.prototype.groupBy = function (key) {
  return this.reduce((rv, x) => {
    const groupedKey = typeof key === 'function' ? key(x) : x[key];
    (rv[groupedKey] = rv[groupedKey] || []).push(x);
    return rv;
  }, {});
};
