export interface IValidationParams {
  required?: boolean;
  maxLength?: number;
  minLength?: number;
  pattern?: RegExp | string;
  maxRange?: number;
  minRange?: number;
  max?: number;
  min?: number;
}

export interface IFormField {
  name: string;
  validationParams?: IValidationParams;
}

export interface IFormFields {
  [formLabel: string]: IFormField;
}

export interface IFormFieldError {
  type: string;
  message: string;
  params?: any;
}

export interface IFormFieldErrors {
  [key: string]: IFormFieldError[];
}
