export interface ISuccessHttpResponse<T = any> {
  success: boolean;
  status: number;
  errors: string[];
  data: T;
}
