export interface IUser {
  aud: string;
  fullname: string;
  email: string;
  exp: number;
  iat: number;
  id: string;
  iss: string;
  nbf: number;
  policies: string;
  roles: string;
  username: string;
}

export class User {
  aud: string;
  fullname: string;
  email: string;
  exp: number;
  iat: number;
  id: string;
  iss: string;
  nbf: number;
  policies: number[];
  roles: string;
  username: string;

  constructor(
    aud?: string,
    fullname?: string,
    email?: string,
    exp?: number,
    iat?: number,
    id?: string,
    iss?: string,
    nbf?: number,
    policies?: string,
    roles?: string,
    username?: string
  ) {
    this.aud = aud;
    this.fullname = fullname;
    this.email = email;
    this.exp = exp;
    this.iat = iat;
    this.id = id;
    this.iss = iss;
    this.nbf = nbf;
    this.policies = policies?.split(',').map((x) => parseInt(x));
    this.roles = roles;
    this.username = username;
  }
}
