import {
  IAge,
  ICategory,
  IGender,
  ILocation,
  IProhibitedCategory,
} from './sharing-state.model';

const enum SharingActions {
  ListProhibitedCategory = '[Sharing] List Prohibited Category',
  ListProhibitedCategorySuccess = '[Sharing] List Prohibited Category Success',
  ListProhibitedCategoryError = '[Sharing] List Prohibited Category Error',
  ListAge = '[Sharing] List Age',
  ListAgeSuccess = '[Sharing] List Age Success',
  ListAgeError = '[Sharing] List Age Error',
  ListGender = '[Sharing] List Gender',
  ListGenderSuccess = '[Sharing] List Gender Success',
  ListGenderError = '[Sharing] List Gender Error',
  ListLocation = '[Sharing] List Location',
  ListLocationSuccess = '[Sharing] List Location Success',
  ListLocationError = '[Sharing] List Location Error',
  ListCategory = '[Sharing] List Category',
  ListCategorySuccess = '[Sharing] List Category Success',
  ListCategoryError = '[Sharing] List Category Error',
}

export class ListProhibitedCategory {
  public static readonly type = SharingActions.ListProhibitedCategory;

  constructor() {}
}

export class ListProhibitedCategorySuccess {
  public static readonly type = SharingActions.ListProhibitedCategorySuccess;

  constructor(public response: IProhibitedCategory[]) {}
}

export class ListProhibitedCategoryError {
  public static readonly type = SharingActions.ListProhibitedCategoryError;

  constructor(public error: any) {}
}

export class ListAge {
  public static readonly type = SharingActions.ListAge;

  constructor() {}
}

export class ListAgeSuccess {
  public static readonly type = SharingActions.ListAgeSuccess;

  constructor(public response: IAge[]) {}
}

export class ListAgeError {
  public static readonly type = SharingActions.ListAgeError;

  constructor(public error: any) {}
}

export class ListGender {
  public static readonly type = SharingActions.ListGender;

  constructor() {}
}

export class ListGenderSuccess {
  public static readonly type = SharingActions.ListGenderSuccess;

  constructor(public response: IGender[]) {}
}

export class ListGenderError {
  public static readonly type = SharingActions.ListGenderError;

  constructor(public error: any) {}
}

export class ListLocation {
  public static readonly type = SharingActions.ListLocation;

  constructor() {}
}

export class ListLocationSuccess {
  public static readonly type = SharingActions.ListLocationSuccess;

  constructor(public response: ILocation[]) {}
}

export class ListLocationError {
  public static readonly type = SharingActions.ListProhibitedCategoryError;

  constructor(public error: any) {}
}

export class ListCategory {
  public static readonly type = SharingActions.ListCategory;

  constructor() {}
}

export class ListCategorySuccess {
  public static readonly type = SharingActions.ListCategorySuccess;

  constructor(public response: ICategory[]) {}
}

export class ListCategoryError {
  public static readonly type = SharingActions.ListCategoryError;

  constructor(public error: any) {}
}
