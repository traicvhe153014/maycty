import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  IAge,
  ICategory,
  IGender,
  ILocation,
  IProhibitedCategory,
} from './sharing-state.model';

const SharingUrls = {
  listProhibitedCategories: `ProhibitedCategories`,
  listGenders: `Genders`,
  listAges: `Ages`,
  listLocations: `Locations`,
  listCategories: `Categories`,
};

@Injectable()
export class SharingService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'sharing/api/v1');
  }

  public listProhibitedCategories(): Observable<IProhibitedCategory[]> {
    return this.httpClient
      .get<ISuccessHttpResponse<IProhibitedCategory[]>>(
        this.createUrl(SharingUrls.listProhibitedCategories)
      )
      .pipe(map((response) => response.data));
  }

  public listAges(): Observable<IAge[]> {
    return this.httpClient
      .get<ISuccessHttpResponse<IAge[]>>(this.createUrl(SharingUrls.listAges))
      .pipe(map((response) => response.data));
  }

  public listGenders(): Observable<IGender[]> {
    return this.httpClient
      .get<ISuccessHttpResponse<IGender[]>>(
        this.createUrl(SharingUrls.listGenders)
      )
      .pipe(map((response) => response.data));
  }

  public listLocations(): Observable<ILocation[]> {
    return this.httpClient
      .get<ISuccessHttpResponse<ILocation[]>>(
        this.createUrl(SharingUrls.listLocations)
      )
      .pipe(map((response) => response.data));
  }

  public listCategories(): Observable<ICategory[]> {
    return this.httpClient
      .get<ISuccessHttpResponse<ICategory[]>>(
        this.createUrl(SharingUrls.listCategories)
      )
      .pipe(map((response) => response.data));
  }
}
