export const redirectLinkPattern =
  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

export const urlPattern =
  /^(?:(?:https?):\/\/|www\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])$/im;

export const passwordPattern = /(?!^[^a-z]+$)^\S+$/;

export const fullNamePattern =
  /^[ ]*[a-zA-Z\u00C0-\u024F\u1E00-\u1EFF0-9$&+,:;=~?@#<>\/|_().%*!"'-]+(?: [a-zA-Z\u00C0-\u024F\u1E00-\u1EFF0-9$&+,:;=~?@#<>\/|_().%*!"'-]+)*[ ]*$/;

export const urlBackUpPattern = /(https:\/\/)|(http:\/\/)|^$/;

export const domainPattern =
  /^(?:(?!https?:\/\/)|www\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])$/im;

export const logoAllowedExtensionPattern = /(?:.jpg|.jpeg|.png(?!\\))+$/;

export const bannerAllowedExtensionPattern =
  /(?:.jpg|.jpeg|.png|.html|.gif|.zip(?!\\))+$/;
