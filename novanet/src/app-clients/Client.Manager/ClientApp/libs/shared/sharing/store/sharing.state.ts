import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';

import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {
  IAge,
  ICategory,
  IGender,
  ILocation,
  IProhibitedCategory,
  ISharingStateModel,
} from './sharing-state.model';
import { SharingService } from './sharing-state.service';
import {
  ListAge,
  ListAgeError,
  ListAgeSuccess,
  ListCategory,
  ListCategoryError,
  ListCategorySuccess,
  ListGender,
  ListGenderError,
  ListGenderSuccess,
  ListLocation,
  ListLocationError,
  ListLocationSuccess,
  ListProhibitedCategory,
  ListProhibitedCategoryError,
  ListProhibitedCategorySuccess,
} from './sharing-state.action';

@Injectable()
@State<ISharingStateModel>({
  name: 'sharing',
  defaults: {
    prohibitedCategories: [],
    ages: [],
    genders: [],
    locations: [],
    categories: [],
    loading: {
      prohibitedCategory: false,
      age: false,
      gender: false,
      location: false,
      category: false,
    },
  },
})
export class SharingState {
  constructor(private sharingService: SharingService) {}

  /**
   * Selectors
   */
  @Selector()
  public static getProhibitedCategories(
    state: ISharingStateModel
  ): IProhibitedCategory[] {
    return state.prohibitedCategories;
  }

  @Selector()
  public static getAges(state: ISharingStateModel): IAge[] {
    return state.ages;
  }

  @Selector()
  public static getGenders(state: ISharingStateModel): IGender[] {
    return state.genders;
  }

  @Selector()
  public static getLocations(state: ISharingStateModel): ILocation[] {
    return state.locations;
  }

  @Selector()
  public static getCategorys(state: ISharingStateModel): ICategory[] {
    return state.categories;
  }

  /**
   * Actions
   */
  @Action(ListProhibitedCategory)
  public listProhibitedCategory(ctx: StateContext<ISharingStateModel>) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        prohibitedCategory: true,
      },
    });
    return this.sharingService.listProhibitedCategories().pipe(
      map((response) =>
        ctx.dispatch(new ListProhibitedCategorySuccess(response))
      ),
      catchError((error) =>
        ctx.dispatch(new ListProhibitedCategoryError(error))
      )
    );
  }

  @Action(ListProhibitedCategorySuccess)
  public listProhibitedCategorySuccess(
    ctx: StateContext<ISharingStateModel>,
    { response }: ListProhibitedCategorySuccess
  ) {
    ctx.patchState({
      prohibitedCategories: response,
      loading: {
        ...ctx.getState().loading,
        prohibitedCategory: false,
      },
    });
  }

  @Action(ListProhibitedCategoryError)
  public listProhibitedCategoryError(
    ctx: StateContext<ISharingStateModel>,
    { error }: ListProhibitedCategoryError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        prohibitedCategory: false,
      },
    });
    return throwError(error);
  }

  @Action(ListGender)
  public listGender(ctx: StateContext<ISharingStateModel>) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        gender: true,
      },
    });
    return this.sharingService.listGenders().pipe(
      map((response) => ctx.dispatch(new ListGenderSuccess(response))),
      catchError((error) => ctx.dispatch(new ListGenderError(error)))
    );
  }

  @Action(ListGenderSuccess)
  public listGenderSuccess(
    ctx: StateContext<ISharingStateModel>,
    { response }: ListGenderSuccess
  ) {
    ctx.patchState({
      genders: response,
      loading: {
        ...ctx.getState().loading,
        gender: false,
      },
    });
  }

  @Action(ListGenderError)
  public listGenderError(
    ctx: StateContext<ISharingStateModel>,
    { error }: ListGenderError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        gender: false,
      },
    });
    return throwError(error);
  }

  @Action(ListAge)
  public listAge(ctx: StateContext<ISharingStateModel>) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        age: true,
      },
    });
    return this.sharingService.listAges().pipe(
      map((response) => ctx.dispatch(new ListAgeSuccess(response))),
      catchError((error) => ctx.dispatch(new ListAgeError(error)))
    );
  }

  @Action(ListAgeSuccess)
  public listAgeSuccess(
    ctx: StateContext<ISharingStateModel>,
    { response }: ListAgeSuccess
  ) {
    ctx.patchState({
      ages: response,
      loading: {
        ...ctx.getState().loading,
        age: false,
      },
    });
  }

  @Action(ListAgeError)
  public listAgeError(
    ctx: StateContext<ISharingStateModel>,
    { error }: ListAgeError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        age: false,
      },
    });
    return throwError(error);
  }

  @Action(ListLocation)
  public listLocation(ctx: StateContext<ISharingStateModel>) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        location: true,
      },
    });
    return this.sharingService.listLocations().pipe(
      map((response) => ctx.dispatch(new ListLocationSuccess(response))),
      catchError((error) => ctx.dispatch(new ListLocationError(error)))
    );
  }

  @Action(ListLocationSuccess)
  public listLocationSuccess(
    ctx: StateContext<ISharingStateModel>,
    { response }: ListLocationSuccess
  ) {
    ctx.patchState({
      locations: response,
      loading: {
        ...ctx.getState().loading,
        location: false,
      },
    });
  }

  @Action(ListLocationError)
  public listLocationError(
    ctx: StateContext<ISharingStateModel>,
    { error }: ListLocationError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        location: false,
      },
    });
    return throwError(error);
  }

  @Action(ListCategory)
  public listCategory(ctx: StateContext<ISharingStateModel>) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        category: true,
      },
    });
    return this.sharingService.listCategories().pipe(
      map((response) => ctx.dispatch(new ListCategorySuccess(response))),
      catchError((error) => ctx.dispatch(new ListCategoryError(error)))
    );
  }

  @Action(ListCategorySuccess)
  public listCategorySuccess(
    ctx: StateContext<ISharingStateModel>,
    { response }: ListCategorySuccess
  ) {
    ctx.patchState({
      categories: response,
      loading: {
        ...ctx.getState().loading,
        category: false,
      },
    });
  }

  @Action(ListCategoryError)
  public listCategoryError(
    ctx: StateContext<ISharingStateModel>,
    { error }: ListCategoryError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        category: false,
      },
    });
    return throwError(error);
  }
}
