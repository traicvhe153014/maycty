import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { SharingService } from './sharing-state.service';
import { SharingState } from './sharing.state';

@NgModule({
  imports: [NgxsModule.forFeature([SharingState])],
  providers: [SharingService],
})
export class SharingStateModule {}
