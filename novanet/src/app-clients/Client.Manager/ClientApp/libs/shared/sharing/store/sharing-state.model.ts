export interface IProhibitedCategory {
  id: string;
  name: string;
}

export interface IAge {
  id: string;
  name: string;
  from: number;
  to: number;
}

export interface IGender {
  id: string;
  name: string;
}

export interface ILocation {
  id: string;
  regionKey: string;
  regionName: string;
  provinceName: string;
}

export interface ICategory {
  id: string;
  name: string;
}

export interface ISharingStateModel {
  prohibitedCategories: IProhibitedCategory[];
  ages: IAge[];
  genders: IGender[];
  locations: ILocation[];
  categories: ICategory[];
  loading: {
    prohibitedCategory: boolean;
    age: boolean;
    gender: boolean;
    location: boolean;
    category: boolean;
  };
}
