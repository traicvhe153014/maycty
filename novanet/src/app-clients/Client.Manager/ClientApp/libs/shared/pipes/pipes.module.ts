import { NgModule } from '@angular/core';
import { SafePipe } from './safe-html.pipe';

@NgModule({
  exports: [SafePipe],
  declarations: [SafePipe],
})
export class PipesModule {}
