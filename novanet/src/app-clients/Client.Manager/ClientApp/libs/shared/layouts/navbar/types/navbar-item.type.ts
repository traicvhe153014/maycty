export interface NavbarItem {
  id: string;
  title: string;
  iconName?: string;
  iconType?: 'stroke' | 'fill' | 'font-awesome';
  subItems?: NavbarItem[];
  isExpand?: boolean;
  link?: string;
  urlRoot?: string;
  policy: string | string[];
}
