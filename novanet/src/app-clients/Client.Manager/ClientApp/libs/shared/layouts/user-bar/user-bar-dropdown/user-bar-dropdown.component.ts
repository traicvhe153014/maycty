import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input } from '@angular/core';
import { User } from '@models';
import { AuthService, Logout } from '@features/auth/store';
import { Select, Store } from '@ngxs/store';
import { IdentityState, IUserResponse } from '@shared/identity';
import { Observable } from 'rxjs';
import { StorageService } from '@core/services';
import { session } from '@components';

@Component({
  selector: 'novanet-user-bar-dropdown',
  templateUrl: './user-bar-dropdown.component.html',
  styleUrls: ['./user-bar-dropdown.component.scss'],
  animations: [
    trigger('showHide', [
      state(
        'show',
        style({
          opacity: 1,
          transform: 'translateY(0px)',
        })
      ),
      state(
        'hidden',
        style({
          opacity: 0,
          transform: 'translateY(-30px)',
        })
      ),
      transition('show => hidden', [animate('300ms ease-in-out')]),
      transition('hidden => show', [animate('300ms ease-in-out')]),
    ]),
  ],
})
export class UserBarDropdownComponent {
  @Input() dropdownDisplayed: boolean;
  public user: User;

  @Select(IdentityState.getCurrentUser)
  public currentUser$: Observable<IUserResponse>;

  constructor(
    private readonly store: Store,
    private readonly authService: AuthService,
    private readonly storageService: StorageService
  ) {
    this.user = authService.getUserData();
  }

  get avatar() {
    return this.storageService.avatar(100, this.user.id);
  }

  public onLogout() {
    this.store.dispatch(new Logout());
    session.clear();
  }
}
