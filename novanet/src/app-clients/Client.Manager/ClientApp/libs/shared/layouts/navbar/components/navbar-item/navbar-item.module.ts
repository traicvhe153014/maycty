import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarItemComponent } from './navbar-item.component';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { RouterModule } from '@angular/router';
import { SvgIconModule } from '@core/components/svg-icon';
import { CheckPolicyModule } from '@shared/layouts/navbar/pipes';

@NgModule({
  declarations: [NavbarItemComponent],
  imports: [
    CommonModule,
    SvgIconModule,
    NzDropDownModule,
    RouterModule,
    CheckPolicyModule,
  ],
  exports: [NavbarItemComponent],
})
export class NavbarItemModule {}
