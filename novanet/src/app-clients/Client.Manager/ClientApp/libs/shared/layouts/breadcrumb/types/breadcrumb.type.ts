export interface BreadcrumbType {
  path: string;
  breadcrumbs: Breadcrumb[];
}

export interface Breadcrumb {
  path?: string;
  useIcon?: boolean;
  title?: string;
  iconName?: string;
}
