import { NgModule } from '@angular/core';
import { ContainerComponent } from './container.component';
import { ContainerRoutingModule } from '@shared/layouts/container/container.routing';
import { CommonModule } from '@angular/common';
import { HeaderModule, NavbarModule } from '@shared/layouts';
import { PageTitleModule } from '@shared/layouts/page-title';

const MODULES = [
  CommonModule,
  NavbarModule,
  PageTitleModule,
  HeaderModule,
  ContainerRoutingModule,
];

@NgModule({
  declarations: [ContainerComponent],
  imports: [...MODULES],
  exports: [ContainerComponent],
})
export class ContainerModule {}
