import { NgModule } from '@angular/core';
import { CheckPolicyPipe } from '@shared/layouts/navbar/pipes/check-policy.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [CheckPolicyPipe],
  imports: [CommonModule],
  exports: [CheckPolicyPipe],
})
export class CheckPolicyModule {}
