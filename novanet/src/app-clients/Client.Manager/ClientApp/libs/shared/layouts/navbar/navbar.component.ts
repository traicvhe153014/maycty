import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NAVBAR_ITEMS } from './constants/navbar-items.constant';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { NavigationStart, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { deepSearch } from '@core/utils';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CheckHasAnyProductFeed, CommonState } from '@shared/common/store';
import { User } from '@models';
import { AuthService } from '@features/auth/store';
import { Policies } from '@core';

@Component({
  selector: 'novanet-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('fade', [
      state(
        'open',
        style({
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          opacity: 0,
        })
      ),
      transition('open => closed', [animate('200ms 100ms ease-in-out')]),
      transition('closed => open', [animate('300ms ease-in-out')]),
    ]),
  ],
})
export class NavbarComponent implements OnInit {
  @ViewChild('navbarContainer', { static: true }) navbarContainer: ElementRef;
  @Output() public navbarWidth = new EventEmitter<number>();

  @Select(CommonState.isCampaignEnabled)
  public isCampaignEnabled$: Observable<boolean>;

  public navbarItems = NAVBAR_ITEMS.filter(
    (item) => item.id !== 'campaignManagement'
  );
  public readonly Array = Array;
  public policies: string[] = [] as string[];
  user: User;
  isExpanded = false;
  selectedItem = '';
  subMenuOpening = '';

  constructor(
    private readonly router: Router,
    private readonly store: Store,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly authService: AuthService
  ) {}

  ngOnInit() {
    this.changeDetectorRef.detectChanges();
    this.store.dispatch(new CheckHasAnyProductFeed());
    this.user = this.authService.getUserData();
    this.policies = this.user.policies.map((x) => Policies[x]);

    this.isCampaignEnabled$.subscribe({
      next: (isCampaignEnabled) => {
        if (!isCampaignEnabled) {
          this.navbarItems = NAVBAR_ITEMS.filter(
            (item) => item.id !== 'campaignManagement'
          );
        } else {
          this.navbarItems = NAVBAR_ITEMS;
        }
        this.selectedItem = deepSearch(this.navbarItems, 'subItems', (item) =>
          item.urlRoot === '/'
            ? item.urlRoot === this.router.url
            : item.urlRoot && this.router.url.startsWith(item.urlRoot)
        )?.id;
        this.changeDetectorRef.detectChanges();
      },
    });

    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationStart),
        map((event) => event as NavigationStart)
      )
      .subscribe({
        next: (event) => {
          this.selectedItem = deepSearch(this.navbarItems, 'subItems', (item) =>
            item.urlRoot === '/'
              ? item.urlRoot === event.url
              : event.url.includes(item.urlRoot)
          )?.id;
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public toggleExpand() {
    this.isExpanded = !this.isExpanded;
    setTimeout(() => {
      this.navbarWidth.emit(this.navbarContainer.nativeElement.clientWidth);
      this.changeDetectorRef.detectChanges();
    }, 330);
  }

  public onNavItemClick(item: string) {
    this.selectedItem = item;
  }
}
