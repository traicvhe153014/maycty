import { ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from '@features/auth/store';
import { User } from '@models';
import { StorageService } from '@core/services';

@Component({
  selector: 'novanet-user-bar',
  templateUrl: './user-bar.component.html',
})
export class UserBarComponent {
  public dropdownDisplayed = false;
  public user: User;

  constructor(
    private readonly authService: AuthService,
    private readonly storageService: StorageService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.user = authService.getUserData();
  }

  get avatar() {
    return this.storageService.avatar(100, this.user.id);
  }

  public toggleDropdown(value: boolean) {
    this.dropdownDisplayed = value;
    this.changeDetectorRef.detectChanges();
  }
}
