import { BreadcrumbType } from '../types/breadcrumb.type';

export const BreadcrumbConfig: BreadcrumbType[] = [
  {
    path: '/',
    breadcrumbs: [
      {
        path: '/product-management',
        useIcon: true,
        iconName: 'dashboard-icon',
        title: 'Dashboard',
      },
    ],
  },
  {
    path: '/product-feed/list',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý nguồn dữ liệu' },
    ],
  },
  {
    path: '/product-feed/add',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/product-feed/list', title: 'Quản lý nguồn dữ liệu' },
      { title: 'Nguồn dữ liệu mới' },
    ],
  },
  {
    path: '/product-feed/:id',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/product-feed/list', title: 'Quản lý nguồn dữ liệu' },
      { title: 'Chi tiết nguồn dữ liệu' },
    ],
  },
  {
    path: '/product-management',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/product-management', title: 'Danh sách sản phẩm' },
    ],
  },
  {
    path: '/campaign/management',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý chiến dịch' },
    ],
  },
  {
    path: '/campaign/add-campaign',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/campaign/management', title: 'Quản lý chiến dịch' },
      { title: 'Chiến dịch mới' },
    ],
  },
  {
    path: '/campaign/edit-campaign',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/campaign/management', title: 'Quản lý chiến dịch' },
      { title: 'Chỉnh sửa chiến dịch' },
    ],
  },
  {
    path: '/campaign/add-adset',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/campaign/management', title: 'Quản lý chiến dịch' },
      { title: 'Nhóm quảng cáo mới' },
    ],
  },
  {
    path: '/campaign/edit-adset',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/campaign/management', title: 'Quản lý chiến dịch' },
      { title: 'Chỉnh sửa nhóm quảng cáo' },
    ],
  },
  {
    path: '/campaign/add-advertising',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/campaign/management', title: 'Quản lý chiến dịch' },
      { title: 'Tin quảng cáo mới' },
    ],
  },
  {
    path: '/product-group-management',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/product-group-management', title: 'Quản lý nhóm sản phẩm' },
    ],
  },
  {
    path: '/product-group-management/create-product-group',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/product-group-management', title: 'Quản lý nhóm sản phẩm' },
      {
        title: 'Tạo nhóm sản phẩm',
      },
    ],
  },
  {
    path: '/recharge/add-recharge',
    breadcrumbs: [
      {
        title: 'Nạp tiền tài khoản',
      },
    ],
  },
  {
    path: '/recharge',
    breadcrumbs: [
      {
        title: 'Lịch sử nạp tiền',
      },
    ],
  },
  {
    path: '/recharge/list',
    breadcrumbs: [
      {
        title: 'Lịch sử nạp tiền',
      },
    ],
  },
  {
    path: '/object/list',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý đối tượng' },
    ],
  },
  {
    path: '/object',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý đối tượng' },
    ],
  },
  {
    path: '/object/object-group-detail',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/object/list', title: 'Quản lý đối tượng' },
      { title: 'Nhóm đối tượng' },
    ],
  },
  {
    path: '/object/object-group-detail/create',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/object/list', title: 'Quản lý đối tượng' },
      { title: 'Nhóm đối tượng' },
    ],
  },
  {
    path: '/corereport/fraud-click',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Báo cáo' },
      { title: 'Báo cáo click ảo' },
    ],
  },
  {
    path: '/corereport/website',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Báo cáo' },
      { title: 'Báo cáo website' },
    ],
  },
  {
    path: '/corereport/publisher',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Báo cáo' },
      { title: 'Báo cáo Publisher' },
    ],
  },
  {
    path: '/corereport/zone',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Báo cáo' },
      { title: 'Báo cáo vùng quảng cáo' },
    ],
  },
  {
    path: '/corereport/advertising-template',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Báo cáo' },
      { title: 'Báo cáo định dạng quảng cáo' },
    ],
  },
  {
    path: '/website-price/list',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý website' },
      { title: 'Danh sách bảng giá' },
    ],
  },
  {
    path: '/website-price/add-website-price',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý website' },
      { title: 'Danh sách bảng giá', path: '/website-price/list' },
      { title: 'Tạo bảng giá website mới' },
    ],
  },
  {
    path: '/website-price/edit-website-price',
    breadcrumbs: [
      {
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý website' },
      { title: 'Danh sách bảng giá', path: '/website-price/list' },
      { title: 'Sửa bảng giá' },
    ],
  },
  {
    path: '/publisher/list',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/publisher/list', title: 'Quản lý publisher' },
    ],
  },
  {
    path: '/publisher/add',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/publisher/list', title: 'Quản lý publisher' },
      {
        title: 'Tạo publisher',
      },
    ],
  },
  {
    path: '/website/list',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { path: '/website/list', title: 'Danh sách website' },
    ],
  },
  {
    path: '/website/add',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      {path: '/website/list', title: 'Danh sách website'},
      {
        title: 'Tạo website',
      },
    ]
  },
  {
    path: '/zone/list',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý website' },
      { path: '/zone/list', title: 'Danh sách vùng quảng cáo' },
    ],
  },
  {
    path: '/zone/add',
    breadcrumbs: [
      {
        path: '/campaign/management',
        useIcon: true,
        iconName: 'dashboard-icon',
      },
      { title: 'Quản lý website' },
      { path: '/zone/list', title: 'Danh sách vùng quảng cáo' },
      { title: 'Thêm mới vùng quảng cáo' },
    ],
  },
];
