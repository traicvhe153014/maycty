import { Component } from '@angular/core';

@Component({
  selector: 'novanet-page-title',
  templateUrl: './page-title.component.html',
})
export class PageTitleComponent {}
