import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { UserBarDropdownComponent } from './user-bar-dropdown.component';
import { UserAvatarModule } from 'libs/core/components/user-avatar';
import { SvgIconModule } from '@core/components/svg-icon';
import { FormatterModule } from '@core';

@NgModule({
  declarations: [UserBarDropdownComponent],
  imports: [
    CommonModule,
    NzPopoverModule,
    UserAvatarModule,
    SvgIconModule,
    FormatterModule,
  ],
  exports: [UserBarDropdownComponent],
})
export class UserBarDropdownModule {}
