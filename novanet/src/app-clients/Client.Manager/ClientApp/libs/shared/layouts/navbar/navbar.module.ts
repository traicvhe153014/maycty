import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { NavbarItemModule } from './components/navbar-item/navbar-item.module';
import { SvgIconModule } from '@core/components/svg-icon';
import { CheckPolicyModule } from '@shared/layouts/navbar/pipes';

@NgModule({
  declarations: [NavbarComponent],
  imports: [CommonModule, SvgIconModule, NavbarItemModule, CheckPolicyModule],
  exports: [NavbarComponent],
})
export class NavbarModule {}
