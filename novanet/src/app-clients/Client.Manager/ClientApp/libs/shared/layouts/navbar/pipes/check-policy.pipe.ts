import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'checkPolicy' })
export class CheckPolicyPipe implements PipeTransform {
  transform(navbarPolicy: string | string[], policies: string[]): boolean {
    if (Array.isArray(navbarPolicy)) {
      return navbarPolicy.some((policy) => policies.indexOf(policy) > 0);
    }
    return policies.indexOf(navbarPolicy) > 0;
  }
}
