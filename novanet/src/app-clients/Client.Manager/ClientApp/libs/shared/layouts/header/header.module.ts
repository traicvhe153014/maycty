import { NgModule } from '@angular/core';
import { HeaderComponent } from './header.component';
import { CommonModule } from '@angular/common';
import { PageTitleModule } from '../page-title/page-title.module';
import { UserBarModule } from '../user-bar/user-bar.module';
import { BreadcrumbModule } from '../breadcrumb/breadcrumb.module';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, PageTitleModule, UserBarModule, BreadcrumbModule],
  exports: [HeaderComponent],
})
export class HeaderModule {}
