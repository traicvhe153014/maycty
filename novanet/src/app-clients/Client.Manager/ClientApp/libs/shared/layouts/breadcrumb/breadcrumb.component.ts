import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { match } from 'path-to-regexp';
import { BreadcrumbConfig } from './constants/breadcrumb.constant';
import { BreadcrumbType } from './types/breadcrumb.type';

@Component({
  selector: 'novanet-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  public currentBreadcrumb: BreadcrumbType;

  constructor(private router: Router) {
    this.currentBreadcrumb = BreadcrumbConfig.find(
      (item) => !!match(item.path)(router.url.split('?')[0])
    );
  }

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationStart),
        map((event) => event as NavigationStart)
      )
      .subscribe({
        next: (event) => {
          this.currentBreadcrumb = BreadcrumbConfig.find(
            (item) => !!match(item.path)(event.url.split('?')[0])
          );
        },
      });
  }
}
