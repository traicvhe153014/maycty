import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngxs/store';
import { GetMe } from '@shared/identity';
import { AuthService } from '@features/auth/store';

@Component({
  selector: 'novanet-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerComponent implements OnInit {
  @ViewChild('mainContainer', { static: true }) mainContainer: ElementRef;
  public navbarWidth = 64;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private store: Store,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.store.dispatch(new GetMe());
    this.changeDetectorRef.detectChanges();
  }

  public changeContainerNavbarWidth(navbarWidth: number) {
    this.navbarWidth = navbarWidth;
  }
}
