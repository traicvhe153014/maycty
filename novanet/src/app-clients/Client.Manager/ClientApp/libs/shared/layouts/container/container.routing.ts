import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContainerComponent } from '@layouts';
import { HasProductFeedGuard } from '@core';

const routes: Routes = [
  {
    path: '',
    component: ContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('@features/dashboard').then((m) => m.DashboardModule),
        redirectTo: 'campaign/management',
      },
      {
        path: 'product-feed',
        loadChildren: () =>
          import('@features/product-feed/product-feed.module').then(
            (m) => m.ProductFeedModule
          ),
      },
      {
        path: 'product-group-management',
        loadChildren: () =>
          import(
            '@features/product-group-management/product-group-management.module'
          ).then((m) => m.ProductGroupManagementModule),
      },
      {
        path: 'product-management',
        loadChildren: () =>
          import(
            '@features/product-management/feature-product-management.module'
          ).then((m) => m.ProductManagementModule),
      },
      {
        path: 'campaign',
        canActivate: [HasProductFeedGuard],
        loadChildren: () =>
          import(
            '@features/campaign-management/campaign-management.module'
          ).then((m) => m.CampaignManagementModule),
      },
      {
        path: 'object',
        loadChildren: () =>
          import(
            '@features/feature-object-management/object-management.module'
          ).then((m) => m.ObjectManagementModule),
      },
      {
        path: 'recharge',
        loadChildren: () =>
          import(
            '@features/feature-recharge-management/recharge-management.module'
          ).then((m) => m.RechargeManagementModule),
      },
      {
        path: 'corereport',
        loadChildren: () =>
          import('@features/feature-report/feature-report.module').then(
            (m) => m.FeatureReportModule
          ),
      },
      {
        path: 'website-price',
        loadChildren: () =>
          import(
            '@features/feature-website-price-management/website-price-management.module'
          ).then((m) => m.WebsitePriceManagementModule),
      },
      {
        path: 'publisher',
        loadChildren: () =>
          import(
            '@features/feature-publisher-management/publisher-management.module'
          ).then((m) => m.PublisherManagementModule),
      },
      {
        path: 'website',
        loadChildren: () =>
          import(
            '@features/feature-website-management/website-management.module'
          ).then((m) => m.WebsiteManagementModule),
      },
      {
        path: 'zone',
        loadChildren: () =>
          import(
            '@features/feature-zone-management/zone-management.module'
          ).then((m) => m.ZoneManagementModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContainerRoutingModule {}
