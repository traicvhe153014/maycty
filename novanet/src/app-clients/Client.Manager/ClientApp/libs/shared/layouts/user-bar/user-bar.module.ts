import { NgModule } from '@angular/core';
import { UserBarComponent } from './user-bar.component';
import { CommonModule } from '@angular/common';
import { UserBarDropdownModule } from './user-bar-dropdown/user-bar-dropdown.module';

@NgModule({
  declarations: [UserBarComponent],
  exports: [UserBarComponent],
  imports: [CommonModule, UserBarDropdownModule],
})
export class UserBarModule {}
