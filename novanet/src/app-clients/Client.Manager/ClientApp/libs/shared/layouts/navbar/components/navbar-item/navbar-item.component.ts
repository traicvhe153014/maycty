import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { NavbarItem } from '@shared/layouts/navbar/types/navbar-item.type';
import {
  NavbarIconDefaultColor,
  NavbarIconSelectedColor,
} from '@shared/layouts/navbar/constants/_index';
import { AuthService, Policies } from '@core';
import { User } from '@models';

@Component({
  selector: 'novanet-navbar-item',
  templateUrl: './navbar-item.component.html',
  styleUrls: ['./navbar-item.component.scss'],
  animations: [
    trigger('fade', [
      state(
        'open',
        style({
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          opacity: 0,
        })
      ),
      transition('open => closed', [animate('300ms ease-in-out')]),
      transition('closed => open', [animate('300ms ease-in-out')]),
    ]),
    trigger('slide', [
      state(
        'open',
        style({
          height: '*',
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          height: 0,
          opacity: 0,
        })
      ),
      transition('open => closed', [animate('300ms ease-in-out')]),
      transition('closed => open', [animate('300ms ease-in-out')]),
    ]),
  ],
})
export class NavbarItemComponent implements OnInit {
  @Input() selectedItem: string;
  @Input() isExpanded: boolean;
  @Input() navItem: NavbarItem;
  @Output() itemSelected = new EventEmitter<string>();

  public readonly Array = Array;
  public readonly navbarIconDefaultColor = NavbarIconDefaultColor;
  public readonly navbarIconSelectedColor = NavbarIconSelectedColor;
  public isSubMenuOpened: boolean;
  public user: User;
  public policies: string[] = [] as string[];

  constructor(private readonly authService: AuthService) {}

  public get isParentSelected() {
    return this.selectedItem?.startsWith(this.navItem.id);
  }

  public get isSelected() {
    return this.selectedItem === this.navItem.id;
  }

  public get hasSubItems() {
    return !!this.navItem.subItems && this.navItem.subItems.length > 0;
  }

  ngOnInit(): void {
    this.isSubMenuOpened = this.selectedItem?.includes(this.navItem.id);
    this.user = this.authService.getUserData();
    this.policies = this.user.policies.map((x) => Policies[x]);
  }

  public onNavItemClick(event: MouseEvent, item: string) {
    event.stopPropagation();
    this.itemSelected.emit(item);
  }

  public onChildNavItemClick(item: string) {
    this.itemSelected.emit(item);
  }

  public toggleSubMenuOpened(event: MouseEvent) {
    event.stopPropagation();
    this.isSubMenuOpened = !this.isSubMenuOpened;
  }
}
