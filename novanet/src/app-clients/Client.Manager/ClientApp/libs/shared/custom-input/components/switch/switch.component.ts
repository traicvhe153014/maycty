import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Injector,
  Input,
  TemplateRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputTypeEnum } from '@core/enums';
import { BaseControl } from '../_index';

@Component({
  selector: 'novanet-switch-input',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SwitchComponent extends BaseControl<string> {
  @Input() public label: string;
  @Input() public class: string;
  @Input() public checkedTemplate: TemplateRef<any>;
  @Input() public unCheckedTemplate: TemplateRef<any>;

  public inputTypeEnum = InputTypeEnum;
  public passwordType = InputTypeEnum.PASSWORD;

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  public changePasswordType(type: InputTypeEnum) {
    this.passwordType = type;
  }
}
