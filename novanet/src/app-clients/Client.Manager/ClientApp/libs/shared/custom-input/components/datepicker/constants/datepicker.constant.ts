import { EDatepicker } from '@shared/custom-input/components/datepicker/enums/datepicker.enum';
import * as dayjs from 'dayjs';

const now = dayjs();

export const DatepickerConstant = [
  {
    type: EDatepicker.Today,
    title: 'Hôm nay',
    value: [
      dayjs().hour(0).minute(0).second(0).toDate(),
      dayjs().hour(23).minute(59).second(59).toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.Yesterday,
    title: 'Hôm qua',
    value: [
      now.subtract(1, 'day').hour(0).minute(0).second(0).toDate(),
      now.subtract(1, 'day').hour(23).minute(59).second(59).toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.ThisWeek,
    title: 'Tuần này(Th2 - Hôm nay)',
    value: [
      dayjs()
        .startOf('week')
        .add(1, 'day')
        .hour(0)
        .minute(0)
        .second(0)
        .toDate(),
      dayjs()
        .endOf('week')
        .add(1, 'day')
        .hour(23)
        .minute(59)
        .second(59)
        .toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.LastSevenDays,
    title: '7 ngày qua',
    value: [
      now.subtract(7, 'day').hour(23).minute(59).second(59).toDate(),
      now.hour(0).minute(0).second(0).toDate(),
    ],
  },
  {
    type: EDatepicker.LastWeek,
    title: 'Tuần trước(Th2 - CN)',
    value: [
      dayjs()
        .startOf('weeks')
        .add(1, 'day')
        .subtract(1, 'week')
        .minute(0)
        .second(0)
        .toDate(),
      dayjs()
        .endOf('weeks')
        .add(1, 'day')
        .subtract(1, 'week')
        .hour(23)
        .minute(59)
        .second(59)
        .toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.LastFourteenDays,
    title: '14 ngày qua',
    value: [
      now.subtract(14, 'day').hour(23).minute(59).second(59).toDate(),
      now.hour(0).minute(0).second(0).toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.ThisMonth,
    title: 'Tháng này',
    value: [
      dayjs().startOf('month').minute(0).second(0).toDate(),
      dayjs().endOf('month').hour(23).minute(59).second(59).toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.LastThirtyDays,
    title: '30 ngày qua',
    value: [
      now.subtract(30, 'day').hour(23).minute(59).second(59).toDate(),
      now.hour(0).minute(0).second(0).toDate(),
    ],
    active: false,
  },
  {
    type: EDatepicker.LastMonth,
    title: 'Tháng trước',
    value: [
      now.subtract(1, 'month').startOf('month').minute(0).second(0).toDate(),
      now
        .subtract(1, 'month')
        .endOf('month')
        .hour(23)
        .minute(59)
        .second(59)
        .toDate(),
    ],
    active: false,
  },
];
