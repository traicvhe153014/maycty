export interface IOption {
    [label: string]: string | number;
}