import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  EventEmitter,
  Injector,
  Input,
  Output,
  Type,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NgControl,
} from '@angular/forms';
import { NzI18nService, vi_VN } from 'ng-zorro-antd/i18n';
import { vi } from 'date-fns/locale';
import { IFormFieldError } from '@models';
import { IInputComponentModel } from '../models/_index';

@Directive()
export class BaseControl<T>
  implements ControlValueAccessor, AfterViewChecked, AfterViewInit
{
  @Input() public layout: IInputComponentModel;
  @Input() public errorMessages: IFormFieldError[] = [];
  @Input() public name: string;
  @Input() public type: string;
  @Input() public placeholder = '';
  @Input() public tabIndex: number;
  @Input() public required: boolean;
  @Input() public disabled: boolean;
  @Input() public maxLength: number;
  @Input() public icon: string;
  @Input() public wordCounter = true;
  @Input() public enableTitle = true;
  @Input() public containerClass: string;
  @Input() public isNumberOnly = false;
  @Input() public enableTooltip = false;
  @Input() public noMatStyle = false;
  @Input() public fixedIcon: string;
  @Input() public readonly = false;

  @Output() public onblur = new EventEmitter<any>();
  @Output() public onfocus = new EventEmitter<any>();

  public control: AbstractControl;
  public isFocus = false;

  protected inputValue: string | T;

  constructor(
    protected injector: Injector,
    protected changeDetectorRef: ChangeDetectorRef,
    protected i18n?: NzI18nService
  ) {
    this.i18n?.setDateLocale(vi);
    this.i18n?.setLocale(vi_VN);
  }

  get value(): string | T {
    return this.inputValue;
  }

  set value(val: string | T) {
    this.writeValue(val);
    this.onChange(val);
  }

  get hasError(): boolean {
    return (
      !!this.control &&
      this.control.invalid &&
      (this.control.dirty || this.control.touched)
    );
  }

  public ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  public ngAfterViewInit(): void {
    const ngControl = this.injector.get<NgControl>(
      NgControl as Type<NgControl>
    );
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }
  }

  public writeValue(val: string | T): void {
    if (val !== undefined) {
      this.inputValue = val;
    }
  }

  public onBlur(): void {
    this.isFocus = false;
    this.value =
      typeof this.value === 'number' && this.value === 0
        ? this.value
        : this.value
        ? typeof this.value === 'string'
          ? this.value.trim()
          : this.value
        : '';
    this.onTouched();
    this.onblur.emit();
  }

  public onFocus(): void {
    this.isFocus = true;
    this.onfocus.emit();
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  protected onChange = (_: any) => {};
  protected onTouched = () => {};
}
