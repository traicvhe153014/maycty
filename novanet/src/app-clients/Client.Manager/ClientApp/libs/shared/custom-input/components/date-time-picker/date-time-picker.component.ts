import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  forwardRef,
  Input,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '@shared/custom-input/components/base-control';

@Component({
  selector: 'novanet-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateTimePickerComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTimePickerComponent extends BaseControl<Date> {
  @Input() disabledDate: ((d: Date) => boolean) | undefined;

  visible = false;
  privateValue: Date | null = null;
  privateHourPart = new Date().getHours().toString().padStart(2, '0');
  privateMinutePart = new Date().getMinutes().toString().padStart(2, '0');
  editingHourPart = false;
  editingMinutePart = false;
  @ViewChild('hourPart') hourPartRef: ElementRef;
  @ViewChild('minutePart') minutePartRef: ElementRef;

  onPopoverVisibleChange(value: boolean) {
    if (value) {
      this.privateValue =
        typeof this.value === 'string' ? new Date(this.value) : this.value;
      this.privateHourPart = this.privateValue
        ? this.privateValue.getHours().toString().padStart(2, '0')
        : '';
      this.privateMinutePart = this.privateValue
        ? this.privateValue.getMinutes().toString().padStart(2, '0')
        : '';
      setTimeout(() => {
        this.setHourInputWidth();
        this.setMinuteInputWidth();
      });
    }
  }

  onDateChange(result: Date): void {
    if (this.privateHourPart) {
      result.setHours(parseInt(this.privateHourPart, 10));
    } else {
      result.setHours(0);
    }
    if (this.privateMinutePart) {
      result.setMinutes(parseInt(this.privateMinutePart, 10));
    } else {
      result.setMinutes(0);
    }
    result.setSeconds(0);
    result.setMilliseconds(0);
    this.privateValue = result;
  }

  resetDate() {
    this.value = null;
    this.privateValue = null;
    this.privateHourPart = '';
    this.privateMinutePart = '';
    this.setHourInputWidth();
    this.setMinuteInputWidth();
  }

  onClickHourPart() {
    setTimeout(() => {
      this.hourPartRef.nativeElement.select();
    });
  }

  onClickMinutePart(event?: Event) {
    if (event) {
      event.stopPropagation();
    }
    setTimeout(() => {
      this.minutePartRef.nativeElement.select();
    });
  }

  onChangeHourPart(event: KeyboardEvent) {
    event.preventDefault();
    const numberValue = parseInt(event.key, 10);

    if (event.key === 'Backspace') {
      this.privateHourPart = '';
      this.hourPartRef.nativeElement.value = '';
      this.editingHourPart = false;
      this.setHourValue();
      this.setHourInputWidth();
      this.onClickHourPart();
      return;
    }
    if (event.key === 'ArrowRight') {
      this.onClickMinutePart();
      return;
    }
    if (!/[0-9]/.test(event.key)) {
      this.hourPartRef.nativeElement.value = this.privateHourPart;
      this.editingHourPart = false;
      this.setHourInputWidth();
      this.onClickHourPart();
      return;
    }
    if (this.editingHourPart) {
      if (this.privateHourPart === '00' || this.privateHourPart === '01') {
        this.privateHourPart = this.privateHourPart[1] + numberValue;
        this.editingHourPart = false;
        this.setHourValue();
        this.setHourInputWidth();
        this.onClickMinutePart();
      } else if (this.privateHourPart === '02') {
        if (numberValue <= 3) {
          this.privateHourPart = this.privateHourPart[1] + numberValue;
          this.editingHourPart = false;
          this.setHourValue();
          this.setHourInputWidth();
          this.onClickMinutePart();
        } else {
          this.privateMinutePart = numberValue.toString().padStart(2, '0');
          this.editingHourPart = false;
          this.editingMinutePart = true;
          this.setMinuteValue();
          this.setMinuteInputWidth();
          this.onClickMinutePart();
        }
      }
    } else {
      if (numberValue > 2) {
        this.privateHourPart = numberValue.toString().padStart(2, '0');
        this.editingHourPart = false;
        this.setHourValue();
        this.setHourInputWidth();
        this.onClickMinutePart();
      } else {
        this.privateHourPart = numberValue.toString().padStart(2, '0');
        this.editingHourPart = true;
        this.setHourValue();
        this.setHourInputWidth();
        this.onClickHourPart();
      }
    }
  }

  onChangeMinutePart(event: KeyboardEvent) {
    event.preventDefault();
    const numberValue = parseInt(event.key, 10);

    if (event.key === 'Backspace') {
      this.privateMinutePart = '';
      this.minutePartRef.nativeElement.value = '';
      this.editingMinutePart = false;
      this.setMinuteValue();
      this.setMinuteInputWidth();
      this.onClickMinutePart();
      return;
    }
    if (event.key === 'ArrowLeft') {
      this.onClickHourPart();
      return;
    }
    if (!/[0-9]/.test(event.key)) {
      this.minutePartRef.nativeElement.value = this.privateMinutePart;
      this.editingMinutePart = false;
      this.setMinuteInputWidth();
      this.onClickMinutePart();
      return;
    }
    if (this.editingMinutePart) {
      this.privateMinutePart = this.privateMinutePart[1] + numberValue;
      this.editingMinutePart = false;
      this.setMinuteValue();
      this.setMinuteInputWidth();
      this.onClickMinutePart();
    } else {
      this.privateMinutePart = numberValue.toString().padStart(2, '0');
      this.editingMinutePart = numberValue <= 5;
      this.setMinuteValue();
      this.setMinuteInputWidth();
      this.onClickMinutePart();
    }
  }

  onBlurHourPart() {
    this.editingHourPart = false;
  }

  onBlurMinutePart() {
    this.editingMinutePart = false;
  }

  resetTimePart() {
    this.privateHourPart = '';
    this.hourPartRef.nativeElement.value = '';
    this.privateMinutePart = '';
    this.minutePartRef.nativeElement.value = '';
    this.setHourValue();
    this.setMinuteValue();
    this.setHourInputWidth();
    this.setMinuteInputWidth();
  }

  applyDatePicker() {
    this.value = this.privateValue;
    this.visible = false;
  }

  cancelDatePicker() {
    this.visible = false;
  }

  private setHourValue() {
    if (!this.privateValue) {
      return;
    }
    const newDate = new Date(this.privateValue);
    newDate.setHours(parseInt(this.privateHourPart || '0', 10));
    this.privateValue = newDate;
  }

  private setMinuteValue() {
    if (!this.privateValue) {
      return;
    }
    const newDate = new Date(this.privateValue);
    newDate.setMinutes(parseInt(this.privateMinutePart || '0', 10));
    this.privateValue = newDate;
  }

  private setHourInputWidth() {
    if (!this.hourPartRef.nativeElement) {
      return;
    }
    const tempHour = document.createElement('span');
    tempHour.innerHTML = this.privateHourPart || 'hh';
    document.body.appendChild(tempHour);
    this.hourPartRef.nativeElement.style.width =
      tempHour.getBoundingClientRect().width + 'px';
    document.body.removeChild(tempHour);
  }

  private setMinuteInputWidth() {
    if (!this.minutePartRef.nativeElement) {
      return;
    }
    const tempMinute = document.createElement('span');
    tempMinute.innerHTML = this.privateMinutePart || 'mm';
    document.body.appendChild(tempMinute);
    this.minutePartRef.nativeElement.style.width =
      tempMinute.getBoundingClientRect().width + 'px';
    document.body.removeChild(tempMinute);
  }
}
