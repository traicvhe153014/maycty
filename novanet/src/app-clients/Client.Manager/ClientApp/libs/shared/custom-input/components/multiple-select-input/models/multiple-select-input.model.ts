import { NzSelectOptionInterface } from 'ng-zorro-antd/select';

export interface IOptionGroup {
  labelGroup: string;
  nzOptions: NzSelectOptionInterface[];
}
