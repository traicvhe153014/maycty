import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { BaseControl } from '@shared/custom-input/components/_index';
import {
  NzSelectModeType,
  NzSelectOptionInterface,
} from 'ng-zorro-antd/select';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'novanet-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectInputComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectInputComponent extends BaseControl<string> {
  @Input() nzOptions: NzSelectOptionInterface[] | undefined;
  @Input() width: string;
  @Input() inputClass: string;
  @Input() nzDropdownRender: TemplateRef<any> | null;
  @Input() nzMode: NzSelectModeType = 'default';
  @Input() nzNotFoundContent: string;
  @Input() serverSearch = false;
  @Input() public maxTag: number;
  @Input() public maxTagText: string;
  @Input() public maxTagPlaceholder: boolean;

  @Output() nzScrollToBottom = new EventEmitter<void>();
  @Output() searchChange = new EventEmitter<string>();

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  public scrollToBottom() {
    this.nzScrollToBottom.emit();
  }

  onSearch(value: string): void {
    this.searchChange.emit(value);
  }
}
