import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  HostListener,
  Injector,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { BaseControl } from '@shared/custom-input/components/base-control';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as dayjs from 'dayjs';
import { DatepickerConstant } from './constants';
import { NzI18nService } from 'ng-zorro-antd/i18n';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker/date-picker.component';
import { clone, cloneDeep } from 'lodash';

@Component({
  selector: 'novanet-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatePickerComponent
  extends BaseControl<string | Date[]>
  implements OnInit
{
  @Input() format: string;
  @Input() ngClass = '';
  @Input() disabledDate: ((d: Date) => boolean) | undefined;
  @Input() nzShowToday = true;
  @Input() nzRenderExtraFooter: TemplateRef<any> | undefined;
  @Input() inputType: 'single' | 'range' = 'single';
  @Input() placeholderDateTime: string | string[] = '';
  @Input() footerTemplate: boolean;
  @Input() ngModelOptions: boolean;

  @Output() valueChange = new EventEmitter<Date[]>();

  @ViewChild('novanetDatepicker') datepickerRef: NzDatePickerComponent;
  @ViewChild('novanetSingleDatepicker')
  public singleDatepickerRef: NzDatePickerComponent;
  public datepic: Date;

  public options = DatepickerConstant;
  public validValue: boolean;

  public isRangePickerOpened = false;
  public isOpensingle = false;
  private privateValue: Date[] | any = [null, null];
  private previousValue: Date[] | string = this.inputValue;
  private previousOption = DatepickerConstant;

  constructor(
    injector: Injector,
    changeDetectorRef: ChangeDetectorRef,
    i18n: NzI18nService
  ) {
    super(injector, changeDetectorRef, i18n);
  }

  override get value(): string | Date[] {
    return this.inputValue;
  }

  override set value(val: string | Date[]) {
    this.inputValue = val;
    this.onChange(val);
  }

  ngOnInit() {
    this.previousValue = cloneDeep(this.inputValue);
  }

  public onValueChange(value: Date[] | any, dateType: string) {
    if (!this.privateValue[0] && !this.privateValue[1]) {
      this.privateValue = this.value;
    }
    const startDate = dayjs(this.privateValue[0]);

    const endDate = dayjs(this.privateValue[1]);

    let selectedValue = null;

    if (dateType === 'start_date') {
      selectedValue = dayjs(value).hour(0).minute(0).second(0).millisecond(0);
    } else {
      selectedValue = dayjs(value)
        .hour(23)
        .minute(59)
        .second(59)
        .millisecond(99);
    }

    if (this.inputType === 'single') {
      this.value = value;
    } else {
      if (dateType === 'start_date') {
        this.privateValue[0] = value as any;
        this.validValue = endDate.isAfter(selectedValue);
      } else if (dateType === 'end_date') {
        this.privateValue[1] = value as any;
        this.validValue = startDate.isBefore(selectedValue);
      }
    }

    this.options.forEach((option) => {
      option.active = false;
    });
  }

  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event): void {
    if (
      this.inputType === 'range' &&
      this.isRangePickerOpened &&
      !this.datepickerRef?.cdkConnectedOverlay?.overlayRef?.overlayElement?.contains(
        event.target
      ) &&
      !this.datepickerRef?.rangePickerInputs?.first?.nativeElement?.contains(
        event.target
      ) &&
      !this.datepickerRef?.rangePickerInputs?.last?.nativeElement?.contains(
        event.target
      )
    ) {
      this.cancelDatePicker();
    }
  }

  public setDateValue(values: Date[], option) {
    const exist = this.options.find((x) => x.type === option.type);
    exist.active = true;
    this.options.forEach((item) => {
      if (item.type !== option.type) {
        item.active = false;
      }
    });
    const clonedValues = cloneDeep(values);
    this.writeValue(clonedValues);
    this.privateValue = clonedValues;
    this.validValue = true;
  }

  public change(value: Date[]) {
    if (this.inputType === 'single') {
      this.valueChange.emit(value);
    }
  }

  public onModelChange(value: Date[]) {
    if (this.inputType === 'single') {
      this.value = value;
      return;
    }
    if (this.inputType === 'range' && !value.length) {
      this.privateValue = [];
      this.value = value;
      this.valueChange.emit(this.value);
    }
  }

  public cancelDatePicker() {
    this.value = this.previousValue;
    this.privateValue = this.previousValue;
    this.options = this.previousOption;
    this.isRangePickerOpened = false;
    this.validValue = undefined;
    this.datepickerRef.close();
  }

  public applyDatePicker() {
    if (
      !this.validValue ||
      !this.privateValue[0] ||
      !this.privateValue[1] ||
      dayjs(this.privateValue[0]).isAfter(dayjs(this.privateValue[1]))
    ) {
      return;
    }
    this.value = cloneDeep(this.privateValue);
    if (typeof this.value === 'string') {
      this.valueChange.emit([new Date(this.value)]);
    } else {
      this.valueChange.emit(this.value);
    }
    this.isRangePickerOpened = false;
    this.datepickerRef.close();
  }

  public onOpen($event) {
    if ($event) {
      this.previousValue = cloneDeep(this.value);
      this.previousOption = this.options;
      this.privateValue = clone(this.previousValue);
      this.validValue = undefined;
    } else {
      if (!this.validValue) {
        this.value = this.previousValue;
        this.privateValue = this.previousValue;
        this.options = this.previousOption;
        this.validValue = undefined;
      }
    }
  }

  public onChangeDate(result: Date) {}

  public onOk(result: Date | Date[] | null) {}
}
