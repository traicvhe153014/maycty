import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Injector,
  Input,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputTypeEnum } from '@core/enums';
import { BaseControl } from '../_index';

@Component({
  selector: 'novanet-checkbox-input',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxComponent extends BaseControl<string> {
  @Input() public label: string;
  @Input() public class: string;

  public inputTypeEnum = InputTypeEnum;
  public passwordType = InputTypeEnum.PASSWORD;

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  public changePasswordType(type: InputTypeEnum) {
    this.passwordType = type;
  }
}
