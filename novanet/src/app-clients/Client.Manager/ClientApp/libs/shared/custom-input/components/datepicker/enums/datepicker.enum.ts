export enum EDatepicker {
  Today = 1,
  Yesterday,
  ThisWeek,
  LastSevenDays,
  LastWeek,
  LastFourteenDays,
  ThisMonth,
  LastThirtyDays,
  LastMonth,
}
