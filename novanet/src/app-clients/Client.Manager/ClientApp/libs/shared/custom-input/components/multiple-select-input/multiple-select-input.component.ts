import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '@shared/custom-input/components/base-control';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { NzSelectComponent } from 'ng-zorro-antd/select/select.component';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { MultipleSelectInputEnum } from '@shared/custom-input/components/multiple-select-input/enums';
import { IOptionGroup } from '@shared/custom-input/components/multiple-select-input/models/multiple-select-input.model';

@Component({
  selector: 'novanet-multiple-select-input',
  templateUrl: './multiple-select-input.component.html',
  styleUrls: ['./multiple-select-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultipleSelectInputComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultipleSelectInputComponent
  extends BaseControl<any[]>
  implements OnInit, AfterViewInit, OnDestroy, AfterContentInit
{
  @Input() nzOptions: NzSelectOptionInterface[] | undefined;
  @Input() width: string;
  @Input() height: string;
  @Input() widthOverLoad: string;
  @Input() inputClass: string;
  @Input() nzDropdownRender: TemplateRef<any> | null;
  @Input() nzNotFoundContent: string;
  @Input() serverSearch = false;
  @Input() public useMaxTag = true;
  @Input() public maxTag: number = 2;
  @Input() public maxTagText: string;
  @Input() public maxTagPlaceholder: string;
  @Input() public topInput = true;
  @Input() public styleSelect: MultipleSelectInputEnum;
  @Input() public nzOptionGroup: IOptionGroup[] | undefined;
  @Input() public tileDropdown: string;
  @Input() public selectedColumnDefault: any[] = [];
  @Input() public privateValue: string | any[] = [];
  public privateValueDefault: string | any[] = [];

  @Output() nzScrollToBottom = new EventEmitter<void>();
  @Output() search = new EventEmitter<string>();

  public readonly string = String;
  public visible = false;
  public privateVisible = false;
  public selectMultiplePrivate: string[] = [];
  public selectedMultiple: string[] = [];
  public sizeInputHidden: { [key: string]: string } = {
    ['height']: '40px',
    ['width']: '400px',
  };
  public topDropdownOld: number;
  public multipleSelectStyle = MultipleSelectInputEnum;
  public valueChange: string | any[] = [];

  @ViewChild('multipleSelect')
  private multipleSelect: NzSelectComponent;
  @ViewChild('multiplePrivateSelect')
  private multiplePrivateSelect: NzSelectComponent;
  @ViewChild('multipleSelectInput') targetElement: any;

  private debouncedSearch$ = new Subject<string>();
  private destroy$ = new Subject<void>();

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  ngOnInit() {
    this.debouncedSearch$
      .pipe(debounceTime(300), takeUntil(this.destroy$))
      .subscribe({
        next: (data) => {
          this.search.emit(data);
        },
      });
    if (this.styleSelect === MultipleSelectInputEnum.MULTIPLE_SELECT_COLUMN) {
      this.updatePrivateValueDefault(this.selectedColumnDefault);
    }
    this.onReset();
  }

  ngAfterContentInit() {
    setTimeout(() => {
      if (this.topInput) {
        this.topDropdownOld = this.targetElement?.nativeElement.offsetTop;
      } else {
        this.topDropdownOld = 140;
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.debouncedSearch$.complete();
  }

  override ngAfterViewInit() {
    super.ngAfterViewInit();
    const self = this;
    if (this.multipleSelect) {
      this.multipleSelect.onItemDelete = new Proxy(
        this.multipleSelect.onItemDelete,
        {
          apply(target, that, args) {
            target.apply(that, args);
            self.privateValue = self.value;
            self.privateVisible = false;
            self.visible = false;
            setTimeout(() => {
              self.setSizeInputHidden();
            });
          },
        }
      );
    }
  }

  public loadMoreOptions() {
    this.nzScrollToBottom.emit();
  }

  public onVisibleChange(event: boolean) {
    this.visible = event;
    this.privateVisible = event;
    setTimeout(() => {
      this.setSizeInputHidden();
    });
  }

  public onPrivateVisibleChange(event: boolean) {
    this.privateVisible = event;
  }

  public onSearch(event: string) {
    const privateSearchElement =
      this.multiplePrivateSelect.originElement.nativeElement.querySelector(
        '.ant-select-selection-search-input'
      );
    privateSearchElement.value = event;
    privateSearchElement.dispatchEvent(new Event('input'));
    this.debouncedSearch$.next(event);
  }

  public onApply() {
    this.value = this.privateValue;
    this.privateValueDefault = this.privateValue;
    this.privateVisible = false;
    this.visible = false;
    setTimeout(() => {
      this.setSizeInputHidden();
    });
  }

  public onReset() {
    this.privateValue = this.privateValueDefault;
    this.valueChange = this.privateValueDefault;
  }

  public isOptionChecked(value): boolean {
    return this.privateValue.includes(value);
  }

  public setSizeInputHidden() {
    if (this.styleSelect === MultipleSelectInputEnum.MULTIPLE_SELECT_COLUMN)
      return;
    const height = this.targetElement.nativeElement.offsetHeight;
    let styleElement = document.getElementById('multipleDropdownStyle');
    if (!styleElement) {
      styleElement = document.createElement('style');
      styleElement.id = 'multipleDropdownStyle';
      styleElement.innerHTML = `.topDropdown { top: ${
        height + this.topDropdownOld
      }px !important; }`;
      document.body.appendChild(styleElement);
    } else {
      styleElement.innerHTML = `.topDropdown { top: ${
        height + this.topDropdownOld
      }px !important; }`;
    }
    this.changeDetectorRef.detectChanges();
  }

  public onChangeValue(val: any[]) {
    this.valueChange = val.sort(function (a, b) {
      return a - b;
    });
    this.changeDetectorRef.detectChanges();
  }

  public updatePrivateValueDefault(listOptionGroup: any[]) {
    if (this.styleSelect === MultipleSelectInputEnum.MULTIPLE_SELECT_COLUMN) {
      let privateValueDefault = [];
      listOptionGroup.forEach(function (item) {
        privateValueDefault.push(item);
      });
      this.privateValueDefault = privateValueDefault.sort(function (a, b) {
        return a - b;
      });
    }
  }

  get onPrivateValueDefault(): boolean {
    let status = true;
    if (this.privateValueDefault.length !== this.valueChange.length)
      return false;
    if (
      this.privateValueDefault.length > 0 &&
      typeof this.privateValueDefault[0] === 'number'
    ) {
      const valueChange = this.valueChange as number[];
      (this.privateValueDefault as number[])
        .sort(function (a, b) {
          return a - b;
        })
        .forEach(function (item, index) {
          if (valueChange[index] !== item) {
            status = false;
            return;
          }
        });
      return status;
    }
    return true;
  }
}
