export interface IInputComponentModel {
  tooltip?: string;
  title?: string;
  label: string;
  label2?: string;
  placeholder: string;
}
