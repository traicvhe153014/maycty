import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '@shared/custom-input/components/base-control';

@Component({
  selector: 'novanet-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PopupComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopupComponent<T>
  extends BaseControl<any[]>
  implements OnInit, AfterViewInit
{
  @Input() public nzOptions: T[];
  @Input() public listValueKey: string[];
  @Input() public valueKey: string;
  @Input() public label: string;
  @Input() public optionsId: any;
  public isConfirmActiveChangeModalVisible = false;
  public privateValue: string[] = [];
  public privateValueChange: string[] = [];

  @Output() onUpdate = new EventEmitter<{
    optionsId: any;
    listValueKey: any[];
  }>();

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  ngOnInit() {}

  override ngAfterViewInit() {
    this.privateValue = this.listValueKey;
    this.privateValueChange = this.listValueKey;
  }

  public isOptionChecked(value): boolean {
    return this.privateValue.includes(value);
  }

  public handleCancelConfirmChangeActive() {
    this.isConfirmActiveChangeModalVisible = false;
  }

  public setEditCategory() {
    this.isConfirmActiveChangeModalVisible = true;
  }

  public onChangeOption($event) {
    this.privateValueChange = $event;
  }

  public clickConfirmChangeActive() {
    this.privateValue = this.privateValueChange;
    this.onUpdate.emit({
      optionsId: this.optionsId,
      listValueKey: this.privateValue,
    });
    this.isConfirmActiveChangeModalVisible = false;
  }
}
