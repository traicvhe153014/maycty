import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { NzSizeDSType } from 'ng-zorro-antd/core/types';

@Component({
  selector: 'novanet-switch-no-animation',
  template: `
    <nz-switch
      *ngIf="!nzControl"
      (ngModelChange)="change($event)"
      [nzLoading]="loading"
      ngClass="{{ isRendered ? 'inline-block' : 'hidden' }} {{
        active ? 'switch-active' : 'switch-inactive'
      }}"
      [ngModel]="active"
      [nzCheckedChildren]="checkedTemplate"
      [nzUnCheckedChildren]="unCheckedTemplate"
      [nzSize]="size"></nz-switch>
    <nz-switch
      *ngIf="nzControl"
      [nzControl]="true"
      (click)="click()"
      [nzLoading]="loading"
      ngClass="{{ isRendered ? 'inline-block' : 'hidden' }} {{
        active ? 'switch-active' : 'switch-inactive'
      }}"
      [(ngModel)]="active"
      [nzCheckedChildren]="checkedTemplate"
      [nzUnCheckedChildren]="unCheckedTemplate"
      [nzSize]="size"></nz-switch>
    <ng-template #checkedTemplate>
      <svg-icon [height]="7" [width]="9" name="tick-small"></svg-icon>
    </ng-template>
    <ng-template #unCheckedTemplate>
      <svg-icon [size]="8" name="cross-small"></svg-icon>
    </ng-template>
  `,
  styles: [
    `
      :host ::ng-deep {
        .switch-active {
          > button {
            background-color: #0061c1;
          }
        }

        .switch-inactive {
          > button {
            background-color: #bdbdbd;
          }
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SwitchNoAnimationComponent implements AfterViewInit {
  @Input() active: boolean;
  @Input() loading: boolean;
  @Input() nzControl: boolean;
  @Input() size: NzSizeDSType = 'default';
  @Output() activeChange = new EventEmitter<boolean>();

  isRendered = false;

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.isRendered = true;
    this.changeDetectorRef.detectChanges();
  }

  change(event: boolean) {
    this.activeChange.emit(event);
  }

  click() {
    this.activeChange.emit(!this.active);
  }
}
