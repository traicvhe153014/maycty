import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '../_index';
import { NzInputNumberComponent } from 'ng-zorro-antd/input-number';

@Component({
  selector: 'novanet-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NumberInputComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberInputComponent extends BaseControl<number> {
  @Input() unit: string;
  @Input() min: number;
  @Input() max: number;
  @Input() arrow = true;
  @Input() allowZero = false;
  @Output() inputKeyDown = new EventEmitter<KeyboardEvent>();

  @ViewChild('numberInput') numberInputRef: NzInputNumberComponent;

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  override get value(): string | any {
    return this.inputValue;
  }

  override set value(value: string | number) {
    if (typeof value === 'number' && value > Number.MAX_SAFE_INTEGER) {
      value = 0;
    } else if (typeof value === 'string' && value.includes('e')) {
      value = '0';
    }
    if (this.isFocus) {
      this.writeValue(`${value}`?.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
      this.onChange(`${value}`?.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    } else {
      this.writeValue(
        value || (this.allowZero && value !== '')
          ? +`${value}`?.replace(/\$\s?|(,*)/g, '')
          : ''
      );
      this.onChange(
        value || (this.allowZero && value !== '')
          ? +`${value}`?.replace(/\$\s?|(,*)/g, '')
          : ''
      );
    }
  }

  formatter = (value) =>
    value
      ? `${value}`?.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      : value === 0
      ? '0'
      : '';

  parser = (value) =>
    value
      ? value.replace(/\$\s?|(,*)/g, '')
      : this.allowZero && value === 0
      ? '0'
      : '';

  onKeyDown(event: KeyboardEvent) {
    if (/^[a-z]$/.test(event.key.toLowerCase())) {
      event.preventDefault();
    }
    this.inputKeyDown.emit(event);
  }

  focus() {
    this.numberInputRef.focus();
  }
}
