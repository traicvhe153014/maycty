import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { SwitchNoAnimationComponent } from './switch-no-animation.component';
import { NzSwitchModule } from 'ng-zorro-antd/switch';

@NgModule({
  declarations: [SwitchNoAnimationComponent],
  imports: [CommonModule, NzSwitchModule, FormsModule, SvgIconModule],
  exports: [SwitchNoAnimationComponent],
})
export class SwitchNoAnimationModule {}
