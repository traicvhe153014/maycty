import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputTypeEnum } from '@core/enums';
import { BaseControl } from '../_index';
import { NzAutocompleteComponent } from 'ng-zorro-antd/auto-complete';

@Component({
  selector: 'novanet-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextInputComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextInputComponent extends BaseControl<string> {
  @Input() public controlError = false;
  @Input() public hideErrorIcon = false;
  @Input() public inputType: string;
  @Input() public class: string;
  @Input() public max: number;
  @Input() public min: number;
  @Input() public templateAddOnAfter: TemplateRef<any>;
  @Input() public templateAddOnBefore: TemplateRef<any>;
  @Input() public autocomplete: string | undefined;
  @Input() public nzAutocomplete: NzAutocompleteComponent | undefined;
  @Input() public indexInput: number;
  @Input() public showCounter: boolean = false;
  @Input() public counterMaxLength: number | undefined;

  @Output() public deleteInput = new EventEmitter<number>();
  @Output() public changeInput = new EventEmitter<string>();

  public inputTypeEnum = InputTypeEnum;
  public passwordType = InputTypeEnum.PASSWORD;

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  get currentType(): string {
    return this.inputType
      ? this.inputType === this.inputTypeEnum.PASSWORD
        ? this.passwordType
        : this.inputType
      : this.inputTypeEnum.TEXT;
  }

  public changePasswordType(type: InputTypeEnum) {
    this.passwordType = type;
  }

  public onCancelInput() {
    this.deleteInput.emit(this.indexInput);
  }

  public onChangeInput(val: string) {
    this.changeInput.emit(val);
  }
}
