import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '@shared/custom-input/components/base-control';
import { IPredicateModel } from '@core/models';
import { PredicateOperatorEnum } from '@core/enums';
import { NzSelectComponent } from 'ng-zorro-antd/select/select.component';
import { ISearchOption } from '../../../../core/models/search-option.model';
import { ESearchInput } from '@shared/custom-input/components/search-input/enums/search.enums';

@Component({
  selector: 'novanet-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchInputComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchInputComponent<TEnumLevel>
  extends BaseControl<any[]>
  implements OnInit
{
  @Input() filterPredicates: IPredicateModel[] = [];
  @Input() searchLevelFields: { [key: number]: string } = {};
  @Input() searchOperatorLabels: { [key: number]: string } = {};
  @Input() searchLevelLabels: { [key: number]: string } = {};
  @Input() storageFilter: string;
  @Input() levelEnum: TEnumLevel;
  @Input() selectedSearchOptionStorage: ISearchOption[];
  @Input() searchWith: string;
  @Input() placeHolder: string;
  @Input() searchType: ESearchInput;

  @Output() searchOptionsChange = new EventEmitter<IPredicateModel[]>();
  @Output() onfocusInput = new EventEmitter<boolean>();

  @ViewChild('searchInput') searchInputRef: NzSelectComponent;
  @ViewChild('previewSelected') previewSelectedRef: ElementRef;
  @ViewChild('divSearchInput') divSearchInput: ElementRef;

  public searchOptions: ISearchOption[] = [];
  public selectedSearchOptions: ISearchOption[] = [];
  public keyword = '';
  public searchDropdownOpen = false;
  public isClickOption = false;
  public EnumSearchInput = ESearchInput;

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  ngOnInit() {
    this.selectedSearchOptions = this.selectedSearchOptionStorage;
    this.searchOptions = this.selectedSearchOptionStorage;
    this.filterPredicates = this.searchOptions.map((option) => ({
      value: option.value,
      operator: option.operator,
      field: this.searchLevelFields[option.level],
    }));
  }

  public onSearchOptionSelect(options: ISearchOption[]) {
    this.selectedSearchOptions.forEach((option) => {
      if (option.value === this.keyword) {
        return;
      }
    });
    this.selectedSearchOptions = options;
    this.filterPredicates = options.map((option) => ({
      value: option.value,
      operator: option.operator,
      field: this.searchLevelFields[option.level],
    }));
    this.isClickOption = true;
    setTimeout(() => {
      this.searchInputRef.focus();
    });
    this.changeDetectorRef.detectChanges();
    this.searchOptionsChange.emit(this.filterPredicates);
  }

  public onSearchInput(value: string): void {
    this.keyword = value;
    setTimeout(() => {
      this.setNoneDropdown();
    });
    if (!value) {
      this.searchOptions = [];
      this.changeDetectorRef.detectChanges();
      return;
    }
    this.searchOptions = Object.values(this.levelEnum)
      .filter(isNaN)
      .map((level) => ({
        level: this.levelEnum[level],
        operator: PredicateOperatorEnum.Contains,
        value,
      }));
    this.changeDetectorRef.detectChanges();
  }

  public onSearchOpenChange(state: boolean) {
    this.searchDropdownOpen = state;
    setTimeout(() => {
      this.setNoneDropdown();
    });
  }

  public filterPredicateComparator(
    option1: ISearchOption,
    option2: ISearchOption
  ) {
    return (
      option1?.level === option2?.level &&
      option1?.value === option2?.value &&
      option1?.operator === option2?.operator
    );
  }

  onClose(): void {
    this.onSearchOptionSelect([]);
    setTimeout(() => {
      this.isFocus = true;
    });
  }

  public onFocusSearch(): void {
    this.isFocus = true;
    this.onfocus.emit();
  }

  public openSearch(): void {
    this.isFocus = true;
    setTimeout(() => {
      this.searchInputRef.focus();
      this.setNoneDropdown();
    });
    this.onfocus.emit();
  }

  public setNoneDropdown() {
    const element = document.getElementsByClassName('searchDropdown')[0];
    if (this.keyword === '') {
      element.classList.add('searchDropdownDisplayNone');
      element.classList.remove('searchDropdownDisplayBlock');
    } else {
      element.classList.add('searchDropdownDisplayBlock');
      element.classList.remove('searchDropdownDisplayNone');
    }
  }

  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event): void {
    if (
      !this.searchInputRef?.cdkConnectedOverlay?.overlayRef?.overlayElement?.contains(
        event.target
      ) &&
      !this.searchInputRef?.originElement?.nativeElement?.contains(
        event.target
      ) &&
      !this.previewSelectedRef?.nativeElement?.contains(event.target) &&
      !this.divSearchInput?.nativeElement?.contains(event.target)
    ) {
      this.isFocus = false;
      this.onTouched();
      this.onblur.emit();
    }
  }
}
