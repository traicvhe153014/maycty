import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '../_index';
import { NzSelectModeType } from 'ng-zorro-antd/select';
import { IHttpGetRequest } from '@core/models';
import { NzSelectSizeType } from 'ng-zorro-antd/select/select.component';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'novanet-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DropdownComponent<T>
  extends BaseControl<string>
  implements OnInit, OnDestroy
{
  @Input() public selectType: 'select' | 'dropdown' = 'select';
  @Input() public valueKey: string;
  @Input() public label: string;
  @Input() public options: T[];
  @Input() public mode: NzSelectModeType = 'default';
  @Input() public isLoading = false;
  @Input() public page = 1;
  @Input() public pageSize = 10;
  @Input() public skip = 0;
  @Input() public optionHeight = 32;
  @Input() public size: NzSelectSizeType = 'default';
  @Input() public allowClear: boolean;
  @Input() public maxTag: number;
  @Input() public maxTagPlaceholder: boolean;
  @Input() public nzDropdownClassName: string;
  @Input() public nzOptionHeightPx: number | null;
  @Input() public nzOverlayClassName: string;
  @Input() public nzDisabled: boolean;
  @Input() public popoverNotification: string;
  @Input() serverSearch = false;
  @Input() showSearch = false;

  @Output() public getNextBatch = new EventEmitter<IHttpGetRequest>();
  @Output() public getChange = new EventEmitter<string>();
  @Output() public valueKeyDropdown = new EventEmitter<any>();
  @Output() search = new EventEmitter<string>();

  public readonly TemplateRef = TemplateRef;

  @ViewChild('selectTemplate') selectTemplate: TemplateRef<any>;

  private debouncedSearch$ = new Subject<string>();
  private destroy$ = new Subject<void>();

  constructor(injector: Injector, changeDetectorRef: ChangeDetectorRef) {
    super(injector, changeDetectorRef);
  }

  ngOnInit() {
    if (this.serverSearch) {
      this.debouncedSearch$
        .pipe(debounceTime(300), takeUntil(this.destroy$))
        .subscribe({
          next: (data) => {
            this.search.emit(data);
          },
        });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.debouncedSearch$.complete();
  }

  public onSearch(event: string) {
    if (this.serverSearch) {
      this.debouncedSearch$.next(event);
    } else {
      this.search.emit(event);
    }
  }

  public loadMore(): void {
    if (this.isLoading) {
      return;
    }
    this.page += 1;
    this.skip = this.skip + this.pageSize;
    const params = {
      page: this.page,
      skip: this.skip,
      pageSize: this.pageSize,
    } as IHttpGetRequest;
    this.getNextBatch.emit(params);
  }

  public emitChange($event): void {
    this.getChange.emit($event);
  }

  public isItemTemplate(item: string | TemplateRef<any>): boolean {
    return item instanceof TemplateRef;
  }

  public valueKeyDropdownMenu(value: any): void {
    this.valueKeyDropdown.emit(value);
  }
}
