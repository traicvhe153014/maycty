import { NgModule } from '@angular/core';
import {
  DatePickerComponent,
  DateTimePickerComponent,
  MultipleSelectInputComponent,
  NumberInputComponent,
  RadioInputComponent,
  SelectInputComponent,
  TextareaInputComponent,
  TextInputComponent,
} from './components/_index';
import { CommonModule } from '@angular/common';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { DropdownComponent } from './components/dropdown';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NZ_I18N, vi_VN } from 'ng-zorro-antd/i18n';
import { CheckboxComponent } from './components/checkbox';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { SwitchComponent } from '@shared/custom-input/components/switch';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { CastModule, FormatterModule } from '@core';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { SvgIconModule } from '@core/components/svg-icon';
import { PopupComponent } from '@shared/custom-input/components/popup/popup.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { SearchInputComponent } from '@shared/custom-input/components/search-input/search-input.component';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzButtonModule } from 'ng-zorro-antd/button';

const COMPONENTS = [
  TextInputComponent,
  TextareaInputComponent,
  NumberInputComponent,
  DropdownComponent,
  SelectInputComponent,
  DatePickerComponent,
  CheckboxComponent,
  SwitchComponent,
  MultipleSelectInputComponent,
  RadioInputComponent,
  DateTimePickerComponent,
  PopupComponent,
  SearchInputComponent,
];

const MODULES = [
  CommonModule,
  NzInputModule,
  NzInputNumberModule,
  FormsModule,
  ReactiveFormsModule,
  NzDatePickerModule,
  NzSelectModule,
  NzSpinModule,
  NzCheckboxModule,
  NzSwitchModule,
  NzAutocompleteModule,
  CastModule,
  NzDropDownModule,
  NzIconModule,
  NzPopoverModule,
  NzDividerModule,
  NzSpaceModule,
  SvgIconModule,
  NzModalModule,
  NzButtonModule,
];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [...MODULES, FormatterModule, NzTagModule],
  providers: [{ provide: NZ_I18N, useValue: vi_VN }],
})
export class NovanetInputModule {}
