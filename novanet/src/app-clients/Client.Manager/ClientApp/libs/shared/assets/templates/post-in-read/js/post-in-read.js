const localData = {
    productName: !0,
    brandName: 'Attack on Titan',
    brandAvatar:
      'https://c-cl.cdn.smule.com/rs-s78/arr/6d/a4/d060b6b2-2b6e-4b25-9442-6ed1b316e8c5.jpg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa urna lectus lacus, sed facilisi diam. Viverra sodales in nulla eu gravida diam tristique vestibulum vitae.',
    bannerImageUrl: '',
    redirectUrl:
      'https://c-cl.cdn.smule.com/rs-s78/arr/6d/a4/d060b6b2-2b6e-4b25-9442-6ed1b316e8c5.jpg',
    promotion: !0,
    freeDelivery: !0,
    templateType: 0,
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 3 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 4e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 1e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 12 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  url = new URL(window.location.href),
  apiGetData = url.searchParams.get('api_get_data');

const AdvertisingSetting = 'AdvertisingSetting';
const data = JSON.parse(sessionStorage.getItem(AdvertisingSetting)).find(
  (x) => x.templateId === 5
);

function getData() {
  renderItems(data);
}

async function getDataAsync() {
  const e = await fetch(dataFromApi);
  let r = await e.json();
  (document.body.style.background =
    'url(' + r.bannerImageUrl + ') no-repeat center'),
    console.log(window.location.href),
    renderItems(r);
}

function renderItems(e) {
  let r = '';
  for (let t = 0; t < 4; t++)
    r +=
      '<a target="_blank" href=' +
      e.products[t].redirectUrl +
      " class='post-in-read-slide wrapbox auto'>\n        <div class='relative'>\n          <img src='" +
      e.products[t].productImageUrl +
      "'/>\n" +
      (e.freeDelivery ? "<div\nclass='absolute trapezoid-up'>\n</div>\n" : '') +
      (e.promotion
        ? "<div class='absolute trapezoid-down'>\n<span class='absolute sale-text'>sale " +
          formatNumber(getDiscount(e.products[t])) +
          '%</span>\n</div>\n'
        : '') +
      (e.freeDelivery
        ? "<span class='absolute free-delivery-text'>FREESHIP</span>\n"
        : '') +
      '        </div>\n        <div class="description">\n          <div class="title">\n' +
      (e.productName ? e.products[t].title : '') +
      '          </div>\n          <div class=\'detail-price\'>\n            <span class="price">' +
      (e.promotion
        ? formatNumber(e.products[t].discountedPrice)
        : formatNumber(e.products[t].price)) +
      'đ</span>\n' +
      (e.promotion
        ? '<span class="promotion">' +
          formatNumber(e.products[t].price) +
          'đ</span>\n'
        : '') +
      '          </div>\n        </div></a>';
  return (
    (document.getElementsByClassName('post-in-read-wrapper')[0].innerHTML = r),
    (document.getElementById('brand-avatar').src = e.imageLink),
    (document.getElementById('brand-name').innerHTML = e.brandName),
    (document.getElementById('brand-description').innerHTML = e.description),
    (document.getElementById('see-more').href = e.redirectUrl),
    r
  );
}

function formatNumber(e) {
  return new Intl.NumberFormat('en-EN', { maximumSignificantDigits: 2 }).format(
    e
  );
}

function getDiscount(e) {
  return ((e.price - e.discountedPrice) / e.price) * 100;
}

getData();
