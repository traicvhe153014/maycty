const localData = {
    productName: !0,
    bannerImageUrl:
      'https://img.freepik.com/free-photo/hand-painted-watercolor-background-with-sky-clouds-shape_24972-1095.jpg?size=626&ext=jpg',
    redirectUrl: '/xxx',
    promotion: !0,
    freeDelivery: !0,
    templateType: 0,
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 3 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl:
          'https://c-cl.cdn.smule.com/rs-s90/arr/c8/a5/f60b9f5d-b393-454b-905d-c1953d2e2a62.jpg',
        redirectUrl: '/',
        discountedPrice: 4e4,
        price: 3e5,
      },
    ],
  },
  url = new URL(window.location.href),
  apiGetData = url.searchParams.get('api_get_data');

const AdvertisingSetting = 'AdvertisingSetting';
const data = JSON.parse(sessionStorage.getItem(AdvertisingSetting)).find(
  (x) => x.templateId === 1
);

function getData() {
  // (document.body.style.background =
  //   'url(' + data.imageLink + ') no-repeat center'),
  renderItems(data);
}

async function getDataAsync() {
  const e = await fetch(dataFromApi);
  let t = await e.json();
  (document.body.style.background =
    'url(' + t.bannerImageUrl + ') no-repeat center'),
    console.log(window.location.href),
    renderItems(t);
}

function renderItems(e) {
  let t = '';
  for (let r = 0; r < 2; r++)
    t +=
      '<a target="_blank" href=' +
      e.products[r].redirectUrl +
      " class='mobile-banner-card-slide wrapbox auto'>\n        <div class='relative'>\n          <img src='" +
      e.products[r].productImageUrl +
      "'/>\n" +
      (e.freeDelivery ? "<div\nclass='absolute trapezoid-up'>\n</div>\n" : '') +
      (e.promotion
        ? "<div class='absolute trapezoid-down'>\n<span class='absolute sale-text'>sale " +
          formatNumber(getDiscount(e.products[r])) +
          '%</span>\n</div>\n'
        : '') +
      (e.freeDelivery
        ? "<span class='absolute free-delivery-text'>FREESHIP</span>\n"
        : '') +
      '        </div>\n        <div class="description">\n          <div class="title">\n' +
      (e.productName ? e.products[r].title : '') +
      '          </div>\n          <div class=\'detail-price\'>\n            <span class="price">' +
      (e.promotion
        ? formatNumber(e.products[r].discountedPrice)
        : formatNumber(e.products[r].price)) +
      'đ</span>\n' +
      (e.promotion
        ? '<span class="promotion">' +
          formatNumber(e.products[r].price) +
          'đ</span>\n'
        : '') +
      '          </div>\n        </div></a>';
  const r = getBackgroundSize();
  return (
    (document.getElementsByClassName(
      'mobile-banner-card-wrapper'
    )[0].innerHTML = t),
    (document.getElementsByClassName('mobile-banner-card-wrapper')[0].href =
      e.redirectUrl),
    (document.body.style.background =
      'url(' + data.imageLink + ') 0% 0% no-repeat'),
    (document.body.style.backgroundSize =
      r.backgroundWidth + 'px ' + r.backgroundHeight + 'px'),
    t
  );
}

function formatNumber(e) {
  return new Intl.NumberFormat('en-EN', { maximumSignificantDigits: 2 }).format(
    e
  );
}

function getDiscount(e) {
  return ((e.price - e.discountedPrice) / e.price) * 100;
}

function getBackgroundSize() {
  const e = window.innerWidth;
  return { backgroundWidth: e, backgroundHeight: (12 * e) / 25 };
}

getData();
