export const environment = {
  production: false,
  host: 'http://localhost:5000',
  sdkUrl: 'http://localhost:4200/assets/dev-sdk.js',
  streamingRootUrl: 'http://localhost:5009',
};
