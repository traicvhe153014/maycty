export const environment = {
  production: true,
  host: 'https://devv6.novanet.vn',
  sdkUrl: 'https://devv6.novanet.vn/assets/dev-sdk.js',
  streamingRootUrl: 'https://devv6streaming.novanet.vn',
};
