export const environment = {
  production: true,
  host: 'http://testv6.novanet.vn',
  sdkUrl: 'http://testv6.novanet.vn/assets/test-sdk.js',
  streamingRootUrl: 'http://testv6streaming.novanet.vn',
};
