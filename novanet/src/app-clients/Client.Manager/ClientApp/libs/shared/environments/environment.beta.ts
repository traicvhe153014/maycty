export const environment = {
  production: false,
  host: 'https://betav6.novanet.vn',
  sdkUrl: 'https://betav6.novanet.vn/assets/beta-sdk.js',
  streamingRootUrl: 'https://betav6streaming.novanet.vn',
};
