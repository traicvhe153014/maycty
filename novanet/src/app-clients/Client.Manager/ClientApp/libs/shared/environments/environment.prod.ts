export const environment = {
  production: true,
  host: 'https://v6.novanet.vn',
  sdkUrl: 'https://v6.novanet.vn/assets/sdk.js',
  streamingRootUrl: 'https://v6streaming.novanet.vn',
};
