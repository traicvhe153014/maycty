export const environment = {
  production: false,
  host: 'http://localhost:5001/',
  streamingRootUrl: 'http://localhost:5009',
};
