import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';

import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import {
  ISettingStateModel,
  IWebsitesResponse,
  IZone,
  IZonesResponse,
} from './settings-state.model';
import { SettingsStateService } from './settings-state.service';
import {
  ClearWebsiteList,
  ClearZoneList,
  CreateZone,
  CreateZoneError,
  CreateZoneSuccess,
  GetMoreWebsiteList,
  GetMoreWebsiteListError,
  GetMoreWebsiteListSuccess,
  GetMoreZoneList,
  GetMoreZoneListError,
  GetMoreZoneListSuccess,
  GetWebsiteList,
  GetWebsiteListError,
  GetWebsiteListSuccess,
  GetZoneList,
  GetZoneListError,
  GetZoneListSuccess,
  UpdateHasMorePages,
  UpdateZone,
  UpdateZoneError,
  UpdateZoneSuccess,
} from './settings-state.action';
import { IWebsiteStateModel } from '@features/feature-website-management/store/website-state.model';

@Injectable()
@State<ISettingStateModel>({
  name: 'settings',
  defaults: {
    websites: [],
    zones: [],
    loading: {
      website: false,
      zone: false,
    },
    totalWebsite: 0,
    totalZone: 0,
    hasMorePages: true,
    page: 1,
  },
})
export class SettingsState {
  constructor(private settingsStateService: SettingsStateService) {}

  @Selector()
  public static getWebsites(state: ISettingStateModel): IWebsitesResponse {
    return {
      data: state.websites,
      total: state.totalWebsite,
    };
  }

  @Selector()
  public static getLoadingWebsites(state: ISettingStateModel): boolean {
    return state.loading.website;
  }

  @Selector()
  public static getZones(state: ISettingStateModel): IZonesResponse {
    return {
      data: state.zones,
      total: state.totalZone,
    };
  }

  @Selector()
  public static getZonesResponse(state: ISettingStateModel): IZone[] {
    return state.zones;
  }

  @Selector()
  public static getLoadingZones(state: ISettingStateModel): boolean {
    return state.loading.zone;
  }

  @Selector()
  public static getTotalZone(state: ISettingStateModel): number {
    return state.totalZone;
  }

  @Selector()
  public static getCurrentPage(state: ISettingStateModel): number {
    return state.page;
  }

  @Action(GetWebsiteList)
  public getWebsiteList(
    ctx: StateContext<ISettingStateModel>,
    { params }: GetWebsiteList
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        website: true,
      },
    });
    return this.settingsStateService
      .listWebsites({
        ...params,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetWebsiteListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetWebsiteListError(err)))
      );
  }

  @Action(GetWebsiteListSuccess)
  public getWebsiteListSuccess(
    ctx: StateContext<ISettingStateModel>,
    { response, params }: GetWebsiteListSuccess
  ) {
    const hasMorePages = response.data.length === params.pageSize;
    ctx.patchState({
      websites: response.data,
      totalWebsite: response.total,
      hasMorePages,
      loading: {
        ...ctx.getState().loading,
        website: false,
      },
    });
  }

  @Action(GetWebsiteListError)
  public getWebsiteListError(
    ctx: StateContext<ISettingStateModel>,
    { error }: GetWebsiteListError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        website: false,
      },
    });
    return throwError(error);
  }

  @Action(GetMoreWebsiteList)
  public GetMoreWebsiteList(
    ctx: StateContext<ISettingStateModel>,
    { params }: GetMoreWebsiteList
  ) {
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        website: true,
      },
    });
    return this.settingsStateService
      .listWebsites({
        ...params,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetMoreWebsiteListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetMoreWebsiteListError(err)))
      );
  }

  @Action(GetMoreWebsiteListSuccess)
  public GetMoreWebsiteListSuccess(
    ctx: StateContext<ISettingStateModel>,
    { response, params }: GetMoreWebsiteListSuccess
  ) {
    const currentWebsites = ctx.getState().websites;
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    ctx.patchState({
      websites: [...currentWebsites, ...response.data],
      totalWebsite: response.total,
      page: newPage,
      hasMorePages,
      loading: {
        ...ctx.getState().loading,
        website: false,
      },
    });
  }

  @Action(GetMoreWebsiteListError)
  public getMoreWebsiteListError(
    ctx: StateContext<ISettingStateModel>,
    { error }: GetMoreWebsiteListError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        website: false,
      },
    });
    return throwError(error);
  }

  @Action(ClearWebsiteList)
  public clearWebsiteList(ctx: StateContext<ISettingStateModel>) {
    ctx.patchState({
      websites: [],
      totalWebsite: 0,
    });
  }

  @Action(GetZoneList)
  public getZoneList(
    ctx: StateContext<ISettingStateModel>,
    { params }: GetZoneList
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        zone: true,
      },
    });
    return this.settingsStateService
      .listZones({
        ...params,
      })
      .pipe(
        map((response) => ctx.dispatch(new GetZoneListSuccess(response))),
        catchError((err) => ctx.dispatch(new GetZoneListError(err)))
      );
  }

  @Action(GetZoneListSuccess)
  public getZoneListSuccess(
    ctx: StateContext<ISettingStateModel>,
    { response }: GetZoneListSuccess
  ) {
    ctx.patchState({
      zones: response.data,
      totalZone: response.total,
      loading: {
        ...ctx.getState().loading,
        zone: false,
      },
    });
  }

  @Action(GetZoneListError)
  public getZoneListError(
    ctx: StateContext<ISettingStateModel>,
    { error }: GetZoneListError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        zone: false,
      },
    });
    return throwError(error);
  }

  @Action(GetMoreZoneList)
  public GetMoreZoneList(
    ctx: StateContext<ISettingStateModel>,
    { params }: GetMoreZoneList
  ) {
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    if (ctx.getState().loading.zone === true) {
      return new Observable();
    }
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        zone: true,
      },
    });
    return this.settingsStateService
      .listZones({
        ...params,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetMoreZoneListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetMoreWebsiteListError(err)))
      );
  }

  @Action(GetMoreZoneListSuccess)
  public GetMoreZoneListSuccess(
    ctx: StateContext<ISettingStateModel>,
    { response, params }: GetMoreZoneListSuccess
  ) {
    const currentZones = ctx.getState().zones;
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    ctx.patchState({
      zones: [...currentZones, ...response.data],
      totalZone: response.total,
      page: newPage,
      hasMorePages,
      loading: {
        ...ctx.getState().loading,
        zone: false,
      },
    });
  }

  @Action(GetMoreZoneListError)
  public getMoreZoneListError(
    ctx: StateContext<ISettingStateModel>,
    { error }: GetMoreZoneListError
  ) {
    ctx.patchState({
      loading: {
        ...ctx.getState().loading,
        zone: false,
      },
    });
    return throwError(error);
  }

  @Action(CreateZone)
  public create(
    ctx: StateContext<ISettingStateModel>,
    { payload }: CreateZone
  ) {
    return this.settingsStateService.createZone(payload).pipe(
      map((response) => ctx.dispatch(new CreateZoneSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateZoneError(error)))
    );
  }

  @Action(CreateZoneSuccess)
  public createSuccess(
    ctx: StateContext<ISettingStateModel>,
    { response }: CreateZoneSuccess
  ) {
    return response;
  }

  @Action(CreateZoneError)
  public createError(
    ctx: StateContext<ISettingStateModel>,
    { error }: CreateZoneError
  ) {
    return throwError(error);
  }

  @Action(ClearZoneList)
  public clearZoneList(ctx: StateContext<ISettingStateModel>) {
    ctx.patchState({
      zones: [],
      totalZone: 0,
      hasMorePages: true,
      page: 1,
    });
  }

  @Action(UpdateZone)
  public updateZone(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: UpdateZone
  ) {
    return this.settingsStateService.updateZones(payload).pipe(
      map((response) => ctx.dispatch(new UpdateZoneSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateZoneError(error)))
    );
  }

  @Action(UpdateZoneSuccess)
  public updateZoneSuccess(
    ctx: StateContext<ISettingStateModel>,
    { zoneResponse }: UpdateZoneSuccess
  ) {
    const { zones } = ctx.getState();
    const updatedZones = zones.map((zone) =>
      zone.id === zoneResponse.id
        ? {
            ...zone,
            name: zoneResponse.name,
            usingBackup: zoneResponse.usingBackup,
            backupType: zoneResponse.backupType,
            redirectLink: zoneResponse.redirectLink,
            backupCode: zoneResponse.backupCode,
          }
        : zone
    );
    ctx.patchState({ zones: updatedZones });
  }

  @Action(UpdateZoneError)
  public updateError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: UpdateZoneError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(UpdateHasMorePages)
  public updateHasMorePages(
    ctx: StateContext<IWebsiteStateModel>,
    { hasMorePages }: UpdateHasMorePages
  ) {
    ctx.patchState({ hasMorePages, page: 1 });
  }
}
