import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { SettingsStateService } from './settings-state.service';
import { SettingsState } from './settings.state';

@NgModule({
  imports: [NgxsModule.forFeature([SettingsState])],
  providers: [SettingsStateService],
})
export class SettingsStateModule {}
