import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  IWebsitesResponse,
  IZone,
  IZoneCreateRequest,
  IZoneListRequest,
  IZonesResponse,
  IZoneUpdateRequest,
  IZoneUpdateResponse,
} from './settings-state.model';
import { IHttpGetRequest } from '@core/models';

const SettingsUrls = {
  listWebsites: `website/list`,
  listZones: `zone/list`,
  updateZone: `zone/update`,
  createZone: 'zone/create',
};

@Injectable()
export class SettingsStateService extends BaseService {
  constructor(
      httpClient: HttpClient,
      @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public listWebsites(data: IHttpGetRequest): Observable<IWebsitesResponse> {
    const params = this.createParams(data);
    return this.httpClient
        .get<ISuccessHttpResponse<IWebsitesResponse>>(
            this.createUrl(SettingsUrls.listWebsites),
            {
              params,
            }
        )
        .pipe(map((response) => response.data));
  }

  public listZones(data: IZoneListRequest): Observable<IZonesResponse> {
    const params = this.createParams(data);
    return this.httpClient
        .get<ISuccessHttpResponse<IZonesResponse>>(
            this.createUrl(SettingsUrls.listZones),
            {
              params,
            }
        )
        .pipe(map((response) => response.data));
  }

  public updateZones(
      payload: IZoneUpdateRequest
  ): Observable<IZoneUpdateResponse> {
    return this.httpClient
        .put<ISuccessHttpResponse<IZoneUpdateResponse>>(
            this.createUrl(SettingsUrls.updateZone),
            payload
        )
        .pipe(map((res) => res.data));
  }

  public createZone(payload: Partial<IZoneCreateRequest>): Observable<IZone> {
    return this.httpClient
        .post<ISuccessHttpResponse<IZone>>(
            this.createUrl(SettingsUrls.createZone),
            payload
        )
        .pipe(map((response) => response.data));
  }
}
