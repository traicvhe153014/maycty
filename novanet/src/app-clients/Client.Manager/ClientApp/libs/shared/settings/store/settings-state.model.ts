import { ETemplateType } from '@features/campaign-management/enums';
import { IHttpGetRequest } from '@core/models';
import { ZoneBackupType } from '@features/feature-zone-management/components/enums/zone-list.enum';
import { CampaignType } from '@features/campaign-management/store';

export interface ISettingStateModel {
  websites: IWebsite[];
  zones: IZone[];
  loading: {
    website: boolean;
    zone: boolean;
  };
  totalWebsite: number;
  totalZone: number;
  page: number;
  hasMorePages: boolean;
}

export interface IWebsitesResponse {
  data: IWebsite[];
  total: number;
}

export interface IWebsite {
  id: string;
  name: string;
  domain: string;
  url: string;
  publisherEmail?: string;
  keyword: string;
  createdAt: string;
  modifiedAt: string;
  active: boolean;
}

export interface IZoneListRequest extends IHttpGetRequest {
  websiteId?: string;
  withZoneTemplate?: boolean;
}

export interface IZonesResponse {
  data: IZone[];
  total: number;
}

export interface IZoneTemplate {
  id: string;
  zoneId?: string;
  advertisingTemplateId?: string;
  templateType: ETemplateType;
}

export interface IZone {
  id: string;
  name: string;
  isActive: boolean;
  height: number;
  width: number;
  websiteId: string;
  domain: string;
  url: string;
  publisherEmail: string;
  publisherId: string;
  zoneTemplates?: IZoneTemplate[];
  advertisingTemplateName?: string[];
  usingBackup?: boolean;
  backupType?: ZoneBackupType;
  code?: string;
  backupCode?: string;
  redirectLink?: string;
  campaignTypes: CampaignType[];
}

export interface IZoneUpdateRequest {
  id: string;
  name?: string;
  usingBackup?: boolean;
  redirectLink?: string;
  backupType?: ZoneBackupType;
  backupCode?: string;
}

export interface IZoneUpdateResponse {
  id: string;
  name: string;
  campaignType: CampaignType;
  usingBackup: boolean;
  backupCode: string;
  backupType: ZoneBackupType;
  websiteId: string;
  redirectLink?: string;
}

export interface IZoneCreateRequest {
  name: string;
  height: number;
  width: number;
  campaignType: CampaignType;
  usingBackup: boolean;
  backupType?: ZoneBackupType;
  backupCode?: string;
  websiteId: string;
  templateTypes: ETemplateType[];
  redirectLink?: string;
}
