import {
  IWebsitesResponse,
  IZone,
  IZoneCreateRequest,
  IZoneListRequest,
  IZonesResponse, IZoneUpdateRequest, IZoneUpdateResponse,
} from './settings-state.model';
import { IHttpGetRequest } from '@core/models';

const enum SettingsActions {
  GetWebsiteList = '[Adset] Get Website List',
  GetWebsiteListSuccess = '[Adset] Get Website List Success',
  GetWebsiteListError = '[Adset] Get Website List Error',
  ClearWebsiteList = '[Adset] Clear Website List',
  GetMoreWebsiteList = '[Adset] Get More Website List',
  GetMoreWebsiteListSuccess = '[Adset] Get More List Website Success',
  GetMoreWebsiteListError = '[Adset] Get More List Website Error',
  GetZoneList = '[Adset] Get Zone List',
  GetZoneListSuccess = '[Adset] Get Zone List Success',
  GetZoneListError = '[Adset] Get Zone List Error',
  ClearZoneList = '[Adset] Clear Zone List',
  GetMoreZoneList = '[Adset] Get More Zone List',
  GetMoreZoneListSuccess = '[Adset] Get More List Zone Success',
  GetMoreZoneListError = '[Adset] Get More List Zone Error',
  UpdateZone = '[Adset] Update Zone',
  UpdateZoneSuccess = '[Adset] Update Zone Success',
  UpdateZoneError = '[Adset] Update Zone Error',
  CreateZone = '[Settings] Create Zone',
  CreateZoneSuccess = '[Settings] Create Zone Success',
  CreateZoneError = '[Settings] Create Zone Error',
  UpdateHasMorePages = '[Settings] Update Has More Pages',
}

export class GetWebsiteList {
  public static readonly type = SettingsActions.GetWebsiteList;

  constructor(public params: IHttpGetRequest) {}
}

export class GetWebsiteListSuccess {
  public static readonly type = SettingsActions.GetWebsiteListSuccess;

  constructor(
    public response: IWebsitesResponse,
    public params: IHttpGetRequest
  ) {}
}

export class GetWebsiteListError {
  public static readonly type = SettingsActions.GetWebsiteListError;

  constructor(public error: any) {}
}

export class GetMoreWebsiteListSuccess {
  public static readonly type = SettingsActions.GetMoreWebsiteListSuccess;

  constructor(
    public response: IWebsitesResponse,
    public params: IHttpGetRequest
  ) {}
}

export class GetMoreWebsiteListError {
  public static readonly type = SettingsActions.GetMoreWebsiteListError;

  constructor(public error: any) {}
}

export class GetMoreWebsiteList {
  public static readonly type = SettingsActions.GetMoreWebsiteList;

  constructor(public params: any) {}
}

export class ClearWebsiteList {
  public static readonly type = SettingsActions.ClearWebsiteList;
}

export class GetZoneList {
  public static readonly type = SettingsActions.GetZoneList;

  constructor(public params: IZoneListRequest) {}
}

export class GetZoneListSuccess {
  public static readonly type = SettingsActions.GetZoneListSuccess;

  constructor(public response: IZonesResponse) {}
}

export class GetZoneListError {
  public static readonly type = SettingsActions.GetZoneListError;

  constructor(public error: any) {}
}

export class GetMoreZoneListSuccess {
  public static readonly type = SettingsActions.GetMoreZoneListSuccess;

  constructor(public response: IZonesResponse, public params: any) {}
}

export class GetMoreZoneListError {
  public static readonly type = SettingsActions.GetMoreZoneListError;

  constructor(public error: any) {}
}

export class GetMoreZoneList {
  public static readonly type = SettingsActions.GetMoreZoneList;

  constructor(public params: IZoneListRequest) {}
}

export class ClearZoneList {
  public static readonly type = SettingsActions.ClearZoneList;
}

export class CreateZone {
  public static readonly type = SettingsActions.CreateZone;

  constructor(public payload: Partial<IZoneCreateRequest>) {}
}

export class CreateZoneSuccess {
  public static readonly type = SettingsActions.CreateZoneSuccess;

  constructor(public response: IZone) {}
}

export class CreateZoneError {
  public static readonly type = SettingsActions.CreateZoneError;

  constructor(public error: any) {}
}

export class UpdateZone {
  public static readonly type = SettingsActions.UpdateZone;

  constructor(public payload: IZoneUpdateRequest) {}
}

export class UpdateZoneSuccess {
  public static readonly type = SettingsActions.UpdateZoneSuccess;

  constructor(public zoneResponse: IZoneUpdateResponse) {}
}

export class UpdateZoneError {
  public static readonly type = SettingsActions.UpdateZoneError;

  constructor(public error: any) {}
}

export class UpdateHasMorePages {
  public static readonly type = SettingsActions.UpdateHasMorePages;

  constructor(public hasMorePages: boolean) {}
}
