﻿import {Inject, Injectable} from '@angular/core';
import * as signalR from '@microsoft/signalr';
import {baseUrl} from "@shared/services/baseUrl";

@Injectable({providedIn: 'root'})
export class NovanetSignalrService {
    hubConnection: signalR.HubConnection;

    constructor(@Inject(baseUrl) protected hostUrl: string) {
    }

    startConnection = () => {
        const options = {
            transport: signalR.HttpTransportType.LongPolling,
            logging: signalR.LogLevel.Trace,
            useDefaultCredentials: true,
            withCredentials: true,
        };
        const accessToken = localStorage.getItem("ACCESS_TOKEN");
        this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl(`${this.hostUrl}/hub?access_token=${accessToken}`, options)
            .build();

        this.hubConnection.start().then(() => {
            console.log("Novanet Hub Started!");
        }).catch(err => console.log("Novanet Hub Error" + err));
    }

    send() {
        this.hubConnection.invoke("Send", "Ping")
            .catch(err => console.error(err));
    }

    receive() {
        this.hubConnection.on("Novanet", (res) => {
            console.log(res);
        })
    }
}
