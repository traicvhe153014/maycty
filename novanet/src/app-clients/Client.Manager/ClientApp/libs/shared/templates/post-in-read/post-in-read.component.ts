import {
  ChangeDetectorRef,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { SwiperComponent } from 'swiper/angular';

// import Swiper core and required modules
import SwiperCore, { EffectCoverflow, Pagination } from 'swiper';
import { IProductResponse } from '@features/product-management/models/product-management-table.model';
import { ITemplateConfiguration } from '@features/campaign-management/store';
import { ETemplateType } from '@features/campaign-management/enums';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '@environment';

// install Swiper modules
SwiperCore.use([EffectCoverflow, Pagination]);

@Component({
  selector: 'novanet-post-in-read',
  templateUrl: './post-in-read.component.html',
  styleUrls: ['./post-in-read.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PostInReadComponent {
  @Input() public products: IProductResponse[];
  @Input() public templateConfigurations: ITemplateConfiguration[];
  public swiper: SwiperComponent;
  public thumbsSwiper: any;
  public showBanner: boolean;

  constructor(
    private sanitizer: DomSanitizer,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get template(): ITemplateConfiguration {
    return this.templateConfigurations.find(
      (x) => x.templateType === ETemplateType.PostInRead
    );
  }

  get avatarImage() {
    const backgroundImageUrl = this.template.configurations[
      'avatarUrl'
    ]?.replace('x{0}', 'x800');
    const url =
      environment.host + '/storage/api/v1/stream?name=' + backgroundImageUrl;
    return this.sanitizer.bypassSecurityTrustUrl(
      this.template.configurations['imageLink']
        ? this.template.configurations['imageLink'] ||
            '/assets/images/mobile-advertising/post-in-read-content.svg'
        : backgroundImageUrl?.includes('x800')
        ? url
        : this.template.configurations['imageLink'] ||
          '/assets/images/mobile-advertising/post-in-read-content.svg'
        ? this.template.configurations['imageLink'] ||
          '/assets/images/mobile-advertising/post-in-read-content.svg'
        : '/assets/images/icons/novanet-logo-re.svg'
    );
  }

  public salePercent(data): number {
    return data['SalePrice']
      ? 100 - (+data['SalePrice'] / +data['Price']) * 100 || 0
      : 0;
  }

  public onShowBanner() {
    this.showBanner = !this.showBanner;
  }
}
