import { NgModule } from '@angular/core';
import { CatfishComponent } from './catfish.component';
import { SwiperModule } from 'swiper/angular';
import { CommonModule } from '@angular/common';

const COMPONENTS = [CatfishComponent];

const MODULES = [SwiperModule, CommonModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class CatfishModule {}
