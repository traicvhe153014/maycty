import { Component, Input, ViewEncapsulation } from '@angular/core';
import { IProductResponse } from '@features/product-management/models/product-management-table.model';
import { ITemplateConfiguration } from '@features/campaign-management/store';
import { ETemplateType } from '@features/campaign-management/enums';
import { environment } from '@environment';
import { FeatureType } from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/enums';
import { DomSanitizer } from '@angular/platform-browser';
import {
  extendBannerDefault,
  logoDefault,
} from '@shared/templates/catfish-collab-branding/constants';

@Component({
  selector: 'novanet-popup-banner',
  templateUrl: './popup-banner.component.html',
  styleUrls: ['./popup-banner.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupBannerComponent {
  @Input() public products: IProductResponse[];
  @Input() public templateConfigurations: ITemplateConfiguration[];
  @Input() public featuresType: FeatureType;

  public readonly extendBannerDefault = extendBannerDefault;

  constructor(private sanitizer: DomSanitizer) {}

  get template(): ITemplateConfiguration {
    return this.templateConfigurations.find(
      (x) => x.templateType === ETemplateType.MobileBannerCard
    );
  }

  get extendBannerFileType() {
    let templateNumber = this.templateIndex();
    return (
      this.templateConfigurations[templateNumber].configurations
        .popupExtendBannerFileType ?? 'image'
    );
  }

  get extendBannerImage() {
    let url = '';
    let templateNumber = this.templateIndex();
    const urlDefault = extendBannerDefault;
    if (
      this.templateConfigurations[templateNumber].configurations.extendBanner &&
      typeof this.templateConfigurations[templateNumber].configurations
        .extendBanner === 'string'
    ) {
      url =
        environment.host +
        '/storage/api/v1/stream?name=' +
        this.templateConfigurations[
          templateNumber
        ].configurations.extendBanner.replace('x{0}', 'x800');
    }
    if (
      this.templateConfigurations[templateNumber].configurations
        .imageExtendBannerLink
    ) {
      url =
        this.templateConfigurations[templateNumber].configurations
          .imageExtendBannerLink;
    }
    if (this.extendBannerFileType === 'html') {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url || urlDefault);
    }
    return this.sanitizer.bypassSecurityTrustUrl(url || urlDefault);
  }

  get logoImage() {
    let url = '';
    let templateNumber = this.templateIndex();
    const urlDefault = logoDefault;
    if (
      this.templateConfigurations[templateNumber].configurations.logo &&
      typeof this.templateConfigurations[templateNumber].configurations.logo ===
        'string'
    ) {
      url =
        environment.host +
        '/storage/api/v1/stream?name=' +
        this.templateConfigurations[templateNumber].configurations.logo.replace(
          'x{0}',
          'x800'
        );
    }
    if (
      this.templateConfigurations[templateNumber].configurations.imageLogoLink
    ) {
      url =
        this.templateConfigurations[templateNumber].configurations
          .imageLogoLink;
    }
    return this.sanitizer.bypassSecurityTrustUrl(url || urlDefault);
  }

  get logoName() {
    let logo = 'Tiêu đề';
    let templateNumber = this.templateIndex();
    if (this.templateConfigurations[templateNumber].configurations.title) {
      logo = this.templateConfigurations[templateNumber].configurations.title;
    }
    return logo;
  }

  get description() {
    let description = 'Nội dung';
    let templateNumber = this.templateIndex();
    if (
      this.templateConfigurations[templateNumber].configurations.description
    ) {
      description =
        this.templateConfigurations[templateNumber].configurations.description;
    }
    return description;
  }

  get descriptionCTA() {
    let description = 'Tìm hiểu thêm';
    let templateNumber = this.templateIndex();
    if (this.templateConfigurations[templateNumber].configurations.cta) {
      description =
        this.templateConfigurations[templateNumber].configurations.cta;
    }
    return description;
  }

  get isCTA() {
    let templateNumber = this.templateIndex();
    return this.templateConfigurations[templateNumber].configurations.isCta;
  }

  public templateIndex(): number {
    if (this.featuresType === FeatureType.EDIT) {
      return 0;
    } else {
      return 2;
    }
  }
}
