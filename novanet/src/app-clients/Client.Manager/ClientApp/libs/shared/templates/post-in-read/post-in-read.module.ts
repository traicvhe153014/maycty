import { NgModule } from '@angular/core';
import { PostInReadComponent } from './post-in-read.component';
import { SwiperModule } from 'swiper/angular';
import { CommonModule } from '@angular/common';

const COMPONENTS = [PostInReadComponent];

const MODULES = [SwiperModule, CommonModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class PostInReadModule {}
