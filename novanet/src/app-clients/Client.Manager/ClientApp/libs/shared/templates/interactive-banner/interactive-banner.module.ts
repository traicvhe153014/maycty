import { NgModule } from '@angular/core';
import { InteractiveBannerComponent } from './interactive-banner.component';
import { SwiperModule } from 'swiper/angular';
import { CommonModule } from '@angular/common';

const COMPONENTS = [InteractiveBannerComponent];

const MODULES = [SwiperModule, CommonModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class InteractiveBannerModule {}
