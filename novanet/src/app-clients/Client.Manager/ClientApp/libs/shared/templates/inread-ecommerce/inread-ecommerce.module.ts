import { NgModule } from '@angular/core';
import { InreadEcommerceComponent } from './inread-ecommerce.component';
import { SwiperModule } from 'swiper/angular';
import { CommonModule } from '@angular/common';

const COMPONENTS = [InreadEcommerceComponent];

const MODULES = [SwiperModule, CommonModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class InreadEcommerceModule {}
