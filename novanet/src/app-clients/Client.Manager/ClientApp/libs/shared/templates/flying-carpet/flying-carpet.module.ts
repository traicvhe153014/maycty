import { NgModule } from '@angular/core';
import { FlyingCarpetComponent } from './flying-carpet.component';
import { SwiperModule } from 'swiper/angular';
import { CommonModule } from '@angular/common';

const COMPONENTS = [FlyingCarpetComponent];

const MODULES = [SwiperModule, CommonModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class FlyingCarpetModule {}
