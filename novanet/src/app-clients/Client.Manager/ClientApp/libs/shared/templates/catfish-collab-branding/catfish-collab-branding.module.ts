import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatfishCollabBrandingComponent } from '@shared/templates/catfish-collab-branding/catfish-collab-branding.component';

@NgModule({
  declarations: [CatfishCollabBrandingComponent],
  imports: [CommonModule],
  exports: [CatfishCollabBrandingComponent],
})
export class CatfishCollabBrandingModule {}
