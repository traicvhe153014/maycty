import { Component, Input, ViewEncapsulation } from '@angular/core';
import { IProductResponse } from '@features/product-management/models/product-management-table.model';
import { ITemplateConfiguration } from '@features/campaign-management/store';
import { FeatureType } from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/enums';
import { DomSanitizer } from '@angular/platform-browser';
import SwiperCore, { Autoplay } from 'swiper';
import {
  extendBannerDefault300x200,
  logoDefault,
} from '@shared/templates/catfish-collab-branding/constants';
import { environment } from '@environment';

SwiperCore.use([Autoplay]);
@Component({
  selector: 'novanet-notification-banner-display',
  templateUrl: './notification-banner.component.html',
  styleUrls: ['./notification-banner.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NotificationBannerComponent {
  @Input() public products: IProductResponse[];
  @Input() public templateConfigurations: ITemplateConfiguration[];
  @Input() public featuresType: FeatureType;

  public statusBanner = false;
  public thumbsSwiper: any;
  public listImageDefault = [
    extendBannerDefault300x200,
    extendBannerDefault300x200,
    extendBannerDefault300x200,
  ];

  constructor(private sanitizer: DomSanitizer) {}

  get logoImage() {
    let url = '';
    let templateNumber = this.templateIndex();
    const urlDefault = logoDefault;
    if (
      this.templateConfigurations[templateNumber].configurations.logo &&
      typeof this.templateConfigurations[templateNumber].configurations.logo ===
        'string'
    ) {
      url =
        environment.host +
        '/storage/api/v1/stream?name=' +
        this.templateConfigurations[templateNumber].configurations.logo.replace(
          'x{0}',
          'x800'
        );
    }
    if (
      this.templateConfigurations[templateNumber].configurations
        .logoNotification
    ) {
      url =
        this.templateConfigurations[templateNumber].configurations
          .logoNotification;
    }
    return this.sanitizer.bypassSecurityTrustUrl(url || urlDefault);
  }

  get listBannerNotification(): string[] {
    let listUrl = [];
    let templateNumber = this.templateIndex();
    const urlDefault = this.listImageDefault;
    if (
      this.templateConfigurations[templateNumber].configurations
        .listBannerNotification
    ) {
      listUrl =
        this.templateConfigurations[templateNumber].configurations
          .listBannerNotification;
    }
    return listUrl.length > 0 ? listUrl : urlDefault;
  }

  get titleBanner() {
    let logo = 'Tiêu đề';
    let templateNumber = this.templateIndex();
    if (this.templateConfigurations[templateNumber].configurations.title) {
      logo = this.templateConfigurations[templateNumber].configurations.title;
    }
    return logo;
  }

  get shortNotice() {
    let description = 'Thông báo rút gọn';
    let templateNumber = this.templateIndex();
    if (
      this.templateConfigurations[templateNumber].configurations
        .shortcutNotification
    ) {
      description =
        this.templateConfigurations[templateNumber].configurations
          .shortcutNotification;
    }
    return description;
  }

  get extendNotification() {
    let description = 'Thông báo mở rộng';
    let templateNumber = this.templateIndex();
    if (
      this.templateConfigurations[templateNumber].configurations
        .extendNotification
    ) {
      description =
        this.templateConfigurations[templateNumber].configurations
          .extendNotification;
    }
    return description;
  }

  get descriptionCTA() {
    let description = 'Tư vấn ngay';
    let templateNumber = this.templateIndex();
    if (this.templateConfigurations[templateNumber].configurations.cta) {
      description =
        this.templateConfigurations[templateNumber].configurations.cta;
    }
    return description;
  }

  get isCTA() {
    let templateNumber = this.templateIndex();
    return this.templateConfigurations[templateNumber].configurations.isCta;
  }

  get isCustomImageBanner() {
    let templateNumber = this.templateIndex();
    return this.templateConfigurations[templateNumber].configurations
      .isCustomImage;
  }

  public onChangeStatusBanner() {
    this.statusBanner = true;
  }

  public templateIndex(): number {
    return 0;
  }

  public sanitizerUrl(val: string): any {
    return this.sanitizer.bypassSecurityTrustUrl(val);
  }

  public onClose() {
    if (this.statusBanner) {
      this.statusBanner = false;
    }
  }
}
