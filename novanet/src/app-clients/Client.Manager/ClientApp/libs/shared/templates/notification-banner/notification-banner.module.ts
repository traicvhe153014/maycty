import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationBannerComponent } from '@shared/templates/notification-banner/notification-banner.component';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  declarations: [NotificationBannerComponent],
  imports: [CommonModule, NzCarouselModule, SwiperModule],
  exports: [NotificationBannerComponent],
})
export class NotificationBannerDisplayModule {}
