import { Component, Input, ViewEncapsulation } from '@angular/core';
import { IProductResponse } from '@features/product-management/models/product-management-table.model';
import { ITemplateConfiguration } from '@features/campaign-management/store';
import { FeatureType } from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/enums';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '@environment';
import {
  collapseBannerDefault,
  extendBannerDefault,
  logoDefault,
} from '@shared/templates/catfish-collab-branding/constants';

@Component({
  selector: 'novanet-catfish-collab',
  templateUrl: './catfish-collab-branding.component.html',
  styleUrls: ['./catfish-collab-branding.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CatfishCollabBrandingComponent {
  @Input() public products: IProductResponse[];
  @Input() public templateConfigurations: ITemplateConfiguration[];
  @Input() public featuresType: FeatureType;

  public statusBanner = false;
  public readonly collapseBannerDefault = collapseBannerDefault;
  public readonly extendBannerDefault = extendBannerDefault;

  constructor(private sanitizer: DomSanitizer) {}

  get logoImage() {
    let url = '';
    let templateNumber = this.templateIndex();
    const urlDefault = logoDefault;
    if (
      this.templateConfigurations[templateNumber].configurations.logo &&
      typeof this.templateConfigurations[templateNumber].configurations.logo ===
        'string'
    ) {
      url =
        environment.host +
        '/storage/api/v1/stream?name=' +
        this.templateConfigurations[templateNumber].configurations.logo.replace(
          'x{0}',
          'x800'
        );
    }
    if (this.templateConfigurations[templateNumber].configurations.logoLink) {
      url = this.templateConfigurations[templateNumber].configurations.logoLink;
    }
    return this.sanitizer.bypassSecurityTrustUrl(url || urlDefault);
  }

  get extendBannerFileType() {
    let templateNumber = this.templateIndex();
    return (
      this.templateConfigurations[templateNumber].configurations
        .catfishExtendBannerFileType ?? 'image'
    );
  }

  get extendBannerImage() {
    let url = '';
    let templateNumber = this.templateIndex();
    const urlDefault = extendBannerDefault;
    if (
      this.templateConfigurations[templateNumber].configurations.extendBanner &&
      typeof this.templateConfigurations[templateNumber].configurations
        .extendBanner === 'string'
    ) {
      url =
        environment.host +
        '/storage/api/v1/stream?name=' +
        this.templateConfigurations[
          templateNumber
        ].configurations.extendBanner.replace('x{0}', 'x800');
    }
    if (
      this.templateConfigurations[templateNumber].configurations
        .extendBannerLink
    ) {
      url =
        this.templateConfigurations[templateNumber].configurations
          .extendBannerLink;
    }
    if (this.extendBannerFileType === 'html') {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url || urlDefault);
    }
    return this.sanitizer.bypassSecurityTrustUrl(url || urlDefault);
  }

  get collapseBannerFileType() {
    let templateNumber = this.templateIndex();
    return (
      this.templateConfigurations[templateNumber].configurations
        .catfishCollapseBannerFileType ?? 'image'
    );
  }

  get collapseBannerImage() {
    let url = '';
    let templateNumber = this.templateIndex();
    const urlDefault = collapseBannerDefault;
    if (
      this.templateConfigurations[templateNumber].configurations
        .collapseBanner &&
      typeof this.templateConfigurations[templateNumber].configurations
        .collapseBanner === 'string'
    ) {
      url =
        environment.host +
        '/storage/api/v1/stream?name=' +
        this.templateConfigurations[
          templateNumber
        ].configurations.collapseBanner.replace('x{0}', 'x800');
    }
    if (
      this.templateConfigurations[templateNumber].configurations
        .collapseBannerLink
    ) {
      url =
        this.templateConfigurations[templateNumber].configurations
          .collapseBannerLink;
    }
    if (this.collapseBannerFileType === 'html') {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url || urlDefault);
    }
    return this.sanitizer.bypassSecurityTrustUrl(url || urlDefault);
  }

  get logoName() {
    let logo = 'Tiêu đề';
    let templateNumber = this.templateIndex();
    if (this.templateConfigurations[templateNumber].configurations.title) {
      logo = this.templateConfigurations[templateNumber].configurations.title;
    }
    return logo;
  }

  get description() {
    let description = 'Nội dung';
    let templateNumber = this.templateIndex();
    if (
      this.templateConfigurations[templateNumber].configurations.description
    ) {
      description =
        this.templateConfigurations[templateNumber].configurations.description;
    }
    return description;
  }

  get descriptionCTA() {
    let description = 'Tư vấn ngay';
    let templateNumber = this.templateIndex();
    if (this.templateConfigurations[templateNumber].configurations.cta) {
      description =
        this.templateConfigurations[templateNumber].configurations.cta;
    }
    return description;
  }

  get isCTA() {
    let templateNumber = this.templateIndex();
    return this.templateConfigurations[templateNumber].configurations.isCta;
  }

  public onChangeStatusBanner() {
    this.statusBanner = !this.statusBanner;
  }

  public templateIndex(): number {
    if (this.featuresType === FeatureType.EDIT) {
      return 0;
    } else {
      return 1;
    }
  }
}
