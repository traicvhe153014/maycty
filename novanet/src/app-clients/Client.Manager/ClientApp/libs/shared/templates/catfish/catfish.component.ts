import { Component, Input, ViewEncapsulation } from '@angular/core';
import { SwiperComponent } from 'swiper/angular';

// import Swiper core and required modules
import SwiperCore, { FreeMode, Navigation, Thumbs } from 'swiper';
import { IProductResponse } from '@features/product-management/models/product-management-table.model';
import { ITemplateConfiguration } from '@features/campaign-management/store';
import { ETemplateType } from '@features/campaign-management/enums';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '@environment';

// install Swiper modules
SwiperCore.use([FreeMode, Navigation, Thumbs]);

@Component({
  selector: 'novanet-catfish',
  templateUrl: './catfish.component.html',
  styleUrls: ['./catfish.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CatfishComponent {
  @Input() public products: IProductResponse[];
  @Input() public templateConfigurations: ITemplateConfiguration[];
  public swiper: SwiperComponent;
  public thumbsSwiper: any;
  public showBanner: boolean;

  constructor(private sanitizer: DomSanitizer) {}

  get template(): ITemplateConfiguration {
    return this.templateConfigurations.find(
      (x) => x.templateType === ETemplateType.CatfishEcom
    );
  }

  get backgroundImage() {
    const backgroundImageUrl = this.template.configurations[
      'backgroundImageUrl'
    ]?.replace('x{0}', 'x800');

    const url =
      environment.host + '/storage/api/v1/stream?name=' + backgroundImageUrl;
    return this.sanitizer.bypassSecurityTrustUrl(
      this.template.configurations['imageLink']
        ? this.template.configurations['imageLink'] ||
            '/assets/templates/catfish-ecom/default-images/footer-image.png'
        : backgroundImageUrl?.includes('x800')
        ? url
        : this.template.configurations['imageLink'] ||
          '/assets/templates/catfish-ecom/default-images/footer-image.png'
    );
  }

  get expandImage() {
    const expandedContentUrl = this.template.configurations[
      'expandedContentUrl'
    ]?.replace('x{0}', 'x800');
    const url =
      environment.host + '/storage/api/v1/stream?name=' + expandedContentUrl;
    return this.sanitizer.bypassSecurityTrustUrl(
      expandedContentUrl?.includes('x800')
        ? url
        : this.template.configurations['expandedContentUrl'] ||
            '/assets/templates/catfish-ecom/default-images/expand-image.png'
    );
  }

  public salePercent(data): number {
    return data['SalePrice']
      ? 100 - (+data['SalePrice'] / +data['Price']) * 100 || 0
      : 0;
  }

  public onShowBanner() {
    this.showBanner = !this.showBanner;
  }
}
