import { NgModule } from '@angular/core';
import { MobileBannerComponent } from './mobile-banner.component';
import { SwiperModule } from 'swiper/angular';
import { CommonModule } from '@angular/common';

const COMPONENTS = [MobileBannerComponent];

const MODULES = [SwiperModule, CommonModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class MobileBannerModule {}
