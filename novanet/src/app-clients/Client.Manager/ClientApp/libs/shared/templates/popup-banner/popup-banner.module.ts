import { NgModule } from '@angular/core';
import { PopupBannerComponent } from '@shared/templates/popup-banner/popup-banner.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [PopupBannerComponent],
  imports: [CommonModule],
  exports: [PopupBannerComponent],
})
export class PopupBannerModule {}
