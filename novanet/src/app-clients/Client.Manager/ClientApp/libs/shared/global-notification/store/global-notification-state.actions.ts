import { GlobalNotificationEnum } from '../enums';
import { ICampaignListRequest } from '@features/campaign-management/store';
import { IHttpGetRequest } from '@core/models';
import { IGlobalUploadNotificationModel } from '@shared/global-notification';

export enum ExportApi {
  Campaign = 1,
  Product,
}

export class ShowGlobalNotification {
  public static readonly type: string = '[Notification] ShowGlobalNotification';

  constructor(
    public type: GlobalNotificationEnum,
    public label?: string,
    public description?: string,
    public duration?: number,
    public status?: number
  ) {}
}

export class UpdateIsReportDownloading {
  public static readonly type: string =
    '[Notification] Update isReportDownloading';

  constructor(public value: boolean) {}
}

export class UpdateisNotificationDownloading {
  public static readonly type: string =
    '[Notification] Update isNotificationDownloading';

  constructor(public value: boolean) {}
}

export class CancelDownloading {
  public static readonly type: string = '[Notification] CancelDownloading';

  constructor() {}
}

export class ExportReport {
  public static readonly type: string = '[Notification] ExportReport';

  constructor(public payload: ICampaignListRequest) {}
}

export class ExportProductReport {
  public static readonly type: string = '[Notification] ExportProductReport';

  constructor(public payload: IHttpGetRequest) {}
}

export class ShowGlobalUploadNotification {
  public static readonly type: string = '[Notification] GlobalUpload';

  constructor(public payload: IGlobalUploadNotificationModel) {}
}

export class AddUploadProcessingVideo {
  public static readonly type: string =
    '[Notification] AddUploadProcessingVideo';

  constructor(public payload: IGlobalUploadNotificationModel) {}
}

export class RemoveUploadProcessingVideo {
  public static readonly type: string =
    '[Notification] RemoveUploadProcessingVideo';

  constructor(public videoId: string) {}
}
