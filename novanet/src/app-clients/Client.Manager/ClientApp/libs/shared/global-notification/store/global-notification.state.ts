import {
  GlobalNotificationEnum,
  StatusNotificationEnum,
} from '@shared/global-notification/enums';
import { catchError } from 'rxjs/operators';
import { ICampaignListRequest } from '@features/campaign-management/store/campaign/campaign-state.model';
import { Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';

import {
  AddUploadProcessingVideo,
  CancelDownloading,
  ExportProductReport,
  ExportReport,
  RemoveUploadProcessingVideo,
  ShowGlobalNotification,
  ShowGlobalUploadNotification,
  UpdateisNotificationDownloading,
  UpdateIsReportDownloading,
} from './global-notification-state.actions';
import { GlobalNotificationStateModel } from './global-notification-state.model';
import {
  GlobalNotificationModel,
  IGlobalUploadNotificationModel,
} from '../models/global-notification.model';
import { CampaignService } from '@features/campaign-management/store';
import { ProductManagementService } from '@features/product-management/store';

@Injectable()
@State<GlobalNotificationStateModel>({
  name: 'globalNotification',
  defaults: {
    notification: null,
    isReportDownloading: false,
    isNotificationDownloading: false,
    cancelDownload: false,
    lastReportPayload: null,
    uploadNotification: undefined,
    processingVideoIds: [],
  },
})
export class GlobalNotificationState {
  private exportProductReportSubscription: Subscription;
  private exportReportSubscription: Subscription;

  constructor(
    private campaignService: CampaignService,
    private productManagementService: ProductManagementService
  ) {}

  @Selector()
  public static getGlobalNotificationState(
    state: GlobalNotificationStateModel
  ): GlobalNotificationModel {
    return state.notification;
  }

  @Selector()
  public static getGlobalUploadNotificationState(
    state: GlobalNotificationStateModel
  ): IGlobalUploadNotificationModel {
    return state.uploadNotification;
  }

  @Selector()
  public static getProcessingVideoIds(
    state: GlobalNotificationStateModel
  ): IGlobalUploadNotificationModel[] {
    return state.processingVideoIds;
  }

  @Selector()
  public static isReportDownloading(
    state: GlobalNotificationStateModel
  ): boolean {
    return state.isReportDownloading;
  }

  @Selector()
  public static isNotificationDownloading(
    state: GlobalNotificationStateModel
  ): boolean {
    return state.isNotificationDownloading;
  }

  @Selector()
  public static getCancelDownloading(
    state: GlobalNotificationStateModel
  ): boolean {
    return state.cancelDownload;
  }

  @Selector()
  public static getLastReportPayload(
    state: GlobalNotificationStateModel
  ): ICampaignListRequest {
    return state.lastReportPayload;
  }

  @Action(ShowGlobalNotification)
  public showGlobalNotification(
    ctx: StateContext<GlobalNotificationStateModel>,
    { type, label, description, duration, status }: ShowGlobalNotification
  ): void {
    const notification = {
      type,
      label,
      description,
      duration,
      status,
    } as GlobalNotificationModel;

    ctx.patchState({ notification });
  }

  @Action(UpdateIsReportDownloading)
  public updateIsReportDownloading(
    ctx: StateContext<GlobalNotificationStateModel>,
    { value }: UpdateIsReportDownloading
  ): void {
    ctx.patchState({
      isReportDownloading: value,
    });
  }

  @Action(UpdateisNotificationDownloading)
  public updateisNotificationDownloading(
    ctx: StateContext<GlobalNotificationStateModel>,
    { value }: UpdateisNotificationDownloading
  ): void {
    ctx.patchState({
      isNotificationDownloading: value,
    });
  }

  @Action(CancelDownloading)
  public cancelDownloading(ctx: StateContext<GlobalNotificationStateModel>) {
    if (this.exportReportSubscription) {
      this.exportReportSubscription.unsubscribe();
    }
    if (this.exportProductReportSubscription) {
      this.exportProductReportSubscription.unsubscribe();
    }
  }

  @Action(ExportReport)
  public exportReport(
    ctx: StateContext<GlobalNotificationStateModel>,
    { payload }: ExportReport
  ) {
    ctx.patchState({
      lastReportPayload: payload,
    });
    this.exportReportSubscription = this.campaignService
      .exportExcel(payload)
      .pipe(
        catchError(() => {
          this.downloadFail(ctx);
          return null;
        })
      )
      .subscribe((response) => {
        this.downloadSuccess(ctx, response);
      });
  }

  @Action(ExportProductReport)
  public exportProductReport(
    ctx: StateContext<GlobalNotificationStateModel>,
    { payload }: ExportProductReport
  ) {
    ctx.patchState({
      lastReportPayload: payload,
    });
    this.exportProductReportSubscription = this.productManagementService
      .exportExcel(payload)
      .pipe(
        catchError(() => {
          this.downloadFail(ctx);
          return null;
        })
      )
      .subscribe((response) => {
        this.downloadSuccess(ctx, response);
      });
  }

  @Action(ShowGlobalUploadNotification)
  public showGlobalUploadNotification(
    ctx: StateContext<GlobalNotificationStateModel>,
    { payload }: ShowGlobalUploadNotification
  ): void {
    ctx.patchState({ uploadNotification: payload });
  }

  @Action(AddUploadProcessingVideo)
  public addUploadProcessingVideo(
    ctx: StateContext<GlobalNotificationStateModel>,
    { payload }: AddUploadProcessingVideo
  ): void {
    const { processingVideoIds } = ctx.getState();
    if (processingVideoIds.every((item) => item.videoId !== payload.videoId)) {
      ctx.patchState({
        processingVideoIds: [...processingVideoIds, payload],
      });
    }
  }

  @Action(RemoveUploadProcessingVideo)
  public removeUploadProcessingVideo(
    ctx: StateContext<GlobalNotificationStateModel>,
    { videoId }: RemoveUploadProcessingVideo
  ): void {
    const { processingVideoIds } = ctx.getState();
    ctx.patchState({
      processingVideoIds: processingVideoIds.filter(
        (item) => item.videoId !== videoId
      ),
    });
  }

  private downloadFail(ctx: StateContext<GlobalNotificationStateModel>) {
    ctx.dispatch(new UpdateIsReportDownloading(false));
    ctx.dispatch(new UpdateisNotificationDownloading(true));
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.resultdownload,
        `Tải xuống thất bại`,
        ``,
        10000,
        StatusNotificationEnum.error
      )
    );
  }

  private downloadSuccess(
    ctx: StateContext<GlobalNotificationStateModel>,
    response
  ) {
    const byteCharacters = atob(response.responseContent);

    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    const file = new Blob([byteArray], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    const fileUrl = window.URL.createObjectURL(file);

    // create <a> tag dinamically
    const fileLink = document.createElement('a');
    fileLink.href = fileUrl;

    fileLink.download = response.fileName;

    fileLink.click();

    ctx.dispatch(new UpdateIsReportDownloading(false));
    ctx.dispatch(new UpdateisNotificationDownloading(true));
  }
}
