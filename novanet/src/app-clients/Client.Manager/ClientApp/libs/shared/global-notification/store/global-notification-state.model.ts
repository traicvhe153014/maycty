import {
  GlobalNotificationModel,
  IGlobalUploadNotificationModel,
} from '../models';

export interface GlobalNotificationStateModel {
  notification: GlobalNotificationModel;
  isReportDownloading: boolean;
  isNotificationDownloading: boolean;
  cancelDownload: boolean;
  lastReportPayload: any;
  uploadNotification?: IGlobalUploadNotificationModel;
  processingVideoIds: IGlobalUploadNotificationModel[];
}
