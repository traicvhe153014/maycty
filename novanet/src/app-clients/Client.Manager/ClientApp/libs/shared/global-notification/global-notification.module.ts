import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GlobalNotificationStateModule } from './store';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { GlobalNotificationComponentsModule } from './components';

const COMPONENTS = [];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule,
    GlobalNotificationStateModule,
    NzSpinModule,
    GlobalNotificationComponentsModule,
  ],
})
export class GlobalNotificationModule {}
