import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '@environment';

@Pipe({
  name: 'videoProgressIframeUrl',
})
export class VideoProgressIframeUrlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(videoId: string): any {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `${environment.streamingRootUrl}/progress/${videoId}`
    );
  }
}
