export enum GlobalNotificationEnum {
  success = 0,
  info,
  warning,
  error,
  download,
  resultdownload,
}

export enum StatusNotificationEnum {
  success = 0,
  error,
}

export enum EUploadNotificationStep {
  Upload,
  Process,
  Finish,
}
