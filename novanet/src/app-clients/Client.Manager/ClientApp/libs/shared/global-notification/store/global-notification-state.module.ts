import { NgModule } from '@angular/core';
import { CampaignService } from '@features/campaign-management/store';
import { NgxsModule } from '@ngxs/store';

import { GlobalNotificationState } from './global-notification.state';
import { ProductManagementService } from '@features/product-management/store';

@NgModule({
  imports: [NgxsModule.forFeature([GlobalNotificationState])],
  providers: [CampaignService, ProductManagementService],
})
export class GlobalNotificationStateModule {}
