import {
  EUploadNotificationStep,
  GlobalNotificationEnum,
  StatusNotificationEnum,
} from '../enums';

export interface GlobalNotificationModel {
  type: GlobalNotificationEnum;
  label?: string;
  description?: string;
  duration?: number;
  status?: StatusNotificationEnum;
}

export interface IGlobalUploadNotificationModel {
  step: EUploadNotificationStep;
  percentage: number;
  processId: string;
  fileName: string;
  videoId?: string;
}

export const uploadingVideosKey = 'UploadingVideos';
