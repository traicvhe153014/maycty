import {
  ChangeDetectorRef,
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { environment } from '@environment';
import { EUploadNotificationStep } from './../../enums/global-notification.enum';
import {
  IGlobalUploadNotificationModel,
  uploadingVideosKey,
} from './../../models/global-notification.model';
import {
  AddUploadProcessingVideo,
  GlobalNotificationState,
  RemoveUploadProcessingVideo,
} from '@shared/global-notification/store';
import { Observable, Subject } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'novanet-global-upload-notification',
  templateUrl: './global-upload-notification.component.html',
})
export class GlobalUploadNotificationComponent implements OnDestroy, OnInit {
  @ViewChild('notificationRef', { static: false }) template?: TemplateRef<any>;
  @ViewChild('emptyCloseBtn', { static: false })
  emptyCloseBtnRef?: TemplateRef<any>;
  public readonly eUploadNotificationStep = EUploadNotificationStep;
  public readonly environment = environment;
  @Select(GlobalNotificationState.getProcessingVideoIds)
  public processingVideos$: Observable<IGlobalUploadNotificationModel[]>;
  @Select(GlobalNotificationState.getGlobalUploadNotificationState)
  private pushNotification$: Observable<IGlobalUploadNotificationModel>;
  private readonly destroy$: Subject<void> = new Subject<void>();

  private processingVideos: IGlobalUploadNotificationModel[] = [];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private notificationService: NzNotificationService,
    private store: Store
  ) {}

  private get getStorageUploadingVideos(): IGlobalUploadNotificationModel[] {
    const uploadingVideosStr = localStorage.getItem(uploadingVideosKey);
    let uploadingVideos: IGlobalUploadNotificationModel[] = [];
    if (uploadingVideosStr) {
      uploadingVideos = JSON.parse(
        uploadingVideosStr
      ) as IGlobalUploadNotificationModel[];
    }
    return uploadingVideos;
  }

  public ngOnInit(): void {
    this.processingVideos$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (data) => {
        this.processingVideos = data;
      },
    });
    this.pushNotification$.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if (data) {
        this.notificationService.template(this.template!, {
          nzData: data,
          nzKey: data.processId,
          nzDuration: 0,
          nzPlacement: 'bottomRight',
          nzCloseIcon: this.emptyCloseBtnRef,
        });
        this.changeDetectorRef.markForCheck();
      }
    });
    const uploadingVideos = this.getStorageUploadingVideos;
    for (const uploadingVideo of uploadingVideos) {
      this.store.dispatch(new AddUploadProcessingVideo(uploadingVideo));
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public closeNotification(processId: string) {
    this.notificationService.remove(processId);
  }

  @HostListener('window:message', ['$event'])
  public onProgressIFrameMessage(event: Event) {
    const eventData = (event as MessageEvent).data;
    if (!eventData) {
      return;
    }
    const { percentage, message, videoId } = eventData;
    if (message !== 'videoProgress' && message !== 'videoFinish') {
      return;
    }
    const video = this.processingVideos.find(
      (item) => item.videoId === videoId
    );
    if (!video) {
      return;
    }

    if (message === 'videoFinish') {
      this.notificationService.remove(video.processId);
      this.store.dispatch(new RemoveUploadProcessingVideo(video.videoId));
      let uploadingVideos = this.getStorageUploadingVideos;
      uploadingVideos = uploadingVideos.filter(
        (item) => item.videoId !== video.videoId
      );
      localStorage.setItem(uploadingVideosKey, JSON.stringify(uploadingVideos));
      return;
    }

    if (percentage !== 100) {
      this.notificationService.template(this.template!, {
        nzData: {
          videoId: video.videoId,
          processId: video.processId,
          percentage,
          step: EUploadNotificationStep.Process,
          fileName: video.fileName,
        } as IGlobalUploadNotificationModel,
        nzKey: video.processId,
        nzDuration: 0,
        nzPlacement: 'bottomRight',
        nzCloseIcon: this.emptyCloseBtnRef,
      });
      return;
    }

    this.notificationService.template(this.template!, {
      nzData: {
        videoId: video.videoId,
        processId: video.processId,
        percentage: 0,
        step: EUploadNotificationStep.Finish,
        fileName: video.fileName,
      },
      nzKey: video.processId,
      nzDuration: 3000,
      nzPlacement: 'bottomRight',
    });
    setTimeout(() => {
      this.notificationService.remove(video.processId);
    }, 3000);
    this.store.dispatch(new RemoveUploadProcessingVideo(video.videoId));

    let uploadingVideos = this.getStorageUploadingVideos;
    uploadingVideos = uploadingVideos.filter(
      (item) => item.videoId !== video.videoId
    );
    localStorage.setItem(uploadingVideosKey, JSON.stringify(uploadingVideos));
  }

  @HostListener('window:beforeunload', ['$event'])
  private beforeUnloadHandler(event: Event) {
    const hasUploading = this.processingVideos.some(
      (x) => x.step === EUploadNotificationStep.Upload
    );
    if (hasUploading) {
      confirm('Are you sure you want to discard your changes? hihi');
      event.preventDefault();
      event.returnValue =
        'Video đang được tải lên. Rời khỏi trang web bây giờ có thể khiến cho tin quảng cáo bị lỗi. Bạn có muốn tiếp tục?' as any;
    }
    // event.returnValue = false;
    return !hasUploading;
  }
}
