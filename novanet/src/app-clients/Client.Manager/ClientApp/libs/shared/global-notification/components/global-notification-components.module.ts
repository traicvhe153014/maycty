import { NgModule } from '@angular/core';
import { GlobalNotificationComponent } from './global-notification';
import { CommonModule } from '@angular/common';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { GlobalUploadNotificationComponent } from './global-upload-notification';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { VideoProgressIframeUrlPipe } from '@shared/global-notification/pipes/video-progress-iframe-url.pipe';

const COMPONENTS = [
  GlobalNotificationComponent,
  GlobalUploadNotificationComponent,
  VideoProgressIframeUrlPipe,
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [CommonModule, SvgIconModule, NzModalModule, NzProgressModule],
  exports: [COMPONENTS],
})
export class GlobalNotificationComponentsModule {}
