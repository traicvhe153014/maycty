import {
  CancelDownloading,
  ExportApi,
  ExportProductReport,
  ShowGlobalNotification,
  UpdateisNotificationDownloading,
  UpdateIsReportDownloading,
} from './../../store/global-notification-state.actions';
import { StatusNotificationEnum } from './../../enums/global-notification.enum';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, take } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import {
  ExportReport,
  GlobalNotificationState,
} from '@shared/global-notification/store';
import { GlobalNotificationModel } from '@shared/global-notification/models';
import { GlobalNotificationEnum } from '@shared/global-notification/enums';

@Component({
  selector: 'novanet-global-notification',
  templateUrl: './global-notification.component.html',
  styleUrls: ['./global-notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalNotificationComponent implements OnDestroy, OnInit {
  @ViewChild(TemplateRef, { static: false }) template?: TemplateRef<any>;

  public globalNotificationEnum = GlobalNotificationEnum;
  public isVisibleMiddle = false;
  public idNotification: string;
  public statusDownload = StatusNotificationEnum;
  public durationNotificationCancel = 2000;
  @Select(GlobalNotificationState.isReportDownloading)
  isReportDownloading$: Observable<boolean>;
  @Select(GlobalNotificationState.isNotificationDownloading)
  isNotificationDownloading$: Observable<boolean>;
  @Select(GlobalNotificationState.getCancelDownloading)
  cancelDownloading$: Observable<boolean>;
  @Select(GlobalNotificationState.getLastReportPayload)
  getLastReportPayload$: Observable<any>;
  @Select(GlobalNotificationState.getGlobalNotificationState)
  private pushNotification$: Observable<GlobalNotificationModel>;
  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private notificationService: NzNotificationService,
    private store: Store
  ) {}

  public ngOnInit(): void {
    this.pushNotification$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: GlobalNotificationModel) => {
        if (data) {
          const id = this.notificationService.template(this.template!, {
            nzData: data,
            nzDuration:
              data.type === GlobalNotificationEnum.download ||
              data.type === GlobalNotificationEnum.resultdownload
                ? data.duration
                : 2000,
            nzPlacement:
              data.type === GlobalNotificationEnum.download ||
              data.type === GlobalNotificationEnum.resultdownload
                ? 'bottomRight'
                : 'topRight',
            nzCloseIcon:
              data.type === GlobalNotificationEnum.download ? 'none' : 'close',
            nzClass:
              data.type === GlobalNotificationEnum.download ||
              data.type === GlobalNotificationEnum.resultdownload
                ? 'download'
                : '',
          });
          this.changeDetectorRef.markForCheck();
          this.idNotification = id.messageId;
        }
      });
    this.statusNotificationDownloading();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public showCancelModal(): void {
    this.isVisibleMiddle = true;
  }

  public handleCancelModal(): void {
    this.isVisibleMiddle = false;
  }

  public handleOkModal(): void {
    this.isVisibleMiddle = false;
    this.notificationService.remove(this.idNotification);
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.resultdownload,
        `Đã huỷ tải xuống`,
        ``,
        this.durationNotificationCancel,
        StatusNotificationEnum.error
      )
    );
    this.store.dispatch(new UpdateIsReportDownloading(false));
    this.store.dispatch(new CancelDownloading());
  }

  public statusNotificationDownloading() {
    this.isNotificationDownloading$
      .pipe(takeUntil(this.destroy$))
      .subscribe((isNotificationDownloading) => {
        if (isNotificationDownloading) {
          this.notificationService.remove(this.idNotification);
          this.store.dispatch(new UpdateisNotificationDownloading(false));
        }
      });
  }

  public downloadAgain() {
    this.getLastReportPayload$.pipe(take(1)).subscribe((data) => {
      this.notificationService.remove(this.idNotification);
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.download,
          `Đang tải xuống báo cáo`,
          `Quá trình tải xuống có thể mất nhiều thời gian.
              Vui lòng không reload lại trang và duy trì mạng ổn định.`
        )
      );

      this.store.dispatch(new UpdateIsReportDownloading(true));
      switch (data.exportApi) {
        case ExportApi.Product:
          this.store.dispatch(new ExportProductReport(data));
          break;
        default:
          this.store.dispatch(new ExportReport(data));
          break;
      }
    });
  }
}
