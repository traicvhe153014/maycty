import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';

const CommonUrls = {
  hasAnyProductFeed: `ProductFeed/any`,
};

@Injectable()
export class CommonService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public hasAnyProductFeed(): Observable<boolean> {
    return this.httpClient
      .get<ISuccessHttpResponse<boolean>>(
        this.createUrl(CommonUrls.hasAnyProductFeed)
      )
      .pipe(map((response) => response.data));
  }
}
