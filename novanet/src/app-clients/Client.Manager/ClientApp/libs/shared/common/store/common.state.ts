import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  CheckHasAnyProductFeed,
  CheckHasAnyProductFeedError,
  CheckHasAnyProductFeedSuccess,
} from './common-state.action';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ICommonStateModel } from './common-state.model';
import { CommonService } from './common-state.service';

@Injectable()
@State<ICommonStateModel>({
  name: 'common',
  defaults: {
    isCampaignEnabled: undefined,
  },
})
export class CommonState {
  constructor(private commonService: CommonService) {}

  /**
   * Selectors
   */
  @Selector()
  public static isCampaignEnabled(state: ICommonStateModel): boolean {
    return state.isCampaignEnabled;
  }

  /**
   * Actions
   */
  @Action(CheckHasAnyProductFeed)
  public checkHasAnyProductFeed(ctx: StateContext<ICommonStateModel>) {
    return this.commonService.hasAnyProductFeed().pipe(
      map((response) =>
        ctx.dispatch(new CheckHasAnyProductFeedSuccess(response))
      ),
      catchError((error) =>
        ctx.dispatch(new CheckHasAnyProductFeedError(error))
      )
    );
  }

  @Action(CheckHasAnyProductFeedSuccess)
  public checkHasAnyProductFeedSuccess(
    ctx: StateContext<ICommonStateModel>,
    { response }: CheckHasAnyProductFeedSuccess
  ) {
    ctx.patchState({ isCampaignEnabled: response });
  }

  @Action(CheckHasAnyProductFeedError)
  public checkHasAnyProductFeedError(
    ctx: StateContext<ICommonStateModel>,
    { error }: CheckHasAnyProductFeedError
  ) {
    return throwError(error);
  }
}
