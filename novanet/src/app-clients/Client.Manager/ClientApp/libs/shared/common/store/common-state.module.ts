import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CommonService } from './common-state.service';
import { CommonState } from './common.state';

@NgModule({
  imports: [NgxsModule.forFeature([CommonState])],
  providers: [CommonService],
})
export class CommonStateModule {}
