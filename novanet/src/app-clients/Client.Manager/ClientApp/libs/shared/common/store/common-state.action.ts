const enum CommonActions {
  CheckHasAnyProductFeed = '[Common] Has Any Product Feed',
  CheckHasAnyProductFeedSuccess = '[Common] Has Any Product Feed Success',
  CheckHasAnyProductFeedError = '[Common] Has Any Product Feed Error',
}

export class CheckHasAnyProductFeed {
  public static readonly type = CommonActions.CheckHasAnyProductFeed;

  constructor() {}
}

export class CheckHasAnyProductFeedSuccess {
  public static readonly type = CommonActions.CheckHasAnyProductFeedSuccess;

  constructor(public response: boolean) {}
}

export class CheckHasAnyProductFeedError {
  public static readonly type = CommonActions.CheckHasAnyProductFeedError;

  constructor(public error: any) {}
}
