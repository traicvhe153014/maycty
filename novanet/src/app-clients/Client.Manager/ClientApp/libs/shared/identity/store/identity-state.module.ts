import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { IdentityService } from './identity-state.service';
import { IdentityState } from './identity.state';

@NgModule({
  imports: [NgxsModule.forFeature([IdentityState])],
  providers: [IdentityService],
})
export class IdentityStateModule {}
