import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import {
  IdentityService,
  IIdentityStateModel,
  IUserResponse,
} from '@shared/identity';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {
  GetMe,
  GetMeSuccess,
  GetUser,
  GetUserError,
  GetUserSuccess,
  UpdateAmount,
} from '@shared/identity/store/identity-state.actions';
import { userBudgetWarningAmount } from '@shared/identity/constants';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';

@Injectable()
@State<IIdentityStateModel>({
  name: 'identity',
  defaults: {
    currentUser: undefined,
  },
})
export class IdentityState {
  constructor(private identityService: IdentityService, private store: Store) {}

  /**
   * Selectors
   */
  @Selector()
  public static getCurrentUser(
    state: IIdentityStateModel
  ): IUserResponse | undefined {
    return state.currentUser;
  }

  /**
   * Actions
   */
  @Action(GetUser)
  public getUser(ctx: StateContext<IIdentityStateModel>, { email }: GetUser) {
    return this.identityService.getUser(email).pipe(
      map((response) => ctx.dispatch(new GetUserSuccess(response))),
      catchError((error) => ctx.dispatch(new GetUserError(error)))
    );
  }

  @Action(GetUserSuccess)
  public getUserSuccess(
    ctx: StateContext<IIdentityStateModel>,
    { response }: GetUserSuccess
  ) {
    ctx.patchState({
      currentUser: response,
    });
    if (response?.amount < userBudgetWarningAmount) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Tài khoản còn dưới 100,000 VNĐ bạn vui lòng nạp thêm tiền'
        )
      );
    }
  }

  @Action(GetMe)
  public getMe(ctx: StateContext<IIdentityStateModel>) {
    return this.identityService.getMe().pipe(
      map((response) => ctx.dispatch(new GetMeSuccess(response))),
      catchError((error) => ctx.dispatch(new GetUserError(error)))
    );
  }

  @Action(GetMeSuccess)
  public getMeSuccess(
    ctx: StateContext<IIdentityStateModel>,
    { response }: GetUserSuccess
  ) {
    ctx.patchState({
      currentUser: response,
    });
  }

  @Action(GetUserError)
  public getUserError(
    ctx: StateContext<IIdentityStateModel>,
    { error }: GetUserError
  ) {
    return throwError(error);
  }

  @Action(UpdateAmount)
  public updateAmount(
    ctx: StateContext<IIdentityStateModel>,
    { amount, email }: UpdateAmount
  ) {
    const currentUser = ctx.getState().currentUser;
    if (!currentUser || currentUser.email !== email) {
      return;
    }
    ctx.patchState({
      currentUser: {
        ...currentUser,
        amount: currentUser?.amount + amount,
      },
    });
  }
}
