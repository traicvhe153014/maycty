import { IUserResponse } from '@shared/identity';

const enum IdentityActions {
  GetUser = '[Identity] Get User',
  GetMe = '[Identity] Get Me',
  GetUserSuccess = '[Identity] Get User Success',
  GetMeSuccess = '[Identity] Get Me Success',
  GetUserError = '[Identity] Get User Error',
  UpdateAmount = '[Identity] Update Amount',
}

export class GetMe {
  public static readonly type = IdentityActions.GetMe;

  constructor() {}
}

export class GetMeSuccess {
  public static readonly type = IdentityActions.GetMeSuccess;

  constructor(public response: IUserResponse) {}
}

export class GetUser {
  public static readonly type = IdentityActions.GetUser;

  constructor(public email: string) {}
}

export class GetUserSuccess {
  public static readonly type = IdentityActions.GetUserSuccess;

  constructor(public response: IUserResponse) {}
}

export class GetUserError {
  public static readonly type = IdentityActions.GetUserError;

  constructor(public error: any) {}
}

export class UpdateAmount {
  public static readonly type = IdentityActions.UpdateAmount;

  constructor(public amount: number, public email: string) {}
}
