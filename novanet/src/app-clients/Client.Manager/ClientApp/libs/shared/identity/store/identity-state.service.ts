import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import { IListUserRequest, IUserResponse } from '@shared/identity';

const IdentityUrls = {
  listUser: `list`,
  getUser: `get`,
  getMe: `me`,
};

@Injectable()
export class IdentityService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'identity/api/v1');
  }

  public listUser(params: IListUserRequest): Observable<IUserResponse[]> {
    const httpParams = new HttpParams({
      fromObject: {
        ...params,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IUserResponse[]>>(
        this.createUrl(IdentityUrls.listUser),
        { params: httpParams }
      )
      .pipe(map((response) => response.data));
  }

  public getUser(email: string): Observable<IUserResponse> {
    const httpParams = new HttpParams({
      fromObject: {
        email,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IUserResponse>>(
        this.createUrl(IdentityUrls.getUser),
        { params: httpParams }
      )
      .pipe(map((response) => response.data));
  }

  public getMe(): Observable<IUserResponse> {
    return this.httpClient
      .get<ISuccessHttpResponse<IUserResponse>>(
        this.createUrl(IdentityUrls.getMe)
      )
      .pipe(map((response) => response.data));
  }
}
