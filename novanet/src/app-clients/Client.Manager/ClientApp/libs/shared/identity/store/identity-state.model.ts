import { PredicateOperatorEnum } from '@core/enums';

export interface IListUserRequest {
  page: number;
  pageSize: number;
  keyword?: string;
  queryField?: string;
  operator: PredicateOperatorEnum;
  isAccountantIgnored?: boolean;
}

export interface IUserResponse {
  id: string;
  email: string;
  fullName: string;
  userName: string;
  amount?: number;
  subId: number;
}

export interface IIdentityStateModel {
  currentUser?: IUserResponse;
}
