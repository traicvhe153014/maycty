import { Component, Input } from '@angular/core';
import { IStatusBlockConfig } from '@core/models';

@Component({
  selector: 'novanet-status-block',
  template: `
    <span
      *ngIf="!!currentConfig?.tooltip"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[{{
        currentConfig?.textColor
      }}] bg-[{{ currentConfig?.backgroundColor }}]"
      nz-tooltip
      nzTooltipColor="#001F4C"
      nzTooltipOverlayClassName="text-[10px] campaign-list-tooltip"
      [nzTooltipTitle]="currentConfig?.tooltip">
      {{ currentConfig?.text }}
    </span>
    <span
      *ngIf="!currentConfig?.tooltip"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[{{
        currentConfig?.textColor
      }}] bg-[{{ currentConfig?.backgroundColor }}]">
      {{ currentConfig?.text }}
    </span>
  `,
})
export class StatusBlockComponent<T> {
  @Input() public statusConfigs: IStatusBlockConfig<T>[];
  @Input() public value: T;

  public get currentConfig(): IStatusBlockConfig<T> {
    return this.statusConfigs.find((item) => item.value === this.value);
  }
}
