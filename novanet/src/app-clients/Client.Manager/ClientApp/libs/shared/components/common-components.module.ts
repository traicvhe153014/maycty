import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusBlockComponent } from '@shared/components/status-block';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [StatusBlockComponent],
  imports: [CommonModule, NzToolTipModule],
  exports: [StatusBlockComponent],
})
export class CommonComponentsModule {}
