import { NgControl } from '@angular/forms';
import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[novanetValidateOnblur]',
})
export class ValidateOnBlurDirective {
  private validators: any;
  private asyncValidators: any;

  constructor(public formControl: NgControl) {}

  @HostListener('onfocus') onFocus($event) {
    this.validators = this.formControl.control.validator;
    this.asyncValidators = this.formControl.control.asyncValidator;
    this.formControl.control.clearAsyncValidators();
    this.formControl.control.clearValidators();
  }

  @HostListener('onblur') onBlur($event) {
    this.formControl.control.setAsyncValidators(this.asyncValidators);
    this.formControl.control.setValidators(this.validators);
    this.formControl.control.updateValueAndValidity();
  }
}
