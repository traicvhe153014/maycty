import { NgModule } from '@angular/core';
import { ValidateOnBlurDirective } from '@shared/directives/validate-on-blur/validate-on-blur.directive';

@NgModule({
  declarations: [ValidateOnBlurDirective],
  exports: [ValidateOnBlurDirective],
})
export class ValidateOnBlurModule {}
