import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[novanetScrollListener]',
})
export class ScrollListenerDirective {
  // This will emit the scroll update to whatever you want to assign with its emitted value
  @Output() containerScroll: EventEmitter<void> = new EventEmitter<void>();

  @HostListener('scroll', []) // Listens to Window Scroll Event
  onWindowScroll() {
    this.containerScroll.emit(); // Use the @Output() windowScroll to emit a simple string 'scrolled' whenever the window triggers a scroll event from the user
  }
}
