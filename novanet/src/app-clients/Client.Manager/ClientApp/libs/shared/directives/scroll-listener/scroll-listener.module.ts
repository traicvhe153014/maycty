import { NgModule } from '@angular/core';
import { ScrollListenerDirective } from './scroll-listener.directive';

@NgModule({
  declarations: [ScrollListenerDirective],
  exports: [ScrollListenerDirective],
})
export class ScrollListenerModule {}
