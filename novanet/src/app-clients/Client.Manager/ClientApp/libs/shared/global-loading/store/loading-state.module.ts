import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { LoadingState } from './loading.state';

@NgModule({
  imports: [NgxsModule.forFeature([LoadingState])],
})
export class LoadingStateModule {}
