import { Action, Selector, State, StateContext } from '@ngxs/store';

import { HideLoading, ShowLoading } from './loading-state.actions';
import { LoadingStateModel } from './loading-state.model';

@State<LoadingStateModel>({
  name: 'loading',
  defaults: {
    actionsInProgress: [],
    lastUpdateTimestamp: null,
  },
})
export class LoadingState {
  @Selector()
  public static getLoadingState(state: LoadingStateModel): string[] {
    return state.actionsInProgress && state.actionsInProgress.length
      ? state.actionsInProgress
      : null;
  }

  @Action(ShowLoading)
  public showLoading(
    ctx: StateContext<LoadingStateModel>,
    { label }: ShowLoading
  ): void {
    const actionsInProgress = [...ctx.getState().actionsInProgress, label];
    const lastUpdateTimestamp = Date.now();
    const loadingResetTimeoutMs = 20000;

    ctx.patchState({ actionsInProgress, lastUpdateTimestamp });
    setTimeout(() => {
      if (lastUpdateTimestamp === ctx.getState().lastUpdateTimestamp) {
        ctx.patchState({ actionsInProgress: [] });
      }
    }, loadingResetTimeoutMs);
  }

  @Action(HideLoading)
  public hideLoading(
    ctx: StateContext<LoadingStateModel>,
    { label }: HideLoading
  ): void {
    const actionsInProgress = [...ctx.getState().actionsInProgress];
    const index = actionsInProgress.findIndex((item) => item === label);

    if (index !== -1) {
      actionsInProgress.splice(index, 1);
    }

    ctx.patchState({ actionsInProgress });
  }
}
