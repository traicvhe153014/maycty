export class ShowLoading {
  public static readonly type: string = '[Loading] ShowLoading';

  constructor(public label?: string) {}
}

export class HideLoading {
  public static readonly type: string = '[Loading] HideLoading';

  constructor(public label?: string) {}
}
