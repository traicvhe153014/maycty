import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoadingComponent, LoadingOverlayComponent } from './components';
import { LoadingStateModule } from './store';
import { NzSpinModule } from 'ng-zorro-antd/spin';

const COMPONENTS: any[] = [LoadingOverlayComponent, LoadingComponent];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [CommonModule, LoadingStateModule, NzSpinModule],
})
export class GlobalLoadingModule {}
