export interface LoadingStateModel {
  actionsInProgress: string[];
  lastUpdateTimestamp: number;
}
