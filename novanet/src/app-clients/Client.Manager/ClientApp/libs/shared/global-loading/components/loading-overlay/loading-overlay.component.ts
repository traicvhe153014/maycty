import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LoadingState } from '@shared/global-loading/store';

@Component({
  selector: 'novanet-loading-overlay',
  templateUrl: './loading-overlay.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingOverlayComponent implements OnDestroy, OnInit {
  public loadingInProgress: boolean;
  @Select(LoadingState.getLoadingState)
  private progress$: Observable<number>;
  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  public ngOnInit(): void {
    this.progress$.pipe(takeUntil(this.destroy$)).subscribe((data: number) => {
      this.loadingInProgress = !!data;
      this.changeDetectorRef.markForCheck();
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
