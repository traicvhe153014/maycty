import { Component } from '@angular/core';

@Component({
  selector: 'novanet-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent {}
