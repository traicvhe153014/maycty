import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import {
  IAdvertisingResponse,
  IAdvertisingSummary,
} from '@features/campaign-management/store';
import {
  AdListSummaryEnum,
  AdListTableEnum,
} from '@features/campaign-management/enums';
import { metricSortFields } from '@core';

export const adListSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '125vw',
  summaryRow: true,
  sortType: 'single',
} as DataTableSettingModel;

export const adListColumns: ISettingColumnTable<IAdvertisingResponse, any>[] = [
  {
    id: AdListTableEnum.CHECKBOX,
    title: 'Checkbox',
    align: 'center',
    pinLeft: true,
    width: '50px',
  },
  {
    id: AdListTableEnum.INDEX,
    title: 'Stt',
    pinLeft: true,
    width: '50px',
  },
  {
    id: AdListTableEnum.IS_ACTIVE,
    title: 'Bật/tắt',
    pinLeft: true,
    width: '70px',
    sort: true,
    name: 'isActive',
    ascending: false,
    descending: true,
  },
  {
    id: AdListTableEnum.NAME,
    title: 'Tên QC | Chiến dịch | Nhóm',
    width: '400px',
    pinLeft: true,
    sort: true,
    name: 'name',
  },
  {
    id: AdListTableEnum.TEMPLATE,
    title: 'Định dạng',
    width: '350px',
  },
  {
    id: AdListTableEnum.REDIRECT_LINK,
    title: 'URL đích',
    width: '300px',
  },
  {
    id: AdListTableEnum.STATUS,
    title: 'Trạng thái',
    width: '150px',
    sort: true,
    name: 'status',
    ascending: true,
    descending: false,
  },
  {
    id: AdListTableEnum.BUDGET,
    title: 'Ngân sách (VNĐ)',
    sort: true,
    width: '170px',
  },
  {
    id: AdListTableEnum.COSTS,
    title: 'Chi phí (VNĐ)',
    width: '150px',
    sort: true,
    name: metricSortFields.totalCost,
  },
  {
    id: AdListTableEnum.IMPRESSIONS,
    title: 'Lượt xem',
    width: '150px',
    sort: true,
    name: metricSortFields.totalImpressions,
  },
  {
    id: AdListTableEnum.CLICKS,
    title: 'Lượt nhấn',
    width: '125px',
    sort: true,
    name: metricSortFields.totalClicks,
  },
  {
    id: AdListTableEnum.CTR,
    title: 'CTR',
    width: '100px',
    sort: true,
    name: metricSortFields.totalCtr,
  },
  {
    id: AdListTableEnum.CPC,
    title: 'CPC (VNĐ)',
    width: '125px',
    sort: true,
    name: metricSortFields.totalCpc,
  },
  {
    id: AdListTableEnum.PURCHASE,
    title: 'SL mua hàng',
    width: '125px',
    sort: true,
    name: metricSortFields.totalPurchase,
  },
  {
    id: AdListTableEnum.PURCHASE_CONVERSION,
    title: '% chuyển đổi mua hàng',
    width: '175px',
    sort: true,
    name: metricSortFields.totalPurchaseConversion,
  },
  {
    id: AdListTableEnum.CPS,
    title: 'CPS (VNĐ)',
    width: '125px',
    sort: true,
    name: metricSortFields.totalCps,
  },
  {
    id: AdListTableEnum.VIDEO_VIEW_3S,
    title: 'Video view 3s',
    width: '160px',
  },
  {
    id: AdListTableEnum.VIDEO_25_PERCENT,
    title: 'Video watches at 25%',
    width: '160px',
  },
  {
    id: AdListTableEnum.VIDEO_50_PERCENT,
    title: 'Video watches at 50%',
    width: '160px',
  },
  {
    id: AdListTableEnum.VIDEO_75_PERCENT,
    title: 'Video watches at 75%',
    width: '160px',
  },
  {
    id: AdListTableEnum.VIDEO_100_PERCENT,
    title: 'Video watches at 100%',
    width: '160px',
  },
];

export const adListSummaryColumns: ISummaryColumnTable<IAdvertisingSummary>[] =
  [
    {
      id: AdListSummaryEnum.COUNT,
      colspan: 4,
      pinLeft: true,
    },
    {
      id: AdListSummaryEnum.CAMPAIGN_TYPE,
    },
    {
      id: AdListSummaryEnum.REDIRECT_LINK,
    },
    {
      id: AdListSummaryEnum.STATUS,
    },
    {
      id: AdListSummaryEnum.BUDGET,
    },
    {
      id: AdListSummaryEnum.COSTS,
    },
    {
      id: AdListSummaryEnum.IMPRESSIONS,
    },
    {
      id: AdListSummaryEnum.CLICKS,
    },
    {
      id: AdListSummaryEnum.CTR,
    },
    {
      id: AdListSummaryEnum.CPC,
    },
    {
      id: AdListSummaryEnum.PURCHASE,
    },
    {
      id: AdListSummaryEnum.PURCHASE_CONVERSION,
    },
    {
      id: AdListSummaryEnum.CPS,
    },
    {
      id: AdListSummaryEnum.VIDEO_VIEW_3S,
    },
    {
      id: AdListSummaryEnum.VIDEO_25_PERCENT,
    },
    {
      id: AdListSummaryEnum.VIDEO_50_PERCENT,
    },
    {
      id: AdListSummaryEnum.VIDEO_75_PERCENT,
    },
    {
      id: AdListSummaryEnum.VIDEO_100_PERCENT,
    },
  ];

export const AdOptionColumnDefault = [
  AdListTableEnum.BUDGET,
  AdListTableEnum.COSTS,
  AdListTableEnum.IMPRESSIONS,
  AdListTableEnum.CLICKS,
  AdListTableEnum.CTR,
  AdListTableEnum.CPC,
  AdListTableEnum.VIDEO_VIEW_3S,
  AdListTableEnum.PURCHASE,
  AdListTableEnum.PURCHASE_CONVERSION,
  AdListTableEnum.CPS,
] as number[];
