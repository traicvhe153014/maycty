import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import {
  AdsetListSummaryEnum,
  AdsetListTableEnum,
} from '@features/campaign-management/enums';
import {
  IAdsetResponse,
  IAdsetSummary,
} from '@features/campaign-management/store/adset';
import { IFormFieldErrors, IFormFields } from '@models';
import { metricSortFields } from '@core';

export const adsetListSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '125vw',
  summaryRow: true,
  sortType: 'single',
} as DataTableSettingModel;

export const adsetListColumns: ISettingColumnTable<IAdsetResponse, any>[] = [
  {
    id: AdsetListTableEnum.CHECKBOX,
    title: 'Checkbox',
    align: 'center',
    pinLeft: true,
    width: '50px',
  },
  {
    id: AdsetListTableEnum.INDEX,
    title: 'Stt',
    pinLeft: true,
    width: '50px',
  },
  {
    id: AdsetListTableEnum.IS_ACTIVE,
    title: 'Bật/tắt',
    pinLeft: true,
    width: '70px',
    name: 'IsActive',
    sort: true,
    ascending: false,
    descending: true,
  },
  {
    id: AdsetListTableEnum.NAME,
    title: 'Tên nhóm',
    pinLeft: true,
    sort: true,
    width: '400px',
    name: 'Name',
  },
  {
    id: AdsetListTableEnum.CAMPAIGN_NAME,
    title: 'Chiến dịch QC',
    sort: true,
    width: '300px',
    name: 'Campaign.Name',
  },
  {
    id: AdsetListTableEnum.STATUS,
    title: 'Trạng thái',
    sort: true,
    width: '150px',
    name: 'Status',
    ascending: true,
    descending: false,
  },
  {
    id: AdsetListTableEnum.CAMPAIGN_TYPE,
    title: 'Loại QC',
    sort: false,
    width: '125px',
  },
  {
    id: AdsetListTableEnum.BUDGET,
    title: 'Ngân sách (VNĐ)',
    sort: true,
    width: '170px',
  },
  {
    id: AdsetListTableEnum.COSTS,
    title: 'Tổng chi phí (VNĐ)',
    name: metricSortFields.totalCost,
    sort: true,
    width: '150px',
  },
  {
    id: AdsetListTableEnum.IMPRESSIONS,
    title: 'Lượt xem',
    name: metricSortFields.totalImpressions,
    sort: true,
    width: '125px',
  },
  {
    id: AdsetListTableEnum.CLICKS,
    title: 'Lượt nhấn',
    name: metricSortFields.totalClicks,
    sort: true,
    width: '125px',
  },
  {
    id: AdsetListTableEnum.CTR,
    title: 'CTR',
    name: metricSortFields.totalCtr,
    sort: true,
    width: '100px',
  },
  {
    id: AdsetListTableEnum.CPC,
    title: 'CPC (VNĐ)',
    name: metricSortFields.totalCpc,
    sort: true,
    width: '125px',
  },
  {
    id: AdsetListTableEnum.PURCHASE,
    title: 'SL mua hàng',
    sort: true,
    name: metricSortFields.totalPurchase,
    width: '125px',
  },
  {
    id: AdsetListTableEnum.PURCHASE_CONVERSION,
    title: '% chuyển đổi mua hàng',
    sort: true,
    name: metricSortFields.totalPurchaseConversion,
    width: '175px',
  },
  {
    id: AdsetListTableEnum.CPS,
    title: 'CPS (VNĐ)',
    sort: true,
    name: metricSortFields.totalCps,
    width: '125px',
  },
  {
    id: AdsetListTableEnum.VIDEO_VIEW_3S,
    title: 'Video view 3s',
    width: '160px',
  },
  {
    id: AdsetListTableEnum.VIDEO_25_PERCENT,
    title: 'Video watches at 25%',
    width: '160px',
  },
  {
    id: AdsetListTableEnum.VIDEO_50_PERCENT,
    title: 'Video watches at 50%',
    width: '160px',
  },
  {
    id: AdsetListTableEnum.VIDEO_75_PERCENT,
    title: 'Video watches at 75%',
    width: '160px',
  },
  {
    id: AdsetListTableEnum.VIDEO_100_PERCENT,
    title: 'Video watches at 100%',
    width: '160px',
  },
];

export const adsetListSummaryColumns: ISummaryColumnTable<IAdsetSummary>[] = [
  {
    id: AdsetListSummaryEnum.COUNT,
    colspan: 4,
    pinLeft: true,
  },
  {
    id: AdsetListSummaryEnum.CAMPAIGN_NAME,
  },
  {
    id: AdsetListSummaryEnum.STATUS,
  },
  {
    id: AdsetListSummaryEnum.CAMPAIGN_TYPE,
  },
  {
    id: AdsetListSummaryEnum.BUDGET,
  },
  {
    id: AdsetListSummaryEnum.COSTS,
  },
  {
    id: AdsetListSummaryEnum.IMPRESSIONS,
  },
  {
    id: AdsetListSummaryEnum.CLICKS,
  },
  {
    id: AdsetListSummaryEnum.CTR,
  },
  {
    id: AdsetListSummaryEnum.CPC,
  },
  {
    id: AdsetListSummaryEnum.PURCHASE,
  },
  {
    id: AdsetListSummaryEnum.PURCHASE_CONVERSION,
  },
  {
    id: AdsetListSummaryEnum.CPS,
  },
  {
    id: AdsetListSummaryEnum.VIDEO_VIEW_3S,
  },
  {
    id: AdsetListSummaryEnum.VIDEO_25_PERCENT,
  },
  {
    id: AdsetListSummaryEnum.VIDEO_50_PERCENT,
  },
  {
    id: AdsetListSummaryEnum.VIDEO_75_PERCENT,
  },
  {
    id: AdsetListSummaryEnum.VIDEO_100_PERCENT,
  },
];

export const regionOrders = [
  'Miền Bắc',
  'Miền Trung',
  'Miền Nam',
  'Nước ngoài',
];

export const genderOrders = ['Nam', 'Nữ', 'Không xác định'];

export const createAdsetFormFields: IFormFields = {
  name: {
    name: 'name',
    validationParams: {
      required: true,
      maxLength: 150,
    },
  },
  campaignId: {
    name: 'campaignId',
  },
  applyAllProductGroups: {
    name: 'applyAllProductGroups',
  },
  productGroupIds: {
    name: 'productGroupIds',
  },
  hasProhibitedCategories: {
    name: 'hasProhibitedCategories',
  },
  prohibitedCategoryIds: {
    name: 'prohibitedCategoryIds',
  },
  hasObject: {
    name: 'hasObject',
  },
  objectIds: {
    name: 'objectIds',
  },
  locationIds: {
    name: 'locationIds',
  },
  ageIds: {
    name: 'ageIds',
  },
  genderIds: {
    name: 'genderIds',
  },
  categoryIds: {
    name: 'categoryIds',
  },
  uniformed: {
    name: 'uniformed',
  },
  conditionsStoppingMarketing: {
    name: 'conditionsStoppingMarketing',
  },
  scheduledAllTime: {
    name: 'scheduledAllTime',
  },
};

export const createAdsetFormFieldErrors: IFormFieldErrors = {
  name: [
    {
      type: 'required',
      message: 'Thiếu tên nhóm quảng cáo',
    },
    {
      type: 'maxlength',
      message: 'Số lượng kí tự tối đa là 150 kí tự',
    },
  ],
};

export const AdsetOptionColumnDefault = [
  AdsetListTableEnum.BUDGET,
  AdsetListTableEnum.COSTS,
  AdsetListTableEnum.IMPRESSIONS,
  AdsetListTableEnum.CLICKS,
  AdsetListTableEnum.CTR,
  AdsetListTableEnum.CPC,
  AdsetListTableEnum.VIDEO_VIEW_3S,
  AdsetListTableEnum.PURCHASE,
  AdsetListTableEnum.PURCHASE_CONVERSION,
  AdsetListTableEnum.CPS,
] as number[];
