export interface ISettingTemplate {
  templateId: number;
  productName: boolean;
  bannerImageUrl: string;
  imageLink: string;
  useBannerTemplate: string;
  redirectUrl: string;
  brandAvatar: string;
  brandName: string;
  description: string;
  promotion: boolean;
  freeDelivery: boolean;
  templateType: boolean;
  products: ISettingProductItem[];
  expandedContentUrl: string;
}

export interface ISettingProductItem {
  title: string;
  productImageUrl: string;
  redirectUrl: string;
  discountedPrice: number;
  price: number;
}
