import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { IFormFieldErrors, IFormFields } from '@models';
import { FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { EAdsetAddRemarketingOption } from './enums';
import { IAdsetByIdResponse } from '@features/campaign-management/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'novanet-adset-add-remarketing',
  templateUrl: './adset-add-remarketing.component.html',
  styleUrls: ['./adset-add-remarketing.component.scss'],
})
export class AdsetAddRemarketingComponent implements OnInit, OnDestroy {
  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;
  @Input() formType: string; // add | edit
  @Input() currentAdset: IAdsetByIdResponse | undefined;

  public form: FormGroup;
  public currentMarketingOption = EAdsetAddRemarketingOption.NO_MORE_MARKETING;
  public readonly marketingOption = EAdsetAddRemarketingOption;
  private readonly destroy$ = new Subject<void>();

  constructor(
    private rootFormGroup: FormGroupDirective,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get remarketingType(): FormControl {
    return this.form.get('remarketingType') as FormControl;
  }

  ngOnInit() {
    this.form = this.rootFormGroup.control;
    this.form
      .get('remarketingType')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: (value: EAdsetAddRemarketingOption) => {
          this.currentMarketingOption = value;
        },
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onChangeMarketingOption(option: number) {
    this.destroy$.next();
    this.destroy$.complete();
    this.currentMarketingOption = option;
    this.changeDetectorRef.detectChanges();
    this.remarketingType.patchValue(option);
  }
}
