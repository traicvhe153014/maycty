import { Component, Input } from '@angular/core';
import { CampaignStatusEnum } from '@features/campaign-management/store/campaign';

@Component({
  selector: 'novanet-campaign-status-icon',
  templateUrl: './campaign-status-icon.component.html',
  styleUrls: ['./campaign-status-icon.component.scss'],
})
export class CampaignStatusIconComponent {
  @Input() status: CampaignStatusEnum;
  public readonly campaignStatusEnum = CampaignStatusEnum;
}
