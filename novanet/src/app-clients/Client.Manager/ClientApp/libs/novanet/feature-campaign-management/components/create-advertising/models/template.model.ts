import { ETemplateType } from '@features/campaign-management/enums';

export interface ITemplate {
  id: ETemplateType;
  template: string;
  activated: true;
}
