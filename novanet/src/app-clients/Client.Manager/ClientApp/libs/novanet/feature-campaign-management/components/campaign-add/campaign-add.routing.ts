import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampaignAddComponent } from './campaign-add.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignAddComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignAddRouting {}
