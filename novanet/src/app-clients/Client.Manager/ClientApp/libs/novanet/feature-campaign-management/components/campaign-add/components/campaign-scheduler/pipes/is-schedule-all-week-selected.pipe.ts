import { Pipe, PipeTransform } from '@angular/core';
import { ICampaignScheduling } from '@features/campaign-management/store/campaign';

const DAYS_IN_WEEK = 7;

@Pipe({ name: 'isScheduleAllWeekSelected' })
export class IsScheduleAllWeekSelectedPipe implements PipeTransform {
  transform(
    schedulings: Partial<ICampaignScheduling>[],
    hour: number
  ): boolean {
    const items = schedulings.filter((item) => item.timing.getHours() === hour);
    return items.length === DAYS_IN_WEEK;
  }
}
