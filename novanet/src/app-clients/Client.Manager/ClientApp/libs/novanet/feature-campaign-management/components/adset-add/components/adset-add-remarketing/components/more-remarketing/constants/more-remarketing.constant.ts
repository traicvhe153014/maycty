import { IFormFieldErrors, IFormFields } from '@models';
import { DataTableSettingModel } from '@data-table/models';
import { IDropdownValues } from '@core/models';
import { EUniformedUnit } from '@features/campaign-management/components/adset-add/components/adset-add-remarketing/components/more-remarketing/enums';

export const MoreMarketingFormFields = {
  uniformed: {
    name: 'uniformed',
    validationParams: {
      required: true,
    },
  },
  uniformedPrice: {
    name: 'uniformedPrice',
    validationParams: {
      required: true,
    },
  },
  uniformedUnit: {
    name: 'uniformedUnit',
    validationParams: {
      required: true,
    },
  },
  selectedWebsites: {
    name: 'selectedWebsites',
    validationParams: {
      required: true,
    },
  },
  excludedWebsite: {
    name: 'excludedWebsite',
    validationParams: {
      required: true,
    },
  },
  excludedWebsites: {
    name: 'excludedWebsites',
    validationParams: {
      required: true,
    },
  },
  targetingMarketingByProduct: {
    name: 'targetingMarketingByProduct',
    validationParams: {
      required: true,
    },
  },
  adImpressions: {
    name: 'adImpressions',
    validationParams: {
      required: true,
    },
  },
  remarketingTime: {
    name: 'remarketingTime',
    validationParams: {
      required: true,
    },
  },
  timeOnDisplay: {
    name: 'timeOnDisplay',
    validationParams: {
      required: true,
    },
  },
  conditionsStoppingMarketing: {
    name: 'conditionsStoppingMarketing',
    validationParams: {
      required: true,
    },
  },
  clickedAdvertising: {
    name: 'clickedAdvertising',
    validationParams: {
      required: true,
    },
  },
  viewer: {
    name: 'viewer',
    validationParams: {
      required: true,
    },
  },
  viewedMax: {
    name: 'viewedMax',
    validationParams: {
      required: true,
    },
  },
  remarketingType: {
    name: 'remarketingType',
    validationParams: {
      required: true,
    },
  },
} as IFormFields;

export const MoreMarketingFormFieldsErrors: IFormFieldErrors = {
  adImpressions: [
    {
      type: 'required',
      message: 'Không để trống dữ liệu.',
    },
  ],
  viewedMax: [
    {
      type: 'required',
      message: 'Không để trống.',
    },
  ],
  timeOnDisplay: [
    {
      type: 'required',
      message: 'Không để trống dữ liệu.',
    },
  ],
  remarketingTime: [
    {
      type: 'required',
      message: 'Không để trống dữ liệu.',
    },
  ],
};

export const UniformedUnitDropdown = [
  {
    id: EUniformedUnit.CPC,
    label: 'CPC',
  },
  {
    id: EUniformedUnit.CPM,
    label: 'CPM',
  },
] as IDropdownValues[];

export const MoreMarketingSettingTable = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;
