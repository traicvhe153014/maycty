import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdsetAddComponent } from './adset-add.component';

const routes: Routes = [
  {
    path: '',
    component: AdsetAddComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdsetAddRouting {}
