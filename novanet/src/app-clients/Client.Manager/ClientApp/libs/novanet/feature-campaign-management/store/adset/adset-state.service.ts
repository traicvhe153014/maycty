import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  IAdsetByIdResponse,
  IAdsetCreateRequest,
  IAdsetListRequest,
  IAdsetListResponse,
  IAdsetProductGroup,
  IAdsetResponse,
  IAdsetUpdateProductGroupRequest,
  IAdsetUpdateRequest,
} from './adset-state.model';
import { Observable, Subject } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';

const AdsetUrls = {
  getList: `advertisingSet/list`,
  getById: `advertisingSet/get`,
  create: 'advertisingSet/create',
  update: 'advertisingSet/update',
  updateProductGroup: 'advertisingSet/updateProductGroup',
};

@Injectable()
export class AdsetService extends BaseService {
  public applySelectedWebsiteTemplate$ = new Subject<string[]>();

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public getList(params: IAdsetListRequest): Observable<IAdsetListResponse> {
    const processedParams = {
      ...params,
      startDate: params.startDate?.toISOString(),
      endDate: params.endDate?.toISOString(),
    };
    return this.httpClient
      .post<ISuccessHttpResponse<IAdsetListResponse>>(
        this.createUrl(AdsetUrls.getList),
        processedParams
      )
      .pipe(map((response) => response.data));
  }

  public getById(id: string): Observable<IAdsetByIdResponse> {
    const params = new HttpParams({
      fromObject: {
        id,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IAdsetByIdResponse>>(
        this.createUrl(AdsetUrls.getById),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public create(payload: IAdsetCreateRequest): Observable<IAdsetResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IAdsetResponse>>(
        this.createUrl(AdsetUrls.create),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public update(
    payload: Partial<IAdsetUpdateRequest>
  ): Observable<IAdsetResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IAdsetResponse>>(
        this.createUrl(AdsetUrls.update),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public updateProductGroup(
    payload: IAdsetUpdateProductGroupRequest
  ): Observable<IAdsetProductGroup> {
    return this.httpClient
      .put<ISuccessHttpResponse<IAdsetProductGroup>>(
        this.createUrl(AdsetUrls.updateProductGroup),
        payload
      )
      .pipe(map((response) => response.data));
  }
}
