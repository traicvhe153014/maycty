import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampaignManagementComponent } from './campaign-management.component';

const routes: Routes = [
  {
    path: 'management',
    component: CampaignManagementComponent,
  },
  {
    path: 'add-campaign',
    loadChildren: () =>
      import('@features/campaign-management/components/campaign-add').then(
        (m) => m.CampaignAddModule
      ),
  },
  {
    path: 'edit-campaign',
    loadChildren: () =>
      import('@features/campaign-management/components/campaign-add').then(
        (m) => m.CampaignAddModule
      ),
  },
  {
    path: 'add-adset',
    loadChildren: () =>
      import('@features/campaign-management/components/adset-add').then(
        (m) => m.AdsetAddModule
      ),
  },
  {
    path: 'edit-adset',
    loadChildren: () =>
      import('@features/campaign-management/components/adset-add').then(
        (m) => m.AdsetAddModule
      ),
  },
  {
    path: 'add-advertising',
    loadChildren: () =>
      import(
        '@features/campaign-management/components/create-advertising'
      ).then((m) => m.CreateAdvertisingModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignManagementRouting {}
