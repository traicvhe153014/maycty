import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ETemplateType } from '@features/campaign-management/enums';
import {
  ITemplateConfiguration,
  ITemplateDisplayConfiguration,
} from '@features/campaign-management/store';
import {
  AllowSizeColumnPopupBanner,
  AllowSizeSetting,
} from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/constant';
import { ESettingFormatAdvertising } from '@features/campaign-management/components/create-advertising/components/setting-format-advertising/enums';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import {
  createNotificationBannerFromFieldErrors,
  createNotificationBannerFromFields,
} from './notification-banner.contants';
import { convertPrice, isFileSizeValid } from '@core/utils';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { Store } from '@ngxs/store';
import { cloneDeep } from 'lodash';
import { environment } from '@environment';

interface ImageModel {
  name: string;
  src: string;
  file: File;
}

@Component({
  selector: 'novanet-notification-banner',
  templateUrl: './notification-banner.component.html',
  styleUrls: ['./notification-banner.component.scss'],
})
export class NotificationBannerComponent implements OnInit {
  @Input() public height = 0;
  @Input() public width = 0;
  @Input() public dataTable = [];
  @Input() public templateType: ETemplateType;
  @Input() public templateConfiguration: ITemplateConfiguration;
  @Output() public templateConfigurationChange =
    new EventEmitter<ITemplateDisplayConfiguration>();

  @ViewChild('replaceFileInput') replaceFileInputRef: ElementRef;

  public templateTypes = ETemplateType;
  public settingColumn = AllowSizeColumnPopupBanner;
  public settingTable = AllowSizeSetting;
  public allowSizeColumn = ESettingFormatAdvertising;
  public notificationBannerForm: FormGroup;
  public currentImage: ImageModel[] = [];
  public currentAvatar: ImageModel[] = [];
  public replaceImageIndex: number | undefined = undefined;

  public readonly createFormFields = createNotificationBannerFromFields;
  public readonly createFormFieldErrors =
    createNotificationBannerFromFieldErrors;

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get images(): FormControl {
    return this.notificationBannerForm.get('images') as FormControl;
  }

  get logo(): FormControl {
    return this.notificationBannerForm.get('logo') as FormControl;
  }

  get title(): string {
    return this.notificationBannerForm.get('title').value;
  }

  get extendNotification(): string {
    return this.notificationBannerForm.get('extendNotification').value;
  }

  get isCta(): boolean {
    return this.notificationBannerForm.get('isCta').value;
  }

  get isCustomImage(): boolean {
    return this.notificationBannerForm.get('isCustomImage').value;
  }

  get isDelay(): boolean {
    return this.notificationBannerForm.get('isDelay').value;
  }

  get cta(): boolean {
    return this.notificationBannerForm.get('cta').value;
  }

  get delay(): boolean {
    return this.notificationBannerForm.get('delay').value;
  }

  get logoNotification(): FormControl {
    return this.notificationBannerForm.get('logoNotification') as FormControl;
  }

  get listBannerNotification(): FormControl {
    return this.notificationBannerForm.get(
      'listBannerNotification'
    ) as FormControl;
  }

  public ngOnInit() {
    this.buildNotificationBannerForm();
    const listImage = [];
    this.notificationBannerForm.controls['images'].value.forEach?.((image) => {
      listImage.push(
        environment.host +
          '/storage/api/v1/stream?name=' +
          image.replace('x{0}', 'x800')
      );
    });
    this.notificationBannerForm.patchValue({
      listBannerNotification: listImage,
    });
  }

  public handleBannerInput(field: string, event: Event) {
    const files = (event.target as HTMLInputElement).files;
    const fileList = [] as File[];
    // eslint-disable-next-line @typescript-eslint/prefer-for-of
    for (let i = 0; i < files.length; i++) {
      fileList.push(files.item(i));
    }
    if (
      this.currentImage.length + fileList.length > 3 ||
      this.templateConfiguration.configurations.images?.length +
        fileList.length >
        3
    ) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên quá 3 ảnh'
        )
      );
      return;
    }
    if (isFileSizeValid(fileList, 1024000)) {
      fileList.forEach((file) => {
        if (!/(?:.jpg|.jpeg|.png(?!\\))+$/.test(file.name)) {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.warning,
              'Cảnh báo',
              'Sai định dạng ảnh'
            )
          );
          return;
        }
        const fr = new FileReader();
        let height = 0;
        let width = 0;
        fr.onload = () => {
          const img = new Image();
          img.onload = () => {
            width = img.width;
            height = img.height;
            const ratio = width / height;
            const currentImageRatio = this.width / this.height;
            if (
              field !== 'expandedContent' &&
              currentImageRatio &&
              ratio !== currentImageRatio
            ) {
              this.store.dispatch(
                new ShowGlobalNotification(
                  GlobalNotificationEnum.warning,
                  'Cảnh báo',
                  'Sai kích thước ảnh'
                )
              );
              return;
            }
            this.currentImage = [
              ...(this.templateConfiguration.configurations.images ||
                this.currentImage),
              {
                name: file.name,
                src: img.src,
                file,
              },
            ];
            if (file) {
              const listImage =
                this.notificationBannerForm.controls['listBannerNotification']
                  .value;
              this.notificationBannerForm.patchValue({
                listBannerNotification: [
                  ...listImage,
                  window.URL.createObjectURL(file),
                ],
              });
            }
            this.images.patchValue(cloneDeep(this.currentImage));
            (event.target as HTMLInputElement).value = '';
            this.changeDetectorRef.detectChanges();
          };
          img.src = fr.result as any;
        };
        fr.readAsDataURL(file);
      });
    } else {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên dữ liệu quá 1MB'
        )
      );
    }
  }

  public handleReplaceBannerInput(field: string, event: Event) {
    const files = (event.target as HTMLInputElement).files;
    if (this.replaceImageIndex === undefined || files.length === 0) {
      return;
    }
    const file = files.item(0);
    if (isFileSizeValid([file], 1024000)) {
      if (!/(?:.jpg|.jpeg|.png(?!\\))+$/.test(file.name)) {
        this.replaceImageIndex = undefined;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Cảnh báo',
            'Sai định dạng ảnh'
          )
        );
        return;
      }
      const fr = new FileReader();
      let height = 0;
      let width = 0;
      fr.onload = () => {
        const img = new Image();
        img.onload = () => {
          width = img.width;
          height = img.height;
          const ratio = width / height;
          const currentImageRatio = this.width / this.height;
          if (
            field !== 'expandedContent' &&
            currentImageRatio &&
            ratio !== currentImageRatio
          ) {
            this.replaceImageIndex = undefined;
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.warning,
                'Cảnh báo',
                'Sai kích thước ảnh'
              )
            );
            return;
          }
          this.currentImage = (
            this.templateConfiguration.configurations.images ||
            this.currentImage
          ).map((currentImage, i) => {
            if (i !== this.replaceImageIndex) {
              return currentImage;
            }
            return {
              name: file.name,
              src: img.src,
              file,
            };
          });
          const listImage = [];
          this.notificationBannerForm.controls[
            'listBannerNotification'
          ].value.forEach((image, index) => {
            if (index === this.replaceImageIndex) {
              listImage.push(window.URL.createObjectURL(file));
            } else {
              listImage.push(image);
            }
          });
          this.notificationBannerForm.patchValue({
            listBannerNotification: listImage,
          });
          this.images.patchValue(cloneDeep(this.currentImage));
          (event.target as HTMLInputElement).value = '';
          this.changeDetectorRef.detectChanges();
        };
        img.src = fr.result as any;
      };
      fr.readAsDataURL(file);
    } else {
      this.replaceImageIndex = undefined;
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên dữ liệu quá 1MB'
        )
      );
    }
  }

  public handleAvatarInput(field: string, files: FileList) {
    const fileList = [] as File[];
    // eslint-disable-next-line @typescript-eslint/prefer-for-of
    for (let i = 0; i < files.length; i++) {
      fileList.push(files.item(i));
    }
    if (isFileSizeValid(fileList, 1024000)) {
      if (!/(?:.jpg|.jpeg|.png(?!\\))+$/.test(fileList[0].name)) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Cảnh báo',
            'Sai định dạng ảnh'
          )
        );
        return;
      }
      const fr = new FileReader();
      fr.onload = () => {
        const img = new Image();
        img.src = fr.result as any;
        this.currentAvatar.push({
          name: fileList[0].name,
          src: img.src,
          file: fileList[0],
        });
        this.notificationBannerForm.patchValue({
          logoNotification: files[0]
            ? window.URL.createObjectURL(files[0])
            : '',
        });
        this.logo.patchValue(cloneDeep(this.currentAvatar[0]));
        this.changeDetectorRef.detectChanges();
      };
      fr.readAsDataURL(fileList[0]);
    } else {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên dữ liệu quá 1MB'
        )
      );
    }
  }

  public onConfigChange(cta: string, $event: any) {}

  public replaceImage(index: number) {
    this.replaceImageIndex = index;
    this.replaceFileInputRef.nativeElement.click();
  }

  public removeImage(index: number) {
    this.currentImage = (
      this.templateConfiguration.configurations.images || this.currentImage
    ).filter((x, i) => i !== index);
    this.images.patchValue(cloneDeep(this.currentImage));
    const listImage =
      this.notificationBannerForm.controls['listBannerNotification'].value;
    this.listBannerNotification.patchValue(
      cloneDeep(listImage.filter((x, i) => i !== index))
    );
  }

  public clearAvatar(bannerImage: string) {
    this.currentAvatar = [];
    this.logo.patchValue('');
    this.logoNotification.reset();
  }

  public getStorageUrl(url: string) {
    const bannerUrl = url.replace('x{0}', 'x800');
    return environment.host + '/storage/api/v1/stream?name=' + bannerUrl;
  }

  private notificationBannerFormValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    // validate cta
    const isCta = control.get(createNotificationBannerFromFields.isCta.name)
      .value as boolean;
    const cta = control.get(createNotificationBannerFromFields.cta.name)
      .value as string;
    let ctaError: { [key: string]: boolean } | null = null;

    if (isCta) {
      if (!cta) {
        ctaError = { required: true };
      } else if (
        cta.length >
        createNotificationBannerFromFields.cta.validationParams.maxLength
      ) {
        ctaError = { maxlength: true };
      }
    }
    control
      .get(createNotificationBannerFromFields.cta.name)
      .setErrors(ctaError);

    // validate delay
    const isDelay = control.get(createNotificationBannerFromFields.isDelay.name)
      .value as boolean;
    const delay = control.get(
      createNotificationBannerFromFields.delay.name
    ).value;
    let delayError: { [key: string]: boolean } | null = null;

    if (isDelay) {
      if (!delay) {
        delayError = { required: true };
      } else {
        const delayNumber = convertPrice(delay);
        if (
          delayNumber >
          createNotificationBannerFromFields.delay.validationParams.max
        ) {
          delayError = { max: true };
        } else if (
          delayNumber <
          createNotificationBannerFromFields.delay.validationParams.min
        ) {
          delayError = { min: true };
        }
      }
    }
    control
      .get(createNotificationBannerFromFields.delay.name)
      .setErrors(delayError);
    return null;
  }

  private buildNotificationBannerForm() {
    const config = {
      [this.createFormFields.title.name]: [
        this.templateConfiguration.configurations.title ?? '',
        [
          Validators.maxLength(
            this.createFormFields.title.validationParams.maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.logo.name]: [
        this.templateConfiguration.configurations.logo ?? '',
        [],
      ],

      [this.createFormFields.shortcutNotification.name]: [
        this.templateConfiguration.configurations.shortcutNotification ?? '',
        [
          Validators.maxLength(
            this.createFormFields.shortcutNotification.validationParams
              .maxLength as number
          ),
        ],
      ],
      [this.createFormFields.extendNotification.name]: [
        this.templateConfiguration.configurations.extendNotification ?? '',
        [
          Validators.maxLength(
            this.createFormFields.extendNotification.validationParams
              .maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.cta.name]: [
        this.templateConfiguration.configurations.cta ?? '',
        [
          Validators.maxLength(
            this.createFormFields.cta.validationParams.maxLength as number
          ),
        ],
      ],
      [this.createFormFields.ctaUrl.name]: [
        this.templateConfiguration.configurations.ctaUrl ?? '',
        [
          Validators.pattern(
            this.createFormFields.ctaUrl.validationParams.pattern
          ),
          Validators.maxLength(
            this.createFormFields.ctaUrl.validationParams.maxLength as number
          ),
        ],
      ],
      [this.createFormFields.images.name]: [
        this.templateConfiguration.configurations.images ?? [],
        [],
      ],
      [this.createFormFields.delay.name]: [
        this.templateConfiguration.configurations.delay ?? 3,
        [
          Validators.required,
          Validators.max(
            this.createFormFields.delay.validationParams.max as number
          ),
          Validators.min(
            this.createFormFields.delay.validationParams.min as number
          ),
        ],
      ],
      [this.createFormFields.isCta.name]: [
        this.templateConfiguration.configurations.isCta ?? true,
        [Validators.required],
      ],
      [this.createFormFields.isCustomImage.name]: [
        this.templateConfiguration.configurations.isCustomImage ?? true,
        [Validators.required],
      ],
      [this.createFormFields.isDelay.name]: [
        this.templateConfiguration.configurations.isDelay ?? true,
        [Validators.required],
      ],
      ['logoNotification']: [''],
      ['listBannerNotification']: [[]],
    };

    this.notificationBannerForm = this.formBuilder.group(config, {
      validators: this.notificationBannerFormValidator,
    });

    this.notificationBannerForm.valueChanges.subscribe((value) => {
      if (!this.title) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống tiêu đề',
          configurations: value,
        });
        return;
      }
      if (!this.extendNotification) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống thông báo mở rộng',
          configurations: value,
        });
        return;
      }

      if (this.isCta) {
        if (!this.cta) {
          this.templateConfigurationChange.emit({
            isValid: false,
            message: 'Không để trống CTA',
            configurations: value,
          });
          return;
        }
      }
      if (this.isDelay) {
        if (!this.delay) {
          this.templateConfigurationChange.emit({
            isValid: false,
            message: 'Không để trống thời gian trễ',
            configurations: value,
          });
          return;
        }
      }
      if (this.isCustomImage) {
        if (!this.images.value.length) {
          this.templateConfigurationChange.emit({
            isValid: false,
            message: 'Không để trống ảnh',
            configurations: value,
          });
          return;
        }
      }

      this.templateConfigurationChange.emit({
        isValid: this.notificationBannerForm.valid,
        message: !this.notificationBannerForm.valid
          ? 'Vui lòng điền trường dữ liệu bắt buộc'
          : '',
        configurations: value,
      });
    });
  }
}
