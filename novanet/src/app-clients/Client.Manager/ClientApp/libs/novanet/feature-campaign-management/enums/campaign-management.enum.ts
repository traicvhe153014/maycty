export enum CampaignManagementTableEnum {
  CHECKBOX,
  INDEX,
  IS_ACTIVE,
  TITLE,
  STATUS,
  CAMPAIGN_TYPE,
  BUDGET,
  COSTS,
  IMPRESSIONS,
  CLICKS,
  CTR,
  CPC,
  PURCHASE,
  PURCHASE_CONVERSION,
  CPS,
  VIDEO_VIEW_3S,
  VIDEO_25_PERCENT,
  VIDEO_50_PERCENT,
  VIDEO_75_PERCENT,
  VIDEO_100_PERCENT,
  DATE,
}

export enum CampaignManagementSummaryEnum {
  COUNT,
  STATUS=4,
  CAMPAIGN_TYPE,
  BUDGET,
  COSTS,
  IMPRESSIONS,
  CLICKS,
  CTR,
  CPC,
  PURCHASE,
  PURCHASE_CONVERSION,
  CPS,
  VIDEO_VIEW_3S,
  VIDEO_25_PERCENT,
  VIDEO_50_PERCENT,
  VIDEO_75_PERCENT,
  VIDEO_100_PERCENT,
  DATE,
}

export enum CampaignSearchOperatorEnum {
  Contains,
  NotContains,
}

export enum CampaignSearchLevelEnum {
  Campaign,
  Adset,
  Ad,
  Template,
}
