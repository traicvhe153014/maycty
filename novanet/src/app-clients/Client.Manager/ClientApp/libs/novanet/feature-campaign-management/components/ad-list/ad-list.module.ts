import { NgModule } from '@angular/core';
import { AdListComponent } from './ad-list.component';
import { CommonModule } from '@angular/common';
import {DataTableModule, FormatterModule, MapObjectModule} from '@core';
import { FormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { RouterModule } from '@angular/router';
import { AdNameItemModule } from './components';
import { SharedFilterModule } from '../shared-filter';
import { CampaignStatusIconModule } from '../campaign-status-icon';
import { NzSpinModule } from 'ng-zorro-antd/spin';

@NgModule({
  declarations: [AdListComponent],
    imports: [
        CommonModule,
        FormatterModule,
        FormsModule,
        SharedFilterModule,
        NovanetInputModule,
        DataTableModule,
        NzSelectModule,
        NzDropDownModule,
        NzSwitchModule,
        SvgIconModule,
        CampaignStatusIconModule,
        RouterModule,
        AdNameItemModule,
        NzSpinModule,
        MapObjectModule,
    ],
  exports: [AdListComponent],
})
export class AdListModule {}
