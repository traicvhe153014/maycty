import { NgModule } from '@angular/core';
import { CreateAdvertisingComponent } from './create-advertising.component';
import { CommonModule } from '@angular/common';
import { CreateAdvertisingRouting } from './create-advertising.routing';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { DataTableModule } from '@core';
import { CreateAdvertisingComponentsModule } from './components';
import { PipesModule } from '@shared/pipes';
import { SvgIconModule } from '@core/components/svg-icon';
import { NovanetInputModule } from '@shared/custom-input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { ProductManagementService } from '@features/product-management/store';
import { ProductGroupManagementService } from '@features/product-group-management/store';
import { CatfishModule } from '@shared/templates/catfish';
import { InteractiveBannerModule } from '@shared/templates/interactive-banner';
import { PostInReadModule } from '@shared/templates/post-in-read';
import { InreadEcommerceModule } from '@shared/templates/inread-ecommerce';
import { FlyingCarpetModule } from '@shared/templates/flying-carpet';
import { MobileBannerModule } from '@shared/templates/mobile-banner';
import { SettingDisplayAdvertisingModule } from './components/setting-display-advertising';
import { PopupBannerModule } from '@shared/templates/popup-banner';
import { CatfishCollabBrandingModule } from '@shared/templates/catfish-collab-branding';
import { NotificationBannerDisplayModule } from '@shared/templates/notification-banner';

const MODULES = [
  CommonModule,
  CreateAdvertisingRouting,
  CreateAdvertisingComponentsModule,
  NzCollapseModule,
  NzCheckboxModule,
  FormsModule,
  SvgIconModule,
  DataTableModule,
  ReactiveFormsModule,
  NzAffixModule,
  PipesModule,
  NovanetInputModule,
  NzButtonModule,
  CatfishModule,
  InteractiveBannerModule,
  PostInReadModule,
  InreadEcommerceModule,
  PopupBannerModule,
  CatfishCollabBrandingModule,
  NotificationBannerDisplayModule,
];

const COMPONENTS = [CreateAdvertisingComponent];

const SERVICES = [ProductManagementService, ProductGroupManagementService];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    ...MODULES,
    InreadEcommerceModule,
    FlyingCarpetModule,
    MobileBannerModule,
    SettingDisplayAdvertisingModule,
  ],
  exports: [...COMPONENTS],
  providers: [...SERVICES],
})
export class CreateAdvertisingModule {}
