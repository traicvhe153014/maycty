import {
  ExportReport,
  GlobalNotificationState,
} from '@shared/global-notification/store';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ISettingColumnTable } from '@data-table/models';
import {
  CampaignManagementSummaryEnum,
  CampaignManagementTableEnum,
} from '@features/campaign-management/enums';
import {
  CampaignState,
  CampaignStatusEnum,
  CampaignType,
  GetCampaignList,
  GetMoreCampaignList,
  ICampaignListRequest,
  ICampaignResponse,
  ICampaignSummary,
  ICampaignUpdateRequest,
  SelectCampaign,
  UpdateCampaign,
} from '@features/campaign-management/store/campaign';
import {
  CampaignGroupOptionColumn,
  CampaignListSetting,
  campaignListSummaryColumns,
  campaignManagementFilter,
  CampaignOptionColumnDefault,
  campaignTypeLabelList,
  campaignTypeLabels,
  MetricColumnEnumMappings,
  shortDateDayjsFormat,
} from '@features/campaign-management/constants';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { IPredicateModel } from '@core/models';
import { ICampaignManagementFilter } from '@features/campaign-management/models';
import { ESortType } from '@core/enums';
import { session, storage } from '@components';
import { ICampaignSortOption } from '@features/campaign-management/types';
import { metricSortFields } from '@core';

@Component({
  selector: 'novanet-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss'],
})
export class CampaignListComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;
  @ViewChild('budgetHeader') budgetHeader: TemplateRef<any>;
  @ViewChild('costHeader') costHeader: TemplateRef<any>;
  @ViewChild('impressionHeader') impressionHeader: TemplateRef<any>;
  @ViewChild('clickHeader') clickHeader: TemplateRef<any>;
  @ViewChild('ctrHeader') ctrHeader: TemplateRef<any>;
  @ViewChild('cpcHeader') cpcHeader: TemplateRef<any>;
  @ViewChild('purchaseHeader') purchaseHeader: TemplateRef<any>;
  @ViewChild('purchaseConversionHeader')
  purchaseConversionHeader: TemplateRef<any>;
  @ViewChild('cpsHeader') cpsHeader: TemplateRef<any>;
  @ViewChild('videoView3sHeader') videoView3sHeader: TemplateRef<any>;
  @ViewChild('videoWatchesAt25%Header')
  videoWatchesAt25Header: TemplateRef<any>;
  @ViewChild('videoWatchesAt50%Header')
  videoWatchesAt50Header: TemplateRef<any>;
  @ViewChild('videoWatchesAt75%Header')
  videoWatchesAt75Header: TemplateRef<any>;
  @ViewChild('videoWatchesAt100%Header')
  videoWatchesAt100Header: TemplateRef<any>;

  @Select(CampaignState.getList) campaigns$: Observable<ICampaignResponse[]>;
  @Select(CampaignState.getPage) currentPage$: Observable<number>;
  @Select(CampaignState.getLoading) loading$: Observable<boolean>;
  @Select(CampaignState.getSummary)
  campaignSummary$: Observable<ICampaignSummary>;
  @Select(CampaignState.getSelected)
  selectedCampaignIds$: Observable<string[]>;
  @Select(GlobalNotificationState.getCancelDownloading)
  cancelDownloading$: Observable<boolean>;
  public readonly settings = CampaignListSetting;
  public readonly campaignManagementTableEnum = CampaignManagementTableEnum;
  public readonly campaignManagementSummaryEnum = CampaignManagementSummaryEnum;
  public readonly shortDateFormat = shortDateDayjsFormat;
  public readonly campaignMetricSortField = metricSortFields;
  public readonly campaignTypeLabels = campaignTypeLabels;
  public readonly campaignTypeLabelList = campaignTypeLabelList;
  public readonly pageSize = 24;
  public columns: ISettingColumnTable<ICampaignResponse, any>[] = [];
  public summaryColumns = campaignListSummaryColumns;
  public selectedCampaigns: string[] = [];
  public filterDateRange: Date[] = [];
  public filterStatus: CampaignStatusEnum | undefined = undefined;
  public filterPredicates: IPredicateModel[] = [];
  public filterCampaignType: CampaignType | undefined = undefined;
  public filterSelectedColumns: number[] = [];
  public sort = '+status,-isActive,-modifiedAt';
  public switchLoadings: { [key: string]: boolean } = {};
  public campaignGroupOptionColumn = CampaignGroupOptionColumn;
  public columnsConstant: number[] | [];
  public summaryColumnsConstant: number[] | [];
  public columnsDefault: ISettingColumnTable<ICampaignResponse, any>[] = [];
  public summaryColumnsDefault = campaignListSummaryColumns;
  public selectedColumnDefault = CampaignOptionColumnDefault;
  exportExcelSubscription: Subscription;
  private readonly sortDefault = '+status,-isActive,-modifiedAt';
  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private store: Store
  ) {
    this.columnsConstant = [
      CampaignManagementTableEnum.CHECKBOX,
      CampaignManagementTableEnum.INDEX,
      CampaignManagementTableEnum.IS_ACTIVE,
      CampaignManagementTableEnum.TITLE,
      CampaignManagementTableEnum.STATUS,
      CampaignManagementTableEnum.CAMPAIGN_TYPE,
      CampaignManagementTableEnum.DATE,
    ];
    this.summaryColumnsConstant = [
      CampaignManagementSummaryEnum.COUNT,
      CampaignManagementSummaryEnum.STATUS,
      CampaignManagementSummaryEnum.CAMPAIGN_TYPE,
      CampaignManagementSummaryEnum.DATE,
    ];
  }

  public get isAnySelected() {
    return this.selectedCampaigns.length > 0;
  }

  ngOnInit() {
    if (session.get('tableCampaignSelected')) {
      this.store.dispatch(
        new SelectCampaign(session.get('tableCampaignSelected'), true)
      );
    }
    this.selectedCampaignIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (value) => {
        this.selectedCampaigns = value;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onNextPage() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMoreCampaignList({
            filters: this.filterPredicates,
            page: currentPage + 1,
            pageSize: this.pageSize,
            startDate: this.filterDateRange[0],
            endDate: this.filterDateRange[1],
            status: this.filterStatus,
            sorts: this.sort,
            campaignType: this.filterCampaignType,
            fields: this.filterSelectedColumns
              .map(
                (item) =>
                  MetricColumnEnumMappings[CampaignManagementTableEnum[item]]
              )
              .filter((item) => !!item),
          })
        );
      },
    });
  }

  public onSelectCampaignChanged(event: Event, id: string) {
    let updatedCampaignIds: string[] = [];
    if (this.isCampaignSelected(id)) {
      updatedCampaignIds = this.selectedCampaigns.filter(
        (campaignId) => campaignId !== id
      );
    } else {
      updatedCampaignIds = [...this.selectedCampaigns, id];
    }
    session.set('tableCampaignSelected', updatedCampaignIds);
    this.store.dispatch(new SelectCampaign(updatedCampaignIds));
    this.changeDetectorRef.detectChanges();
  }

  public onSelectAllCampaignChanged(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.campaigns$.pipe(take(1)).subscribe({
        next: (campaigns) => {
          const updatedCampaignIds = campaigns.map((campaign) => campaign.id);
          this.store.dispatch(new SelectCampaign(updatedCampaignIds));
          session.set('tableCampaignSelected', updatedCampaignIds);
        },
      });
    } else {
      this.store.dispatch(new SelectCampaign([]));
      session.set('tableCampaignSelected', []);
    }
  }

  public isCampaignSelected(id: string) {
    return !!this.selectedCampaigns.find((campaignId) => campaignId === id);
  }

  ngAfterViewInit(): void {
    this.columnsDefault = [
      {
        id: CampaignManagementTableEnum.CHECKBOX,
        title: this.checkboxHeader,
        align: 'center',
        pinLeft: true,
        width: '50px',
      },
      {
        id: CampaignManagementTableEnum.INDEX,
        title: 'Stt',
        pinLeft: true,
        width: '50px',
      },
      {
        id: CampaignManagementTableEnum.IS_ACTIVE,
        title: 'Bật/tắt',
        pinLeft: true,
        width: '70px',
        sort: true,
        name: 'IsActive',
        ascending: false,
        descending: true,
      },
      {
        id: CampaignManagementTableEnum.TITLE,
        title: 'Tiêu đề',
        pinLeft: true,
        sort: true,
        width: '400px',
        name: 'Name',
      },
      {
        id: CampaignManagementTableEnum.STATUS,
        title: 'Trạng thái',
        sort: true,
        width: '125px',
        name: 'Status',
        ascending: true,
        descending: false,
      },
      {
        id: CampaignManagementTableEnum.CAMPAIGN_TYPE,
        title: 'Loại QC',
        sort: false,
        width: '125px',
      },
      {
        id: CampaignManagementTableEnum.BUDGET,
        title: this.budgetHeader,
        width: '170px',
      },
      {
        id: CampaignManagementTableEnum.COSTS,
        title: this.costHeader,
        width: '150px',
      },
      {
        id: CampaignManagementTableEnum.IMPRESSIONS,
        title: this.impressionHeader,
        width: '125px',
      },
      {
        id: CampaignManagementTableEnum.CLICKS,
        title: this.clickHeader,
        width: '125px',
      },
      {
        id: CampaignManagementTableEnum.CTR,
        title: this.ctrHeader,
        width: '100px',
      },
      {
        id: CampaignManagementTableEnum.CPC,
        title: this.cpcHeader,
        width: '125px',
      },
      {
        id: CampaignManagementTableEnum.PURCHASE,
        title: this.purchaseHeader,
        width: '125px',
      },
      {
        id: CampaignManagementTableEnum.PURCHASE_CONVERSION,
        title: this.purchaseConversionHeader,
        width: '175px',
      },
      {
        id: CampaignManagementTableEnum.CPS,
        title: this.cpsHeader,
        width: '125px',
      },
      {
        id: CampaignManagementTableEnum.VIDEO_VIEW_3S,
        title: this.videoView3sHeader,
        width: '160px',
      },
      {
        id: CampaignManagementTableEnum.VIDEO_25_PERCENT,
        title: this.videoWatchesAt25Header,
        width: '160px',
      },
      {
        id: CampaignManagementTableEnum.VIDEO_50_PERCENT,
        title: this.videoWatchesAt50Header,
        width: '160px',
      },
      {
        id: CampaignManagementTableEnum.VIDEO_75_PERCENT,
        title: this.videoWatchesAt75Header,
        width: '160px',
      },
      {
        id: CampaignManagementTableEnum.VIDEO_100_PERCENT,
        title: this.videoWatchesAt100Header,
        width: '160px',
      },
      {
        id: CampaignManagementTableEnum.DATE,
        title: 'Bắt đầu/kết thúc',
        width: '125px',
      },
    ];

    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (
      filter &&
      filter.selectedColumnCampaign &&
      filter.selectedColumnCampaign.length > 0
    ) {
      this.selectedColumnDefault = filter.selectedColumnCampaign;
      this.updateColumns(filter.selectedColumnCampaign);
    } else {
      this.updateColumns(CampaignOptionColumnDefault);
    }
    this.changeDetectorRef.detectChanges();
  }

  public createReport() {
    // TODO: [89] create OnReport report
  }

  public showAdGroup() {
    // TODO: [89] show Ad Group
  }

  public showAd() {
    // TODO: [89] show Ad
  }

  // FILTER CHANGE
  public onFilterPredicatesChange(predicates: IPredicateModel[]) {
    this.filterPredicates = predicates;
    this.dispatchGetList(1);
  }

  public onFilterStatusChange(status) {
    this.filterStatus = status;
    this.dispatchGetList(1);
  }

  public onFilterCampaignTypeChange(campaignType: CampaignType) {
    this.filterCampaignType = campaignType;
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.campaignType = campaignType;
    } else {
      filter = {
        campaignType,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.dispatchGetList(1);
  }

  public onFilterDateRangeChange(result: Date[]) {
    this.filterDateRange = result;
    this.dispatchGetList(1);
  }

  public onAllFilterChange(result: ICampaignManagementFilter) {
    this.filterDateRange = result.dateRange;
    this.filterStatus = result.status;
    this.filterPredicates = result.predicates;
    this.filterCampaignType = result.campaignType;
    this.filterSelectedColumns = result.selectedColumnCampaign;
    this.dispatchGetList(1);
  }

  // FILTER CHANGE END

  public onCampaignIsActiveChange(campaign: ICampaignResponse) {
    if (this.switchLoadings[campaign.id]) {
      return;
    }
    const payload: Partial<ICampaignUpdateRequest> = {
      id: campaign.id,
      name: campaign.name,
      isActive: !campaign.isActive,
      isEcommerceUpdating: false,
    };
    this.switchLoadings[campaign.id] = true;
    this.changeDetectorRef.detectChanges();
    this.store.dispatch(new UpdateCampaign(payload)).subscribe({
      next: (response) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.switchLoadings[campaign.id] = false;
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.switchLoadings[campaign.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public sortMetric(event: ICampaignSortOption) {
    const sortField =
      (event.type === ESortType.Descending ? '-' : '') + event.field;
    if (this.sort === sortField) {
      this.sort = '';
    } else {
      this.columns.forEach((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sort = sortField;
    }
    this.dispatchGetList(1);
  }

  public dispatchGetList(page: number, value?, statusCustomColumn?: boolean) {
    if (value) {
      const sortedColumn = value[value.length - 1];
      this.columns.forEach((column) => {
        if (!sortedColumn || sortedColumn?.name !== column.name) {
          column.ascending = false;
          column.descending = false;
        } else {
          switch (sortedColumn.direction) {
            case ESortType.Ascending:
              this.sort = sortedColumn.name;
              column.ascending = true;
              column.descending = false;
              break;
            case ESortType.Descending:
              this.sort = '-' + sortedColumn.name;
              column.ascending = false;
              column.descending = true;
              break;
          }
        }
      });
      if (!sortedColumn) {
        this.sort = '';
      }
    }

    if (statusCustomColumn) {
      this.sort = this.sortDefault;
    }
    this.store.dispatch(
      new GetCampaignList({
        filters: this.filterPredicates,
        page,
        pageSize: this.pageSize,
        startDate: this.filterDateRange[0],
        endDate: this.filterDateRange[1],
        status: this.filterStatus,
        sorts: this.sort,
        campaignType: this.filterCampaignType,
        fields: this.filterSelectedColumns
          .map(
            (item) =>
              MetricColumnEnumMappings[CampaignManagementTableEnum[item]]
          )
          .filter((item) => !!item),
      })
    );
  }

  public exportData($event) {
    const param = {
      startDate: this.filterDateRange[0],
      endDate: this.filterDateRange[1],
      exportType: $event.exportType,
      campaignIds: this.selectedCampaigns,
    } as ICampaignListRequest;

    this.store.dispatch(new ExportReport(param));
  }

  public updateColumns(columnUpdates: number[]) {
    const columnUpdate = [...columnUpdates, ...this.columnsConstant];
    const summaryColumnUpdate = [
      ...columnUpdates,
      ...this.summaryColumnsConstant,
    ];
    let columns = [];
    let summaryColumns = [];
    this.columnsDefault.forEach((col) => {
      columnUpdate.forEach((item, index) => {
        if (col.id === item) {
          columns.push(col);
        }
      });
    });
    this.summaryColumnsDefault.forEach((colSum) => {
      summaryColumnUpdate.forEach((item, index) => {
        if (colSum.id === item) {
          summaryColumns.push(colSum);
        }
      });
    });
    this.columns = columns;
    this.summaryColumns = summaryColumns;
    this.filterSelectedColumns = columns.map((item) => item.id);

    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.selectedColumnCampaign = columnUpdates;
    } else {
      filter = {
        selectedColumnCampaign: columnUpdates,
      };
    }
    storage.set(campaignManagementFilter, filter);

    this.dispatchGetList(1, undefined, true);
    this.changeDetectorRef.detectChanges();
  }
}
