import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import {
  ICampaignResponse,
  ICampaignUpdateRequest,
  NavigateCampaignNextLevel,
  UpdateCampaign,
} from '@features/campaign-management/store/campaign';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';

@Component({
  selector: 'novanet-campaign-editable-title',
  templateUrl: './editable-campaign-title.component.html',
})
export class EditableCampaignTitleComponent {
  @Input() campaign: ICampaignResponse;
  @Input() isAnySelected: boolean;
  public isEditing = false;
  public nameEditing: string;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public setEditing() {
    this.isEditing = true;
    this.nameEditing = this.campaign.name;
  }

  public onNameEditingKeyUp(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.saveEditing();
    }
  }

  public cancelEditing() {
    this.isEditing = false;
  }

  public saveEditing() {
    if (this.nameEditing === this.campaign.name || !this.nameEditing) {
      this.isEditing = false;
      return;
    }
    if (this.nameEditing.length > 150) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          'Số lượng kí tự tối đa là 150 kí tự'
        )
      );
      return;
    }
    const payload: Partial<ICampaignUpdateRequest> = {
      id: this.campaign.id,
      name: this.nameEditing,
      isActive: this.campaign.isActive,
      isEcommerceUpdating: false,
    };
    this.store.dispatch(new UpdateCampaign(payload)).subscribe({
      next: (response) => {
        this.isEditing = false;
        this.changeDetectorRef.detectChanges();
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công',
            'Cập nhật tên chiến dịch thành công'
          )
        );
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Thất bại',
            'Có lỗi xảy ra trong quá trình cập nhật tên chiến dịch'
          )
        );
      },
    });
  }

  public navigateToNextLevel() {
    this.store.dispatch(new NavigateCampaignNextLevel(this.campaign?.id));
  }
}
