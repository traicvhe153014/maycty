import { IPredicateModel } from '@core/models';
import { EDayOfWeek } from '@shared/enums';
import { ReportOptions } from '@features/campaign-management/enums';
import { ExportApi } from '@shared/global-notification';

export enum CampaignStatusEnum {
  NotStarted = 0,
  Running = 1,
  Paused = 2,
  Error = 3,
  Finished = 4,
  Unfinished = 5,
}

export enum CampaignType {
  Ecommerce,
  Display,
  Native,
}

export type CampaignStatusValue = keyof typeof CampaignStatusEnum;
export type CampaignTypeValue = keyof typeof CampaignType;

export interface ICampaignListRequest {
  pageSize: number;
  page: number;
  filters?: IPredicateModel[];
  status?: CampaignStatusEnum;
  startDate?: Date;
  endDate?: Date;
  withSummary?: boolean;
  withReport?: boolean;
  sorts?: string;
  exportType?: ReportOptions;
  campaignIds?: [];
  advertisingSetIds?: [];
  advertisingIds?: [];
  exportApi?: ExportApi;
  campaignType?: CampaignType;
  fields?: string[];
}

export interface ICampaignResponse {
  id: string;
  isActive: boolean;
  name: string;
  status: CampaignStatusEnum;
  startDate: string;
  endDate: string;
  data: {
    dailyBudget: number;
    totalBudget: number;
    dailyCost: number;
    totalCost: number;
    dailyImpressions: number;
    totalImpressions: number;
    dailyClicks: number;
    totalClicks: number;
    dailyCtr: number;
    totalCtr: number;
    dailyCpc: number;
    totalCpc: number;
    dailyPurchase: number;
    totalPurchase: number;
    dailyPurchaseConversion: number;
    totalPurchaseConversion: number;
    dailyCps: number;
    totalCps: number;
  };
}

export interface ICampaignSummary {
  [key: string]: any;
}

export interface ICampaignListResponse {
  data: ICampaignResponse[];
  summary?: ICampaignSummary;
}

export interface ICampaignCreateRequest {
  name: string;
  campaignType: CampaignTypeValue;
  ecommerceAmountSpent: number;
  ecommerceDailyBudget: number;
  ecommerceProductFeedId: string;
  ecommerceStartDate: Date;
  ecommerceEndDate?: Date;
  ecommerceScheduled?: boolean;
  ecommerceScheduledAllTime?: boolean;
  schedulings?: Partial<ICampaignScheduling>[];
}

export interface ICampaignGetResponse {
  id: string;
  name: string;
  campaignType: CampaignType;
  ecommerceAmountSpent: number;
  ecommerceDailyBudget: number;
  ecommerceProductFeedId: string;
  ecommerceStartDate: Date;
  ecommerceEndDate?: Date;
  ecommerceScheduled?: boolean;
  ecommerceScheduledAllTime?: boolean;
  campaignScheduling?: Partial<ICampaignSchedulingResponse>[];
  totalCost: number;
}

export interface ICampaignSchedulingResponse {
  id: string;
  campaignId: string;
  dayOfWeek: EDayOfWeek;
  timing: string;
}

export interface ICampaignScheduling {
  id: string;
  campaignId: string;
  dayOfWeek: EDayOfWeek;
  timing: Date;
}

export interface ICampaignUpdateRequest {
  id: string;
  name: string;
  isActive: boolean;
  campaignType: CampaignTypeValue;
  ecommerceAmountSpent: number;
  ecommerceDailyBudget: number;
  ecommerceProductFeedId: string;
  ecommerceStartDate: Date;
  ecommerceEndDate?: Date;
  ecommerceScheduled?: boolean;
  ecommerceScheduledAllTime?: boolean;
  campaignScheduling?: Partial<ICampaignScheduling>[];
  isEcommerceUpdating: boolean;
}

export interface ICampaignStateModel {
  campaigns: ICampaignResponse[];
  campaignSummary?: ICampaignSummary;
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  detailedCampaign?: ICampaignGetResponse;
  selectedCampaignIds: string[];
  navigatedCampaignId?: string;
}
