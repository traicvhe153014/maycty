import {
  CampaignStatusEnum,
  CampaignType,
} from '@features/campaign-management/store';
import { IPredicateModel } from '@core/models';
import { ReportOptions } from '@features/campaign-management/enums';

export interface ICampaignManagementFilter {
  status?: CampaignStatusEnum;
  dateRange?: Date[];
  predicates?: IPredicateModel[];
  exportType?: ReportOptions;
  campaignType?: CampaignType;
  selectedColumnCampaign?: number[];
  selectedColumnAdset?: number[];
  selectedColumnAdvertising?: number[];
}
