import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { ETemplateType } from '@features/campaign-management/enums';
import {
  ITemplateConfiguration,
  ITemplateDisplayConfiguration,
} from '@features/campaign-management/store';
import { Store } from '@ngxs/store';
import {
  CatfishCollapseBrandingComponent,
  NotificationBannerComponent,
  PopupBannerBrandingComponent,
} from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/components';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'novanet-setting-display-advertising',
  templateUrl: './setting-display-advertising.component.html',
})
export class SettingDisplayAdvertisingComponent {
  @Input() public height = 0;
  @Input() public width = 0;
  @Input() public dataTable = [];
  @Input() public templateType: ETemplateType;
  @Input() public templateConfiguration: ITemplateConfiguration;
  @Output() public templateConfigurationChange =
    new EventEmitter<ITemplateDisplayConfiguration>();

  @ViewChild(CatfishCollapseBrandingComponent)
  catfishCollapseBrandingComponent: CatfishCollapseBrandingComponent;
  @ViewChild(NotificationBannerComponent)
  notificationBannerComponent: NotificationBannerComponent;
  @ViewChild(PopupBannerBrandingComponent)
  popupBannerBrandingComponent: PopupBannerBrandingComponent;

  public templateTypes = ETemplateType;
  public expandedContent: File | null = null;

  constructor(private store: Store) {}

  public get getForm(): FormGroup | null {
    switch (this.templateType) {
      case ETemplateType.NotificationBanner:
        return this.notificationBannerComponent?.notificationBannerForm;
      case ETemplateType.CatFishCollabBranding:
        return this.catfishCollapseBrandingComponent?.notificationBannerForm;
      case ETemplateType.PopupBannerBranding:
        return this.popupBannerBrandingComponent?.notificationBannerForm;
      default:
        return null;
    }
  }

  public handleBannerInput($event: ITemplateDisplayConfiguration) {
    this.templateConfigurationChange.emit($event);
  }

  public validateForm(shouldTouch: boolean = true): boolean {
    const form = this.getForm;
    if (!form) {
      return false;
    }
    if (shouldTouch) {
      form.markAllAsTouched();
    }
    let isValid = form.valid;
    isValid = isValid && !!form.get('logo').value;
    switch (this.templateType) {
      case ETemplateType.NotificationBanner:
        if (form.get('isCustomImage').value) {
          isValid = isValid && form.get('images').value.length;
        }
        break;
      case ETemplateType.CatFishCollabBranding:
        isValid =
          isValid &&
          !!form.get('collapseBanner').value &&
          !!form.get('extendBanner').value;
        break;
      case ETemplateType.PopupBannerBranding:
        isValid = isValid && !!form.get('extendBanner').value;
        break;
    }
    return isValid;
  }
}
