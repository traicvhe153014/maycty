export enum EAdsetAddRemarketingOption {
  MORE_MARKETING = 1,
  NO_MORE_MARKETING,
  ONLY_MARKETING,
}
