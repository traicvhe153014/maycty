import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  IAdvertisingCreateRequest,
  IAdvertisingListRequest,
  IAdvertisingListResponse,
  IAdvertisingResponse,
  IAdvertisingTemplate,
  IAdvertisingUpdateRequest,
} from '@features/campaign-management/store';
import {
  IAdvertisingTemplateReportRequest,
  IAdvertisingTemplateReportResponse,
} from '@features/feature-report/store';

const AdvertisingUrls = {
  get: `advertising/get`,
  getList: `advertising/list`,
  listTemplate: `advertising/listTemplate`,
  create: 'advertising/create',
  update: 'advertising/update',
  uploadFile: 'Advertising/UploadFile',
  advertisingTemplate: 'Advertising/ReportTemplate',
};

@Injectable()
export class AdvertisingService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public getList(
    params: IAdvertisingListRequest
  ): Observable<IAdvertisingListResponse> {
    const processedParams = {
      ...params,
      startDate: params.startDate?.toISOString(),
      endDate: params.endDate?.toISOString(),
    };
    return this.httpClient
      .post<ISuccessHttpResponse<IAdvertisingListResponse>>(
        this.createUrl(AdvertisingUrls.getList),
        processedParams
      )
      .pipe(map((response) => response.data));
  }

  public get(advertisingId: string): Observable<IAdvertisingResponse> {
    const params = new HttpParams({
      fromObject: {
        id: advertisingId,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IAdvertisingResponse>>(
        this.createUrl(AdvertisingUrls.get),
        { params }
      )
      .pipe(map((response) => response.data));
  }

  public listTemplate(): Observable<IAdvertisingTemplate[]> {
    return this.httpClient
      .get<ISuccessHttpResponse<IAdvertisingTemplate[]>>(
        this.createUrl(AdvertisingUrls.listTemplate)
      )
      .pipe(map((response) => response.data));
  }

  public getAdvertisingTemplateReport(
    payload: IAdvertisingTemplateReportRequest
  ): Observable<IAdvertisingTemplateReportResponse> {
    const processedPayload: { [key: string]: any } = { ...payload };
    Object.keys(processedPayload).forEach(
      (key) =>
        (processedPayload[key] === undefined ||
          processedPayload[key] === null) &&
        delete processedPayload[key]
    );
    if (processedPayload.startDate) {
      processedPayload.startDate = processedPayload.startDate.toISOString();
    }
    if (processedPayload.endDate) {
      processedPayload.endDate = processedPayload.endDate.toISOString();
    }
    if (processedPayload.templateTypes) {
      processedPayload.templateTypes = processedPayload.templateTypes.join(',');
    }
    const params = new HttpParams({
      fromObject: processedPayload,
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IAdvertisingTemplateReportResponse>>(
        this.createUrl(AdvertisingUrls.advertisingTemplate),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public create(
    payload: IAdvertisingCreateRequest
  ): Observable<IAdvertisingResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IAdvertisingResponse>>(
        this.createUrl(AdvertisingUrls.create),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public update(
    payload: IAdvertisingUpdateRequest
  ): Observable<IAdvertisingResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IAdvertisingResponse>>(
        this.createUrl(AdvertisingUrls.update),
        payload
      )
      .pipe(map((response) => response.data));
  }
}
