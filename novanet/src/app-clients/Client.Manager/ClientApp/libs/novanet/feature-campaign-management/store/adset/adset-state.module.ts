import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { AdsetService } from './adset-state.service';
import { AdsetState } from './adset.state';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [NgxsModule.forFeature([AdsetState]), RouterModule],
  providers: [AdsetService],
})
export class AdsetStateModule {}
