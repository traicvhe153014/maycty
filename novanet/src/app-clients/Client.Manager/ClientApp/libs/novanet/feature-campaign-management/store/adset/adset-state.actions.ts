import {
  IAdsetByIdResponse,
  IAdsetCreateRequest,
  IAdsetListRequest,
  IAdsetListResponse,
  IAdsetProductGroup,
  IAdsetResponse,
  IAdsetUpdateProductGroupRequest,
  IAdsetUpdateRequest,
} from './adset-state.model';

const enum AdsetActions {
  GetAdsetList = '[Adset] Get List',
  GetAdsetListSuccess = '[Adset] Get List Success',
  GetAdsetListError = '[Adset] Get List Error',
  GetMoreAdsetList = '[Adset] Get More List',
  GetMoreAdsetListSuccess = '[Adset] Get More List Success',
  GetMoreAdsetListError = '[Adset] Get More List Error',
  GetAdset = '[Adset] Get',
  GetAdsetSuccess = '[Adset] Get Success',
  GetAdsetError = '[Adset] Get Error',
  CreateAdset = '[Adset] Create',
  CreateAdsetSuccess = '[Adset] Create Success',
  CreateAdsetError = '[Adset] Create Error',
  UpdateAdset = '[Adset] Update',
  UpdateAdsetSuccess = '[Adset] Update Success',
  UpdateAdsetError = '[Adset] Update Error',
  UpdateAdsetProductGroup = '[Adset] Update Product Group',
  UpdateAdsetProductGroupSuccess = '[Adset] Update Product Group Success',
  UpdateAdsetProductGroupError = '[Adset] Update Product Group Error',
  SelectAdset = '[Adset] Select',
  ClearAdset = '[Adset] Clear',
  NavigateAdsetNextLevel = '[Adset] Navigate Next Level',
}

export class GetAdsetList {
  public static readonly type = AdsetActions.GetAdsetList;

  constructor(public params: IAdsetListRequest) {}
}

export class GetAdsetListSuccess {
  public static readonly type = AdsetActions.GetAdsetListSuccess;

  constructor(
    public response: IAdsetListResponse,
    public params: IAdsetListRequest
  ) {}
}

export class GetAdsetListError {
  public static readonly type = AdsetActions.GetAdsetListError;

  constructor(public error: any) {}
}

export class GetMoreAdsetList {
  public static readonly type = AdsetActions.GetMoreAdsetList;

  constructor(public params: IAdsetListRequest) {}
}

export class GetMoreAdsetListSuccess {
  public static readonly type = AdsetActions.GetMoreAdsetListSuccess;

  constructor(
    public response: IAdsetListResponse,
    public params: IAdsetListRequest
  ) {}
}

export class GetMoreAdsetListError {
  public static readonly type = AdsetActions.GetMoreAdsetListError;

  constructor(public error: any) {}
}

export class GetAdset {
  public static readonly type = AdsetActions.GetAdset;

  constructor(public id: string) {}
}

export class GetAdsetSuccess {
  public static readonly type = AdsetActions.GetAdsetSuccess;

  constructor(public adset: IAdsetByIdResponse) {}
}

export class GetAdsetError {
  public static readonly type = AdsetActions.GetAdsetError;

  constructor(public error: any) {}
}

export class CreateAdset {
  public static readonly type = AdsetActions.CreateAdset;

  constructor(public payload: IAdsetCreateRequest) {}
}

export class CreateAdsetSuccess {
  public static readonly type = AdsetActions.CreateAdsetSuccess;

  constructor(public adset: IAdsetResponse) {}
}

export class CreateAdsetError {
  public static readonly type = AdsetActions.CreateAdsetError;

  constructor(public error: any) {}
}

export class UpdateAdset {
  public static readonly type = AdsetActions.UpdateAdset;

  constructor(public payload: Partial<IAdsetUpdateRequest>) {}
}

export class UpdateAdsetSuccess {
  public static readonly type = AdsetActions.UpdateAdsetSuccess;

  constructor(public adset: IAdsetResponse) {}
}

export class UpdateAdsetError {
  public static readonly type = AdsetActions.UpdateAdsetError;

  constructor(public error: any) {}
}

export class UpdateAdsetProductGroup {
  public static readonly type = AdsetActions.UpdateAdsetProductGroup;

  constructor(public payload: IAdsetUpdateProductGroupRequest) {}
}

export class UpdateAdsetProductGroupSuccess {
  public static readonly type = AdsetActions.UpdateAdsetProductGroupSuccess;

  constructor(public response: IAdsetProductGroup) {}
}

export class UpdateAdsetProductGroupError {
  public static readonly type = AdsetActions.UpdateAdsetProductGroupError;

  constructor(public error: any) {}
}

export class SelectAdset {
  public static readonly type = AdsetActions.SelectAdset;

  constructor(public adsetIds: string[], public currentLevelOnly = false) {}
}

export class ClearAdset {
  public static readonly type = AdsetActions.ClearAdset;

  constructor() {}
}

export class NavigateAdsetNextLevel {
  public static readonly type = AdsetActions.NavigateAdsetNextLevel;

  constructor(public adsetId: string) {}
}
