import { CampaignStatusEnum } from '@features/campaign-management/store/campaign';
import {
  AdListTableEnum,
  AdsetListTableEnum,
  CampaignManagementTableEnum,
  ReportOptions,
} from '@features/campaign-management/enums';
import { IOptionGroup } from '@shared/custom-input/components/multiple-select-input/models/multiple-select-input.model';

export const CampaignStatusOptions = [
  {
    value: CampaignStatusEnum.NotStarted,
    label: 'Chưa chạy',
  },
  {
    value: CampaignStatusEnum.Running,
    label: 'Đang chạy',
  },
  {
    value: CampaignStatusEnum.Paused,
    label: 'Tạm dừng',
  },
  {
    value: CampaignStatusEnum.Error,
    label: 'Lỗi',
  },
  {
    value: CampaignStatusEnum.Finished,
    label: 'Hoàn thành',
  },
  {
    value: CampaignStatusEnum.Unfinished,
    label: 'Chưa hoàn thành',
  },
];

export const CampaignGenerateReportOptions = [
  {
    valueKey: ReportOptions.GeneralCampaignReport,
    label: 'Tải xuống tổng quan chiến dịch',
  },
  {
    valueKey: ReportOptions.DetailCampaignReport,
    label: 'Tải xuống chi tiết chiến dịch',
  },
];
export const AdGroupGenerateReportOptions = [
  {
    valueKey: ReportOptions.AdGroupReport,
    label: 'Tải xuống báo cáo nhóm QC',
  },
];
export const AdvertisementGenerateReportOptions = [
  {
    valueKey: ReportOptions.AdvertisementReport,
    label: 'Tải xuống báo cáo tin QC',
  },
];

export const CampaignGroupOptionColumn = [
  {
    labelGroup: 'Chọn bảng giá hiện thị',
    nzOptions: [
      {
        label: 'Ngân sách tổng',
        value: CampaignManagementTableEnum.BUDGET,
      },
      {
        label: 'Tổng chi phí',
        value: CampaignManagementTableEnum.COSTS,
      },
      {
        label: 'Lượt xem',
        value: CampaignManagementTableEnum.IMPRESSIONS,
      },
      {
        label: 'Lượt nhấn',
        value: CampaignManagementTableEnum.CLICKS,
      },
      {
        label: 'CTR',
        value: CampaignManagementTableEnum.CTR,
      },
      {
        label: 'CPC',
        value: CampaignManagementTableEnum.CPC,
      },
    ],
  },
  {
    labelGroup: 'Video view',
    nzOptions: [
      {
        label: 'Video view 3s',
        value: CampaignManagementTableEnum.VIDEO_VIEW_3S,
      },
      {
        label: 'Video watches at 25%',
        value: CampaignManagementTableEnum.VIDEO_25_PERCENT,
      },
      {
        label: 'Video watches at 50%',
        value: CampaignManagementTableEnum.VIDEO_50_PERCENT,
      },
      {
        label: 'Video watches at 75%',
        value: CampaignManagementTableEnum.VIDEO_75_PERCENT,
      },
      {
        label: 'Video watches at 100%',
        value: CampaignManagementTableEnum.VIDEO_100_PERCENT,
      },
    ],
  },
  {
    labelGroup: 'Ecommerce',
    nzOptions: [
      {
        label: 'SL mua hàng',
        value: CampaignManagementTableEnum.PURCHASE,
      },
      {
        label: '% chuyển đổi mua hàng',
        value: CampaignManagementTableEnum.PURCHASE_CONVERSION,
      },
      {
        label: 'CPS',
        value: CampaignManagementTableEnum.CPS,
      },
    ],
  },
] as IOptionGroup[];

export const AdsetGroupOptionColumn = [
  {
    labelGroup: 'Chọn bảng giá hiện thị',
    nzOptions: [
      {
        label: 'Ngân sách tổng',
        value: AdsetListTableEnum.BUDGET,
      },
      {
        label: 'Tổng chi phí',
        value: AdsetListTableEnum.COSTS,
      },
      {
        label: 'Lượt xem',
        value: AdsetListTableEnum.IMPRESSIONS,
      },
      {
        label: 'Lượt nhấn',
        value: AdsetListTableEnum.CLICKS,
      },
      {
        label: 'CTR',
        value: AdsetListTableEnum.CTR,
      },
      {
        label: 'CPC',
        value: AdsetListTableEnum.CPC,
      },
    ],
  },
  {
    labelGroup: 'Video view',
    nzOptions: [
      {
        label: 'Video view 3s',
        value: AdsetListTableEnum.VIDEO_VIEW_3S,
      },
      {
        label: 'Video watches at 25%',
        value: AdsetListTableEnum.VIDEO_25_PERCENT,
      },
      {
        label: 'Video watches at 50%',
        value: AdsetListTableEnum.VIDEO_50_PERCENT,
      },
      {
        label: 'Video watches at 75%',
        value: AdsetListTableEnum.VIDEO_75_PERCENT,
      },
      {
        label: 'Video watches at 100%',
        value: AdsetListTableEnum.VIDEO_100_PERCENT,
      },
    ],
  },
  {
    labelGroup: 'Ecommerce',
    nzOptions: [
      {
        label: 'SL mua hàng',
        value: AdsetListTableEnum.PURCHASE,
      },
      {
        label: '% chuyển đổi mua hàng',
        value: AdsetListTableEnum.PURCHASE_CONVERSION,
      },
      {
        label: 'CPS',
        value: AdsetListTableEnum.CPS,
      },
    ],
  },
] as IOptionGroup[];

export const AdGroupOptionColumn = [
  {
    labelGroup: 'Chọn bảng giá hiện thị',
    nzOptions: [
      {
        label: 'Ngân sách tổng',
        value: AdListTableEnum.BUDGET,
      },
      {
        label: 'Tổng chi phí',
        value: AdListTableEnum.COSTS,
      },
      {
        label: 'Lượt xem',
        value: AdListTableEnum.IMPRESSIONS,
      },
      {
        label: 'Lượt nhấn',
        value: AdListTableEnum.CLICKS,
      },
      {
        label: 'CTR',
        value: AdListTableEnum.CTR,
      },
      {
        label: 'CPC',
        value: AdListTableEnum.CPC,
      },
    ],
  },
  {
    labelGroup: 'Video view',
    nzOptions: [
      {
        label: 'Video view 3s',
        value: AdListTableEnum.VIDEO_VIEW_3S,
      },
      {
        label: 'Video watches at 25%',
        value: AdListTableEnum.VIDEO_25_PERCENT,
      },
      {
        label: 'Video watches at 50%',
        value: AdListTableEnum.VIDEO_50_PERCENT,
      },
      {
        label: 'Video watches at 75%',
        value: AdListTableEnum.VIDEO_75_PERCENT,
      },
      {
        label: 'Video watches at 100%',
        value: AdListTableEnum.VIDEO_100_PERCENT,
      },
    ],
  },
  {
    labelGroup: 'Ecommerce',
    nzOptions: [
      {
        label: 'SL mua hàng',
        value: AdListTableEnum.PURCHASE,
      },
      {
        label: '% chuyển đổi mua hàng',
        value: AdListTableEnum.PURCHASE_CONVERSION,
      },
      {
        label: 'CPS',
        value: AdListTableEnum.CPS,
      },
    ],
  },
] as IOptionGroup[];

export const MetricColumnEnumMappings = {
  BUDGET: 'budget',
  COSTS: 'cost',
  IMPRESSIONS: 'impressions',
  CLICKS: 'clicks',
  CTR: 'ctr',
  CPC: 'cpc',
  PURCHASE: 'purchase',
  PURCHASE_CONVERSION: 'purchaseConversion',
  CPS: 'cps',
  VIDEO_VIEW_3S: 'videoView3s',
  VIDEO_25_PERCENT: 'video25Percent',
  VIDEO_50_PERCENT: 'video50Percent',
  VIDEO_75_PERCENT: 'video75Percent',
  VIDEO_100_PERCENT: 'video100Percent',
};
