import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortActivePipe } from './sort-active.pipe';

@NgModule({
  declarations: [SortActivePipe],
  imports: [CommonModule],
  exports: [SortActivePipe],
})
export class SortActiveModule {}
