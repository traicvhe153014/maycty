import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CampaignSearchLevelEnum } from '@features/campaign-management/enums';
import { Select } from '@ngxs/store';
import {
  AdsetState,
  AdvertisingState,
  CampaignState,
} from '@features/campaign-management/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { campaignManagementFilter } from '@features/campaign-management/constants';

@Component({
  selector: 'novanet-campaign-management',
  templateUrl: './campaign-management.component.html',
  styleUrls: ['./campaign-management.component.scss'],
})
export class CampaignManagementComponent implements OnInit, OnDestroy {
  @Select(CampaignState.getSelected)
  selectedCampaignIds$: Observable<string[]>;
  @Select(AdsetState.getSelected)
  selectedAdsetIds$: Observable<string[]>;
  @Select(AdvertisingState.getSelected)
  selectedAdvertisingIds$: Observable<string[]>;

  @Select(CampaignState.getNavigatedCampaignId)
  navigatedCampaignId$: Observable<string | undefined>;
  @Select(AdsetState.getNavigatedAdsetId)
  navigatedAdsetId$: Observable<string | undefined>;

  public selectedCampaignIds: string[] = [];
  public selectedAdsetIds: string[] = [];
  public selectedAdvertisingIds: string[] = [];
  public selectedCreateReport: string[] = [];

  public tabIndex = 0;
  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    const currentLevel =
      this.activatedRoute.snapshot.queryParamMap.get('level');
    if (currentLevel) {
      this.tabIndex = CampaignSearchLevelEnum[currentLevel];
    }
  }

  public ngOnInit(): void {
    this.selectedCampaignIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (campaignIds) => {
        this.selectedCampaignIds = campaignIds;
      },
    });
    this.selectedAdsetIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (adsetIds) => {
        this.selectedAdsetIds = adsetIds;
      },
    });
    this.selectedAdvertisingIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (adIds) => {
        this.selectedAdvertisingIds = adIds;
      },
    });

    this.navigatedCampaignId$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (campaignId) => {
        if (!campaignId) {
          return;
        }
        this.tabIndex = CampaignSearchLevelEnum.Adset;
        this.router
          .navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: {
              level: CampaignSearchLevelEnum[CampaignSearchLevelEnum.Adset],
            },
            queryParamsHandling: 'merge',
          })
          .then();
      },
    });

    this.navigatedAdsetId$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (adsetId) => {
        if (!adsetId) {
          return;
        }
        this.tabIndex = CampaignSearchLevelEnum.Ad;
        this.router
          .navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: {
              level: CampaignSearchLevelEnum[CampaignSearchLevelEnum.Ad],
            },
            queryParamsHandling: 'merge',
          })
          .then();
      },
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    sessionStorage.removeItem(campaignManagementFilter);
  }

  public onTabIndexChange(index: number) {
    this.tabIndex = index;
    this.router
      .navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: { level: CampaignSearchLevelEnum[index] },
        queryParamsHandling: 'merge',
      })
      .then();
  }
}
