import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignAddComponent } from './campaign-add.component';
import { RouterModule } from '@angular/router';
import { CampaignAddRouting } from './campaign-add.routing';
import { NovanetInputModule } from '@shared/custom-input';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { CampaignAddEcommerceModule } from './components/campaign-add-ecommerce';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzModalModule } from 'ng-zorro-antd/modal';

@NgModule({
  declarations: [CampaignAddComponent],
  imports: [
    CommonModule,
    RouterModule,
    CampaignAddRouting,
    NovanetInputModule,
    NzTabsModule,
    CampaignAddEcommerceModule,
    NzToolTipModule,
    FormsModule,
    ReactiveFormsModule,
    NzModalModule,
  ],
  exports: [CampaignAddComponent],
})
export class CampaignAddModule {}
