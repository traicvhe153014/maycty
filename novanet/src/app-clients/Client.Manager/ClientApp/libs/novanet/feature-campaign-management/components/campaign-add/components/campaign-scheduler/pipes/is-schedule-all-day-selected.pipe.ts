import { Pipe, PipeTransform } from '@angular/core';
import { ICampaignScheduling } from '@features/campaign-management/store/campaign';

const HOURS_IN_DAY = 24;

@Pipe({ name: 'isScheduleAllDaySelected' })
export class IsScheduleAllDaySelectedPipe implements PipeTransform {
  transform(
    schedulings: Partial<ICampaignScheduling>[],
    dayOfWeek: number
  ): boolean {
    const items = schedulings.filter((item) => item.dayOfWeek === dayOfWeek);
    return items.length === HOURS_IN_DAY;
  }
}
