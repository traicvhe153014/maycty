import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Store } from '@ngxs/store';
import {
  IAdsetResponse,
  IAdsetUpdateRequest,
  NavigateAdsetNextLevel,
  UpdateAdset,
} from '@features/campaign-management/store/adset';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { NavigateCampaignNextLevel } from '@features/campaign-management/store';

@Component({
  selector: 'novanet-editable-adset-name',
  templateUrl: './editable-adset-name.component.html',
})
export class EditableAdsetNameComponent {
  @Input() adset: IAdsetResponse;
  @Input() isAnySelected: boolean;
  @Input() isExpanded: boolean;
  @Output() expandChange = new EventEmitter<void>();

  public isEditing = false;
  public nameEditing: string;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public toggleRowExpand() {
    this.expandChange.emit();
  }

  public setEditing() {
    this.isEditing = true;
    this.nameEditing = this.adset.name;
  }

  public cancelEditing() {
    this.isEditing = false;
  }

  public saveEditing() {
    if (this.nameEditing === this.adset.name || !this.nameEditing) {
      this.isEditing = false;
      return;
    }
    if (this.nameEditing.length > 150) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          'Số lượng kí tự tối đa là 150 kí tự'
        )
      );
      return;
    }

    const payload: Partial<IAdsetUpdateRequest> = {
      id: this.adset.id,
      name: this.nameEditing,
      isActive: this.adset.isActive,
      isFullUpdate: false,
    };
    this.store.dispatch(new UpdateAdset(payload)).subscribe({
      next: (response) => {
        this.isEditing = false;
        this.changeDetectorRef.detectChanges();
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công',
            'Cập nhật tên nhóm QC thành công'
          )
        );
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Thất bại',
            'Có lỗi xảy ra trong quá trình cập nhật tên nhóm QC'
          )
        );
      },
    });
  }

  public navigateToNextLevel() {
    this.store.dispatch(new NavigateAdsetNextLevel(this.adset?.id));
  }
}
