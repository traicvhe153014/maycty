import { Component, Input } from '@angular/core';
import {
  IAdvertisingAppliedTemplate,
  IAdvertisingResponse,
} from '@features/campaign-management/store';
import { ETemplateType } from '@features/campaign-management/enums';

@Component({
  selector: 'novanet-ad-name-item',
  templateUrl: './ad-name-item.component.html',
})
export class AdNameItemComponent {
  @Input() advertising: IAdvertisingResponse;

  public readonly templateType = ETemplateType;

  public get firstTemplate(): IAdvertisingAppliedTemplate | undefined {
    return this.advertising.templates?.[0];
  }
}
