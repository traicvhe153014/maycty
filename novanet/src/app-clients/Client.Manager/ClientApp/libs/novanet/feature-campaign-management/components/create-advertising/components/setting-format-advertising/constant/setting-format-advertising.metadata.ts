import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import { ESettingFormatAdvertising } from '../enums';

export const AllowSizeColumn: ISettingColumnTable<any, any>[] = [
  {
    id: ESettingFormatAdvertising.TYPE,
    title: 'Loại',
    width: '150px',
    align: 'left',
  },
  {
    id: ESettingFormatAdvertising.CONTAINER_SIZE,
    title: 'Kích cỡ Banner',
    align: 'left',
    width: '150px',
  },
  {
    id: ESettingFormatAdvertising.SAFE_AREA,
    title: 'Vùng an toàn',
    align: 'left',
    width: '150px',
  },
  {
    id: ESettingFormatAdvertising.FILE_SIZE,
    title: 'Dung lượng tối đa',
    align: 'left',
    width: '150px',
  },
];

export const AllowSizeSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: false,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;
