import { NgModule } from '@angular/core';
import { AdNameItemComponent } from './ad-name-item.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AdNameItemComponent],
  imports: [CommonModule, RouterModule],
  exports: [AdNameItemComponent],
})
export class AdNameItemModule {}
