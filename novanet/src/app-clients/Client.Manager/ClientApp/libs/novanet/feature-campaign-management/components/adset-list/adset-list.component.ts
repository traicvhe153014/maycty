import {
  ExportReport,
  GlobalNotificationState,
} from '@shared/global-notification/store';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { IPredicateModel } from '@core/models';
import {
  CampaignState,
  CampaignStatusEnum,
  CampaignType,
  GetCampaignList,
  GetMoreCampaignList,
  ICampaignListRequest,
  ICampaignResponse,
} from '@features/campaign-management/store/campaign';
import { Select, Store } from '@ngxs/store';
import {
  AdsetState,
  GetAdsetList,
  GetMoreAdsetList,
  IAdsetProductGroupResponse,
  IAdsetResponse,
  IAdsetSummary,
  IAdsetUpdateProductGroupRequest,
  IAdsetUpdateRequest,
  SelectAdset,
  UpdateAdset,
  UpdateAdsetProductGroup,
} from '@features/campaign-management/store/adset';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import {
  AdsetGroupOptionColumn,
  adsetListColumns,
  adsetListSetting,
  adsetListSummaryColumns,
  AdsetOptionColumnDefault,
  campaignManagementFilter,
  campaignSearchLevelFields,
  campaignTypeLabelList,
  campaignTypeLabels,
  MetricColumnEnumMappings,
} from '@features/campaign-management/constants';
import {
  AdsetListSummaryEnum,
  AdsetListTableEnum,
  CampaignSearchLevelEnum,
  ReportOptions,
} from '@features/campaign-management/enums';
import { ISettingColumnTable } from '@data-table/models';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { shouldLoadMore } from '@core/utils';
import { ESortType, PredicateOperatorEnum } from '@core/enums';
import { ICampaignManagementFilter } from '@features/campaign-management/models';
import { session, storage } from '@components';

@Component({
  selector: 'novanet-adset-list',
  templateUrl: './adset-list.component.html',
  styleUrls: ['./adset-list.component.scss'],
})
export class AdsetListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public searchDropdownOpen = false;
  public selectedAdsets: string[] = [];
  public filterDateRange: Date[] = [];
  public filterStatus: CampaignStatusEnum | undefined = undefined;
  public filterPredicates: IPredicateModel[] = [];
  public readonly pageSize = 24;
  public expandedAdsets: string[] = [];
  public selectedCampaigns: string[] = [];
  public filterCampaignType: CampaignType | undefined = undefined;
  public filterSelectedColumns: number[] = [];
  public exceptFilterStatus: CampaignStatusEnum[] = [
    CampaignStatusEnum.Finished,
    CampaignStatusEnum.NotStarted,
  ];

  public columns: ISettingColumnTable<IAdsetResponse, any>[] = [];
  public readonly settings = adsetListSetting;
  public summaryColumns = adsetListSummaryColumns;
  public readonly adsetListTableEnum = AdsetListTableEnum;
  public readonly adsetListSummaryEnum = AdsetListSummaryEnum;
  public readonly campaignTypeLabels = campaignTypeLabels;
  public readonly campaignTypeLabelList = campaignTypeLabelList;
  public readonly adsetGroupOptionColumn = AdsetGroupOptionColumn;
  public selectedColumnDefault = AdsetOptionColumnDefault;
  public readonly summaryColumnsDefault = adsetListSummaryColumns;

  @Select(AdsetState.getList) adsets$: Observable<IAdsetResponse[]>;
  @Select(AdsetState.getPage) currentPage$: Observable<number>;
  @Select(AdsetState.getLoading) loading$: Observable<boolean>;
  @Select(AdsetState.getSummary)
  adsetSummary$: Observable<IAdsetSummary>;
  @Select(AdsetState.getSelected)
  selectedAdsetIds$: Observable<string[]>;
  @Select(CampaignState.getSelected)
  selectedCampaignIds$: Observable<string[]>;
  @Select(GlobalNotificationState.getCancelDownloading)
  cancelDownloading$: Observable<boolean>;

  @Select(CampaignState.getList) campaigns$: Observable<ICampaignResponse[]>;
  @Select(CampaignState.getPage) campaignCurrentPage$: Observable<number>;
  public searchCampaignKeyword = '';
  public searchCampaignKeywordChanged: Subject<string> = new Subject<string>();
  exportExcelSubscription: Subscription;
  public switchLoadings: { [key: string]: boolean } = {};
  public isActiveValues: { [key: string]: boolean } = {};
  public switchProductGroupLoadings: { [key: string]: boolean } = {};
  public isActiveProductGroupValues: { [key: string]: boolean } = {};
  public columnsConstant: number[] | [];
  public summaryColumnsConstant: number[] | [];
  public columnsDefault: ISettingColumnTable<IAdsetResponse, any>[] = [];

  private searchCampaignKeywordChangeSubscription: Subscription;
  private sort = '+status,-isActive,-modifiedAt';
  private sortDefault = '+status,-isActive,-modifiedAt';
  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private store: Store
  ) {
    this.columnsConstant = [
      AdsetListTableEnum.CHECKBOX,
      AdsetListTableEnum.INDEX,
      AdsetListTableEnum.IS_ACTIVE,
      AdsetListTableEnum.NAME,
      AdsetListTableEnum.CAMPAIGN_NAME,
      AdsetListTableEnum.STATUS,
      AdsetListTableEnum.CAMPAIGN_TYPE,
    ];
    this.summaryColumnsConstant = [
      AdsetListSummaryEnum.COUNT,
      AdsetListSummaryEnum.CAMPAIGN_NAME,
      AdsetListSummaryEnum.STATUS,
      AdsetListSummaryEnum.CAMPAIGN_TYPE,
    ];
  }

  public get isAnySelected() {
    return this.selectedAdsets.length > 0;
  }

  public get validFilterStatus() {
    if (this.exceptFilterStatus.some((item) => item === this.filterStatus)) {
      return undefined;
    }
    return this.filterStatus;
  }

  ngAfterViewInit(): void {
    this.columns = adsetListColumns.map((item) =>
      item.id !== AdsetListTableEnum.CHECKBOX
        ? item
        : {
            ...item,
            title: this.checkboxHeader,
          }
    );

    this.columnsDefault = this.columns;
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (
      filter &&
      filter.selectedColumnAdset &&
      filter.selectedColumnAdset.length > 0
    ) {
      this.selectedColumnDefault = filter.selectedColumnAdset;
      this.updateColumns(filter.selectedColumnAdset);
    } else {
      this.updateColumns(AdsetOptionColumnDefault);
    }
    this.changeDetectorRef.detectChanges();
  }

  public onNextPage() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMoreAdsetList({
            filters: this.filterPredicates,
            page: currentPage + 1,
            pageSize: this.pageSize,
            startDate: this.filterDateRange[0],
            endDate: this.filterDateRange[1],
            status: this.validFilterStatus,
            campaignIds: this.selectedCampaigns,
            sorts: this.sort,
            campaignType: this.filterCampaignType,
            fields: this.filterSelectedColumns
              .map((item) => MetricColumnEnumMappings[AdsetListTableEnum[item]])
              .filter((item) => !!item),
          })
        );
      },
    });
  }

  public onSelectAdsetChanged(event: Event, id: string) {
    let updatedAdsetIds: string[] = [];
    if (this.isAdsetSelected(id)) {
      updatedAdsetIds = this.selectedAdsets.filter((adsetId) => adsetId !== id);
    } else {
      updatedAdsetIds = [...this.selectedAdsets, id];
    }
    session.set('tableAdsetSelected', updatedAdsetIds);
    this.store.dispatch(new SelectAdset(updatedAdsetIds));
    this.changeDetectorRef.detectChanges();
  }

  public onSelectAllAdsetChanged(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.adsets$.pipe(take(1)).subscribe({
        next: (adsets) => {
          const updatedAdsetIds = adsets.map((adset) => adset.id);
          this.store.dispatch(new SelectAdset(updatedAdsetIds));
          session.set('tableAdsetSelected', updatedAdsetIds);
        },
      });
    } else {
      this.store.dispatch(new SelectAdset([]));
      session.set('tableAdsetSelected', []);
    }
  }

  public isAdsetSelected(id: string) {
    return !!this.selectedAdsets.find((adsetId) => adsetId === id);
  }

  public toggleRowExpand(id: string) {
    if (this.isAdsetExpanded(id)) {
      this.expandedAdsets = this.expandedAdsets.filter(
        (adsetId) => adsetId !== id
      );
    } else {
      this.expandedAdsets = [...this.expandedAdsets, id];
    }
    this.changeDetectorRef.detectChanges();
  }

  public isAdsetExpanded(id: string) {
    return !!this.expandedAdsets.find((adsetId) => adsetId === id);
  }

  ngOnInit() {
    if (session.get('tableAdsetSelected')) {
      this.store.dispatch(
        new SelectAdset(session.get('tableAdsetSelected'), true)
      );
    }
    this.campaignCurrentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.dispatchGetCampaignList(currentPage);
      },
    });
    this.searchCampaignKeywordChangeSubscription =
      this.searchCampaignKeywordChanged
        .pipe(debounceTime(300), distinctUntilChanged())
        .subscribe({
          next: (model) => {
            this.searchCampaignKeyword = model;
            this.dispatchGetCampaignList(1);
          },
        });
    this.selectedAdsetIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (value) => {
        this.selectedAdsets = value;
        this.changeDetectorRef.detectChanges();
      },
    });
    this.selectedCampaignIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (value) => {
        this.selectedCampaigns = value;
        this.changeDetectorRef.detectChanges();
      },
    });
    this.adsets$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (adsets) => {
        this.isActiveValues = adsets.reduce((result, item) => {
          return { ...result, [item.id]: item.isActive };
        }, {} as { [key: string]: boolean });
        this.isActiveProductGroupValues = adsets.reduce((result, item) => {
          if (!!item.productGroupData) {
            const groupActives = item.productGroupData.reduce(
              (groupResult, group) => ({
                ...groupResult,
                [item.id + '_' + group.id]: group.isActive,
              }),
              {} as { [key: string]: boolean }
            );
            return { ...result, ...groupActives };
          }
          return result;
        }, {} as { [key: string]: boolean });
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  ngOnDestroy() {
    this.searchCampaignKeywordChangeSubscription.unsubscribe();
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onCampaignListScroll(event: Event) {
    if (shouldLoadMore(event.target as HTMLElement)) {
      this.campaignCurrentPage$.pipe(take(1)).subscribe({
        next: (currentPage) => {
          this.store.dispatch(
            new GetMoreCampaignList({
              page: currentPage + 1,
              pageSize: this.pageSize,
              filters: [
                {
                  field:
                    campaignSearchLevelFields[CampaignSearchLevelEnum.Campaign],
                  operator: PredicateOperatorEnum.Contains,
                  value: this.searchCampaignKeyword,
                },
              ],
              withReport: false,
              withSummary: false,
            })
          );
        },
      });
    }
  }

  // FILTER CHANGE
  public onFilterPredicatesChange(predicates: IPredicateModel[]) {
    this.filterPredicates = predicates;
    this.dispatchGetList(1);
  }

  public onFilterStatusChange(e: CampaignStatusEnum) {
    this.filterStatus = e;
    this.dispatchGetList(1);
  }

  public onFilterCampaignTypeChange(campaignType: CampaignType) {
    this.filterCampaignType = campaignType;
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.campaignType = campaignType;
    } else {
      filter = {
        campaignType,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.dispatchGetList(1);
  }

  public onFilterDateRangeChange(result: Date[]) {
    this.filterDateRange = result;
    this.dispatchGetList(1);
  }

  public onAllFilterChange(result: ICampaignManagementFilter) {
    this.filterDateRange = result.dateRange;
    this.filterStatus = result.status;
    this.filterPredicates = result.predicates;
    this.filterCampaignType = result.campaignType;
    this.filterSelectedColumns = result.selectedColumnAdset;
    forkJoin([this.selectedCampaignIds$.pipe(take(1))]).subscribe({
      next: ([selectedCampaigns]) => {
        this.selectedCampaigns = selectedCampaigns;
        this.dispatchGetList(1);
      },
    });
  }

  // FILTER CHANGE END

  public createReport() {}

  public showAd() {}

  public onAdsetIsActiveChange(adset: IAdsetResponse) {
    if (this.switchLoadings[adset.id]) {
      return;
    }
    const payload: Partial<IAdsetUpdateRequest> = {
      id: adset.id,
      name: adset.name,
      isActive: !adset.isActive,
      isFullUpdate: false,
    };
    this.switchLoadings[adset.id] = true;
    this.changeDetectorRef.detectChanges();
    this.store.dispatch(new UpdateAdset(payload)).subscribe({
      next: (response) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.switchLoadings[adset.id] = false;
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.switchLoadings[adset.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public onAdsetProductGroupIsActiveChange(
    item: IAdsetProductGroupResponse,
    adsetId: string
  ) {
    const payload: IAdsetUpdateProductGroupRequest = {
      productGroupId: item.id,
      advertisingSetId: adsetId,
      isActive: !item.isActive,
    };
    this.switchProductGroupLoadings[adsetId + '_' + item.id] = true;
    this.store.dispatch(new UpdateAdsetProductGroup(payload)).subscribe({
      next: (response) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.switchProductGroupLoadings[adsetId + '_' + item.id] = false;
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.switchProductGroupLoadings[adsetId + '_' + item.id] = false;
        this.changeDetectorRef.reattach();
      },
    });
  }

  public dispatchGetList(page: number, value?, statusCustomColumn?: boolean) {
    if (value) {
      const sortedColumn = value[value.length - 1];
      this.columns.forEach((column) => {
        if (!sortedColumn || sortedColumn?.name !== column.name) {
          column.ascending = false;
          column.descending = false;
        } else {
          switch (sortedColumn.direction) {
            case ESortType.Ascending:
              this.sort = sortedColumn.name;
              column.ascending = true;
              column.descending = false;
              break;
            case ESortType.Descending:
              this.sort = '-' + sortedColumn.name;
              column.ascending = false;
              column.descending = true;
              break;
          }
        }
      });
      if (!sortedColumn) {
        this.sort = '';
      }
    }

    if (statusCustomColumn) {
      this.sort = this.sortDefault;
    }

    this.store.dispatch(
      new GetAdsetList({
        filters: this.filterPredicates,
        page,
        pageSize: this.pageSize,
        startDate: this.filterDateRange[0],
        endDate: this.filterDateRange[1],
        status: this.validFilterStatus,
        campaignIds:
          session.get('tableCampaignSelected') ?? this.selectedCampaigns,
        sorts: this.sort,
        campaignType: this.filterCampaignType,
        fields: this.filterSelectedColumns
          .map((item) => MetricColumnEnumMappings[AdsetListTableEnum[item]])
          .filter((item) => !!item),
      })
    );
  }

  public exportData($event) {
    const param = {
      startDate: this.filterDateRange[0],
      endDate: this.filterDateRange[1],
      exportType: ReportOptions.AdGroupReport,
      advertisingSetIds: this.selectedAdsets,
    } as ICampaignListRequest;

    this.store.dispatch(new ExportReport(param));
  }

  public updateColumns(columnUpdates: number[]) {
    const columnUpdate = [...columnUpdates, ...this.columnsConstant];
    const summaryColumnUpdate = [
      ...columnUpdates,
      ...this.summaryColumnsConstant,
    ];
    let columns = [];
    let summaryColumns = [];
    this.columnsDefault.forEach((col) => {
      columnUpdate.forEach((item, index) => {
        if (col.id === item) {
          columns.push(col);
        }
      });
    });
    this.summaryColumnsDefault.forEach((colSum) => {
      summaryColumnUpdate.forEach((item, index) => {
        if (colSum.id === item) {
          summaryColumns.push(colSum);
        }
      });
    });
    this.columns = columns;
    this.summaryColumns = summaryColumns;
    this.filterSelectedColumns = columns.map((item) => item.id);

    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.selectedColumnAdset = columnUpdates;
    } else {
      filter = {
        selectedColumnAdset: columnUpdates,
      };
    }
    storage.set(campaignManagementFilter, filter);

    this.dispatchGetList(1, undefined, true);
    this.changeDetectorRef.detectChanges();
  }

  private dispatchGetCampaignList(page: number) {
    this.store.dispatch(
      new GetCampaignList({
        page,
        pageSize: this.pageSize,
        filters: [
          {
            field: campaignSearchLevelFields[CampaignSearchLevelEnum.Campaign],
            operator: PredicateOperatorEnum.Contains,
            value: this.searchCampaignKeyword,
          },
        ],
        withSummary: false,
        withReport: false,
      })
    );
  }
}
