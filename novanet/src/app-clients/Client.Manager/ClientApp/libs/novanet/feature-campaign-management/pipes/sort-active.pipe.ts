import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sortActive' })
export class SortActivePipe implements PipeTransform {
  transform(sorts: string, field: string): boolean {
    return sorts.split(',').some((sort) => sort.trim() === field);
  }
}
