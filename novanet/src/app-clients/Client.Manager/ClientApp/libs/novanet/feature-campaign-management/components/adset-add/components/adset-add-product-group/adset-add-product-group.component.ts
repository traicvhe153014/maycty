import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { IFormFieldErrors, IFormFields } from '@models';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { IProhibitedCategory, SharingState } from '@shared/sharing/store';
import { Observable, skip, Subject } from 'rxjs';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { map, takeUntil } from 'rxjs/operators';
import {
  ClearProductGroups,
  FeatureProductManagementState,
  GetProductGroups,
} from '@features/product-group-management/store';
import { IHttpGetRequest } from '@core/models';
import {
  IProductGroupHttpGetRequest,
  IProductGroupManagementData,
} from '@features/product-group-management/models';
import { cloneDeep } from 'lodash';
import {
  selectedProductGroupColumns,
  selectedProductGroupSetting,
} from '@features/campaign-management/components/adset-add/constants';
import { AdsetAddSelectedProductGroupsEnum } from '@features/campaign-management/components/adset-add/enums';
import { IAdsetByIdResponse } from '@features/campaign-management/store';
import * as dayjs from 'dayjs';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';

@Component({
  selector: 'novanet-adset-add-product-group',
  templateUrl: './adset-add-product-group.component.html',
})
export class AdsetAddProductGroupComponent implements OnInit, OnDestroy {
  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;
  @Input() formType: string; // add | edit
  @Input() currentAdset: IAdsetByIdResponse | undefined;
  @Input() productFeedId: string;

  @Select(SharingState.getProhibitedCategories)
  prohibitedCategories$: Observable<IProhibitedCategory[]>;

  @Select(FeatureProductManagementState.getProductGroups)
  public productGroups$: Observable<IProductGroupManagementData[]>;

  public productGroups: IProductGroupManagementData[] = [];
  public form: FormGroup;

  public productGroupLoaded = false;
  public readonly productGroupPageSize = 10;
  public readonly selectedProductGroupSetting = selectedProductGroupSetting;
  public readonly selectedProductGroupColumns = selectedProductGroupColumns;
  public readonly adsetAddSelectedProductGroupsEnum =
    AdsetAddSelectedProductGroupsEnum;
  private productGroupSkip = 0;
  private productGroupPage = 1;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private rootFormGroup: FormGroupDirective,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public get productGroupsDisabled() {
    return (this.formType === 'edit' &&
      dayjs(this.currentAdset?.campaign?.ecommerceStartDate).isBefore(
        dayjs()
      )) ||
      (this.formType === 'edit' && this.currentAdset?.applyAllProductGroups)
      ? ''
      : null;
  }

  public get productGroupOptions(): NzSelectOptionInterface[] {
    let productGroups = cloneDeep(this.productGroups);
    if (this.formType === 'edit') {
      this.form.value.productGroupIds.forEach((productId) => {
        productGroups = productGroups.filter((x) => x.id !== productId);
      });
    }
    return productGroups.map((item) => ({
      label: item.name,
      value: item.id,
    }));
  }

  public get prohibitedCategoryOptions$(): Observable<
    NzSelectOptionInterface[]
  > {
    return this.prohibitedCategories$.pipe(
      map((items): NzSelectOptionInterface[] =>
        items.map((item) => ({
          label: item.name,
          value: item.id,
        }))
      )
    );
  }

  public get selectedProductGroups(): IProductGroupManagementData[] {
    const productGroupIds = this.form.get(this.formFields.productGroupIds.name)
      .value as string[];
    return productGroupIds.map((productGroupId) =>
      this.productGroups.find((item) => item.id === productGroupId)
    );
  }

  ngOnInit() {
    this.form = this.rootFormGroup.control;
    const params = {
      page: this.productGroupPage,
      skip: 0,
      pageSize: this.productGroupPageSize,
      productFeedId: this.productFeedId,
    } as IProductGroupHttpGetRequest;
    this.store.dispatch(new GetProductGroups(params));
    this.getProductGroups();
    this.form
      .get('productGroupIds')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: (productGroupIds) => {
          const newProductGroupId = productGroupIds.find(
            (item) => !this.form.value.productGroupIds.includes(item)
          );
          const newProductGroup = this.productGroups.find(
            (item) => item.id === newProductGroupId
          );
          if (!newProductGroup) {
            return;
          }
          if (newProductGroup.stoppedProducts > 0) {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.warning,
                'Nhóm sản phẩm có chứa sản phẩm hết hàng không thể phân phối quảng cáo'
              )
            );
          }
        },
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearProductGroups());
  }

  public isProductGroupDisabled(productGroupId: string): boolean {
    return (
      this.formType === 'edit' &&
      dayjs(this.currentAdset?.campaign?.ecommerceStartDate).isBefore(
        dayjs()
      ) &&
      !!this.currentAdset?.advertisingSetProductGroups?.find(
        (item) => item.productGroupId === productGroupId
      )
    );
  }

  public loadMoreProductGroups() {
    if (this.productGroupLoaded) {
      return;
    }
    this.productGroupPage += 1;
    this.productGroupSkip += this.productGroupPageSize;
    const params = {
      page: this.productGroupPage,
      skip: this.productGroupSkip,
      pageSize: this.productGroupPageSize,
      productFeedId: this.productFeedId,
    } as IHttpGetRequest;
    this.store.dispatch(new GetProductGroups(params));
  }

  public removeProductGroup(id: string) {
    const productGroupIds = this.form.get(this.formFields.productGroupIds.name)
      .value as string[];
    this.form.patchValue({
      productGroupIds: productGroupIds.filter((item) => item !== id),
    });
  }

  public removeAllProductGroups() {
    this.form.patchValue({
      productGroupIds: [],
    });
  }

  private getProductGroups() {
    this.productGroups$
      .pipe(skip(1), takeUntil(this.destroy$))
      .subscribe((response) => {
        if (!response || this.productGroupLoaded) {
          return;
        }
        if (!response.length) {
          this.productGroupLoaded = true;
          return;
        }
        let data = cloneDeep(response);
        data = data.map((item) => ({
          ...item,
          checked: false,
          enableChangeValue: false,
          loadMoreProductAttributeValue: false,
        }));
        this.productGroups = [...this.productGroups, ...data];
        if (response.length < this.productGroupPageSize) {
          this.productGroupLoaded = true;
        }
        this.changeDetectorRef.detectChanges();
      });
  }
}
