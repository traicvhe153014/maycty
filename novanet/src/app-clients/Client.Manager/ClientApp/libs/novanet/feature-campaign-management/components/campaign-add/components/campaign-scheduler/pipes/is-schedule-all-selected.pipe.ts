import { Pipe, PipeTransform } from '@angular/core';
import { ICampaignScheduling } from '@features/campaign-management/store/campaign';

const MAX_ITEMS = 24 * 7;

@Pipe({ name: 'isScheduleAllSelected' })
export class IsScheduleAllSelectedPipe implements PipeTransform {
  transform(schedulings: Partial<ICampaignScheduling>[]): boolean {
    return schedulings.length === MAX_ITEMS;
  }
}
