import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { IFormFieldErrors, IFormFields } from '@models';
import { GlobalNotificationEnum } from '@shared/global-notification';
import { IHttpGetRequest } from '@core/models';
import { cloneDeep } from 'lodash';
import { EMoreRemarketing } from './enums';
import {
  MoreMarketingFormFields,
  MoreMarketingFormFieldsErrors,
  MoreMarketingSettingTable,
  UniformedUnitDropdown,
  WebsitesSettingColumn,
} from './constants';
import { EAdsetAddRemarketingOption } from '@features/campaign-management/components';
import {
  ClearWebsiteList,
  GetMoreWebsiteList,
  GetWebsiteList,
  IWebsite,
  IWebsitesResponse,
  SettingsState,
} from '@shared/settings/store';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import {
  AdsetService,
  IAdsetByIdResponse,
} from '@features/campaign-management/store';
import * as dayjs from 'dayjs';
import { DataTableComponent } from '@data-table/data-table.component';

@Component({
  selector: 'novanet-more-remarketing',
  templateUrl: './more-remarketing.component.html',
  styleUrls: ['./more-remarketing.component.scss'],
})
export class MoreRemarketingComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @Input() public marketingOption: EAdsetAddRemarketingOption;
  @Input() formType: string; // add | edit
  @Input() currentAdset: IAdsetByIdResponse | undefined;

  @Output() public formValue = new EventEmitter<any>();

  @Select(SettingsState.getWebsites)
  public websites$: Observable<IWebsitesResponse>;

  @Select(SettingsState.getLoadingWebsites)
  public loading$: Observable<boolean>;

  @ViewChild('selectAll') public selectAllRef: TemplateRef<any>;
  @ViewChild('removeAll') public removeAllRef: TemplateRef<any>;
  @ViewChild('titleWebsite') public titleWebsite: TemplateRef<any>;
  @ViewChild('titleWebsiteSelected')
  public titleWebsiteSelected: TemplateRef<any>;
  @ViewChild('sourceProductTypeTable', { static: false })
  public sourceProductTypeTable: DataTableComponent<any>;
  @ViewChild('targetProductTypeTable', { static: false })
  public targetProductTypeTable: DataTableComponent<any>;

  public websites: IWebsite[] = [];
  public selectedOfWebsite: IWebsite[] = [];
  public lengthTotalWebsite = 0;
  public basedWebsites: IWebsite[] = [];
  public listOfWebsiteDropdownData: IWebsite[] = [];
  public listOfSelectedWebsite: IWebsite[] = [];
  public settings: DataTableSettingModel = MoreMarketingSettingTable;
  public searchForm: FormGroup;
  public moreMarketingForm: FormGroup;
  public scrollX: string | null = null;
  public scrollY: string | null = null;
  public editEnabled = false;
  public isRemoved = false;
  public pageSize = 100;
  public readonly eMarketingOption = EAdsetAddRemarketingOption;
  public readonly uniformedUnitDropdown = UniformedUnitDropdown;
  public readonly moreMarketingFormFields: IFormFields =
    MoreMarketingFormFields;
  public readonly moreMarketingFormFieldsError: IFormFieldErrors =
    MoreMarketingFormFieldsErrors;
  public columns: ISettingColumnTable<any, any>[] = WebsitesSettingColumn;
  public columnsSelected: ISettingColumnTable<any, any>[] =
    WebsitesSettingColumn;
  public readonly MoreRemarketingEnum = EMoreRemarketing;
  public readonly globalNotificationEnum = GlobalNotificationEnum;
  private filterWebsites = '';
  private filterWebsitesSelected = '';
  private page = 1;
  private productAttributesLoaded = false;

  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private rootFormGroup: FormGroupDirective,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private adsetService: AdsetService
  ) {}

  get selectedWebsites(): FormControl {
    return this.moreMarketingForm.get(
      this.moreMarketingFormFields['selectedWebsites'].name
    ) as FormControl;
  }

  get uniformed(): boolean {
    return this.moreMarketingForm.get(
      this.moreMarketingFormFields['uniformed'].name
    ).value as boolean;
  }

  public get isUniformedDisabled() {
    return (
      this.formType === 'edit' &&
      dayjs(this.currentAdset?.campaign?.ecommerceStartDate).isBefore(dayjs())
    );
  }

  public ngOnInit(): void {
    this.moreMarketingForm = this.rootFormGroup.control;
    this.getWebsites();
    this.buildSearchForm();

    this.adsetService.applySelectedWebsiteTemplate$
      .pipe(takeUntil(this.destroy$))
      .subscribe({
        next: (appliedWebsites) => {
          this.listOfSelectedWebsite =
            this.selectedWebsites.value?.reduce((result, item) => {
              const value = this.basedWebsites.find(
                (basedWebsite) => basedWebsite.id === item
              );
              if (!value?.url?.includes(this.filterWebsitesSelected)) {
                return result;
              }
              result.push(value);
              return result;
            }, []) ?? [];
          this.selectedOfWebsite = this.listOfSelectedWebsite;
          this.websites =
            this.basedWebsites?.filter(
              (item) =>
                !this.selectedOfWebsite.find(
                  (selected) => item.id === selected?.id
                )
            ) ?? [];
        },
      });

    this.selectedWebsites.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe({
        next: (formValue) => {
          this.listOfSelectedWebsite =
            formValue?.reduce((result, item) => {
              const value = this.selectedOfWebsite.find(
                (basedWebsite) => basedWebsite.id === item
              );
              if (!value?.url?.includes(this.filterWebsitesSelected)) {
                return result;
              }
              result.push(value);
              return result;
            }, []) ?? [];
          this.websites =
            this.basedWebsites?.filter(
              (item) =>
                !this.selectedOfWebsite.find(
                  (selected) => item.id === selected?.id
                )
            ) ?? [];
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  ngAfterViewInit(): void {
    this.columns = [
      {
        id: EMoreRemarketing.INDEX,
        title: 'STT',
        width: '5%',
      },
      {
        id: EMoreRemarketing.WEBSITE,
        title: this.titleWebsite,
        width: '70%',
      },
      {
        id: EMoreRemarketing.CHOOSE,
        title: this.selectAllRef,
        width: '10%',
        align: 'center',
      },
    ];
    this.columnsSelected = [
      {
        id: EMoreRemarketing.INDEX,
        title: 'STT',
        width: '5%',
      },
      {
        id: EMoreRemarketing.WEBSITE,
        title: this.titleWebsiteSelected,
        width: '70%',
      },
      {
        id: EMoreRemarketing.CHOOSE,
        title: this.removeAllRef,
        width: '10%',
        align: 'center',
      },
    ];
  }

  public ngOnDestroy() {
    this.store.dispatch(new ClearWebsiteList());
    this.destroy$.next();
    this.destroy$.complete();
  }

  public checkAll() {
    this.isRemoved = false;
    this.targetProductTypeTable.valueSearch = '';
    const listIdWebsite = this.websites
      .map((website) => website.id)
      .concat(this.selectedOfWebsite.map((website) => website.id));
    const listDataWebsite = this.websites.concat(this.selectedOfWebsite);
    this.selectedOfWebsite = listDataWebsite;
    this.listOfSelectedWebsite = listDataWebsite;
    this.onsearchDomain('', false);
    this.selectedWebsites.patchValue(listIdWebsite);
    this.changeDetectorRef.detectChanges();
  }

  public removeAllSelected() {
    this.isRemoved = false;
    this.filterWebsitesSelected = '';
    this.filterWebsites = '';
    this.onsearchDomain('', true);
    this.selectedWebsites.patchValue([]);
    this.sourceProductTypeTable.valueSearch = '';
    this.selectedOfWebsite = [];
    this.changeDetectorRef.detectChanges();
  }

  public onNextPage() {
    this.page += 1;
    this.store.dispatch(
      new GetMoreWebsiteList({
        page: this.page,
        pageSize: this.pageSize,
      })
    );
  }

  public getWebsites() {
    let loaded = false;
    this.websites$.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      if (response.data.length > 0) {
        this.lengthTotalWebsite = response.total;
        this.basedWebsites = response.data;
        this.listOfWebsiteDropdownData = response.data;
        if (!this.filterWebsites && !loaded) {
          this.listOfSelectedWebsite =
            this.selectedWebsites.value?.reduce((result, item) => {
              const value = this.basedWebsites.find(
                (basedWebsite) => basedWebsite.id === item
              );
              if (!value?.url?.includes(this.filterWebsitesSelected)) {
                return result;
              }
              result.push(value);
              return result;
            }, []) ?? [];
          this.selectedOfWebsite = this.listOfSelectedWebsite;
          loaded = true;
        }
        this.websites = response.data.reduce((result, item) => {
          const selected = this.listOfSelectedWebsite.find(
            (website) => website.id === item.id
          );
          if (selected) {
            return result;
          }
          result.push(item);
          return result;
        }, []);
      } else if (this.filterWebsites !== '') {
        this.lengthTotalWebsite = 0;
        this.basedWebsites = [];
        this.listOfWebsiteDropdownData = [];
        this.websites = [];
      }
    });
  }

  public nextBatch($event?: boolean) {}

  public onSubmitFormMoreMarketing(): void {}

  public onAddWebsite(website: IWebsite) {
    if (!website.active) {
      return;
    }
    if (website) {
      const existItem = cloneDeep(
        this.websites.find((item) => item.id === website.id)
      );
      if (existItem) {
        this.selectedOfWebsite.push(existItem);
        const selectedWebsites = this.selectedWebsites.value;
        selectedWebsites.push(existItem.id);
        this.selectedWebsites.patchValue(selectedWebsites);
      }
      this.changeDetectorRef.detectChanges();
    }
  }

  public onRemoveWebsite(website: IWebsite) {
    if (!website.active) {
      return;
    }
    const existItem = cloneDeep(
      this.listOfSelectedWebsite.find((item) => item?.id === website?.id)
    );
    if (existItem) {
      this.selectedOfWebsite = this.selectedOfWebsite.filter(
        (x) => x.id !== existItem.id
      );
      let selectedWebsites = this.selectedWebsites.value;
      selectedWebsites = selectedWebsites.filter((x) => x !== existItem.id);
      this.selectedWebsites.patchValue(selectedWebsites);
      this.changeDetectorRef.detectChanges();
    }
  }

  public onsearchDomain(e: string, domain: boolean) {
    this.isRemoved = true;
    if (domain) {
      this.filterWebsites = e ?? '';
      this.page = 1;
      const params = {
        page: this.page,
        pageSize: this.pageSize,
        keyWord: this.filterWebsites,
      } as IHttpGetRequest;
      this.store.dispatch(new GetWebsiteList(params));
    } else {
      this.filterWebsitesSelected = e ?? '';
      this.listOfSelectedWebsite = this.selectedOfWebsite?.filter((x) =>
        x.url.includes(e)
      );
    }
  }

  public removeAllSearches() {
    this.sourceProductTypeTable.valueSearch = '';
    this.onsearchDomain('', true);
    this.isRemoved = false;
  }

  private buildSearchForm() {
    const config = {
      ['search']: ['', [Validators.required]],
    };
    this.searchForm = this.formBuilder.group(config);
  }
}
