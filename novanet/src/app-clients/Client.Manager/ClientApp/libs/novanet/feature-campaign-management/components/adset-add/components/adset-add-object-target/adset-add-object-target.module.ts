import { NgModule } from '@angular/core';
import { AdsetAddObjectTargetComponent } from './adset-add-object-target.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from '@core';
import { RouterModule } from '@angular/router';
import { NovanetInputModule } from '@shared/custom-input';
import { ObjectManagementStateModule } from '@features/feature-object-management/store';

@NgModule({
  declarations: [AdsetAddObjectTargetComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    RouterModule,
    NovanetInputModule,
    ObjectManagementStateModule,
  ],
  exports: [AdsetAddObjectTargetComponent],
})
export class AdsetAddObjectTargetModule {}
