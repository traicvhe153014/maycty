import { NgModule } from '@angular/core';
import { AdsetAddComponent } from './adset-add.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdsetAddRouting } from './adset-add.routing';
import {
  AdsetAddAdTargetModule,
  AdsetAddNameModule,
  AdsetAddObjectTargetModule,
  AdsetAddProductGroupModule,
  AdsetAddRemarketingModule,
} from './components';
import { AdsetService } from '@features/campaign-management/store';
import { CampaignSchedulerModule } from '@features/campaign-management/components';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ScrollListenerModule } from '@shared/directives';

@NgModule({
  declarations: [AdsetAddComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdsetAddRouting,
    AdsetAddNameModule,
    AdsetAddProductGroupModule,
    AdsetAddObjectTargetModule,
    AdsetAddAdTargetModule,
    AdsetAddRemarketingModule,
    CampaignSchedulerModule,
    NzModalModule,
    ScrollListenerModule,
  ],
  providers: [AdsetService],
})
export class AdsetAddModule {}
