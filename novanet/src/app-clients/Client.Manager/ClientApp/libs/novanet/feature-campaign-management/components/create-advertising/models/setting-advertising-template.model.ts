export interface ISettingAdvertisingTemplateCore {
  defaultBanner: boolean;
  displayProductName: boolean;
  freeDelivery: boolean;
  promotion: boolean;
  replacedBanner: string;
}

export type ISettingMobileCard = ISettingAdvertisingTemplateCore;

export type ISettingInteractiveBanner = ISettingAdvertisingTemplateCore;

export type ISettingFlyingCarpet = ISettingAdvertisingTemplateCore;

export type ISettingInReadEcommerce = ISettingAdvertisingTemplateCore;

export interface ISettingPostInRead extends ISettingAdvertisingTemplateCore {
  content: string;
  avatar: string;
  brandName: string;
}

export interface ISettingCatfishEcom extends ISettingAdvertisingTemplateCore {
  expandedContent: string;
}
