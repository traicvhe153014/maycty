import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ListAge,
  ListCategory,
  ListGender,
  ListLocation,
  ListProhibitedCategory,
} from '@shared/sharing/store';
import {
  AdsetService,
  CreateAdset,
  IAdsetByIdResponse,
  IAdsetCreateRequest,
  IAdsetUpdateRequest,
  UpdateAdset,
} from '@features/campaign-management/store/adset';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  createAdsetFormFieldErrors,
  createAdsetFormFields,
} from '@features/campaign-management/constants';
import { IFormFields } from '@models';
import {
  EAdsetAddRemarketingOption,
  MoreMarketingFormFields,
} from '@features/campaign-management/components';
import { convertAdsetToForm } from '@features/campaign-management/utils';
import { forkJoin, Observable, skip, Subject } from 'rxjs';
import * as dayjs from 'dayjs';
import {
  CampaignType,
  GetCampaign,
  ICampaignGetResponse,
  ICampaignScheduling,
  ICampaignStateModel,
} from '@features/campaign-management/store';
import { ClearObjectGroups } from '@features/feature-object-management/store';
import { scrollIntoViewBySelector } from '@core/utils';
import { FeatureProductManagementState } from '@features/product-group-management/store';
import { IProductGroupManagementData } from '@features/product-group-management/models';
import { takeUntil } from 'rxjs/operators';
import { IHttpGetRequest } from '@core/models';
import { GetWebsiteList } from '@shared/settings/store';

@Component({
  selector: 'novanet-adset-add',
  templateUrl: './adset-add.component.html',
  styleUrls: ['./adset-add.component.scss'],
})
export class AdsetAddComponent implements OnInit, OnDestroy {
  public formType: string; // add | edit
  public createForm: FormGroup;
  public readonly createFormFields = createAdsetFormFields;
  public readonly createFormFieldErrors = createAdsetFormFieldErrors;
  public readonly moreMarketingFormFields: IFormFields =
    MoreMarketingFormFields;
  public readonly campaignType = CampaignType;
  public currentAdset: IAdsetByIdResponse | undefined;
  public campaign: ICampaignGetResponse | undefined;
  public schedulingItems: Partial<ICampaignScheduling>[] = [];
  public isConfirmSubmitVisible = false;

  @Select(FeatureProductManagementState.getProductGroups)
  public productGroups$: Observable<IProductGroupManagementData[]>;

  public productGroups: IProductGroupManagementData[] = [];

  public isScrolled$: Subject<void> = new Subject<void>();

  private readonly destroy$: Subject<void> = new Subject<void>();

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private adsetService: AdsetService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get isUniformedDisabled() {
    return (
      this.formType === 'edit' &&
      dayjs(this.currentAdset?.campaign?.ecommerceStartDate).isBefore(dayjs())
    );
  }

  ngOnInit(): void {
    this.formType = this.router.url.includes('add-adset') ? 'add' : 'edit';
    this.buildCreateForm();
    const campaignId =
      this.activatedRoute.snapshot.queryParamMap.get('campaignId');
    this.createForm.patchValue({
      campaignId,
    });
    forkJoin([
      this.store.dispatch(new ListProhibitedCategory()),
      this.store.dispatch(new ListAge()),
      this.store.dispatch(new ListGender()),
      this.store.dispatch(new ListLocation()),
      this.store.dispatch(new ListCategory()),
    ]).subscribe({
      next: () => {
        this.changeDetectorRef.detectChanges();
      },
    });
    this.store.dispatch(new GetCampaign(campaignId)).subscribe({
      next: (data) => {
        this.campaign = (data.campaign as ICampaignStateModel).detailedCampaign;
        this.changeDetectorRef.detectChanges();
      },
    });

    if (this.formType === 'edit') {
      const adsetId = this.activatedRoute.snapshot.queryParamMap.get('adsetId');
      if (!adsetId) {
        this.router.navigateByUrl('/').then();
        return;
      }

      this.adsetService.getById(adsetId).subscribe({
        next: (adset) => {
          this.currentAdset = adset;
          const formValue = convertAdsetToForm(adset);
          this.createForm.patchValue({
            ...formValue,
            name: adset.name,
          });
          this.schedulingItems = adset.advertisingSetScheduling.map((item) => ({
            id: item.id,
            campaignId: item.campaignId,
            timing: new Date(item.timing),
            dayOfWeek: item.dayOfWeek,
          }));
          const params = {
            page: 1,
            pageSize: 100,
            keyWord: '',
          } as IHttpGetRequest;
          this.store.dispatch(new GetWebsiteList(params));
          this.changeDetectorRef.detectChanges();
        },
      });
    } else {
      const params = {
        page: 1,
        pageSize: 100,
        keyWord: '',
      } as IHttpGetRequest;
      this.store.dispatch(new GetWebsiteList(params));
    }
    this.getProductGroups();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearObjectGroups());
    this.isScrolled$.next();
    this.isScrolled$.complete();
  }

  public submitForm() {
    if (this.createForm.invalid) {
      this.createForm.markAllAsTouched();
      if (!this.createForm.get('name').value?.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Tên nhóm không được để trống'
          )
        );
      }
      setTimeout(() => {
        scrollIntoViewBySelector('.error-message');
      }, 200);
      return;
    }
    if (
      !this.createForm.get('applyAllProductGroups').value &&
      this.campaign.campaignType === CampaignType.Ecommerce
    ) {
      const productGroupIds = this.createForm.get('productGroupIds').value as
        | string[]
        | undefined;
      if (!productGroupIds?.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Bạn cần phải chọn tối thiểu 1 nhóm sản phẩm'
          )
        );
        setTimeout(() => {
          scrollIntoViewBySelector('.product-group', 'start');
        }, 200);
        return;
      }
      const allInvalidProductGroup = productGroupIds?.every(
        (productGroupId) => {
          const productGroup = this.productGroups.find(
            (item) => item.id === productGroupId
          );
          return productGroup && productGroup.runningProducts === 0;
        }
      );
      if (allInvalidProductGroup) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Nhóm chỉ có sản phẩm hết hàng không thể tạo quảng cáo'
          )
        );
        setTimeout(() => {
          scrollIntoViewBySelector('.product-group', 'start');
        }, 200);
        return;
      }
    }
    if (this.createForm.get('hasObject').value) {
      if (!this.createForm.get('objectIds').value?.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Bạn cần phải chọn tối thiểu 1 nhóm đối tượng'
          )
        );
        setTimeout(() => {
          scrollIntoViewBySelector('.object-target', 'start');
        }, 200);
        return;
      }
    }
    if (
      !this.createForm.get('ageIds').value?.length ||
      !this.createForm.get('locationIds').value?.length ||
      !this.createForm.get('genderIds').value?.length ||
      !this.createForm.get('categoryIds').value?.length
    ) {
      if (!this.createForm.get('ageIds').value?.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Bạn cần chọn tối thiểu 1 nhắm chọn theo độ tuổi để chạy'
          )
        );
      }
      if (!this.createForm.get('locationIds').value?.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Bạn cần chọn tối thiểu 1 nhắm chọn theo địa lý để chạy'
          )
        );
      }
      if (!this.createForm.get('genderIds').value?.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Bạn cần chọn tối thiểu 1 nhắm chọn theo giới tính để chạy'
          )
        );
      }
      if (!this.createForm.get('categoryIds').value.length) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Bạn cần chọn tối thiểu một chuyên mục để chạy'
          )
        );
      }

      setTimeout(() => {
        scrollIntoViewBySelector('.ad-target', 'start');
      }, 200);
      return;
    }
    if (!this.createForm.get('selectedWebsites').value?.length) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Bạn cần phải chọn tối thiểu 1 website để chạy'
        )
      );
      setTimeout(() => {
        scrollIntoViewBySelector('.more-marketing', 'start');
      }, 200);
      return;
    }
    if (
      !this.campaign?.ecommerceScheduled &&
      !this.createForm.get('scheduledAllTime').value &&
      !this.schedulingItems.length
    ) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Bạn cần chọn tối thiểu một khung giờ để tiếp tục'
        )
      );
      return;
    }
    if (this.formType === 'add') {
      const payload: IAdsetCreateRequest = this.createForm.value;

      this.store.dispatch(new CreateAdset(payload)).subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Tạo nhóm quảng cáo thành công'
            )
          );
        },
        error: () => {
          this.store.dispatch(
            new ShowGlobalNotification(GlobalNotificationEnum.error, 'Lỗi')
          );
        },
      });
    } else {
      const payload: Partial<IAdsetUpdateRequest> = {
        ...this.createForm.value,
        id: this.currentAdset?.id,
        campaignId: this.currentAdset?.campaignId,
        isFullUpdate: true,
      };
      if (!this.campaign?.ecommerceScheduled) {
        payload.schedulings = this.schedulingItems;
      }
      this.store.dispatch(new UpdateAdset(payload)).subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Cập nhật nhóm quảng cáo thành công'
            )
          );
          this.router
            .navigate(['/campaign/management'], {
              queryParams: { level: 'Adset' },
            })
            .then();
        },
        error: () => {
          this.store.dispatch(
            new ShowGlobalNotification(GlobalNotificationEnum.error, 'Lỗi')
          );
        },
      });
    }
  }

  public handleCancelSubmit() {
    this.isConfirmSubmitVisible = false;
  }

  public confirmSubmit() {
    this.submitForm();
    this.isConfirmSubmitVisible = false;
  }

  public onClickSubmit() {
    if (this.formType === 'add') {
      this.submitForm();
      return;
    }
    this.isConfirmSubmitVisible = true;
  }

  public clearForm() {
    this.createForm.patchValue(convertAdsetToForm({} as IAdsetByIdResponse));
  }

  private buildCreateForm() {
    const config = {
      [this.createFormFields.name.name]: [
        '',
        [
          Validators.maxLength(
            this.createFormFields.name.validationParams.maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.campaignId.name]: ['', []],
      [this.createFormFields.applyAllProductGroups.name]: [true, []],
      [this.createFormFields.productGroupIds.name]: [[], []],
      [this.createFormFields.hasProhibitedCategories.name]: [false, []],
      [this.createFormFields.prohibitedCategoryIds.name]: [[], []],
      [this.createFormFields.hasObject.name]: [false, []],
      [this.createFormFields.objectIds.name]: [[], []],
      [this.createFormFields.locationIds.name]: [[], []],
      [this.createFormFields.genderIds.name]: [[], []],
      [this.createFormFields.ageIds.name]: [[], []],
      [this.createFormFields.categoryIds.name]: [[], []],
      [this.createFormFields.uniformed.name]: [[], []],
      [this.moreMarketingFormFields['uniformed'].name]: [false],
      [this.moreMarketingFormFields['uniformedPrice'].name]: [
        0,
        [Validators.required],
      ],
      [this.moreMarketingFormFields['uniformedUnit'].name]: [
        0,
        [Validators.required],
      ],
      [this.moreMarketingFormFields['selectedWebsites'].name]: [[]],
      [this.moreMarketingFormFields['excludedWebsite'].name]: [false],
      [this.moreMarketingFormFields['excludedWebsites'].name]: [[]],
      [this.moreMarketingFormFields['targetingMarketingByProduct'].name]: [
        false,
      ],
      [this.moreMarketingFormFields['adImpressions'].name]: [
        100,
        [Validators.required],
      ],
      [this.moreMarketingFormFields['remarketingTime'].name]: [
        10,
        [Validators.required],
      ],
      [this.moreMarketingFormFields['timeOnDisplay'].name]: [
        50,
        [Validators.required],
      ],
      [this.moreMarketingFormFields['clickedAdvertising'].name]: [false],

      [this.moreMarketingFormFields['viewer'].name]: [false],

      [this.moreMarketingFormFields['viewedMax'].name]: [
        0,
        [Validators.required],
      ],

      [this.moreMarketingFormFields['remarketingType'].name]: [
        EAdsetAddRemarketingOption.NO_MORE_MARKETING,
      ],
      [this.createFormFields['scheduledAllTime'].name]: [true],
    };
    this.createForm = this.formBuilder.group(config);
  }

  private getProductGroups() {
    this.productGroups$
      .pipe(skip(1), takeUntil(this.destroy$))
      .subscribe((response) => {
        if (!response || !response.length) {
          return;
        }
        this.productGroups = [...this.productGroups, ...response];
      });
  }
}
