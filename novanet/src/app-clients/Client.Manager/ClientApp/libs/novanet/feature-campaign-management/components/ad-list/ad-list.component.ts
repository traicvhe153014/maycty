import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  AdsetState,
  AdvertisingState,
  CampaignState,
  CampaignStatusEnum,
  CampaignType,
  GetAdsetList,
  GetAdvertisingList,
  GetMoreAdsetList,
  GetMoreAdvertisingList,
  IAdsetResponse,
  IAdvertisingAppliedTemplate,
  IAdvertisingResponse,
  IAdvertisingSummary,
  IAdvertisingUpdateRequest,
  ICampaignListRequest,
  SelectAdvertising,
  UpdateAdvertising,
} from '@features/campaign-management/store';
import { IPredicateModel } from '@core/models';
import { ISettingColumnTable } from '@data-table/models';
import { Select, Store } from '@ngxs/store';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import {
  ExportReport,
  GlobalNotificationEnum,
  GlobalNotificationState,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  AdListSummaryEnum,
  AdListTableEnum,
  CampaignSearchLevelEnum,
  ReportOptions,
} from '@features/campaign-management/enums';
import {
  AdGroupOptionColumn,
  adListColumns,
  adListSetting,
  adListSummaryColumns,
  AdOptionColumnDefault,
  campaignManagementFilter,
  campaignSearchLevelFields,
  campaignTypeLabelList,
  MetricColumnEnumMappings,
} from '@features/campaign-management/constants';
import { shouldLoadMore } from '@core/utils';
import { ESortType, PredicateOperatorEnum } from '@core/enums';
import { ICampaignManagementFilter } from '@features/campaign-management/models';
import { session, storage } from '@components';
import { StorageService } from '@core/services';

@Component({
  selector: 'novanet-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.scss'],
})
export class AdListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public searchDropdownOpen = false;
  public selectedAds: string[] = [];
  public filterDateRange: Date[] = [];
  public filterStatus: CampaignStatusEnum | undefined = undefined;
  public filterCampaignType: CampaignType | undefined = undefined;
  public filterPredicates: IPredicateModel[] = [];
  public filterSelectedColumns: number[] = [];
  public selectedCampaigns: string[] = [];
  public selectedAdsets: string[] = [];
  public exceptFilterStatus: CampaignStatusEnum[] = [
    CampaignStatusEnum.Finished,
    CampaignStatusEnum.NotStarted,
    CampaignStatusEnum.Unfinished,
  ];
  public columnsConstant: number[] | [];
  public summaryColumnsConstant: number[] | [];
  public columnsDefault: ISettingColumnTable<IAdvertisingResponse, any>[] = [];

  public columns: ISettingColumnTable<IAdvertisingResponse, any>[] = [];
  public readonly pageSize = 50;
  public readonly settings = adListSetting;
  public summaryColumns = adListSummaryColumns;
  public readonly adListTableEnum = AdListTableEnum;
  public readonly adListSummaryEnum = AdListSummaryEnum;
  public readonly campaignTypeLabelList = campaignTypeLabelList;
  public readonly adGroupOptionColumn = AdGroupOptionColumn;
  public selectedColumnDefault = AdOptionColumnDefault;
  public readonly summaryColumnsDefault = adListSummaryColumns;

  @Select(AdvertisingState.getList) advertisings$: Observable<
    IAdvertisingResponse[]
  >;
  @Select(AdvertisingState.getPage) currentPage$: Observable<number>;
  @Select(AdvertisingState.getLoading) loading$: Observable<boolean>;
  @Select(AdvertisingState.getSummary)
  adSummary$: Observable<IAdvertisingSummary>;
  @Select(AdvertisingState.getSelected)
  selectedAdvertisingIds$: Observable<string[]>;
  @Select(AdsetState.getSelected)
  selectedAdsetIds$: Observable<string[]>;
  @Select(CampaignState.getSelected)
  selectedCampaignIds$: Observable<string[]>;
  @Select(GlobalNotificationState.getCancelDownloading)
  cancelDownloading$: Observable<boolean>;

  @Select(AdsetState.getList) adsets$: Observable<IAdsetResponse[]>;
  @Select(AdsetState.getPage) adsetCurrentPage$: Observable<number>;
  public searchAdsetKeyword = '';
  public searchAdsetKeywordChanged: Subject<string> = new Subject<string>();
  exportExcelSubscription: Subscription;
  public switchLoadings: { [key: string]: boolean } = {};
  private searchAdsetKeywordChangeSubscription: Subscription;
  private sort = '+status,-isActive,-modifiedAt';
  private readonly sortDefault = '+status,-isActive,-modifiedAt';
  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private store: Store,
    private storageService: StorageService
  ) {
    this.columnsConstant = [
      AdListTableEnum.CHECKBOX,
      AdListTableEnum.INDEX,
      AdListTableEnum.IS_ACTIVE,
      AdListTableEnum.NAME,
      AdListTableEnum.REDIRECT_LINK,
      AdListTableEnum.STATUS,
      AdListTableEnum.TEMPLATE,
    ];
    this.summaryColumnsConstant = [
      AdListSummaryEnum.COUNT,
      AdListSummaryEnum.CAMPAIGN_TYPE,
      AdListSummaryEnum.REDIRECT_LINK,
      AdListSummaryEnum.STATUS,
    ];
  }

  public get isAnySelected() {
    return this.selectedAds.length > 0;
  }

  public get validFilterStatus() {
    if (this.exceptFilterStatus.some((item) => item === this.filterStatus)) {
      return undefined;
    }
    return this.filterStatus;
  }

  ngAfterViewInit(): void {
    this.columns = adListColumns.map((item) =>
      item.id !== AdListTableEnum.CHECKBOX
        ? item
        : {
            ...item,
            title: this.checkboxHeader,
          }
    );
    this.columnsDefault = this.columns;

    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (
      filter &&
      filter.selectedColumnAdvertising &&
      filter.selectedColumnAdvertising.length > 0
    ) {
      this.selectedColumnDefault = filter.selectedColumnAdvertising;
      this.updateColumns(filter.selectedColumnAdvertising);
    } else {
      this.updateColumns(AdOptionColumnDefault);
    }
    this.changeDetectorRef.detectChanges();
  }

  public getFirstTemplate(
    advertising: IAdvertisingResponse
  ): IAdvertisingAppliedTemplate | undefined {
    return advertising.templates?.[0];
  }

  public onNextPage() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMoreAdvertisingList({
            filters: this.filterPredicates,
            page: currentPage + 1,
            pageSize: this.pageSize,
            startDate: this.filterDateRange[0],
            endDate: this.filterDateRange[1],
            status: this.validFilterStatus,
            campaignIds: this.selectedCampaigns,
            advertisingSetIds: this.selectedAdsets,
            sorts: this.sort,
            campaignType: this.filterCampaignType,
            fields: this.filterSelectedColumns
              .map((item) => MetricColumnEnumMappings[AdListTableEnum[item]])
              .filter((item) => !!item),
          })
        );
      },
    });
  }

  public onSelectAdChanged(event: Event, id: string) {
    let updatedAdIds: string[];
    if (this.isAdSelected(id)) {
      updatedAdIds = this.selectedAds.filter((adId) => adId !== id);
    } else {
      updatedAdIds = [...this.selectedAds, id];
    }
    session.set('tableAdSelected', updatedAdIds);
    this.store.dispatch(new SelectAdvertising(updatedAdIds));
  }

  public onSelectAllAdsChanged(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.advertisings$.pipe(take(1)).subscribe({
        next: (advertisings) => {
          const updatedAdIds = advertisings.map((ad) => ad.id);
          this.store.dispatch(new SelectAdvertising(updatedAdIds));
          session.set('tableAdSelected', updatedAdIds);
        },
      });
    } else {
      this.store.dispatch(new SelectAdvertising([]));
      session.set('tableAdSelected', []);
    }
  }

  public isAdSelected(id: string) {
    return !!this.selectedAds.find((adId) => adId === id);
  }

  ngOnInit() {
    if (session.get('tableAdSelected')) {
      this.store.dispatch(
        new SelectAdvertising(session.get('tableAdSelected'))
      );
    }
    this.adsetCurrentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.dispatchGetAdsetList(currentPage);
      },
    });
    this.searchAdsetKeywordChangeSubscription = this.searchAdsetKeywordChanged
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe({
        next: (model) => {
          this.searchAdsetKeyword = model;
          this.dispatchGetAdsetList(1);
        },
      });
    this.selectedAdvertisingIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (value) => {
        this.selectedAds = value;
        this.changeDetectorRef.detectChanges();
      },
    });
    this.selectedCampaignIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (value) => {
        this.selectedCampaigns = value;
        this.changeDetectorRef.detectChanges();
      },
    });
    this.selectedAdsetIds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (value) => {
        this.selectedAdsets = value;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  ngOnDestroy() {
    this.searchAdsetKeywordChangeSubscription.unsubscribe();
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onAdsetListScroll(event: Event) {
    if (shouldLoadMore(event.target as HTMLElement)) {
      this.adsetCurrentPage$.pipe(take(1)).subscribe({
        next: (currentPage) => {
          this.store.dispatch(
            new GetMoreAdsetList({
              page: currentPage + 1,
              pageSize: this.pageSize,
              filters: [
                {
                  field:
                    campaignSearchLevelFields[CampaignSearchLevelEnum.Adset],
                  operator: PredicateOperatorEnum.Contains,
                  value: this.searchAdsetKeyword,
                },
              ],
              withReport: false,
              withSummary: false,
            })
          );
        },
      });
    }
  }

  // FILTER CHANGE
  public onFilterPredicatesChange(predicates: IPredicateModel[]) {
    this.filterPredicates = predicates;
    this.dispatchGetList(1);
  }

  public onFilterCampaignTypeChange(campaignType: CampaignType) {
    this.filterCampaignType = campaignType;
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.campaignType = campaignType;
    } else {
      filter = {
        campaignType,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.dispatchGetList(1);
  }

  public onFilterStatusChange(e: CampaignStatusEnum) {
    this.filterStatus = e;
    this.dispatchGetList(1);
  }

  // FILTER CHANGE END

  public onFilterDateRangeChange(result: Date[]) {
    this.filterDateRange = result;
    this.dispatchGetList(1);
  }

  public onAllFilterChange(result: ICampaignManagementFilter) {
    this.filterDateRange = result.dateRange;
    this.filterStatus = result.status;
    this.filterPredicates = result.predicates;
    this.filterCampaignType = result.campaignType;
    this.filterSelectedColumns = result.selectedColumnAdvertising;
    forkJoin([
      this.selectedCampaignIds$.pipe(take(1)),
      this.selectedAdsetIds$.pipe(take(1)),
    ]).subscribe({
      next: ([selectedCampaigns, selectedAdset]) => {
        this.selectedCampaigns = selectedCampaigns;
        this.selectedAdsets = selectedAdset;

        this.dispatchGetList(1);
      },
    });
  }

  public createReport() {}

  public onAdIsActiveChange(ad: IAdvertisingResponse) {
    if (this.switchLoadings[ad.id]) {
      return;
    }
    const payload: IAdvertisingUpdateRequest = {
      id: ad.id,
      name: ad.name,
      isActive: !ad.isActive,
      isFullUpdate: false,
    };
    this.switchLoadings[ad.id] = true;
    this.store.dispatch(new UpdateAdvertising(payload)).subscribe({
      next: (response) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.switchLoadings[ad.id] = false;
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.switchLoadings[ad.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public dispatchGetList(page: number, value?, statusCustomColumn?: boolean) {
    if (value) {
      const sortedColumn = value[value.length - 1];
      this.columns.forEach((column) => {
        if (!sortedColumn || sortedColumn?.name !== column.name) {
          column.ascending = false;
          column.descending = false;
        } else {
          switch (sortedColumn.direction) {
            case ESortType.Ascending:
              this.sort = sortedColumn.name;
              column.ascending = true;
              column.descending = false;
              break;
            case ESortType.Descending:
              this.sort = '-' + sortedColumn.name;
              column.ascending = false;
              column.descending = true;
              break;
          }
        }
      });
      if (!sortedColumn) {
        this.sort = '';
      }
    }

    if (statusCustomColumn) {
      this.sort = this.sortDefault;
    }

    this.store.dispatch(
      new GetAdvertisingList({
        filters: this.filterPredicates,
        page,
        pageSize: this.pageSize,
        startDate: this.filterDateRange[0],
        endDate: this.filterDateRange[1],
        status: this.validFilterStatus,
        campaignIds:
          session.get('tableCampaignSelected') ?? this.selectedCampaigns,
        advertisingSetIds:
          session.get('tableAdsetSelected') ?? this.selectedAds,
        sorts: this.sort,
        campaignType: this.filterCampaignType,
        fields: this.filterSelectedColumns
          .map((item) => MetricColumnEnumMappings[AdListTableEnum[item]])
          .filter((item) => !!item),
      })
    );
  }

  public unsub() {
    if (this.exportExcelSubscription) {
      this.exportExcelSubscription.unsubscribe();
    }
  }

  public exportData($event) {
    const param = {
      startDate: this.filterDateRange[0],
      endDate: this.filterDateRange[1],
      exportType: ReportOptions.AdvertisementReport,
      advertisingIds: this.selectedAds,
    } as ICampaignListRequest;

    this.store.dispatch(new ExportReport(param));
  }

  sendFile() {
    // TODO: remove test send file
    // this.storageService.processVideo(this.videoFile).subscribe();
  }

  public updateColumns(columnUpdates: number[]) {
    const columnUpdate = [...columnUpdates, ...this.columnsConstant];
    const summaryColumnUpdate = [
      ...columnUpdates,
      ...this.summaryColumnsConstant,
    ];
    let columns = [];
    let summaryColumns = [];
    this.columnsDefault.forEach((col) => {
      columnUpdate.forEach((item, index) => {
        if (col.id === item) {
          columns.push(col);
        }
      });
    });
    this.summaryColumnsDefault.forEach((colSum) => {
      summaryColumnUpdate.forEach((item, index) => {
        if (colSum.id === item) {
          summaryColumns.push(colSum);
        }
      });
    });
    this.columns = columns;
    this.summaryColumns = summaryColumns;
    this.filterSelectedColumns = columns.map((item) => item.id);

    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.selectedColumnAdvertising = columnUpdates;
    } else {
      filter = {
        selectedColumnAdvertising: columnUpdates,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.dispatchGetList(1, undefined, true);
    this.changeDetectorRef.detectChanges();
  }

  private dispatchGetAdsetList(page: number) {
    this.store.dispatch(
      new GetAdsetList({
        page,
        pageSize: this.pageSize,
        filters: [
          {
            field: campaignSearchLevelFields[CampaignSearchLevelEnum.Adset],
            operator: PredicateOperatorEnum.Contains,
            value: this.searchAdsetKeyword,
          },
        ],
        withSummary: false,
        withReport: false,
      })
    );
  }
}
