import {
  ISettingCatfishEcom,
  ISettingFlyingCarpet,
  ISettingInReadEcommerce,
  ISettingInteractiveBanner,
  ISettingMobileCard,
  ISettingPostInRead,
} from './setting-advertising-template.model';
import { ETemplateType } from '@features/campaign-management/enums';

export interface ICreateAdvertising {
  advertisingType: ETemplateType;
  advertisingName: string;
  redirectUrl: string;
  settingAdvertising:
    | ISettingMobileCard
    | ISettingInteractiveBanner
    | ISettingFlyingCarpet
    | ISettingInReadEcommerce
    | ISettingPostInRead
    | ISettingCatfishEcom;
}
