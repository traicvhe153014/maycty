import { NgModule } from '@angular/core';
import { AdsetAddNameComponent } from './adset-add-name.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { SvgIconModule } from '@core/components/svg-icon';
import { AdsetService } from '@features/campaign-management/store';

@NgModule({
  declarations: [AdsetAddNameComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NovanetInputModule,
    NzPopoverModule,
    SvgIconModule,
  ],
  exports: [AdsetAddNameComponent],
  providers: [AdsetService],
})
export class AdsetAddNameModule {}
