import { DataTableSettingModel, ISummaryColumnTable } from '@data-table/models';
import {
  CampaignManagementSummaryEnum,
  CampaignManagementTableEnum,
  CampaignSearchLevelEnum,
} from '@features/campaign-management/enums';
import { PredicateOperatorEnum } from '@core/enums';
import {
  CampaignType,
  ICampaignResponse,
} from '@features/campaign-management/store/campaign';
import { IFormFieldErrors, IFormFields } from '@models';

export const CampaignListSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '125vw',
  summaryRow: true,
  sortType: 'single',
} as DataTableSettingModel;

export const campaignListSummaryColumns: ISummaryColumnTable<ICampaignResponse>[] =
  [
    {
      id: CampaignManagementSummaryEnum.COUNT,
      colspan: 4,
      pinLeft: true,
    },
    {
      id: CampaignManagementSummaryEnum.STATUS,
    },
    {
      id: CampaignManagementSummaryEnum.CAMPAIGN_TYPE,
    },
    {
      id: CampaignManagementSummaryEnum.BUDGET,
    },
    {
      id: CampaignManagementSummaryEnum.COSTS,
    },
    {
      id: CampaignManagementSummaryEnum.IMPRESSIONS,
    },
    {
      id: CampaignManagementSummaryEnum.CLICKS,
    },
    {
      id: CampaignManagementSummaryEnum.CTR,
    },
    {
      id: CampaignManagementSummaryEnum.CPC,
    },
    {
      id: CampaignManagementSummaryEnum.PURCHASE,
    },
    {
      id: CampaignManagementSummaryEnum.PURCHASE_CONVERSION,
    },
    {
      id: CampaignManagementSummaryEnum.CPS,
    },
    {
      id: CampaignManagementSummaryEnum.VIDEO_VIEW_3S,
    },
    {
      id: CampaignManagementSummaryEnum.VIDEO_25_PERCENT,
    },
    {
      id: CampaignManagementSummaryEnum.VIDEO_50_PERCENT,
    },
    {
      id: CampaignManagementSummaryEnum.VIDEO_75_PERCENT,
    },
    {
      id: CampaignManagementSummaryEnum.VIDEO_100_PERCENT,
    },
    {
      id: CampaignManagementSummaryEnum.DATE,
    },
  ];

export const shortDateDayjsFormat = 'DD/MM/YYYY';
export const shortDateFormat = 'dd/MM/yyyy';
export const campaignManagementFilter = 'CampaignManagementFilter';

export const campaignNumberPattern = /^\d+$/;
export const campaignMinimumAmountSpent = 100000;
export const campaignMinimumDailyBudget = 1000;

export const campaignSearchLevelLabels = {
  [CampaignSearchLevelEnum.Campaign]: 'Chiến dịch',
  [CampaignSearchLevelEnum.Adset]: 'Nhóm quảng cáo',
  [CampaignSearchLevelEnum.Ad]: 'Tin quảng cáo',
  [CampaignSearchLevelEnum.Template]: 'Định dạng',
};

export const campaignSearchLevelFields = {
  [CampaignSearchLevelEnum.Campaign]: 'Campaign.Name',
  [CampaignSearchLevelEnum.Adset]: 'Adset.Name',
  [CampaignSearchLevelEnum.Ad]: 'Ad.Name',
  [CampaignSearchLevelEnum.Template]: 'Template',
};

export const campaignSearchOperatorLabels = {
  [PredicateOperatorEnum.Contains]: 'chứa',
  [PredicateOperatorEnum.NotContains]: 'không chứa',
};

export const campaignTypeLabels = {
  [CampaignType.Ecommerce]: 'Ecommerce',
  [CampaignType.Display]: 'Display ads',
  // [CampaignType.Native]: 'Native ads',
};

export const campaignTypeLabelList = [
  { value: CampaignType.Display, label: 'Display ads' },
  { value: CampaignType.Ecommerce, label: 'Ecommerce' },
];

export const campaignCreateFormFields: IFormFields = {
  name: {
    name: 'name',
    validationParams: {
      required: true,
      maxLength: 150,
    },
  },
  campaignType: {
    name: 'campaignType',
    validationParams: {
      required: true,
    },
  },
  ecommerceAmountSpent: {
    name: 'ecommerceAmountSpent',
    validationParams: {
      required: true,
      pattern: campaignNumberPattern,
      min: campaignMinimumAmountSpent,
    },
  },
  ecommerceDailyBudget: {
    name: 'ecommerceDailyBudget',
    validationParams: {
      required: true,
      pattern: campaignNumberPattern,
      min: campaignMinimumDailyBudget,
    },
  },
  ecommerceProductFeedId: {
    name: 'ecommerceProductFeedId',
    validationParams: {
      required: true,
    },
  },
  ecommerceStartDate: {
    name: 'ecommerceStartDate',
    validationParams: {
      required: true,
    },
  },
  ecommerceEndDate: {
    name: 'ecommerceEndDate',
  },
  ecommerceScheduled: {
    name: 'ecommerceScheduled',
  },
  ecommerceScheduledAllTime: {
    name: 'ecommerceScheduledAllTime',
  },
  schedulings: {
    name: 'schedulings',
  },
};

export const createFormFieldErrors: IFormFieldErrors = {
  name: [
    {
      type: 'required',
      message: 'Thiếu tên chiến dịch',
    },
    {
      type: 'maxlength',
      message: 'Số lượng kí tự tối đa là 150 kí tự',
    },
  ],
  ecommerceProductFeedId: [
    {
      type: 'required',
      message: 'Bạn cần chọn nguồn dữ liệu để tạo chiến dịch',
    },
  ],
  ecommerceAmountSpent: [
    {
      type: 'required',
      message: 'Thiếu tổng ngân sách',
    },
    {
      type: 'pattern',
      message: 'Ngân sách không hợp lệ',
    },
    {
      type: 'maxlength',
      message: 'Tổng ngân sách tối đa 12 kí tự',
    },
    {
      type: 'min',
      message: 'Ngân sách tối thiểu 100,000 đ',
    },
    {
      type: 'lessThanCost',
      message: 'Ngân sách không được nhỏ hơn tổng chi phí',
    },
  ],
  ecommerceDailyBudget: [
    {
      type: 'required',
      message: 'Thiếu ngân sách hàng ngày',
    },
    {
      type: 'pattern',
      message: 'Ngân sách hàng ngày không hợp lệ',
    },
    {
      type: 'maxlength',
      message: 'Tổng ngân sách tối đa 12 kí tự',
    },
    {
      type: 'min',
      message: 'Ngân sách hàng ngày tối thiểu 1,000 đ',
    },
    {
      type: 'exceedAmountSpent',
      message: 'Ngân sách hàng ngày không được lớn hơn tổng ngân sách',
    },
  ],
  ecommerceStartDate: [
    {
      type: 'required',
      message: 'Thiếu ngày bắt đầu',
    },
    {
      type: 'exceedEndDate',
      message: 'Ngày bắt đầu không vượt quá ngày kết thúc',
    },
  ],
};

export const CampaignOptionColumnDefault = [
  CampaignManagementTableEnum.BUDGET,
  CampaignManagementTableEnum.COSTS,
  CampaignManagementTableEnum.IMPRESSIONS,
  CampaignManagementTableEnum.CLICKS,
  CampaignManagementTableEnum.CTR,
  CampaignManagementTableEnum.CPC,
  CampaignManagementTableEnum.VIDEO_VIEW_3S,
  CampaignManagementTableEnum.PURCHASE,
  CampaignManagementTableEnum.PURCHASE_CONVERSION,
  CampaignManagementTableEnum.CPS,
] as number[];
