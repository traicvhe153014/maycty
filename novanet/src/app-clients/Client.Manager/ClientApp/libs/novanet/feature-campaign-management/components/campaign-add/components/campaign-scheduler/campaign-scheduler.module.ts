import { NgModule } from '@angular/core';
import { CampaignSchedulerComponent } from './campaign-scheduler.component';
import { CommonModule } from '@angular/common';
import {
  IsScheduleAllDaySelectedPipe,
  IsScheduleAllSelectedPipe,
  IsScheduleAllWeekSelectedPipe,
  IsScheduleSelectedPipe,
} from './pipes';

@NgModule({
  declarations: [
    CampaignSchedulerComponent,
    IsScheduleAllWeekSelectedPipe,
    IsScheduleSelectedPipe,
    IsScheduleAllDaySelectedPipe,
    IsScheduleAllSelectedPipe,
  ],
  imports: [CommonModule],
  exports: [CampaignSchedulerComponent],
})
export class CampaignSchedulerModule {}
