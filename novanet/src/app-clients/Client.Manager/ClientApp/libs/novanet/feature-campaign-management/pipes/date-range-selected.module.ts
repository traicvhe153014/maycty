import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRangeSelectedPipe } from './date-range-selected.pipe';

@NgModule({
  declarations: [DateRangeSelectedPipe],
  imports: [CommonModule],
  exports: [DateRangeSelectedPipe],
})
export class DateRangeSelectedModule {}
