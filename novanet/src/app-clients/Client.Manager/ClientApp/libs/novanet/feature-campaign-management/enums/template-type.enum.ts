export enum ETemplateType {
  MobileBannerCard = 1,
  InteractiveBanner,
  FlyingCarpet,
  InReadEcommerce,
  PostInRead,
  CatfishEcom,
  NotificationBanner,
  CatFishCollabBranding,
  PopupBannerBranding,
}
