import { Pipe, PipeTransform } from '@angular/core';
import { ILocation } from '@shared/sharing/store';

@Pipe({
  name: 'selectedByRegion',
})
export class SelectedByRegionPipe implements PipeTransform {
  transform(
    value: string[],
    regionName: string,
    locations: ILocation[]
  ): string {
    const allLocationsInRegion: ILocation[] = [];
    const selectedLocations: ILocation[] = [];
    locations.forEach((location): void => {
      if (location.regionName !== regionName) {
        return;
      }
      allLocationsInRegion.push(location);
      if (!!value.find((id) => id === location.id)) {
        selectedLocations.push(location);
      }
    });
    return `(${selectedLocations.length}/${allLocationsInRegion.length})`;
  }
}
