import { CampaignSearchLevelEnum } from '@features/campaign-management/enums';
import { ESortType, PredicateOperatorEnum } from '@core/enums';

export interface ICampaignSearchOption {
  value: string;
  operator: PredicateOperatorEnum;
  level: CampaignSearchLevelEnum;
}

export interface ICampaignSortOption {
  field: string;
  type: ESortType;
}
