import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ETemplateType } from '@features/campaign-management/enums';
import {
  ITemplateConfiguration,
  ITemplateDisplayConfiguration,
} from '@features/campaign-management/store';
import {
  AllowSizeColumnPopupBanner,
  AllowSizeSetting,
} from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/constant';
import { ESettingFormatAdvertising } from '@features/campaign-management/components/create-advertising/components/setting-format-advertising/enums';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { isFileSizeValid } from '@core/utils';
import { zipMimeType } from '@features/campaign-management/components/create-advertising/components/setting-format-advertising/constant';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { Store } from '@ngxs/store';
import { cloneDeep } from 'lodash';
import {
  createPopupBannerFormFields,
  createPopupBannerFromFieldErrors,
} from './popup-banner-branding.contants';
import {
  bannerAllowedExtensionPattern,
  logoAllowedExtensionPattern,
} from '@shared/sharing/constants';

interface ImageModel {
  name: string;
  src: string;
  file: File;
}

@Component({
  selector: 'novanet-popup-banner-branding',
  templateUrl: './popup-banner-branding.component.html',
})
export class PopupBannerBrandingComponent implements OnInit {
  @Input() public height = 0;
  @Input() public width = 0;
  @Input() public dataTable = [];
  @Input() public templateType: ETemplateType;
  @Input() public templateConfiguration: ITemplateConfiguration;
  @Output() public templateConfigurationChange =
    new EventEmitter<ITemplateDisplayConfiguration>();

  public templateTypes = ETemplateType;
  public settingColumn = AllowSizeColumnPopupBanner;
  public settingTable = AllowSizeSetting;
  public allowSizeColumn = ESettingFormatAdvertising;
  public notificationBannerForm: FormGroup;
  public currentAvatar: ImageModel[] = [];
  public currentExtendBanner: ImageModel[] = [];

  public readonly createFormFields = createPopupBannerFormFields;
  public readonly createFormFieldErrors = createPopupBannerFromFieldErrors;

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get logo(): FormControl {
    return this.notificationBannerForm.get('logo') as FormControl;
  }

  get extendBanner(): FormControl {
    return this.notificationBannerForm.get('extendBanner') as FormControl;
  }

  get title(): string {
    return this.notificationBannerForm.get('title').value;
  }

  get description(): string {
    return this.notificationBannerForm.get('description').value;
  }

  get isCta(): boolean {
    return this.notificationBannerForm.get('isCta').value;
  }

  get cta(): boolean {
    return this.notificationBannerForm.get('cta').value;
  }

  get imageLogoLink(): FormControl {
    return this.notificationBannerForm.get('imageLogoLink') as FormControl;
  }

  get imageExtendBannerLink(): FormControl {
    return this.notificationBannerForm.get(
      'imageExtendBannerLink'
    ) as FormControl;
  }

  public ngOnInit() {
    this.buildNotificationBannerForm();
  }

  public handleImageInput(field: string, event: Event) {
    const files = (event.target as HTMLInputElement).files;
    const fileList = [] as File[];
    // eslint-disable-next-line @typescript-eslint/prefer-for-of
    for (let i = 0; i < files.length; i++) {
      fileList.push(files.item(i));
    }
    let maxSize = 5;
    if (field === 'logo') {
      maxSize = 1;
    }
    if (isFileSizeValid(fileList, 1024000 * maxSize)) {
      const allowedFileType =
        field === 'logo'
          ? logoAllowedExtensionPattern
          : bannerAllowedExtensionPattern;
      if (!allowedFileType.test(fileList[0].name)) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Cảnh báo',
            'Sai định dạng ảnh'
          )
        );
        (event.target as HTMLInputElement).value = '';
        return;
      }
      const fr = new FileReader();
      fr.onload = () => {
        if (
          files.item(0)?.type === zipMimeType &&
          field === 'expandedContent'
        ) {
          this.templateConfiguration.configurations.images = null;
          (event.target as HTMLInputElement).value = '';
          return;
        }
        const fileType = files.item(0)?.type;
        if (
          (fileType === 'text/html' || fileType === zipMimeType) &&
          field === 'extendBanner'
        ) {
          this.currentExtendBanner.push({
            name: fileList[0].name,
            src: fileList[0].name,
            file: fileList[0],
          });
          this.notificationBannerForm.patchValue({
            imageExtendBannerLink: files[0]
              ? window.URL.createObjectURL(files[0])
              : '',
            popupExtendBannerFileType:
              fileType === zipMimeType ? 'zip' : 'html',
          });
          this.changeDetectorRef.detectChanges();
          this.extendBanner.patchValue(cloneDeep(this.currentExtendBanner[0]));
          (event.target as HTMLInputElement).value = '';
          return;
        }
        const img = new Image();
        img.onload = () => {
          switch (field) {
            case 'logo':
              this.currentAvatar.push({
                name: fileList[0].name,
                src: img.src,
                file: fileList[0],
              });
              this.notificationBannerForm.patchValue({
                imageLogoLink: files[0]
                  ? window.URL.createObjectURL(files[0])
                  : '',
              });
              this.changeDetectorRef.detectChanges();
              this.logo.patchValue(cloneDeep(this.currentAvatar[0]));
              (event.target as HTMLInputElement).value = '';
              break;
            case 'extendBanner':
              if (img.width / img.height !== 375 / 810) {
                this.store.dispatch(
                  new ShowGlobalNotification(
                    GlobalNotificationEnum.warning,
                    'Cảnh báo',
                    'Sai kích thước ảnh'
                  )
                );
                (event.target as HTMLInputElement).value = '';
                return;
              }
              this.currentExtendBanner.push({
                name: fileList[0].name,
                src: img.src,
                file: fileList[0],
              });
              this.notificationBannerForm.patchValue({
                imageExtendBannerLink: files[0]
                  ? window.URL.createObjectURL(files[0])
                  : '',
                popupExtendBannerFileType: 'image',
              });
              this.changeDetectorRef.detectChanges();
              this.extendBanner.patchValue(
                cloneDeep(this.currentExtendBanner[0])
              );
              (event.target as HTMLInputElement).value = '';
              break;
          }
          this.changeDetectorRef.detectChanges();
        };
        img.src = fr.result as any;
      };
      fr.readAsDataURL(fileList[0]);
      this.changeDetectorRef.detectChanges();
    } else {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên dữ liệu quá ' + maxSize + 'MB'
        )
      );
      (event.target as HTMLInputElement).value = '';
    }
  }

  public clearAvatar(field: string) {
    switch (field) {
      case 'logo':
        this.currentAvatar = [];
        this.logo.reset();
        this.imageLogoLink.reset();
        break;
      case 'extendBanner':
        this.currentExtendBanner = [];
        this.extendBanner.patchValue(null);
        this.imageExtendBannerLink.reset();
        this.notificationBannerForm.patchValue({
          popupExtendBannerFileType: 'image',
        });
        break;
    }
  }

  private popupFormValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    // validate cta
    const isCta = control.get(createPopupBannerFormFields.isCta.name)
      .value as boolean;
    const cta = control.get(createPopupBannerFormFields.cta.name)
      .value as string;
    let ctaError: { [key: string]: boolean } | null = null;

    if (isCta) {
      if (!cta) {
        ctaError = { required: true };
      } else if (
        cta.length > createPopupBannerFormFields.cta.validationParams.maxLength
      ) {
        ctaError = { maxlength: true };
      }
    }
    control.get(createPopupBannerFormFields.cta.name).setErrors(ctaError);
    return null;
  }

  private buildNotificationBannerForm() {
    const config = {
      [this.createFormFields.title.name]: [
        this.templateConfiguration.configurations.title ??
          this.templateConfiguration.configurations.title,
        [
          Validators.maxLength(
            this.createFormFields.title.validationParams.maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.logo.name]: [
        this.templateConfiguration.configurations.logo ?? '',
        [],
      ],
      [this.createFormFields.extendBanner.name]: [
        this.templateConfiguration.configurations.extendBanner ?? '',
        [],
      ],
      [this.createFormFields.description.name]: [
        this.templateConfiguration.configurations.description ?? '',
        [
          Validators.required,
          Validators.maxLength(
            this.createFormFields.description.validationParams
              .maxLength as number
          ),
        ],
      ],
      [this.createFormFields.cta.name]: [
        this.templateConfiguration.configurations.cta ?? '',
        [
          Validators.required,
          Validators.maxLength(
            this.createFormFields.cta.validationParams.maxLength as number
          ),
        ],
      ],
      [this.createFormFields.ctaUrl.name]: [
        this.templateConfiguration.configurations.ctaUrl ?? '',
        [
          Validators.pattern(
            this.createFormFields.ctaUrl.validationParams.pattern
          ),
          Validators.maxLength(
            this.createFormFields.ctaUrl.validationParams.maxLength as number
          ),
        ],
      ],
      [this.createFormFields.isCta.name]: [
        this.templateConfiguration.configurations.isCta ?? true,
        [Validators.required],
      ],
      ['imageLogoLink']: [''],
      ['imageExtendBannerLink']: [''],
      ['popupExtendBannerFileType']: [
        this.getFileType(
          this.templateConfiguration.configurations.extendBanner
        ),
      ],
    };

    this.notificationBannerForm = this.formBuilder.group(config, {
      validators: this.popupFormValidator,
    });

    this.notificationBannerForm.valueChanges.subscribe((value) => {
      if (!this.extendBanner.value) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống banner mở rộng',
          configurations: value,
        });
        return;
      }

      if (!this.title) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống tiêu đề',
          configurations: value,
        });
        return;
      }

      if (!this.description) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống nội dung',
          configurations: value,
        });
        return;
      }

      if (this.isCta) {
        if (!this.cta) {
          this.templateConfigurationChange.emit({
            isValid: false,
            message: 'Không để trống CTA',
            configurations: value,
          });
          return;
        }
      }

      this.templateConfigurationChange.emit({
        isValid: this.notificationBannerForm.valid,
        message: !this.notificationBannerForm.valid
          ? 'Vui lòng điền trường dữ liệu bắt buộc'
          : '',
        configurations: value,
      });
    });
  }

  private getFileType(fileName: string | undefined) {
    return fileName?.includes('.html')
      ? 'html'
      : fileName?.includes('.zip')
      ? 'zip'
      : 'image';
  }
}
