import { IFormFieldErrors, IFormFields } from '@models';
import { urlPattern } from '@shared/sharing/constants';

export const createNotificationBannerFromFields: IFormFields = {
  logo: {
    name: 'logo',
  },
  title: {
    name: 'title',
    validationParams: {
      required: true,
      maxLength: 40,
    },
  },
  shortcutNotification: {
    name: 'shortcutNotification',
    validationParams: {
      maxLength: 120,
    },
  },
  extendNotification: {
    name: 'extendNotification',
    validationParams: {
      required: true,
      maxLength: 250,
    },
  },
  cta: {
    name: 'cta',
    validationParams: {
      required: true,
      maxLength: 20,
    },
  },
  ctaUrl: {
    name: 'ctaUrl',
    validationParams: {
      pattern: urlPattern,
      maxLength: 2048,
    },
  },
  delay: {
    name: 'delay',
    validationParams: {
      required: true,
      min: 1,
      max: 60,
    },
  },
  images: {
    name: 'images',
  },
  isCta: {
    name: 'isCta',
  },
  isCustomImage: {
    name: 'isCustomImage',
  },
  isDelay: {
    name: 'isDelay',
  },
};

export const createNotificationBannerFromFieldErrors: IFormFieldErrors = {
  title: [
    {
      type: 'required',
      message: 'Thiếu tiêu đề',
    },
    {
      type: 'maxlength',
      message: 'Tiêu đề tối đa 30 kí tự',
    },
  ],
  shortcutNotification: [
    {
      type: 'maxlength',
      message: 'Thông báo rút gọn tối đa 40 kí tự',
    },
  ],
  extendNotification: [
    {
      type: 'required',
      message: 'Thiếu thông báo mở rộng',
    },
    {
      type: 'maxlength',
      message: 'Thông báo mở rộng tối đa 250 kí tự',
    },
  ],
  cta: [
    {
      type: 'required',
      message: 'Thiếu nội dung CTA',
    },
    {
      type: 'maxlength',
      message: 'Nội dung CTA tối đa 20 kí tự',
    },
  ],
  ctaUrl: [
    {
      type: 'maxlength',
      message: 'Số lượng tối đa là 2048 kí tự',
    },
    {
      type: 'pattern',
      message: 'Không đúng định dạng URL',
    },
  ],
  delay: [
    {
      type: 'required',
      message: 'Không để trống dữ liệu',
    },
    {
      type: 'min',
      message: 'Độ trễ trong khoảng 1-60s',
    },
    {
      type: 'max',
      message: 'Độ trễ trong khoảng 1-60s',
    },
  ],
};
