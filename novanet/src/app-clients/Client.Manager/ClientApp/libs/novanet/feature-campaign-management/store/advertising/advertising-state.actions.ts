import {
  IAdvertisingCreateRequest,
  IAdvertisingListRequest,
  IAdvertisingListResponse,
  IAdvertisingResponse,
  IAdvertisingTemplate,
  IAdvertisingUpdateRequest,
} from './advertising-state.model';

const enum AdvertisingActions {
  GetAdvertisingList = '[Advertising] Get List',
  GetAdvertisingListSuccess = '[Advertising] Get List Success',
  GetAdvertisingListError = '[Advertising] Get List Error',
  GetMoreAdvertisingList = '[Advertising] Get More List',
  GetMoreAdvertisingListSuccess = '[Advertising] Get More List Success',
  GetMoreAdvertisingListError = '[Advertising] Get More List Error',
  ListAdvertisingTemplate = '[Advertising] List Template',
  ListAdvertisingTemplateSuccess = '[Advertising] List Template Success',
  ListAdvertisingTemplateError = '[Advertising] List Template Error',
  CreateAdvertising = '[Advertising] Create',
  CreateAdvertisingSuccess = '[Advertising] Create Success',
  CreateAdvertisingError = '[Advertising] Create Error',
  UpdateAdvertising = '[Advertising] Update',
  UpdateAdvertisingSuccess = '[Advertising] Update Success',
  UpdateAdvertisingError = '[Advertising] Update Error',
  SelectAdvertising = '[Advertising] Select',
  ClearAdvertising = '[Advertising] Clear',
}

export class GetAdvertisingList {
  public static readonly type = AdvertisingActions.GetAdvertisingList;

  constructor(public params: IAdvertisingListRequest) {}
}

export class GetAdvertisingListSuccess {
  public static readonly type = AdvertisingActions.GetAdvertisingListSuccess;

  constructor(
    public response: IAdvertisingListResponse,
    public params: IAdvertisingListRequest
  ) {}
}

export class GetAdvertisingListError {
  public static readonly type = AdvertisingActions.GetAdvertisingListError;

  constructor(public error: any) {}
}

export class GetMoreAdvertisingList {
  public static readonly type = AdvertisingActions.GetMoreAdvertisingList;

  constructor(public params: IAdvertisingListRequest) {}
}

export class GetMoreAdvertisingListSuccess {
  public static readonly type =
    AdvertisingActions.GetMoreAdvertisingListSuccess;

  constructor(
    public response: IAdvertisingListResponse,
    public params: IAdvertisingListRequest
  ) {}
}

export class GetMoreAdvertisingListError {
  public static readonly type = AdvertisingActions.GetMoreAdvertisingListError;

  constructor(public error: any) {}
}

export class ListAdvertisingTemplate {
  public static readonly type = AdvertisingActions.ListAdvertisingTemplate;

  constructor() {}
}

export class ListAdvertisingTemplateSuccess {
  public static readonly type =
    AdvertisingActions.ListAdvertisingTemplateSuccess;

  constructor(public response: IAdvertisingTemplate[]) {}
}

export class ListAdvertisingTemplateError {
  public static readonly type = AdvertisingActions.ListAdvertisingTemplateError;

  constructor(public error: any) {}
}

export class CreateAdvertising {
  public static readonly type = AdvertisingActions.CreateAdvertising;

  constructor(public payload: IAdvertisingCreateRequest) {}
}

export class CreateAdvertisingSuccess {
  public static readonly type = AdvertisingActions.CreateAdvertisingSuccess;

  constructor(public advertising: IAdvertisingResponse) {}
}

export class CreateAdvertisingError {
  public static readonly type = AdvertisingActions.CreateAdvertisingError;

  constructor(public error: any) {}
}

export class UpdateAdvertising {
  public static readonly type = AdvertisingActions.UpdateAdvertising;

  constructor(public payload: IAdvertisingUpdateRequest) {}
}

export class UpdateAdvertisingSuccess {
  public static readonly type = AdvertisingActions.UpdateAdvertisingSuccess;

  constructor(public advertising: IAdvertisingResponse) {}
}

export class UpdateAdvertisingError {
  public static readonly type = AdvertisingActions.UpdateAdvertisingError;

  constructor(public error: any) {}
}

export class SelectAdvertising {
  public static readonly type = AdvertisingActions.SelectAdvertising;

  constructor(public advertisingIds: string[]) {}
}

export class ClearAdvertising {
  public static readonly type = AdvertisingActions.ClearAdvertising;

  constructor() {}
}
