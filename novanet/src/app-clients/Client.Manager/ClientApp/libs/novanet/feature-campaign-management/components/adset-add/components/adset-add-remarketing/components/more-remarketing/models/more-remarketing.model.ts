export interface IMoreRemarketing {
  uniformed: boolean;
  uniformedPrice: number;
  uniformedUnit: string;
  selectedWebsites: string[];
  excludedWebsite: boolean;
  excludedWebsites: string[];
  targetingMarketingByProduct: boolean;
  adImpressions: number;
  timeOnDisplay: number;
  remarketingTime: number;
  conditionsStoppingMarketing: IConditionsStoppingMarketing;
}

export interface IConditionsStoppingMarketing {
  clickedAdvertising: boolean;
  viewer: boolean;
  viewedMax: number;
}
