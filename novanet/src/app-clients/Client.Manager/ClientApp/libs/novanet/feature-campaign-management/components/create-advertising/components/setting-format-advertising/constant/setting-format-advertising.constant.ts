import { IBannerDetail } from '../models';

export const MobileBannerCard = {
  type: 'Banner ngang',
  containerSize: '375x180px',
  safeArea: '110x180px',
  fileSize: '1 MB',
};

export const InteractiveBanner = {
  type: 'Banner dọc',
  containerSize: '375x810px',
  safeArea: '375x255px',
  fileSize: '1mb',
  fileType: 'Html5,Jpeg, png, Gif',
} as IBannerDetail;

export const FlyingCarpetBanner = {
  type: 'Banner dọc',
  containerSize: '375x810px',
  safeArea: '375x200px',
  fileSize: '1mb',
} as IBannerDetail;

export const InReadEcommerceBanner = {
  type: 'Banner dọc',
  containerSize: '375x600px',
  safeArea: '375x150px',
  fileSize: '1 MB',
} as IBannerDetail;

export const CatfishEcomBanner: IBannerDetail[] = [
  {
    type: 'Banner chân trang',
    containerSize: '375x120px',
    safeArea: '375x20px',
    fileSize: '1 MB',
  },
  {
    type: 'Banner mở rộng',
    containerSize: '375x810px',
    safeArea: '375x600px',
    fileSize: '1 MB',
  },
];

export const NotificationBannerImage: IBannerDetail[] = [
  {
    type: 'Banner ngang',
    containerSize: '300x200 px',
    safeArea: '300x200 px',
    fileSize: '1mb',
    fileType: 'Jpeg, png',
  },
];

export const CatfishCollapseBanner: IBannerDetail[] = [
  {
    type: 'Banner thu gọn',
    containerSize: 'Html5, Jpeg, png, Gif',
    safeArea: '375x120',
    fileSize: '5mb',
    fileType: 'Html5, Jpeg, png, Gif',
  },
  {
    type: 'Banner mở rộng',
    containerSize: 'Html5, Jpeg, png, Gif',
    safeArea: '375x810',
    fileSize: '5mb',
    fileType: 'Html5, Jpeg, png, Gif',
  },
];

export const PopupBannerBranding = {
  type: 'Banner dọc',
  containerSize: 'Html5,Jpeg, png, Gif',
  safeArea: '375x810px',
  fileSize: '5mb',
  fileType: 'Html5,Jpeg, png, Gif',
} as IBannerDetail;

export const zipMimeType = 'application/x-zip-compressed';
