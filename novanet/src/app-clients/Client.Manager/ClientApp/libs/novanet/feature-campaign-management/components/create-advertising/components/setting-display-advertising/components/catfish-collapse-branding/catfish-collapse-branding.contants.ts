import { IFormFieldErrors, IFormFields } from '@models';
import { urlPattern } from '@shared/sharing/constants';

export const createCatFishCollapseFormFields: IFormFields = {
  logo: {
    name: 'logo',
  },
  collapseBanner: {
    name: 'collapseBanner',
    validationParams: {
      required: true,
    },
  },
  extendBanner: {
    name: 'extendBanner',
    validationParams: {
      required: true,
    },
  },
  title: {
    name: 'title',
    validationParams: {
      required: true,
      maxLength: 30,
    },
  },
  description: {
    name: 'description',
    validationParams: {
      required: true,
      maxLength: 250,
    },
  },
  cta: {
    name: 'cta',
    validationParams: {
      required: true,
      maxLength: 20,
    },
  },
  ctaUrl: {
    name: 'ctaUrl',
    validationParams: {
      pattern: urlPattern,
      maxLength: 2048,
    },
  },
  isCta: {
    name: 'isCta',
  },
};

export const createCatfishCollabBrandingFormFieldErrors: IFormFieldErrors = {
  title: [
    {
      type: 'required',
      message: 'Thiếu tiêu đề',
    },
    {
      type: 'maxlength',
      message: 'Tiêu đề tối đa 30 kí tự',
    },
  ],
  description: [
    {
      type: 'required',
      message: 'Thiếu nội dung',
    },
    {
      type: 'maxlength',
      message: 'Nội dung tối đa 250 kí tự',
    },
  ],
  cta: [
    {
      type: 'required',
      message: 'Thiếu nội dung CTA',
    },
    {
      type: 'maxlength',
      message: 'Nội dung CTA tối đa 20 kí tự',
    },
  ],
  ctaUrl: [
    {
      type: 'maxlength',
      message: 'Số lượng tối đa là 2048 kí tự',
    },
    {
      type: 'pattern',
      message: 'Không đúng định dạng URL',
    },
  ],
};
