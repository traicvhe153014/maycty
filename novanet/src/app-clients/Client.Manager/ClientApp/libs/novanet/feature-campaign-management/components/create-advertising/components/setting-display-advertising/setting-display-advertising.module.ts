import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { DataTableModule } from '@core';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { SvgIconModule } from '@core/components/svg-icon';
import { SettingDisplayAdvertisingComponent } from './setting-display-advertising.component';
import {
  CatfishCollapseBrandingComponent,
  NotificationBannerComponent,
  PopupBannerBrandingComponent,
} from './components';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

const MODULES: any[] = [
  FormsModule,
  NzSwitchModule,
  DataTableModule,
  CommonModule,
  NovanetInputModule,
  SvgIconModule,
];
const COMPONENTS: any[] = [
  SettingDisplayAdvertisingComponent,
  CatfishCollapseBrandingComponent,
  PopupBannerBrandingComponent,
  NotificationBannerComponent,
];

@NgModule({
  imports: [...MODULES, ReactiveFormsModule, NzToolTipModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class SettingDisplayAdvertisingModule {}
