import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignSortButtonComponent } from './campaign-sort-button.component';
import { SortActiveModule } from '@features/campaign-management/pipes';

@NgModule({
  declarations: [CampaignSortButtonComponent],
  imports: [CommonModule, SortActiveModule],
  exports: [CampaignSortButtonComponent],
})
export class CampaignSortButtonModule {}
