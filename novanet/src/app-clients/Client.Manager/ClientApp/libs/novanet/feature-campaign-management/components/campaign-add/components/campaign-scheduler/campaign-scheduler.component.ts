import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ICampaignScheduling } from '@features/campaign-management/store/campaign';
import { dayOfWeeks, hours } from './constants';
import { EDayOfWeek } from '@shared/enums/day-of-week.enum';

@Component({
  selector: 'novanet-campaign-scheduler',
  templateUrl: './campaign-scheduler.component.html',
})
export class CampaignSchedulerComponent {
  @Input() schedulingItems: Partial<ICampaignScheduling>[];
  @Output() schedulingItemsChange = new EventEmitter<
    Partial<ICampaignScheduling>[]
  >();

  public readonly hours = hours;
  public readonly dayOfWeeks = dayOfWeeks;
  public readonly dayOfWeeksEnum = EDayOfWeek;

  public formatHour(i: number) {
    return `${String(i).padStart(2, '0')}:00`;
  }

  public isAllWeekSelected(hour: number): boolean {
    const items = this.schedulingItems.filter(
      (item) => item.timing.getHours() === hour
    );
    return items.length === this.dayOfWeeks.length;
  }

  public isAllDaySelected(dayOfWeek: number): boolean {
    const items = this.schedulingItems.filter(
      (item) => item.dayOfWeek === dayOfWeek
    );
    return items.length === this.hours.length;
  }

  public onSchedulingsChange(dayOfWeek: number, hour: number) {
    const currentSchedulingIndex = this.schedulingItems.findIndex(
      (item) => item.dayOfWeek === dayOfWeek && item.timing.getHours() === hour
    );
    let updatedSchedulings: Partial<ICampaignScheduling>[];
    if (currentSchedulingIndex === -1) {
      updatedSchedulings = [
        ...this.schedulingItems,
        {
          timing: new Date(0, 0, 0, hour, 0, 0),
          dayOfWeek,
        },
      ];
    } else {
      updatedSchedulings = this.schedulingItems.filter(
        (_, i) => i !== currentSchedulingIndex
      );
    }
    this.schedulingItemsChange.emit(updatedSchedulings);
  }

  public onSchedulingsAllDayChange(dayOfWeek: number) {
    let updatedSchedulings = this.schedulingItems.filter(
      (item) => item.dayOfWeek !== dayOfWeek
    );
    if (this.isAllDaySelected(dayOfWeek)) {
      this.schedulingItemsChange.emit(updatedSchedulings);
      return;
    }
    updatedSchedulings = [
      ...updatedSchedulings,
      ...this.hours.map(
        (hour): Partial<ICampaignScheduling> => ({
          dayOfWeek,
          timing: new Date(0, 0, 0, hour, 0, 0),
        })
      ),
    ];
    this.schedulingItemsChange.emit(updatedSchedulings);
  }

  public onSchedulingsAllWeekChange(hour: number) {
    let updatedSchedulings = this.schedulingItems.filter(
      (item) => item.timing.getHours() !== hour
    );
    if (this.isAllWeekSelected(hour)) {
      this.schedulingItemsChange.emit(updatedSchedulings);
      return;
    }
    updatedSchedulings = [
      ...updatedSchedulings,
      ...this.dayOfWeeks.map(
        (dayOfWeek): Partial<ICampaignScheduling> => ({
          dayOfWeek: dayOfWeek.value,
          timing: new Date(0, 0, 0, hour, 0, 0),
        })
      ),
    ];
    this.schedulingItemsChange.emit(updatedSchedulings);
  }

  public onSchedulingsAllChange() {
    if (
      this.schedulingItems.length ===
      this.hours.length * this.dayOfWeeks.length
    ) {
      this.schedulingItemsChange.emit([]);
      return;
    }
    const updatedSchedulings: Partial<ICampaignScheduling>[] = [];
    this.dayOfWeeks.forEach((dayOfWeek) => {
      this.hours.forEach((hour) => {
        updatedSchedulings.push({
          dayOfWeek: dayOfWeek.value,
          timing: new Date(0, 0, 0, hour, 0, 0),
        });
      });
    });
    this.schedulingItemsChange.emit(updatedSchedulings);
  }
}
