import { NgModule } from '@angular/core';
import { AdsetAddAdTargetComponent } from './adset-add-ad-target.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { GroupLocationPipe, SelectedByRegionPipe } from './pipes';

@NgModule({
  declarations: [
    AdsetAddAdTargetComponent,
    GroupLocationPipe,
    SelectedByRegionPipe,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NzCollapseModule],
  exports: [AdsetAddAdTargetComponent, GroupLocationPipe, SelectedByRegionPipe],
})
export class AdsetAddAdTargetModule {}
