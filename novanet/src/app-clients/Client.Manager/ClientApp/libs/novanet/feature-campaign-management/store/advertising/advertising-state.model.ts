import { IPredicateModel } from '@core/models';
import {
  CampaignStatusEnum,
  CampaignType,
} from '@features/campaign-management/store/campaign';
import { ETemplateType } from '@features/campaign-management/enums';

export interface IAdvertisingListRequest {
  pageSize: number;
  page: number;
  filters?: IPredicateModel[];
  status?: CampaignStatusEnum;
  startDate?: Date;
  endDate?: Date;
  withSummary?: boolean;
  withReport?: boolean;
  campaignIds?: string[];
  campaignType?: CampaignType;
  advertisingSetIds?: string[];
  sorts?: string;
  fields?: number[];
}

export interface IAdvertisingResponse {
  id: string;
  name: string;
  isActive: boolean;
  productFeedId?: string;
  advertisingSetId: string;
  campaignId: string;
  advertisingSetName: string;
  campaignName: string;
  redirectLink: string;
  templates: IAdvertisingAppliedTemplate[];
  status: CampaignStatusEnum;
  data: {
    totalCost: number;
    impressions: number;
    clicks: number;
    bannerClicks: number;
    bannerClickRate: number;
    ctr: number;
    cpc: number;
    purchase: number;
    purchaseConversion: number;
    cps: number;
    videoView3s: number;
    video25Percent: number;
    video50Percent: number;
    video75Percent: number;
    video100Percent: number;
  };
  campaignType: CampaignType;
}

export interface IAdvertisingAppliedTemplate {
  templateId: string;
  templateName: string;
  templateType: ETemplateType;
  width: number;
  height: number;
  configurations: { [key: string]: any };
}

export interface IAdvertisingSummary {
  total: number;
  totalCost: number;
  impressions: number;
  clicks: number;
  ctr: number;
  cpc: number;
  purchase: number;
  purchaseConversion: number;
  cps: number;
  videoView3s: number;
  video25Percent: number;
  video50Percent: number;
  video75Percent: number;
  video100Percent: number;
}

export interface IAdvertisingListResponse {
  data: IAdvertisingResponse[];
  summary: IAdvertisingSummary;
}

export interface IAdvertisingCreateRequest {
  name: string;
  advertisingSetId: string;
  redirectLink: string;
  templateConfigurations: ITemplateConfiguration[];
}

export interface ITemplateConfiguration {
  templateId?: string;
  products?: number;
  templateName?: string;
  templateType: ETemplateType;
  isActive: boolean;
  configurations: { [key: string]: any };
}

export interface ITemplateDisplayConfiguration {
  isValid: boolean;
  message: string;
  configurations: { [key: string]: any };
}

export interface IAdvertisingUpdateRequest {
  id: string;
  name?: string;
  isActive?: boolean;
  redirectUrl?: string;
  postContent?: string;
  bannerImage?: string;
  backgroundImageUrl?: string;
  avatarName?: string;
  avatarUrl?: string;
  showFreeDelivery?: boolean;
  showDiscountedPrice?: boolean;
  productName?: boolean;
  useBannerTemplate?: boolean;
  isFullUpdate?: boolean;
  expandedContentUrl?: string;
  cta?: string;
  ctaUrl?: string;
  extendBanner?: string;
  extendNotification?: string;
  isCta?: boolean;
  isCustomImage?: boolean;
  logo?: string;
  shortcutNotification?: string;
  title?: string;
  collapseBanner?: string;
  description?: string;
  images?: string;
  delay?: number;
  isDelay?: boolean;
}

export interface IAdvertisingTemplate {
  id: string;
  name: string;
  width: number;
  height: number;
  templateType: ETemplateType;
  templatePosition: string;
  campaignType: CampaignType;
}

export interface IAdvertisingStateModel {
  advertisings: IAdvertisingResponse[];
  advertisingSummary?: IAdvertisingSummary;
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  selectedAdvertisingIds: string[];
  loadingTemplate: boolean;
  advertisingTemplates: IAdvertisingTemplate[];
}
