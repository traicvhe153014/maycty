import { NgModule } from '@angular/core';
import { CampaignManagementComponent } from './campaign-management.component';
import { CommonModule } from '@angular/common';
import { CampaignManagementRouting } from './campaign-management.routing';
import { RouterModule } from '@angular/router';
import {
  AdListModule,
  AdsetListModule,
  CampaignListModule,
} from '@features/campaign-management/components';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CampaignManagementComponent],
  imports: [
    CommonModule,
    CampaignManagementRouting,
    RouterModule,
    CampaignListModule,
    NzTabsModule,
    AdsetListModule,
    AdListModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class CampaignManagementModule {}
