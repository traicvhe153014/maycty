import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { shortDateFormat } from '@features/campaign-management/constants';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { IFormFieldErrors, IFormFields } from '@models';
import {
  CampaignType,
  ICampaignGetResponse,
  ICampaignScheduling,
} from '@features/campaign-management/store/campaign';
import { Select, Store } from '@ngxs/store';
import {
  GetMoreProductFeedList,
  GetProductFeedList,
  IProductFeedResponse,
  ProductFeedPermission,
  ProductFeedState,
} from '@features/product-feed/store';
import { Observable, takeWhile } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import * as dayjs from 'dayjs';
import { DatePickerComponent } from '@shared/custom-input/components/_index';

@Component({
  selector: 'novanet-campaign-add-ecommerce',
  templateUrl: './campaign-add-ecommerce.component.html',
  styleUrls: ['./campaign-add-ecommerce.component.scss'],
})
export class CampaignAddEcommerceComponent implements OnInit {
  @Select(ProductFeedState.getList) productFeeds$: Observable<
    IProductFeedResponse[]
  >;
  @Select(ProductFeedState.getLoading) productFeedLoading$: Observable<boolean>;
  @Select(ProductFeedState.getPage) productFeedPage$: Observable<number>;
  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;
  @Input() schedulingItems: Partial<ICampaignScheduling>[];
  @Input() formType: string; // add | edit
  @Input() currentCampaign: ICampaignGetResponse | undefined;
  @ViewChild('datepickerRef', { static: false })
  datepickerRef: DatePickerComponent;

  @Output() schedulingItemsChange = new EventEmitter<
    Partial<ICampaignScheduling>[]
  >();

  public form: FormGroup;
  public isConfirmScheduledVisible = false;
  public ecommerceScheduled: boolean;
  public ecommerceEndDatePlaceholder = 'Chọn ngày kết thúc';

  public readonly shortDateFormat = shortDateFormat;
  public readonly campaignType = CampaignType;
  private readonly pageSize = 24;

  constructor(
    private rootFormGroup: FormGroupDirective,
    private store: Store,
    private changeDetectionRef: ChangeDetectorRef
  ) {
    this.form = this.rootFormGroup.control;
    this.ecommerceScheduled = this.currentCampaign?.ecommerceScheduled ?? true;
  }

  public get isStartDateDisabled() {
    return (
      this.formType === 'edit' &&
      dayjs(this.currentCampaign?.ecommerceStartDate).isBefore(
        dayjs().hour(23).minute(59).second(59)
      )
    );
  }

  public get productFeedOptions$(): Observable<NzSelectOptionInterface[]> {
    return this.productFeeds$.pipe(
      map((productFeeds): NzSelectOptionInterface[] =>
        productFeeds.map((productFeed) => ({
          label: productFeed.name,
          value: productFeed.id,
        }))
      )
    );
  }

  ngOnInit() {
    if (this.formType === 'add') {
      this.ecommerceScheduled =
        this.form.get('ecommerceScheduled').value ?? true;
    } else {
      this.form.valueChanges
        .pipe(takeWhile((formValue) => formValue?.name !== null))
        .subscribe({
          next: (formValue) => {
            this.ecommerceScheduled = formValue?.ecommerceScheduled ?? true;
          },
        });
      if (!this.currentCampaign?.ecommerceEndDate) {
        this.ecommerceEndDatePlaceholder = 'Không có ngày kết thúc';
      }
    }

    this.store
      .dispatch(
        new GetProductFeedList({
          page: 1,
          pageSize: this.pageSize,
          keyword: '',
          sorts: ['-modifiedAt'],
          permission: ProductFeedPermission.Granted,
          withAvailableProducts: true,
        })
      )
      .subscribe({
        next: () => {
          this.changeDetectionRef.detectChanges();
        },
      });
  }

  public loadMoreProductFeeds() {
    this.productFeedPage$.pipe(take(1)).subscribe({
      next: (page) => {
        this.store.dispatch(
          new GetMoreProductFeedList({
            page,
            pageSize: this.pageSize,
            keyword: '',
            sorts: ['-modifiedAt'],
            permission: ProductFeedPermission.Granted,
            withAvailableProducts: true,
          })
        );
      },
    });
  }

  public onSchedulingItemsChange(data: Partial<ICampaignScheduling>[]) {
    this.schedulingItemsChange.emit(data);
  }

  public disabledStartDate(date: Date): boolean {
    return dayjs(date).isBefore(dayjs().add(-1, 'day'));
  }

  public disabledEndDate(date: Date): boolean {
    return dayjs(date).isBefore(dayjs());
  }

  public clearEndDate() {
    this.ecommerceEndDatePlaceholder = 'Không có ngày kết thúc';
    this.form.patchValue({ ecommerceEndDate: null });
    this.datepickerRef.singleDatepickerRef.close();
  }

  public handleCancelScheduled() {
    this.isConfirmScheduledVisible = false;
  }

  public confirmUpdateScheduled() {
    this.isConfirmScheduledVisible = false;
    this.ecommerceScheduled = !this.ecommerceScheduled;
    this.form.patchValue({
      ecommerceScheduled: this.ecommerceScheduled,
    });
  }

  public onUpdateScheduled() {
    if (this.formType === 'add') {
      this.ecommerceScheduled = !this.ecommerceScheduled;
      this.form.patchValue({
        ecommerceScheduled: this.ecommerceScheduled,
      });
      return;
    }
    this.isConfirmScheduledVisible = true;
  }
}
