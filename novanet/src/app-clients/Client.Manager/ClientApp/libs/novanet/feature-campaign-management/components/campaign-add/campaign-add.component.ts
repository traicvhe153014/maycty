import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CampaignType,
  CreateCampaign,
  GetCampaign,
  ICampaignCreateRequest,
  ICampaignGetResponse,
  ICampaignScheduling,
  ICampaignStateModel,
  ICampaignUpdateRequest,
  UpdateCampaign,
} from '@features/campaign-management/store/campaign';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  campaignCreateFormFields,
  createFormFieldErrors,
} from '@features/campaign-management/constants';
import {
  IProductFeedResponse,
  ProductFeedState,
} from '@features/product-feed/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { convertPrice } from '@core/utils';

@Component({
  selector: 'novanet-campaign-add',
  templateUrl: './campaign-add.component.html',
  styleUrls: ['./campaign-add.component.scss'],
})
export class CampaignAddComponent implements OnInit, OnDestroy {
  public formType: string; // add | edit
  public createForm: FormGroup;
  public readonly createFormFields = campaignCreateFormFields;
  public currentCampaignType = CampaignType.Ecommerce;
  public schedulingItems: Partial<ICampaignScheduling>[] = [];

  public readonly createFormFieldErrors = createFormFieldErrors;
  public readonly campaignTypeOption = CampaignType;

  public currentCampaign: ICampaignGetResponse | undefined;
  public isConfirmSubmitVisible = false;

  @Select(ProductFeedState.getList) productFeeds$: Observable<
    IProductFeedResponse[]
  >;
  private productFeeds: IProductFeedResponse[] = [];

  private readonly destroy$: Subject<void> = new Subject<void>();

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private notification: NzNotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.formType = this.router.url.includes('add-campaign') ? 'add' : 'edit';
    this.buildCreateForm();
    this.createForm.controls.ecommerceAmountSpent.valueChanges.subscribe({
      next: () => {
        const dailyBudgetField = this.createForm.get('ecommerceDailyBudget');
        if (
          !dailyBudgetField.hasError('exceedAmountSpent') &&
          !dailyBudgetField.valid
        ) {
          return;
        }
        const dailyBudget = parseInt(dailyBudgetField.value, 10);
        const amountSpent = parseInt(
          this.createForm.get('ecommerceAmountSpent').value,
          10
        );
        const errors =
          dailyBudget > amountSpent
            ? {
                exceedAmountSpent: true,
              }
            : null;
        this.createForm.controls.ecommerceDailyBudget.setErrors(errors);
      },
    });

    this.createForm.controls.ecommerceDailyBudget.valueChanges.subscribe({
      next: () => {
        if (!this.createForm.get('ecommerceDailyBudget').valid) {
          return;
        }
        const dailyBudget = parseInt(
          this.createForm.get('ecommerceDailyBudget').value,
          10
        );
        const amountSpent = parseInt(
          this.createForm.get('ecommerceAmountSpent').value,
          10
        );
        if (dailyBudget <= amountSpent) {
          return;
        }
        this.createForm.controls.ecommerceDailyBudget.setErrors({
          exceedAmountSpent: true,
        });
      },
    });

    this.createForm.controls.campaignType.valueChanges.subscribe({
      next: (value) => {
        this.currentCampaignType = value;
      },
    });

    if (this.formType === 'edit') {
      const campaignId =
        this.activatedRoute.snapshot.queryParamMap.get('campaignId');
      if (!campaignId) {
        this.router.navigateByUrl('/').then();
        return;
      }
      this.store.dispatch(new GetCampaign(campaignId)).subscribe({
        next: (data) => {
          const campaign = (data.campaign as ICampaignStateModel)
            .detailedCampaign;
          this.createForm.patchValue({
            name: campaign.name,
            campaignType: campaign.campaignType,
            ecommerceAmountSpent: campaign.ecommerceAmountSpent,
            ecommerceDailyBudget: campaign.ecommerceDailyBudget,
            ecommerceProductFeedId: campaign.ecommerceProductFeedId,
            ecommerceStartDate: campaign.ecommerceStartDate,
            ecommerceEndDate: campaign.ecommerceEndDate,
            ecommerceScheduled: campaign.ecommerceScheduled,
            ecommerceScheduledAllTime: campaign.ecommerceScheduledAllTime,
          });
          this.schedulingItems = campaign.campaignScheduling.map((item) => ({
            id: item.id,
            campaignId: item.campaignId,
            timing: new Date(item.timing),
            dayOfWeek: item.dayOfWeek,
          }));
          this.currentCampaign = campaign;
          this.changeDetectorRef.detectChanges();
        },
      });
    }

    this.productFeeds$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (data) => {
        this.productFeeds = data;
      },
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public handleCancelSubmit() {
    this.isConfirmSubmitVisible = false;
  }

  public confirmSubmit() {
    this.submitForm();
    this.isConfirmSubmitVisible = false;
  }

  public onClickSubmit() {
    if (this.formType === 'add') {
      this.submitForm();
      return;
    }
    this.isConfirmSubmitVisible = true;
  }

  public submitForm() {
    if (!this.createForm.valid) {
      this.createForm.markAllAsTouched();
      return;
    }
    if (
      this.createForm.get('ecommerceScheduled').value &&
      !this.createForm.get('ecommerceScheduledAllTime').value &&
      !this.schedulingItems.length
    ) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Bạn cần chọn tối thiểu một khung giờ để tiếp tục'
        )
      );
      return;
    }
    const selectedProductFeed = this.productFeeds.find(
      (item) => item.id === this.createForm.get('ecommerceProductFeedId').value
    );
    if (selectedProductFeed?.activeProduct === 0) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Nguồn dữ liệu phải đã cấp quyền và sản phẩm không bị lỗi'
        )
      );
      return;
    }
    if (this.formType === 'add') {
      const payload: ICampaignCreateRequest = {
        ...this.createForm.value,
        schedulings: this.schedulingItems,
      };
      this.store.dispatch(new CreateCampaign(payload)).subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Tạo chiến dịch thành công'
            )
          );
        },
        error: () => {
          this.store.dispatch(
            new ShowGlobalNotification(GlobalNotificationEnum.error, 'Lỗi')
          );
        },
      });
    } else {
      const payload: Partial<ICampaignUpdateRequest> = {
        ...this.createForm.value,
        id: this.currentCampaign.id,
        schedulings: this.schedulingItems,
        isEcommerceUpdating: true,
      };
      this.store.dispatch(new UpdateCampaign(payload)).subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Cập nhật chiến dịch thành công'
            )
          );
          this.router
            .navigate(['/campaign/management'], {
              queryParams: { level: 'Campaign' },
            })
            .then();
        },
        error: (err) => {
          const errorResponse = err?.error?.errors?.[0];
          let errorMessage = 'Lỗi';
          if (
            errorResponse ===
            'Ngân sách tổng không được chỉnh sửa nhỏ hơn tổng chi phí chạy quảng cáo'
          ) {
            errorMessage = errorResponse;
          }
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              errorMessage
            )
          );
        },
      });
    }
  }

  public onChangeCampaignType(option: number) {
    this.destroy$.next();
    this.destroy$.complete();
    this.currentCampaignType = option;
    this.changeDetectorRef.detectChanges();
    this.createForm.patchValue({
      campaignType: option,
    });
  }

  private buildCreateForm() {
    const config = {
      [this.createFormFields.name.name]: [
        '',
        [
          Validators.maxLength(
            this.createFormFields.name.validationParams.maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.campaignType.name]: [
        CampaignType.Ecommerce,
        [Validators.required],
      ],
      [this.createFormFields.ecommerceAmountSpent.name]: [
        '',
        [
          Validators.required,
          Validators.pattern(
            this.createFormFields.ecommerceAmountSpent.validationParams.pattern
          ),
          Validators.min(
            this.createFormFields.ecommerceAmountSpent.validationParams.min
          ),
          this.validateNumberLength(),
        ],
      ],
      [this.createFormFields.ecommerceDailyBudget.name]: [
        '',
        [
          Validators.required,
          Validators.pattern(
            this.createFormFields.ecommerceDailyBudget.validationParams.pattern
          ),
          Validators.min(
            this.createFormFields.ecommerceDailyBudget.validationParams.min
          ),
          this.validateNumberLength(),
        ],
      ],
      [this.createFormFields.ecommerceProductFeedId.name]: [undefined, []],
      [this.createFormFields.ecommerceStartDate.name]: [
        undefined,
        [Validators.required],
      ],
      [this.createFormFields.ecommerceEndDate.name]: [undefined, []],
      [this.createFormFields.ecommerceScheduled.name]: [true, []],
      [this.createFormFields.ecommerceScheduledAllTime.name]: [true, []],
      [this.createFormFields.schedulings.name]: [[], []],
    };
    this.createForm = this.formBuilder.group(config, {
      validators: this.createFormValidator,
    });
  }

  private validateNumberLength(): ValidatorFn {
    return (control): ValidationErrors | null => {
      const priceNumber = convertPrice(control.value);
      if (priceNumber.toString().length > 12) {
        return { maxlength: true };
      }
      return null;
    };
  }

  private createFormValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    // validate start end date
    const startDate = new Date(
      control.get('ecommerceStartDate').value
    ).getTime();
    const endDate = new Date(control.get('ecommerceEndDate').value).getTime();

    if (!startDate) {
      const errors = !startDate
        ? {
            required: true,
          }
        : null;

      control.get('ecommerceStartDate').setErrors(errors);
    } else {
      const errors =
        startDate >= endDate && endDate
          ? {
              exceedEndDate: true,
            }
          : null;

      control.get('ecommerceStartDate').setErrors(errors);
    }

    // validate productFeedId
    const productFeedId = control.get(
      campaignCreateFormFields.ecommerceProductFeedId.name
    ).value;
    const campaignType = control.get(campaignCreateFormFields.campaignType.name)
      .value as CampaignType;
    let productFeedIdError: { [key: string]: boolean } | null = null;
    if (campaignType === CampaignType.Ecommerce) {
      if (!productFeedId) {
        productFeedIdError = { required: true };
      }
    }
    control
      .get(campaignCreateFormFields.ecommerceProductFeedId.name)
      .setErrors(productFeedIdError);

    return null;
  }
}
