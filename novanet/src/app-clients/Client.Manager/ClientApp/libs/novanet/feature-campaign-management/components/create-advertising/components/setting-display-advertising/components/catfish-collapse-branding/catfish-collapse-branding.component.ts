import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ETemplateType } from '@features/campaign-management/enums';
import {
  ITemplateConfiguration,
  ITemplateDisplayConfiguration,
} from '@features/campaign-management/store';
import {
  AllowSizeColumnPopupBanner,
  AllowSizeSetting,
} from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/constant';
import { ESettingFormatAdvertising } from '@features/campaign-management/components/create-advertising/components/setting-format-advertising/enums';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { isFileSizeValid } from '@core/utils';
import { zipMimeType } from '@features/campaign-management/components/create-advertising/components/setting-format-advertising/constant';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { Store } from '@ngxs/store';
import { cloneDeep } from 'lodash';
import {
  createCatfishCollabBrandingFormFieldErrors,
  createCatFishCollapseFormFields,
} from './catfish-collapse-branding.contants';
import {
  bannerAllowedExtensionPattern,
  logoAllowedExtensionPattern,
} from '@shared/sharing/constants';

interface ImageModel {
  name: string;
  src: string;
  file: File;
}

@Component({
  selector: 'novanet-catfish-collapse-branding',
  templateUrl: './catfish-collapse-branding.component.html',
})
export class CatfishCollapseBrandingComponent implements OnInit {
  @Input() public height = 0;
  @Input() public width = 0;
  @Input() public dataTable = [];
  @Input() public templateType: ETemplateType;
  @Input() public templateConfiguration: ITemplateConfiguration;
  @Output() public templateConfigurationChange =
    new EventEmitter<ITemplateDisplayConfiguration>();

  public templateTypes = ETemplateType;
  public settingColumn = AllowSizeColumnPopupBanner;
  public settingTable = AllowSizeSetting;
  public allowSizeColumn = ESettingFormatAdvertising;
  public notificationBannerForm: FormGroup;
  public currentImage: ImageModel[] = [];
  public currentAvatar: ImageModel[] = [];
  public currentCollapseBanner: ImageModel[] = [];
  public currentExtendBanner: ImageModel[] = [];

  public readonly createFormFields = createCatFishCollapseFormFields;
  public readonly createFormFieldErrors =
    createCatfishCollabBrandingFormFieldErrors;

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get logo(): FormControl {
    return this.notificationBannerForm.get('logo') as FormControl;
  }

  get collapseBanner(): FormControl {
    return this.notificationBannerForm.get('collapseBanner') as FormControl;
  }

  get extendBanner(): FormControl {
    return this.notificationBannerForm.get('extendBanner') as FormControl;
  }

  get title(): string {
    return this.notificationBannerForm.get('title').value;
  }

  get description(): string {
    return this.notificationBannerForm.get('description').value;
  }

  get isCta(): boolean {
    return this.notificationBannerForm.get('isCta').value;
  }

  get cta(): boolean {
    return this.notificationBannerForm.get('cta').value;
  }

  get logoLink(): FormControl {
    return this.notificationBannerForm.get('logoLink') as FormControl;
  }

  get extendBannerLink(): FormControl {
    return this.notificationBannerForm.get('extendBannerLink') as FormControl;
  }

  get collapseBannerLink(): FormControl {
    return this.notificationBannerForm.get('collapseBannerLink') as FormControl;
  }

  public ngOnInit() {
    this.buildNotificationBannerForm();
  }

  public handleImageInput(field: string, event: Event) {
    const files = (event.target as HTMLInputElement).files;
    const fileList = [] as File[];
    // eslint-disable-next-line @typescript-eslint/prefer-for-of
    for (let i = 0; i < files.length; i++) {
      fileList.push(files.item(i));
    }
    let maxSize = 5;
    if (field === 'logo') {
      maxSize = 1;
    }
    if (isFileSizeValid(fileList, 1024000 * maxSize)) {
      const allowedFileType =
        field === 'logo'
          ? logoAllowedExtensionPattern
          : bannerAllowedExtensionPattern;
      if (!allowedFileType.test(fileList[0].name)) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.warning,
            'Cảnh báo',
            'Sai định dạng ảnh'
          )
        );
        (event.target as HTMLInputElement).value = '';
        return;
      }
      const fr = new FileReader();
      fr.onload = () => {
        if (
          files.item(0)?.type === zipMimeType &&
          field === 'expandedContent'
        ) {
          this.templateConfiguration.configurations.images = null;
          return;
        }
        const fileType = files.item(0)?.type;
        if (
          (fileType === 'text/html' || fileType === zipMimeType) &&
          field === 'collapseBanner'
        ) {
          this.currentCollapseBanner.push({
            name: fileList[0].name,
            src: fileList[0].name,
            file: fileList[0],
          });
          this.notificationBannerForm.patchValue({
            collapseBannerLink: files[0]
              ? window.URL.createObjectURL(files[0])
              : '',
            catfishCollapseBannerFileType:
              fileType === zipMimeType ? 'zip' : 'html',
          });
          this.collapseBanner.patchValue(
            cloneDeep(this.currentCollapseBanner[0])
          );
          (event.target as HTMLInputElement).value = '';
          return;
        }
        if (
          (fileType === 'text/html' || fileType === zipMimeType) &&
          field === 'extendBanner'
        ) {
          this.currentExtendBanner.push({
            name: fileList[0].name,
            src: fileList[0].name,
            file: fileList[0],
          });
          this.notificationBannerForm.patchValue({
            extendBannerLink: files[0]
              ? window.URL.createObjectURL(files[0])
              : '',
            catfishExtendBannerFileType:
              fileType === zipMimeType ? 'zip' : 'html',
          });
          this.extendBanner.patchValue(cloneDeep(this.currentExtendBanner[0]));
          (event.target as HTMLInputElement).value = '';
          return;
        }
        const img = new Image();
        img.onload = () => {
          switch (field) {
            case 'logo':
              this.currentAvatar.push({
                name: fileList[0].name,
                src: img.src,
                file: fileList[0],
              });
              this.notificationBannerForm.patchValue({
                logoLink: files[0] ? window.URL.createObjectURL(files[0]) : '',
              });
              this.logo.patchValue(cloneDeep(this.currentAvatar[0]));
              (event.target as HTMLInputElement).value = '';
              break;
            case 'collapseBanner':
              if (img.width / img.height !== 375 / 120) {
                this.store.dispatch(
                  new ShowGlobalNotification(
                    GlobalNotificationEnum.warning,
                    'Cảnh báo',
                    'Sai kích thước ảnh'
                  )
                );
                return;
              }
              this.currentCollapseBanner.push({
                name: fileList[0].name,
                src: img.src,
                file: fileList[0],
              });
              this.notificationBannerForm.patchValue({
                collapseBannerLink: files[0]
                  ? window.URL.createObjectURL(files[0])
                  : '',
                catfishCollapseBannerFileType: 'image',
              });
              this.collapseBanner.patchValue(
                cloneDeep(this.currentCollapseBanner[0])
              );
              (event.target as HTMLInputElement).value = '';
              break;
            case 'extendBanner':
              if (img.width / img.height !== 375 / 810) {
                this.store.dispatch(
                  new ShowGlobalNotification(
                    GlobalNotificationEnum.warning,
                    'Cảnh báo',
                    'Sai kích thước ảnh'
                  )
                );
                (event.target as HTMLInputElement).value = '';
                return;
              }
              this.currentExtendBanner.push({
                name: fileList[0].name,
                src: img.src,
                file: fileList[0],
              });
              this.notificationBannerForm.patchValue({
                extendBannerLink: files[0]
                  ? window.URL.createObjectURL(files[0])
                  : '',
                catfishExtendBannerFileType: 'image',
              });
              this.extendBanner.patchValue(
                cloneDeep(this.currentExtendBanner[0])
              );
              (event.target as HTMLInputElement).value = '';
              break;
          }
        };
        img.src = fr.result as any;
        this.changeDetectorRef.detectChanges();
      };
      fr.readAsDataURL(fileList[0]);
    } else {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên dữ liệu quá ' + maxSize + 'MB'
        )
      );
      (event.target as HTMLInputElement).value = '';
    }
  }

  public clearAvatar(field: string) {
    switch (field) {
      case 'logo':
        this.currentAvatar = [];
        this.logo.reset();
        this.logoLink.reset();
        break;
      case 'collapseBanner':
        this.currentCollapseBanner = [];
        this.collapseBanner.patchValue(null);
        this.collapseBannerLink.reset();
        this.notificationBannerForm.patchValue({
          catfishCollapseBannerFileType: 'image',
        });
        break;
      case 'extendBanner':
        this.currentExtendBanner = [];
        this.extendBanner.patchValue(null);
        this.extendBannerLink.reset();
        this.notificationBannerForm.patchValue({
          catfishExtendBannerFileType: 'image',
        });
        break;
    }
  }

  private catfishFormValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    // validate cta
    const isCta = control.get(createCatFishCollapseFormFields.isCta.name)
      .value as boolean;
    const cta = control.get(createCatFishCollapseFormFields.cta.name)
      .value as string;
    let ctaError: { [key: string]: boolean } | null = null;

    if (isCta) {
      if (!cta) {
        ctaError = { required: true };
      } else if (
        cta.length >
        createCatFishCollapseFormFields.cta.validationParams.maxLength
      ) {
        ctaError = { maxlength: true };
      }
    }
    control.get(createCatFishCollapseFormFields.cta.name).setErrors(ctaError);
    return null;
  }

  private buildNotificationBannerForm() {
    const config = {
      [this.createFormFields.title.name]: [
        this.templateConfiguration.configurations.title ?? '',
        [
          Validators.maxLength(
            this.createFormFields.title.validationParams.maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.logo.name]: [
        this.templateConfiguration.configurations.logo ?? '',
        [],
      ],
      [this.createFormFields.collapseBanner.name]: [
        this.templateConfiguration.configurations.collapseBanner ?? '',
        [],
      ],
      [this.createFormFields.extendBanner.name]: [
        this.templateConfiguration.configurations.extendBanner ?? '',
        [],
      ],
      [this.createFormFields.description.name]: [
        this.templateConfiguration.configurations.description ?? '',
        [
          Validators.maxLength(
            this.createFormFields.description.validationParams
              .maxLength as number
          ),
          Validators.required,
        ],
      ],
      [this.createFormFields.cta.name]: [
        this.templateConfiguration.configurations.cta ?? '',
        [
          Validators.maxLength(
            this.createFormFields.cta.validationParams.maxLength as number
          ),
        ],
      ],
      [this.createFormFields.ctaUrl.name]: [
        this.templateConfiguration.configurations.ctaUrl ?? '',
        [
          Validators.pattern(
            this.createFormFields.ctaUrl.validationParams.pattern
          ),
          Validators.maxLength(
            this.createFormFields.ctaUrl.validationParams.maxLength as number
          ),
        ],
      ],
      [this.createFormFields.isCta.name]: [
        this.templateConfiguration.configurations.isCta ?? true,
        [Validators.required],
      ],
      ['collapseBannerLink']: [''],
      ['extendBannerLink']: [''],
      ['logoLink']: [''],
      ['catfishCollapseBannerFileType']: [
        this.getFileType(
          this.templateConfiguration.configurations.collapseBanner
        ),
      ],
      ['catfishExtendBannerFileType']: [
        this.getFileType(
          this.templateConfiguration.configurations.extendBanner
        ),
      ],
    };

    this.notificationBannerForm = this.formBuilder.group(config, {
      validators: this.catfishFormValidator,
    });

    this.notificationBannerForm.valueChanges.subscribe((value) => {
      if (!this.collapseBanner.value) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống banner thu gọn',
          configurations: value,
        });
        return;
      }

      if (!this.extendBanner.value) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống banner mở rộng',
          configurations: value,
        });
        return;
      }

      if (!this.title) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống tiêu đề',
          configurations: value,
        });
        return;
      }

      if (!this.description) {
        this.templateConfigurationChange.emit({
          isValid: false,
          message: 'Không để trống nội dung',
          configurations: value,
        });
        return;
      }

      if (this.isCta) {
        if (!this.cta) {
          this.templateConfigurationChange.emit({
            isValid: false,
            message: 'Không để trống CTA',
            configurations: value,
          });
          return;
        }
      }

      this.templateConfigurationChange.emit({
        isValid: this.notificationBannerForm.valid,
        message: !this.notificationBannerForm.valid
          ? 'Vui lòng điền trường dữ liệu bắt buộc'
          : '',
        configurations: value,
      });
    });
  }

  private getFileType(fileName: string | undefined) {
    return fileName?.includes('.html')
      ? 'html'
      : fileName?.includes('.zip')
      ? 'zip'
      : 'image';
  }
}
