import { IBannerDetail } from '../models';

export const MobileBannerCard = {
  type: 'Banner ngang',
  containerSize: '375x180px',
  safeArea: '110x180px',
  fileSize: '1 MB',
};

export const InteractiveBanner = {
  type: 'Banner dọc',
  containerSize: '375x810px',
  safeArea: '375x255px',
  fileSize: '1 MB',
} as IBannerDetail;

export const FlyingCarpetBanner = {
  type: 'Banner dọc',
  containerSize: '375x810px',
  safeArea: '375x200px',
  fileSize: '1 MB',
} as IBannerDetail;

export const InReadEcommerceBanner = {
  type: 'Banner dọc',
  containerSize: '375x600px',
  safeArea: '375x150px',
  fileSize: '1 MB',
} as IBannerDetail;

export const CatfishEcomBanner: IBannerDetail[] = [
  {
    type: 'Banner chân trang',
    containerSize: '375x120px',
    safeArea: '375x20px',
    fileSize: '1 MB',
  },
  {
    type: 'Banner mở rộng',
    containerSize: '375x810px',
    safeArea: '375x600px',
    fileSize: '1 MB',
  },
];

export const zipMimeType = 'application/x-zip-compressed';
