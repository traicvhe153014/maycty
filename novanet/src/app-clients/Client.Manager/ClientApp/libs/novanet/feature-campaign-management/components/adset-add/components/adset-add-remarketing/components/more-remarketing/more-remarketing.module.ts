import { NgModule } from '@angular/core';
import { MoreRemarketingComponent } from './more-remarketing.component';
import { DataTableModule } from '@core';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { ReactiveFormsModule } from '@angular/forms';

const MODULES = [DataTableModule, CommonModule, NovanetInputModule];

const COMPONENTS = [MoreRemarketingComponent];

@NgModule({
  exports: [...COMPONENTS],
  imports: [...MODULES, ReactiveFormsModule],
  declarations: [...COMPONENTS],
})
export class MoreRemarketingModule {}
