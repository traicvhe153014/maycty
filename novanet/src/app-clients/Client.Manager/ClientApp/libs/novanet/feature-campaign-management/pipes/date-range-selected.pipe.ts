import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateRangeSelected',
})
export class DateRangeSelectedPipe implements PipeTransform {
  transform(value: Date[]): boolean {
    return !!value && value.length !== 0 && value.every((item) => !!item);
  }
}
