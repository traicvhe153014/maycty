import { CampaignService } from './campaign-state.service';
import { Injectable } from '@angular/core';
import {
  ClearCampaign,
  CreateCampaign,
  CreateCampaignError,
  CreateCampaignSuccess,
  GetCampaign,
  GetCampaignError,
  GetCampaignList,
  GetCampaignListError,
  GetCampaignListSuccess,
  GetCampaignSuccess,
  GetMoreCampaignList,
  GetMoreCampaignListError,
  GetMoreCampaignListSuccess,
  NavigateCampaignNextLevel,
  SelectCampaign,
  UpdateCampaign,
  UpdateCampaignError,
  UpdateCampaignSuccess,
} from './campaign-state.actions';
import { Observable, throwError } from 'rxjs';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import {
  ICampaignResponse,
  ICampaignStateModel,
  ICampaignSummary,
} from './campaign-state.model';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  AdsetState,
  AdvertisingState,
  SelectAdset,
  SelectAdvertising,
} from '@features/campaign-management/store';
import { session } from '@core';

@Injectable()
@State<ICampaignStateModel>({
  name: 'campaign',
  defaults: {
    loading: false,
    page: 1,
    campaigns: [],
    campaignSummary: undefined,
    hasMorePages: true,
    detailedCampaign: undefined,
    selectedCampaignIds: [],
  },
})
export class CampaignState {
  constructor(
    private campaignService: CampaignService,
    private router: Router,
    private store: Store
  ) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(state: ICampaignStateModel): ICampaignResponse[] {
    return state.campaigns;
  }

  @Selector()
  public static getLoading(state: ICampaignStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getPage(state: ICampaignStateModel): number {
    return state.page;
  }

  @Selector()
  public static getSummary(state: ICampaignStateModel): ICampaignSummary {
    return state.campaignSummary;
  }

  @Selector()
  public static getSelected(state: ICampaignStateModel): string[] {
    return state.selectedCampaignIds;
  }

  @Selector()
  public static getNavigatedCampaignId(
    state: ICampaignStateModel
  ): string | undefined {
    return state.navigatedCampaignId;
  }

  /**
   * Actions
   */
  @Action(GetCampaignList)
  public getList(
    ctx: StateContext<ICampaignStateModel>,
    { params }: GetCampaignList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.campaignService
      .getList({
        ...params,
        withSummary: params.withSummary ?? true,
        withReport: params.withReport ?? true,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetCampaignListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetCampaignListError(err)))
      );
  }

  @Action(GetCampaignListSuccess)
  public getListSuccess(
    ctx: StateContext<ICampaignStateModel>,
    { response, params }: GetCampaignListSuccess
  ) {
    const newPage =
      response.data.length === 0 && params.page !== 1
        ? params.page - 1
        : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    // ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      campaigns: response.data,
      campaignSummary: response.summary,
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetCampaignListError)
  public getListError(
    ctx: StateContext<ICampaignStateModel>,
    { error }: GetCampaignListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetMoreCampaignList)
  public getMoreList(
    ctx: StateContext<ICampaignStateModel>,
    { params }: GetMoreCampaignList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.campaignService
      .getList({
        ...params,
        withSummary: false,
        withReport: params.withReport ?? true,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetMoreCampaignListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetMoreCampaignListError(err)))
      );
  }

  @Action(GetMoreCampaignListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<ICampaignStateModel>,
    { response, params }: GetMoreCampaignListSuccess
  ) {
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    const currentCampaigns = ctx.getState().campaigns;
    ctx.patchState({
      campaigns: [...currentCampaigns, ...response.data],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreCampaignListError)
  public getMoreListError(
    ctx: StateContext<ICampaignStateModel>,
    { error }: GetMoreCampaignListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetCampaign)
  public get(
    ctx: StateContext<ICampaignStateModel>,
    { campaignId }: GetCampaign
  ) {
    ctx.patchState({ loading: true });
    return this.campaignService.get(campaignId).pipe(
      map((response) => ctx.dispatch(new GetCampaignSuccess(response))),
      catchError((err) => ctx.dispatch(new GetCampaignError(err)))
    );
  }

  @Action(GetCampaignSuccess)
  public getSuccess(
    ctx: StateContext<ICampaignStateModel>,
    { campaign }: GetCampaignSuccess
  ) {
    ctx.patchState({
      detailedCampaign: campaign,
      loading: false,
    });
    return campaign;
  }

  @Action(GetCampaignError)
  public getError(
    ctx: StateContext<ICampaignStateModel>,
    { error }: GetCampaignError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(CreateCampaign)
  public create(
    ctx: StateContext<ICampaignStateModel>,
    { payload }: CreateCampaign
  ) {
    return this.campaignService.create(payload).pipe(
      map((response) => ctx.dispatch(new CreateCampaignSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateCampaignError(error)))
    );
  }

  @Action(CreateCampaignSuccess)
  public createSuccess(
    ctx: StateContext<ICampaignStateModel>,
    { campaign }: CreateCampaignSuccess
  ) {
    this.router
      .navigate(['/campaign/add-adset'], {
        queryParams: { campaignId: campaign.id },
      })
      .then();
    return campaign;
  }

  @Action(CreateCampaignError)
  public createError(
    ctx: StateContext<ICampaignStateModel>,
    { error }: CreateCampaignError
  ) {
    return throwError(error);
  }

  @Action(UpdateCampaign)
  public update(
    ctx: StateContext<ICampaignStateModel>,
    { payload }: UpdateCampaign
  ) {
    return this.campaignService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateCampaignSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateCampaignError(error)))
    );
  }

  @Action(UpdateCampaignSuccess)
  public updateSuccess(
    ctx: StateContext<ICampaignStateModel>,
    { campaign }: UpdateCampaignSuccess
  ) {
    const { campaigns } = ctx.getState();
    const updatedCampaigns = campaigns.map((item) =>
      item.id !== campaign.id
        ? item
        : {
            ...item,
            name: campaign.name,
            isActive: campaign.isActive,
            status: campaign.status,
          }
    );
    ctx.patchState({ campaigns: updatedCampaigns });
  }

  @Action(UpdateCampaignError)
  public updateError(
    ctx: StateContext<ICampaignStateModel>,
    { error }: UpdateCampaignError
  ) {
    return throwError(error);
  }

  @Action(NavigateCampaignNextLevel)
  public navigateCampaignNextLevel(
    ctx: StateContext<ICampaignStateModel>,
    { campaignId }: NavigateCampaignNextLevel
  ) {
    ctx.patchState({
      navigatedCampaignId: campaignId,
      selectedCampaignIds: [campaignId],
    });
    session.set('tableCampaignSelected', [campaignId]);
    ctx.patchState({
      navigatedCampaignId: undefined,
    });
  }

  @Action(SelectCampaign)
  public select(
    ctx: StateContext<ICampaignStateModel>,
    { campaignIds, currentLevelOnly }: SelectCampaign
  ) {
    ctx.patchState({
      selectedCampaignIds: campaignIds,
    });
    if (currentLevelOnly) {
      return;
    }
    // update selected ad set
    const selectedAdsets =
      this.store.selectSnapshot(AdsetState.getSelected) ?? [];
    const adsets = this.store.selectSnapshot(AdsetState.getList) ?? [];
    const updatedAdsetIds = selectedAdsets.filter((adsetId) => {
      const adset = adsets.find((item) => item.id === adsetId);
      return campaignIds.find((campaignId) => campaignId === adset?.campaignId);
    });
    session.set('tableAdsetSelected', updatedAdsetIds);
    this.store.dispatch(new SelectAdset(updatedAdsetIds));

    // update selected ad
    const selectedAds =
      this.store.selectSnapshot(AdvertisingState.getSelected) ?? [];
    const ads = this.store.selectSnapshot(AdvertisingState.getList) ?? [];
    const updatedAdIds = selectedAds.filter((adsetId) => {
      const ad = ads.find((item) => item.id === adsetId);
      return updatedAdsetIds.find(
        (adsetId) => adsetId === ad?.advertisingSetId
      );
    });
    session.set('tableAdSelected', updatedAdIds);
    this.store.dispatch(new SelectAdvertising(updatedAdIds));
  }

  @Action(ClearCampaign)
  public clear(ctx: StateContext<ICampaignStateModel>) {
    ctx.patchState({
      campaigns: [],
      campaignSummary: undefined,
      selectedCampaignIds: [],
      detailedCampaign: undefined,
      hasMorePages: false,
      page: 1,
      loading: false,
      navigatedCampaignId: undefined,
    });
  }
}
