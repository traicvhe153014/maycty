import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';
import {
  CreateAdvertisingFormFieldErrors,
  CreateAdvertisingFormFields,
  settingDisplayAdsPanels,
  settingPanels,
  TemplatesPreview,
} from './constants';
import { cloneDeep, uniq } from 'lodash';
import {
  AllowSizeSetting,
  zipMimeType,
} from './components/setting-format-advertising/constant';
import { IFormFields } from '@models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AdsetService,
  AdvertisingService,
  CampaignType,
  CreateAdvertising,
  IAdsetByIdResponse,
  IAdvertisingCreateRequest,
  IAdvertisingResponse,
  IAdvertisingUpdateRequest,
  ITemplateConfiguration,
  ITemplateDisplayConfiguration,
  UpdateAdvertising,
} from '@features/campaign-management/store';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { ETemplateType } from '@features/campaign-management/enums';
import { StorageService } from '@core/services';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { DOCUMENT } from '@angular/common';
import { HideLoading, ShowLoading } from '@shared/global-loading';
import { switchMap } from 'rxjs/operators';
import { forkJoin, Observable, timer } from 'rxjs';
import { environment } from '@environment';
import { ProductGroupManagementService } from '@features/product-group-management/store';
import { IHttpGetRequest } from '@core/models';
import { ProductManagementService } from '@features/product-management/store';
import { IProductResponse } from '@features/product-management/models/product-management-table.model';
import { EProductEntityStatus } from '@features/product-management/enums';
import { SettingDisplayAdvertisingComponent } from '@features/campaign-management/components/create-advertising/components/setting-display-advertising';
import { EFileUpLoad } from '@features/campaign-management/components/create-advertising/components/setting-format-advertising/enums';
import { FeatureType } from '@features/campaign-management/components/create-advertising/components/setting-display-advertising/enums';
import { scrollIntoViewBySelector } from '@core/utils';

@Component({
  selector: 'novanet-create-advertising',
  templateUrl: './create-advertising.component.html',
  styleUrls: ['./create-advertising.component.scss'],
})
export class CreateAdvertisingComponent implements OnInit, AfterViewInit {
  @ViewChild('iframe', { static: false }) iframe: ElementRef;
  @ViewChildren(SettingDisplayAdvertisingComponent)
  settingDisplayAdvertisingComponents: QueryList<SettingDisplayAdvertisingComponent>;
  template: ViewContainerRef;
  public selectedAdvertisingFormat = false;

  public createForm: FormGroup;
  public settingTable = AllowSizeSetting;
  public panels = [];
  public currentTemplate;
  public readonly createAdvertisingFormFields: IFormFields =
    CreateAdvertisingFormFields;
  public readonly templatesPreview = TemplatesPreview;
  public readonly templateEnum = ETemplateType;
  public readonly createAdvertisingFormFieldErrors =
    CreateAdvertisingFormFieldErrors;
  public readonly campaignType = CampaignType;

  public templateConfigurations: ITemplateConfiguration[] = [];
  public disabledCollapse = false;
  public uploading = false;
  public createOrUpdate = false;
  public advertisingId = null;
  public products = 0;
  public productDetails: IProductResponse[] = [];
  public advertisingSet: IAdsetByIdResponse;
  public advertisingSetCampaignType: CampaignType;
  public disabledCheckBox: { [key: string]: boolean } = {};
  public featuresTypeAdvertising = FeatureType.CREATE;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private formBuilder: FormBuilder,
    private store: Store,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private advertisingService: AdvertisingService,
    private adsetService: AdsetService,
    private productGroupManagementService: ProductGroupManagementService,
    private productManagementService: ProductManagementService,
    private storageService: StorageService
  ) {}

  public get isAnyTemplateUsed(): boolean {
    return !!this.templateConfigurations.find((item) => item.isActive);
  }

  ngOnInit() {
    const advertisingSetId =
      this.activatedRoute.snapshot.queryParamMap.get('adsetId');

    this.advertisingId = this.activatedRoute.snapshot.queryParamMap.get('adId');

    if (this.advertisingId) {
      this.featuresTypeAdvertising = FeatureType.EDIT;
      this.advertisingService.get(this.advertisingId).subscribe((item) => {
        this.disabledCollapse = true;
        this.advertisingSetCampaignType = item.campaignType;
        const params = {
          page: 1,
          pageSize: 5,
          fields: ['Title'],
          productFeedIds: item.productFeedId ? item.productFeedId : '',
          status: EProductEntityStatus.Running,
        } as IHttpGetRequest;
        this.productManagementService
          .getProducts(params)
          .subscribe((response) => {
            this.productDetails = response.data;
            this.products = response.data.length;
            this.changeDetectorRef.detectChanges();
          });
        this.changeDetectorRef.detectChanges();
        this.buildCreateAdvertisingForm(item);
        switch (item.campaignType) {
          case CampaignType.Ecommerce:
            this.panels = cloneDeep(settingPanels);
            break;
          case CampaignType.Display:
            this.panels = cloneDeep(settingDisplayAdsPanels);
            break;
        }
        this.panels = this.panels.filter(
          (x) => x.id === item.templates[0].templateType
        );
        this.panels.forEach((x) => {
          x.active = true;
          x.activeAdvertising = true;
        });

        this.currentTemplate = item.templates[0].templateType;
        this.templateConfigurations = item.templates.map((panel) => {
          const config: ITemplateConfiguration =
            this.advertisingSetCampaignType === CampaignType.Display
              ? {
                  templateType: panel.templateType,
                  configurations: {
                    collapseBanner: panel.configurations.collapseBanner,
                    cta: panel.configurations.cta,
                    ctaUrl: panel.configurations.ctaUrl,
                    extendBanner: panel.configurations.extendBanner,
                    extendNotification: panel.configurations.extendNotification,
                    images:
                      panel.configurations.images &&
                      JSON.parse(
                        decodeURIComponent(panel.configurations.images)
                      ),
                    isCta: panel.configurations.isCta === 'True',
                    isCustomImage:
                      panel.configurations.isCustomImage === 'True',
                    logo: panel.configurations.logo,
                    shortcutNotification:
                      panel.configurations.shortcutNotification,
                    title: panel.configurations.title,
                    description: panel.configurations.description,
                    isDelay: panel.configurations.isDelay === 'True',
                    delay: panel.configurations.delay
                      ? parseInt(panel.configurations.delay)
                      : 0,
                    popupExtendBannerFileType: this.getFileType(
                      panel.configurations.extendBanner
                    ),
                    catfishExtendBannerFileType: this.getFileType(
                      panel.configurations.extendBanner
                    ),
                    catfishCollapseBannerFileType: this.getFileType(
                      panel.configurations.collapseBanner
                    ),
                  },
                  isActive: true,
                }
              : {
                  isActive: true,
                  configurations: {
                    productName: panel.configurations.productName === 'True',
                    showDiscountedPrice:
                      panel.configurations.showDiscountedPrice === 'True',
                    showFreeDelivery:
                      panel.configurations.showFreeDelivery === 'True',
                  },
                  templateType: panel.templateType,
                };
          if (this.advertisingSetCampaignType === CampaignType.Display) {
            return config;
          }

          if (panel.configurations.avatarUrl) {
            const avatarUrl = panel.configurations.avatarUrl.replace(
              'x{0}',
              'x800'
            );
            config.configurations.currentAvatar =
              environment.host + '/storage/api/v1/stream?name=' + avatarUrl;
          } else if (panel.configurations.expandedContent) {
            const expandedContentUrl =
              panel.configurations.expandedContentUrl.replace('x{0}', 'x800');
            config.configurations.expandedContent =
              environment.host +
              '/storage/api/v1/stream?name=' +
              expandedContentUrl;
          } else {
            const bannerUrl = panel.configurations.bannerImage.replace(
              'x{0}',
              'x800'
            );
            config.configurations.currentBanner =
              environment.host + '/storage/api/v1/stream?name=' + bannerUrl;
          }
          config.configurations.avatarName = panel.configurations.avatarName;
          config.configurations.avatarUrl = panel.configurations.avatarUrl;
          config.configurations.postContent = panel.configurations.postContent;
          config.configurations.useBannerTemplate =
            panel.configurations.useBannerTemplate === 'True';
          config.configurations.backgroundImageUrl =
            panel.configurations.bannerImage;
          config.configurations.expandedContentUrl =
            panel.configurations.expandedContentUrl;
          return config;
        });
      });
    } else if (advertisingSetId) {
      this.adsetService.getById(advertisingSetId).subscribe({
        next: (adset) => {
          this.advertisingSet = adset;
          this.advertisingSetCampaignType =
            this.advertisingSet.campaign?.campaignType;
          switch (this.advertisingSet.campaign?.campaignType) {
            case CampaignType.Ecommerce:
              this.panels = cloneDeep(settingPanels);
              break;
            case CampaignType.Display:
              this.panels = cloneDeep(settingDisplayAdsPanels);
              break;
          }
          switch (this.advertisingSet.campaign?.campaignType) {
            case CampaignType.Ecommerce: {
              const params = {
                page: 1,
                pageSize: 5,
                fields: ['Title'],
                productFeedIds: adset.campaign.ecommerceProductFeedId,
                status: EProductEntityStatus.Running,
              } as IHttpGetRequest;
              this.productManagementService
                .getProducts(params)
                .subscribe((response) => {
                  this.productDetails = response.data;
                  this.products = response.data.length;
                  this.changeDetectorRef.detectChanges();
                });
              this.changeDetectorRef.detectChanges();
              const productGroupIds = adset.advertisingSetProductGroups.map(
                (productGroup) => productGroup.productGroupId
              );
              if (!adset.applyAllProductGroups && productGroupIds.length > 0) {
                this.productGroupManagementService
                  .getProductGroupById(productGroupIds)
                  .subscribe((response) => {
                    let productIds = [] as string[];
                    response.forEach((item) => {
                      item.productGroupEntities.forEach((x) => {
                        productIds.push(x.productId);
                      });
                    });
                    productIds = uniq(productIds);
                    this.products = productIds.length;
                    this.changeDetectorRef.detectChanges();
                  });
              }
              this.templateConfigurations = settingPanels.map((panel) => {
                const config: ITemplateConfiguration = {
                  isActive: panel.active,
                  products: panel.products,
                  configurations: {
                    productName: true,
                    showDiscountedPrice: true,
                    showFreeDelivery: true,
                  },
                  templateType: panel.id,
                };
                if (panel.id === ETemplateType.PostInRead) {
                  config.configurations.avatarUrl = undefined;
                  config.configurations.avatarName = '';
                  config.configurations.postContent = '';
                } else if (panel.id === ETemplateType.CatfishEcom) {
                  config.configurations.useBannerTemplate = true;
                  config.configurations.backgroundImageUrl = undefined;
                  config.configurations.expandedContentUrl = undefined;
                } else {
                  config.configurations.useBannerTemplate = true;
                  config.configurations.backgroundImageUrl = undefined;
                }
                return config;
              });
              break;
            }
            case CampaignType.Display:
              this.templateConfigurations = settingDisplayAdsPanels.map(
                (panel) => {
                  const config: ITemplateConfiguration = {
                    isActive: panel.active,
                    products: panel.products,
                    configurations: {
                      title: '',
                      logo: '',
                      shortcutNotification: '',
                      extendNotification: '',
                      cta: '',
                      ctaUrl: '',
                      images: '',
                      delay: undefined,
                      isCta: true,
                      isCustomImage: true,
                      isDelay: true,
                    },
                    templateType: panel.id,
                  };
                  return config;
                }
              );
              break;
          }
          this.buildCreateAdvertisingForm();
          this.createForm.patchValue({ advertisingSetId });
          this.changeDetectorRef.detectChanges();
        },
      });
    }
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  public getTemplateConfiguration(type: ETemplateType) {
    return this.templateConfigurations.find(
      (item) => item.templateType === type
    );
  }

  public activeStatusChange(event: boolean, panel) {
    panel.activeAdvertising = event;
    panel.active = event;
    if (event) {
      this.currentTemplate = panel.id;
      this.panels.forEach((item) => {
        if (panel.id !== item.id) {
          item.active = false;
        }
      });
    }

    const exist = this.templateConfigurations.find(
      (item) => item.templateType === panel.id
    );
    if (exist) {
      exist.isActive = event;
    }

    if (this.advertisingSetCampaignType === CampaignType.Display) {
      this.disabledCheckBox[panel.id] = true;
      for (const item of this.panels) {
        this.disabledCheckBox[item.id.toString()] =
          event === true ? item.id !== panel.id : false;
      }
    }

    this.changeDetectorRef.detectChanges();
  }

  public shouldShowDisplayAdHeaderWarning(panel) {
    const configuration = this.templateConfigurations.find(
      (item) => item.templateType === panel.id
    );
    if (!configuration?.isActive) {
      return false;
    }
    const formComponent = this.settingDisplayAdvertisingComponents.find(
      (item) => item.templateType === panel.id
    );
    return !formComponent?.validateForm(false) ?? false;
  }

  public onActiveStatus($event, panel) {
    this.selectedAdvertisingFormat = !$event;
    this.currentTemplate = panel.id;
    this.currentTemplate = panel.id;
    panel.active = $event;
    this.panels.forEach((item) => {
      if (panel.id !== item.id) {
        item.active = false;
      }
    });
    this.changeDetectorRef.detectChanges();
  }

  public onTemplateConfigurationChange(
    type: ETemplateType,
    event: ITemplateConfiguration
  ) {
    this.templateConfigurations = this.templateConfigurations.map((item) =>
      item.templateType === type ? event : item
    );
    const item = this.templateConfigurations.find(
      (x) => x.templateType === type
    );
    if (!this.advertisingId) {
      if (item.templateType !== ETemplateType.PostInRead) {
        this.disabledCollapse = !item.configurations['useBannerTemplate'];
        if (
          !item.configurations['useBannerTemplate'] &&
          (item.configurations['bannerImage'] ||
            item.configurations['brandAvatar'])
        ) {
          this.disabledCollapse = false;
        }
      }
    }
  }

  public onTemplateDisplayConfigurationChange(
    type: ETemplateType,
    config: ITemplateDisplayConfiguration
  ) {
    const panel = this.templateConfigurations.find(
      (x) => x.templateType === type
    );
    panel.configurations = config.configurations;
    if (panel.isActive) {
      const formComponent = this.settingDisplayAdvertisingComponents.find(
        (item) => item.templateType === type
      );
      const formValid = formComponent?.validateForm(false);
      if (formValid) {
        this.disabledCheckBox = {};
      } else {
        for (const item of this.panels) {
          this.disabledCheckBox[item.id.toString()] = type !== item.id;
        }
      }
    }
  }

  public async onSubmit(event: Event) {
    event.preventDefault();
    const labelCreating = 'create advertising';
    const labelUpdating = 'update advertising';
    let existBrandAvatar = true;
    let existBannerImage = true;
    let existImageHtml = true;
    const payload: IAdvertisingCreateRequest = {
      ...this.createForm.value,
      templateConfigurations: this.templateConfigurations.filter(
        (item) => item.isActive
      ),
    };

    let isValid = this.createForm.valid;
    if (this.advertisingSetCampaignType === CampaignType.Display) {
      const isDisplayAdsConfigurationValid =
        this.validateDisplayAdsConfiguration(payload);
      isValid = isValid && isDisplayAdsConfigurationValid;
    }
    if (!isValid) {
      this.createForm.markAllAsTouched();
      this.changeDetectorRef.detectChanges();
      if (this.advertisingSetCampaignType === CampaignType.Display) {
        let message = '';
        if (!payload.name) {
          message = 'Tên tin quảng cáo không được để trống';
          this.scrollErrorInputIntoView('.error-message');
        } else if (!payload.redirectLink) {
          message = 'Link đích không được để trống';
          this.scrollErrorInputIntoView('.error-message');
        } else {
          message = 'Thiếu thông tin định dạng';
          this.scrollErrorInputIntoView(
            '.ant-collapse-content:not([style*="height: 0px"]) .error-message'
          );
        }
        this.store.dispatch(
          new ShowGlobalNotification(GlobalNotificationEnum.warning, message)
        );
      }
      return;
    }
    this.uploading = true;

    switch (this.advertisingSetCampaignType) {
      case CampaignType.Display:
        if (this.advertisingId) {
          this.store.dispatch(new ShowLoading(labelUpdating));
          this.updateAdvertisingDisplay(payload);
        } else {
          await this.handleDisplayAdsConfiguration(payload);
          payload.templateConfigurations.forEach((x) => {
            delete x.configurations.redirectUrl;
            delete x.configurations.imageExtendBannerLink;
            delete x.configurations.imageLogoLink;
            delete x.configurations.extendBannerLink;
            delete x.configurations.collapseBannerLink;
            delete x.configurations.logoLink;
            delete x.configurations.logoNotification;
            delete x.configurations.listBannerNotification;
            delete x.configurations.catfishCollapseBannerFileType;
            delete x.configurations.catfishExtendBannerFileType;
            delete x.configurations.popupExtendBannerFileType;
          });
          this.store.dispatch(new CreateAdvertising(payload)).subscribe({
            next: () => {
              this.store.dispatch(
                new ShowGlobalNotification(
                  GlobalNotificationEnum.success,
                  'Tạo tin quảng cáo thành công'
                )
              );
              this.createOrUpdate = false;
              this.store.dispatch(new HideLoading(labelCreating));
            },
            error: () => {
              this.store.dispatch(
                new ShowGlobalNotification(GlobalNotificationEnum.error, 'Lỗi')
              );
              this.createOrUpdate = false;
              this.store.dispatch(new HideLoading(labelCreating));
            },
          });
        }
        break;
      case CampaignType.Ecommerce:
        payload.templateConfigurations.forEach((item) => {
          if (
            item.templateType === ETemplateType.PostInRead &&
            !item.configurations['brandAvatar'] &&
            !item.configurations['currentAvatar']
          ) {
            existBrandAvatar = false;
          }

          if (
            item.templateType !== ETemplateType.PostInRead &&
            !item.configurations['bannerImage'] &&
            !item.configurations['currentBanner'] &&
            !item.configurations['useBannerTemplate']
          ) {
            existBannerImage = false;
          }

          if (
            item.templateType === ETemplateType.CatfishEcom &&
            !item.configurations['useBannerTemplate'] &&
            !item.configurations['expandedContent'] &&
            !item.configurations['expandedContentUrl']
          ) {
            existImageHtml = false;
          }
        });
        if (
          (this.disabledCollapse && !this.advertisingId) ||
          !existBannerImage
        ) {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.warning,
              'Banner không được để trống'
            )
          );
          this.uploading = false;
          return;
        }
        if (!existBrandAvatar) {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.warning,
              'Ảnh Avatar không được để trống'
            )
          );
          this.uploading = false;
          const exist = this.panels.find(
            (x) => x.id === ETemplateType.PostInRead
          );
          exist.active = true;
          this.changeDetectorRef.detectChanges();
          return;
        }
        if (!existImageHtml) {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.warning,
              'Ảnh/HTML5 không được để trống'
            )
          );
          this.uploading = false;
          const exist = this.panels.find(
            (x) => x.id === ETemplateType.CatfishEcom
          );
          exist.active = true;
          this.changeDetectorRef.detectChanges();
          return;
        }
        if (this.advertisingId) {
          this.store.dispatch(new ShowLoading(labelUpdating));
          this.updateAdvertising(payload);
        } else {
          this.store.dispatch(new ShowLoading(labelCreating));
          this.uploadAndCreate(0, payload);
        }
        break;
    }
  }

  public updateAdvertisingDisplay(payload) {
    let payloadUpdateData = {
      id: this.advertisingId,
      name: payload.name,
      redirectUrl: payload.redirectLink,
      cta: payload.templateConfigurations[0].configurations['cta'],
      ctaUrl: payload.templateConfigurations[0].configurations['ctaUrl'],
      extendNotification:
        payload.templateConfigurations[0].configurations['extendNotification'],
      isCta: payload.templateConfigurations[0].configurations['isCta'],
      isCustomImage:
        payload.templateConfigurations[0].configurations['isCustomImage'],
      shortcutNotification:
        payload.templateConfigurations[0].configurations[
          'shortcutNotification'
        ],
      title: payload.templateConfigurations[0].configurations['title'],
      isFullUpdate: true,
      description:
        payload.templateConfigurations[0].configurations['description'],
      isDelay: payload.templateConfigurations[0].configurations['isDelay'],
    } as IAdvertisingUpdateRequest;

    const delay = payload.templateConfigurations[0].configurations['delay'];
    if (delay !== undefined && delay !== null) {
      if (typeof delay === 'number') {
        payloadUpdateData.delay = delay;
      } else if (typeof delay === 'string') {
        payloadUpdateData.delay = parseInt(delay);
      }
    }
    const templateType = payload.templateConfigurations[0].templateType;
    const extendBanner =
      payload.templateConfigurations[0].configurations['extendBanner'];
    const logo = payload.templateConfigurations[0].configurations['logo'];
    const collapseBanner =
      payload.templateConfigurations[0].configurations['collapseBanner'];
    const images = payload.templateConfigurations[0].configurations['images'];

    let listFileUpLoad: { [key: string]: Observable<string> } = {};

    if (
      (logo?.file || images?.some((image) => image?.file)) &&
      templateType === ETemplateType.NotificationBanner
    ) {
      // NotificationBanner
      if (logo?.file) {
        listFileUpLoad[EFileUpLoad.LOGO] = this.storageService.upload(
          logo.file
        );
      }
      if (Array.isArray(images) && images?.length) {
        images.forEach((image, index) => {
          if (image?.file) {
            listFileUpLoad[`${EFileUpLoad.CUSTOM_IMAGES}_${index}`] =
              this.storageService.upload(image.file);
          }
        });
      }
      if (listFileUpLoad) {
        forkJoin(listFileUpLoad).subscribe({
          next: (listFileName) => {
            payloadUpdateData.logo = listFileName[EFileUpLoad.LOGO]
              ? listFileName[EFileUpLoad.LOGO]
              : logo;
            payloadUpdateData.images =
              Array.isArray(images) && images?.length
                ? images
                    .map(
                      (image, index) =>
                        listFileName[`${EFileUpLoad.CUSTOM_IMAGES}_${index}`] ??
                        image
                    )
                    .filter((image) => image)
                : images;
            this.dispatchUpdateAdvertising(payloadUpdateData);
          },
          error: (err) => {
            this.notificationError('Lỗi');
            return;
          },
        });
      }
      return;
    } else if (
      (logo?.file || extendBanner?.file || collapseBanner?.file) &&
      templateType === ETemplateType.CatFishCollabBranding
    ) {
      // CatFishCollapseBranding
      if (logo?.file) {
        listFileUpLoad[EFileUpLoad.LOGO] = this.storageService.upload(
          logo.file
        );
      }
      if (extendBanner?.file) {
        listFileUpLoad[EFileUpLoad.EXTEND_BANNER] = extendBanner.name?.endsWith(
          '.zip'
        )
          ? this.storageService.uploadZip(extendBanner.file)
          : this.storageService.upload(extendBanner.file);
      }
      if (collapseBanner?.file) {
        listFileUpLoad[EFileUpLoad.COLLAPSE_BANNER] =
          collapseBanner.name?.endsWith('.zip')
            ? this.storageService.uploadZip(collapseBanner.file)
            : this.storageService.upload(collapseBanner.file);
      }
      if (listFileUpLoad) {
        forkJoin(listFileUpLoad).subscribe({
          next: (listFileName) => {
            payloadUpdateData.logo = listFileName[EFileUpLoad.LOGO]
              ? listFileName[0]
              : logo;
            payloadUpdateData.extendBanner = listFileName[
              EFileUpLoad.EXTEND_BANNER
            ]
              ? listFileName[1]
              : extendBanner;
            payloadUpdateData.collapseBanner = listFileName[
              EFileUpLoad.COLLAPSE_BANNER
            ]
              ? listFileName[2]
              : collapseBanner;
            this.dispatchUpdateAdvertising(payloadUpdateData);
          },
          error: (err) => {
            this.notificationError('Lỗi');
            return;
          },
        });
      }
      return;
    } else if (
      (logo?.file || extendBanner?.file) &&
      templateType === ETemplateType.PopupBannerBranding
    ) {
      // PopupBannerBranding
      if (logo?.file) {
        listFileUpLoad[EFileUpLoad.LOGO] = this.storageService.upload(
          logo.file
        );
      }
      if (extendBanner?.file) {
        listFileUpLoad[EFileUpLoad.EXTEND_BANNER] = extendBanner.name?.endsWith(
          '.zip'
        )
          ? this.storageService.uploadZip(extendBanner.file)
          : this.storageService.upload(extendBanner.file);
      }
      if (listFileUpLoad) {
        forkJoin(listFileUpLoad).subscribe({
          next: (listFileName) => {
            payloadUpdateData.logo = listFileName[EFileUpLoad.LOGO]
              ? listFileName[0]
              : logo;
            payloadUpdateData.extendBanner = listFileName[
              EFileUpLoad.EXTEND_BANNER
            ]
              ? listFileName[1]
              : extendBanner;
            this.dispatchUpdateAdvertising(payloadUpdateData);
          },
          error: (err) => {
            this.notificationError('Lỗi');
            return;
          },
        });
      }
      return;
    }

    switch (templateType) {
      case ETemplateType.NotificationBanner:
        payloadUpdateData.images = !images?.some((image) => image?.file)
          ? images
          : payloadUpdateData.images;
        payloadUpdateData.logo = !logo.file ? logo : payloadUpdateData.logo;
        break;
      case ETemplateType.CatFishCollabBranding:
        payloadUpdateData.collapseBanner = !collapseBanner.file
          ? collapseBanner
          : payloadUpdateData.collapseBanner;
        payloadUpdateData.extendBanner = !extendBanner.file
          ? extendBanner
          : payloadUpdateData.extendBanner;
        payloadUpdateData.logo = !logo.file ? logo : payloadUpdateData.logo;
        break;
      case ETemplateType.PopupBannerBranding:
        payloadUpdateData.extendBanner = !extendBanner.file
          ? extendBanner
          : payloadUpdateData.extendBanner;
        payloadUpdateData.logo = !logo.file ? logo : payloadUpdateData.logo;
        break;
    }
    this.dispatchUpdateAdvertising(payloadUpdateData);
  }

  public notificationSuccess(message: string) {
    this.createOrUpdate = false;
    this.uploading = false;
    this.store.dispatch(
      new ShowGlobalNotification(GlobalNotificationEnum.success, message)
    );
  }

  public notificationError(message: string) {
    this.createOrUpdate = false;
    this.uploading = false;
    this.store.dispatch(
      new ShowGlobalNotification(GlobalNotificationEnum.error, message)
    );
  }

  private validateDisplayAdsConfiguration(
    payload: IAdvertisingCreateRequest,
    shouldNotify: boolean = true
  ): boolean {
    let isValid = true;
    for (const templateConfiguration of this.templateConfigurations.filter(
      (item) => item.isActive
    )) {
      const formComponent = this.settingDisplayAdvertisingComponents.find(
        (item) => item.templateType === templateConfiguration.templateType
      );
      const formValid = formComponent.validateForm(shouldNotify);
      isValid = isValid && formValid;
    }
    return isValid;
  }

  private async handleDisplayAdsConfiguration(
    payload: IAdvertisingCreateRequest
  ) {
    payload.templateConfigurations.forEach((item) => {
      item.configurations = cloneDeep(item.configurations);
    });
    for (const template of payload.templateConfigurations) {
      if (template.configurations.images) {
        for (const image of template.configurations.images) {
          image.uploadFileName = await this.storageService
            .upload(image.file)
            .toPromise();
        }
      }

      if (template.configurations.logo?.file) {
        template.configurations.logo.uploadFileName = await this.storageService
          .upload(template.configurations.logo.file)
          .toPromise();
      }

      if (template.configurations.collapseBanner?.file) {
        template.configurations.collapseBanner.uploadFileName =
          await (template.configurations.collapseBanner?.name?.endsWith('zip')
            ? this.storageService.uploadZip(
                template.configurations.collapseBanner.file
              )
            : this.storageService.upload(
                template.configurations.collapseBanner.file
              )
          ).toPromise();
      }

      if (template.configurations.extendBanner?.file) {
        template.configurations.extendBanner.uploadFileName =
          await (template.configurations.extendBanner?.name?.endsWith('zip')
            ? this.storageService.uploadZip(
                template.configurations.extendBanner.file
              )
            : this.storageService.upload(
                template.configurations.extendBanner.file
              )
          ).toPromise();
      }

      if (template.configurations.images) {
        template.configurations.images.forEach((image, key) => {
          template.configurations.images[key] = cloneDeep(
            image.uploadFileName.replace('x{0}', 'x800')
          );
        });
      }

      if (template.configurations.logo) {
        template.configurations.logo = cloneDeep(
          template.configurations.logo.uploadFileName.replace('x{0}', 'x800')
        );
      }

      if (template.configurations.collapseBanner) {
        template.configurations.collapseBanner = cloneDeep(
          template.configurations.collapseBanner.uploadFileName.replace(
            'x{0}',
            'x800'
          )
        );
      }

      if (template.configurations.extendBanner) {
        template.configurations.extendBanner = cloneDeep(
          template.configurations.extendBanner.uploadFileName.replace(
            'x{0}',
            'x800'
          )
        );
      }
    }
  }

  private updateAdvertising(payload: IAdvertisingCreateRequest) {
    const payloadUpdateData = {
      id: this.advertisingId,
      name: payload.name,
      redirectUrl: payload.redirectLink,
      postContent:
        payload.templateConfigurations[0].configurations['postContent'],
      bannerImage:
        payload.templateConfigurations[0].configurations['backgroundImageUrl'],
      expandedContentUrl:
        payload.templateConfigurations[0].configurations['expandedContentUrl'],
      avatarName:
        payload.templateConfigurations[0].configurations['avatarName'],
      avatarUrl: payload.templateConfigurations[0].configurations['avatarUrl'],
      showDiscountedPrice:
        payload.templateConfigurations[0].configurations['showDiscountedPrice'],
      showFreeDelivery:
        payload.templateConfigurations[0].configurations['showFreeDelivery'],
      productName:
        payload.templateConfigurations[0].configurations['productName'],
      useBannerTemplate:
        payload.templateConfigurations[0].configurations['useBannerTemplate'],
      isFullUpdate: true,
    } as IAdvertisingUpdateRequest;

    const bannerImage =
      payload.templateConfigurations[0].configurations['bannerImage'];
    const brandAvatar =
      payload.templateConfigurations[0].configurations['brandAvatar'];
    const expandedContent =
      payload.templateConfigurations[0].configurations['expandedContent'];
    if (brandAvatar) {
      this.storageService.upload(brandAvatar).subscribe((fileName) => {
        payloadUpdateData.avatarUrl = fileName;
        this.dispatchUpdateAdvertising(payloadUpdateData);
      });
      return;
    }
    if (bannerImage || expandedContent) {
      if (expandedContent && bannerImage) {
        const uploadCallback = this.uploadZipOrImage(
          payload.templateConfigurations,
          0,
          'expandedContent'
        );
        uploadCallback().subscribe((fileName) => {
          payloadUpdateData.expandedContentUrl = fileName;
          this.storageService.upload(bannerImage).subscribe((fileName) => {
            payloadUpdateData.bannerImage = fileName;
            this.dispatchUpdateAdvertising(payloadUpdateData);
          });
        });
        return;
      } else if (bannerImage) {
        this.storageService.upload(bannerImage).subscribe((fileName) => {
          payloadUpdateData.bannerImage = fileName;
          this.dispatchUpdateAdvertising(payloadUpdateData);
        });
        return;
      } else if (expandedContent) {
        const uploadCallback = this.uploadZipOrImage(
          payload.templateConfigurations,
          0,
          'expandedContent'
        );
        uploadCallback().subscribe((fileName) => {
          payloadUpdateData.expandedContentUrl = fileName;
          this.dispatchUpdateAdvertising(payloadUpdateData);
        });
        return;
      }
    }
    this.dispatchUpdateAdvertising(payloadUpdateData);
  }

  private dispatchUpdateAdvertising(
    payloadUpdateData: IAdvertisingUpdateRequest
  ) {
    const label = 'update advertising';
    this.uploading = false;
    this.createOrUpdate = true;
    this.store.dispatch(new UpdateAdvertising(payloadUpdateData)).subscribe({
      next: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Cập nhật tin quảng cáo  thành công'
          )
        );
        this.createOrUpdate = false;
        this.store.dispatch(new HideLoading(label));
      },
      error: () => {
        this.store.dispatch(
          new ShowGlobalNotification(GlobalNotificationEnum.error, 'Lỗi')
        );
        this.createOrUpdate = false;
        this.store.dispatch(new HideLoading(label));
      },
    });
  }

  private uploadAndCreate(index, payload) {
    const label = 'create advertising';
    if (
      payload.templateConfigurations[index]?.templateType ===
        ETemplateType.CatfishEcom &&
      !payload.templateConfigurations[index].configurations['useBannerTemplate']
    ) {
      this.storageService
        .upload(
          payload.templateConfigurations[index].configurations[
            'expandedContent'
          ]
        )
        .subscribe(
          (fileName) => {
            payload.templateConfigurations[index].configurations[
              'expandedContentUrl'
            ] = fileName;
            this.handleSubmit(index, payload, label);
          },
          (error) => {
            this.uploading = false;
            this.store.dispatch(new HideLoading(label));
          }
        );
      return;
    }

    this.handleSubmit(index, payload, label);
  }

  private handleSubmit(index, payload, label) {
    if (index !== payload.templateConfigurations.length) {
      if (
        payload.templateConfigurations[index].configurations['bannerImage'] &&
        !payload.templateConfigurations[index].configurations[
          'useBannerTemplate'
        ]
      ) {
        this.storageService
          .upload(
            payload.templateConfigurations[index].configurations['bannerImage']
          )
          .subscribe(
            (fileName) => {
              payload.templateConfigurations[index].configurations[
                'bannerImage'
              ] = fileName;
              this.uploadAndCreate(index + 1, payload);
            },
            (error) => {
              this.uploading = false;
              this.store.dispatch(new HideLoading(label));
            }
          );
      } else if (
        payload.templateConfigurations[index].configurations['brandAvatar'] &&
        typeof payload.templateConfigurations[index].configurations[
          'brandAvatar'
        ] !== 'string'
      ) {
        this.storageService
          .upload(
            payload.templateConfigurations[index].configurations['brandAvatar']
          )
          .subscribe(
            (fileName) => {
              payload.templateConfigurations[index].configurations[
                'avatarUrl'
              ] = fileName;
              this.uploadAndCreate(index + 1, payload);
            },
            (error) => {
              this.store.dispatch(new HideLoading(label));
            }
          );
      } else if (
        payload.templateConfigurations[index].configurations[
          'expandedContentUrl'
        ] &&
        !payload.templateConfigurations[index].configurations[
          'useExpandedContentTemplate'
        ]
      ) {
        const uploadCallback = this.uploadZipOrImage(
          payload.templateConfigurations,
          index,
          'expandedContent'
        );
        uploadCallback().subscribe(
          (fileName) => {
            payload.templateConfigurations[index].configurations[
              'expandedContentUrl'
            ] = fileName;
            this.uploadAndCreate(index + 1, payload);
          },
          (error) => {
            this.store.dispatch(new HideLoading(label));
          }
        );
      } else {
        this.uploadAndCreate(index + 1, payload);
      }
      return;
    }
    payload.templateConfigurations.forEach((x) => {
      delete x.configurations.imageLink;
      delete x.configurations.brandAvatar;
      delete x.configurations.showDelivery;
      delete x.configurations.redirectUrl;
      delete x.configurations.currentAvatar;
      delete x.configurations.currentBanner;
      delete x.configurations.expandedContent;
      delete x.configurations.currentExpandedBanner;
      x.configurations.bannerImage = x.configurations.bannerImage ?? '';
      x.configurations.expandedContentUrl =
        x.configurations.expandedContentUrl ?? '';
    });
    this.uploading = false;
    this.createOrUpdate = true;
    this.store.dispatch(new CreateAdvertising(payload)).subscribe({
      next: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Tạo tin quảng cáo  thành công'
          )
        );
        this.createOrUpdate = false;
        this.store.dispatch(new HideLoading(label));
      },
      error: () => {
        this.store.dispatch(
          new ShowGlobalNotification(GlobalNotificationEnum.error, 'Lỗi')
        );
        this.createOrUpdate = false;
        this.store.dispatch(new HideLoading(label));
      },
    });
  }

  private buildCreateAdvertisingForm(item?: IAdvertisingResponse) {
    const config = {
      [this.createAdvertisingFormFields.name.name]: [
        item ? item.name : '',
        [
          Validators.required,
          Validators.maxLength(
            this.createAdvertisingFormFields.name.validationParams
              .maxLength as number
          ),
        ],
      ],
      [this.createAdvertisingFormFields.banner.name]: [''],
      [this.createAdvertisingFormFields.advertisingSetId.name]: [''],
      [this.createAdvertisingFormFields.redirectLink.name]: [
        item ? item.redirectLink : '',
        [
          ...(this.advertisingSetCampaignType === CampaignType.Display
            ? [Validators.required]
            : []),
          Validators.pattern(
            this.createAdvertisingFormFields.redirectLink.validationParams
              .pattern
          ),
          Validators.maxLength(
            this.createAdvertisingFormFields.redirectLink.validationParams
              .maxLength as number
          ),
        ],
      ],
    };
    this.createForm = this.formBuilder.group(config);
    this.changeDetectorRef.detectChanges();
    this.createForm.valueChanges
      .pipe(
        switchMap(
          (_) => timer(500),
          (item) => item
        )
      )
      .subscribe((item) => {
        this.templateConfigurations.forEach((x) => {
          x.configurations.redirectUrl = item.redirectLink;
        });
      });
  }

  private uploadZipOrImage(
    templateConfigurations: ITemplateConfiguration[],
    index: number,
    fieldContent: string
  ): () => Observable<string> {
    if (
      templateConfigurations[index].configurations[fieldContent]?.type ===
      zipMimeType
    ) {
      return () =>
        this.storageService.uploadZip(
          templateConfigurations[index].configurations[fieldContent]
        );
    }

    return () =>
      this.storageService.upload(
        templateConfigurations[index].configurations[fieldContent]
      );
  }

  private scrollErrorInputIntoView(messageSelector: string) {
    setTimeout(() => {
      scrollIntoViewBySelector(messageSelector);
      setTimeout(() => {
        const input = document
          .querySelector(messageSelector)
          ?.parentElement.querySelector(
            '.border-\\[\\#EB5757\\]'
          ) as HTMLInputElement;
        input?.focus?.();
      }, 500);
    }, 200);
  }

  private getFileType(fileName: string | undefined) {
    return fileName?.includes('.html')
      ? 'html'
      : fileName?.includes('.zip')
      ? 'zip'
      : 'image';
  }
}
