import { IPredicateModel } from '@core/models';
import {
  CampaignStatusEnum,
  CampaignType,
  ICampaignGetResponse,
  ICampaignScheduling,
  ICampaignSchedulingResponse,
} from '@features/campaign-management/store/campaign';
import { EAdsetAddRemarketingOption } from '@features/campaign-management/components';
import { EUniformedUnit } from '@features/campaign-management/components/adset-add/components/adset-add-remarketing/components/more-remarketing/enums';

export enum EWebsiteCondition {
  Include = 1,
  Exclude = 2,
}

export interface IAdsetListRequest {
  pageSize: number;
  page: number;
  filters?: IPredicateModel[];
  status?: CampaignStatusEnum;
  startDate?: Date;
  endDate?: Date;
  withSummary?: boolean;
  withReport?: boolean;
  campaignIds?: string[];
  sorts?: string;
  campaignType?: CampaignType;
  fields?: number[];
}

export interface IAdsetResponse {
  id: string;
  name: string;
  isActive: boolean;
  campaignId: string;
  campaignName: string;
  status: CampaignStatusEnum;
  data: {
    totalCost: number;
    impressions: number;
    clicks: number;
    ctr: number;
    cpc: number;
    purchase: number;
    purchaseConversion: number;
    cps: number;
    videoView3s: number;
    video25Percent: number;
    video50Percent: number;
    video75Percent: number;
    video100Percent: number;
  };
  productGroupData: IAdsetProductGroupResponse[];
}

export interface IAdsetProductGroupResponse {
  id: string;
  name: string;
  status: CampaignStatusEnum;
  isActive: boolean;
  data: {
    totalCost: number;
    impressions: number;
    clicks: number;
    videoView3s: number;
    video25Percent: number;
    video50Percent: number;
    video75Percent: number;
    video100Percent: number;
  };
}

export interface IAdsetSummary {
  total: number;
  totalCost: number;
  impressions: number;
  clicks: number;
  ctr: number;
  cpc: number;
  purchase: number;
  purchaseConversion: number;
  cps: number;
  videoView3s: number;
  video25Percent: number;
  video50Percent: number;
  video75Percent: number;
  video100Percent: number;
}

export interface IAdsetListResponse {
  data: IAdsetResponse[];
  summary: IAdsetSummary;
}

export interface IAdsetByIdResponse {
  id: string;
  name: string;
  isActive: boolean;
  campaignId: string;
  status: CampaignStatusEnum;
  hasProhibitedCategories: boolean;
  applyAllProductGroups: boolean;
  hasObject: boolean;
  advertisingSetProhibitedCategories: {
    advertisingSetId: string;
    prohibitedCategoryId: string;
    id: string;
  }[];
  advertisingSetGenders: {
    advertisingSetId: string;
    genderId: string;
    id: string;
  }[];
  advertisingSetCategories: {
    advertisingSetId: string;
    categoryId: string;
    id: string;
  }[];
  advertisingSetAges: {
    ageId: string;
    advertisingSetId: string;
    id: string;
  }[];
  advertisingSetLocations: {
    advertisingSetId: string;
    locationId: string;
    id: string;
  }[];
  advertisingSetObjects: {
    advertisingSetId: string;
    objectId: string;
    id: string;
  }[];
  advertisingSetProductGroups: {
    advertisingSetId: string;
    productGroupId: string;
    id: string;
    isActive: boolean;
    status: CampaignStatusEnum;
  }[];
  // remarketing field
  adImpressions?: number;
  clickedAdvertising?: boolean;
  remarketingTime?: number;
  remarketingType?: EAdsetAddRemarketingOption;
  timeOnDisplay?: number;
  uniformed: boolean;
  uniformedPrice?: number;
  uniformedUnit?: EUniformedUnit;
  viewedMax?: number;
  viewer?: boolean;
  excludedWebsite?: boolean;
  advertisingSetWebsites: {
    advertisingSetId: string;
    websiteId: string;
    id: string;
    websiteCondition: EWebsiteCondition;
  }[];
  targetingMarketingByProduct?: boolean;
  campaign?: ICampaignGetResponse;
  advertisingSetScheduling: ICampaignSchedulingResponse[];
  scheduledAllTime?: boolean;
}

export interface IAdsetCreateRequest {
  name: string;
  campaignId: string;
  applyAllProductGroups: boolean;
  productGroupIds: string[];
  hasProhibitedCategories: boolean;
  prohibitedCategoryIds: string[];
  hasObject: boolean;
  objectIds: string[];
  locationIds: string[];
  ageIds: string[];
  genderIds: string[];
}

export interface IAdsetUpdateRequest {
  id: string;
  name: string;
  isActive: boolean;
  isFullUpdate: boolean;
  schedulings?: Partial<ICampaignScheduling>[];
}

export interface IAdsetUpdateProductGroupRequest {
  productGroupId: string;
  advertisingSetId: string;
  isActive: boolean;
}

export interface IAdsetProductGroup {
  id: string;
  advertisingSetId: string;
  advertisingSetStatus: CampaignStatusEnum;
  productGroupId: string;
  status: CampaignStatusEnum;
  isActive: boolean;
}

export interface IAdsetStateModel {
  adsets: IAdsetResponse[];
  adsetSummary?: IAdsetSummary;
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  detailedAdset?: IAdsetByIdResponse;
  selectedAdsetIds: string[];
  navigatedAdsetId?: string;
}
