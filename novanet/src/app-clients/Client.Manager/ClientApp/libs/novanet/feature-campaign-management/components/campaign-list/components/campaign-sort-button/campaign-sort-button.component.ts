import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ESortType } from '@core/enums';
import { ICampaignSortOption } from '@features/campaign-management/types';

@Component({
  selector: 'novanet-campaign-sort-button',
  templateUrl: './campaign-sort-button.component.html',
})
export class CampaignSortButtonComponent {
  @Input() sort: string;
  @Input() sortField: string;
  @Output() sortChange = new EventEmitter<ICampaignSortOption>();

  public readonly eSortType = ESortType;

  public sortMetric(field: string, type: ESortType) {
    this.sortChange.emit({ field, type });
  }
}
