import { NgModule } from '@angular/core';
import { AdsetAddRemarketingComponent } from './adset-add-remarketing.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { MoreRemarketingModule } from './components';

const COMPONENTS = [AdsetAddRemarketingComponent];

const MODULES = [CommonModule, FormsModule, ReactiveFormsModule, NzTabsModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, MoreRemarketingModule],
  exports: [...COMPONENTS],
})
export class AdsetAddRemarketingModule {}
