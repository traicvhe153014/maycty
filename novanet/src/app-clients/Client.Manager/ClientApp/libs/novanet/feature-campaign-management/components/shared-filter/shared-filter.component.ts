import { GlobalNotificationEnum } from '@shared/global-notification/enums';
import {
  ShowGlobalNotification,
  UpdateIsReportDownloading,
} from '@shared/global-notification/store/global-notification-state.actions';
import { take } from 'rxjs/operators';
import { GlobalNotificationState } from '@shared/global-notification/store';
import {
  AdGroupGenerateReportOptions,
  AdvertisementGenerateReportOptions,
  CampaignGenerateReportOptions,
  campaignManagementFilter,
  campaignSearchLevelFields,
  campaignSearchLevelLabels,
  campaignSearchOperatorLabels,
  CampaignStatusOptions,
  shortDateFormat,
} from '@features/campaign-management/constants';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  CampaignStatusEnum,
  CampaignType,
} from '@features/campaign-management/store/campaign';
import { IPredicateModel } from '@core/models';
import { ICampaignSearchOption } from '@features/campaign-management/types';
import {
  CampaignSearchLevelEnum,
  ReportOptions,
} from '@features/campaign-management/enums';
import { storage } from '@core';
import { ICampaignManagementFilter } from '@features/campaign-management/models';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { MultipleSelectInputEnum } from '@shared/custom-input/components/multiple-select-input/enums';
import { IOptionGroup } from '@shared/custom-input/components/multiple-select-input/models/multiple-select-input.model';

@Component({
  selector: 'novanet-shared-filter',
  templateUrl: './shared-filter.component.html',
  styleUrls: ['./shared-filter.component.scss'],
})
export class SharedFilterComponent implements OnInit, AfterViewInit {
  @Input() createElement: TemplateRef<any>;
  @Input() actionsElement: TemplateRef<any>;
  @Input() showActions: boolean;
  @Input() exceptFilterStatus: CampaignStatusEnum[] = [];
  @Input() tabIndex: number;
  @Input() campaignGroupOptionColumn: IOptionGroup[] = [];
  @Input() selectedColumnDefault: any[] = [];
  @Output() dateRangeChange = new EventEmitter<Date[]>();
  @Output() statusChange = new EventEmitter<CampaignStatusEnum>();
  @Output() predicatesChange = new EventEmitter<IPredicateModel[]>();
  @Output() allFilterChange = new EventEmitter<ICampaignManagementFilter>();
  @Output() exportData = new EventEmitter<ICampaignManagementFilter>();
  @Output() updateColumns = new EventEmitter<number[]>();

  public campaignStatusOptions = CampaignStatusOptions;
  public readonly shortDateFormat = shortDateFormat;
  public readonly campaignSearchLevelLabels = campaignSearchLevelLabels;
  public readonly campaignSearchOperatorLabels = campaignSearchOperatorLabels;
  public readonly campaignSearchLevelFields = campaignSearchLevelFields;

  public searchDropdownOpen = false;
  public filterDateRange: Date[] = [new Date(), new Date()];
  public filterStatus: CampaignStatusEnum | null = null;
  public filterPredicates: IPredicateModel[] = [];
  public keyword = '';
  public searchOptions: ICampaignSearchOption[] = [];
  public selectedSearchOptions: ICampaignSearchOption[] = [];
  public dateTimeForm: FormGroup;
  public ListSelectedReportOptions: any[];
  public level: string;
  public durationNotificationCancel = Number.MAX_SAFE_INTEGER;
  public popoverNotification: string;
  public campaignSearchLevelEnum = CampaignSearchLevelEnum;
  public isFocus = false;
  public filterCampaignType: CampaignType | undefined = undefined;
  public multipleSelectStyle = MultipleSelectInputEnum;
  public selectedColumn: string[] = [];

  @ViewChild('onreportOption') onreportOption: TemplateRef<any>;
  @ViewChild('reportDropdown') reportDropdown: ElementRef;

  @Select(GlobalNotificationState.isReportDownloading)
  isReportDownloading$: Observable<boolean>;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private store: Store
  ) {}

  ngOnInit() {
    this.popoverNotification =
      'Báo cáo khác đang được tải xuống, vui lòng chờ tới khi báo cáo được tải xuống thành công!';
    switch (this.tabIndex) {
      case 0:
        this.ListSelectedReportOptions = CampaignGenerateReportOptions;
        break;
      case 1:
        this.ListSelectedReportOptions = AdGroupGenerateReportOptions;
        break;
      case 2:
        this.ListSelectedReportOptions = AdvertisementGenerateReportOptions;
        break;
    }
    const filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    this.filterDateRange = [];

    if (filter) {
      if (
        filter.dateRange &&
        Array.isArray(filter.dateRange) &&
        filter.dateRange.length
      ) {
        this.filterDateRange = [
          new Date(filter.dateRange[0]),
          new Date(filter.dateRange[1]),
        ];
        const config = {
          ['date']: [this.filterDateRange],
        };
        this.dateTimeForm = this.formBuilder.group(config);
        this.dateTimeForm
          .get('date')
          .valueChanges.pipe()
          .subscribe((values) => {
            this.onChangeFilterDateRange(values);
          });
      } else {
        const config = {
          ['date']: [this.filterDateRange],
        };
        this.dateTimeForm = this.formBuilder.group(config);
        this.dateTimeForm
          .get('date')
          .valueChanges.pipe()
          .subscribe((values) => {
            this.onChangeFilterDateRange(values);
          });
      }
      this.filterStatus = filter.status ?? null;
      this.filterPredicates = filter.predicates ?? [];
      this.filterCampaignType = filter.campaignType;
    } else {
      const config = {
        ['date']: [this.filterDateRange],
      };
      this.dateTimeForm = this.formBuilder.group(config);
      this.dateTimeForm
        .get('date')
        .valueChanges.pipe()
        .subscribe((values) => {
          this.onChangeFilterDateRange(values);
        });
    }
    this.selectedSearchOptions = this.filterPredicates.map((item) => ({
      value: item.value,
      operator: item.operator,
      level: parseInt(
        Object.keys(campaignSearchLevelFields).find(
          (field) => campaignSearchLevelFields[field] === item.field
        ) ?? '0',
        10
      ),
    }));
    this.searchOptions = this.selectedSearchOptions;

    if (this.exceptFilterStatus.length > 0) {
      this.exceptFilterStatus.forEach(
        (exceptFilterStatus) =>
          (this.campaignStatusOptions = this.campaignStatusOptions.filter(
            (value) => value.value !== exceptFilterStatus
          ))
      );
    }
    this.allFilterChange.emit({
      status: this.filterStatus,
      dateRange: this.filterDateRange,
      predicates: this.filterPredicates,
      campaignType: this.filterCampaignType,
      selectedColumnCampaign: filter?.selectedColumnCampaign ?? [],
      selectedColumnAdset: filter?.selectedColumnAdset ?? [],
      selectedColumnAdvertising: filter?.selectedColumnAdvertising ?? [],
    });
  }

  ngAfterViewInit(): void {
    this.ListSelectedReportOptions = this.ListSelectedReportOptions.map(
      (item) => {
        if (item.valueKey !== ReportOptions.OnReport) {
          return item;
        }
        return {
          ...item,
          label: this.onreportOption,
        };
      }
    );
  }

  public onSearch(options: IPredicateModel[]) {
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.predicates = options;
    } else {
      filter = {
        predicates: options,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.filterPredicates = options;
    this.predicatesChange.emit(options);
  }

  public onFilterStatusChange(status: CampaignStatusEnum) {
    this.filterStatus = status;
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.status = status;
    } else {
      filter = {
        status,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.statusChange.emit(status);
  }

  public onChangeFilterDateRange(result: Date[]) {
    this.filterDateRange = result;
    let filter = storage.get(
      campaignManagementFilter
    ) as ICampaignManagementFilter;
    if (filter) {
      filter.dateRange = result;
    } else {
      filter = {
        dateRange: result,
      };
    }
    storage.set(campaignManagementFilter, filter);
    this.dateRangeChange.emit(result);
  }

  public onSelectReport(event): void {
    this.isReportDownloading$.pipe(take(1)).subscribe((isReportDownloading) => {
      if (isReportDownloading) {
        return;
      }
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.download,
          `Đang tải xuống báo cáo`,
          `Quá trình tải xuống có thể mất nhiều thời gian.
              Vui lòng không reload lại trang và duy trì mạng ổn định.`
        )
      );
      this.store.dispatch(new UpdateIsReportDownloading(true));
      this.exportData.emit({
        status: this.filterStatus,
        dateRange: this.filterDateRange,
        predicates: this.filterPredicates,
        exportType: event,
      });
    });
  }

  public onFocusSearch() {
    this.isFocus = true;
  }

  public onBlurSearch() {
    this.isFocus = false;
  }

  public onSelectColumn(value: number[]) {
    this.updateColumns.emit(value);
  }
}
