import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { IFormFieldErrors, IFormFields } from '@models';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { Select } from '@ngxs/store';
import {
  IAge,
  ICategory,
  IGender,
  ILocation,
  SharingState,
} from '@shared/sharing/store';
import { Observable } from 'rxjs';
import { genderOrders } from '@features/campaign-management/constants';

@Component({
  selector: 'novanet-adset-add-ad-target',
  templateUrl: './adset-add-ad-target.component.html',
  styleUrls: ['./adset-add-ad-target.component.scss'],
})
export class AdsetAddAdTargetComponent implements OnInit {
  @Select(SharingState.getLocations) locations$: Observable<ILocation[]>;
  @Select(SharingState.getAges) ages$: Observable<IAge[]>;
  @Select(SharingState.getGenders) genders$: Observable<IGender[]>;
  @Select(SharingState.getCategorys) categories$: Observable<ICategory[]>;

  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;

  public form: FormGroup;
  public locations: ILocation[] = [];
  public genders: IGender[] = [];
  public ages: IAge[] = [];
  public categories: ICategory[] = [];

  constructor(
    private rootFormGroup: FormGroupDirective,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public isLocationChecked(locationId: string): boolean {
    const locationIds = this.form.get('locationIds').value as string[];
    return !!locationIds.find((id) => id === locationId);
  }

  public isRegionChecked(regionName: string): boolean {
    const locationIds = this.form.get('locationIds').value as string[];
    const allLocationsInRegion: ILocation[] = [];
    const selectedLocations: ILocation[] = [];
    this.locations.forEach((location): void => {
      if (location.regionName !== regionName) {
        return;
      }
      allLocationsInRegion.push(location);
      if (!!locationIds.find((id) => id === location.id)) {
        selectedLocations.push(location);
      }
    });
    return allLocationsInRegion.length === selectedLocations.length;
  }

  public isAgeChecked(ageId: string): boolean {
    const ageIds = this.form.get('ageIds').value as string[];
    return !!ageIds.find((id) => id === ageId);
  }

  public isAllAgesChecked(): boolean {
    const ageIds = this.form.get('ageIds').value as string[];
    return ageIds.length === this.ages.length;
  }

  public isGenderChecked(genderId: string): boolean {
    const genderIds = this.form.get('genderIds').value as string[];
    return !!genderIds.find((id) => id === genderId);
  }

  public isAllGendersChecked(): boolean {
    const genderIds = this.form.get('genderIds').value as string[];
    return genderIds.length === this.genders.length;
  }

  public isAllCategoriesChecked(): boolean {
    const categoryIds = this.form.get('categoryIds').value as string[];
    return categoryIds.length === this.categories.length;
  }

  public isCategoryChecked(categoryId: string): boolean {
    const categoryIds = this.form.get('categoryIds').value as string[];
    return !!categoryIds.find((id) => id === categoryId);
  }

  ngOnInit() {
    this.form = this.rootFormGroup.control;
    this.locations$.subscribe({
      next: (locations) => {
        this.locations = locations;
        this.changeDetectorRef.detectChanges();
      },
    });
    this.categories$.subscribe({
      next: (categories) => {
        this.categories = categories;
        this.changeDetectorRef.detectChanges();
      },
    });
    this.ages$.subscribe({
      next: (ages) => {
        this.ages = ages.slice().sort((a, b) => a.from - b.to);
        this.changeDetectorRef.detectChanges();
      },
    });
    this.genders$.subscribe({
      next: (genders) => {
        this.genders = genders
          .slice()
          .sort(
            (a, b) =>
              genderOrders.indexOf(a.name) - genderOrders.indexOf(b.name)
          );
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public toggleLocation(event: Event, locationId: string) {
    const locationIds = this.form.get('locationIds').value as string[];
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        locationIds: [...locationIds, locationId],
      });
    } else {
      this.form.patchValue({
        locationIds: locationIds.filter((id) => id !== locationId),
      });
    }
  }

  public toggleAllProvinces(event: Event, regionName: string) {
    const locationIds = this.form.get('locationIds').value as string[];
    if ((event.target as HTMLInputElement).checked) {
      const newLocationIds = this.locations
        .filter((location) => location.regionName === regionName)
        .map((location) => location.id);
      this.form.patchValue({
        locationIds: Array.from(new Set([...locationIds, ...newLocationIds])),
      });
    } else {
      this.form.patchValue({
        locationIds: locationIds.filter(
          (id) =>
            !this.locations.find(
              (location) =>
                location.regionName === regionName && location.id === id
            )
        ),
      });
    }
  }

  public onClickAllProvince(event: Event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
  }

  public toggleAge(event: Event, ageId: string) {
    const ageIds = this.form.get('ageIds').value as string[];
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        ageIds: [...ageIds, ageId],
      });
    } else {
      this.form.patchValue({
        ageIds: ageIds.filter((id) => id !== ageId),
      });
    }
  }

  public toggleAllAges(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        ageIds: this.ages.map((age) => age.id),
      });
    } else {
      this.form.patchValue({
        ageIds: [],
      });
    }
  }

  public toggleGender(event: Event, genderId: string) {
    const genderIds = this.form.get('genderIds').value as string[];
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        genderIds: [...genderIds, genderId],
      });
    } else {
      this.form.patchValue({
        genderIds: genderIds.filter((id) => id !== genderId),
      });
    }
  }

  public toggleAllGenders(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        genderIds: this.genders.map((gender) => gender.id),
      });
    } else {
      this.form.patchValue({
        genderIds: [],
      });
    }
  }

  public toggleAllCategories(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        categoryIds: this.categories.map((category) => category.id),
      });
    } else {
      this.form.patchValue({
        categoryIds: [],
      });
    }
  }

  public toggleCategory(event: Event, categoryId: string) {
    const categoryIds = this.form.get('categoryIds').value as string[];
    if ((event.target as HTMLInputElement).checked) {
      this.form.patchValue({
        categoryIds: [...categoryIds, categoryId],
      });
    } else {
      this.form.patchValue({
        categoryIds: categoryIds.filter((id) => id !== categoryId),
      });
    }
  }
}
