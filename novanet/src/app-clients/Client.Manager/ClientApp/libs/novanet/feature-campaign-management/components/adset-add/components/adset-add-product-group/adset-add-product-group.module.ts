import { NgModule } from '@angular/core';
import { AdsetAddProductGroupComponent } from './adset-add-product-group.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';
import { RouterModule } from '@angular/router';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { DataTableModule } from '@core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { ProductGroupManagementStateModule } from '@features/product-group-management/store';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [AdsetAddProductGroupComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NovanetInputModule,
    RouterModule,
    NzSwitchModule,
    NzToolTipModule,
    ProductGroupManagementStateModule,
    DataTableModule,
    SvgIconModule,
  ],
  exports: [AdsetAddProductGroupComponent],
})
export class AdsetAddProductGroupModule {}
