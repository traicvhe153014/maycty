import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { IFormFieldErrors, IFormFields } from '@models';
import { FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import {
  selectedObjectColumns,
  selectedObjectSetting,
} from '@features/campaign-management/components/adset-add/constants';
import { AdsetAddSelectedObjectEnum } from '@features/campaign-management/components/adset-add/enums';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, Subscription } from 'rxjs';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { debounceTime, map, takeUntil } from 'rxjs/operators';
import {
  GetMoreObjectGroups,
  GetObjectGroups,
  ObjectManagementState,
} from '@features/feature-object-management/store';
import { IObjectGroupsResponse } from '@features/feature-object-management/models';
import { IHttpGetRequest } from '@core/models';

@Component({
  selector: 'novanet-adset-add-object-target',
  templateUrl: './adset-add-object-target.component.html',
})
export class AdsetAddObjectTargetComponent implements OnInit, OnDestroy {
  @Select(ObjectManagementState.getObjectGroups)
  public objects$: Observable<IObjectGroupsResponse[]>;
  public objects: IObjectGroupsResponse[] = [];

  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;

  public readonly selectedObjectSetting = selectedObjectSetting;
  public readonly selectedObjectColumns = selectedObjectColumns;
  public selectedObjects: IObjectGroupsResponse[];
  public readonly adsetAddSelectedObjectEnum = AdsetAddSelectedObjectEnum;

  public searchObjectChanged: Subject<string> = new Subject<string>();
  public form: FormGroup;
  private searchObjectChangeSubscription: Subscription;
  private page = 1;
  private pageSize = 50;
  private searchKeyword = '';

  private destroy$ = new Subject<void>();

  constructor(
    private rootFormGroup: FormGroupDirective,
    private store: Store,
    private changeDetectionRef: ChangeDetectorRef
  ) {}

  public get ObjectsId(): FormControl {
    return this.form.get(this.formFields.objectIds.name) as FormControl;
  }

  public get objectOptions(): Observable<NzSelectOptionInterface[]> {
    return this.objects$.pipe(
      map(
        (objects) =>
          objects?.map((item) => ({
            label: item.name,
            value: item.id,
          })) ?? []
      )
    );
  }

  ngOnInit() {
    this.form = this.rootFormGroup.control;
    this.searchObjectChangeSubscription = this.searchObjectChanged
      .pipe(debounceTime(300))
      .subscribe({
        next: (model) => {
          this.onSearch(model);
        },
      });
    const params = {
      page: this.page,
      skip: 0,
      pageSize: this.pageSize,
      keyWord: this.searchKeyword,
      sorts: ['-modifiedAt'],
    } as IHttpGetRequest;
    this.store.dispatch(new GetObjectGroups(params));
    this.ObjectsId.valueChanges.subscribe((value) => {
      this.selectedObjects = this.objects
        .map((item) =>
          value.find((objectId) => objectId === item.id) ? item : null
        )
        ?.filter((item) => item !== null);

      this.changeDetectionRef.detectChanges();
    });
    this.objects$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (data) => {
        // concat objects list with new data and remove duplicates
        this.objects = [
          ...new Map(
            [...this.objects, ...(data ?? [])].map((item) => [item.id, item])
          ).values(),
        ];
        this.selectedObjects = this.objects
          .map((item) =>
            this.ObjectsId.value.find((objectId) => objectId === item.id)
              ? item
              : null
          )
          ?.filter((item) => item !== null);
        this.changeDetectionRef.detectChanges();
      },
    });
  }

  ngOnDestroy() {
    this.searchObjectChangeSubscription.unsubscribe();
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onClickHasObject() {
    this.changeDetectionRef.detectChanges();
  }

  public onSearch(value: string) {
    this.searchKeyword = value;
    this.page = 1;
    const params = {
      page: this.page,
      pageSize: this.pageSize,
      keyWord: value,
      sorts: ['-modifiedAt'],
    } as IHttpGetRequest;
    this.store.dispatch(new GetObjectGroups(params));
  }

  public loadMoreObjects() {
    this.page += 1;
    const params = {
      page: this.page,
      pageSize: this.pageSize,
      keyWord: this.searchKeyword,
      sorts: ['-modifiedAt'],
    } as IHttpGetRequest;
    this.store.dispatch(new GetMoreObjectGroups(params));
  }

  public removeObject(id: string) {
    const objectIds = this.form.get(this.formFields.objectIds.name)
      .value as string[];
    this.form.patchValue({
      objectIds: objectIds.filter((item) => item !== id),
    });
  }

  public removeAllObjects() {
    this.form.patchValue({
      objectIds: [],
    });
  }
}
