export enum ESettingFormatAdvertising {
  TYPE,
  CONTAINER_SIZE,
  SAFE_AREA,
  FILE_SIZE,
  CONTENT_FORMAT,
  BANNER_SIZE,
}

export enum FeatureType {
  EDIT,
  CREATE,
}
