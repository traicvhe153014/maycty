export enum EMoreRemarketing {
  INDEX,
  WEBSITE,
  CHOOSE,
}

export enum EUniformedUnit {
  CPC,
  CPM,
}
