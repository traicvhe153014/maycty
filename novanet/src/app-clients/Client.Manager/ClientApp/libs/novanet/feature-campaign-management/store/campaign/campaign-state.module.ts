import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CampaignService } from './campaign-state.service';
import { CampaignState } from './campaign.state';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [NgxsModule.forFeature([CampaignState]), RouterModule],
  providers: [CampaignService],
})
export class CampaignStateModule {}
