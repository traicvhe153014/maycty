export interface IBannerDetail {
  type: string;
  containerSize: string;
  safeArea: string;
  fileSize: string;
}
