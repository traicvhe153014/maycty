import { NgModule } from '@angular/core';
import { CampaignListComponent } from './campaign-list.component';
import { CommonModule } from '@angular/common';
import { DataTableModule, FormatterModule, MapObjectModule } from '@core';
import { RouterModule } from '@angular/router';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditableCampaignTitleModule } from './components';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SvgIconModule } from '@core/components/svg-icon';
import { SharedFilterModule } from '../shared-filter';
import { CampaignStatusIconModule } from '../campaign-status-icon';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { CampaignSortButtonModule } from './components/campaign-sort-button';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { DateRangeSelectedModule } from '@features/campaign-management/pipes';

@NgModule({
  declarations: [CampaignListComponent],
  imports: [
    CommonModule,
    SvgIconModule,
    DataTableModule,
    RouterModule,
    NzTableModule,
    NzSwitchModule,
    FormsModule,
    CampaignStatusIconModule,
    EditableCampaignTitleModule,
    NzDatePickerModule,
    NzSelectModule,
    NzDropDownModule,
    NzIconModule,
    NzAutocompleteModule,
    FormatterModule,
    SharedFilterModule,
    CampaignSortButtonModule,
    NzSpinModule,
    DateRangeSelectedModule,
    ReactiveFormsModule,
    MapObjectModule,
  ],
  exports: [CampaignListComponent],
})
export class CampaignListModule {}
