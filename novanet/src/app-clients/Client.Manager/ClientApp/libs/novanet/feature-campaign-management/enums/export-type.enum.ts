export enum ReportOptions {
  DetailCampaignReport = 1,
  GeneralCampaignReport,
  AdGroupReport,
  AdvertisementReport,
  OnReport,
}
