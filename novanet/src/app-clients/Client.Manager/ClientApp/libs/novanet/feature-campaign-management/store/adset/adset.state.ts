import {
  IAdsetProductGroupResponse,
  IAdsetResponse,
  IAdsetStateModel,
  IAdsetSummary,
} from './adset-state.model';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { AdsetService } from './adset-state.service';
import {
  ClearAdset,
  CreateAdset,
  CreateAdsetError,
  CreateAdsetSuccess,
  GetAdset,
  GetAdsetError,
  GetAdsetList,
  GetAdsetListError,
  GetAdsetListSuccess,
  GetAdsetSuccess,
  GetMoreAdsetList,
  GetMoreAdsetListError,
  GetMoreAdsetListSuccess,
  NavigateAdsetNextLevel,
  SelectAdset,
  UpdateAdset,
  UpdateAdsetError,
  UpdateAdsetProductGroup,
  UpdateAdsetProductGroupError,
  UpdateAdsetProductGroupSuccess,
  UpdateAdsetSuccess,
} from './adset-state.actions';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  AdvertisingState,
  ICampaignStateModel,
  NavigateCampaignNextLevel,
  SelectAdvertising,
} from '@features/campaign-management/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { session } from '@core';

@Injectable()
@State<IAdsetStateModel>({
  name: 'adset',
  defaults: {
    loading: false,
    page: 1,
    adsets: [],
    adsetSummary: undefined,
    hasMorePages: true,
    detailedAdset: undefined,
    selectedAdsetIds: [],
  },
})
export class AdsetState {
  constructor(
    private adsetService: AdsetService,
    private router: Router,
    private store: Store
  ) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(state: IAdsetStateModel): IAdsetResponse[] {
    return state.adsets;
  }

  @Selector()
  public static getLoading(state: IAdsetStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getPage(state: IAdsetStateModel): number {
    return state.page;
  }

  @Selector()
  public static getSummary(state: IAdsetStateModel): IAdsetSummary {
    return state.adsetSummary;
  }

  @Selector()
  public static getSelected(state: IAdsetStateModel): string[] {
    return state.selectedAdsetIds;
  }

  @Selector()
  public static getNavigatedAdsetId(
    state: IAdsetStateModel
  ): string | undefined {
    return state.navigatedAdsetId;
  }

  /**
   * Actions
   */
  @Action(GetAdsetList)
  public getList(
    ctx: StateContext<IAdsetStateModel>,
    { params }: GetAdsetList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.adsetService
      .getList({
        ...params,
        withSummary: params.withSummary ?? true,
        withReport: params.withReport ?? true,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetAdsetListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetAdsetListError(err)))
      );
  }

  @Action(GetAdsetListSuccess)
  public getListSuccess(
    ctx: StateContext<IAdsetStateModel>,
    { response, params }: GetAdsetListSuccess
  ) {
    const newPage =
      response.data.length === 0 && params.page !== 1
        ? params.page - 1
        : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    // ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      adsets: response.data,
      adsetSummary: response.summary,
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetAdsetListError)
  public getListError(
    ctx: StateContext<IAdsetStateModel>,
    { error }: GetAdsetListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetMoreAdsetList)
  public getMoreList(
    ctx: StateContext<IAdsetStateModel>,
    { params }: GetMoreAdsetList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.adsetService
      .getList({
        ...params,
        withSummary: false,
        withReport: params.withReport ?? true,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetMoreAdsetListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetMoreAdsetListError(err)))
      );
  }

  @Action(GetMoreAdsetListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<IAdsetStateModel>,
    { response, params }: GetMoreAdsetListSuccess
  ) {
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    const currentAdsets = ctx.getState().adsets;
    ctx.patchState({
      adsets: [...currentAdsets, ...response.data],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreAdsetListError)
  public getMoreListError(
    ctx: StateContext<IAdsetStateModel>,
    { error }: GetMoreAdsetListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetAdset)
  public get(ctx: StateContext<IAdsetStateModel>, { id }: GetAdset) {
    ctx.patchState({ loading: true });
    return this.adsetService.getById(id).pipe(
      map((response) => ctx.dispatch(new GetAdsetSuccess(response))),
      catchError((err) => ctx.dispatch(new GetAdsetError(err)))
    );
  }

  @Action(GetAdsetSuccess)
  public getSuccess(
    ctx: StateContext<IAdsetStateModel>,
    { adset }: GetAdsetSuccess
  ) {
    ctx.patchState({
      loading: false,
      detailedAdset: adset,
    });
  }

  @Action(GetAdsetError)
  public getError(
    ctx: StateContext<IAdsetStateModel>,
    { error }: GetAdsetError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(CreateAdset)
  public create(ctx: StateContext<IAdsetStateModel>, { payload }: CreateAdset) {
    return this.adsetService.create(payload).pipe(
      map((response) => ctx.dispatch(new CreateAdsetSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateAdsetError(error)))
    );
  }

  @Action(CreateAdsetSuccess)
  public createSuccess(
    ctx: StateContext<IAdsetStateModel>,
    { adset }: CreateAdsetSuccess
  ) {
    this.router
      .navigate(['/campaign/add-advertising'], {
        queryParams: { adsetId: adset.id },
      })
      .then();
    return adset;
  }

  @Action(CreateAdsetError)
  public createError(
    ctx: StateContext<IAdsetStateModel>,
    { error }: CreateAdsetError
  ) {
    return throwError(error);
  }

  @Action(UpdateAdset)
  public update(ctx: StateContext<IAdsetStateModel>, { payload }: UpdateAdset) {
    return this.adsetService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateAdsetSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateAdsetError(error)))
    );
  }

  @Action(UpdateAdsetSuccess)
  public updateSuccess(
    ctx: StateContext<IAdsetStateModel>,
    { adset }: UpdateAdsetSuccess
  ) {
    const { adsets } = ctx.getState();
    const updatedAdsets = adsets.map((item) =>
      item.id !== adset.id
        ? item
        : {
            ...item,
            name: adset.name,
            isActive: adset.isActive,
            status: adset.status,
          }
    );
    ctx.patchState({ adsets: updatedAdsets });
  }

  @Action(UpdateAdsetError)
  public updateError(
    ctx: StateContext<IAdsetStateModel>,
    { error }: UpdateAdsetError
  ) {
    return throwError(error);
  }

  @Action(NavigateAdsetNextLevel)
  public navigateAdsetNextLevel(
    ctx: StateContext<IAdsetStateModel>,
    { adsetId }: NavigateAdsetNextLevel
  ) {
    ctx.patchState({
      navigatedAdsetId: adsetId,
      selectedAdsetIds: [adsetId],
    });
    session.set('tableAdsetSelected', [adsetId]);
  }

  @Action(UpdateAdsetProductGroup)
  public updateProductGroup(
    ctx: StateContext<IAdsetStateModel>,
    { payload }: UpdateAdsetProductGroup
  ) {
    return this.adsetService.updateProductGroup(payload).pipe(
      map((response) =>
        ctx.dispatch(new UpdateAdsetProductGroupSuccess(response))
      ),
      catchError((error) =>
        ctx.dispatch(new UpdateAdsetProductGroupError(error))
      )
    );
  }

  @Action(UpdateAdsetProductGroupSuccess)
  public updateProductGroupSuccess(
    ctx: StateContext<IAdsetStateModel>,
    { response }: UpdateAdsetProductGroupSuccess
  ) {
    ctx.setState(
      patch({
        adsets: updateItem<IAdsetResponse>(
          (x) => x.id === response.advertisingSetId,
          patch({
            status: response.advertisingSetStatus,
            productGroupData: updateItem<IAdsetProductGroupResponse>(
              (z) => z.id === response.productGroupId,
              patch({
                status: response.status,
                isActive: response.isActive,
              })
            ),
          })
        ),
      })
    );
  }

  @Action(UpdateAdsetProductGroupError)
  public updateProductGroupError(
    ctx: StateContext<IAdsetStateModel>,
    { error }: UpdateAdsetProductGroupError
  ) {
    return throwError(error);
  }

  @Action(SelectAdset)
  public select(
    ctx: StateContext<IAdsetStateModel>,
    { adsetIds, currentLevelOnly }: SelectAdset
  ) {
    ctx.patchState({
      selectedAdsetIds: adsetIds,
    });
    if (currentLevelOnly) {
      return;
    }
    // update selected ad
    const selectedAds =
      this.store.selectSnapshot(AdvertisingState.getSelected) ?? [];
    const ads = this.store.selectSnapshot(AdvertisingState.getList) ?? [];
    const updatedAdIds = selectedAds.filter((adsetId) => {
      const ad = ads.find((item) => item.id === adsetId);
      return adsetIds.find((adsetId) => adsetId === ad?.advertisingSetId);
    });
    session.set('tableAdSelected', updatedAdIds);
    this.store.dispatch(new SelectAdvertising(updatedAdIds));
  }

  @Action(ClearAdset)
  public clear(ctx: StateContext<IAdsetStateModel>) {
    ctx.patchState({
      adsets: [],
      adsetSummary: undefined,
      selectedAdsetIds: [],
      detailedAdset: undefined,
      hasMorePages: false,
      page: 1,
      loading: false,
    });
  }
}
