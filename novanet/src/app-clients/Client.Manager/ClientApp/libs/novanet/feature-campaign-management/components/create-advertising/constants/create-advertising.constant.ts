import { ISettingTemplate } from './setting-template.model';
import { IFormFieldErrors, IFormFields } from '@models';
import { urlBackUpPattern, urlPattern } from '@shared/sharing/constants';
import { ITemplate } from '../models';
import { ETemplateType } from '@features/campaign-management/enums';

export const AdvertisingSetting = 'AdvertisingSetting';

export const CreateAdvertisingFormFields: IFormFields = {
  name: {
    name: 'name',
    validationParams: {
      maxLength: 300,
      required: true,
    },
  },
  banner: {
    name: 'banner',
  },
  advertisingSetId: {
    name: 'advertisingSetId',
    validationParams: {
      required: true,
    },
  },
  redirectLink: {
    name: 'redirectLink',
    validationParams: {
      pattern: urlBackUpPattern,
      maxLength: 2048,
    },
  },
};

export const CreateAdvertisingFormFieldErrors: IFormFieldErrors = {
  name: [
    {
      type: 'required',
      message: 'Thiếu tên tin quảng cáo',
    },
    {
      type: 'maxlength',
      message: 'Số lượng kí tự tối đa là 300 kí tự',
    },
  ],
  redirectLink: [
    {
      type: 'pattern',
      message: 'Không đúng định dạng URL',
    },
    {
      type: 'required',
      message: 'Thiếu URL',
    },
    {
      type: 'maxlength',
      message: 'Số lượng tối đa là 2048 kí tự',
    },
  ],
};

export const DefaultSettingTemplates = [
  {
    templateId: ETemplateType.MobileBannerCard,
    productName: true,
    bannerImageUrl: 'https://i.imgur.com/0EeHACx.png',
    redirectUrl: '/xxxxx',
    brandName: 'Novaon',
    brandAvatar:
      'https://www.looper.com/img/gallery/the-attack-on-titan-mikasa-moment-that-means-more-than-you-think/l-intro-1642547357.jpg',
    description:
      'What is Lorem Ipsum?\n' +
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    promotion: true,
    freeDelivery: true,
    templateType: true,
    imageLink: 'https://i.imgur.com/0EeHACx.png',
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  {
    templateId: ETemplateType.FlyingCarpet,
    productName: true,
    bannerImageUrl: 'https://i.imgur.com/EltBTbM.png',
    redirectUrl: '/xxxxx',
    brandName: 'Novaon',
    brandAvatar: 'https://imgur.com/EltBTbM',
    description:
      'What is Lorem Ipsum?\n' +
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    promotion: true,
    freeDelivery: true,
    templateType: true,
    imageLink: 'https://i.imgur.com/EltBTbM.png',
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  {
    templateId: ETemplateType.PostInRead,
    productName: true,
    bannerImageUrl: 'https://i.imgur.com/caJYpRL.png',
    redirectUrl: '/xxxxx',
    brandName: 'Novaon',
    brandAvatar: 'https://i.imgur.com/caJYpRL.png',
    description:
      'What is Lorem Ipsum?\n' +
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    promotion: true,
    freeDelivery: true,
    templateType: true,
    imageLink: 'https://i.imgur.com/caJYpRL.png',
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  {
    templateId: ETemplateType.InteractiveBanner,
    productName: true,
    bannerImageUrl: 'https://i.imgur.com/rzpXXiU.png',
    redirectUrl: '/xxxxx',
    brandName: 'Novaon',
    brandAvatar:
      'https://www.looper.com/img/gallery/the-attack-on-titan-mikasa-moment-that-means-more-than-you-think/l-intro-1642547357.jpg',
    description:
      'What is Lorem Ipsum?\n' +
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    promotion: true,
    freeDelivery: true,
    templateType: true,
    imageLink: 'https://i.imgur.com/rzpXXiU.png',
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  {
    templateId: ETemplateType.InReadEcommerce,
    productName: true,
    bannerImageUrl: 'https://i.imgur.com/pL1CGbn.png',
    redirectUrl: '/xxxxx',
    brandName: 'Novaon',
    brandAvatar:
      'https://www.looper.com/img/gallery/the-attack-on-titan-mikasa-moment-that-means-more-than-you-think/l-intro-1642547357.jpg',
    description:
      'What is Lorem Ipsum?\n' +
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    promotion: true,
    freeDelivery: true,
    templateType: true,
    imageLink: 'https://i.imgur.com/pL1CGbn.png',
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
  {
    templateId: ETemplateType.CatfishEcom,
    productName: true,
    bannerImageUrl: 'https://i.imgur.com/7Y64fhh.png',
    redirectUrl: '/xxxxx',
    brandName: 'Novaon',
    brandAvatar:
      'https://www.looper.com/img/gallery/the-attack-on-titan-mikasa-moment-that-means-more-than-you-think/l-intro-1642547357.jpg',
    description:
      'What is Lorem Ipsum?\n' +
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    promotion: true,
    freeDelivery: true,
    templateType: true,
    expandedContentUrl: '',
    imageLink: 'https://i.imgur.com/0EeHACx.png',
    products: [
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 6 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 26e3,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 8 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 12e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 7 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 3e4,
        price: 3e5,
      },
      {
        title: 'Bánh Lạc Lễ hộp Tinh Tế Hộp 9 Bánh',
        productImageUrl: 'https://i.imgur.com/hMvDboy.png',
        redirectUrl: '/',
        discountedPrice: 9e4,
        price: 3e5,
      },
    ],
  },
] as ISettingTemplate[];

export const TemplatesPreview = [
  {
    id: ETemplateType.MobileBannerCard,
    template: '/assets/templates/mobile-banner-card/mobile-banner-card.html',
    activated: true,
  },
  {
    id: ETemplateType.InteractiveBanner,
    template: '/assets/templates/interactive-banner/interactive-banner.html',
    activated: false,
  },
  {
    id: ETemplateType.FlyingCarpet,
    template: '/assets/templates/flying-carpet/flying-carpet.html',
    activated: false,
  },
  {
    id: ETemplateType.InReadEcommerce,
    template: '/assets/templates/inread-ecommerce/inread-ecommerce.html',
    activated: false,
  },
  {
    id: ETemplateType.PostInRead,
    template: '/assets/templates/post-in-read/post-in-read.html',
    activated: false,
  },
  {
    id: ETemplateType.CatfishEcom,
    template: '/assets/templates/catfish-ecom/catfish-ecom.html',
    activated: false,
  },
] as ITemplate[];
