import { SvgIconModule } from '@core/components/svg-icon';
import { NgModule } from '@angular/core';
import { SharedFilterComponent } from './shared-filter.component';
import { CommonModule } from '@angular/common';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';

@NgModule({
  declarations: [SharedFilterComponent],
  imports: [
    CommonModule,
    NzDatePickerModule,
    NzSelectModule,
    NzDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NovanetInputModule,
    SvgIconModule,
  ],
  exports: [SharedFilterComponent],
})
export class SharedFilterModule {}
