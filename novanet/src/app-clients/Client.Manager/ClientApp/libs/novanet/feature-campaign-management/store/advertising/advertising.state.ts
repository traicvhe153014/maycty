import {
  IAdvertisingResponse,
  IAdvertisingStateModel,
  IAdvertisingSummary,
  IAdvertisingTemplate,
} from './advertising-state.model';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AdvertisingService } from './advertising-state.service';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  ClearAdvertising,
  CreateAdvertising,
  CreateAdvertisingError,
  CreateAdvertisingSuccess,
  GetAdvertisingList,
  GetAdvertisingListError,
  GetAdvertisingListSuccess,
  GetMoreAdvertisingList,
  GetMoreAdvertisingListError,
  GetMoreAdvertisingListSuccess,
  ListAdvertisingTemplate,
  ListAdvertisingTemplateError,
  ListAdvertisingTemplateSuccess,
  SelectAdvertising,
  UpdateAdvertising,
  UpdateAdvertisingError,
  UpdateAdvertisingSuccess,
} from './advertising-state.actions';

@Injectable()
@State<IAdvertisingStateModel>({
  name: 'Advertising',
  defaults: {
    loading: false,
    page: 1,
    advertisings: [],
    advertisingSummary: undefined,
    hasMorePages: true,
    selectedAdvertisingIds: [],
    loadingTemplate: false,
    advertisingTemplates: [],
  },
})
export class AdvertisingState {
  constructor(
    private advertisingService: AdvertisingService,
    private router: Router
  ) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(state: IAdvertisingStateModel): IAdvertisingResponse[] {
    return state.advertisings;
  }

  @Selector()
  public static getLoading(state: IAdvertisingStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getPage(state: IAdvertisingStateModel): number {
    return state.page;
  }

  @Selector()
  public static getSummary(state: IAdvertisingStateModel): IAdvertisingSummary {
    return state.advertisingSummary;
  }

  @Selector()
  public static getSelected(state: IAdvertisingStateModel): string[] {
    return state.selectedAdvertisingIds;
  }

  @Selector()
  public static getListTemplates(
    state: IAdvertisingStateModel
  ): IAdvertisingTemplate[] {
    return state.advertisingTemplates;
  }

  @Selector()
  public static getGroupedTemplates(state: IAdvertisingStateModel): {
    [key: string]: IAdvertisingTemplate[];
  } {
    return state.advertisingTemplates.groupBy(
      (item) => `${item.width}x${item.height} - ${item.templatePosition}`
    );
  }

  @Selector()
  public static getTemplateSizes(
    state: IAdvertisingStateModel
  ): { label: string; value: string }[] {
    return Object.keys(
      state.advertisingTemplates.groupBy(
        (item) => `${item.width}x${item.height} - ${item.templatePosition}`
      )
    )
      .map((key) => ({
        label: key,
        value: key,
      }))
      .sort((a, b) => a.value.localeCompare(b.value));
  }

  @Selector()
  public static getLoadingTemplate(state: IAdvertisingStateModel): boolean {
    return state.loadingTemplate;
  }

  /**
   * Actions
   */
  @Action(GetAdvertisingList)
  public getList(
    ctx: StateContext<IAdvertisingStateModel>,
    { params }: GetAdvertisingList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.advertisingService
      .getList({
        ...params,
        withSummary: params.withSummary ?? true,
        withReport: params.withReport ?? true,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetAdvertisingListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetAdvertisingListError(err)))
      );
  }

  @Action(GetAdvertisingListSuccess)
  public getListSuccess(
    ctx: StateContext<IAdvertisingStateModel>,
    { response, params }: GetAdvertisingListSuccess
  ) {
    const newPage =
      response.data.length === 0 && params.page !== 1
        ? params.page - 1
        : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    // ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      advertisings: response.data,
      advertisingSummary: response.summary,
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetAdvertisingListError)
  public getListError(
    ctx: StateContext<IAdvertisingStateModel>,
    { error }: GetAdvertisingListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetMoreAdvertisingList)
  public getMoreList(
    ctx: StateContext<IAdvertisingStateModel>,
    { params }: GetMoreAdvertisingList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.advertisingService
      .getList({
        ...params,
        withSummary: false,
        withReport: params.withReport ?? true,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetMoreAdvertisingListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetMoreAdvertisingListError(err)))
      );
  }

  @Action(GetMoreAdvertisingListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<IAdvertisingStateModel>,
    { response, params }: GetMoreAdvertisingListSuccess
  ) {
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    const currentAdvertisings = ctx.getState().advertisings;
    ctx.patchState({
      advertisings: [...currentAdvertisings, ...response.data],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreAdvertisingListError)
  public getMoreListError(
    ctx: StateContext<IAdvertisingStateModel>,
    { error }: GetMoreAdvertisingListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(ListAdvertisingTemplate)
  public listAdvertisingTemplate(ctx: StateContext<IAdvertisingStateModel>) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loadingTemplate: true });
    return this.advertisingService.listTemplate().pipe(
      map((response) =>
        ctx.dispatch(new ListAdvertisingTemplateSuccess(response))
      ),
      catchError((err) => ctx.dispatch(new ListAdvertisingTemplateError(err)))
    );
  }

  @Action(ListAdvertisingTemplateSuccess)
  public listAdvertisingTemplateSuccess(
    ctx: StateContext<IAdvertisingStateModel>,
    { response }: ListAdvertisingTemplateSuccess
  ) {
    ctx.patchState({
      loadingTemplate: false,
      advertisingTemplates: response,
    });
  }

  @Action(ListAdvertisingTemplateError)
  public listAdvertisingTemplateError(
    ctx: StateContext<IAdvertisingStateModel>,
    { error }: ListAdvertisingTemplateError
  ) {
    ctx.patchState({ loadingTemplate: false });
    return throwError(error);
  }

  @Action(CreateAdvertising)
  public create(
    ctx: StateContext<IAdvertisingStateModel>,
    { payload }: CreateAdvertising
  ) {
    return this.advertisingService.create(payload).pipe(
      map((response) => ctx.dispatch(new CreateAdvertisingSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateAdvertisingError(error)))
    );
  }

  @Action(CreateAdvertisingSuccess)
  public createSuccess(
    ctx: StateContext<IAdvertisingStateModel>,
    { advertising }: CreateAdvertisingSuccess
  ) {
    this.router
      .navigate(['/campaign/management'], { queryParams: { level: 'Ad' } })
      .then();
    return advertising;
  }

  @Action(CreateAdvertisingError)
  public createError(
    ctx: StateContext<IAdvertisingStateModel>,
    { error }: CreateAdvertisingError
  ) {
    return throwError(error);
  }

  @Action(UpdateAdvertising)
  public update(
    ctx: StateContext<IAdvertisingStateModel>,
    { payload }: UpdateAdvertising
  ) {
    return this.advertisingService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateAdvertisingSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateAdvertisingError(error)))
    );
  }

  @Action(UpdateAdvertisingSuccess)
  public updateSuccess(
    ctx: StateContext<IAdvertisingStateModel>,
    { advertising }: UpdateAdvertisingSuccess
  ) {
    const { advertisings } = ctx.getState();
    const updatedAdvertisings = advertisings.map((item) =>
      item.id !== advertising.id
        ? item
        : {
            ...item,
            name: advertising.name,
            isActive: advertising.isActive,
            status: advertising.status,
          }
    );
    this.router
      .navigate(['/campaign/management'], { queryParams: { level: 'Ad' } })
      .then();
    ctx.patchState({ advertisings: updatedAdvertisings });
  }

  @Action(UpdateAdvertisingError)
  public updateError(
    ctx: StateContext<IAdvertisingStateModel>,
    { error }: UpdateAdvertisingError
  ) {
    return throwError(error);
  }

  @Action(SelectAdvertising)
  public select(
    ctx: StateContext<IAdvertisingStateModel>,
    { advertisingIds }: SelectAdvertising
  ) {
    ctx.patchState({
      selectedAdvertisingIds: advertisingIds,
    });
  }

  @Action(ClearAdvertising)
  public clear(ctx: StateContext<IAdvertisingStateModel>) {
    ctx.patchState({
      advertisings: [],
      advertisingSummary: undefined,
      selectedAdvertisingIds: [],
      hasMorePages: false,
      page: 1,
      loading: false,
    });
  }
}
