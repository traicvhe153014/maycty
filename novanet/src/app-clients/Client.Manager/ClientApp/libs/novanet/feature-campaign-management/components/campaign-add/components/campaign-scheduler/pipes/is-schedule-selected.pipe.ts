import { Pipe, PipeTransform } from '@angular/core';
import { ICampaignScheduling } from '@features/campaign-management/store/campaign';

@Pipe({ name: 'isScheduleSelected' })
export class IsScheduleSelectedPipe implements PipeTransform {
  transform(
    schedulings: Partial<ICampaignScheduling>[],
    dayOfWeek: number,
    hour: number
  ): boolean {
    return !!schedulings?.find(
      (item) => item.dayOfWeek === dayOfWeek && item.timing.getHours() === hour
    );
  }
}
