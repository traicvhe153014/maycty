import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import { AdsetAddSelectedObjectEnum } from '@features/campaign-management/components/adset-add/enums';

export const selectedObjectSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: false,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;

export const selectedObjectColumns = [
  {
    id: AdsetAddSelectedObjectEnum.INDEX,
    title: 'Stt',
    width: '50px',
    align: 'center',
  },
  {
    id: AdsetAddSelectedObjectEnum.NAME,
    title: 'Tên nhóm đối tượng',
  },
  // {
  //   id: AdsetAddSelectedObjectEnum.ESTIMATED_SCALE,
  //   title: 'Quy mô ước tính',
  //   width: '115px',
  // },
  {
    id: AdsetAddSelectedObjectEnum.ACTION,
    title: 'Thao tác',
    width: '70px',
    align: 'center',
  },
] as ISettingColumnTable<any, any>[];
