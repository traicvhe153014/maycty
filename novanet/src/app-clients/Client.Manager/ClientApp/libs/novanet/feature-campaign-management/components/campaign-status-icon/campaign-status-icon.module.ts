import { NgModule } from '@angular/core';
import { CampaignStatusIconComponent } from './campaign-status-icon.component';
import { CommonModule } from '@angular/common';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [CampaignStatusIconComponent],
  imports: [CommonModule, NzToolTipModule],
  exports: [CampaignStatusIconComponent],
})
export class CampaignStatusIconModule {}
