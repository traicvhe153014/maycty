import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AllowSizeColumn, AllowSizeSetting, zipMimeType } from './constant';
import { ESettingFormatAdvertising } from './enums';
import { ETemplateType } from '@features/campaign-management/enums';
import { ITemplateConfiguration } from '@features/campaign-management/store';
import { DefaultSettingTemplates } from '@features/campaign-management/components/create-advertising/constants';
import { isFileSizeValid } from '@core/utils';
import { Store } from '@ngxs/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';

@Component({
  selector: 'novanet-setting-format-advertising',
  templateUrl: './setting-format-advertising.component.html',
})
export class SettingFormatAdvertisingComponent {
  @Input() public height = 0;
  @Input() public width = 0;
  @Input() public dataTable = [];
  @Input() public templateType: ETemplateType;
  @Input() public templateConfiguration: ITemplateConfiguration;
  @Output() public templateConfigurationChange =
    new EventEmitter<ITemplateConfiguration>();

  public templateTypes = ETemplateType;
  public settingColumn = AllowSizeColumn;
  public settingTable = AllowSizeSetting;
  public bannerToUpload: File | null = null;
  public expandedContent: File | null = null;
  public allowSizeColumn = ESettingFormatAdvertising;

  public avatarNameError: boolean;

  constructor(private store: Store) {}

  public handleBannerInput(field: string, files: FileList) {
    if (isFileSizeValid([files.item(0)], 1024000)) {
      const fr = new FileReader();
      let height = 0;
      let width = 0;
      fr.onload = () => {
        if (
          files.item(0)?.type === zipMimeType &&
          field === 'expandedContent'
        ) {
          this.expandedContent = null;
          this.templateConfiguration.configurations.currentExpandedBanner =
            null;
          this.expandedContent = files.item(0);
          this.onConfigChange(field, this.expandedContent);
          return;
        }
        const img = new Image();
        img.onload = () => {
          width = img.width;
          height = img.height;
          const ratio = width / height;
          const currentImageRatio = this.width / this.height;
          const imageOrHtml = 375 / 810;
          if (
            field !== 'expandedContent' &&
            currentImageRatio &&
            ratio !== currentImageRatio
          ) {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.warning,
                'Cảnh báo',
                'Sai kích thước ảnh'
              )
            );
            return;
          } else if (
            field === 'expandedContent' &&
            imageOrHtml &&
            ratio !== imageOrHtml
          ) {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.warning,
                'Cảnh báo',
                'Sai kích thước ảnh'
              )
            );
            return;
          }
          if (field === 'expandedContent') {
            this.expandedContent = null;
            this.templateConfiguration.configurations.currentExpandedBanner =
              null;
            this.expandedContent = files.item(0);
            this.onConfigChange(field, this.expandedContent);
          } else {
            this.bannerToUpload = null;
            this.templateConfiguration.configurations.currentAvatar = null;
            this.templateConfiguration.configurations.currentBanner = null;
            this.bannerToUpload = files.item(0);
            this.onConfigChange(field, this.bannerToUpload);
          }
        };
        img.src = fr.result as any;
      };
      fr.readAsDataURL(files[0]);
    } else {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Cảnh báo',
          'Không tải lên dữ liệu quá 1MB'
        )
      );
    }
  }

  public clearBanner(field: string) {
    if (field === 'expandedContent') {
      this.expandedContent = null;
      this.onConfigChange(field, this.expandedContent);
      this.templateConfiguration.configurations.currentExpandedBanner = null;
      return;
    }

    this.bannerToUpload = null;
    this.templateConfiguration.configurations.currentAvatar = null;
    this.templateConfiguration.configurations.currentBanner = null;

    this.onConfigChange(field, this.bannerToUpload);
  }

  public onConfigChange(field: string, value: any) {
    if (field === 'avatarName') {
      if (value.length > 45) {
        this.avatarNameError = true;
        return;
      }
      this.avatarNameError = false;
    }

    const template = DefaultSettingTemplates.find(
      (x) => x.templateId === this.templateType
    );
    let updatedConfig: ITemplateConfiguration = {} as ITemplateConfiguration;
    if (
      field === 'brandAvatar' ||
      field === 'bannerImage' ||
      field === 'expandedContent'
    ) {
      if (field === 'bannerImage') {
        updatedConfig = {
          ...this.templateConfiguration,
          configurations: {
            ...this.templateConfiguration.configurations,
            imageLink: value
              ? window.URL.createObjectURL(value)
              : template.bannerImageUrl,
            [field]: value,
          },
        };
      }
      if (field === 'brandAvatar') {
        updatedConfig = {
          ...this.templateConfiguration,
          configurations: {
            ...this.templateConfiguration.configurations,
            imageLink: value
              ? window.URL.createObjectURL(value)
              : template.brandAvatar,
            [field]: value,
          },
        };
      }
      if (field === 'expandedContent') {
        updatedConfig = {
          ...this.templateConfiguration,
          configurations: {
            ...this.templateConfiguration.configurations,
            expandedContentUrl: value
              ? window.URL.createObjectURL(value)
              : template.expandedContentUrl,
            [field]: value,
          },
        };
      }
    } else {
      updatedConfig = {
        ...this.templateConfiguration,
        configurations: {
          ...this.templateConfiguration.configurations,
          [field]: value,
        },
      };
    }
    if (updatedConfig.configurations?.postContent?.length > 150) {
      updatedConfig.configurations.postContent =
        updatedConfig.configurations.postContent.substring(0, 150) + '...';
    }
    this.templateConfigurationChange.emit(updatedConfig);
  }
}
