import { NgModule } from '@angular/core';
import { SettingFormatAdvertisingComponent } from './setting-format-advertising';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from '@core';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { SvgIconModule } from '@core/components/svg-icon';

const MODULES: any[] = [
  FormsModule,
  NzSwitchModule,
  DataTableModule,
  CommonModule,
  NovanetInputModule,
  SvgIconModule,
];
const COMPONENTS: any[] = [SettingFormatAdvertisingComponent];

@NgModule({
  imports: [...MODULES],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class CreateAdvertisingComponentsModule {}
