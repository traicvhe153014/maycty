import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  ICampaignCreateRequest,
  ICampaignGetResponse,
  ICampaignListRequest,
  ICampaignListResponse,
  ICampaignResponse,
  ICampaignUpdateRequest,
} from './campaign-state.model';

const CampaignUrls = {
  getList: `campaign/list`,
  exportDataToExcel: `campaign/exportDataToExcel`,
  get: 'campaign/get',
  create: 'campaign/create',
  update: 'campaign/update',
};

@Injectable()
export class CampaignService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public getList(
    params: ICampaignListRequest
  ): Observable<ICampaignListResponse> {
    const processedParams = {
      ...params,
      startDate: params.startDate?.toISOString(),
      endDate: params.endDate?.toISOString(),
    };
    return this.httpClient
      .post<ISuccessHttpResponse<ICampaignListResponse>>(
        this.createUrl(CampaignUrls.getList),
        processedParams
      )
      .pipe(map((response) => response.data));
  }

  public get(campaignId: string): Observable<ICampaignGetResponse> {
    const params = new HttpParams({
      fromObject: {
        id: campaignId,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<ICampaignGetResponse>>(
        this.createUrl(CampaignUrls.get),
        { params }
      )
      .pipe(map((response) => response.data));
  }

  public create(
    payload: ICampaignCreateRequest
  ): Observable<ICampaignResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<ICampaignResponse>>(
        this.createUrl(CampaignUrls.create),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public update(
    payload: Partial<ICampaignUpdateRequest>
  ): Observable<ICampaignResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<ICampaignResponse>>(
        this.createUrl(CampaignUrls.update),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public exportExcel(params: ICampaignListRequest) {
    const processedParams = {
      ...params,
      startDate: params.startDate?.toISOString(),
      endDate: params.endDate?.toISOString(),
    };
    return this.httpClient.post<any>(
      this.createUrl(CampaignUrls.exportDataToExcel),
      processedParams
    );
  }
}
