import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAdvertisingComponent } from './create-advertising.component';

const routes: Routes = [
  {
    path: '',
    component: CreateAdvertisingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateAdvertisingRouting {}
