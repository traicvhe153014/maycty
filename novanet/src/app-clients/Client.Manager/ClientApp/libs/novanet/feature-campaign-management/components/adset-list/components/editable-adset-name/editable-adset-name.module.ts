import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditableAdsetNameComponent } from './editable-adset-name.component';
import { FormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { RouterModule } from '@angular/router';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [EditableAdsetNameComponent],
  imports: [
    CommonModule,
    SvgIconModule,
    FormsModule,
    RouterModule,
    NzToolTipModule,
  ],
  exports: [EditableAdsetNameComponent],
})
export class EditableAdsetNameModule {}
