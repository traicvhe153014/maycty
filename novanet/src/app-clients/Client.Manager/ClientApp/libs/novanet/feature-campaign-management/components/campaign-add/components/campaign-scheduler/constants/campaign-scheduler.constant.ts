import { EDayOfWeek } from '@shared/enums/day-of-week.enum';

export const hours = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
  22, 23,
];
export const dayOfWeeks = [
  {
    value: EDayOfWeek.Monday,
    label: 'Thứ 2',
  },
  {
    value: EDayOfWeek.Tuesday,
    label: 'Thứ 3',
  },
  {
    value: EDayOfWeek.Wednesday,
    label: 'Thứ 4',
  },
  {
    value: EDayOfWeek.Thursday,
    label: 'Thứ 5',
  },
  {
    value: EDayOfWeek.Friday,
    label: 'Thứ 6',
  },
  {
    value: EDayOfWeek.Saturday,
    label: 'Thứ 7',
  },
  {
    value: EDayOfWeek.Sunday,
    label: 'CN',
  },
];
