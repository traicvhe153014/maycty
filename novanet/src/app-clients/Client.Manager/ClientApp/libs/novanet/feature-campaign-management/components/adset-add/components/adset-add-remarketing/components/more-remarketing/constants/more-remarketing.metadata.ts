import { ISettingColumnTable } from '@data-table/models';
import { EMoreRemarketing } from '../enums/more-remarketing.enum';

export const WebsitesSettingColumn = [
  {
    id: EMoreRemarketing.INDEX,
    title: 'STT',
    width: '10%',
  },
  {
    id: EMoreRemarketing.WEBSITE,
    title: 'Danh sách Website',
    width: '70%',
  },
  {
    id: EMoreRemarketing.CHOOSE,
    title: 'Chọn',
    width: '10%',
    align: 'center',
  },
] as ISettingColumnTable<any, any>[];
