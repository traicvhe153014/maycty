import { NgModule } from '@angular/core';
import { CampaignAddEcommerceComponent } from './campaign-add-ecommerce.component';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingIconModule } from '@core';
import { CampaignSchedulerModule } from '../campaign-scheduler';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [CampaignAddEcommerceComponent],
  imports: [
    CommonModule,
    FormsModule,
    NovanetInputModule,
    NzDatePickerModule,
    NzSwitchModule,
    SvgIconModule,
    ReactiveFormsModule,
    CampaignSchedulerModule,
    NzSelectModule,
    LoadingIconModule,
    NzModalModule,
    RouterModule,
  ],
  exports: [CampaignAddEcommerceComponent],
})
export class CampaignAddEcommerceModule {}
