import {
  EWebsiteCondition,
  IAdsetByIdResponse,
} from '@features/campaign-management/store';
import {
  EAdsetAddRemarketingOption,
  EUniformedUnit,
} from '@features/campaign-management/components';

export function convertAdsetToForm(adset: IAdsetByIdResponse): {
  [key: string]: any;
} {
  return {
    ageIds: adset.advertisingSetAges?.map((item) => item.ageId) ?? [],
    categoryIds:
      adset.advertisingSetCategories?.map((item) => item.categoryId) ?? [],
    locationIds:
      adset.advertisingSetLocations?.map((item) => item.locationId) ?? [],
    genderIds: adset.advertisingSetGenders?.map((item) => item.genderId) ?? [],
    hasProhibitedCategories: adset.hasProhibitedCategories,
    prohibitedCategoryIds:
      adset.advertisingSetProhibitedCategories?.map(
        (item) => item.prohibitedCategoryId
      ) ?? [],
    hasObject: adset.hasObject ?? false,
    objectIds: adset.advertisingSetObjects?.map((item) => item.objectId) ?? [],
    productGroupIds:
      adset.advertisingSetProductGroups?.map((item) => item.productGroupId) ??
      [],
    applyAllProductGroups: adset.applyAllProductGroups ?? true,
    uniformed: adset.uniformed ?? false,
    uniformedPrice: adset.uniformedPrice ?? 0,
    uniformedUnit: adset.uniformedUnit ?? EUniformedUnit.CPM,
    selectedWebsites:
      adset.advertisingSetWebsites
        ?.filter((item) => item.websiteCondition === EWebsiteCondition.Include)
        ?.map((item) => item.websiteId) ?? [],
    excludedWebsite: adset.excludedWebsite,
    excludedWebsites:
      adset.advertisingSetWebsites
        ?.filter((item) => item.websiteCondition === EWebsiteCondition.Exclude)
        ?.map((item) => item.websiteId) ?? [],
    targetingMarketingByProduct: adset.targetingMarketingByProduct,
    adImpressions: adset.adImpressions ?? 0,
    remarketingTime: adset.remarketingTime ?? 0,
    timeOnDisplay: adset.timeOnDisplay ?? 0,
    clickedAdvertising: adset.clickedAdvertising,
    viewer: adset.viewer,
    viewedMax: adset.viewedMax ?? 0,
    remarketingType:
      adset.remarketingType ?? EAdsetAddRemarketingOption.NO_MORE_MARKETING,
    scheduledAllTime: adset.scheduledAllTime ?? true,
  };
}
