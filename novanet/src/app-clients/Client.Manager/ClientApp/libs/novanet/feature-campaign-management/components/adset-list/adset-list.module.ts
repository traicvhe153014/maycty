import { NgModule } from '@angular/core';
import { AdsetListComponent } from './adset-list.component';
import { CommonModule } from '@angular/common';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { SharedFilterModule } from '../shared-filter';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { DataTableModule, FormatterModule, MapObjectModule } from '@core';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { FormsModule } from '@angular/forms';
import { EditableAdsetNameModule } from './components/editable-adset-name';
import { CampaignStatusIconModule } from '../campaign-status-icon';
import { RouterModule } from '@angular/router';
import { NovanetInputModule } from '@shared/custom-input';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';

@NgModule({
  declarations: [AdsetListComponent],
  imports: [
    CommonModule,
    NzDatePickerModule,
    NzSelectModule,
    NzDropDownModule,
    SharedFilterModule,
    NzIconModule,
    DataTableModule,
    NzSwitchModule,
    SvgIconModule,
    FormatterModule,
    FormsModule,
    EditableAdsetNameModule,
    CampaignStatusIconModule,
    RouterModule,
    NovanetInputModule,
    SwitchNoAnimationModule,
    NzSpinModule,
    MapObjectModule,
  ],
  exports: [AdsetListComponent],
})
export class AdsetListModule {}
