import {
  ICampaignCreateRequest,
  ICampaignGetResponse,
  ICampaignListRequest,
  ICampaignListResponse,
  ICampaignResponse,
  ICampaignUpdateRequest,
} from './campaign-state.model';

const enum CampaignActions {
  GetCampaignList = '[Campaign] Get List',
  GetCampaignListSuccess = '[Campaign] Get List Success',
  GetCampaignListError = '[Campaign] Get List Error',
  GetMoreCampaignList = '[Campaign] Get More List',
  GetMoreCampaignListSuccess = '[Campaign] Get More List Success',
  GetMoreCampaignListError = '[Campaign] Get More List Error',
  GetCampaign = '[Campaign] Get',
  GetCampaignSuccess = '[Campaign] Get Success',
  GetCampaignError = '[Campaign] Get Error',
  CreateCampaign = '[Campaign] Create',
  CreateCampaignSuccess = '[Campaign] Create Success',
  CreateCampaignError = '[Campaign] Create Error',
  UpdateCampaign = '[Campaign] Update',
  UpdateCampaignSuccess = '[Campaign] Update Success',
  UpdateCampaignError = '[Campaign] Update Error',
  SelectCampaign = '[Campaign] Select',
  ClearCampaign = '[Campaign] Clear',
  NavigateCampaignNextLevel = '[Campaign] Navigate Next Level',
}

export class GetCampaignList {
  public static readonly type = CampaignActions.GetCampaignList;

  constructor(public params: ICampaignListRequest) {}
}

export class GetCampaignListSuccess {
  public static readonly type = CampaignActions.GetCampaignListSuccess;

  constructor(
    public response: ICampaignListResponse,
    public params: ICampaignListRequest
  ) {}
}

export class GetCampaignListError {
  public static readonly type = CampaignActions.GetCampaignListError;

  constructor(public error: any) {}
}

export class GetMoreCampaignList {
  public static readonly type = CampaignActions.GetMoreCampaignList;

  constructor(public params: ICampaignListRequest) {}
}

export class GetMoreCampaignListSuccess {
  public static readonly type = CampaignActions.GetMoreCampaignListSuccess;

  constructor(
    public response: ICampaignListResponse,
    public params: ICampaignListRequest
  ) {}
}

export class GetMoreCampaignListError {
  public static readonly type = CampaignActions.GetMoreCampaignListError;

  constructor(public error: any) {}
}

export class GetCampaign {
  public static readonly type = CampaignActions.GetCampaign;

  constructor(public campaignId: string) {}
}

export class GetCampaignSuccess {
  public static readonly type = CampaignActions.GetCampaignSuccess;

  constructor(public campaign: ICampaignGetResponse) {}
}

export class GetCampaignError {
  public static readonly type = CampaignActions.GetCampaignError;

  constructor(public error: any) {}
}

export class CreateCampaign {
  public static readonly type = CampaignActions.CreateCampaign;

  constructor(public payload: ICampaignCreateRequest) {}
}

export class CreateCampaignSuccess {
  public static readonly type = CampaignActions.CreateCampaignSuccess;

  constructor(public campaign: ICampaignResponse) {}
}

export class CreateCampaignError {
  public static readonly type = CampaignActions.CreateCampaignError;

  constructor(public error: any) {}
}

export class UpdateCampaign {
  public static readonly type = CampaignActions.UpdateCampaign;

  constructor(public payload: Partial<ICampaignUpdateRequest>) {}
}

export class UpdateCampaignSuccess {
  public static readonly type = CampaignActions.UpdateCampaignSuccess;

  constructor(public campaign: ICampaignResponse) {}
}

export class UpdateCampaignError {
  public static readonly type = CampaignActions.UpdateCampaignError;

  constructor(public error: any) {}
}

export class SelectCampaign {
  public static readonly type = CampaignActions.SelectCampaign;

  constructor(public campaignIds: string[], public currentLevelOnly = false) {}
}

export class ClearCampaign {
  public static readonly type = CampaignActions.ClearCampaign;

  constructor() {}
}

export class NavigateCampaignNextLevel {
  public static readonly type = CampaignActions.NavigateCampaignNextLevel;

  constructor(public campaignId: string) {}
}
