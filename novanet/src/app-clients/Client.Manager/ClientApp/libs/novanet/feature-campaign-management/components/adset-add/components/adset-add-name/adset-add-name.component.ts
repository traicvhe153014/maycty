import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { IFormFieldErrors, IFormFields } from '@models';
import { Select, Store } from '@ngxs/store';
import {
  AdsetService,
  AdsetState,
  GetAdsetList,
  GetMoreAdsetList,
  IAdsetByIdResponse,
  IAdsetResponse,
} from '@features/campaign-management/store';
import { Observable, Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import { shouldLoadMore } from '@core/utils';
import { convertAdsetToForm } from '@features/campaign-management/utils';
import * as dayjs from 'dayjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'novanet-adset-add-name',
  templateUrl: './adset-add-name.component.html',
})
export class AdsetAddNameComponent implements OnInit, OnDestroy {
  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;
  @Input() formType: string; // add | edit
  @Input() currentAdset: IAdsetByIdResponse | undefined;
  @Input() scrollUpdate: Subject<void>;
  @Output() public clearForm = new EventEmitter<void>();

  public useTemplate = false;
  public templateAdsetId: string = undefined;
  public templateAdsetVisible = false;
  public campaignId: string = undefined;
  public form: FormGroup;

  public readonly pageSize = 24;
  @Select(AdsetState.getList) adsets$: Observable<IAdsetResponse[]>;
  @Select(AdsetState.getPage) adsetCurrentPage$: Observable<number>;
  private destroy$ = new Subject<void>();
  private formUpdated$ = new Subject<void>();

  constructor(
    private rootFormGroup: FormGroupDirective,
    private store: Store,
    private adsetService: AdsetService,
    private changeDetectorRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute
  ) {}

  public getAdsetTemplateName$(id: string): Observable<string> {
    return this.adsets$.pipe(
      map((adsets) => adsets.find((adset) => adset.id === id)?.name)
    );
  }

  ngOnInit() {
    this.campaignId =
      this.activatedRoute.snapshot.queryParamMap.get('campaignId');
    this.form = this.rootFormGroup.control;
    this.adsetCurrentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.dispatchGetAdsetList(currentPage);
      },
    });
    this.scrollUpdate.pipe(takeUntil(this.destroy$)).subscribe(() => {
      if (this.templateAdsetVisible) {
        this.templateAdsetVisible = false;
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.formUpdated$.complete();
  }

  public onUseTemplateChange(event: boolean) {
    this.useTemplate = event;
    this.changeDetectorRef.detectChanges();
  }

  public onTemplateAdsetIdChange(event: string) {
    this.templateAdsetId = event;
  }

  public applyTemplateAdset() {
    this.formUpdated$.next();
    if (this.templateAdsetId) {
      this.adsetService.getById(this.templateAdsetId).subscribe({
        next: (adset) => {
          this.templateAdsetVisible = false;
          const formValue = convertAdsetToForm(adset);
          if (
            this.formType === 'edit' &&
            dayjs(this.currentAdset?.campaign?.ecommerceStartDate).isBefore(
              dayjs()
            )
          ) {
            formValue.uniformed = this.currentAdset?.uniformed;
            formValue.uniformedUnit = this.currentAdset?.uniformedUnit;
            formValue.uniformedPrice = this.currentAdset?.uniformedPrice;
            formValue.applyAllProductGroups =
              this.currentAdset?.applyAllProductGroups;
            formValue.productGroupIds =
              this.currentAdset?.advertisingSetProductGroups?.map(
                (item) => item.productGroupId
              ) ?? [];
          }
          this.form.patchValue(formValue);
          this.adsetService.applySelectedWebsiteTemplate$.next(
            formValue.selectedWebsites
          );
          this.form.valueChanges
            .pipe(takeUntil(this.formUpdated$), take(1))
            .subscribe({
              next: (value) => {
                this.templateAdsetId = '';
              },
            });
        },
      });
    }
  }

  public removeDropdown(): void {
    this.onTemplateAdsetIdChange('');
    this.clearForm.emit();
    this.adsetService.applySelectedWebsiteTemplate$.next([]);
  }

  public onAdsetListScroll(event: Event) {
    if (shouldLoadMore(event.target as HTMLElement)) {
      this.adsetCurrentPage$.pipe(take(1)).subscribe({
        next: (currentPage) => {
          this.store.dispatch(
            new GetMoreAdsetList({
              page: currentPage,
              pageSize: this.pageSize,
              campaignIds: [this.campaignId],
            })
          );
        },
      });
    }
  }

  private dispatchGetAdsetList(page: number) {
    this.store.dispatch(
      new GetAdsetList({
        page,
        pageSize: this.pageSize,
        withSummary: false,
        campaignIds: [this.campaignId],
      })
    );
  }
}
