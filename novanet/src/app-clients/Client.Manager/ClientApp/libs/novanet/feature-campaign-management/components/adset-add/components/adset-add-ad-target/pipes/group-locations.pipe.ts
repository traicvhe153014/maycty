import { Pipe, PipeTransform } from '@angular/core';
import { ILocation } from '@shared/sharing/store';
import { regionOrders } from '@features/campaign-management/constants';

@Pipe({
  name: 'groupLocation',
})
export class GroupLocationPipe implements PipeTransform {
  transform(value: ILocation[]) {
    const groupedLocations = value
      .slice()
      .sort((a, b) => {
        if (a.regionName === b.regionName) {
          return a.provinceName.localeCompare(b.provinceName);
        }
        return (
          regionOrders.indexOf(a.regionName) -
          regionOrders.indexOf(b.regionName)
        );
      })
      .groupBy('regionName');
    return Object.keys(groupedLocations).map((regionName) => ({
      regionName,
      locations: groupedLocations[regionName],
      active: false,
    }));
  }
}
