import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { AdvertisingService } from './advertising-state.service';
import { AdvertisingState } from './advertising.state';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [NgxsModule.forFeature([AdvertisingState]), RouterModule],
  providers: [AdvertisingService],
})
export class AdvertisingStateModule {}
