import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditableCampaignTitleComponent } from './editable-campaign-title.component';
import { FormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { RouterModule } from '@angular/router';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [EditableCampaignTitleComponent],
  imports: [
    CommonModule,
    SvgIconModule,
    FormsModule,
    RouterModule,
    NzToolTipModule,
  ],
  exports: [EditableCampaignTitleComponent],
})
export class EditableCampaignTitleModule {}
