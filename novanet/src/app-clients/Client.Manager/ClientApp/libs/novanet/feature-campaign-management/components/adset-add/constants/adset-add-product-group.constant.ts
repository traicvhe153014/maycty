import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import { AdsetAddSelectedProductGroupsEnum } from '@features/campaign-management/components/adset-add/enums';

export const selectedProductGroupSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: false,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;

export const selectedProductGroupColumns = [
  {
    id: AdsetAddSelectedProductGroupsEnum.INDEX,
    title: 'Stt',
    width: '50px',
    align: 'center',
  },
  {
    id: AdsetAddSelectedProductGroupsEnum.NAME,
    title: 'Tiêu đề nhóm',
  },
  {
    id: AdsetAddSelectedProductGroupsEnum.PRODUCT_COUNT,
    title: 'Số lượng SP',
    width: '90px',
  },
  {
    id: AdsetAddSelectedProductGroupsEnum.ACTION,
    title: 'Thao tác',
    width: '70px',
    align: 'center',
  },
] as ISettingColumnTable<any, any>[];
