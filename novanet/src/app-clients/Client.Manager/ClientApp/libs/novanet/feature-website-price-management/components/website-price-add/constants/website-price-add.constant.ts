import { IFormFieldErrors, IFormFields } from '@models';
import { IDropdownValues } from '@core/models';
import { EPriceType } from '@features/feature-website-price-management/store';

export const websitePriceCreateFormFields: IFormFields = {
  websiteId: {
    name: 'websiteId',
    validationParams: {
      required: true,
    },
  },
  zoneId: {
    name: 'zoneId',
    validationParams: {
      required: true,
    },
  },
  publisherPriceType: {
    name: 'publisherPriceType',
    validationParams: {
      required: true,
    },
  },
  startDate: {
    name: 'startDate',
    validationParams: {
      required: true,
    },
  },
  prices: {
    name: 'prices',
    validationParams: {
      required: true,
    },
  },
};

export const websitePriceDetailFields: IFormFields = {
  templateType: {
    name: 'templateType',
    validationParams: {
      required: true,
    },
  },
  campaignType: {
    name: 'campaignType',
  },
  buyPriceType: {
    name: 'buyPriceType',
    validationParams: {
      required: true,
    },
  },
  buyPrice: {
    name: 'buyPrice',
    validationParams: {
      required: true,
      min: 0,
    },
  },
  sellPriceType: {
    name: 'sellPriceType',
    validationParams: {
      required: true,
    },
  },
  sellPrice: {
    name: 'sellPrice',
    validationParams: {
      required: true,
      min: 0,
    },
  },
};

export const createFormFieldErrors: IFormFieldErrors = {
  websiteId: [
    {
      type: 'required',
      message: 'Chưa chọn website',
    },
  ],
  zoneId: [
    {
      type: 'required',
      message: 'Chưa chọn vùng quảng cáo',
    },
  ],
  startDate: [
    {
      type: 'required',
      message: 'Chưa chọn thời điểm áp dụng',
    },
    {
      type: 'notInThePast',
      message: 'Không thể chọn thời gian trước thời điểm hiện tại',
    },
    {
      type: 'existed',
      message: 'Thời gian áp dụng bảng giá đã tồn tại',
    },
  ],
};

export const detailFieldErrors: IFormFieldErrors = {
  buyPrice: [
    {
      type: 'required',
      message: 'Chưa nhập giá',
    },
    {
      type: 'min',
      message: 'Giá không hợp lệ',
    },
    {
      type: 'maxlength',
      message: 'Tối đa 10 chữ số',
    },
    {
      type: 'buyPriceGreaterThan',
      message: 'Giá mua phải nhỏ hơn giá bán',
    },
  ],
  sellPrice: [
    {
      type: 'required',
      message: 'Chưa nhập giá',
    },
    {
      type: 'min',
      message: 'Giá không hợp lệ',
    },
    {
      type: 'maxlength',
      message: 'Tối đa 10 chữ số',
    },
    {
      type: 'sellPriceLessThan',
      message: 'Giá bán phải lớn hơn giá mua',
    },
  ],
};

export const PriceTypeDropdowns = [
  {
    id: EPriceType.CPC,
    label: 'CPC',
  },
  {
    id: EPriceType.CPM,
    label: 'CPM',
  },
] as IDropdownValues[];
