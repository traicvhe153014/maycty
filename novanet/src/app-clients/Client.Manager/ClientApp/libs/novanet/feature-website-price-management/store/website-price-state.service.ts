import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  IGetWebsitePriceResponse,
  IListWebsitePriceRequest,
  IListWebsitePriceResponse,
  IWebsitePriceCreateRequest,
  IWebsitePriceUpdateRequest,
  IWebsitePriceUpdateResponse,
} from './website-price-state.model';

const WebsitePriceUrls = {
  getList: `websitePrice/list`,
  getById: `websitePrice/get`,
  create: `websitePrice/create`,
  update: `websitePrice/update`,
};

@Injectable()
export class WebsitePriceService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public getList(
    payload: IListWebsitePriceRequest
  ): Observable<IListWebsitePriceResponse> {
    const params = new HttpParams({
      fromObject: {
        ...payload,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IListWebsitePriceResponse>>(
        this.createUrl(WebsitePriceUrls.getList),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public getById(id: string): Observable<IGetWebsitePriceResponse> {
    const params = new HttpParams({
      fromObject: {
        id,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IGetWebsitePriceResponse>>(
        this.createUrl(WebsitePriceUrls.getById),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public create(
    payload: Partial<IWebsitePriceCreateRequest>
  ): Observable<IWebsitePriceUpdateResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IWebsitePriceUpdateResponse>>(
        this.createUrl(WebsitePriceUrls.create),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public update(
    payload: Partial<IWebsitePriceUpdateRequest>
  ): Observable<IWebsitePriceUpdateResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IWebsitePriceUpdateResponse>>(
        this.createUrl(WebsitePriceUrls.update),
        payload
      )
      .pipe(map((response) => response.data));
  }
}
