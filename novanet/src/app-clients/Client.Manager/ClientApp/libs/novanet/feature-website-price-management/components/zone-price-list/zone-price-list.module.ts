import { NgModule } from '@angular/core';
import { ZonePriceListComponent } from './zone-price-list.component';
import { CommonModule } from '@angular/common';
import {
  CastModule,
  DataTableModule,
  FormatterModule,
  LoadingIconModule,
  SortListModule,
  WebsiteOptionsModule,
} from '@core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';
import { RouterModule } from '@angular/router';
import { NovanetInputModule } from '@shared/custom-input';
import { CommonComponentsModule } from '@shared/components';
import { DisplayTemplatePricePipe } from './pipes';
import { NzModalModule } from 'ng-zorro-antd/modal';

@NgModule({
  declarations: [ZonePriceListComponent, DisplayTemplatePricePipe],
  imports: [
    CommonModule,
    DataTableModule,
    LoadingIconModule,
    FormsModule,
    SvgIconModule,
    FormatterModule,
    SwitchNoAnimationModule,
    RouterModule,
    NovanetInputModule,
    ReactiveFormsModule,
    CommonComponentsModule,
    CastModule,
    NzModalModule,
    WebsiteOptionsModule,
    SortListModule,
  ],
  exports: [ZonePriceListComponent],
})
export class ZonePriceListModule {}
