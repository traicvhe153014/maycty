import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { WebsitePriceManagementComponent } from '@features/feature-website-price-management/website-price-management.component';

const routes: Route[] = [
  {
    path: 'list',
    component: WebsitePriceManagementComponent,
  },
  {
    path: 'add-website-price',
    loadChildren: () =>
      import(
        '@features/feature-website-price-management/components/website-price-add'
      ).then((m) => m.WebsitePriceAddModule),
  },
  {
    path: 'edit-website-price',
    loadChildren: () =>
      import(
        '@features/feature-website-price-management/components/website-price-add'
      ).then((m) => m.WebsitePriceAddModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebsitePriceManagementRoutingModule {}
