import { ZonePriceListEnum } from '@features/feature-website-price-management/components/zone-price-list/enums';
import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import {
  IWebsitePriceResponse,
  WebsitePriceStatusEnum,
} from '@features/feature-website-price-management/store';
import { IStatusBlockConfig } from '@core/models';
import { WebsitePriceListEnum } from '@features/feature-website-price-management/components/website-price-list/enums';

export const ZonePriceListSettingTable = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '125vw',
  summaryRow: false,
  sortType: 'single',
} as DataTableSettingModel;

export const ZonePriceListColumns = [
  {
    id: ZonePriceListEnum.INDEX,
    title: 'Stt',
    width: '50px',
    pinLeft: true,
  },
  {
    id: ZonePriceListEnum.IS_ACTIVE,
    title: 'Bật/tắt',
    width: '80px',
    pinLeft: true,
  },
  {
    id: ZonePriceListEnum.ZONE,
    title: 'Vùng quảng cáo',
    width: '300px',
    pinLeft: true,
  },
  {
    id: ZonePriceListEnum.CAMPAIGN_TYPE,
    title: 'Loại QC',
    width: '120px',
    pinLeft: true,
  },
  {
    id: ZonePriceListEnum.STATUS,
    title: 'Trạng thái giá',
    sort: true,
    name: 'Status',
    width: '120px',
    pinLeft: true,
    ascending: true,
    descending: false,
  },
  {
    id: ZonePriceListEnum.PRICE_TYPE,
    title: 'Loại giá',
    sort: true,
    name: 'PriceType',
    width: '100px',
    pinLeft: true,
  },
  {
    id: ZonePriceListEnum.NOTIFICATION_BANNER,
    title: 'Notification Banner (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.CATFISH_COLLAB_BRANDING,
    title: 'Catfish Collab Branding (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.POPUP_BANNER_BRANDING,
    title: 'Popup Banner Branding (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.CATFISH_ECOM,
    title: 'Catfish Ecom (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.MOBILE_BANNER,
    title: 'Mobile Banner Card (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.INTERACTIVE_BANNER,
    title: 'Interactive Banner (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.FLYING_CARPET,
    title: 'Flying Carpet (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.IN_READ_ECOMMERCE,
    title: 'In Read Ecommerce (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.POST_IN_READ,
    title: 'Post In Read (VNĐ)',
    width: '200px',
  },
  {
    id: ZonePriceListEnum.END_DATE,
    title: 'Ngày kết thúc',
    width: '150px',
    sort: true,
    name: 'EndDate',
  },
] as ISettingColumnTable<IWebsitePriceResponse, any>[];

export const ZonePriceStatusConfigs: IStatusBlockConfig<WebsitePriceStatusEnum>[] =
  [
    {
      value: WebsitePriceStatusEnum.Running,
      text: 'Đang chạy',
      backgroundColor: 'rgba(0,97,193,0.1)',
      textColor: '#0061C1',
    },
    {
      value: WebsitePriceStatusEnum.NotStarted,
      text: 'Chưa chạy',
      backgroundColor: 'rgba(79,79,79,0.1)',
      textColor: '#4F4F4F',
    },
    {
      value: WebsitePriceStatusEnum.Paused,
      text: 'Tạm dừng',
      backgroundColor: 'rgba(79,79,79,0.1)',
      textColor: '#4F4F4F',
    },
    {
      value: WebsitePriceStatusEnum.Archived,
      text: 'Lịch sử',
      backgroundColor: 'rgba(242,153,74,0.1)',
      textColor: '#F2994A',
    },
  ];
