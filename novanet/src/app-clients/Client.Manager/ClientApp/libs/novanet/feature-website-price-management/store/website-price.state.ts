import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  IGetWebsitePriceResponse,
  IWebsitePriceResponse,
  IWebsitePriceStateModel,
  PublisherPriceType,
} from './website-price-state.model';
import { WebsitePriceService } from './website-price-state.service';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HideLoading } from '@shared/global-loading';
import {
  ClearAllWebsitePricesData,
  CreateWebsitePrice,
  CreateWebsitePriceError,
  CreateWebsitePriceSuccess,
  GetMoreWebsitePriceList,
  GetMoreWebsitePriceListError,
  GetMoreWebsitePriceListSuccess,
  GetWebsitePrice,
  GetWebsitePriceError,
  GetWebsitePriceList,
  GetWebsitePriceListError,
  GetWebsitePriceListSuccess,
  GetWebsitePriceSuccess,
  UpdateWebsitePrice,
  UpdateWebsitePriceError,
  UpdateWebsitePriceSuccess,
} from './website-price-state.actions';
import { UpdateAdset } from '@features/campaign-management/store';

@Injectable()
@State<IWebsitePriceStateModel>({
  name: 'websitePrice',
  defaults: {
    loading: false,
    websitePrices: [],
    page: 1,
    hasMorePages: true,
    total: 0,
    detailedWebsitePrice: undefined,
  },
})
export class WebsitePriceState {
  constructor(private readonly websitePriceService: WebsitePriceService) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(
    state: IWebsitePriceStateModel
  ): IWebsitePriceResponse[] {
    return state.websitePrices;
  }

  @Selector()
  public static getPage(state: IWebsitePriceStateModel): number {
    return state.page;
  }

  @Selector()
  public static getHasMorePages(state: IWebsitePriceStateModel): boolean {
    return state.hasMorePages;
  }

  @Selector()
  public static getTotal(state: IWebsitePriceStateModel): number {
    return state.total;
  }

  @Selector()
  public static getLoading(state: IWebsitePriceStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getDetailed(
    state: IWebsitePriceStateModel
  ): IGetWebsitePriceResponse | undefined {
    return state.detailedWebsitePrice;
  }

  @Selector()
  public static getDetailedWebsiteOptions(state: IWebsitePriceStateModel) {
    return {
      data: [
        {
          id: state.detailedWebsitePrice?.websiteId,
          publisherEmail: state.detailedWebsitePrice?.publisherEmail,
          domain: state.detailedWebsitePrice?.domain,
          url: state.detailedWebsitePrice?.url,
        },
      ],
    };
  }

  @Selector()
  public static getDetailedZoneOptions(state: IWebsitePriceStateModel) {
    return {
      data: [
        {
          id: state.detailedWebsitePrice?.zoneId,
          name: state.detailedWebsitePrice?.zoneName,
        },
      ],
    };
  }

  /**
   * Actions
   */
  @Action(GetWebsitePriceList)
  public getList(
    ctx: StateContext<IWebsitePriceStateModel>,
    { payload }: GetWebsitePriceList
  ) {
    const loadingLabel = 'Get WebsitePrice List';
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.websitePriceService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(new GetWebsitePriceListSuccess(response, payload))
      ),
      catchError((err) => ctx.dispatch(new GetWebsitePriceListError(err)))
    );
  }

  @Action(GetWebsitePriceListSuccess)
  public getListSuccess(
    ctx: StateContext<IWebsitePriceStateModel>,
    { response, payload }: GetWebsitePriceListSuccess
  ) {
    const { data: websitePrices } = response;
    const loadingLabel = 'Get WebsitePrice List';
    const newPage =
      websitePrices.length === 0 && payload.page !== 1
        ? payload.page - 1
        : payload.page;
    const hasMorePages = websitePrices.length === payload.pageSize;
    ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      websitePrices,
      page: newPage,
      hasMorePages,
      loading: false,
      total: response.total,
    });
  }

  @Action(GetWebsitePriceListError)
  public getListError(
    ctx: StateContext<IWebsitePriceStateModel>,
    { error }: GetWebsitePriceListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetMoreWebsitePriceList)
  public getMoreList(
    ctx: StateContext<IWebsitePriceStateModel>,
    { payload }: GetMoreWebsitePriceList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.websitePriceService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(new GetMoreWebsitePriceListSuccess(response, payload))
      ),
      catchError((err) => ctx.dispatch(new GetMoreWebsitePriceListError(err)))
    );
  }

  @Action(GetMoreWebsitePriceListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<IWebsitePriceStateModel>,
    { response, payload }: GetMoreWebsitePriceListSuccess
  ) {
    const { data: websitePrices } = response;
    const newPage =
      websitePrices.length === 0 ? payload.page - 1 : payload.page;
    const hasMorePages = websitePrices.length === payload.pageSize;
    const currentWebsitePrices = ctx.getState().websitePrices;
    ctx.patchState({
      websitePrices: [...currentWebsitePrices, ...websitePrices],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreWebsitePriceListError)
  public getMoreListError(
    ctx: StateContext<IWebsitePriceStateModel>,
    { error }: GetMoreWebsitePriceListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetWebsitePrice)
  public get(
    ctx: StateContext<IWebsitePriceStateModel>,
    { id }: GetWebsitePrice
  ) {
    const loadingLabel = 'Get WebsitePrice';
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.websitePriceService.getById(id).pipe(
      map((response) => ctx.dispatch(new GetWebsitePriceSuccess(response))),
      catchError((err) => ctx.dispatch(new GetWebsitePriceError(err)))
    );
  }

  @Action(GetWebsitePriceSuccess)
  public getSuccess(
    ctx: StateContext<IWebsitePriceStateModel>,
    { response }: GetWebsitePriceSuccess
  ) {
    const loadingLabel = 'Get WebsitePrice List';
    ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      loading: false,
      detailedWebsitePrice: response,
    });
  }

  @Action(GetWebsitePriceError)
  public getError(
    ctx: StateContext<IWebsitePriceStateModel>,
    { error }: GetWebsitePriceError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(CreateWebsitePrice)
  public create(
    ctx: StateContext<IWebsitePriceStateModel>,
    { payload }: CreateWebsitePrice
  ) {
    return this.websitePriceService.create(payload).pipe(
      map((response) => ctx.dispatch(new CreateWebsitePriceSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateWebsitePriceError(error)))
    );
  }

  @Action(CreateWebsitePriceSuccess)
  public createSuccess(
    ctx: StateContext<IWebsitePriceStateModel>,
    { response }: CreateWebsitePriceSuccess
  ) {
    return response;
  }

  @Action(CreateWebsitePriceError)
  public createError(
    ctx: StateContext<IWebsitePriceStateModel>,
    { error }: CreateWebsitePriceError
  ) {
    return throwError(error);
  }

  @Action(UpdateWebsitePrice)
  public update(
    ctx: StateContext<IWebsitePriceStateModel>,
    { payload }: UpdateAdset
  ) {
    return this.websitePriceService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateWebsitePriceSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateWebsitePriceError(error)))
    );
  }

  @Action(UpdateWebsitePriceSuccess)
  public updateSuccess(
    ctx: StateContext<IWebsitePriceStateModel>,
    { response }: UpdateWebsitePriceSuccess
  ) {
    const { websitePrices } = ctx.getState();
    const updatedWebsitePrices = websitePrices.map((item) => {
      if (item.publisherPriceType === PublisherPriceType.Website) {
        if (item.websiteId !== response.websiteId) {
          return item;
        }
        return {
          ...item,
          prices: item.prices.map((price) => {
            if (price.websitePriceId !== response.id) {
              return price;
            }
            return {
              ...price,
              status: response.status,
              isActive: response.isActive,
            };
          }),
        };
      }
      if (item.publisherPriceType === PublisherPriceType.Zone) {
        if (item.zoneId !== response.zoneId) {
          return item;
        }
        return {
          ...item,
          prices: item.prices.map((price) => {
            if (price.websitePriceId !== response.id) {
              return price;
            }
            return {
              ...price,
              status: response.status,
              isActive: response.isActive,
            };
          }),
        };
      }
      return item;
    });
    ctx.patchState({ websitePrices: updatedWebsitePrices });
  }

  @Action(UpdateWebsitePriceError)
  public updateError(
    ctx: StateContext<IWebsitePriceStateModel>,
    { error }: UpdateWebsitePriceError
  ) {
    return throwError(error);
  }

  @Action(ClearAllWebsitePricesData)
  public clearAllData(ctx: StateContext<IWebsitePriceStateModel>) {
    const websitePriceState = {
      websitePrices: [],
      page: 1,
      loading: false,
      hasMorePages: false,
      total: 0,
      detailedWebsitePrice: undefined,
    } as IWebsitePriceStateModel;
    ctx.patchState(websitePriceState);
  }
}
