import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WebsitePriceTypeEnum } from '@features/feature-website-price-management/enums';

@Component({
  selector: 'novanet-website-price-management',
  templateUrl: './website-price-management.component.html',
  styleUrls: ['./website-price-management.component.scss'],
})
export class WebsitePriceManagementComponent {
  public tabIndex = 0;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    const currentType = this.activatedRoute.snapshot.queryParamMap.get('type');
    if (currentType) {
      this.tabIndex = WebsitePriceTypeEnum[currentType];
    }
  }

  public onTabIndexChange(index: number) {
    this.router
      .navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: { type: WebsitePriceTypeEnum[index] },
        queryParamsHandling: 'merge',
      })
      .then(() => {
        this.tabIndex = index;
      });
  }
}
