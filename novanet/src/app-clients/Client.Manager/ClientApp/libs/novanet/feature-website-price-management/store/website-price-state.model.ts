import { ETemplateType } from '@features/campaign-management/enums';
import { CampaignType } from '@features/campaign-management/store';
import { IZoneTemplate } from '@shared/settings/store';

export enum WebsitePriceStatusEnum {
  Running = 1,
  Paused,
  NotStarted,
  Archived,
}

export enum PublisherPriceType {
  Publisher = 1,
  Website,
  Zone,
}

export enum EPriceType {
  CPM,
  CPC,
  CPA,
}

export interface IListWebsitePriceRequest {
  page: number;
  pageSize: number;
  keyword?: string;
  ids?: string;
  sorts?: string[];
  websiteId?: string;
  publisherPriceType: PublisherPriceType;
}

export interface IListWebsitePriceResponse {
  data: IWebsitePriceResponse[];
  total: number;
}

export interface IWebsitePriceResponse {
  websiteId: string;
  zoneId?: string;
  zoneName?: string;
  domain: string;
  url: string;
  publisherId: string;
  publisherEmail: string;
  createdAt: string;
  prices: IDetailedWebsitePrice[];
  publisherPriceType: PublisherPriceType;
}

export interface IDetailedWebsitePrice {
  websitePriceId: string;
  isActive: boolean;
  status: WebsitePriceStatusEnum;
  startDate: string;
  endDate: string;
  templatePrices: { [key: number]: ITemplatePrice };
  campaignTypes: CampaignType[];
}

export interface ITemplatePrice {
  buyPrice: number;
  buyPriceType: EPriceType;
  sellPrice: number;
  sellPriceType: EPriceType;
  templateType: ETemplateType;
  campaignType?: CampaignType;
}

export interface IGetWebsitePriceResponse {
  id: string;
  websiteId: string;
  zoneId?: string;
  campaignTypes: CampaignType[];
  startDate: string;
  publisherPriceType: PublisherPriceType;
  templatePrices: ITemplatePrice[];
  zoneTemplates?: IZoneTemplate[];
  zoneName?: string;
  domain: string;
  url: string;
  publisherEmail: string;
}

export interface IWebsitePriceCreateRequest {
  websiteId: string;
  zoneId?: string;
  campaignTypes: CampaignType[];
  startDate: string;
  publisherPriceType: PublisherPriceType;
  templatePrices: ITemplatePrice[];
}

export interface IWebsitePriceUpdateRequest {
  id: string;
  isActive: boolean;
  websiteId: string;
  zoneId?: string;
  campaignTypes: CampaignType[];
  startDate: string;
  publisherPriceType: PublisherPriceType;
  templatePrices: ITemplatePrice[];
  isFullUpdate: boolean;
}

export interface IWebsitePriceUpdateResponse {
  id: string;
  isActive: boolean;
  websiteId: string;
  zoneId?: string;
  createdAt: string;
  publisherPriceType: PublisherPriceType;
  startDate: string;
  endDate: string;
  status: WebsitePriceStatusEnum;
}

export interface IWebsitePriceStateModel {
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  total: number;
  websitePrices: IWebsitePriceResponse[];
  detailedWebsitePrice?: IGetWebsitePriceResponse;
}
