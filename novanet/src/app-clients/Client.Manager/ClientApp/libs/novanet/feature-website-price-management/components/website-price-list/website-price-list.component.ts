import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  ClearAllWebsitePricesData,
  EPriceType,
  GetWebsitePriceList,
  IWebsitePriceResponse,
  PublisherPriceType,
  WebsitePriceState,
  WebsitePriceStatusEnum,
} from '@features/feature-website-price-management/store';
import { EMPTY, expand, Observable } from 'rxjs';
import { WebsitePriceListEnum } from '@features/feature-website-price-management/components/website-price-list/enums';
import {
  WebsitePriceListColumns,
  WebsitePriceListSettingTable,
  WebsitePriceStatusConfigs,
} from '@features/feature-website-price-management/components/website-price-list/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { IFieldDetail } from '@data-table/models';
import { ESortType } from '@core/enums';
import {
  GetMoreWebsiteList,
  GetWebsiteList,
  IWebsitesResponse,
  SettingsState,
} from '@shared/settings/store';
import { ETemplateType } from '@features/campaign-management/enums';
import { campaignTypeLabels } from '@features/campaign-management/constants';

@Component({
  selector: 'novanet-website-price-list',
  templateUrl: './website-price-list.component.html',
  styleUrls: ['./website-price-list.component.scss'],
})
export class WebsitePriceListComponent implements OnInit, OnDestroy {
  @Select(WebsitePriceState.getList) public websitePrices$: Observable<
    IWebsitePriceResponse[]
  >;
  @Select(WebsitePriceState.getPage) public currentPage$: Observable<number>;
  @Select(WebsitePriceState.getHasMorePages)
  public hasMorePages$: Observable<boolean>;
  @Select(WebsitePriceState.getLoading) public loading$: Observable<boolean>;
  @Select(WebsitePriceState.getTotal) public total$: Observable<number>;
  @Select(SettingsState.getWebsites) websites$: Observable<IWebsitesResponse>;
  @Select(SettingsState.getLoadingWebsites)
  websitesLoading$: Observable<boolean>;

  public readonly websitePriceListEnum = WebsitePriceListEnum;
  public columns = WebsitePriceListColumns;
  public readonly settings = WebsitePriceListSettingTable;
  public readonly websitePriceStatusConfigs = WebsitePriceStatusConfigs;
  public readonly websitePriceStatusEnum = WebsitePriceStatusEnum;
  public readonly eTemplateType = ETemplateType;
  public readonly ePriceType = EPriceType;

  public websiteKeyword = '';
  public sorts = ['status'];
  public readonly baseSorts = ['status'];
  public readonly pageSize = 50;
  public selectedWebsiteId: string;
  public expandedWebsiteIds: { [key: string]: boolean } = {};
  public campaignTypeLabels = campaignTypeLabels;

  private websitePage = 1;
  private readonly websitePageSize = 24;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.selectedWebsiteId =
      this.activatedRoute.snapshot.queryParamMap.get('websiteId');
    this.store
      .dispatch(
        new GetWebsiteList({
          page: this.websitePage,
          pageSize: this.websitePageSize,
          keyWord: this.websiteKeyword,
        })
      )
      .pipe(
        expand((data) => {
          if (
            !this.selectedWebsiteId ||
            data.settings.websites.find(
              (item) => item.id === this.selectedWebsiteId
            )
          ) {
            return EMPTY;
          }
          this.websitePage += 1;
          return this.store.dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          );
        })
      )
      .subscribe(() => {});
    if (this.selectedWebsiteId) {
      this.store.dispatch(
        new GetWebsitePriceList({
          page: 1,
          pageSize: this.pageSize,
          sorts: this.sorts,
          websiteId: this.selectedWebsiteId,
          publisherPriceType: PublisherPriceType.Website,
        })
      );
    }
  }

  public onSearch(event: string) {
    this.websiteKeyword = event;
    this.websitePage = 1;
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
        keyWord: this.websiteKeyword,
      })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearAllWebsitePricesData());
  }

  public loadMoreWebsites() {
    this.websitesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.websitePage += 1;
        this.store.dispatch(
          new GetMoreWebsiteList({
            page: this.websitePage,
            pageSize: this.websitePageSize,
            keyWord: this.websiteKeyword,
          })
        );
      },
    });
  }

  public searchWebsitePrices(values?: IFieldDetail[]) {
    if (values.length) {
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.store.dispatch(
      new GetWebsitePriceList({
        page: 1,
        pageSize: this.pageSize,
        sorts: this.sorts,
        websiteId: this.selectedWebsiteId,
        publisherPriceType: PublisherPriceType.Website,
      })
    );
  }

  public toggleExpandedWebsite(id: string) {
    this.expandedWebsiteIds[id] = !this.expandedWebsiteIds[id];
  }

  public onSelectWebsite(websiteId: string) {
    this.selectedWebsiteId = websiteId;
    if (this.selectedWebsiteId) {
      this.store.dispatch(
        new GetWebsitePriceList({
          page: 1,
          pageSize: this.pageSize,
          sorts: this.sorts,
          websiteId: this.selectedWebsiteId,
          publisherPriceType: PublisherPriceType.Website,
        })
      );
    } else {
      this.store.dispatch(new ClearAllWebsitePricesData());
    }

    this.router
      .navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: { websiteId },
        queryParamsHandling: 'merge',
      })
      .then();
  }
}
