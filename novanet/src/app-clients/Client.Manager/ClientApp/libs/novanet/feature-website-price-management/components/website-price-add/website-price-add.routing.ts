import { NgModule } from '@angular/core';
import { WebsitePriceAddComponent } from './website-price-add.component';
import { Route, RouterModule } from '@angular/router';

const routes: Route[] = [
  {
    path: '',
    component: WebsitePriceAddComponent,
  },
];

@NgModule({
  imports: [RouterModule, RouterModule.forChild(routes)],
})
export class WebsitePriceAddRoutingModule {}
