import { NgModule } from '@angular/core';
import { WebsitePriceAddComponent } from './website-price-add.component';
import { CommonModule } from '@angular/common';
import { WebsitePriceAddRoutingModule } from './website-price-add.routing';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetTemplateNameModule, WebsiteOptionsModule } from '@core/pipes';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [WebsitePriceAddComponent],
  imports: [
    CommonModule,
    WebsitePriceAddRoutingModule,
    NovanetInputModule,
    FormsModule,
    ReactiveFormsModule,
    WebsiteOptionsModule,
    GetTemplateNameModule,
    SvgIconModule,
  ],
  exports: [WebsitePriceAddComponent],
})
export class WebsitePriceAddModule {}
