import { NgModule } from '@angular/core';
import { WebsitePriceListComponent } from './website-price-list.component';
import { CommonModule } from '@angular/common';
import {
  CastModule,
  DataTableModule,
  FormatterModule,
  LoadingIconModule,
  SortListModule,
  WebsiteOptionsModule,
} from '@core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';
import { RouterModule } from '@angular/router';
import { NovanetInputModule } from '@shared/custom-input';
import { CommonComponentsModule } from '@shared/components';
import { DisplayTemplatePricePipe } from './pipes';

@NgModule({
  declarations: [WebsitePriceListComponent, DisplayTemplatePricePipe],
  imports: [
    CommonModule,
    DataTableModule,
    LoadingIconModule,
    FormsModule,
    SvgIconModule,
    FormatterModule,
    SwitchNoAnimationModule,
    RouterModule,
    NovanetInputModule,
    ReactiveFormsModule,
    CommonComponentsModule,
    CastModule,
    WebsiteOptionsModule,
    SortListModule,
  ],
  exports: [WebsitePriceListComponent],
})
export class WebsitePriceListModule {}
