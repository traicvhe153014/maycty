import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  CampaignType,
  IAdvertisingStateModel,
  IAdvertisingTemplate,
  ListAdvertisingTemplate,
} from '@features/campaign-management/store';
import { Select, Store } from '@ngxs/store';
import { EMPTY, expand, Observable, Subject } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ActivatedRoute, Router } from '@angular/router';
import {
  createFormFieldErrors,
  detailFieldErrors,
  PriceTypeDropdowns,
  websitePriceCreateFormFields,
  websitePriceDetailFields,
} from './constants';
import {
  CreateWebsitePrice,
  EPriceType,
  GetWebsitePrice,
  ITemplatePrice,
  IWebsitePriceCreateRequest,
  IWebsitePriceStateModel,
  IWebsitePriceUpdateRequest,
  PublisherPriceType,
  UpdateWebsitePrice,
  WebsitePriceState,
} from '@features/feature-website-price-management/store';
import {
  GetMoreWebsiteList,
  GetMoreZoneList,
  GetWebsiteList,
  GetZoneList,
  IWebsitesResponse,
  IZonesResponse,
  SettingsState,
} from '@shared/settings/store';
import { map, take, takeUntil } from 'rxjs/operators';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { ETemplateType } from '@features/campaign-management/enums';
import * as dayjs from 'dayjs';
import { NumberInputComponent } from '@shared/custom-input/components/number-input/number-input.component';
import { campaignTypeLabels } from '@features/campaign-management/constants';

@Component({
  selector: 'novanet-website-price-add',
  templateUrl: './website-price-add.component.html',
  styleUrls: ['./website-price-add.component.scss'],
})
export class WebsitePriceAddComponent implements OnInit, OnDestroy {
  public formType: string; // add | edit
  public createForm: FormGroup;
  public readonly createFormFields = websitePriceCreateFormFields;
  public readonly createFormFieldErrors = createFormFieldErrors;
  public readonly detailFormFields = websitePriceDetailFields;
  public readonly detailFormFieldErrors = detailFieldErrors;
  public readonly publisherPriceType = PublisherPriceType;
  public readonly campaignType = CampaignType;
  public readonly priceTypeDropdowns = PriceTypeDropdowns;
  public readonly campaignTypeLabels = campaignTypeLabels;

  @Select(SettingsState.getWebsites) websites$: Observable<IWebsitesResponse>;
  @Select(SettingsState.getLoadingWebsites)
  websitesLoading$: Observable<boolean>;
  @Select(SettingsState.getZones) zones$: Observable<IZonesResponse>;
  @Select(SettingsState.getLoadingZones)
  zonesLoading$: Observable<boolean>;
  @Select(WebsitePriceState.getDetailedWebsiteOptions)
  detailedWebsiteOptions$: Observable<IWebsitesResponse>;
  @Select(WebsitePriceState.getDetailedZoneOptions)
  detailedZoneOptions$: Observable<IZonesResponse>;

  @ViewChildren(NumberInputComponent)
  priceInputs: QueryList<NumberInputComponent>;

  public websiteKeyword = '';
  public zoneKeyword = '';

  private websitePage = 1;
  private readonly websitePageSize = 24;
  private zonePage = 1;
  private readonly zonePageSize = 24;

  private readonly destroy$: Subject<void> = new Subject<void>();

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private notification: NzNotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public get pricesFormArray() {
    return this.createForm.get(this.createFormFields.prices.name) as FormArray;
  }

  private static convertPrice(price: number | string): number {
    return !price
      ? 0
      : typeof price === 'number'
      ? price
      : parseInt(price.replace(/,/g, ''), 10);
  }

  private static isEmpty(value) {
    return value === '' || value === undefined || value === null;
  }

  public priceFormGroup(index: number) {
    return this.pricesFormArray.at(index) as FormGroup;
  }

  public getCampaignTypeRowspan(index: number): number {
    const currentFormGroup = this.priceFormGroup(index);
    const currentTemplateType = currentFormGroup.get('templateType')
      .value as ETemplateType;
    const currentCampaignType = currentFormGroup.get('campaignType')
      .value as CampaignType;

    let count = 0;
    for (let i = 0; i < this.pricesFormArray.length; i++) {
      if (
        currentCampaignType !== this.priceFormGroup(i).get('campaignType').value
      ) {
        if (count > 0) {
          break;
        } else {
          continue;
        }
      }
      if (
        currentTemplateType !==
          this.priceFormGroup(i).get('templateType').value &&
        count === 0
      ) {
        return 0;
      }
      count += 1;
    }
    return count;
  }

  ngOnInit(): void {
    this.store
      .dispatch(new ListAdvertisingTemplate())
      .pipe(take(1))
      .subscribe({
        next: (data) => {
          const advertisingTemplates = [
            ...(data.Advertising as IAdvertisingStateModel)
              .advertisingTemplates,
          ].sort(
            (a, b) =>
              b.campaignType - a.campaignType || a.templateType - b.templateType
          );

          this.formType = this.router.url.includes('add-website-price')
            ? 'add'
            : 'edit';
          this.buildCreateForm(advertisingTemplates);

          if (this.formType === 'edit') {
            const websitePriceId =
              this.activatedRoute.snapshot.queryParamMap.get('websitePriceId');
            if (!websitePriceId) {
              this.router.navigateByUrl('/').then();
              return;
            }
            this.store
              .dispatch(new GetWebsitePrice(websitePriceId))
              .pipe(take(1))
              .subscribe({
                next: (data) => {
                  const websitePrice = (
                    data.websitePrice as IWebsitePriceStateModel
                  ).detailedWebsitePrice;
                  this.createForm.patchValue({
                    websiteId: websitePrice?.websiteId,
                    publisherPriceType: websitePrice?.publisherPriceType,
                    startDate: new Date(websitePrice?.startDate),
                    zoneId: websitePrice?.zoneId,
                  });
                  while (this.pricesFormArray.length !== 0) {
                    this.pricesFormArray.removeAt(0);
                  }
                  let templatePrices: ITemplatePrice[];
                  if (
                    websitePrice.publisherPriceType ===
                    PublisherPriceType.Website
                  ) {
                    templatePrices = advertisingTemplates.map((option) => {
                      const currentPrice = websitePrice?.templatePrices.find(
                        (price) => price.templateType === option.templateType
                      );
                      return (
                        currentPrice ?? {
                          sellPrice: null,
                          buyPrice: null,
                          templateType: option.templateType,
                          sellPriceType: EPriceType.CPC,
                          buyPriceType: EPriceType.CPC,
                          campaignType: option.campaignType,
                        }
                      );
                    });
                  } else {
                    templatePrices =
                      websitePrice.zoneTemplates?.map((template) => {
                        const currentPrice = websitePrice?.templatePrices.find(
                          (price) =>
                            price.templateType === template.templateType
                        );
                        const currentTemplate = advertisingTemplates.find(
                          (x) => x.templateType === template.templateType
                        );
                        return (
                          currentPrice ?? {
                            sellPrice: null,
                            buyPrice: null,
                            templateType: template.templateType,
                            sellPriceType: EPriceType.CPC,
                            buyPriceType: EPriceType.CPC,
                            campaignType: currentTemplate?.campaignType,
                          }
                        );
                      }) ?? [];
                  }

                  templatePrices.forEach((item) => {
                    this.pricesFormArray.push(
                      this.generatePriceForm(
                        item.templateType,
                        item.campaignType,
                        item
                      )
                    );
                  });
                  this.changeDetectorRef.detectChanges();
                },
              });
          } else {
            const websiteId =
              this.activatedRoute.snapshot.queryParamMap.get('websiteId');
            this.loadWebsites(websiteId);
            if (!!websiteId) {
              this.createForm.patchValue({ websiteId });
            }
          }
          this.createForm
            .get('websiteId')
            .valueChanges.pipe(takeUntil(this.destroy$))
            .subscribe({
              next: (websiteId) => {
                this.createForm.patchValue({ zoneId: null });
                this.setPriceForm(advertisingTemplates);
                const publisherPriceType =
                  this.createForm.get('publisherPriceType').value;
                if (publisherPriceType === PublisherPriceType.Zone) {
                  if (!websiteId) {
                    this.createForm.patchValue({
                      publisherPriceType: PublisherPriceType.Website,
                    });
                  } else {
                    this.zonePage = 1;
                    this.store.dispatch(
                      new GetZoneList({
                        page: this.zonePage,
                        pageSize: this.zonePageSize,
                        keyWord: this.zoneKeyword,
                        websiteId,
                        withZoneTemplate: true,
                      })
                    );
                  }
                }
              },
            });
          this.createForm
            .get('publisherPriceType')
            .valueChanges.pipe(takeUntil(this.destroy$))
            .subscribe({
              next: (publisherPriceType) => {
                this.createForm.patchValue({ zoneId: null });
                this.setPriceForm(advertisingTemplates);
                const websiteId = this.createForm.get('websiteId').value;
                if (
                  publisherPriceType === PublisherPriceType.Zone &&
                  !!websiteId
                ) {
                  this.zonePage = 1;
                  this.store.dispatch(
                    new GetZoneList({
                      page: this.zonePage,
                      pageSize: this.zonePageSize,
                      keyWord: this.zoneKeyword,
                      websiteId,
                      withZoneTemplate: true,
                    })
                  );
                }
              },
            });
          this.createForm
            .get('zoneId')
            .valueChanges.pipe(takeUntil(this.destroy$))
            .subscribe({
              next: () => {
                this.setPriceForm(advertisingTemplates);
              },
            });
        },
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public disabledStartDate(date: Date): boolean {
    return dayjs(date).isBefore(dayjs().add(-1, 'day'));
  }

  public onSearchWebsite(event: string) {
    this.websiteKeyword = event;
    this.websitePage = 1;
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
        keyWord: this.websiteKeyword,
      })
    );
  }

  public loadMoreWebsites() {
    return this.websitesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.websitePage += 1;
        this.store
          .dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          )
          .subscribe({
            error: () => {
              this.websitePage -= 1;
            },
          });
      },
    });
  }

  public onSearchZone(event: string) {
    const websiteId = this.createForm.get(
      this.createFormFields.websiteId.name
    ).value;
    if (!websiteId) {
      return;
    }
    this.zoneKeyword = event;
    this.zonePage = 1;
    this.store.dispatch(
      new GetZoneList({
        page: this.zonePage,
        pageSize: this.zonePageSize,
        keyWord: this.zoneKeyword,
        websiteId,
        withZoneTemplate: true,
      })
    );
  }

  public loadMoreZones() {
    this.zonesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        const websiteId = this.createForm.get(
          this.createFormFields.websiteId.name
        ).value;
        if (!websiteId) {
          return;
        }
        this.zonePage += 1;
        this.store
          .dispatch(
            new GetMoreZoneList({
              page: this.zonePage,
              pageSize: this.zonePageSize,
              keyWord: this.zoneKeyword,
              websiteId,
              withZoneTemplate: true,
            })
          )
          .subscribe({
            error: () => {
              this.zonePage -= 1;
            },
          });
      },
    });
  }

  public onClickZonePriceType(event: Event) {
    if (!this.createForm.get(this.createFormFields.websiteId.name).value) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          'Cần chọn 1 website'
        )
      );
      event.preventDefault();
    }
  }

  public onKeyDownPriceInput(event: KeyboardEvent, index: number) {
    if (event.key === 'Enter') {
      const nextInput = this.priceInputs.get(index + 1);
      if (nextInput) {
        nextInput.focus();
      }
    }
  }

  public submitForm() {
    let isValid = true;
    this.createForm.markAllAsTouched();
    if (!this.createForm.valid) {
      isValid = false;
    }
    if (!isValid) {
      if (
        this.createForm
          .get(this.createFormFields.prices.name)
          .getError('noPrice')
      ) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Bạn cần điền tối thiểu giá cho một định dạng'
          )
        );
      }
      return;
    }
    if (this.formType === 'add') {
      const payload: IWebsitePriceCreateRequest = {
        ...this.createForm.value,
        templatePrices: this.createForm.value.prices
          .filter((item) => !!item.buyPrice && !!item.sellPrice)
          .map((item) => ({
            ...item,
            buyPrice: WebsitePriceAddComponent.convertPrice(item.buyPrice),
            sellPrice: WebsitePriceAddComponent.convertPrice(item.sellPrice),
          })),
      };
      delete (payload as any).prices;
      this.store.dispatch(new CreateWebsitePrice(payload)).subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Bảng giá được khởi tạo thành công'
            )
          );
          const queryParams = {
            type:
              payload.publisherPriceType === PublisherPriceType.Website
                ? 'Website'
                : 'Zone',
            websiteId: payload.websiteId,
          };
          this.router.navigate(['website-price/list'], { queryParams }).then();
        },
        error: (err) => {
          const errorMessage = err?.error?.errors?.[0];
          if (errorMessage === 'Thời gian áp dụng bảng giá đã tồn tại') {
            this.createForm
              .get(this.createFormFields.startDate.name)
              .setErrors({ existed: true });
            this.changeDetectorRef.detectChanges();
          } else {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.error,
                'Có lỗi xảy ra trong quá trình tạo bảng giá'
              )
            );
          }
        },
      });
    } else {
      const websitePriceId =
        this.activatedRoute.snapshot.queryParamMap.get('websitePriceId');
      const payload: Partial<IWebsitePriceUpdateRequest> = {
        ...this.createForm.value,
        templatePrices: this.createForm.value.prices
          .filter((item) => !!item.buyPrice && !!item.sellPrice)
          .map((item) => ({
            ...item,
            buyPrice: WebsitePriceAddComponent.convertPrice(item.buyPrice),
            sellPrice: WebsitePriceAddComponent.convertPrice(item.sellPrice),
          })),
        isFullUpdate: true,
        id: websitePriceId,
      };
      delete (payload as any).prices;
      this.store.dispatch(new UpdateWebsitePrice(payload)).subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Cập nhật bảng giá thành công'
            )
          );
          const queryParams = {
            type:
              payload.publisherPriceType === PublisherPriceType.Website
                ? 'Website'
                : 'Zone',
            websiteId: payload.websiteId,
          };
          this.router.navigate(['website-price/list'], { queryParams }).then();
        },
        error: (err) => {
          const errorMessage = err?.error?.errors?.[0];
          if (errorMessage === 'Thời gian áp dụng bảng giá đã tồn tại') {
            this.createForm
              .get(this.createFormFields.startDate.name)
              .setErrors({ existed: true });
            this.changeDetectorRef.detectChanges();
          } else {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.error,
                'Có lỗi xảy ra trong quá trình cập nhật bảng giá'
              )
            );
          }
        },
      });
    }
  }

  private loadWebsites(websiteId: string | undefined) {
    this.store
      .dispatch(
        new GetWebsiteList({
          page: this.websitePage,
          pageSize: this.websitePageSize,
          keyWord: this.websiteKeyword,
        })
      )
      .pipe(
        expand((data) => {
          if (
            !websiteId ||
            data.settings.websites.find((item) => item.id === websiteId)
          ) {
            return EMPTY;
          }
          this.websitePage += 1;
          return this.store.dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          );
        })
      )
      .subscribe(() => {});
  }

  private setPriceForm(advertisingTemplates: IAdvertisingTemplate[]) {
    const publisherPriceType = this.createForm.get(
      this.createFormFields.publisherPriceType.name
    ).value as PublisherPriceType;
    if (publisherPriceType === PublisherPriceType.Zone) {
      const zoneId = this.createForm.get(this.createFormFields.zoneId.name)
        .value as string;
      if (!zoneId) {
        this.setFullPriceForm(advertisingTemplates);
        return;
      }
      this.zones$
        .pipe(
          map((item) => item.data),
          take(1)
        )
        .subscribe({
          next: (zones) => {
            const zone = zones.find((item) => item.id === zoneId);
            if (!zone) {
              this.setFullPriceForm(advertisingTemplates);
              return;
            }
            const currentPrices = this.createForm.get(
              this.createFormFields.prices.name
            ).value;
            while (this.pricesFormArray.length !== 0) {
              this.pricesFormArray.removeAt(0);
            }
            zone.zoneTemplates?.forEach((item) => {
              const currentPrice = currentPrices.find(
                (price) => price.templateType === item.templateType
              );
              const currentTemplate = advertisingTemplates.find(
                (x) => x.templateType === item.templateType
              );
              this.pricesFormArray.push(
                this.generatePriceForm(
                  item.templateType,
                  currentTemplate.campaignType,
                  currentPrice
                )
              );
            });
          },
        });
    } else {
      this.setFullPriceForm(advertisingTemplates);
    }
  }

  private setFullPriceForm(advertisingTemplates: IAdvertisingTemplate[]) {
    const currentPrices = this.createForm.get(
      this.createFormFields.prices.name
    ).value;
    while (this.pricesFormArray.length !== 0) {
      this.pricesFormArray.removeAt(0);
    }
    advertisingTemplates?.forEach((item) => {
      const currentPrice = currentPrices.find(
        (price) => price.templateType === item.templateType
      );
      this.pricesFormArray.push(
        this.generatePriceForm(
          item.templateType,
          item.campaignType,
          currentPrice
        )
      );
    });
  }

  private startDateValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.value) {
        return { required: true };
      }
      const value =
        typeof control.value === 'string'
          ? new Date(control.value)
          : (control.value as Date);
      if (dayjs(value).isBefore(dayjs())) {
        return { notInThePast: true };
      }
      return null;
    };
  }

  private priceValidator(control: AbstractControl): ValidationErrors | null {
    let buyPriceError: { [key: string]: boolean } | null = null;
    let sellPriceError: { [key: string]: boolean } | null = null;

    const buyPrice = control.get(websitePriceDetailFields.buyPrice.name).value;
    const sellPrice = control.get(
      websitePriceDetailFields.sellPrice.name
    ).value;
    const buyPriceType = control.get(websitePriceDetailFields.buyPriceType.name)
      .value as EPriceType;
    const sellPriceType = control.get(
      websitePriceDetailFields.sellPriceType.name
    ).value as EPriceType;

    if (
      !WebsitePriceAddComponent.isEmpty(buyPrice) ||
      !WebsitePriceAddComponent.isEmpty(sellPrice)
    ) {
      if (WebsitePriceAddComponent.isEmpty(buyPrice)) {
        buyPriceError = { required: true };
      } else {
        const buyPriceNumber = WebsitePriceAddComponent.convertPrice(buyPrice);
        const sellPriceNumber =
          WebsitePriceAddComponent.convertPrice(sellPrice);
        if (buyPriceNumber.toString().length > 10) {
          buyPriceError = { maxlength: true };
        } else if (
          buyPriceType === sellPriceType &&
          sellPrice !== '' &&
          sellPrice !== null &&
          buyPriceNumber >= sellPriceNumber
        ) {
          buyPriceError = { buyPriceGreaterThan: true };
        }
      }
      if (WebsitePriceAddComponent.isEmpty(sellPrice)) {
        sellPriceError = { required: true };
      } else {
        const buyPriceNumber = WebsitePriceAddComponent.convertPrice(buyPrice);
        const sellPriceNumber =
          WebsitePriceAddComponent.convertPrice(sellPrice);
        if (sellPriceNumber.toString().length > 10) {
          sellPriceError = { maxlength: true };
        } else if (
          buyPriceType === sellPriceType &&
          buyPrice !== '' &&
          buyPrice !== null &&
          buyPriceNumber >= sellPriceNumber
        ) {
          sellPriceError = { sellPriceLessThan: true };
        }
      }
    }

    control
      .get(websitePriceDetailFields.buyPrice.name)
      .setErrors(buyPriceError);
    control
      .get(websitePriceDetailFields.sellPrice.name)
      .setErrors(sellPriceError);
    return null;
  }

  private createFormValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    // validate zoneId
    const zoneId = control.get(websitePriceCreateFormFields.zoneId.name).value;
    const publisherPriceType = control.get(
      websitePriceCreateFormFields.publisherPriceType.name
    ).value as PublisherPriceType;
    let zoneIdError: { [key: string]: boolean } | null = null;

    if (publisherPriceType === PublisherPriceType.Zone) {
      if (!zoneId) {
        zoneIdError = { required: true };
      }
    }
    control
      .get(websitePriceCreateFormFields.zoneId.name)
      .setErrors(zoneIdError);

    // validate at least 1 templatePrice
    let pricesError: { [key: string]: boolean } | null = null;
    const prices = control.get(websitePriceCreateFormFields.prices.name)
      .value as ITemplatePrice[];
    const anyPrice = prices.some(
      (price) => !!price.buyPrice && !!price.sellPrice
    );
    if (!anyPrice) {
      pricesError = { noPrice: true };
    }
    control
      .get(websitePriceCreateFormFields.prices.name)
      .setErrors(pricesError);
    return null;
  }

  private generatePriceForm(
    templateType: ETemplateType,
    campaignType: CampaignType,
    templatePrice?: ITemplatePrice
  ) {
    return this.formBuilder.group(
      {
        [this.detailFormFields.templateType.name]: [
          templateType,
          [Validators.required],
        ],
        [this.detailFormFields.campaignType.name]: [campaignType],
        [this.detailFormFields.buyPriceType.name]: [
          templatePrice?.buyPriceType ?? EPriceType.CPM,
          [Validators.required],
        ],
        [this.detailFormFields.buyPrice.name]: [
          templatePrice?.buyPrice ?? null,
          [
            Validators.required,
            Validators.min(this.detailFormFields.buyPrice.validationParams.min),
          ],
        ],
        [this.detailFormFields.sellPriceType.name]: [
          templatePrice?.sellPriceType ?? EPriceType.CPC,
          [Validators.required],
        ],
        [this.detailFormFields.sellPrice.name]: [
          templatePrice?.sellPrice ?? null,
          [
            Validators.required,
            Validators.min(
              this.detailFormFields.sellPrice.validationParams.min
            ),
          ],
        ],
      },
      { validators: this.priceValidator }
    );
  }

  private buildCreateForm(advertisingTemplates: IAdvertisingTemplate[]) {
    const priceConfigs = advertisingTemplates.map((template) =>
      this.generatePriceForm(template.templateType, template.campaignType)
    );

    const config = {
      [this.createFormFields.websiteId.name]: [null, [Validators.required]],
      [this.createFormFields.publisherPriceType.name]: [
        PublisherPriceType.Website,
        [Validators.required],
      ],
      [this.createFormFields.startDate.name]: [
        undefined,
        [this.startDateValidator()],
      ],
      [this.createFormFields.zoneId.name]: [null, []],
      [this.createFormFields.prices.name]: this.formBuilder.array(priceConfigs),
    };
    this.createForm = this.formBuilder.group(config, {
      validators: this.createFormValidator,
    });
    this.changeDetectorRef.detectChanges();
  }
}
