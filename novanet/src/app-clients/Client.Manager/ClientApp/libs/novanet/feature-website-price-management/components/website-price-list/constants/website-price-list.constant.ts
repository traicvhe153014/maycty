import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import {
  IWebsitePriceResponse,
  WebsitePriceStatusEnum,
} from '@features/feature-website-price-management/store';
import { WebsitePriceListEnum } from '@features/feature-website-price-management/components/website-price-list/enums';
import { IStatusBlockConfig } from '@core/models';
import { ZonePriceListEnum } from '@features/feature-website-price-management/components/zone-price-list/enums';

export const WebsitePriceListSettingTable = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '125vw',
  summaryRow: false,
  sortType: 'single',
} as DataTableSettingModel;

export const WebsitePriceListColumns = [
  {
    id: WebsitePriceListEnum.WEBSITE,
    title: 'Website',
    width: '300px',
    pinLeft: true,
  },
  {
    id: WebsitePriceListEnum.CAMPAIGN_TYPE,
    title: 'Loại QC',
    width: '120px',
    pinLeft: true,
  },
  {
    id: WebsitePriceListEnum.STATUS,
    title: 'Trạng thái giá',
    sort: true,
    name: 'Status',
    width: '120px',
    pinLeft: true,
    ascending: true,
    descending: false,
  },
  {
    id: WebsitePriceListEnum.PRICE_TYPE,
    title: 'Loại giá',
    sort: true,
    name: 'PriceType',
    width: '100px',
    pinLeft: true,
  },
  {
    id: WebsitePriceListEnum.NOTIFICATION_BANNER,
    title: 'Notification Banner (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.CATFISH_COLLAB_BRANDING,
    title: 'Catfish Collab Branding (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.POPUP_BANNER_BRANDING,
    title: 'Popup Banner Branding (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.CATFISH_ECOM,
    title: 'Catfish Ecom (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.MOBILE_BANNER,
    title: 'Mobile Banner Card (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.INTERACTIVE_BANNER,
    title: 'Interactive Banner (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.FLYING_CARPET,
    title: 'Flying Carpet (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.IN_READ_ECOMMERCE,
    title: 'In Read Ecommerce (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.POST_IN_READ,
    title: 'Post In Read (VNĐ)',
    width: '200px',
  },
  {
    id: WebsitePriceListEnum.END_DATE,
    title: 'Ngày kết thúc',
    width: '150px',
    sort: true,
    name: 'EndDate',
  },
] as ISettingColumnTable<IWebsitePriceResponse, any>[];

export const WebsitePriceStatusConfigs: IStatusBlockConfig<WebsitePriceStatusEnum>[] =
  [
    {
      value: WebsitePriceStatusEnum.Running,
      text: 'Đang chạy',
      backgroundColor: 'rgba(0,97,193,0.1)',
      textColor: '#0061C1',
    },
    {
      value: WebsitePriceStatusEnum.NotStarted,
      text: 'Chưa chạy',
      backgroundColor: 'rgba(79,79,79,0.1)',
      textColor: '#4F4F4F',
    },
    {
      value: WebsitePriceStatusEnum.Archived,
      text: 'Lịch sử',
      backgroundColor: 'rgba(242,153,74,0.1)',
      textColor: '#F2994A',
    },
  ];
