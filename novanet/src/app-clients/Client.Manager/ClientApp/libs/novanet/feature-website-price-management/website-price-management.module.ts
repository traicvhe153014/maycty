import { NgModule } from '@angular/core';
import { WebsitePriceManagementComponent } from './website-price-management.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WebsitePriceManagementRoutingModule } from './website-price-management.routing';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import {
  WebsitePriceListModule,
  ZonePriceListModule,
} from '@features/feature-website-price-management/components';

@NgModule({
  declarations: [WebsitePriceManagementComponent],
  imports: [
    CommonModule,
    RouterModule,
    WebsitePriceManagementRoutingModule,
    NzTabsModule,
    ZonePriceListModule,
    WebsitePriceListModule,
  ],
  exports: [WebsitePriceManagementComponent],
})
export class WebsitePriceManagementModule {}
