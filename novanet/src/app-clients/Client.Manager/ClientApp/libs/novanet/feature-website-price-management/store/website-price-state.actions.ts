import {
  IGetWebsitePriceResponse,
  IListWebsitePriceRequest,
  IListWebsitePriceResponse,
  IWebsitePriceCreateRequest,
  IWebsitePriceUpdateRequest,
  IWebsitePriceUpdateResponse,
} from './website-price-state.model';

const enum WebsitePriceActions {
  GetWebsitePriceList = '[WebsitePrice] Get List',
  GetWebsitePriceListSuccess = '[WebsitePrice] Get List Success',
  GetWebsitePriceListError = '[WebsitePrice] Get List Error',
  GetMoreWebsitePriceList = '[WebsitePrice] Get More List',
  GetMoreWebsitePriceListSuccess = '[WebsitePrice] Get More List Success',
  GetMoreWebsitePriceListError = '[WebsitePrice] Get More List Error',
  GetWebsitePrice = '[WebsitePrice] Get',
  GetWebsitePriceSuccess = '[WebsitePrice] Get Success',
  GetWebsitePriceError = '[WebsitePrice] Get Error',
  CreateWebsitePrice = '[WebsitePrice] Create',
  CreateWebsitePriceSuccess = '[WebsitePrice] Create Success',
  CreateWebsitePriceError = '[WebsitePrice] Create Error',
  UpdateWebsitePrice = '[WebsitePrice] Update',
  UpdateWebsitePriceSuccess = '[WebsitePrice] Update Success',
  UpdateWebsitePriceError = '[WebsitePrice] Update Error',
  ClearAllWebsitePricesData = '[WebsitePrice] Clear All',
}

export class GetWebsitePriceList {
  public static readonly type = WebsitePriceActions.GetWebsitePriceList;

  constructor(public payload: IListWebsitePriceRequest) {}
}

export class GetWebsitePriceListSuccess {
  public static readonly type = WebsitePriceActions.GetWebsitePriceListSuccess;

  constructor(
    public response: IListWebsitePriceResponse,
    public payload: IListWebsitePriceRequest
  ) {}
}

export class GetWebsitePriceListError {
  public static readonly type = WebsitePriceActions.GetWebsitePriceListError;

  constructor(public error: any) {}
}

export class GetMoreWebsitePriceList {
  public static readonly type = WebsitePriceActions.GetMoreWebsitePriceList;

  constructor(public payload: IListWebsitePriceRequest) {}
}

export class GetMoreWebsitePriceListSuccess {
  public static readonly type =
    WebsitePriceActions.GetMoreWebsitePriceListSuccess;

  constructor(
    public response: IListWebsitePriceResponse,
    public payload: IListWebsitePriceRequest
  ) {}
}

export class GetMoreWebsitePriceListError {
  public static readonly type =
    WebsitePriceActions.GetMoreWebsitePriceListError;

  constructor(public error: any) {}
}

export class GetWebsitePrice {
  public static readonly type = WebsitePriceActions.GetWebsitePrice;

  constructor(public id: string) {}
}

export class GetWebsitePriceSuccess {
  public static readonly type = WebsitePriceActions.GetWebsitePriceSuccess;

  constructor(public response: IGetWebsitePriceResponse) {}
}

export class GetWebsitePriceError {
  public static readonly type = WebsitePriceActions.GetWebsitePriceError;

  constructor(public error: any) {}
}

export class CreateWebsitePrice {
  public static readonly type = WebsitePriceActions.CreateWebsitePrice;

  constructor(public payload: Partial<IWebsitePriceCreateRequest>) {}
}

export class CreateWebsitePriceSuccess {
  public static readonly type = WebsitePriceActions.CreateWebsitePriceSuccess;

  constructor(public response: IWebsitePriceUpdateResponse) {}
}

export class CreateWebsitePriceError {
  public static readonly type = WebsitePriceActions.CreateWebsitePriceError;

  constructor(public error: any) {}
}

export class UpdateWebsitePrice {
  public static readonly type = WebsitePriceActions.UpdateWebsitePrice;

  constructor(public payload: Partial<IWebsitePriceUpdateRequest>) {}
}

export class UpdateWebsitePriceSuccess {
  public static readonly type = WebsitePriceActions.UpdateWebsitePriceSuccess;

  constructor(public response: IWebsitePriceUpdateResponse) {}
}

export class UpdateWebsitePriceError {
  public static readonly type = WebsitePriceActions.UpdateWebsitePriceError;

  constructor(public error: any) {}
}

export class ClearAllWebsitePricesData {
  public static readonly type = WebsitePriceActions.ClearAllWebsitePricesData;
}
