import { Pipe, PipeTransform } from '@angular/core';
import { IDetailedWebsitePrice } from '@features/feature-website-price-management/store';

@Pipe({
  name: 'displayTemplatePrice',
})
export class DisplayTemplatePricePipe implements PipeTransform {
  transform(value: any): IDetailedWebsitePrice[] {
    return value.slice(1);
  }
}
