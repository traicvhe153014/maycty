import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { WebsitePriceState } from './website-price.state';
import { WebsitePriceService } from './website-price-state.service';

@NgModule({
  imports: [NgxsModule.forFeature([WebsitePriceState])],
  providers: [WebsitePriceService],
})
export class WebsitePriceStateModule {}
