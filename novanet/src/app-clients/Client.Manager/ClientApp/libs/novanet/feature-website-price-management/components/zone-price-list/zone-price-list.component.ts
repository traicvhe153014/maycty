import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  ClearAllWebsitePricesData,
  EPriceType,
  GetWebsitePriceList,
  IDetailedWebsitePrice,
  IWebsitePriceResponse,
  IWebsitePriceUpdateRequest,
  PublisherPriceType,
  UpdateWebsitePrice,
  WebsitePriceState,
  WebsitePriceStatusEnum,
} from '@features/feature-website-price-management/store';
import { EMPTY, expand, Observable } from 'rxjs';
import {
  GetMoreWebsiteList,
  GetWebsiteList,
  IWebsitesResponse,
  SettingsState,
} from '@shared/settings/store';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { IFieldDetail } from '@data-table/models';
import { ESortType } from '@core/enums';
import { ZonePriceListEnum } from '@features/feature-website-price-management/components/zone-price-list/enums';
import {
  ZonePriceListColumns,
  ZonePriceListSettingTable,
  ZonePriceStatusConfigs,
} from '@features/feature-website-price-management/components/zone-price-list/constants';
import { ETemplateType } from '@features/campaign-management/enums';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { cloneDeep } from 'lodash';
import { campaignTypeLabels } from '@features/campaign-management/constants';

@Component({
  selector: 'novanet-zone-price-list',
  templateUrl: './zone-price-list.component.html',
})
export class ZonePriceListComponent implements OnInit, OnDestroy {
  @Select(WebsitePriceState.getList) public zonePrices$: Observable<
    IWebsitePriceResponse[]
  >;
  @Select(WebsitePriceState.getPage) public currentPage$: Observable<number>;
  @Select(WebsitePriceState.getHasMorePages)
  public hasMorePages$: Observable<boolean>;
  @Select(WebsitePriceState.getLoading) public loading$: Observable<boolean>;
  @Select(WebsitePriceState.getTotal) public total$: Observable<number>;
  @Select(SettingsState.getWebsites) websites$: Observable<IWebsitesResponse>;
  @Select(SettingsState.getLoadingWebsites)
  websitesLoading$: Observable<boolean>;

  public readonly zonePriceListEnum = ZonePriceListEnum;
  public columns = ZonePriceListColumns;
  public readonly settings = ZonePriceListSettingTable;
  public readonly zonePriceStatusConfigs = ZonePriceStatusConfigs;
  public readonly websitePriceStatusEnum = WebsitePriceStatusEnum;
  public readonly eTemplateType = ETemplateType;
  public readonly ePriceType = EPriceType;

  public websiteKeyword = '';
  public sorts = ['status'];
  public readonly baseSorts = ['status'];
  public readonly pageSize = 50;
  public selectedWebsiteId: string;
  public expandedZoneIds: { [key: string]: boolean } = {};
  public switchLoadings: { [key: string]: boolean } = {};
  public isConfirmIsActiveModalVisible = false;
  public confirmingZonePrice: IDetailedWebsitePrice | undefined;
  public campaignTypeLabels = campaignTypeLabels;

  private websitePage = 1;
  private readonly websitePageSize = 24;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.selectedWebsiteId =
      this.activatedRoute.snapshot.queryParamMap.get('websiteId');
    this.store
      .dispatch(
        new GetWebsiteList({
          page: this.websitePage,
          pageSize: this.websitePageSize,
          keyWord: this.websiteKeyword,
        })
      )
      .pipe(
        expand((data) => {
          if (
            !this.selectedWebsiteId ||
            data.settings.websites.find(
              (item) => item.id === this.selectedWebsiteId
            )
          ) {
            return EMPTY;
          }
          this.websitePage += 1;
          return this.store.dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          );
        })
      )
      .subscribe(() => {});
    if (this.selectedWebsiteId) {
      this.store.dispatch(
        new GetWebsitePriceList({
          page: 1,
          pageSize: this.pageSize,
          sorts: this.sorts,
          websiteId: this.selectedWebsiteId,
          publisherPriceType: PublisherPriceType.Zone,
        })
      );
    }
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearAllWebsitePricesData());
  }

  public toggleExpandedZone(id: string) {
    this.expandedZoneIds[id] = !this.expandedZoneIds[id];
  }

  public onSearch(event: string) {
    this.websiteKeyword = event;
    this.websitePage = 1;
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
        keyWord: this.websiteKeyword,
      })
    );
  }

  public loadMoreWebsites() {
    this.websitesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.websitePage += 1;
        this.store.dispatch(
          new GetMoreWebsiteList({
            page: this.websitePage,
            pageSize: this.websitePageSize,
            keyWord: this.websiteKeyword,
          })
        );
      },
    });
  }

  public searchZonePrices(values?: IFieldDetail[]) {
    if (values.length) {
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.store.dispatch(
      new GetWebsitePriceList({
        page: 1,
        pageSize: this.pageSize,
        sorts: this.sorts,
        websiteId: this.selectedWebsiteId,
        publisherPriceType: PublisherPriceType.Zone,
      })
    );
  }

  public onSelectWebsite(websiteId: string) {
    this.selectedWebsiteId = websiteId;
    if (this.selectedWebsiteId) {
      this.store.dispatch(
        new GetWebsitePriceList({
          page: 1,
          pageSize: this.pageSize,
          sorts: this.sorts,
          websiteId: this.selectedWebsiteId,
          publisherPriceType: PublisherPriceType.Zone,
        })
      );
    } else {
      this.store.dispatch(new ClearAllWebsitePricesData());
    }

    this.router
      .navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: { websiteId },
        queryParamsHandling: 'merge',
      })
      .then();
  }

  public onPriceIsActiveChange(websitePrice: IDetailedWebsitePrice) {
    if (websitePrice.isActive) {
      this.confirmingZonePrice = cloneDeep(websitePrice);
      this.isConfirmIsActiveModalVisible = true;
    } else {
      this.clickConfirmIsActive(websitePrice);
    }
  }

  handleCancelConfirmIsActive() {
    this.confirmingZonePrice = undefined;
    this.isConfirmIsActiveModalVisible = false;
  }

  public clickConfirmIsActive(websitePrice: IDetailedWebsitePrice | undefined) {
    if (!websitePrice) {
      return;
    }
    if (this.switchLoadings[websitePrice.websitePriceId]) {
      return;
    }
    const payload: Partial<IWebsitePriceUpdateRequest> = {
      id: websitePrice.websitePriceId,
      isActive: !websitePrice.isActive,
      isFullUpdate: false,
    };
    this.switchLoadings[websitePrice.websitePriceId] = true;
    this.changeDetectorRef.detectChanges();
    this.store.dispatch(new UpdateWebsitePrice(payload)).subscribe({
      next: (response) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.switchLoadings[websitePrice.websitePriceId] = false;
        this.isConfirmIsActiveModalVisible = false;
        this.confirmingZonePrice = undefined;
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.switchLoadings[websitePrice.websitePriceId] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }
}
