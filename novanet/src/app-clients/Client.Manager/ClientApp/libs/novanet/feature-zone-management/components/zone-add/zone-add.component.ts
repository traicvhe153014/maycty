import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  CreateZone,
  GetMoreWebsiteList,
  GetWebsiteList,
  IWebsitesResponse,
  IZoneCreateRequest,
  SettingsState,
} from '@shared/settings/store';
import { EMPTY, expand, Observable, Subject } from 'rxjs';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  CampaignTypeOptions,
  ZoneBackupTypeOptions,
  zoneCreateFormFieldErrors,
  zoneCreateFormFields,
} from '@features/feature-zone-management/components/zone-add/constants';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ActivatedRoute, Router } from '@angular/router';
import { take, takeUntil } from 'rxjs/operators';
import {
  AdvertisingState,
  CampaignType,
  IAdvertisingStateModel,
  IAdvertisingTemplate,
  ListAdvertisingTemplate,
} from '@features/campaign-management/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { ETemplateType } from '@features/campaign-management/enums';
import { StorageService } from '@core/services';
import { isFileSizeValid } from '@core/utils';
import { ZoneBackupType } from '@features/feature-zone-management/components/enums/zone-list.enum';
import { forEach, template } from 'lodash';

@Component({
  selector: 'novanet-zone-add',
  templateUrl: './zone-add.component.html',
})
export class ZoneAddComponent implements OnInit, OnDestroy {
  public formType: string; // add | edit
  public createForm: FormGroup;
  public readonly createFormFields = zoneCreateFormFields;
  public readonly createFormFieldErrors = zoneCreateFormFieldErrors;
  public readonly campaignTypeOptions = CampaignTypeOptions;
  public readonly zoneBackupType = ZoneBackupType;
  public readonly zoneBackupTypeOptions = ZoneBackupTypeOptions;
  public campaignType = CampaignType;
  public checkCampaignType: { [key: string]: boolean } = {};

  @Select(SettingsState.getWebsites) websites$: Observable<IWebsitesResponse>;
  @Select(SettingsState.getLoadingWebsites)
  websitesLoading$: Observable<boolean>;
  @Select(AdvertisingState.getGroupedTemplates) groupedTemplates$: Observable<{
    [key: string]: IAdvertisingTemplate[];
  }>;
  @Select(AdvertisingState.getTemplateSizes) templateSizes$: Observable<
    { label: string; value: string }[]
  >;
  public loadingTemplate = false;
  public loadingWebsites = false;

  public websiteKeyword = '';
  public backUpFileToUpload = false;
  public backUpNameFile: string | undefined;
  public messageError: string;
  public fileListBackUp: FileList;
  public formSubmitting = false;

  private websitePage = 1;
  private readonly websitePageSize = 50;

  private readonly destroy$: Subject<void> = new Subject<void>();

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private notification: NzNotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private storageService: StorageService
  ) {}

  public get isLoading() {
    return this.loadingWebsites || this.loadingTemplate;
  }

  public hasAnyTemplate(campaignType: CampaignType): boolean {
    let check = false;

    this.groupedTemplates$.pipe().subscribe((data) => {
      data[this.createForm.get(this.createFormFields.size.name).value]
        ? data[
            this.createForm.get(this.createFormFields.size.name).value
          ].forEach((value) => {
            if (value.campaignType === campaignType) {
              check = true;
            }
          })
        : (check = false);
    });
    return check;
  }

  ngOnInit(): void {
    this.loadingTemplate = true;
    this.loadingWebsites = true;

    this.store
      .dispatch(new ListAdvertisingTemplate())
      .pipe(take(1))
      .subscribe({
        next: (data) => {
          this.createForm
            .get(this.createFormFields.templateTypes.name)
            .markAsUntouched();
          this.loadingTemplate = false;
          this.changeDetectorRef.detectChanges();
        },
      });

    this.formType = this.router.url.includes('edit') ? 'edit' : 'add';
    this.buildCreateForm();

    const websiteId =
      this.activatedRoute.snapshot.queryParamMap.get('websiteId');
    this.loadWebsites(websiteId);
    if (!!websiteId) {
      this.createForm.patchValue({ websiteId });
    }

    this.createForm
      .get('size')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: () => {
          this.createForm.patchValue({ templateTypes: [] });
        },
      });

    this.createForm
      .get('size')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: () => {
          if (
            this.backUpFileToUpload &&
            this.fileListBackUp &&
            (!this.messageError || this.messageError === 'Sai kích thước zone')
          ) {
            const fr = new FileReader();
            const zoneWidth = this.createForm.get('size').value.split('x')[0];
            const zoneHeight = this.createForm.get('size').value.split('x')[1];
            const currentZoneRatio = zoneWidth / zoneHeight;
            fr.onload = () => {
              const img = new Image();
              img.onload = () => {
                const width = img.width;
                const height = img.height;
                const ratio = width / height;
                if (currentZoneRatio !== ratio) {
                  this.messageError = 'Sai kích thước zone';
                  this.changeDetectorRef.detectChanges();
                  return;
                }
                this.messageError = null;
                this.changeDetectorRef.detectChanges();
              };
              img.src = fr.result as any;
            };
            fr.readAsDataURL(this.fileListBackUp[0]);
          }
        },
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public isTemplateTypeChecked(templateType: ETemplateType): boolean {
    const templateTypes = this.createForm.get('templateTypes')
      .value as ETemplateType[];
    return !!templateTypes.some((item) => item === templateType);
  }

  public onSearchWebsite(event: string) {
    this.websiteKeyword = event;
    this.websitePage = 1;
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
        keyWord: this.websiteKeyword,
      })
    );
  }

  public loadMoreWebsites() {
    return this.websitesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.websitePage += 1;
        this.store
          .dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          )
          .subscribe({
            error: () => {
              this.websitePage -= 1;
            },
          });
      },
    });
  }

  public toggleTemplateType(event: Event, templateType: ETemplateType) {
    const templateTypes = this.createForm.get('templateTypes')
      .value as ETemplateType[];
    if ((event.target as HTMLInputElement).checked) {
      this.createForm.patchValue({
        templateTypes: [...templateTypes, templateType],
      });
    } else {
      this.createForm.patchValue({
        templateTypes: templateTypes.filter((item) => item !== templateType),
      });
    }
    this.createForm
      .get(this.createFormFields.templateTypes.name)
      .markAsTouched();
  }

  public handleZoneBackUpInput(files: FileList) {
    const fileName = files[0].name;
    this.fileListBackUp = files;
    this.backUpNameFile = files[0].name;
    this.backUpFileToUpload = true;
    this.createForm.patchValue({
      backupFile: fileName,
    });
    if (!isFileSizeValid([files.item(0)], 512000)) {
      this.messageError = 'Quá dung lượng';
      return;
    }
    if (!/(?:.jpg|.jpeg|.png|.html5|.html(?!\\))+$/.test(fileName)) {
      this.messageError = 'Sai định dạng';
      return;
    }
    const index = this.createForm.value.size.split('x')[1].indexOf(' ');
    const fr = new FileReader();
    const zoneWidth = this.createForm.get('size').value.split('x')[0];
    const zoneHeight = this.createForm.value.size.split('x')[1].slice(0, index);
    const currentZoneRatio = zoneWidth / zoneHeight;
    fr.onload = () => {
      if (fileName.endsWith('html') || fileName.endsWith('html5')) {
        this.messageError = null;
        return;
      }
      const img = new Image();
      img.onload = () => {
        const width = img.width;
        const height = img.height;
        const ratio = width / height;
        if (currentZoneRatio !== ratio) {
          this.messageError = 'Sai kích thước zone';
          this.changeDetectorRef.detectChanges();
          return;
        }
        this.messageError = null;
      };
      img.src = fr.result as any;
    };
    fr.readAsDataURL(files[0]);
  }

  public clearZoneBackUpFile(val: string) {
    this.backUpFileToUpload = false;
    this.backUpNameFile = null;
    this.messageError = null;
    this.fileListBackUp = undefined;
    this.createForm.patchValue({
      backupFile: '',
    });
  }

  public submitForm() {
    let isValid = true;
    this.createForm.markAllAsTouched();
    if (!this.createForm.valid) {
      isValid = false;
    }
    if (!isValid) {
      return;
    }
    if (this.formSubmitting) {
      return;
    }
    if (this.formType === 'add') {
      const index = this.createForm.value.size.split('x')[1].indexOf(' ');
      const payload: Partial<IZoneCreateRequest> = {
        ...this.createForm.value,
        width: this.createForm.value.size.split('x')[0],
        height: this.createForm.value.size.split('x')[1].slice(0, index),
      };
      delete (payload as any).size;
      delete (payload as any).backupFile;
      if (!payload.usingBackup) {
        payload.backupType = null;
      }
      if (
        payload.usingBackup &&
        payload.backupType === ZoneBackupType.BACKUP_FILE
      ) {
        if (!!this.messageError) {
          return;
        }
        this.formSubmitting = true;
        this.storageService.upload(this.fileListBackUp[0]).subscribe({
          next: (fileName) => {
            payload.backupCode = fileName;
            this.dispatchCreateZone(payload);
          },
          error: (error) => {
            this.formSubmitting = false;
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.error,
                'Có lỗi xảy ra trong quá trình tạo vùng quảng cáo'
              )
            );
          },
        });
      } else {
        this.formSubmitting = true;
        this.dispatchCreateZone(payload);
      }
    } else {
    }
  }

  private dispatchCreateZone(payload: Partial<IZoneCreateRequest>) {
    this.store.dispatch(new CreateZone(payload)).subscribe({
      next: () => {
        this.formSubmitting = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Tạo vùng quảng cáo thành công'
          )
        );
        this.router
          .navigate(['zone/list'], {
            queryParams: { websiteId: payload.websiteId },
          })
          .then();
      },
      error: (err) => {
        this.formSubmitting = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra trong quá trình tạo vùng quảng cáo'
          )
        );
      },
    });
  }

  private loadWebsites(websiteId: string | undefined) {
    this.store
      .dispatch(
        new GetWebsiteList({
          page: this.websitePage,
          pageSize: this.websitePageSize,
          keyWord: this.websiteKeyword,
        })
      )
      .pipe(
        expand((data) => {
          if (
            !websiteId ||
            data.settings.websites.find((item) => item.id === websiteId)
          ) {
            this.loadingWebsites = false;
            this.changeDetectorRef.detectChanges();
            return EMPTY;
          }
          this.websitePage += 1;
          return this.store.dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          );
        })
      )
      .subscribe(() => {});
  }

  private templateTypeValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.value || !control.value.length) {
        return { required: true };
      }
      return null;
    };
  }

  private createFormValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    const usingBackup = control.get(zoneCreateFormFields.usingBackup.name)
      .value as boolean;
    const backupType = control.get(zoneCreateFormFields.backupType.name)
      .value as ZoneBackupType;
    const backupCode = control.get(zoneCreateFormFields.backupCode.name).value;
    let backupCodeError: { [key: string]: boolean } | null = null;

    if (usingBackup && backupType === ZoneBackupType.BACKUP_CODE) {
      if (!backupCode) {
        backupCodeError = { required: true };
      }
    }
    control
      .get(zoneCreateFormFields.backupCode.name)
      .setErrors(backupCodeError);
    return null;
  }

  private buildCreateForm() {
    const config = {
      [this.createFormFields.websiteId.name]: [null, [Validators.required]],
      [this.createFormFields.name.name]: [
        '',
        [
          Validators.required,
          Validators.minLength(
            this.createFormFields.name.validationParams.minLength
          ),
          Validators.maxLength(
            this.createFormFields.name.validationParams.maxLength
          ),
        ],
      ],
      [this.createFormFields.size.name]: [null, [Validators.required]],
      [this.createFormFields.campaignType.name]: [
        CampaignType.Ecommerce,
        [Validators.required],
      ],
      [this.createFormFields.templateTypes.name]: [
        [],
        [this.templateTypeValidator()],
      ],
      [this.createFormFields.usingBackup.name]: [false, [Validators.required]],
      [this.createFormFields.backupType.name]: [
        ZoneBackupType.BACKUP_CODE,
        [Validators.required],
      ],
      [this.createFormFields.backupCode.name]: ['', []],
      [this.createFormFields.backupFile.name]: ['', []],
      [this.createFormFields.redirectLink.name]: [
        '',
        [
          Validators.pattern(
            this.createFormFields.redirectLink.validationParams.pattern
          ),
        ],
      ],
    };
    this.createForm = this.formBuilder.group(config, {
      validators: this.createFormValidator,
    });
  }
}
