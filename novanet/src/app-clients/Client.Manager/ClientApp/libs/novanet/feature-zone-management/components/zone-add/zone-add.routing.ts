import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ZoneAddComponent } from './zone-add.component';

const routes: Route[] = [
  {
    path: '',
    component: ZoneAddComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoneAddRoutingModule {}
