import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

const routes: Route[] = [
  {
    path: 'list',
    loadChildren: () =>
      import('@features/feature-zone-management/components/zone-list').then(
        (m) => m.ZoneListModule
      ),
  },
  {
    path: 'add',
    loadChildren: () =>
      import('@features/feature-zone-management/components/zone-add').then(
        (m) => m.ZoneAddModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoneManagementRoutingModule {}
