import { IFormFieldErrors, IFormFields } from '@models';
import { urlPattern } from '@shared/sharing/constants';
import { CampaignType } from '@features/campaign-management/store';
import { ZoneBackupType } from '@features/feature-zone-management/components/enums/zone-list.enum';

export const zoneCreateFormFields: IFormFields = {
  websiteId: {
    name: 'websiteId',
    validationParams: {
      required: true,
    },
  },
  name: {
    name: 'name',
    validationParams: {
      required: true,
      minLength: 10,
      maxLength: 75,
    },
  },
  size: {
    name: 'size',
    validationParams: {
      required: true,
    },
  },
  campaignType: {
    name: 'campaignType',
    validationParams: {
      required: true,
    },
  },
  templateTypes: {
    name: 'templateTypes',
    validationParams: {
      required: true,
    },
  },
  usingBackup: {
    name: 'usingBackup',
    validationParams: {
      required: true,
    },
  },
  backupType: {
    name: 'backupType',
    validationParams: {
      required: true,
    },
  },
  backupCode: {
    name: 'backupCode',
    validationParams: {
      required: true,
    },
  },
  backupFile: {
    name: 'backupFile',
    validationParams: {
      required: true,
    },
  },
  redirectLink: {
    name: 'redirectLink',
    validationParams: {
      pattern: urlPattern,
    },
  },
};

export const zoneCreateFormFieldErrors: IFormFieldErrors = {
  websiteId: [
    {
      type: 'required',
      message: 'Chưa chọn website domain',
    },
  ],
  name: [
    {
      type: 'required',
      message: 'Chưa nhập tên',
    },
    {
      type: 'minlength',
      message: 'Số lượng kí tự tối thiểu là 5 kí tự',
    },
    {
      type: 'maxlength',
      message: 'Số lượng kí tự tối đa là 50 kí tự',
    },
  ],
  templateTypes: [
    {
      type: 'required',
      message: 'Chọn tối thiểu 1 định dạng',
    },
  ],
  size: [
    {
      type: 'required',
      message: 'Chưa chọn kích thước quảng cáo',
    },
  ],
  backupCode: [
    {
      type: 'required',
      message: 'Chưa nhập mã',
    },
  ],
  redirectLink: [
    {
      type: 'pattern',
      message: 'Sai định dạng',
    },
  ],
};

export const CampaignTypeOptions = [
  { value: CampaignType.Ecommerce, label: 'Ecommerce' },
  { value: CampaignType.Display, label: 'Display' },
  { value: CampaignType.Native, label: 'Native' },
];

export const ZoneBackupTypeOptions = [
  {
    label: 'Chạy mã quảng cáo dự phòng',
    value: ZoneBackupType.BACKUP_CODE,
  },
  {
    label: 'Upload file',
    value: ZoneBackupType.BACKUP_FILE,
  },
];
