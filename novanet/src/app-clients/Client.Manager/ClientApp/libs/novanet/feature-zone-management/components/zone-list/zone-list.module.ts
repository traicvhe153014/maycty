import { NgModule } from '@angular/core';
import { ZoneListComponent } from '@features/feature-zone-management/components/zone-list/zone-list.component';
import { CommonModule } from '@angular/common';
import { ZoneListRoutingModule } from '@features/feature-zone-management/components/zone-list/zone-list.routing';
import {
  DataTableModule,
  LoadingIconModule,
  SortListModule,
  WebsiteOptionsModule,
} from '@core';
import { SvgIconModule } from '@core/components/svg-icon';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { ZoneEditableFieldModule } from '@features/feature-zone-management/components/zone-list/components';
import { NzSpinModule } from 'ng-zorro-antd/spin';

@NgModule({
  declarations: [ZoneListComponent],
  imports: [
    CommonModule,
    ZoneListRoutingModule,
    DataTableModule,
    SvgIconModule,
    SwitchNoAnimationModule,
    NovanetInputModule,
    FormsModule,
    LoadingIconModule,
    NzToolTipModule,
    ZoneEditableFieldModule,
    WebsiteOptionsModule,
    NzSpinModule,
    SortListModule,
  ],
  exports: [ZoneListComponent],
})
export class ZoneListModule {}
