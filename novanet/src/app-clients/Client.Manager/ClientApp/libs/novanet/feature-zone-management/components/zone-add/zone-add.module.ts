import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZoneAddComponent } from './zone-add.component';
import { ZoneAddRoutingModule } from './zone-add.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';
import { WebsiteOptionsModule } from '@core';
import { StorageService } from '@core/services';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzSpinModule } from 'ng-zorro-antd/spin';

@NgModule({
  declarations: [ZoneAddComponent],
  imports: [
    CommonModule,
    ZoneAddRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NovanetInputModule,
    WebsiteOptionsModule,
    SvgIconModule,
    NzSpinModule,
  ],
  exports: [ZoneAddComponent],
  providers: [StorageService],
})
export class ZoneAddModule {}
