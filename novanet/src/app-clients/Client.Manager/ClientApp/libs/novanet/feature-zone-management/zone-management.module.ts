import { NgModule } from '@angular/core';
import { ZoneManagementComponent } from './zone-management.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ZoneManagementRoutingModule } from './zone-management.routing';

@NgModule({
  declarations: [ZoneManagementComponent],
  imports: [CommonModule, RouterModule, ZoneManagementRoutingModule],
  exports: [ZoneManagementComponent],
})
export class ZoneManagementModule {}
