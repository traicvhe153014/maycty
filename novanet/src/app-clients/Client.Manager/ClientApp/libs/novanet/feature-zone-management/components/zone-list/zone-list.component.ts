import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  ClearZoneList,
  GetMoreWebsiteList,
  GetMoreZoneList,
  GetWebsiteList,
  GetZoneList,
  IWebsitesResponse,
  IZone,
  IZoneUpdateRequest,
  SettingsState,
  UpdateHasMorePages,
  UpdateZone,
} from '@shared/settings/store';
import { EMPTY, expand, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ZoneListSettingTable } from '@features/feature-zone-management/components/constants/zone-list.metadata';
import { ZoneListColumns } from '@features/feature-zone-management/components/constants/zone-list.constant';
import {
  TypeEdit,
  ZoneBackupType,
  ZoneListEnum,
} from '@features/feature-zone-management/components/enums/zone-list.enum';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { IFieldDetail } from '@data-table/models';
import { ESortType } from '@core/enums';
import { User } from '@models';
import { AuthService } from '@features/auth/store';
import { campaignTypeLabels } from '@features/campaign-management/constants';
import { environment } from '@environment';

@Component({
  selector: 'novanet-zone-list',
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.scss'],
})
export class ZoneListComponent implements OnInit, OnDestroy {
  @Select(SettingsState.getZonesResponse) public zones$: Observable<IZone[]>;
  @Select(SettingsState.getCurrentPage) public currentPage$: Observable<number>;
  @Select(SettingsState.getLoadingZones) public loading$: Observable<boolean>;
  @Select(SettingsState.getWebsites) websites$: Observable<IWebsitesResponse>;
  @Select(SettingsState.getLoadingWebsites)
  websitesLoading$: Observable<boolean>;
  @Select(SettingsState.getTotalZone) totalZone$: Observable<number>;

  @ViewChild('zoneCodeBackUp') public zoneCodeBackUp: ElementRef;

  public sorts = ['-subId'];
  public selectedWebsiteId: string;
  public websiteKeyword = '';
  public columns: any = ZoneListColumns;
  public zoneLoadings: { [key: string]: boolean } = {};
  public backupType = ZoneBackupType;
  public readonly settings = ZoneListSettingTable;
  public readonly pageSize = 24;
  public readonly baseSorts = ['-subId'];
  public readonly zoneListEnum = ZoneListEnum;
  public TypeEdit = TypeEdit;
  public user: User;
  public campaignTypeLabels = campaignTypeLabels;

  private websitePage = 1;
  private readonly websitePageSize = 20;

  constructor(
    private store: Store,
    private authService: AuthService,
    private changeDetectorRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  public get isAdmin(): boolean {
    if (!this.user) {
      return false;
    }
    return (
      this.user.roles === 'Admin' || this.user.roles === 'PublisherManager'
    );
  }

  ngOnInit() {
    this.user = this.authService.getUserData();
    this.selectedWebsiteId =
      this.activatedRoute.snapshot.queryParamMap.get('websiteId');
    this.store
      .dispatch(
        new GetWebsiteList({
          page: this.websitePage,
          pageSize: this.websitePageSize,
          keyWord: this.websiteKeyword,
        })
      )
      .pipe(
        expand((data) => {
          if (
            !this.selectedWebsiteId ||
            data.settings.websites.find(
              (item) => item.id === this.selectedWebsiteId
            )
          ) {
            return EMPTY;
          }
          this.websitePage += 1;
          return this.store.dispatch(
            new GetMoreWebsiteList({
              page: this.websitePage,
              pageSize: this.websitePageSize,
              keyWord: this.websiteKeyword,
            })
          );
        })
      )
      .subscribe(() => {});
    if (this.selectedWebsiteId) {
      this.store.dispatch(
        new GetZoneList({
          page: 1,
          pageSize: this.pageSize,
          sorts: this.sorts,
          websiteId: this.selectedWebsiteId,
          withZoneTemplate: true,
        })
      );
    }
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearZoneList());
  }

  public zoneScript(code: string) {
    return `<script src="${environment.sdkUrl}"></script>\n${code}`;
  }

  public onSearch(event: string) {
    this.websiteKeyword = event;
    this.websitePage = 1;
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
        keyWord: this.websiteKeyword,
      })
    );
    this.zones$.pipe().subscribe((data) => {});
  }

  public loadMoreWebsites() {
    this.websitesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.websitePage += 1;
        this.store.dispatch(
          new GetMoreWebsiteList({
            page: this.websitePage,
            pageSize: this.websitePageSize,
            keyWord: this.websiteKeyword,
          })
        );
      },
    });
  }

  public onSelectWebsite(websiteId: string) {
    this.selectedWebsiteId = websiteId;
    if (this.selectedWebsiteId) {
      this.store.dispatch(
        new GetZoneList({
          page: 1,
          pageSize: this.pageSize,
          sorts: this.sorts,
          websiteId: this.selectedWebsiteId,
          withZoneTemplate: true,
        })
      );
    } else {
      this.store.dispatch(new ClearZoneList());
    }
    this.store.dispatch(new UpdateHasMorePages(true));

    this.router
      .navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: { websiteId },
        queryParamsHandling: 'merge',
      })
      .then();
  }

  public copyRemarketingCode() {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(this.zoneCodeBackUp.nativeElement);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Đã copy vào clipboard'
      )
    );
  }

  public onIsActiveChange(event: boolean, zone: IZone) {
    const payload: IZoneUpdateRequest = {
      id: zone.id,
      usingBackup: event,
    };
    this.zoneLoadings[zone.id] = true;

    this.store.dispatch(new UpdateZone(payload)).subscribe({
      next: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Cập nhật vùng QC thành công'
          )
        );
        this.zoneLoadings[zone.id] = false;
        this.changeDetectorRef.detectChanges();
      },
      error: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra trong quá trình cập nhật vùng QC'
          )
        );
        this.zoneLoadings[zone.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public sortZones(values?: IFieldDetail[]) {
    if (!this.selectedWebsiteId) return;
    if (values.length) {
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.store.dispatch(
      new GetZoneList({
        page: 1,
        pageSize: this.pageSize,
        sorts: this.sorts,
        websiteId: this.selectedWebsiteId,
        withZoneTemplate: true,
      })
    );
  }

  public onNextPage() {
    if (!this.selectedWebsiteId) return;
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMoreZoneList({
            page: currentPage + 1,
            pageSize: this.pageSize,
            sorts: this.sorts,
            websiteId: this.selectedWebsiteId,
            withZoneTemplate: true,
          })
        );
      },
    });
  }
}
