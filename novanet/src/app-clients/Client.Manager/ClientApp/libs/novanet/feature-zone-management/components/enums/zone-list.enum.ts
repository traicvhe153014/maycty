export enum ZoneListEnum {
  INDEX,
  NAME_ZONE,
  ADVERTISING_NAME,
  ZONE_BACKUP,
  CODE_ZONE,
  CAMPAIGN_TYPE
}

export enum ZoneBackupType {
  BACKUP_CODE,
  BACKUP_FILE,
}

export enum TypeEdit {
  EDIT_ZONE_BACKUP,
  ADD_ZONE_BACKUP,
}

export enum TypeErrorFile {
  FORMAT_FILE = 'Sai định dạng',
  SIZE_FILE = 'Sai kích thước zone',
  CAPACITY_FILE = 'Quá dung lượng',
  NONE = '',
}
