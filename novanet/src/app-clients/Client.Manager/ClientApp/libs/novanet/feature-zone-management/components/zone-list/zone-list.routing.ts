import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ZoneListComponent } from '@features/feature-zone-management/components/zone-list/zone-list.component';

const routes: Route[] = [
  {
    path: '',
    component: ZoneListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoneListRoutingModule {}
