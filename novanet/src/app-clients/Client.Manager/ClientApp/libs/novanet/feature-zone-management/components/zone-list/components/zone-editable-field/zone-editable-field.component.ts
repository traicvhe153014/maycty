import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { IZone, IZoneUpdateRequest, UpdateZone } from '@shared/settings/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  TypeEdit, TypeErrorFile,
  ZoneBackupType,
} from '@features/feature-zone-management/components/enums/zone-list.enum';
import { listBackUpType } from '@features/feature-zone-management/components/constants/zone-list.constant';
import {
  ChangeZoneCodeFieldErrors,
  ChangeZoneFormFields,
} from '@features/feature-zone-management/components/constants/zone-list.metadata';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputTypeEnum } from '@core/enums';
import { StorageService } from '@core/services';
import { isFileSizeValid } from '@core/utils';

@Component({
  selector: 'novanet-zone-editable-field',
  templateUrl: './zone-editable-field.component.html',
  styleUrls: ['./zone-editable-field.component.scss'],
})
export class ZoneEditableFieldComponent implements OnInit {
  @Input() public zone: IZone;
  @Input() public fieldName: string;
  @Input() public typeEdit: TypeEdit;

  public isEditing = false;
  public fieldEditing: string;
  public isLoadingEditable: { [key: string]: boolean } = {};
  public TypeEdit = TypeEdit;
  public isShowModalZoneBackUp = false;
  public selectedBackUpType: ZoneBackupType = ZoneBackupType.BACKUP_CODE;
  public listBackUpType = listBackUpType;
  public changeZoneFormFields = ChangeZoneFormFields;
  public changeZoneCodeFieldErrors = ChangeZoneCodeFieldErrors;
  public changeZoneBackUpFormGroup: FormGroup;
  public readonly inputTypeEnum = InputTypeEnum;
  public backUpType = ZoneBackupType;
  public backUpFileToUpload = false;
  public backUpNameFile: string;
  public messageError = TypeErrorFile.NONE;
  public fileListBackUp: FileList;
  public isCallAPI = false;
  public changeImage = false;
  public messageSize = true;
  public fileNameUPLoad: string | undefined;
  public height = 0;
  public width = 0;
  public isFileNull = false;
  public TypeErrorFile = TypeErrorFile;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.height = this.zone.height;
    this.width = this.zone.width;
    this.buildChangeZoneBackUpForm();
  }

  public setEditing() {
    this.isEditing = true;
    this.fieldEditing = this.zone[this.fieldName];
  }

  public cancelEditing() {
    this.isEditing = false;
  }

  public saveEditing() {
    this.fieldEditing = this.fieldEditing.trim();
    if (this.fieldEditing.length < 5) {
      this.notificationError('Tên vùng QC phải nhập tối thiểu 5 kí tự.');
      return;
    }
    if (this.fieldEditing.length > 50) {
      this.notificationError('Tên vùng QC chỉ có thể nhập tối đa 50 kí tự.');
      return;
    }
    const payload: IZoneUpdateRequest = {
      id: this.zone.id,
      [this.fieldName]: this.fieldEditing,
    };
    this.store.dispatch(new UpdateZone(payload)).subscribe({
      next: () => {
        this.notificationSuccess('Cập nhật vùng QC thành công');
      },
      error: () => {
        this.notificationError(
          'Có lỗi xảy ra trong quá trình cập nhật vùng QC'
        );
      },
    });
  }

  public notificationError(message: string): void {
    this.store.dispatch(
      new ShowGlobalNotification(GlobalNotificationEnum.error, message)
    );
    this.isLoadingEditable[this.zone.id] = false;
    this.isCallAPI = false;
    this.changeDetectorRef.detectChanges();
  }

  public notificationSuccess(message: string): void {
    this.isEditing = false;
    this.store.dispatch(
      new ShowGlobalNotification(GlobalNotificationEnum.success, message)
    );
    this.isLoadingEditable[this.zone.id] = false;
    this.isCallAPI = false;
    this.changeDetectorRef.detectChanges();
  }

  public setEditingZone() {
    if (this.zone.backupType !== undefined) {
      if (this.zone.backupType === ZoneBackupType.BACKUP_FILE) {
        this.backUpNameFile = this.zone.backupCode;
        this.fileNameUPLoad = this.zone.backupCode;
        this.backUpFileToUpload = true;
      }
      this.changeZoneBackUpFormGroup.patchValue({
        categoryBackUp: this.zone.backupType,
        url: this.zone.redirectLink,
        codeBackUp:
          this.zone.backupType === ZoneBackupType.BACKUP_CODE
            ? this.zone.backupCode
            : undefined,
        backUpNameFile:
          this.zone.backupType === ZoneBackupType.BACKUP_FILE
            ? this.zone.backupCode
            : undefined,
      });
    }
    if (this.zone.backupType === undefined) {
      this.changeZoneBackUpFormGroup.patchValue({
        categoryBackUp: this.backUpType.BACKUP_CODE,
        codeBackUp: undefined,
        url: undefined,
      });
    }
    this.isShowModalZoneBackUp = true;
  }

  public cancelModalZoneBackUp() {
    this.changeZoneBackUpFormGroup.reset();
    if (this.isCallAPI) {
      return;
    }
    this.isShowModalZoneBackUp = false;
    this.messageError = TypeErrorFile.NONE;
    this.isFileNull = false;
    this.backUpNameFile = undefined;
    this.backUpFileToUpload = false;
  }

  public clickChangeZoneBackUp() {
    if (this.isCallAPI) {
      return;
    }
    this.isCallAPI = true;
    this.changeZoneBackUpFormGroup.markAllAsTouched();
    if (
      this.changeZoneBackUpFormGroup.get('categoryBackUp').value ===
      ZoneBackupType.BACKUP_CODE
    ) {
      if (!this.changeZoneBackUpFormGroup.get('codeBackUp').value) {
        this.isCallAPI = false;
        return;
      }
      const payload: IZoneUpdateRequest = {
        id: this.zone.id,
        usingBackup:
          this.zone.backupType === undefined ? true : this.zone.usingBackup,
        backupType: ZoneBackupType.BACKUP_CODE,
        backupCode: this.changeZoneBackUpFormGroup.get('codeBackUp').value,
      };
      this.store
        .dispatch(new UpdateZone(payload))
        .pipe()
        .subscribe({
          next: () => {
            this.notificationSuccess('Cập nhật vùng QC thành công');
          },
          error: () => {
            this.notificationError(
              'Có lỗi xảy ra trong quá trình cập nhật vùng QC'
            );
          },
        });
    } else {
      if (
        !/(https:\/\/)|(http:\/\/)|^$/.test(
          this.changeZoneBackUpFormGroup.get('url').value
        ) ||
        !this.messageSize ||
        this.messageError === TypeErrorFile.FORMAT_FILE ||
        !this.fileNameUPLoad
      ) {
        if (!this.fileNameUPLoad) {
          this.isFileNull = true;
        }
        this.isCallAPI = false;
        return;
      }
      if (this.changeImage) {
        this.storageService.upload(this.fileListBackUp[0]).subscribe(
          (fileName) => {
            this.fileNameUPLoad = fileName;
            const payloadRequest: IZoneUpdateRequest = {
              id: this.zone.id,
              usingBackup:
                this.zone.backupType === undefined
                  ? true
                  : this.zone.usingBackup,
              redirectLink: this.changeZoneBackUpFormGroup.get('url').value,
              backupType: ZoneBackupType.BACKUP_FILE,
              backupCode: this.fileNameUPLoad,
            };
            this.store
              .dispatch(new UpdateZone(payloadRequest))
              .pipe()
              .subscribe({
                next: () => {
                  this.notificationSuccess('Cập nhật vùng QC thành công');
                },
                error: () => {
                  this.notificationError(
                    'Có lỗi xảy ra trong quá trình cập nhật vùng QC'
                  );
                },
              });
          },
          (error) => {
            this.notificationError(
              'Có lỗi xảy ra trong quá trình cập nhật vùng QC'
            );
          }
        );
      } else {
        const payload: IZoneUpdateRequest = {
          id: this.zone.id,
          usingBackup:
            this.zone.backupType === undefined ? true : this.zone.usingBackup,
          redirectLink: this.changeZoneBackUpFormGroup.get('url').value,
          backupType: ZoneBackupType.BACKUP_FILE,
          backupCode: this.fileNameUPLoad,
        };
        this.store
          .dispatch(new UpdateZone(payload))
          .pipe()
          .subscribe({
            next: () => {
              this.notificationSuccess('Cập nhật vùng QC thành công');
            },
            error: () => {
              this.notificationError(
                'Có lỗi xảy ra trong quá trình cập nhật vùng QC'
              );
            },
          });
      }
    }
  }

  public handleZoneBackUpInput(files: FileList) {
    const fileName = files[0].name;
    if (isFileSizeValid([files.item(0)], 512000)) {
      if (/(?:.jpg|.jpeg|.png|.html5|.html(?!\\))+$/.test(fileName)) {
        const fr = new FileReader();
        fr.onload = () => {
          const img = new Image();
          img.onload = () => {
            const width = img.width;
            const height = img.height;
            const ratio = width / height;
            if (this.width / this.height !== ratio) {
              this.messageSize = false;
              this.messageError = TypeErrorFile.SIZE_FILE;
              this.isFileNull = false;
              this.changeDetectorRef.detectChanges();
              return;
            }
            this.messageError = TypeErrorFile.NONE;
          };
          img.src = fr.result as any;
        };
        fr.readAsDataURL(files[0]);

        this.messageError = TypeErrorFile.NONE;
        this.changeImage = true;
        this.messageSize = true;
        this.fileListBackUp = files;
        this.fileNameUPLoad = fileName;
      } else {
        this.messageError = TypeErrorFile.FORMAT_FILE;
      }
    } else {
      this.messageSize = false;
      this.messageError = TypeErrorFile.CAPACITY_FILE;
      return;
    }
    this.backUpNameFile = files[0].name;
    this.backUpFileToUpload = true;
  }

  public clearZoneBackUpFile(val: string) {
    if (this.isCallAPI) return;
    this.backUpFileToUpload = false;
    this.backUpNameFile = null;
    this.messageError = TypeErrorFile.NONE;
    this.fileListBackUp = undefined;
    this.fileNameUPLoad = undefined;
    this.isFileNull = false;
  }

  public emitChange(val: string) {}

  private buildChangeZoneBackUpForm() {
    const config = {
      [this.changeZoneFormFields['zoneCodeBackUp'].name]: [
        '',
        [Validators.required],
      ],
      [this.changeZoneFormFields['url'].name]: [
        '',
        [
          Validators.maxLength(
            this.changeZoneFormFields['url'].validationParams
              .maxLength as number
          ),
          Validators.pattern(
            this.changeZoneFormFields['url'].validationParams.pattern
          ),
        ],
      ],
      ['categoryBackUp']: [this.backUpType.BACKUP_CODE],
    };
    this.changeZoneBackUpFormGroup = this.formBuilder.group(config);
  }
}
