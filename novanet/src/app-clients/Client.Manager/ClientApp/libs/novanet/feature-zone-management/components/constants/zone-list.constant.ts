import {
  ZoneBackupType,
  ZoneListEnum,
} from '@features/feature-zone-management/components/enums/zone-list.enum';
import { IZonesResponse } from '@shared/settings/store';
import { ISettingColumnTable } from '@data-table/models';
import { IOption } from '@shared/custom-input/components/dropdown/models/dropdown.model';

export const ZoneListColumns = [
  {
    id: ZoneListEnum.INDEX,
    title: 'Stt',
    align: 'center',
    width: '65px',
    pinLeft: true,
  },
  {
    id: ZoneListEnum.NAME_ZONE,
    title: 'Tên vùng QC/ Kích thước (px)',
    sort: true,
    name: 'Name',
    width: '450px',
    pinLeft: true,
  },
  {
    id: ZoneListEnum.CAMPAIGN_TYPE,
    title: 'Loại QC',
    width: '150px',
  },
  {
    id: ZoneListEnum.ADVERTISING_NAME,
    title: 'Định dạng',
    width: '320px',
  },
  {
    id: ZoneListEnum.ZONE_BACKUP,
    title: 'Quảng cáo dự phòng',
    sort: true,
    name: 'UsingBackup,BackupType',
    width: '300px',
  },
  {
    id: ZoneListEnum.CODE_ZONE,
    title: 'Lấy mã',
    width: '80px',
  },
] as ISettingColumnTable<IZonesResponse, any>[];

export const listBackUpType = [
  {
    id: ZoneBackupType.BACKUP_CODE,
    label: 'Chạy mã quảng cáo dự phòng',
  },
  {
    id: ZoneBackupType.BACKUP_FILE,
    label: 'Upload file',
  },
] as IOption[];
