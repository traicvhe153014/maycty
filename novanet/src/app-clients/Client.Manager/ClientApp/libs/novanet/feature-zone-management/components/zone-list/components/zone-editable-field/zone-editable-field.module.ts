import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from '@core';
import { ZoneEditableFieldComponent } from '@features/feature-zone-management/components/zone-list/components/zone-editable-field/zone-editable-field.component';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NovanetInputModule } from '@shared/custom-input';

@NgModule({
  declarations: [ZoneEditableFieldComponent],
  exports: [ZoneEditableFieldComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SvgIconModule,
    NzSpaceModule,
    NzInputModule,
    FormsModule,
    NzButtonModule,
    NzModalModule,
    NovanetInputModule,
    ReactiveFormsModule,
  ],
})
export class ZoneEditableFieldModule {}
