import { DataTableSettingModel } from '@data-table/models';
import { IFormFieldErrors, IFormFields } from '@models';
import { urlBackUpPattern } from '@shared/sharing/constants';

export const ZoneListSettingTable = {
  id: 'zone-list',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '1220px',
  sortType: 'single',
} as DataTableSettingModel;

export const ChangeZoneFormFields: IFormFields = {
  zoneCodeBackUp: {
    name: 'codeBackUp',
    validationParams: {
      required: true,
    },
  },
  url: {
    name: 'url',
    validationParams: {
      maxLength: 2000,
      pattern: urlBackUpPattern,
    },
  },
};

export const ChangeZoneCodeFieldErrors: IFormFieldErrors = {
  zoneCodeBackUp: [
    {
      type: 'required',
      message: 'Không để trống mã quảng cáo dự phòng',
    },
  ],
  url: [
    {
      type: 'maxlength',
      message: 'Url có độ dài tối đa 2000 ký tự',
    },
    {
      type: 'pattern',
      message: 'Sai định dạng',
    },
  ],
};
