import { DataTableSettingModel } from '@data-table/models';

export const ProductGroupManagementSetting = {
  id: 'product-group',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;
