import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { IProductGroupManagementData } from '@features/product-group-management/models';
import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { Select, Store } from '@ngxs/store';
import {
  CreateProductGroupConstant,
  defaultProductAttributeName,
  errorCreateFormMessage,
  ProductGroupFormFields,
  ProductsSettingTable,
} from './constants/_index';
import { CreateProductGroupEnum } from './enums/_index';
import { IFormFields } from '@models';
import { CreateProductGroup } from '@features/product-group-management/store';
import {
  ClearAllProductsData,
  GetProductAttributes,
  GetProductAttributeValues,
  ProductManagementState,
} from '@features/product-management/store';
import { Observable, Subject } from 'rxjs';
import {
  IListProductAttributeValueResponse,
  IProductAttributeResponse,
  IProductAttributeValueResponse,
} from '@features/product-management/models/_index';
import { take, takeUntil } from 'rxjs/operators';
import { cloneDeep } from 'lodash';
import { IHttpGetRequest } from '@core/models';
import {
  ClearAllProductFeedsData,
  GetMoreProductFeedList,
  GetProductFeedList,
  IListProductFeedRequest,
  IProductFeedResponse,
  ProductFeedPermission,
  ProductFeedState,
} from '@features/product-feed/store';
import { SumArray } from '@core/utils';

@Component({
  selector: 'novanet-product-group-management',
  templateUrl: './create-product-group.component.html',
  styleUrls: ['./create-product-group.component.scss'],
})
export class CreateProductGroupComponent implements OnInit, OnDestroy {
  @Select(ProductManagementState.getProductAttributeValueList)
  public $productAttributeValues: Observable<IListProductAttributeValueResponse>;

  @Select(ProductManagementState.getLoadingStatusProductAttributeValueList)
  public $loadingStatus: Observable<boolean>;

  @Select(ProductManagementState.getProductAttributeList)
  public $productAttributes: Observable<IProductAttributeResponse[]>;

  @Select(ProductManagementState.getLoadingStatusProductAttributeList)
  public $loadingStatusProductAttributes: Observable<boolean>;

  @Select(ProductFeedState.getList)
  public $productFeeds: Observable<IProductFeedResponse[]>;

  @Select(ProductFeedState.getLoading)
  public $loadingProductFeedsStatus: Observable<boolean>;

  public listOfData: IProductGroupManagementData[] = [];
  public settings: DataTableSettingModel;
  public searchForm: FormGroup;
  public productGroupForm: FormGroup;
  public scrollX: string | null = null;
  public scrollY: string | null = null;
  public editEnabled = false;
  public selectedProductAttributes = [] as IProductAttributeValueResponse[];
  public productsResponse: IProductAttributeValueResponse[] = [];
  public productFeedsResponse: IProductFeedResponse[] = [];
  public productAttributesResponse: IProductAttributeResponse[] = [];
  public totalProductAttributes = 0;
  public totalAmount = 0;
  public pageSize = 20;
  public withSummary = true;

  public readonly productGroupFormFields: IFormFields = ProductGroupFormFields;
  public columns: ISettingColumnTable<any, any>[] = ProductsSettingTable;
  public readonly ProductManagementColumnEnum = CreateProductGroupEnum;
  public readonly globalNotificationEnum = GlobalNotificationEnum;
  public readonly createProductGroupFormFieldErrors = errorCreateFormMessage;

  private skip = 0;
  private page = 1;
  private productsLoaded = false;
  private productAttributesLoaded = false;

  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get products(): FormArray {
    return this.productGroupForm.get(
      this.productGroupFormFields['productAttributeValues'].name
    ) as FormArray;
  }

  get productFeedId(): FormControl {
    return this.productGroupForm.get('productFeedId') as FormControl;
  }

  get productAttributeId(): FormControl {
    return this.productGroupForm.get('productAttributeId') as FormControl;
  }

  get remainProducts(): string {
    return this.productsResponse.length + '/' + this.totalProductAttributes;
  }

  get selectedProducts(): string {
    return (
      this.selectedProductAttributes.length + '/' + this.totalProductAttributes
    );
  }

  ngOnInit(): void {
    this.buildSearchForm();
    this.buildFormProductGroup();
    this.settings = CreateProductGroupConstant;
    this.getProducts();
    this.getProductFeeds();
    this.getProductAttributes();
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearAllProductsData());
    this.store.dispatch(new ClearAllProductFeedsData());
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onSearch() {}

  public onChangeProductGroup() {}

  public nextBatch($event?: boolean) {
    if ($event && this.productsLoaded) {
      return;
    }
    if ($event) {
      this.page += 1;
      this.skip = this.skip + this.pageSize;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        withSummary: this.withSummary,
      } as IHttpGetRequest;
      this.store.dispatch(
        new GetProductAttributeValues(
          params,
          this.productFeedId.value,
          this.productAttributeId.value
        )
      );
    } else {
      this.page = 1;
      this.skip = 0;
      this.productsResponse = [];
      this.totalProductAttributes = 0;
      this.selectedProductAttributes = [];
      this.productsLoaded = false;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        withSummary: this.withSummary,
      } as IHttpGetRequest;
      this.store.dispatch(
        new GetProductAttributeValues(
          params,
          this.productFeedId.value,
          this.productAttributeId.value
        )
      );
      this.changeDetectorRef.detectChanges();
    }
  }

  public getNextAttributeValuesBatch($event: IHttpGetRequest) {
    if ($event && !this.productAttributesLoaded) {
      this.store.dispatch(new GetProductAttributes($event));
    }
  }

  public getNextProductFeedsBatch($event: IHttpGetRequest) {
    this.$loadingProductFeedsStatus.pipe(take(1)).subscribe({
      next: (loading) => {
        if ($event && !loading) {
          this.store.dispatch(
            new GetMoreProductFeedList({
              page: $event.page,
              pageSize: $event.pageSize,
              keyword: '',
              sorts: ['name'],
              productValidGreaterThan: 0,
              permission: ProductFeedPermission.Granted,
            })
          );
        }
      },
    });
  }

  public onAddProductType(value: IProductAttributeValueResponse) {
    this.productsResponse = this.productsResponse.filter(
      (item) =>
        item.productAttributeHashcodeValue !==
        value.productAttributeHashcodeValue
    );
    this.selectedProductAttributes = [...this.selectedProductAttributes, value];
    this.products.push(
      this.formBuilder.control(value.productAttributeHashcodeValue)
    );
    this.changeDetectorRef.detectChanges();
  }

  public onAddAllCurrentProductType() {
    this.selectedProductAttributes = this.selectedProductAttributes.concat(
      cloneDeep(this.productsResponse)
    );
    this.productsResponse.forEach((item) => {
      this.products.push(
        this.formBuilder.control(item.productAttributeHashcodeValue)
      );
    });
    this.productsResponse = [];
    this.nextBatch(true);
  }

  public onRemoveProductType(value: IProductAttributeValueResponse) {
    const index = this.selectedProductAttributes.findIndex(
      (item) =>
        item.productAttributeHashcodeValue ===
        value.productAttributeHashcodeValue
    );
    this.products.removeAt(index);
    this.selectedProductAttributes = this.selectedProductAttributes.filter(
      (item) =>
        item.productAttributeHashcodeValue !==
        value.productAttributeHashcodeValue
    );
    this.productsResponse = [...this.productsResponse, value];
  }

  public onRemoveAllProductType() {
    this.productsResponse = this.productsResponse.concat(
      cloneDeep(this.selectedProductAttributes)
    );
    this.selectedProductAttributes = [];
    this.products.clear();
  }

  public onSubmitCreateProductGroup(): void {
    if (this.productGroupForm.invalid) {
      this.productGroupForm.markAllAsTouched();
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Vui lòng điền các trường dữ liệu bắt buộc'
        )
      );
      return;
    }
    if (!this.selectedProductAttributes.length) {
      this.productGroupForm.markAllAsTouched();
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Vui lòng chọn ít nhất một loại sản phẩm'
        )
      );
      return;
    }
    const sumArray = new SumArray(...this.selectedProductAttributes);
    this.productGroupForm.value.amount = sumArray.sum('amount');
    this.store.dispatch(new CreateProductGroup(this.productGroupForm.value));
  }

  private getProducts() {
    this.$productAttributeValues
      .pipe(takeUntil(this.destroy$))
      .subscribe((products) => {
        if (products && products.data) {
          if (products.data.length < this.pageSize) {
            this.productsLoaded = true;
          }
          this.productsResponse = this.productsResponse.concat(
            cloneDeep(products.data)
          );
          this.totalProductAttributes = products.summary['total'];
          this.totalAmount = products.summary['amount'];
          this.changeDetectorRef.detectChanges();
        }
      });
  }

  private getProductFeeds() {
    this.$productFeeds
      .pipe(takeUntil(this.destroy$))
      .subscribe((productFeeds) => {
        if (productFeeds) {
          this.productFeedsResponse = cloneDeep(productFeeds);
        }
      });
    const params = {
      page: this.page,
      skip: this.skip,
      pageSize: this.pageSize,
      sorts: ['-modifiedAt'],
      keyWord: '',
      productValidGreaterThan: 0,
    } as IListProductFeedRequest;
    this.store.dispatch(new GetProductFeedList(params));
  }

  private getProductAttributes() {
    this.$productAttributes
      .pipe(takeUntil(this.destroy$))
      .subscribe((productAttributes) => {
        if (productAttributes && productAttributes.length < this.pageSize) {
          this.productAttributesLoaded = true;
        }
        if (productAttributes) {
          this.productAttributesResponse =
            this.productAttributesResponse.concat(cloneDeep(productAttributes));
        }
      });
    const params = {
      page: this.page,
      skip: this.skip,
      pageSize: this.pageSize,
    } as IHttpGetRequest;
    this.store.dispatch(new GetProductAttributes(params));
  }

  private buildSearchForm() {
    const config = {
      ['search']: ['', [Validators.required]],
    };
    this.searchForm = this.formBuilder.group(config);
  }

  private buildFormProductGroup() {
    const config = {
      [this.productGroupFormFields['productGroupName'].name]: [
        '',
        [
          this.productGroupFormFields['productGroupName'].validationParams
            .required
            ? Validators.required
            : null,
        ],
      ],
      [this.productGroupFormFields['productFeed'].name]: [
        '',
        [
          this.productGroupFormFields['productFeed'].validationParams.required
            ? Validators.required
            : null,
        ],
      ],
      [this.productGroupFormFields['productAttribute'].name]: [
        '',
        [
          this.productGroupFormFields['productAttribute'].validationParams
            .required
            ? Validators.required
            : null,
        ],
      ],
      [this.productGroupFormFields['productAttributeValues'].name]:
        this.formBuilder.array([]),
    };
    this.productGroupForm = this.formBuilder.group(config);
    this.productFeedId.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        if (this.productAttributeId.value && value) {
          this.nextBatch();
        }
      });
    this.productAttributeId.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.columns = ProductsSettingTable.map((item) => {
          if (item.id !== CreateProductGroupEnum.PRODUCT_TYPE) {
            return item;
          }
          if (!value) {
            return {
              ...item,
              title: defaultProductAttributeName,
            };
          }
          const productAttribute = this.productAttributesResponse.find(
            (attribute) => attribute.id === value
          );
          return {
            ...item,
            title: productAttribute?.name ?? defaultProductAttributeName,
          };
        });
        if (this.productFeedId.value && value) {
          this.nextBatch();
        }
      });
  }
}
