import { DataTableSettingModel } from '@data-table/models';

export const CreateProductGroupConstant = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;

export const errorCreateFormMessage = {
  productGroupName: [
    {
      type: 'required',
      message: 'Không để trống tên nhóm sản phẩm',
    },
  ],
  productFeed: [
    {
      type: 'required',
      message: 'Không để trống nguồn dữ liệu',
    },
  ],
  productAttribute: [
    {
      type: 'required',
      message: 'Không để trống phân loại theo',
    },
  ],
};

export const defaultProductAttributeName = 'Loại sản phẩm';
