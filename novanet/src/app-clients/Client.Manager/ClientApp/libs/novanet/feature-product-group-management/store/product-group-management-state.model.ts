import { IProductGroupManagementData } from '../models';

export interface IFeatureProductManagementState {
  productGroups: IProductGroupManagementData[];
  updatedId: string;
}
