import { CreateProductGroupEnum } from '../enums/_index';
import { ISettingColumnTable } from '@data-table/models';

export const ProductsSettingTable = [
  {
    id: CreateProductGroupEnum.INDEX,
    title: 'STT',
    width: '10%',
  },
  {
    id: CreateProductGroupEnum.PRODUCT_TYPE,
    title: 'Loại sản phẩm',
    width: '60%',
  },
  {
    id: CreateProductGroupEnum.AMOUNT,
    title: 'Số lượng',
    width: '20%',
  },
  {
    id: CreateProductGroupEnum.CHOOSE,
    title: 'Chọn',
    width: '10%',
    align: 'center',
  },
] as ISettingColumnTable<any, any>[];
