import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductGroupManagementComponent } from './product-group-management.component';

const routes: Routes = [
  {
    path: '',
    component: ProductGroupManagementComponent,
  },
  {
    path: 'create-product-group',
    loadChildren: () =>
      import('@features/product-group-management/components/_index').then(
        (m) => m.ComponentsProductManagementModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductGroupManagementRouting {}
