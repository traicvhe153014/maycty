import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from '@core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { CreateProductGroupComponent } from './create-product-group/create-product-group.component';
import { NovanetInputModule } from '@shared/custom-input';
import { ComponentsProductManagementRouting } from './components-product-management.routing';
import { ProductManagementStoreModule } from '@features/product-management/store';
import { ProductGroupManagementStateModule } from '@features/product-group-management/store';
import { ProductFeedStateModule } from '@features/product-feed/index';
import { SumColumnPipe } from './create-product-group/pipes';

const COMPONENTS: any[] = [CreateProductGroupComponent];

const PIPES: any[] = [SumColumnPipe];

const MODULES: any[] = [
  ProductManagementStoreModule,
  ProductFeedStateModule,
  ProductGroupManagementStateModule,
  CommonModule,
  ComponentsProductManagementRouting,
  FormsModule,
  DataTableModule,
  NzTableModule,
  ReactiveFormsModule,
  NovanetInputModule,
  NzAffixModule,
];

@NgModule({
  declarations: [...COMPONENTS, ...PIPES],
  imports: [...MODULES],
})
export class ComponentsProductManagementModule {}
