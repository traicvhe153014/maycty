import { ISettingColumnTable } from '@data-table/models';
import { ProductGroupManagementEnum } from '../enums/_index';

export const SettingColumnProductGroupTable = [
  {
    id: ProductGroupManagementEnum.INDEX,
    title: 'STT',
    name: 'index',
    width: '70px',
  },
  {
    id: ProductGroupManagementEnum.PRODUCT_GROUP,
    width: '400px',
    name: 'name',
    title: 'Nhóm sản phẩm',
    sort: true,
  },
  {
    id: ProductGroupManagementEnum.AMOUNT,
    width: '100px',
    name: 'amount',
    title: 'Số lượng',
    sort: true,
  },
  {
    id: ProductGroupManagementEnum.MODIFIED_DATE,
    title: 'Cập nhật',
    name: 'modifiedAt',
    width: '200px',
    sort: true,
    descending: true,
    ascending: false
  },
  {
    id: ProductGroupManagementEnum.SOURCE_DATA,
    title: 'Nguồn dữ liệu',
    name: 'productFeed.name',
    width: '250px',
    sort: true,
  },
  {
    id: ProductGroupManagementEnum.RUNNING_AD_GROUP,
    title: 'Nhóm QC đang chạy',
  },
] as ISettingColumnTable<any, any>[];
