export interface ICreateProductGroup {
  productGroupName: string;
  productFeedId: string;
  productAttributeId: string;
  products: string[];
}
