import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';

import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  ClearProductGroups,
  ClearStatus,
  CreateProductGroup,
  CreateProductGroupFail,
  CreateProductGroupSuccess,
  GetProductGroups,
  IFeatureProductManagementState,
  IProductGroupManagementData,
  ProductGroupManagementService,
  UpdateProductGroup,
  UpdateProductGroupFail,
  UpdateProductGroupSuccess,
} from '@features/product-group-management/index';

@Injectable()
@State<IFeatureProductManagementState>({
  name: 'FeatureProductManagement',
})
export class FeatureProductManagementState implements NgxsOnInit {
  public readonly globalNotificationEnum = GlobalNotificationEnum;

  constructor(
    private featureProductManagementService: ProductGroupManagementService
  ) {}

  @Selector()
  public static getProductGroups(
    state: IFeatureProductManagementState
  ): IProductGroupManagementData[] {
    return state.productGroups;
  }

  @Selector()
  public static getProductGroupUpdateStatus(
    state: IFeatureProductManagementState
  ): string {
    return state.updatedId;
  }

  public ngxsOnInit(ctx?: StateContext<FeatureProductManagementState>): void {}

  @Action(GetProductGroups)
  public getProductGroups(
    ctx: StateContext<IFeatureProductManagementState>,
    { payload }: GetProductGroups
  ) {
    return this.featureProductManagementService
      .getProductGroups(payload)
      .subscribe((response) => {
        ctx.patchState({
          productGroups: response,
        });
      });
  }

  @Action(CreateProductGroup)
  public createProductGroup(
    ctx: StateContext<IFeatureProductManagementState>,
    { payload }: CreateProductGroup
  ) {
    return this.featureProductManagementService
      .createProductGroup(payload)
      .subscribe(
        (response) => {
          ctx.dispatch(new CreateProductGroupSuccess());
        },
        (error) => {
          ctx.dispatch(
            new CreateProductGroupFail(error?.error?.errors?.[0] ?? '')
          );
        }
      );
  }

  @Action(CreateProductGroupSuccess)
  public createProductGroupSuccess(
    ctx: StateContext<IFeatureProductManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        this.globalNotificationEnum.success,
        'Tạo thành công nhóm quảng cáo'
      )
    );
    this.featureProductManagementService.redirectToUrl(
      `/product-group-management`
    );
  }

  @Action(CreateProductGroupFail)
  public createProductGroupFail(
    ctx: StateContext<IFeatureProductManagementState>,
    { payload }: CreateProductGroupFail
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        this.globalNotificationEnum.error,
        'Lỗi',
        payload
      )
    );
  }

  @Action(UpdateProductGroup)
  public updateProductGroup(
    ctx: StateContext<IFeatureProductManagementState>,
    { payload }: UpdateProductGroup
  ) {
    return this.featureProductManagementService
      .updateProductGroup(payload)
      .subscribe(
        (response) => {
          ctx.dispatch(new UpdateProductGroupSuccess(payload));
        },
        (error) => {
          ctx.dispatch(new UpdateProductGroupFail());
        }
      );
  }

  @Action(UpdateProductGroupSuccess)
  public updateProductGroupSuccess(
    ctx: StateContext<IFeatureProductManagementState>,
    { payload }: UpdateProductGroupSuccess
  ): void {
    ctx.patchState({
      updatedId: payload.id,
    });
    ctx.dispatch(
      new ShowGlobalNotification(
        this.globalNotificationEnum.success,
        'Sửa thành công nhóm quảng cáo'
      )
    );
  }

  @Action(UpdateProductGroupFail)
  public updateProductGroupFail(
    ctx: StateContext<IFeatureProductManagementState>
  ): void {
    ctx.patchState({
      updatedId: '',
    });
    ctx.dispatch(
      new ShowGlobalNotification(
        this.globalNotificationEnum.error,
        'Lỗi',
        'Sửa nhóm quảng cáo thất bại'
      )
    );
  }

  @Action(ClearStatus)
  public clearStatus(ctx: StateContext<IFeatureProductManagementState>): void {
    ctx.patchState({
      updatedId: '',
    });
  }

  @Action(ClearProductGroups)
  public clearProductGroups(
    ctx: StateContext<IFeatureProductManagementState>
  ): void {
    ctx.patchState({
      productGroups: [],
    });
  }
}
