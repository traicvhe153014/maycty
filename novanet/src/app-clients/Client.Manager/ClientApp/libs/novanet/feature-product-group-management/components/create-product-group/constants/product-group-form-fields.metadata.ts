import { IFormFields } from '@models';

export const ProductGroupFormFields = {
  productGroupName: {
    name: 'name',
    validationParams: {
      maxLength: 50,
      required: true,
    },
  },
  productFeed: {
    name: 'productFeedId',
    validationParams: {
      required: true,
    },
  },
  productAttribute: {
    name: 'productAttributeId',
    validationParams: {
      required: true,
    },
  },
  productAttributeValues: {
    name: 'ProductAttributeHashKeyValues',
    validationParams: {
      required: true,
    },
  },
} as IFormFields;
