import { IHttpGetRequest } from '@core/models';
import { IProductFeedResponse } from '@features/product-feed/store';

export interface IProductGroupManagementData {
  id: string;
  name: string;
  amount: number;
  modified: string;
  productFeed: IProductFeedResponse;
  advertisingSetRunning: IProductAdsetResponse[];
  productGroupEntities: IProductGroupEntityResponse[];
  checked?: boolean;
  enableChangeValue?: boolean;
  loadMoreProductAttributeValue?: boolean;
  runningProducts: number;
  stoppedProducts: number;
}

export interface IProductAdsetResponse {
  id: string;
  name: string;
  isActive: string;
  status: string;
}

export interface IProductGroupEntityResponse {
  productGroupId: string;
  productAttributeValueId: string;
  productId: string;
  id: string;
}

export interface IProductGroupHttpGetRequest extends IHttpGetRequest {
  productFeedId?: string;
}
