import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { FeatureProductManagementState } from '@features/product-group-management/store/product-group-management.state';
import { ProductGroupManagementService } from '@features/product-group-management/store/product-group-management.service';

@NgModule({
  imports: [NgxsModule.forFeature([FeatureProductManagementState])],
  providers: [ProductGroupManagementService],
})
export class ProductGroupManagementStateModule {}
