import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient } from '@angular/common/http';
import { ISuccessHttpResponse } from '@models';
import {
  ICreateProductGroup,
  IUpdateProductGroup,
} from '../components/create-product-group/models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IHttpGetRequest } from '@core/models';
import { IProductGroupManagementData } from '@features/product-group-management/models';

const ProductManagementUrls = {
  list: `list`,
  get: `get`,
  create: `create`,
  update: `update`,
};

@Injectable()
export class ProductGroupManagementService extends BaseService {
  constructor(
    httpClient: HttpClient,
    private readonly router: Router,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1/ProductGroup');
  }

  public createProductGroup(data: ICreateProductGroup): Observable<any> {
    return this.httpClient
      .post<ISuccessHttpResponse>(
        this.createUrl(ProductManagementUrls.create),
        data
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public updateProductGroup(data: IUpdateProductGroup): Observable<any> {
    return this.httpClient
      .put<ISuccessHttpResponse>(
        this.createUrl(ProductManagementUrls.update),
        data
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public getProductGroupById(Ids: string[]) {
    const params = this.createParams({ Ids });

    return this.httpClient
      .get<ISuccessHttpResponse<IProductGroupManagementData[]>>(
        this.createUrl(ProductManagementUrls.get),
        {
          params,
        }
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public getProductGroups(data: IHttpGetRequest) {
    const params = this.createParams(data);
    return this.httpClient
      .get<ISuccessHttpResponse>(this.createUrl(ProductManagementUrls.list), {
        params,
      })
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public redirectToUrl(url: string): void {
    this.router.navigate([url]).then();
  }
}
