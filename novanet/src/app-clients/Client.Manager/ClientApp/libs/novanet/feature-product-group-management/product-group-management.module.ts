import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ProductGroupManagementComponent } from './product-group-management.component';
import { ProductGroupManagementRouting } from './product-group-management.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from '@core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NovanetInputModule } from '@shared/custom-input';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { ProductGroupManagementStateModule } from './store';

@NgModule({
  declarations: [ProductGroupManagementComponent],
  imports: [
    CommonModule,
    ProductGroupManagementRouting,
    ProductGroupManagementStateModule,
    FormsModule,
    DataTableModule,
    NzTableModule,
    ReactiveFormsModule,
    NovanetInputModule,
    NzAffixModule,
  ],
})
export class ProductGroupManagementModule {}
