import { Pipe, PipeTransform } from '@angular/core';
import { SumArray } from '@core/utils';

@Pipe({
  name: 'SumColumn',
})
export class SumColumnPipe implements PipeTransform {
  transform(values: any[], key: string): number {
    const sumArray = new SumArray(...values);
    return sumArray.sum(key);
  }
}
