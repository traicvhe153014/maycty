export interface IUpdateProductGroup {
  id: string;
  name: string;
}
