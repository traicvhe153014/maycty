import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateProductGroupComponent } from './create-product-group/create-product-group.component';

const routes: Routes = [
  {
    path: '',
    component: CreateProductGroupComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentsProductManagementRouting {}
