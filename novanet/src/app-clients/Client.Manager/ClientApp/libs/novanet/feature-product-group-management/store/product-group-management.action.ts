import {
  ICreateProductGroup,
  IUpdateProductGroup,
} from '../components/create-product-group/models';
import { IHttpGetRequest } from '@core/models';
import {IProductGroupHttpGetRequest} from "@features/product-group-management/models";

const enum ProductGroupManagementAction {
  CreateProductGroup = '[Product Group] CreateProductGroup',
  GetProductGroupList = '[Product Group] GetProductGroupList',
  CreateSuccess = '[Product Group] CreateProductGroupSuccess',
  CreateFail = '[Product Group] CreateProductGroupFail',

  UpdateProductGroup = '[Product Group] UpdateProductGroup',
  UpdateProductGroupSuccess = '[Product Group] UpdateProductGroupSuccess',
  UpdateProductGroupFail = '[Product Group] UpdateProductGroupFail',

  ClearStatus = '[Product Group] ClearStatus',
  ClearProductGroups = '[Product Group] ClearProductGroups',
}

export class CreateProductGroup {
  static type = ProductGroupManagementAction.CreateProductGroup;

  constructor(public payload: ICreateProductGroup) {}
}

export class GetProductGroups {
  static type = ProductGroupManagementAction.GetProductGroupList;

  constructor(public payload: IProductGroupHttpGetRequest) {}
}

export class CreateProductGroupSuccess {
  static type = ProductGroupManagementAction.CreateSuccess;
}

export class CreateProductGroupFail {
  static type = ProductGroupManagementAction.CreateFail;

  constructor(public payload: string) {}
}

export class UpdateProductGroup {
  static type = ProductGroupManagementAction.UpdateProductGroup;

  constructor(public payload: IUpdateProductGroup) {}
}

export class UpdateProductGroupSuccess {
  static type = ProductGroupManagementAction.UpdateProductGroupSuccess;

  constructor(public payload: IUpdateProductGroup) {}
}

export class UpdateProductGroupFail {
  static type = ProductGroupManagementAction.UpdateProductGroupFail;
}

export class ClearStatus {
  static type = ProductGroupManagementAction.ClearStatus;
}

export class ClearProductGroups {
  static type = ProductGroupManagementAction.ClearProductGroups;
}
