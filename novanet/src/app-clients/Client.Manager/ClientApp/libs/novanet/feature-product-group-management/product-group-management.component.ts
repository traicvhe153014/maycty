import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import {
  DataTableSettingModel,
  IFieldDetail,
  ISettingColumnTable,
} from '@data-table/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { IProductGroupManagementData } from './models';
import { ProductGroupManagementSetting } from './constants/_index';
import { ProductGroupManagementEnum } from './enums/_index';
import { SettingColumnProductGroupTable } from '@features/product-group-management/constants/_index';
import {
  ClearProductGroups,
  ClearStatus,
  FeatureProductManagementState,
  GetProductGroups,
  UpdateProductGroup,
} from '@features/product-group-management/store';
import { Observable, Subject } from 'rxjs';
import { IHttpGetRequest } from '@core/models';
import { takeUntil } from 'rxjs/operators';
import { cloneDeep } from 'lodash';
import { ESortType } from '@core/enums';

@Component({
  selector: 'novanet-product-group-management',
  templateUrl: './product-group-management.component.html',
})
export class ProductGroupManagementComponent implements OnInit, OnDestroy {
  @Select(FeatureProductManagementState.getProductGroups)
  public $productGroups: Observable<IProductGroupManagementData[]>;

  @Select(FeatureProductManagementState.getProductGroupUpdateStatus)
  public $updateStatus: Observable<string>;

  public listOfProductGroup: IProductGroupManagementData[] = [];
  public settings: DataTableSettingModel;
  public searchForm: FormGroup;
  public changeProductGroupForm: FormGroup;
  public scrollX: string | null = null;
  public scrollY: string | null = null;
  public editEnabled = false;
  public pageSize = 50;

  public columns: ISettingColumnTable<any, any>[] =
    SettingColumnProductGroupTable;

  public readonly productGroupManagementEnum = ProductGroupManagementEnum;
  public readonly globalNotificationEnum = GlobalNotificationEnum;

  private skip = 0;
  private page = 1;
  private sorts = ['-modifiedAt'];
  private baseSorts = ['-modifiedAt'];
  private destroy$: Subject<void> = new Subject<void>();
  private dataLoaded = false;
  private loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.buildSearchForm();
    this.getProductGroups();
    this.$updateStatus.pipe(takeUntil(this.destroy$)).subscribe((updatedId) => {
      if (updatedId) {
        const exist = this.listOfProductGroup.find(
          (item) => item.id === updatedId
        );
        if (exist) {
          exist.enableChangeValue = !exist.enableChangeValue;
          exist.name = cloneDeep(this.changeProductGroupForm.value.name);
          this.editEnabled = !this.editEnabled;
          this.changeProductGroupForm.reset();
          this.store.dispatch(new ClearStatus());
          this.changeDetectorRef.detectChanges();
        }
      }
    });
    const params = {
      page: this.page,
      skip: 0,
      pageSize: this.pageSize,
      sorts: this.sorts,
    } as IHttpGetRequest;
    this.store.dispatch(new GetProductGroups(params));
    this.settings = ProductGroupManagementSetting;
  }

  public ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearProductGroups());
  }

  public onSearch(values?: IFieldDetail[]) {
    this.changeDetectorRef.detach();
    if (values?.length) {
      this.dataLoaded = false;
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.listOfProductGroup = [];
    this.dataLoaded = false;
    this.page = 1;
    const params = {
      page: 1,
      skip: 0,
      pageSize: this.pageSize,
      keyWord: this.searchForm.value.search,
      sorts: this.sorts,
    } as IHttpGetRequest;
    this.store.dispatch(new GetProductGroups(params));
  }

  public checkAll(value: boolean) {
    this.changeDetectorRef.detectChanges();
  }

  public refreshStatus(_event: { id: string; checked: boolean }) {
    this.changeDetectorRef.detectChanges();
  }

  public onChangeProductGroup() {}

  public nextBatch($event?: boolean) {
    if ($event && !this.dataLoaded && !this.loading) {
      this.loading = true;
      this.page += 1;
      this.skip = this.skip + this.pageSize;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        keyWord: this.searchForm.value.search,
        sorts: this.sorts,
      } as IHttpGetRequest;
      this.store.dispatch(new GetProductGroups(params));
    }
  }

  public onToggleEdit(data: IProductGroupManagementData): void {
    if (this.editEnabled && !data.enableChangeValue) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Hãy lưu dữ liệu trước khi sửa nhóm sản phẩm khác'
        )
      );
      return;
    }
    data.enableChangeValue = !data.enableChangeValue;
    this.editEnabled = !this.editEnabled;
    if (data.enableChangeValue) {
      this.buildProductGroupForm(data);
    } else {
      this.changeProductGroupForm.reset();
    }
    this.changeDetectorRef.detectChanges();
  }

  public onSubmitChangeProductGroup() {
    this.store.dispatch(
      new UpdateProductGroup(cloneDeep(this.changeProductGroupForm.value))
    );
  }

  public onLoadMoreProductAttributeValue(data: IProductGroupManagementData) {
    const exist = this.listOfProductGroup.find((item) => item.id === data.id);
    if (exist) {
      exist.loadMoreProductAttributeValue =
        !exist.loadMoreProductAttributeValue;
      this.changeDetectorRef.detectChanges();
    }
  }

  private getProductGroups() {
    this.$productGroups.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      this.loading = false;
      if (!response || !response.length) {
        this.dataLoaded = true;
        return;
      }
      let data = cloneDeep(response);
      data = data.map((item) => ({
        ...item,
        checked: false,
        enableChangeValue: false,
        loadMoreProductAttributeValue: false,
      }));
      this.listOfProductGroup = this.listOfProductGroup.concat(data);
      this.dataLoaded = response.length < this.pageSize;
      this.changeDetectorRef.detectChanges();
      this.changeDetectorRef.reattach();
    });
  }

  private buildSearchForm() {
    const config = {
      ['search']: [''],
    };
    this.searchForm = this.formBuilder.group(config);
  }

  private buildProductGroupForm(data: IProductGroupManagementData) {
    const config = {
      ['id']: [data.id, [Validators.required]],
      ['name']: [data.name, [Validators.required]],
    };
    this.changeProductGroupForm = this.formBuilder.group(config);
  }
}
