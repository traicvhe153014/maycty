export enum ProductGroupManagementEnum {
  INDEX,
  PRODUCT_GROUP,
  AMOUNT,
  MODIFIED_DATE,
  SOURCE_DATA,
  RUNNING_AD_GROUP,
}
