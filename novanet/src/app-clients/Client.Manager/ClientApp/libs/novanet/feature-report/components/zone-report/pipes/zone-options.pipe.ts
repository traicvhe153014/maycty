import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { map } from 'rxjs/operators';
import { IZonesResponse } from '@shared/settings/store';
import { getTemplateName } from '@core/utils';

@Pipe({
  name: 'zoneOptions',
})
export class ZoneOptionsPipe implements PipeTransform {
  transform(
    value: Observable<IZonesResponse>,
    roles: string | undefined
  ): Observable<NzSelectOptionInterface[]> {
    const isAdmin = roles === 'Admin' || roles === 'PublisherManager';
    return value.pipe(
      map((data) =>
        data.data.map((item) => {
          const templateNames =
            item.zoneTemplates
              ?.map((template) => {
                return getTemplateName(template.templateType);
              })
              .join(', ') ?? '';
          return {
            label:
              isAdmin && !!item.publisherEmail
                ? `${templateNames} - ${item.domain} - ${item.publisherEmail}`
                : `${templateNames} - ${item.domain}`,
            value: item.id,
          };
        })
      )
    );
  }
}
