import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  publisherReportSummaryColumns,
  publisherReportTableColumns,
  publisherReportTableSetting,
} from '@features/feature-report/constants';
import { PublisherReportTableEnum } from '@features/feature-report/enums';
import {
  ClearPublisherReport,
  GetPublisherReport,
  IPublisherReport,
  ReportState,
} from '@features/feature-report/store';
import { Observable, Subject } from 'rxjs';
import {
  GetMorePublisherList,
  GetPublisherList,
  IPublisherResponse,
  PublisherState,
} from '@features/feature-publisher-management/store';
import { take } from 'rxjs/operators';

@Component({
  selector: 'novanet-publisher-report',
  templateUrl: './publisher-report.component.html',
  styleUrls: ['./publisher-report.component.scss'],
})
export class PublisherReportComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  public dateRange: Date[] = [];

  @Select(ReportState.getPublisherReport) publisherReports$: Observable<
    IPublisherReport[]
  >;
  @Select(ReportState.getPublisherReportSummary)
  reportSummary$: Observable<IPublisherReport>;
  @Select(ReportState.getLoading) loading$: Observable<boolean>;
  @Select(PublisherState.getList) publishers$: Observable<IPublisherResponse[]>;
  @Select(PublisherState.getLoading) publishersLoading$: Observable<boolean>;

  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public readonly shortDateFormat = 'dd/MM/yyyy';
  public columns = publisherReportTableColumns;
  public summaryColumns = publisherReportSummaryColumns;
  public readonly settings = publisherReportTableSetting;
  public readonly publisherReportTableEnum = PublisherReportTableEnum;
  public selectedPublishers: string[] = [];
  public selectedPublisherReports: string[] = [];
  private destroy$ = new Subject<void>();
  private publisherPage = 1;
  private readonly publisherPageSize = 24;
  private searchText = '';

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public get isAnySelected() {
    return this.selectedPublisherReports.length > 0;
  }

  ngOnInit() {
    this.onGetReport();
    this.store.dispatch(
      new GetPublisherList({
        page: this.publisherPage,
        pageSize: this.publisherPageSize,
      })
    );
  }

  ngAfterViewInit() {
    this.columns = this.columns.map((item) => {
      if (item.id !== PublisherReportTableEnum.CHECKBOX) {
        return item;
      }
      return {
        ...item,
        title: this.checkboxHeader,
      };
    });
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearPublisherReport());
  }

  public loadMorePublishers() {
    this.publishersLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.publisherPage += 1;
        this.store.dispatch(
          new GetMorePublisherList({
            page: this.publisherPage,
            pageSize: this.publisherPageSize,
            keyword: this.searchText,
          })
        );
      },
    });
  }

  public onChangeDateRange(event: Date[]) {
    this.dateRange = event;
    this.onGetReport();
  }

  public onGetReport() {
    this.store
      .dispatch(
        new GetPublisherReport({
          startDate: this.dateRange[0],
          endDate: this.dateRange[1],
          publisherIds: this.selectedPublishers,
        })
      )
      .subscribe({
        next: () => {
          this.selectedPublisherReports = [];
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public isReportSelected(id: string): boolean {
    return !!this.selectedPublisherReports.find((item) => item === id);
  }

  public selectPublisherReport(publisherId: string) {
    if (!this.isReportSelected(publisherId)) {
      this.selectedPublisherReports = [
        ...this.selectedPublisherReports,
        publisherId,
      ];
    } else {
      this.selectedPublisherReports = this.selectedPublisherReports.filter(
        (item) => item !== publisherId
      );
    }
  }

  public selectAllPublisherReports(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.publisherReports$.pipe(take(1)).subscribe({
        next: (publishers) => {
          this.selectedPublisherReports = publishers.map((item) => item.id);
        },
      });
    } else {
      this.selectedPublisherReports = [];
    }
  }

  public onSelect(value: string[]) {
    this.selectedPublishers = value;
    this.onGetReport();
  }

  public onSearch(event: string) {
    this.searchText = event;
    this.publisherPage = 1;
    this.store.dispatch(
      new GetPublisherList({
        page: this.publisherPage,
        pageSize: this.publisherPageSize,
        keyword: this.searchText,
      })
    );
  }
}
