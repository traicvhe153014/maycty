import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import { PublisherReportTableEnum } from '@features/feature-report/enums';
import { IPublisherReport } from '@features/feature-report/store';

export const publisherReportTableSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '90vw',
  summaryRow: true,
} as DataTableSettingModel;

export const publisherReportTableColumns: ISettingColumnTable<
  IPublisherReport,
  any
>[] = [
  {
    id: PublisherReportTableEnum.CHECKBOX,
    align: 'center',
    width: '40px',
    title: '',
    pinLeft: true,
  },
  {
    id: PublisherReportTableEnum.INDEX,
    width: '40px',
    title: 'STT',
    pinLeft: true,
  },
  {
    id: PublisherReportTableEnum.FULL_NAME,
    width: '200px',
    title: 'Tên publisher',
    pinLeft: true,
  },
  {
    id: PublisherReportTableEnum.EMAIL,
    width: '200px',
    title: 'Email',
    pinLeft: true,
  },
  {
    id: PublisherReportTableEnum.VIEWS,
    width: '125px',
    title: 'Lượt xem',
  },
  {
    id: PublisherReportTableEnum.CLICKS,
    width: '125px',
    title: 'Lượt nhấn (qc)',
  },
  {
    id: PublisherReportTableEnum.CTR,
    width: '100px',
    title: 'CTR (%)',
  },
  {
    id: PublisherReportTableEnum.EARNED,
    width: '150px',
    title: 'Tổng giá mua (VNĐ)',
  },
];

export const publisherReportSummaryColumns: ISummaryColumnTable<IPublisherReport>[] =
  [
    {
      id: PublisherReportTableEnum.INDEX,
      colspan: 4,
      pinLeft: true,
    },
    {
      id: PublisherReportTableEnum.VIEWS,
    },
    {
      id: PublisherReportTableEnum.CLICKS,
    },
    {
      id: PublisherReportTableEnum.CTR,
    },
    {
      id: PublisherReportTableEnum.EARNED,
    },
  ];
