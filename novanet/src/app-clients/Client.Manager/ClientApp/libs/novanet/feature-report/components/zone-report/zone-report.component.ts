import { take } from 'rxjs/operators';
import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  zoneReportSummaryColumns,
  zoneReportTableColumns,
  zoneReportTableSetting,
} from '@features/feature-report/constants';
import { ZoneReportTableEnum } from '@features/feature-report/enums';
import {
  ClearZoneReport,
  GetMoreZoneReport,
  GetZoneReport,
  IZoneReport,
  ReportState,
} from '@features/feature-report/store';
import { Observable } from 'rxjs';
import {
  ClearZoneList,
  GetMoreZoneList,
  GetZoneList,
  IZonesResponse,
  SettingsState,
} from '@shared/settings/store';
import { User } from '@models';
import { AuthService } from '@core';

@Component({
  selector: 'novanet-zone-report',
  templateUrl: './zone-report.component.html',
  styleUrls: ['./zone-report.component.scss'],
})
export class ZoneReportComponent implements OnInit, OnDestroy, AfterViewInit {
  public dateRange: Date[] = [];

  @Select(ReportState.getZoneReport) zoneReports$: Observable<IZoneReport[]>;
  @Select(ReportState.getZoneReportSummary)
  reportSummary$: Observable<IZoneReport>;
  @Select(ReportState.getLoading) loading$: Observable<boolean>;
  @Select(SettingsState.getLoadingZones)
  zonesLoading$: Observable<boolean>;
  @Select(SettingsState.getZones) zones$: Observable<IZonesResponse>;
  @Select(ReportState.getPageZones) currentPage$: Observable<number>;

  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public readonly shortDateFormat = 'dd/MM/yyyy';
  public columns = zoneReportTableColumns;
  public summaryColumns = zoneReportSummaryColumns;
  public readonly settings = zoneReportTableSetting;
  public readonly zoneReportTableEnum = ZoneReportTableEnum;
  public selectedZone: string[] = [];
  public selectedZoneReports: string[] = [];
  public user: User;
  private zonePage = 1;
  private readonly zonePageSize = 34;
  private searchText = '';

  constructor(private store: Store, private authService: AuthService) {}

  public get isAdmin(): boolean {
    if (!this.user) {
      return false;
    }
    return (
      this.user.roles === 'Admin' || this.user.roles === 'PublisherManager'
    );
  }

  public get isAnySelected() {
    return this.selectedZoneReports.length > 0;
  }

  ngOnInit() {
    this.user = this.authService.getUserData();
    this.onGetReport();
    this.store.dispatch(
      new GetZoneList({
        page: this.zonePage,
        pageSize: this.zonePageSize,
        withZoneTemplate: true,
      })
    );
  }

  ngAfterViewInit() {
    const isAdmin = this.isAdmin;
    this.columns = this.columns.map((item) => {
      if (item.id === ZoneReportTableEnum.CHECKBOX) {
        return {
          ...item,
          title: this.checkboxHeader,
        };
      }
      if (item.id === ZoneReportTableEnum.DOMAIN_NAME && !isAdmin) {
        return {
          ...item,
          title: 'Website',
        };
      }
      return item;
    });
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearZoneReport());
    this.store.dispatch(new ClearZoneList());
  }

  public onChangeDateRange(event: Date[]) {
    this.dateRange = event;
    this.onGetReport();
  }

  public onGetReport() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetZoneReport({
            page: 1,
            pageSize: this.zonePageSize * currentPage,
            startDate: this.dateRange[0],
            endDate: this.dateRange[1],
            zoneIds: this.selectedZone,
          })
        );
      },
    });
  }

  public onSelect(value: string[]) {
    this.selectedZone = value;
    this.onGetReport();
  }

  public loadMoreZones() {
    this.zonesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.zonePage += 1;
        this.store.dispatch(
          new GetMoreZoneList({
            page: this.zonePage,
            pageSize: this.zonePageSize,
            keyWord: this.searchText,
            withZoneTemplate: true,
          })
        );
      },
    });
  }

  public isReportSelected(id: string): boolean {
    return !!this.selectedZoneReports.find((item) => item === id);
  }

  public selectZoneReport(publisherId: string) {
    if (!this.isReportSelected(publisherId)) {
      this.selectedZoneReports = [...this.selectedZoneReports, publisherId];
    } else {
      this.selectedZoneReports = this.selectedZoneReports.filter(
        (item) => item !== publisherId
      );
    }
  }

  public selectAllZoneReports(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.zoneReports$.pipe(take(1)).subscribe({
        next: (zoneReport) => {
          this.selectedZoneReports = zoneReport.map((item) => item.id);
        },
      });
    } else {
      this.selectedZoneReports = [];
    }
  }

  public onNextPage() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMoreZoneReport({
            page: currentPage + 1,
            pageSize: this.zonePageSize,
            startDate: this.dateRange[0],
            endDate: this.dateRange[1],
            zoneIds: this.selectedZone,
          })
        );
      },
    });
  }

  public onSearch(event: string) {
    this.searchText = event;
    this.zonePage = 1;
    this.store.dispatch(
      new GetZoneList({
        page: this.zonePage,
        pageSize: this.zonePageSize,
        keyWord: this.searchText,
        withZoneTemplate: true,
      })
    );
  }
}
