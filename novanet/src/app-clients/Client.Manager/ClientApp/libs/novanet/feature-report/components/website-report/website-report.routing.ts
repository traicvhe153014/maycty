import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { WebsiteReportComponent } from './website-report.component';

const routes: Routes = [
  {
    path: '',
    component: WebsiteReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
})
export class WebsiteReportRouting {}
