import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'fraud-click',
    loadChildren: () =>
      import('./components/fraud-click-report').then(
        (m) => m.FraudClickReportModule
      ),
  },
  {
    path: 'website',
    loadChildren: () =>
      import('./components/website-report').then((m) => m.WebsiteReportModule),
  },
  {
    path: 'zone',
    loadChildren: () =>
      import('./components/zone-report').then((m) => m.ZoneReportModule),
  },
  {
    path: 'publisher',
    loadChildren: () =>
      import('./components/publisher-report').then(
        (m) => m.PublisherReportModule
      ),
  },
  {
    path: 'advertising-template',
    loadChildren: () =>
      import('./components/advertising-template-report').then(
        (m) => m.AdvertisingTemplateReportModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
})
export class FeatureReportRouting {}
