import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  websiteReportSummaryColumns,
  websiteReportTableColumns,
  websiteReportTableSetting,
} from '@features/feature-report/constants';
import { WebsiteReportTableEnum } from '@features/feature-report/enums';
import {
  ClearWebsiteReport,
  GetWebsiteReport,
  IWebsiteReport,
  ReportState,
} from '@features/feature-report/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  GetMoreWebsiteList,
  GetWebsiteList,
  IWebsitesResponse,
  SettingsState,
} from '@shared/settings/store';
import { AuthService } from '@features/auth/store';
import { User } from '@models';

@Component({
  selector: 'novanet-website-report',
  templateUrl: './website-report.component.html',
  styleUrls: ['./website-report.component.scss'],
})
export class WebsiteReportComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  public dateRange: Date[] = [];

  @Select(ReportState.getWebsiteReport) websiteReports$: Observable<
    IWebsiteReport[]
  >;
  @Select(ReportState.getWebsiteReportSummary)
  reportSummary$: Observable<IWebsiteReport>;
  @Select(ReportState.getLoading) loading$: Observable<boolean>;
  @Select(SettingsState.getWebsites) websites$: Observable<IWebsitesResponse>;
  @Select(SettingsState.getLoadingWebsites)
  websitesLoading$: Observable<boolean>;

  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public readonly shortDateFormat = 'dd/MM/yyyy';
  public columns = websiteReportTableColumns;
  public summaryColumns = websiteReportSummaryColumns;
  public readonly settings = websiteReportTableSetting;
  public readonly websiteReportTableEnum = WebsiteReportTableEnum;
  public user: User;
  public selectedWebsites: string[] = [];
  public selectedWebsiteReports: string[] = [];
  private websitePage = 1;
  private readonly websitePageSize = 24;
  private searchText = '';

  constructor(
    private store: Store,
    private authService: AuthService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public get isAdmin(): boolean {
    if (!this.user) {
      return false;
    }
    return (
      this.user.roles === 'Admin' || this.user.roles === 'PublisherManager'
    );
  }

  public get isAnySelected() {
    return this.selectedWebsiteReports.length > 0;
  }

  ngOnInit() {
    this.user = this.authService.getUserData();
    this.onGetReport();
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
      })
    );
  }

  ngAfterViewInit() {
    const isAdmin = this.isAdmin;
    this.columns = this.columns
      .filter((item) => {
        if (item.id !== WebsiteReportTableEnum.PUBLISHER) {
          return true;
        }
        return isAdmin;
      })
      .map((item) => {
        if (item.id !== WebsiteReportTableEnum.CHECKBOX) {
          return item;
        }
        return {
          ...item,
          title: this.checkboxHeader,
        };
      });
    if (!isAdmin) {
      this.summaryColumns = this.summaryColumns.map((item) => {
        if (item.id !== WebsiteReportTableEnum.INDEX) {
          return item;
        }
        return {
          ...item,
          colspan: 3,
        };
      });
    }
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearWebsiteReport());
  }

  public loadMoreWebsites() {
    this.websitesLoading$.pipe(take(1)).subscribe({
      next: (loading) => {
        if (loading) {
          return;
        }
        this.websitePage += 1;
        this.store.dispatch(
          new GetMoreWebsiteList({
            page: this.websitePage,
            pageSize: this.websitePageSize,
            keyWord: this.searchText,
          })
        );
      },
    });
  }

  public onChangeDateRange(event: Date[]) {
    this.dateRange = event;
    this.onGetReport();
  }

  public onGetReport() {
    this.store
      .dispatch(
        new GetWebsiteReport({
          startDate: this.dateRange[0],
          endDate: this.dateRange[1],
          websiteIds: this.selectedWebsites,
        })
      )
      .subscribe({
        next: () => {
          this.selectedWebsiteReports = [];
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public isReportSelected(id: string): boolean {
    return !!this.selectedWebsiteReports.find((item) => item === id);
  }

  public selectWebsiteReport(id: string) {
    if (!this.isReportSelected(id)) {
      this.selectedWebsiteReports = [...this.selectedWebsiteReports, id];
    } else {
      this.selectedWebsiteReports = this.selectedWebsiteReports.filter(
        (item) => item !== id
      );
    }
  }

  public selectAllWebsiteReports(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.websiteReports$.pipe(take(1)).subscribe({
        next: (websiteReports) => {
          this.selectedWebsiteReports = websiteReports.map((item) => item.id);
        },
      });
    } else {
      this.selectedWebsiteReports = [];
    }
  }

  public onSelect(value: string[]) {
    this.selectedWebsites = value;
    this.onGetReport();
  }

  public onSearch(event: string) {
    this.searchText = event;
    this.websitePage = 1;
    this.store.dispatch(
      new GetWebsiteList({
        page: this.websitePage,
        pageSize: this.websitePageSize,
        keyWord: this.searchText,
      })
    );
  }
}
