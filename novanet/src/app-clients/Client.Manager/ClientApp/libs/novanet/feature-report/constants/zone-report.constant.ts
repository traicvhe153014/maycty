import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import { ZoneReportTableEnum } from '@features/feature-report/enums';
import { IZoneReport } from '@features/feature-report/store';

export const zoneReportTableSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '95vw',
  summaryRow: true,
} as DataTableSettingModel;

export const zoneReportTableColumns: ISettingColumnTable<IZoneReport, any>[] = [
  {
    id: ZoneReportTableEnum.CHECKBOX,
    align: 'center',
    width: '40px',
    title: '',
    pinLeft: true,
  },
  {
    id: ZoneReportTableEnum.INDEX,
    width: '70px',
    title: 'Stt',
    pinLeft: true,
  },
  {
    id: ZoneReportTableEnum.TEMPLATE_TYPE,
    width: '180px',
    title: 'Vùng quảng cáo',
    pinLeft: true,
  },
  {
    id: ZoneReportTableEnum.DOMAIN_NAME,
    width: '330px',
    title: 'Website - Publisher',
    pinLeft: true,
  },
  {
    id: ZoneReportTableEnum.VIEWS,
    width: '125px',
    title: 'Lượt xem (qc)',
  },
  {
    id: ZoneReportTableEnum.CLICKS,
    width: '125px',
    title: 'Lượt nhấn (qc)',
  },
  {
    id: ZoneReportTableEnum.CTR,
    width: '100px',
    title: 'CTR (%)',
  },
  {
    id: ZoneReportTableEnum.EARNED,
    width: '150px',
    title: 'Tổng giá mua (VNĐ)',
  },
  {
    id: ZoneReportTableEnum.DEFAULT_BANNER_VIEWS,
    width: '200px',
    title: 'Lượt xem banner mặc định',
  },
  {
    id: ZoneReportTableEnum.BACKUP_VIEWS,
    width: '200px',
    title: 'Lượt xem banner backup',
  },
];

export const zoneReportSummaryColumns: ISummaryColumnTable<IZoneReport>[] = [
  {
    id: ZoneReportTableEnum.INDEX,
    colspan: 4,
    pinLeft: true,
  },
  {
    id: ZoneReportTableEnum.VIEWS,
  },
  {
    id: ZoneReportTableEnum.CLICKS,
  },
  {
    id: ZoneReportTableEnum.CTR,
  },
  {
    id: ZoneReportTableEnum.EARNED,
  },
  {
    id: ZoneReportTableEnum.DEFAULT_BANNER_VIEWS,
  },
  {
    id: ZoneReportTableEnum.BACKUP_VIEWS,
  },
];
