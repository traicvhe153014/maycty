import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PublisherReportComponent } from './publisher-report.component';

const routes: Routes = [
  {
    path: '',
    component: PublisherReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
})
export class PublisherReportRouting {}
