import {
  IAdvertisingTemplateReportRequest,
  IAdvertisingTemplateReportResponse,
  IFraudClickReportResponse,
  IPublisherReportRequest,
  IPublisherReportResponse,
  IWebsiteReportRequest,
  IWebsiteReportResponse,
  IZoneReportRequest,
  IZoneReportResponse,
} from './report-state.model';

const enum ReportActions {
  GetFraudClickReport = '[Report] Get Fraud Click Report',
  GetFraudClickReportSuccess = '[Report] Get Fraud Click Report Success',
  GetFraudClickReportError = '[Report] Get Fraud Click Report Error',
  ClearFraudClickReport = '[Report] Clear Fraud Click Report',
  GetWebsiteReport = '[Report] Get Website Report',
  GetWebsiteReportSuccess = '[Report] Get Website Report Success',
  GetWebsiteReportError = '[Report] Get Website Report Error',
  ClearWebsiteReport = '[Report] Clear Website Report',
  GetZoneReport = '[Report] Get Zone Report',
  GetZoneReportSuccess = '[Report] Get Zone Report Success',
  GetZoneReportError = '[Report] Get Zone Report Error',
  ClearZoneReport = '[Report] Clear Zone Report',
  GetPublisherReport = '[Report] Get Publisher Report',
  GetPublisherReportSuccess = '[Report] Get Publisher Report Success',
  GetPublisherReportError = '[Report] Get Publisher Report Error',
  ClearPublisherReport = '[Report] Clear Publisher Report',
  GetAdvertisingTemplateReport = '[Report] Get Advertising Template Report',
  GetAdvertisingTemplateReportSuccess = '[Report] Get Advertising Template Report Success',
  GetAdvertisingTemplateReportError = '[Report] Get Advertising Template Report Error',
  ClearAdvertisingTemplateReport = '[Report] Clear Advertising Template Report',
  ClearAllReports = '[Report] Clear All Reports',
  GetMoreZoneReport = '[Report] Get More Zone Report',
  GetMoreZoneReportSuccess = '[Report] Get More Zone Report Success',
  GetMoreZoneReportError = '[Report] Get More Zone Report Error',
}

export class GetFraudClickReport {
  public static readonly type = ReportActions.GetFraudClickReport;

  constructor(public from: Date, public to: Date) {}
}

export class GetFraudClickReportSuccess {
  public static readonly type = ReportActions.GetFraudClickReportSuccess;

  constructor(public response: IFraudClickReportResponse) {}
}

export class GetFraudClickReportError {
  public static readonly type = ReportActions.GetFraudClickReportError;

  constructor(public error: any) {}
}

export class ClearFraudClickReport {
  public static readonly type = ReportActions.ClearFraudClickReport;

  constructor() {}
}

export class GetWebsiteReport {
  public static readonly type = ReportActions.GetWebsiteReport;

  constructor(public payload: IWebsiteReportRequest) {}
}

export class GetWebsiteReportSuccess {
  public static readonly type = ReportActions.GetWebsiteReportSuccess;

  constructor(public response: IWebsiteReportResponse) {}
}

export class GetWebsiteReportError {
  public static readonly type = ReportActions.GetWebsiteReportError;

  constructor(public error: any) {}
}

export class ClearWebsiteReport {
  public static readonly type = ReportActions.ClearWebsiteReport;

  constructor() {}
}

export class GetZoneReport {
  public static readonly type = ReportActions.GetZoneReport;

  constructor(public payload: IZoneReportRequest) {}
}

export class GetZoneReportSuccess {
  public static readonly type = ReportActions.GetZoneReportSuccess;

  constructor(public response: IZoneReportResponse) {}
}

export class GetZoneReportError {
  public static readonly type = ReportActions.GetZoneReportError;

  constructor(public error: any) {}
}

export class ClearZoneReport {
  public static readonly type = ReportActions.ClearZoneReport;

  constructor() {}
}

export class GetPublisherReport {
  public static readonly type = ReportActions.GetPublisherReport;

  constructor(public payload: IPublisherReportRequest) {}
}

export class GetPublisherReportSuccess {
  public static readonly type = ReportActions.GetPublisherReportSuccess;

  constructor(public response: IPublisherReportResponse) {}
}

export class GetPublisherReportError {
  public static readonly type = ReportActions.GetPublisherReportError;

  constructor(public error: any) {}
}

export class ClearPublisherReport {
  public static readonly type = ReportActions.ClearPublisherReport;

  constructor() {}
}

export class GetAdvertisingTemplateReport {
  public static readonly type = ReportActions.GetAdvertisingTemplateReport;

  constructor(public payload: IAdvertisingTemplateReportRequest) {}
}

export class GetAdvertisingTemplateReportSuccess {
  public static readonly type =
    ReportActions.GetAdvertisingTemplateReportSuccess;

  constructor(public response: IAdvertisingTemplateReportResponse) {}
}

export class GetAdvertisingTemplateReportError {
  public static readonly type = ReportActions.GetAdvertisingTemplateReportError;

  constructor(public error: any) {}
}

export class ClearAdvertisingTemplateReport {
  public static readonly type = ReportActions.ClearAdvertisingTemplateReport;

  constructor() {}
}

export class ClearAllReports {
  public static readonly type = ReportActions.ClearAllReports;

  constructor() {}
}

export class GetMoreZoneReport {
  public static readonly type = ReportActions.GetMoreZoneReport;

  constructor(public params: IZoneReportRequest) {}
}

export class GetMoreZoneReportSuccess {
  public static readonly type = ReportActions.GetMoreZoneReportSuccess;

  constructor(
    public response: IZoneReportResponse,
    public params: IZoneReportRequest
  ) {}
}

export class GetMoreZoneReportError {
  public static readonly type = ReportActions.GetMoreZoneReportError;

  constructor(public error: any) {}
}
