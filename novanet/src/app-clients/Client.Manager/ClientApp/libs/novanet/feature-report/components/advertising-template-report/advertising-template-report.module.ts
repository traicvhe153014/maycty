import { NgModule } from '@angular/core';
import { AdvertisingTemplateReportComponent } from './advertising-template-report.component';
import { CommonModule } from '@angular/common';
import { AdvertisingTemplateReportRoutingModule } from './advertising-template-report.routing';
import { RouterModule } from '@angular/router';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule, FormatterModule } from '@core';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzTagModule } from 'ng-zorro-antd/tag';

@NgModule({
  declarations: [AdvertisingTemplateReportComponent],
  imports: [
    CommonModule,
    AdvertisingTemplateReportRoutingModule,
    RouterModule,
    NovanetInputModule,
    FormsModule,
    DataTableModule,
    FormatterModule,
    ReactiveFormsModule,
    NzSelectModule,
    NzPopoverModule,
    NzCheckboxModule,
    ReactiveFormsModule,
    NzTagModule,
  ],
  exports: [AdvertisingTemplateReportComponent],
})
export class AdvertisingTemplateReportModule {}
