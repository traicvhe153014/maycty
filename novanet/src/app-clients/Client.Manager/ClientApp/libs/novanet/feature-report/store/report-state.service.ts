import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  IFraudClickReportResponse,
  IPublisherReportRequest,
  IPublisherReportResponse,
  IWebsiteReportRequest,
  IWebsiteReportResponse,
  IZoneReportRequest,
  IZoneReportResponse,
} from './report-state.model';

const ReportUrls = {
  fraudClick: `FraudClick`,
  website: `Website`,
  zone: `Zone`,
  publisher: `Publisher`,
};

@Injectable()
export class ReportService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'report/api/v1');
  }

  public getFraudClickReport(
    from: Date,
    to: Date
  ): Observable<IFraudClickReportResponse> {
    const params = new HttpParams({
      fromObject: {
        from: from?.toISOString(),
        to: to?.toISOString(),
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IFraudClickReportResponse>>(
        this.createUrl(ReportUrls.fraudClick),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public getWebsiteReport(
    payload: IWebsiteReportRequest
  ): Observable<IWebsiteReportResponse> {
    const processedPayload: { [key: string]: any } = { ...payload };
    Object.keys(processedPayload).forEach(
      (key) =>
        processedPayload[key] === undefined && delete processedPayload[key]
    );
    if (processedPayload.startDate) {
      processedPayload.startDate = processedPayload.startDate.toISOString();
    }
    if (processedPayload.endDate) {
      processedPayload.endDate = processedPayload.endDate.toISOString();
    }
    if (processedPayload.websiteIds) {
      processedPayload.websiteIds = processedPayload.websiteIds.join(',');
    }
    const params = new HttpParams({
      fromObject: processedPayload,
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IWebsiteReportResponse>>(
        this.createUrl(ReportUrls.website),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public getZoneReport(
    payload: IZoneReportRequest
  ): Observable<IZoneReportResponse> {
    const processedPayload: { [key: string]: any } = { ...payload };
    Object.keys(processedPayload).forEach(
      (key) =>
        processedPayload[key] === undefined && delete processedPayload[key]
    );
    if (processedPayload.startDate) {
      processedPayload.startDate = processedPayload.startDate.toISOString();
    }
    if (processedPayload.endDate) {
      processedPayload.endDate = processedPayload.endDate.toISOString();
    }
    if (processedPayload.zoneIds) {
      processedPayload.zoneIds = processedPayload.zoneIds.join(',');
    }
    const params = new HttpParams({
      fromObject: processedPayload,
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IZoneReportResponse>>(
        this.createUrl(ReportUrls.zone),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public getPublisherReport(
    payload: IPublisherReportRequest
  ): Observable<IPublisherReportResponse> {
    const processedPayload: { [key: string]: any } = { ...payload };
    Object.keys(processedPayload).forEach(
      (key) =>
        processedPayload[key] === undefined && delete processedPayload[key]
    );
    if (processedPayload.startDate) {
      processedPayload.startDate = processedPayload.startDate.toISOString();
    }
    if (processedPayload.endDate) {
      processedPayload.endDate = processedPayload.endDate.toISOString();
    }
    if (processedPayload.publisherIds) {
      processedPayload.publisherIds = processedPayload.publisherIds.join(',');
    }
    const params = new HttpParams({
      fromObject: processedPayload,
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IPublisherReportResponse>>(
        this.createUrl(ReportUrls.publisher),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }
}
