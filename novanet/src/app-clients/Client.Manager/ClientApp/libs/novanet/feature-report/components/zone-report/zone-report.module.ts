import { NgModule } from '@angular/core';
import { ZoneReportComponent } from './zone-report.component';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule } from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { ZoneReportRouting } from './zone-report.routing';
import { DataTableModule, FormatterModule } from '@core';
import { ZoneOptionsPipe } from './pipes';
import { GetTemplateNameModule } from 'libs/core/pipes/get-template-name';
import { NzSpinModule } from 'ng-zorro-antd/spin';

@NgModule({
  declarations: [ZoneReportComponent, ZoneOptionsPipe],
  imports: [
    CommonModule,
    NovanetInputModule,
    FormsModule,
    NzDatePickerModule,
    ZoneReportRouting,
    DataTableModule,
    FormatterModule,
    GetTemplateNameModule,
    NzSpinModule,
  ],
  exports: [ZoneReportComponent],
})
export class ZoneReportModule {}
