import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { ReportService } from './report-state.service';
import { ReportState } from './report.state';
import { AdvertisingService } from '@features/campaign-management/store';

@NgModule({
  imports: [NgxsModule.forFeature([ReportState])],
  providers: [ReportService, AdvertisingService],
})
export class ReportStateModule {}
