import { NgModule } from '@angular/core';
import { FraudClickReportComponent } from './fraud-click-report.component';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule } from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { FraudClickReportRouting } from './fraud-click-report.routing';
import { DataTableModule, FormatterModule } from '@core';

@NgModule({
  declarations: [FraudClickReportComponent],
  imports: [
    CommonModule,
    NovanetInputModule,
    FormsModule,
    NzDatePickerModule,
    FraudClickReportRouting,
    DataTableModule,
    FormatterModule,
  ],
  exports: [FraudClickReportComponent],
})
export class FraudClickReportModule {}
