export enum WebsiteReportTableEnum {
  CHECKBOX,
  INDEX,
  DOMAIN_NAME,
  PUBLISHER,
  VIEWS,
  CLICKS,
  CTR,
  EARNED,
}
