export enum PublisherReportTableEnum {
  CHECKBOX,
  INDEX,
  USER_NAME,
  FULL_NAME,
  EMAIL,
  VIEWS,
  CLICKS,
  CTR,
  EARNED,
}
