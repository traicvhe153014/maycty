import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { GlobalNotificationEnum } from '@shared/global-notification';
import { ReportService } from './report-state.service';
import {
  IAdvertisingTemplateReport,
  IFraudClickReport,
  IPublisherReport,
  IReportStateModel,
  IWebsiteReport,
  IZoneReport,
} from './report-state.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  ClearAdvertisingTemplateReport,
  ClearAllReports,
  ClearFraudClickReport,
  ClearPublisherReport,
  ClearWebsiteReport,
  ClearZoneReport,
  GetAdvertisingTemplateReport,
  GetAdvertisingTemplateReportError,
  GetAdvertisingTemplateReportSuccess,
  GetFraudClickReport,
  GetFraudClickReportError,
  GetFraudClickReportSuccess,
  GetMoreZoneReport,
  GetMoreZoneReportError,
  GetMoreZoneReportSuccess,
  GetPublisherReport,
  GetPublisherReportError,
  GetPublisherReportSuccess,
  GetWebsiteReport,
  GetWebsiteReportError,
  GetWebsiteReportSuccess,
  GetZoneReport,
  GetZoneReportError,
  GetZoneReportSuccess,
} from './report-state.action';
import { AdvertisingService } from '@features/campaign-management/store';

@Injectable()
@State<IReportStateModel>({
  name: 'report',
  defaults: {
    loading: false,
    fraudClickReports: [],
    fraudClickReportSummary: undefined,
    websiteReports: [],
    websiteReportSummary: undefined,
    publisherReports: [],
    publisherReportSummary: undefined,
    zoneReports: [],
    page: 1,
    hasMorePages: true,
    zoneReportSummary: undefined,
    advertisingTemplateReports: [],
    advertisingTemplateReportSummary: undefined,
  },
})
export class ReportState {
  private readonly globalNotificationEnum = GlobalNotificationEnum;

  constructor(
    private readonly reportService: ReportService,
    private readonly advertisingService: AdvertisingService
  ) {}

  /**
   * Selectors
   */
  @Selector()
  public static getFraudClickReport(
    state: IReportStateModel
  ): IFraudClickReport[] {
    return state.fraudClickReports;
  }

  @Selector()
  public static getFraudClickReportSummary(
    state: IReportStateModel
  ): IFraudClickReport {
    return state.fraudClickReportSummary;
  }

  @Selector()
  public static getWebsiteReport(state: IReportStateModel): IWebsiteReport[] {
    return state.websiteReports;
  }

  @Selector()
  public static getWebsiteReportSummary(
    state: IReportStateModel
  ): IWebsiteReport {
    return state.websiteReportSummary;
  }

  @Selector()
  public static getZoneReport(state: IReportStateModel): IZoneReport[] {
    return state.zoneReports;
  }

  @Selector()
  public static getZoneReportSummary(state: IReportStateModel): IZoneReport {
    return state.zoneReportSummary;
  }

  @Selector()
  public static getPublisherReport(
    state: IReportStateModel
  ): IPublisherReport[] {
    return state.publisherReports;
  }

  @Selector()
  public static getPublisherReportSummary(
    state: IReportStateModel
  ): IPublisherReport {
    return state.publisherReportSummary;
  }

  @Selector()
  public static getAdvertisingTemplateReport(
    state: IReportStateModel
  ): IAdvertisingTemplateReport[] {
    return state.advertisingTemplateReports;
  }

  @Selector()
  public static getAdvertisingTemplateReportSummary(
    state: IReportStateModel
  ): IAdvertisingTemplateReport {
    return state.advertisingTemplateReportSummary;
  }

  @Selector()
  public static getLoading(state: IReportStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getPageZones(state: IReportStateModel): number {
    return state.page;
  }
  /**
   * Actions
   */
  // Fraud Click Report Actions
  @Action(GetFraudClickReport)
  public getFraudClickReport(
    ctx: StateContext<IReportStateModel>,
    { from, to }: GetFraudClickReport
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.reportService.getFraudClickReport(from, to).pipe(
      map((response) => ctx.dispatch(new GetFraudClickReportSuccess(response))),
      catchError((err) => ctx.dispatch(new GetFraudClickReportError(err)))
    );
  }

  @Action(GetFraudClickReportSuccess)
  public getFraudClickReportSuccess(
    ctx: StateContext<IReportStateModel>,
    { response }: GetFraudClickReportSuccess
  ) {
    ctx.patchState({
      loading: false,
      fraudClickReports: response.data,
      fraudClickReportSummary: response.summary,
    });
  }

  @Action(GetFraudClickReportError)
  public getFraudClickReportError(
    ctx: StateContext<IReportStateModel>,
    { error }: GetFraudClickReportError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(ClearFraudClickReport)
  public clearFraudClickReport(ctx: StateContext<IReportStateModel>) {
    ctx.patchState({
      fraudClickReports: [],
      fraudClickReportSummary: undefined,
    });
  }

  // Website Report Actions
  @Action(GetWebsiteReport)
  public getWebsiteReport(
    ctx: StateContext<IReportStateModel>,
    { payload }: GetWebsiteReport
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.reportService.getWebsiteReport(payload).pipe(
      map((response) => ctx.dispatch(new GetWebsiteReportSuccess(response))),
      catchError((err) => ctx.dispatch(new GetWebsiteReportError(err)))
    );
  }

  @Action(GetWebsiteReportSuccess)
  public getWebsiteReportSuccess(
    ctx: StateContext<IReportStateModel>,
    { response }: GetWebsiteReportSuccess
  ) {
    ctx.patchState({
      loading: false,
      websiteReports: response.data,
      websiteReportSummary: response.summary,
    });
  }

  @Action(GetWebsiteReportError)
  public getWebsiteReportError(
    ctx: StateContext<IReportStateModel>,
    { error }: GetWebsiteReportError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(ClearWebsiteReport)
  public clearWebsiteReport(ctx: StateContext<IReportStateModel>) {
    ctx.patchState({ websiteReports: [], websiteReportSummary: undefined });
  }

  // Zone Report Actions
  @Action(GetZoneReport)
  public getZoneReport(
    ctx: StateContext<IReportStateModel>,
    { payload }: GetZoneReport
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.reportService.getZoneReport(payload).pipe(
      map((response) => ctx.dispatch(new GetZoneReportSuccess(response))),
      catchError((err) => ctx.dispatch(new GetZoneReportError(err)))
    );
  }

  @Action(GetZoneReportSuccess)
  public getZoneReportSuccess(
    ctx: StateContext<IReportStateModel>,
    { response }: GetZoneReportSuccess
  ) {
    ctx.patchState({
      loading: false,
      zoneReports: response.data,
      zoneReportSummary: response.summary,
    });
  }

  @Action(GetZoneReportError)
  public getZoneReportError(
    ctx: StateContext<IReportStateModel>,
    { error }: GetZoneReportError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(ClearZoneReport)
  public clearZoneReport(ctx: StateContext<IReportStateModel>) {
    ctx.patchState({ zoneReports: [], zoneReportSummary: undefined });
  }

  // Publisher Report Actions
  @Action(GetPublisherReport)
  public getPublisherReport(
    ctx: StateContext<IReportStateModel>,
    { payload }: GetPublisherReport
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.reportService.getPublisherReport(payload).pipe(
      map((response) => ctx.dispatch(new GetPublisherReportSuccess(response))),
      catchError((err) => ctx.dispatch(new GetPublisherReportError(err)))
    );
  }

  @Action(GetPublisherReportSuccess)
  public getPublisherReportSuccess(
    ctx: StateContext<IReportStateModel>,
    { response }: GetPublisherReportSuccess
  ) {
    ctx.patchState({
      loading: false,
      publisherReports: response.data,
      publisherReportSummary: response.summary,
    });
  }

  @Action(GetPublisherReportError)
  public getPublisherReportError(
    ctx: StateContext<IReportStateModel>,
    { error }: GetPublisherReportError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(ClearPublisherReport)
  public clearPublisherReport(ctx: StateContext<IReportStateModel>) {
    ctx.patchState({ publisherReports: [], publisherReportSummary: undefined });
  }

  // Advertising Template Report Actions
  @Action(GetAdvertisingTemplateReport)
  public getAdvertisingTemplateReport(
    ctx: StateContext<IReportStateModel>,
    { payload }: GetAdvertisingTemplateReport
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.advertisingService.getAdvertisingTemplateReport(payload).pipe(
      map((response) =>
        ctx.dispatch(new GetAdvertisingTemplateReportSuccess(response))
      ),
      catchError((err) =>
        ctx.dispatch(new GetAdvertisingTemplateReportError(err))
      )
    );
  }

  @Action(GetAdvertisingTemplateReportSuccess)
  public getAdvertisingTemplateReportSuccess(
    ctx: StateContext<IReportStateModel>,
    { response }: GetAdvertisingTemplateReportSuccess
  ) {
    ctx.patchState({
      loading: false,
      advertisingTemplateReports: response.data,
      advertisingTemplateReportSummary: response.summary,
    });
  }

  @Action(GetAdvertisingTemplateReportError)
  public getAdvertisingTemplateReportError(
    ctx: StateContext<IReportStateModel>,
    { error }: GetAdvertisingTemplateReportError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(ClearAdvertisingTemplateReport)
  public clearAdvertisingTemplateReport(ctx: StateContext<IReportStateModel>) {
    ctx.patchState({
      advertisingTemplateReports: [],
      advertisingTemplateReportSummary: undefined,
    });
  }

  @Action(ClearAllReports)
  public clearAllReports(ctx: StateContext<IReportStateModel>) {
    ctx.patchState({
      fraudClickReports: [],
      fraudClickReportSummary: undefined,
      websiteReports: [],
      websiteReportSummary: undefined,
      zoneReports: [],
      zoneReportSummary: undefined,
      publisherReports: [],
      publisherReportSummary: undefined,
    });
  }

  @Action(GetMoreZoneReport)
  public getMoreZoneReport(
    ctx: StateContext<IReportStateModel>,
    { params }: GetMoreZoneReport
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.reportService.getZoneReport(params).pipe(
      map((response) =>
        ctx.dispatch(new GetMoreZoneReportSuccess(response, params))
      ),
      catchError((err) => ctx.dispatch(new GetMoreZoneReportError(err)))
    );
  }

  @Action(GetMoreZoneReportSuccess)
  public getMoreZoneReportSuccess(
    ctx: StateContext<IReportStateModel>,
    { response, params }: GetMoreZoneReportSuccess
  ) {
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length === params.pageSize;
    const currentZoneReports = ctx.getState().zoneReports;
    ctx.patchState({
      zoneReports: [...currentZoneReports, ...response.data],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreZoneReportError)
  public getMoreZoneReportError(
    ctx: StateContext<IReportStateModel>,
    { error }: GetMoreZoneReportError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }
}
