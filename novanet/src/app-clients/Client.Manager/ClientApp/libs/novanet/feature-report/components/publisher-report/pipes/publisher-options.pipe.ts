import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { IPublisherResponse } from '@features/feature-publisher-management/store';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'publisherOptions',
})
export class PublisherOptionsPipe implements PipeTransform {
  transform(
    value: Observable<IPublisherResponse[]>
  ): Observable<NzSelectOptionInterface[]> {
    return value.pipe(
      map((publishers) =>
        publishers.map((item) => ({
          label: `${item.fullName} - ${item.email}`,
          value: item.id,
        }))
      )
    );
  }
}
