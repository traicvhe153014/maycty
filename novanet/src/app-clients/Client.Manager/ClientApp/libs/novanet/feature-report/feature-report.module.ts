import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureReportComponent } from './feature-report.component';
import { FeatureReportRouting } from './feature-report.routing';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [FeatureReportComponent],
  imports: [CommonModule, FeatureReportRouting, RouterModule],
  exports: [FeatureReportComponent],
})
export class FeatureReportModule {}
