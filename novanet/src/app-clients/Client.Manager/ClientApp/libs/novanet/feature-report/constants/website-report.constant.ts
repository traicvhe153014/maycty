import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import {
  PublisherReportTableEnum,
  WebsiteReportTableEnum,
} from '@features/feature-report/enums';
import { IWebsiteReport } from '@features/feature-report/store';

export const websiteReportTableSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '90vw',
  summaryRow: true,
} as DataTableSettingModel;

export const websiteReportTableColumns: ISettingColumnTable<
  IWebsiteReport,
  any
>[] = [
  {
    id: PublisherReportTableEnum.CHECKBOX,
    align: 'center',
    width: '40px',
    title: '',
    pinLeft: true,
  },
  {
    id: WebsiteReportTableEnum.INDEX,
    width: '40px',
    title: 'STT',
    pinLeft: true,
  },
  {
    id: WebsiteReportTableEnum.DOMAIN_NAME,
    width: '200px',
    title: 'Domain',
    pinLeft: true,
  },
  {
    id: WebsiteReportTableEnum.PUBLISHER,
    width: '200px',
    title: 'Publisher',
    pinLeft: true,
  },
  {
    id: WebsiteReportTableEnum.VIEWS,
    width: '125px',
    title: 'Lượt xem',
  },
  {
    id: WebsiteReportTableEnum.CLICKS,
    width: '125px',
    title: 'Lượt nhấn (qc)',
  },
  {
    id: WebsiteReportTableEnum.CTR,
    width: '100px',
    title: 'CTR (%)',
  },
  {
    id: WebsiteReportTableEnum.EARNED,
    width: '150px',
    title: 'Tổng giá mua (VNĐ)',
  },
];

export const websiteReportSummaryColumns: ISummaryColumnTable<IWebsiteReport>[] =
  [
    {
      id: WebsiteReportTableEnum.INDEX,
      colspan: 4,
      pinLeft: true,
    },
    {
      id: WebsiteReportTableEnum.VIEWS,
    },
    {
      id: WebsiteReportTableEnum.CLICKS,
    },
    {
      id: WebsiteReportTableEnum.CTR,
    },
    {
      id: WebsiteReportTableEnum.EARNED,
    },
  ];
