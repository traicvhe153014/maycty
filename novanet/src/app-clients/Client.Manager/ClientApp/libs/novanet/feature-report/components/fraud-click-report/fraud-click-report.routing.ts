import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FraudClickReportComponent } from './fraud-click-report.component';

const routes: Routes = [
  {
    path: '',
    component: FraudClickReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
})
export class FraudClickReportRouting {}
