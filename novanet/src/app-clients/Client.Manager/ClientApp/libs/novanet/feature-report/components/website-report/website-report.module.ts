import { NgModule } from '@angular/core';
import { WebsiteReportComponent } from './website-report.component';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { WebsiteReportRouting } from './website-report.routing';
import { DataTableModule, FormatterModule, WebsiteOptionsModule } from '@core';

@NgModule({
  declarations: [WebsiteReportComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzDatePickerModule,
    NovanetInputModule,
    WebsiteReportRouting,
    DataTableModule,
    FormatterModule,
    WebsiteOptionsModule,
  ],
  exports: [WebsiteReportComponent],
})
export class WebsiteReportModule {}
