import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import { AdvertisingTemplateReportTableEnum } from '@features/feature-report/enums';
import { IAdvertisingTemplateReport } from '@features/feature-report/store';
import { ETemplateType } from '@features/campaign-management/enums';

export const advertisingTemplateReportTableSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '95vw',
  summaryRow: true,
  sortType: 'single',
} as DataTableSettingModel;

export const advertisingTemplateReportTableColumns: ISettingColumnTable<
  IAdvertisingTemplateReport,
  any
>[] = [
  {
    id: AdvertisingTemplateReportTableEnum.INDEX,
    align: 'center',
    width: '40px',
    title: 'STT',
    pinLeft: true,
  },
  {
    id: AdvertisingTemplateReportTableEnum.TEMPLATE_TYPE,
    width: '200px',
    title: 'Định dạng quảng cáo',
    pinLeft: true,
  },
  {
    id: AdvertisingTemplateReportTableEnum.ADVERTISING_TYPE,
    width: '120px',
    title: 'Loại quảng cáo',
    sort: true,
    name: 'campaignType',
  },
  {
    id: AdvertisingTemplateReportTableEnum.POSITION,
    width: '200px',
    title: 'Vị trí | Kích thước',
    pinLeft: true,
  },
  {
    id: AdvertisingTemplateReportTableEnum.COST,
    width: '150px',
    title: 'Chi phí (VNĐ)',
    sort: true,
    name: 'cost',
  },
  {
    id: AdvertisingTemplateReportTableEnum.IMPRESSIONS,
    width: '125px',
    title: 'Lượt xem',
    sort: true,
    name: 'impressions',
  },
  {
    id: AdvertisingTemplateReportTableEnum.CLICKS,
    width: '125px',
    title: 'Lượt nhấn (qc)',
    sort: true,
    name: 'clicks',
  },
  {
    id: AdvertisingTemplateReportTableEnum.CTR,
    width: '100px',
    title: 'CTR (%)',
    sort: true,
    name: 'ctr',
    ascending: false,
    descending: true,
  },
  {
    id: AdvertisingTemplateReportTableEnum.CPC,
    width: '125px',
    title: 'CPC (VNĐ)',
    sort: true,
    name: 'cpc',
  },
  {
    id: AdvertisingTemplateReportTableEnum.PURCHASE,
    width: '125px',
    title: 'SL mua hàng',
    sort: true,
    name: 'purchase',
  },
  {
    id: AdvertisingTemplateReportTableEnum.PURCHASE_CONVERSION,
    width: '175px',
    title: '% chuyển đổi mua hàng',
    sort: true,
    name: 'purchaseConversion',
  },
  {
    id: AdvertisingTemplateReportTableEnum.CPS,
    width: '120px',
    title: 'CPS (VNĐ)',
    sort: true,
    name: 'cps',
  },
];

export const advertisingTemplateReportSummaryColumns: ISummaryColumnTable<IAdvertisingTemplateReport>[] =
  [
    {
      id: AdvertisingTemplateReportTableEnum.INDEX,
      colspan: 3,
      pinLeft: true,
    },
    {
      id: AdvertisingTemplateReportTableEnum.ADVERTISING_TYPE,
    },
    {
      id: AdvertisingTemplateReportTableEnum.COST,
    },
    {
      id: AdvertisingTemplateReportTableEnum.IMPRESSIONS,
    },
    {
      id: AdvertisingTemplateReportTableEnum.CLICKS,
    },
    {
      id: AdvertisingTemplateReportTableEnum.CTR,
    },
    {
      id: AdvertisingTemplateReportTableEnum.CPC,
    },
    {
      id: AdvertisingTemplateReportTableEnum.PURCHASE,
    },
    {
      id: AdvertisingTemplateReportTableEnum.PURCHASE_CONVERSION,
    },
    {
      id: AdvertisingTemplateReportTableEnum.CPS,
    },
  ];

export const TemplateOptions: { label: string; value: ETemplateType }[] = [
  { label: 'Mobile banner card', value: ETemplateType.MobileBannerCard },
  { label: 'Interactive Banner', value: ETemplateType.InteractiveBanner },
  { label: 'Flying Carpet', value: ETemplateType.FlyingCarpet },
  { label: 'In Read Ecommerce', value: ETemplateType.InReadEcommerce },
  { label: 'Post In Read', value: ETemplateType.PostInRead },
  { label: 'Catfish Ecom', value: ETemplateType.CatfishEcom },
  { label: 'Notification banner', value: ETemplateType.NotificationBanner },
  {
    label: 'Catfish collab branding',
    value: ETemplateType.CatFishCollabBranding,
  },
  { label: 'Popup banner branding', value: ETemplateType.PopupBannerBranding },
];

export const advertisingTemplateFilterCache = 'AdvertisingTemplateFilter';
