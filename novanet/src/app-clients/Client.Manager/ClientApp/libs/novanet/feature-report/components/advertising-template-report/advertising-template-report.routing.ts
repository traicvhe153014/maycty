import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvertisingTemplateReportComponent } from './advertising-template-report.component';

const routes: Routes = [
  {
    path: '',
    component: AdvertisingTemplateReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AdvertisingTemplateReportRoutingModule {}
