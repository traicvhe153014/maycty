import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ZoneReportComponent } from './zone-report.component';

const routes: Routes = [
  {
    path: '',
    component: ZoneReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
})
export class ZoneReportRouting {}
