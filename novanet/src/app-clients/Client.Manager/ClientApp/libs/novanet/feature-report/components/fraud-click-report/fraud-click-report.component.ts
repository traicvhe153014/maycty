import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  fraudClickReportSummaryColumns,
  fraudClickReportTableColumns,
  fraudClickReportTableSetting,
} from '@features/feature-report/constants';
import { FraudClickReportTableEnum } from '@features/feature-report/enums';
import {
  ClearFraudClickReport,
  GetFraudClickReport,
  IFraudClickReport,
  ReportState,
} from '@features/feature-report/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'novanet-fraud-click-report',
  templateUrl: './fraud-click-report.component.html',
  styleUrls: ['./fraud-click-report.component.scss'],
})
export class FraudClickReportComponent implements OnInit, OnDestroy {
  public dateRange: Date[] = [new Date(), new Date()];

  @Select(ReportState.getFraudClickReport) fraudClickReports$: Observable<
    IFraudClickReport[]
  >;
  @Select(ReportState.getFraudClickReportSummary)
  reportSummary$: Observable<IFraudClickReport>;
  @Select(ReportState.getLoading) loading$: Observable<boolean>;

  public readonly shortDateFormat = 'dd/MM/yyyy';
  public columns = fraudClickReportTableColumns;
  public summaryColumns = fraudClickReportSummaryColumns;
  public readonly settings = fraudClickReportTableSetting;
  public readonly fraudClickReportTableEnum = FraudClickReportTableEnum;

  constructor(private store: Store) {}

  ngOnInit() {
    this.onGetReport();
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearFraudClickReport());
  }

  public onChangeDateRange(event: Date[]) {
    this.dateRange = event;
  }

  public onGetReport() {
    this.store.dispatch(
      new GetFraudClickReport(this.dateRange[0], this.dateRange[1])
    );
  }
}
