import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  ClearAdvertisingTemplateReport,
  GetAdvertisingTemplateReport,
  IAdvertisingTemplateReport,
  ReportState,
} from '@features/feature-report/store';
import { Observable, Subject } from 'rxjs';
import {
  advertisingTemplateFilterCache,
  advertisingTemplateReportSummaryColumns,
  advertisingTemplateReportTableColumns,
  advertisingTemplateReportTableSetting,
  TemplateOptions,
} from '@features/feature-report/constants';
import { AdvertisingTemplateReportTableEnum } from '@features/feature-report/enums';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { ESortType } from '@core/enums';
import { NzSelectComponent } from 'ng-zorro-antd/select/select.component';
import { storage } from '@core';
import { CampaignType } from '@features/campaign-management/store';
import {
  campaignManagementFilter,
  campaignTypeLabelList,
  campaignTypeLabels
} from '@features/campaign-management/constants';

@Component({
  selector: 'novanet-advertising-template-report',
  templateUrl: './advertising-template-report.component.html',
  styleUrls: ['./advertising-template-report.component.scss'],
})
export class AdvertisingTemplateReportComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  @Select(ReportState.getAdvertisingTemplateReport)
  advertisingTemplateReports$: Observable<IAdvertisingTemplateReport[]>;
  @Select(ReportState.getAdvertisingTemplateReportSummary)
  reportSummary$: Observable<IAdvertisingTemplateReport>;
  @Select(ReportState.getLoading) loading$: Observable<boolean>;
  public readonly shortDateFormat = 'dd/MM/yyyy';
  public columns = advertisingTemplateReportTableColumns;
  public summaryColumns = advertisingTemplateReportSummaryColumns;
  public readonly settings = advertisingTemplateReportTableSetting;
  public filterCampaignType: CampaignType | undefined = undefined;
  public readonly advertisingTemplateReportTableEnum =
    AdvertisingTemplateReportTableEnum;
  public searchForm: FormGroup;
  public sort = '-ctr';
  public visible = false;
  public privateVisible = false;
  public selectedTemplatesPrivate: number[] = [];
  public selectedTemplates: number[] = [];
  public readonly ListTemplate = TemplateOptions;
  public readonly campaignTypeLabels = campaignTypeLabels;
  public readonly campaignTypeLabelList = campaignTypeLabelList;
  @ViewChild('templateSelect')
  private templateSelect: NzSelectComponent;
  @ViewChild('templatePrivateSelect')
  private templatePrivateSelect: NzSelectComponent;
  private destroy$ = new Subject<void>();

  constructor(private store: Store, private formBuilder: FormBuilder) {}

  ngAfterViewInit() {
    const self = this;
    this.templateSelect.onItemDelete = new Proxy(
      this.templateSelect.onItemDelete,
      {
        apply(target, that, args) {
          target.apply(that, args);
          self.selectedTemplatesPrivate = self.selectedTemplates;
          self.privateVisible = false;
          self.visible = false;
          self.onGetReport();
          storage.set(
            advertisingTemplateFilterCache,
            self.selectedTemplatesPrivate
          );
        },
      }
    );
  }

  ngOnInit() {
    const templateFilter =
      (storage.get(advertisingTemplateFilterCache) as number[]) ?? [];
    this.selectedTemplates = templateFilter;
    this.selectedTemplatesPrivate = templateFilter;
    this.buildSearchForm();
    this.onGetReport();
    this.searchForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe({
      next: () => {
        this.onGetReport();
      },
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearAdvertisingTemplateReport());
  }

  public onFilterCampaignTypeChange(campaignType: CampaignType) {
    this.filterCampaignType = campaignType;
    this.dispatchGetList();
  }

  public onGetReport() {
    this.store.dispatch(
      new GetAdvertisingTemplateReport({
        startDate: this.searchForm.get('date').value[0],
        endDate: this.searchForm.get('date').value[1],
        sorts: this.sort,
        templateTypes: this.selectedTemplates,
        campaignType: this.filterCampaignType,
      })
    );
  }

  public dispatchGetList(value?) {
    if (value) {
      const sortedColumn = value[value.length - 1];
      this.columns.forEach((column) => {
        if (!sortedColumn || sortedColumn?.name !== column.name) {
          column.ascending = false;
          column.descending = false;
        } else {
          switch (sortedColumn.direction) {
            case ESortType.Ascending:
              this.sort = sortedColumn.name;
              column.ascending = true;
              column.descending = false;
              break;
            case ESortType.Descending:
              this.sort = '-' + sortedColumn.name;
              column.ascending = false;
              column.descending = true;
              break;
          }
        }
      });
      if (!sortedColumn) {
        this.sort = '';
      }
    }
    this.onGetReport();
  }

  public onSelect(value: number[]) {
    this.selectedTemplates = value;
    this.onGetReport();
  }

  public onSearch() {
    this.selectedTemplates = this.selectedTemplatesPrivate;
    this.onGetReport();
    this.privateVisible = false;
    this.visible = false;
    storage.set(advertisingTemplateFilterCache, this.selectedTemplatesPrivate);
  }

  private buildSearchForm() {
    const config = {
      ['date']: [[], []],
    };
    this.searchForm = this.formBuilder.group(config);
  }
}
