import { ETemplateType } from '@features/campaign-management/enums';
import { CampaignType } from '@features/campaign-management/store';

export interface IFraudClickReport {
  websiteId: number;
  domain?: string;
  allClick: number;
  trueClick: number;
  falseClick: number;
  realTime: number;
  clickRate: number;
  ip: number;
  doubleClick: number;
  clientCreate: number;
  outOfTimeSpan: number;
  vnProvinceNotConfig: number;
  maxCtr: number;
  countryNotConfig: number;
  percentCutNovanet: number;
}

export interface IFraudClickReportResponse {
  data: IFraudClickReport[];
  summary: IFraudClickReport;
}

export interface IWebsiteReportRequest {
  startDate: Date;
  endDate: Date;
  websiteIds?: string[];
}

export interface IWebsiteReport {
  id: string;
  domainName?: string;
  url?: string;
  publisherEmail?: string;
  views: number;
  clicks: number;
  ctr: number;
  earned: number;
}

export interface IWebsiteReportResponse {
  data: IWebsiteReport[];
  summary: IWebsiteReport;
}

export interface IZoneReport {
  id: string;
  zoneName?: string;
  templateTypes?: ETemplateType[];
  domainName?: string;
  url?: string;
  publisherEmail?: string;
  views: number;
  clicks: number;
  ctr: number;
  earned: number;
  defaultBannerViews?: number;
  backupViews?: number;
  total: number;
}

export interface IZoneReportResponse {
  data: IZoneReport[];
  summary: IZoneReport;
}

export interface IPublisherReportRequest {
  startDate: Date;
  endDate: Date;
  publisherIds?: string[];
}

export interface IPublisherReport {
  id: string;
  userName?: string;
  fullName?: string;
  email?: number;
  views: number;
  clicks: number;
  ctr: number;
  earned: number;
}

export interface IPublisherReportResponse {
  data: IPublisherReport[];
  summary: IPublisherReport;
}

export interface IAdvertisingTemplateReportRequest {
  startDate: Date;
  endDate: Date;
  templateTypes?: ETemplateType[];
  sorts?: string;
  campaignType?: CampaignType;
}

export interface IAdvertisingTemplateReport {
  id: string;
  templateType: ETemplateType;
  templateName: string;
  width: number;
  height: number;
  cost: number;
  impressions: number;
  clicks: number;
  ctr: number;
  cpc: number;
  purchase: number;
  purchaseConversion: number;
  cps: number;
  campaignType: CampaignType;
}

export interface IAdvertisingTemplateReportResponse {
  data: IAdvertisingTemplateReport[];
  summary: IAdvertisingTemplateReport;
}

export interface IReportStateModel {
  loading: boolean;
  fraudClickReports: IFraudClickReport[];
  fraudClickReportSummary?: IFraudClickReport;
  websiteReports: IWebsiteReport[];
  websiteReportSummary?: IWebsiteReport;
  zoneReports: IZoneReport[];
  zoneReportSummary?: IZoneReport;
  publisherReports: IPublisherReport[];
  publisherReportSummary?: IPublisherReport;
  advertisingTemplateReports: IAdvertisingTemplateReport[];
  advertisingTemplateReportSummary?: IAdvertisingTemplateReport;
  page: number;
  hasMorePages: boolean;
}

export interface IZoneReportRequest {
  page?: number;
  pageSize?: number;
  startDate?: Date;
  endDate?: Date;
  zoneIds?: string[];
}
