import { NgModule } from '@angular/core';
import { PublisherReportComponent } from './publisher-report.component';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { PublisherReportRouting } from './publisher-report.routing';
import { CastModule, DataTableModule, FormatterModule } from '@core';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { PublisherOptionsPipe } from '@features/feature-report/components/publisher-report/pipes';

@NgModule({
  declarations: [PublisherReportComponent, PublisherOptionsPipe],
  imports: [
    CommonModule,
    NovanetInputModule,
    FormsModule,
    NzDatePickerModule,
    PublisherReportRouting,
    DataTableModule,
    FormatterModule,
    ReactiveFormsModule,
    NzSelectModule,
    CastModule,
  ],
  exports: [PublisherReportComponent],
})
export class PublisherReportModule {}
