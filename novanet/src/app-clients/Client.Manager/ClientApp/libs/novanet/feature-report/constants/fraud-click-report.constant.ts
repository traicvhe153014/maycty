import {
  DataTableSettingModel,
  ISettingColumnTable,
  ISummaryColumnTable,
} from '@data-table/models';
import { FraudClickReportTableEnum } from '@features/feature-report/enums/fraud-click-report.enum';
import { IFraudClickReport } from '@features/feature-report/store';

export const fraudClickReportTableSetting = {
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '95vw',
  summaryRow: true,
} as DataTableSettingModel;

export const fraudClickReportTableColumns: ISettingColumnTable<
  IFraudClickReport,
  any
>[] = [
  {
    id: FraudClickReportTableEnum.INDEX,
    align: 'center',
    width: '15px',
    title: 'STT',
    pinLeft: true,
  },
  {
    id: FraudClickReportTableEnum.DOMAIN,
    width: '80px',
    title: 'WebsiteId-Domain',
    pinLeft: true,
  },
  {
    id: FraudClickReportTableEnum.ALL_CLICK,
    align: 'center',
    width: '40px',
    title: 'All Click',
  },
  {
    id: FraudClickReportTableEnum.TRUE_CLICK,
    align: 'center',
    width: '40px',
    title: 'True Click',
  },
  {
    id: FraudClickReportTableEnum.FALSE_CLICK,
    align: 'center',
    width: '40px',
    title: 'False Click',
  },
  {
    id: FraudClickReportTableEnum.FALSE_CLICK_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.REAL_TIME,
    align: 'center',
    width: '40px',
    title: 'RealTime',
  },
  {
    id: FraudClickReportTableEnum.REAL_TIME_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.CLICK_RATE,
    align: 'center',
    width: '40px',
    title: 'Click Rate',
  },
  {
    id: FraudClickReportTableEnum.CLICK_RATE_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.IP,
    align: 'center',
    width: '40px',
    title: 'Ip',
  },
  {
    id: FraudClickReportTableEnum.IP_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.DOUBLE_CLICK,
    align: 'center',
    width: '40px',
    title: 'Double Click',
  },
  {
    id: FraudClickReportTableEnum.DOUBLE_CLICK_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.CLIENT_CREATE,
    align: 'center',
    width: '40px',
    title: 'Client Create',
  },
  {
    id: FraudClickReportTableEnum.CLIENT_CREATE_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.OUT_OF_TIMESPAN,
    align: 'center',
    width: '40px',
    title: 'Out Of TimeSpan',
  },
  {
    id: FraudClickReportTableEnum.OUT_OF_TIMESPAN_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.VNPROVINCE_NOT_CONFIG,
    align: 'center',
    width: '40px',
    title: 'Location Not Config',
  },
  {
    id: FraudClickReportTableEnum.VNPROVINCE_NOT_CONFIG_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.MAX_CTR,
    align: 'center',
    width: '40px',
    title: 'Max Ctr',
  },
  {
    id: FraudClickReportTableEnum.MAX_CTR_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.COUNTRY_NOT_CONFIG,
    align: 'center',
    width: '40px',
    title: 'Country',
  },
  {
    id: FraudClickReportTableEnum.COUNTRY_NOT_CONFIG_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
  {
    id: FraudClickReportTableEnum.PERCENT_CUT_NOVANET,
    align: 'center',
    width: '40px',
    title: 'Percent Cut Novanet',
  },
  {
    id: FraudClickReportTableEnum.PERCENT_CUT_NOVANET_PERCENT,
    align: 'center',
    width: '15px',
    title: '%',
  },
];

export const fraudClickReportSummaryColumns: ISummaryColumnTable<IFraudClickReport>[] =
  [
    {
      id: FraudClickReportTableEnum.INDEX,
      colspan: 2,
      pinLeft: true,
    },
    {
      id: FraudClickReportTableEnum.ALL_CLICK,
      align: 'center',
    },
    {
      id: FraudClickReportTableEnum.TRUE_CLICK,
      align: 'center',
    },
    {
      id: FraudClickReportTableEnum.FALSE_CLICK,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.REAL_TIME,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.CLICK_RATE,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.IP,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.DOUBLE_CLICK,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.CLIENT_CREATE,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.OUT_OF_TIMESPAN,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.VNPROVINCE_NOT_CONFIG,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.MAX_CTR,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.COUNTRY_NOT_CONFIG,
      align: 'center',
      colspan: 2,
    },
    {
      id: FraudClickReportTableEnum.PERCENT_CUT_NOVANET,
      align: 'center',
      colspan: 2,
    },
  ];
