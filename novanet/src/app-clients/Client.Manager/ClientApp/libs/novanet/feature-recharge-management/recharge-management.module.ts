import { NgModule } from '@angular/core';
import { RechargeManagementRouting } from './recharge-management.routing';
import { RechargeManagementStateModule } from './store';

const MODULES = [RechargeManagementRouting, RechargeManagementStateModule];

@NgModule({
  imports: [...MODULES],
})
export class RechargeManagementModule {}
