import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddRechargeComponent } from './add-recharge.component';

const routes: Routes = [
  {
    path: '',
    component: AddRechargeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddRechargeRouting {}
