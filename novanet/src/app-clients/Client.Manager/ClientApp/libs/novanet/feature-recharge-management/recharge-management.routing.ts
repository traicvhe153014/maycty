import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
  },
  {
    path: 'add-recharge',
    loadChildren: () =>
      import('@features/feature-recharge-management/components').then(
        (m) => m.AddRechargeModule
      ),
  },
  {
    path: 'list',
    loadChildren: () =>
      import('@features/feature-recharge-management/components').then(
        (m) => m.RechargeHistoryModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RechargeManagementRouting {}
