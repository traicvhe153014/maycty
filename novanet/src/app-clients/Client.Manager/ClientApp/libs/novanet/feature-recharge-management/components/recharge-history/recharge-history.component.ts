import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, switchMap, timer } from 'rxjs';
import {
  DataTableSettingModel,
  IFieldDetail,
  ISettingColumnTable,
} from '@data-table/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalNotificationEnum } from '@shared/global-notification';
import { takeUntil } from 'rxjs/operators';
import { cloneDeep } from 'lodash';
import { IHttpGetRequest } from '@core/models';
import { ERechargeHistory } from '@features/feature-recharge-management/enums';
import {
  RechargeHistoryManagementSetting,
  SettingColumnRechargeHistoryTable,
} from '@features/feature-recharge-management/constants';
import {
  ClearRechargeHistory,
  GetRechargeHistory,
  RechargeManagementState,
} from '@features/feature-recharge-management/store';
import { IRechargeHistoryResponse } from '@features/feature-recharge-management/models';
import { ESortType } from '@core/enums';
import * as dayjs from 'dayjs';

const shortDateFormat = 'dd/MM/yyyy';

@Component({
  selector: 'novanet-recharge-history',
  templateUrl: './recharge-history.component.html',
})
export class RechargeHistoryComponent implements OnInit, OnDestroy {
  @Select(RechargeManagementState.getList)
  public $rechargeHistory: Observable<IRechargeHistoryResponse[]>;

  public listOfData: IRechargeHistoryResponse[] = [];
  public settings: DataTableSettingModel;
  public searchForm: FormGroup;
  public changeProductGroupForm: FormGroup;
  public scrollX: string | null = null;
  public scrollY: string | null = null;
  public filterDateRange: Date[] = [];
  public editEnabled = false;
  public pageSize = 50;

  public readonly shortDateFormat = shortDateFormat;
  public readonly columns: ISettingColumnTable<any, any>[] =
    SettingColumnRechargeHistoryTable;

  public readonly rechargeHistoryEnum = ERechargeHistory;
  public readonly globalNotificationEnum = GlobalNotificationEnum;

  private skip = 0;
  private page = 1;
  private destroy$: Subject<void> = new Subject<void>();
  private dataLoaded = false;
  private sorts = ['-createdAt'];
  private baseSorts = ['-createdAt'];

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.buildSearchForm();
    this.getRechargeHistory();
    const params = {
      page: this.page,
      skip: 0,
      pageSize: this.pageSize,
    } as IHttpGetRequest;
    this.store.dispatch(new GetRechargeHistory(params));
    this.settings = RechargeHistoryManagementSetting;
  }

  public ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearRechargeHistory());
  }

  public onChangeFilterDateRange(result: Date[]) {
    this.filterDateRange = result;
    this.onSearch();
  }

  public onSearch(values?: IFieldDetail[]) {
    this.changeDetectorRef.detach();
    if (values?.length) {
      this.dataLoaded = false;
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }

    this.listOfData = [];
    this.dataLoaded = false;
    const params = {
      page: 1,
      skip: 0,
      pageSize: this.pageSize,
      keyWord: this.searchForm.value.search,
      endDate: this.filterDateRange.length
        ? this.filterDateRange[1].toISOString()
        : null,
      startDate: this.filterDateRange.length
        ? this.filterDateRange[0].toISOString()
        : null,
      sorts: this.sorts,
    } as IHttpGetRequest;

    this.store.dispatch(new GetRechargeHistory(params));
  }

  public checkAll(value: boolean) {
    this.changeDetectorRef.detectChanges();
  }

  public refreshStatus(_event: { id: string; checked: boolean }) {
    this.changeDetectorRef.detectChanges();
  }

  public onChangeProductGroup() {}

  public nextBatch($event?: boolean) {
    if ($event && !this.dataLoaded) {
      this.page += 1;
      this.skip = this.skip + this.pageSize;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        keyWord: this.searchForm.value.search,
        endDate: this.filterDateRange.length
          ? this.filterDateRange[1].toISOString()
          : null,
        startDate: this.filterDateRange.length
          ? this.filterDateRange[0].toISOString()
          : null,
      } as IHttpGetRequest;
      this.store.dispatch(new GetRechargeHistory(params));
    }
  }

  private getRechargeHistory() {
    this.$rechargeHistory
      .pipe(takeUntil(this.destroy$))
      .subscribe((response) => {
        if (this.dataLoaded || !response || !response.length) {
          this.changeDetectorRef.reattach();
          this.changeDetectorRef.detectChanges();
          return;
        }
        let data = cloneDeep(response);
        data = data.map((item) => ({
          ...item,
        }));
        this.listOfData = this.listOfData.concat(data);
        if (response.length < this.pageSize) {
          this.dataLoaded = true;
        }
        this.changeDetectorRef.reattach();
        this.changeDetectorRef.detectChanges();
      });
  }

  private buildSearchForm() {
    const config = {
      ['search']: [''],
      ['date']: [
        [dayjs().add(-1, 'month').toDate(), new Date()],
      ],
    };
    this.searchForm = this.formBuilder.group(config);
    this.searchForm
      .get('date')
      .valueChanges.pipe(
        switchMap((result) => timer((this.filterDateRange = result)))
      )
      .subscribe((result) => {
        this.onSearch();
      });
  }
}
