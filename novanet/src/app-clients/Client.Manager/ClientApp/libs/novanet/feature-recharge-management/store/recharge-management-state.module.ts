import { NgModule } from '@angular/core';
import { RechargeManagementService } from './recharge-management.service';
import { NgxsModule } from '@ngxs/store';
import { RechargeManagementState } from './recharge-management.state';

@NgModule({
  imports: [NgxsModule.forFeature([RechargeManagementState])],
  providers: [RechargeManagementService],
})
export class RechargeManagementStateModule {}
