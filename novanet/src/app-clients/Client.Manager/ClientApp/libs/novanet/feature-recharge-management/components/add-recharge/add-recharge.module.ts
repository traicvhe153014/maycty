import { NgModule } from '@angular/core';
import { AddRechargeComponent } from './add-recharge.component';
import { AddRechargeRouting } from './add-recharge.routing';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { IdentityService } from '@shared/identity';
import { ValidateOnBlurModule } from '@shared/directives';
import { CommonModule } from '@angular/common';

const MODULES = [
  AddRechargeRouting,
  ReactiveFormsModule,
  NzAutocompleteModule,
  FormsModule,
  ValidateOnBlurModule,
];

const COMPONENTS = [AddRechargeComponent];

@NgModule({
  imports: [...MODULES, NovanetInputModule, CommonModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
  providers: [IdentityService],
})
export class AddRechargeModule {}
