import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BaseService, baseUrl } from '@shared/services';
import { IHttpGetRequest } from '@core/models';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import { IAddRecharge, IRechargeHistoryResponse } from '../models';

const PaymentUrls = {
  list: `list`,
  add: `create`,
};

@Injectable()
export class RechargeManagementService extends BaseService {
  constructor(
    httpClient: HttpClient,
    private readonly router: Router,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1/Payment');
  }

  public getRechargeHistory(data: IHttpGetRequest) {
    const params = this.createParams(data);
    return this.httpClient
      .get<ISuccessHttpResponse<IRechargeHistoryResponse[]>>(
        this.createUrl(PaymentUrls.list),
        {
          params,
        }
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public createRecharge(data: IAddRecharge) {
    return this.httpClient
      .post<ISuccessHttpResponse<IRechargeHistoryResponse[]>>(
        this.createUrl(PaymentUrls.add),
        data
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }
}
