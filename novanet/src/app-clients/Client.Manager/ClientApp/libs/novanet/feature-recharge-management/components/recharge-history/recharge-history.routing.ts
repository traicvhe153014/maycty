import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RechargeHistoryComponent } from './recharge-history.component';

const routes: Routes = [
  {
    path: '',
    component: RechargeHistoryComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RechargeHistoryRouting {}
