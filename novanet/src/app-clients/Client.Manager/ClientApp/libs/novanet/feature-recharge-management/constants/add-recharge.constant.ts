import { IFormFieldErrors } from '@models';

export const AddRechargeFormFields = {
  email: {
    name: 'email',
    validationParams: {
      maxLength: 50,
      required: true,
    },
  },
  amount: {
    name: 'amount',
    validationParams: {
      required: true,
    },
  },
  note: {
    name: 'note',
    validationParams: {
      required: true,
    },
  },
};

export const AddRechargeFormFieldErrors: IFormFieldErrors = {
  email: [
    {
      type: 'email',
      message: 'Email không hợp lệ.',
    },
    {
      type: 'required',
      message: 'Không để trống tài khoản.',
    },
  ],
  amount: [
    {
      type: 'required',
      message: 'Không để trống số tiền nạp',
    },
  ],
};

export const RechargeErrorMessages = [
  'Tài khoản giao dịch không có trong hệ thống bạn vui lòng kiểm tra lại',
  'Không thể rút số tiền lớn hơn số tiền trong tài khoản',
];
