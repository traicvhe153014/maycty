import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { IRechargeManagementState } from './recharge-management-state.model';
import { IRechargeHistoryResponse } from '../models';
import { RechargeManagementService } from './recharge-management.service';
import {
  AddRechargeHistory,
  ClearRechargeHistory,
  GetRechargeHistory,
  GetRechargeHistoryFail,
  GetRechargeHistorySuccess,
} from './recharge-management.action';
import { HideLoading, ShowLoading } from '@shared/global-loading';
import { Router } from '@angular/router';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { RechargeErrorMessages } from '@features/feature-recharge-management/constants';
import { UpdateAmount } from '@shared/identity';

@Injectable()
@State<IRechargeManagementState>({
  name: 'RechargeManagementState',
})
export class RechargeManagementState implements NgxsOnInit {
  private label = 'Get Recharge History';

  constructor(
    private rechargeManagementService: RechargeManagementService,
    private router: Router
  ) {}

  @Selector()
  public static getList(
    state: IRechargeManagementState
  ): IRechargeHistoryResponse[] {
    return state.rechargeHistory;
  }

  ngxsOnInit(ctx?: StateContext<any>): void {}

  @Action(GetRechargeHistory)
  public getRechargeHistory(
    ctx: StateContext<IRechargeManagementState>,
    { payload }: GetRechargeHistory
  ) {
    ctx.patchState({
      loading: true,
    });
    this.rechargeManagementService.getRechargeHistory(payload).subscribe(
      (response) => {
        ctx.dispatch(new GetRechargeHistorySuccess(response));
      },
      (error) => {
        ctx.dispatch(new GetRechargeHistoryFail());
      }
    );
  }

  @Action(GetRechargeHistorySuccess)
  public getRechargeHistorySuccess(
    ctx: StateContext<IRechargeManagementState>,
    { payload }: GetRechargeHistorySuccess
  ) {
    ctx.patchState({
      rechargeHistory: payload,
      loading: false,
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(GetRechargeHistoryFail)
  public getRechargeHistoryFail(ctx: StateContext<IRechargeManagementState>) {
    ctx.patchState({
      loading: false,
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(AddRechargeHistory)
  public AddRecharge(
    ctx: StateContext<IRechargeManagementState>,
    { payload }: AddRechargeHistory
  ) {
    ctx.dispatch(new ShowLoading(this.label));
    this.rechargeManagementService.createRecharge(payload).subscribe(
      (response) => {
        ctx.dispatch(new HideLoading(this.label));
        ctx.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Giao dịch thành công'
          )
        );
        ctx.dispatch(new UpdateAmount(payload.amount, payload.email));
        this.router.navigate(['/recharge']);
      },
      (error) => {
        ctx.dispatch(new HideLoading(this.label));
        const errorResponse = error?.error?.errors?.[0];
        let errorMessage = 'Giao dịch thất bại';
        if (RechargeErrorMessages.includes(errorResponse)) {
          errorMessage = errorResponse;
        }
        ctx.dispatch(
          new ShowGlobalNotification(GlobalNotificationEnum.error, errorMessage)
        );
      }
    );
  }

  @Action(ClearRechargeHistory)
  public clearProductGroups(ctx: StateContext<IRechargeManagementState>): void {
    ctx.patchState({
      rechargeHistory: null,
    });
  }
}
