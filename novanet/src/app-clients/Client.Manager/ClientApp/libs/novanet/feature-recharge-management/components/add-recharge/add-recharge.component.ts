import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AddRechargeFormFieldErrors,
  AddRechargeFormFields,
} from '@features/feature-recharge-management/constants';
import { Store } from '@ngxs/store';
import { AddRechargeHistory } from '@features/feature-recharge-management/store';
import { IdentityService } from '@shared/identity';
import { PredicateOperatorEnum } from '@core/enums';
import { debounceTime, take } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'novanet-add-recharge',
  templateUrl: './add-recharge.component.html',
})
export class AddRechargeComponent implements OnInit, OnDestroy {
  public addRechargeFormFields = AddRechargeFormFields;
  public addRechargeFormFieldErrors = AddRechargeFormFieldErrors;
  public addRechargeForm: FormGroup;
  public readonly pageSize = 24;
  public page = 1;
  public searchedUserOptions: string[] = [];
  public searchUserChanged: Subject<Event> = new Subject<Event>();
  private searchUserChangeSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private identityService: IdentityService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public ngOnInit() {
    this.searchUserChangeSubscription = this.searchUserChanged
      .pipe(debounceTime(300))
      .subscribe({
        next: (model) => {
          this.onEmailInput(model);
        },
      });
    this.buildForm();
  }

  public ngOnDestroy() {
    this.searchUserChangeSubscription.unsubscribe();
  }

  public onEmailInput(event: Event) {
    const searchValue = (event.target as HTMLInputElement).value;
    if (!searchValue) {
      this.searchedUserOptions = [];
      this.changeDetectorRef.detectChanges();
      return;
    }
    this.identityService
      .listUser({
        page: 1,
        pageSize: 3,
        keyword: searchValue,
        queryField: 'Email',
        operator: PredicateOperatorEnum.StartsWith,
        isAccountantIgnored: true,
      })
      .pipe(take(1))
      .subscribe({
        next: (data) => {
          this.searchedUserOptions = data.map((item) => item.email);
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public onSubmit() {
    if (!this.addRechargeForm.invalid) {
      this.store.dispatch(new AddRechargeHistory(this.addRechargeForm.value));
    } else {
      this.addRechargeForm.markAllAsTouched();
    }
  }

  private buildForm() {
    const config = {
      [this.addRechargeFormFields['email'].name]: [
        '',
        [
          this.addRechargeFormFields['email'].validationParams.required
            ? Validators.required
            : null,
          Validators.email,
        ],
      ],
      [this.addRechargeFormFields['amount'].name]: [
        '',
        [
          this.addRechargeFormFields['amount'].validationParams.required
            ? Validators.required
            : null,
        ],
      ],
      [this.addRechargeFormFields['note'].name]: [''],
    };
    this.addRechargeForm = this.formBuilder.group(config);
  }
}
