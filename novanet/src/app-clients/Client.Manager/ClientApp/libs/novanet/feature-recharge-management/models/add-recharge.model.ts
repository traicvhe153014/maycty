export interface IAddRecharge {
  email: string;
  amount: number;
  note: string;
}
