import { IHttpGetRequest } from '@core/models';
import { IAddRecharge, IRechargeHistoryResponse } from '../models';

const enum RechargeManagementAction {
  GetRechargeHistory = '[RechargeManagement] GetRechargeHistory',
  GetRechargeHistorySuccess = '[RechargeManagement] GetRechargeHistorySuccess',
  GetRechargeHistoryFail = '[RechargeManagement] GetRechargeHistoryFail',
  AddRecharge = '[RechargeManagement] AddRecharge',
  ClearRechargeHistory = '[RechargeManagement] ClearRechargeHistory',
}

export class GetRechargeHistory {
  static type = RechargeManagementAction.GetRechargeHistory;

  constructor(public payload: IHttpGetRequest) {}
}

export class GetRechargeHistorySuccess {
  static type = RechargeManagementAction.GetRechargeHistorySuccess;

  constructor(public payload: IRechargeHistoryResponse[]) {}
}

export class GetRechargeHistoryFail {
  static type = RechargeManagementAction.GetRechargeHistoryFail;
}

export class AddRechargeHistory {
  static type = RechargeManagementAction.AddRecharge;

  constructor(public payload: IAddRecharge) {}
}

export class ClearRechargeHistory {
  static type = RechargeManagementAction.ClearRechargeHistory;
}
