import { IRechargeHistoryResponse } from '../models';

export interface IRechargeManagementState {
  rechargeHistory: IRechargeHistoryResponse[];
  page: number;
  loading: boolean;
}
