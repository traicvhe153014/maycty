export interface IRechargeHistoryResponse {
  email: string;
  customerName: string;
  amount: number;
  note: string;
  createdAt: string;
}
