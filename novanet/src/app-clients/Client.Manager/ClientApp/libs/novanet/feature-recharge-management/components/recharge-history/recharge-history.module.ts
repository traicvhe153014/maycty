import { NgModule } from '@angular/core';
import { NovanetInputModule } from '@shared/custom-input';
import { RechargeHistoryComponent } from './recharge-history.component';
import { RechargeHistoryRouting } from './recharge-history.routing';
import { DataTableModule } from '@core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';

const MODULES = [
  RechargeHistoryRouting,
  DataTableModule,
  NovanetInputModule,
  ReactiveFormsModule,
  NzDatePickerModule,
  FormsModule,
  CommonModule,
];

const COMPONENTS = [RechargeHistoryComponent];

@NgModule({
  imports: [...MODULES],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class RechargeHistoryModule {}
