import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import { ERechargeHistory } from '../enums';

export const SettingColumnRechargeHistoryTable = [
  {
    id: ERechargeHistory.INDEX,
    title: 'STT',
    width: '40px',
    align: 'center',
  },
  {
    id: ERechargeHistory.CUSTOMER,
    width: '500px',
    title: 'Khách hàng',
    sort: true,
    name: 'Email',
  },
  {
    id: ERechargeHistory.RECHARGE_MONEY,
    width: '200px',
    title: 'Số tiền nạp (VNĐ)',
    sort: true,
    name: 'Amount',
  },
  {
    id: ERechargeHistory.CREATED_DATE,
    title: 'Thời gian nạp tiền',
    width: '200px',
    sort: true,
    name: 'CreatedAt',
  },
  {
    id: ERechargeHistory.NOTE,
    title: 'Ghi chú',
  },
] as ISettingColumnTable<any, any>[];

export const RechargeHistoryManagementSetting = {
  id: 'recharge-history-list',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;
