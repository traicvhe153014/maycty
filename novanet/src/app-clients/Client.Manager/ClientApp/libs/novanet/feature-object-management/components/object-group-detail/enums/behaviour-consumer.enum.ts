export enum EObjectType {
  orderPrice = 1,
  purchaseFrequency = 2,
  purchasedWithin = 3,
  removedFromCart = 4,
  selectedNotCompleted = 5,
  viewedNotAddToCart = 6,
  viewedNotPurchased = 7,
  addedNotPurchased = 8,
  all = 9,
}
