import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { IFormFieldErrors, IFormFields } from '@models';
import {
  BehaviorDropdown,
  objectGroupFormFieldErrors,
  ObjectGroupFormFields,
  ProductDropdown,
  PurchaseFrequencyUnitDropdown,
} from '@features/feature-object-management/constants';
import { EBehavior } from '@features/feature-object-management/enums';
import {
  IConsumerBehaviour,
  IFromTo,
} from '@features/feature-object-management/models';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { cloneDeep, uniq } from 'lodash';
import { EObjectType } from '@features/feature-object-management/components';
import { v4 as uuidv4 } from 'uuid';
import { BehaviourConsumerErrors } from './constants';

@Component({
  selector: 'novanet-behaviour-consumer',
  templateUrl: './behaviour-consumer.component.html',
})
export class BehaviourConsumerComponent implements OnInit, OnDestroy {
  @Input() behaviorConsumer: IConsumerBehaviour[];

  @Output() onBehaviorConsumerData = new EventEmitter<IConsumerBehaviour>();

  public readonly createObjectGroupFormFields: IFormFields =
    ObjectGroupFormFields;

  public form: FormGroup;

  public purchaseFrequencyUnitValues = PurchaseFrequencyUnitDropdown;
  public behaviorValues = BehaviorDropdown;
  public readonly excludeBehaviorValues = BehaviorDropdown.filter(
    (item) => item.id !== EBehavior.ALL
  );
  public products = ProductDropdown;
  public behaviorEnum = EBehavior;
  public behaviorFormArray: FormArray;
  readonly behaviorConsumerError = BehaviourConsumerErrors;
  public readonly objectGroupFormFieldsError: IFormFieldErrors =
    objectGroupFormFieldErrors;

  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private rootFormGroup: FormGroupDirective,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public behaviors(index: number): FormGroup {
    return this.behaviorFormArray.at(index) as FormGroup;
  }

  public behavior(index: number): number {
    return this.behaviors(index).get('behaviorType').value as number;
  }

  public orderPrice(index: number): FormGroup {
    return this.behaviors(index).get('orderPrice') as FormGroup;
  }

  public purchaseFrequency(index: number): FormGroup {
    return this.behaviors(index).get('purchaseFrequency') as FormGroup;
  }

  public viewedNotPurchased(index: number): FormGroup {
    return this.behaviors(index).get('viewedNotPurchased') as FormGroup;
  }

  public viewedNotAddToCart(index: number): FormGroup {
    return this.behaviors(index).get('viewedNotAddToCart') as FormGroup;
  }

  public addedNotPurchased(index: number): FormGroup {
    return this.behaviors(index).get('addedNotPurchased') as FormGroup;
  }

  public removedFromCart(index: number): FormGroup {
    return this.behaviors(index).get('removedFromCart') as FormGroup;
  }

  public purchasedWithin(index: number): FormGroup {
    return this.behaviors(index).get('purchasedWithin') as FormGroup;
  }

  public selectedNotCompleted(index: number): FormGroup {
    return this.behaviors(index).get('selectedNotCompleted') as FormGroup;
  }

  public exclude(index: number): FormArray {
    return this.behaviors(index).get('exclude') as FormArray;
  }

  public excludeBehaviors(index: number, indexExcludeItem: number): FormGroup {
    return this.exclude(index).at(indexExcludeItem) as FormGroup;
  }

  public excludeBehavior(index: number, indexExcludeItem: number): number {
    return this.exclude(index).at(indexExcludeItem).get('behaviorType')
      .value as number;
  }

  public excludeOrderPrice(index: number, indexExcludeItem: number): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('orderPrice') as FormGroup;
  }

  public hasDuplicatedExclude(
    index: number,
    indexExcludeItem: number
  ): boolean {
    return (
      this.behavior(index) === this.excludeBehavior(index, indexExcludeItem)
    );
  }

  public excludePurchaseFrequency(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('purchaseFrequency') as FormGroup;
  }

  public excludeViewedNotPurchased(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('viewedNotPurchased') as FormGroup;
  }

  public excludeviewedNotAddToCart(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('viewedNotAddToCart') as FormGroup;
  }

  public excludeAddedNotPurchased(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('addedNotPurchased') as FormGroup;
  }

  public excludeRemovedFromCart(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('removedFromCart') as FormGroup;
  }

  public excludePurchasedWithin(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('purchasedWithin') as FormGroup;
  }

  public excludeSelectedNotCompleted(
    index: number,
    indexExcludeItem: number
  ): FormGroup {
    return this.exclude(index)
      .at(indexExcludeItem)
      .get('selectedNotCompleted') as FormGroup;
  }

  public ngOnInit(): void {
    this.buildArray();
    this.pushItemToArray();
    this.form = this.rootFormGroup.control;
  }

  public ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public patchData(consumerBehavior: IConsumerBehaviour[]) {
    this.behaviorFormArray.clear();
    let properties = [];
    const groupByConsumerBehavior = consumerBehavior.reduce(
      (groupBehavior, value) => {
        if (!groupBehavior[value.behaviorGroupId]) {
          groupBehavior[value.behaviorGroupId] = [];
        }

        groupBehavior[value.behaviorGroupId].push(value);
        properties.push(value.behaviorGroupId);
        return groupBehavior;
      },
      {}
    );

    properties = uniq(properties);
    const propertiesBehavior = [];
    let mergePropertyItems = [];
    properties.forEach((prop) => {
      propertiesBehavior.push(groupByConsumerBehavior[prop]);
    });
    propertiesBehavior.forEach((array) => {
      const cloneItem = cloneDeep(array[0]) as IConsumerBehaviour;
      delete cloneItem.objectType;
      delete cloneItem.from;
      delete cloneItem.to;
      delete cloneItem.unit;

      array.forEach((item) => {
        switch (item.objectType) {
          case EObjectType.addedNotPurchased:
            cloneItem.addedNotPurchased = this.returnFromToItem(item);
            break;
          case EObjectType.orderPrice:
            cloneItem.orderPrice = this.returnFromToItem(item);
            break;
          case EObjectType.removedFromCart:
            cloneItem.removedFromCart = this.returnFromToItem(item);
            break;
          case EObjectType.selectedNotCompleted:
            cloneItem.selectedNotCompleted = this.returnFromToItem(item);
            break;
          case EObjectType.purchasedWithin:
            cloneItem.purchasedWithin = this.returnFromToItem(item);
            break;
          case EObjectType.viewedNotAddToCart:
            cloneItem.viewedNotAddToCart = this.returnFromToItem(item);
            break;
          case EObjectType.viewedNotPurchased:
            cloneItem.viewedNotPurchased = this.returnFromToItem(item);
            break;
          case EObjectType.purchaseFrequency:
            cloneItem.purchaseFrequency = this.returnFromToItem(item);
            break;
        }
      });
      mergePropertyItems.push(cloneItem);
    });
    mergePropertyItems.forEach((item) => {
      if (item.excludeId) {
        const existItem = mergePropertyItems.find(
          (parentItem) => parentItem.behaviorGroupId === item.excludeId
        ) as IConsumerBehaviour;
        if (existItem) {
          if (!existItem.exclude) {
            existItem.exclude = [];
          }
          existItem.exclude.push(cloneDeep(item));
        }
        mergePropertyItems = mergePropertyItems.filter((x) => x.id !== item.id);
      }
    });
    this.behaviorFormArray.removeAt(0);
    mergePropertyItems.forEach((item) => {
      this.pushItemToArray(item);
    });
    this.onBehaviorConsumerData.emit(this.behaviorFormArray.value);
  }

  public pushItemToArray(item?: IConsumerBehaviour) {
    const formGroup = this.buildConsumerBehaviourForm(item);
    this.behaviorFormArray.push(formGroup);
    this.changeDetectorRef.detectChanges();
  }

  public pushItemToExcludeArray(index: number) {
    const formGroup = this.buildConsumerBehaviourForm(null, true);
    this.exclude(index).push(formGroup);
  }

  public removeBehaviorFromBehaviors(index: number) {
    this.behaviorFormArray.removeAt(index);
  }

  public removeBehaviorFromExcludeBehaviors(
    index: number,
    indexExcludeItem: number
  ) {
    this.exclude(index).removeAt(indexExcludeItem);
  }

  public resetBehavior($event, index: number) {
    this.orderPrice(index).reset({
      objectType: EObjectType.orderPrice,
    });
    this.purchaseFrequency(index).reset({
      objectType: EObjectType.purchaseFrequency,
    });
    this.viewedNotAddToCart(index).reset({
      objectType: EObjectType.viewedNotAddToCart,
    });
    this.addedNotPurchased(index).reset({
      objectType: EObjectType.addedNotPurchased,
    });
    this.removedFromCart(index).reset({
      objectType: EObjectType.removedFromCart,
    });
    this.purchasedWithin(index).reset({
      objectType: EObjectType.purchasedWithin,
    });
    this.selectedNotCompleted(index).reset({
      objectType: EObjectType.selectedNotCompleted,
    });
    this.viewedNotPurchased(index).reset({
      objectType: EObjectType.viewedNotPurchased,
    });
  }

  public resetExcludeBehavior($event, index: number, indexExcludeItem: number) {
    this.excludeOrderPrice(index, indexExcludeItem).reset({
      objectType: EObjectType.orderPrice,
    });
    this.excludePurchaseFrequency(index, indexExcludeItem).reset({
      objectType: EObjectType.purchaseFrequency,
    });
    this.excludeviewedNotAddToCart(index, indexExcludeItem).reset({
      objectType: EObjectType.viewedNotAddToCart,
    });
    this.excludeAddedNotPurchased(index, indexExcludeItem).reset({
      objectType: EObjectType.addedNotPurchased,
    });
    this.excludeRemovedFromCart(index, indexExcludeItem).reset({
      objectType: EObjectType.removedFromCart,
    });
    this.excludePurchasedWithin(index, indexExcludeItem).reset({
      objectType: EObjectType.purchasedWithin,
    });
    this.excludeSelectedNotCompleted(index, indexExcludeItem).reset({
      objectType: EObjectType.selectedNotCompleted,
    });
    this.excludeViewedNotPurchased(index, indexExcludeItem).reset({
      objectType: EObjectType.viewedNotPurchased,
    });
  }

  private returnFromToItem(item: IFromTo) {
    return {
      id: item.id,
      from: item.from,
      to: item.to,
      unit: item.unit,
      activated: true,
    };
  }

  private buildArray() {
    this.behaviorFormArray = this.formBuilder.array([]);
    this.behaviorFormArray.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.onBehaviorConsumerData.emit(value);
      });
  }

  private buildConsumerBehaviourForm(
    item?: IConsumerBehaviour,
    isExclude?: boolean
  ): FormGroup {
    const config = {
      behaviorGroupId: [
        item ? item.behaviorGroupId : uuidv4(),
        [Validators.required],
      ],
      [this.createObjectGroupFormFields['behavior'].name]: [
        item
          ? item.behaviorType
          : isExclude
          ? this.behaviorEnum.BOUGHT
          : this.behaviorEnum.ALL,
        [
          this.createObjectGroupFormFields['behavior'].validationParams.required
            ? Validators.required
            : null,
        ],
      ],
      [this.createObjectGroupFormFields['orderPrice'].name]:
        this.buildFromToForm(item ? item.orderPrice : null),
      [this.createObjectGroupFormFields['purchaseFrequency'].name]:
        this.buildFromToForm(item ? item.purchaseFrequency : null),
      [this.createObjectGroupFormFields['viewedNotPurchased'].name]:
        this.buildFromToForm(item ? item.viewedNotPurchased : null),
      [this.createObjectGroupFormFields['viewedNotAddToCart'].name]:
        this.buildFromToForm(item ? item.viewedNotAddToCart : null),
      [this.createObjectGroupFormFields['addedNotPurchased'].name]:
        this.buildFromToForm(item ? item.addedNotPurchased : null),
      [this.createObjectGroupFormFields['removedFromCart'].name]:
        this.buildFromToForm(item ? item.removedFromCart : null),
      [this.createObjectGroupFormFields['purchasedWithin'].name]:
        this.buildFromToForm(item ? item.purchasedWithin : null),
      [this.createObjectGroupFormFields['selectedNotCompleted'].name]:
        this.buildFromToForm(item ? item.selectedNotCompleted : null),
      [this.createObjectGroupFormFields['exclude'].name]: this.buildExclude(
        item ? item.exclude : null
      ),
    };
    return this.formBuilder.group(config);
  }

  private buildExclude(exclude?: IConsumerBehaviour[]) {
    const array = this.formBuilder.array([]);
    if (exclude && exclude.length) {
      exclude.forEach((item) => {
        const formGroup = this.buildConsumerBehaviourForm(item);
        array.push(formGroup);
      });
    }
    return array;
  }

  private buildFromToForm(item?: IFromTo): FormGroup {
    const config = {
      id: [item ? item.id : uuidv4()],
      [this.createObjectGroupFormFields['activated'].name]: [
        item ? item.activated : false,
      ],
      [this.createObjectGroupFormFields['from'].name]: [item ? item.from : ''],
      [this.createObjectGroupFormFields['to'].name]: [item ? item.to : ''],
      [this.createObjectGroupFormFields['unit'].name]: [item ? item.unit : ''],
      ['objectType']: [item ? item.objectType : ''],
    };
    return this.formBuilder.group(config, {
      validators: this.compareFromAndTo,
    });
  }

  private compareFromAndTo(control: AbstractControl): ValidationErrors | null {
    const activated = control.get('activated').value;
    const from = control.get('from').value ? control.get('from').value : 0;
    const to = control.get('to').value ? control.get('to').value : 0;
    const unit = control.get('unit').value ? control.get('unit').value : 0;
    const objectType = control.get('objectType').value
      ? control.get('objectType').value
      : '';

    if (activated && !to) {
      return { missingValue: true };
    }

    if (activated && typeof from === 'number' && typeof to === 'number') {
      if (from && to && from > to) {
        return { noMatch: true };
      }

      if (!to && from > to) {
        return { noMatch: true };
      }

      if (objectType === EObjectType.purchaseFrequency && !unit) {
        return { missingUnit: true };
      }

      return null;
    }

    return null;
  }
}
