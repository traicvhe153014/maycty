import { IFormFieldErrors, IFormFields } from '@models';
import { redirectLinkPattern } from '@shared/sharing/constants';

export const ObjectGroupFormFields = {
  groupName: {
    name: 'groupName',
    validationParams: {
      maxLength: 150,
      required: true,
    },
  },
  applyAllProducts: {
    name: 'applyAllProducts',
  },
  productType: {
    name: 'productType',
  },
  productName: {
    name: 'productName',
    validationParams: {
      required: true,
    },
  },
  activatedCopy: {
    name: 'activatedCopy',
  },
  copyObject: {
    name: 'copyObject',
    validationParams: {
      required: true,
    },
  },
  visitedWebsite: {
    name: 'visitedWebsite',
    validationParams: {
      required: true,
    },
  },
  consumerBehaviour: {
    name: 'consumerBehaviour',
    validationParams: {
      required: true,
    },
  },
  behavior: {
    name: 'behaviorType',
    validationParams: {
      required: true,
    },
  },
  orderPrice: {
    name: 'orderPrice',
    validationParams: {
      required: true,
    },
  },
  purchaseFrequency: {
    name: 'purchaseFrequency',
    validationParams: {
      required: true,
    },
  },
  viewedNotPurchased: {
    name: 'viewedNotPurchased',
    validationParams: {
      required: true,
    },
  },
  viewedNotAddToCart: {
    name: 'viewedNotAddToCart',
    validationParams: {
      required: true,
    },
  },
  addedNotPurchased: {
    name: 'addedNotPurchased',
    validationParams: {
      required: true,
    },
  },
  removedFromCart: {
    name: 'removedFromCart',
    validationParams: {
      required: true,
    },
  },
  purchasedWithin: {
    name: 'purchasedWithin',
    validationParams: {
      required: true,
    },
  },
  selectedNotCompleted: {
    name: 'selectedNotCompleted',
    validationParams: {
      required: true,
    },
  },
  url: {
    name: 'value',
    validationParams: {
      required: true,
      pattern: redirectLinkPattern,
    },
  },
  conditionType: {
    name: 'conditionType',
    validationParams: {
      required: true,
    },
  },
  activated: {
    name: 'activated',
    validationParams: {
      required: true,
    },
  },
  from: {
    name: 'from',
    validationParams: {
      required: true,
    },
  },
  to: {
    name: 'to',
    validationParams: {
      required: true,
    },
  },
  unit: {
    name: 'unit',
    validationParams: {
      required: true,
    },
  },
  exclude: {
    name: 'exclude',
    validationParams: {
      required: true,
    },
  },
} as IFormFields;

export const objectGroupFormFieldErrors: IFormFieldErrors = {
  groupName: [
    {
      type: 'required',
      message: 'Không để trống tên nhóm đối tượng.',
    },
    {
      type: 'maxlength',
      message: 'Tên nhóm đối tượng không được quá 150 ký tự.',
    },
  ],
  productName: [
    {
      type: 'required',
      message: 'Tên sản phẩm không được để trống',
    },
  ],
};
