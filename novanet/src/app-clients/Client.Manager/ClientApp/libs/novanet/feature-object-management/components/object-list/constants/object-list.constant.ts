import { DataTableSettingModel, ISettingColumnTable } from '@data-table/models';
import { EObjectList } from '../enums';

export const SettingColumnObjectGroupTable = [
  {
    id: EObjectList.INDEX,
    title: 'STT',
    width: '40px',
    align: 'center',
  },
  {
    id: EObjectList.OBJECT_GROUP_NAME,
    width: '700px',
    name: 'Name',
    title: 'Tên phân khúc đối tượng',
    sort: true,
  },
  {
    id: EObjectList.STATUS,
    width: '200px',
    name: 'ConnectionStatus',
    title: 'Trạng thái',
    sort: true,
  },
  {
    id: EObjectList.MODIFIED,
    title: 'Cập nhật',
    name: 'ModifiedAt',
    sort: true,
    descending: true,
    ascending: false,
  },
] as ISettingColumnTable<any, any>[];

export const ObjectListSettingConstant = {
  id: 'object-list',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;
