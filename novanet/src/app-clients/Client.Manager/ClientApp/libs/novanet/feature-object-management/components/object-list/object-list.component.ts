import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import {
  DataTableSettingModel,
  IFieldDetail,
  ISettingColumnTable,
} from '@data-table/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { takeUntil } from 'rxjs/operators';
import { cloneDeep } from 'lodash';
import { IHttpGetRequest } from '@core/models';
import {
  ClearObjectGroups,
  GetObjectGroups,
  ObjectManagementState,
  UpdateObjectGroup,
} from '@features/feature-object-management/store';
import { IObjectGroupsResponse } from '@features/feature-object-management/models';
import {
  ObjectListSettingConstant,
  SettingColumnObjectGroupTable,
} from './constants/object-list.constant';
import { EObjectList } from './enums';
import { ESortType } from '@core/enums';
import { IdentityState, IUserResponse } from '@shared/identity';
import { environment } from '@environment';

@Component({
  selector: 'novanet-object-list',
  templateUrl: './object-list.component.html',
})
export class ObjectListComponent {
  @Select(ObjectManagementState.getObjectGroups)
  public $objectGroups: Observable<IObjectGroupsResponse[]>;

  @Select(IdentityState.getCurrentUser) public currentUser$: Observable<
    IUserResponse | undefined
  >;

  @ViewChild('remarketingCode') public remarketingCode: ElementRef;

  public listOfData: IObjectGroupsResponse[] = [];
  public settings: DataTableSettingModel;
  public searchForm: FormGroup;
  public changeObjectGroupForm: FormGroup;
  public scrollX: string | null = null;
  public scrollY: string | null = null;
  public editEnabled = false;
  public pageSize = 50;

  public readonly columns: ISettingColumnTable<any, any>[] =
    SettingColumnObjectGroupTable;

  public readonly objectGroupsEnum = EObjectList;
  public readonly globalNotificationEnum = GlobalNotificationEnum;
  public userId: number | undefined;
  private skip = 0;
  private page = 1;
  private destroy$: Subject<void> = new Subject<void>();
  private dataLoaded = false;
  private sorts = ['-modifiedAt'];
  private baseSorts = ['-modifiedAt'];

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public get remarketingScript() {
    return `<script src="${environment.sdkUrl}"></script>\n<script>\n\tNovanet.push('config', ${this.userId});\n</script>`;
  }

  ngOnInit(): void {
    this.buildSearchForm();
    this.getObjectGroups();
    const params = {
      page: this.page,
      skip: 0,
      pageSize: this.pageSize,
      sorts: this.baseSorts,
    } as IHttpGetRequest;
    this.store.dispatch(new GetObjectGroups(params));
    this.settings = ObjectListSettingConstant;

    this.currentUser$.pipe(takeUntil(this.destroy$)).subscribe({
      next: (user) => {
        this.userId = user?.subId;
      },
    });
  }

  public ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearObjectGroups());
  }

  public onSearch(values?: IFieldDetail[]) {
    this.changeDetectorRef.detach();

    if (values?.length) {
      this.dataLoaded = false;
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }

    this.listOfData = [];
    this.dataLoaded = false;
    this.page = 1;
    const params = {
      page: 1,
      skip: 0,
      pageSize: this.pageSize,
      keyWord: this.searchForm.value.search,
      sorts: this.sorts,
    } as IHttpGetRequest;

    this.store.dispatch(new GetObjectGroups(params));
  }

  public onToggleEdit(data): void {
    if (this.editEnabled && !data.enableChangeValue) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Hãy lưu dữ liệu trước khi sửa nhóm sản phẩm khác'
        )
      );
      return;
    }
    data.enableChangeValue = !data.enableChangeValue;
    this.editEnabled = !this.editEnabled;
    if (data.enableChangeValue) {
      this.buildObjectGroupForm(data);
    } else {
      this.changeObjectGroupForm.reset();
    }
    this.changeDetectorRef.detectChanges();
  }

  public onSubmitObjectProductGroup(data) {
    this.store.dispatch(
      new UpdateObjectGroup(cloneDeep(this.changeObjectGroupForm.value))
    );
    data.enableChangeValue = !data.enableChangeValue;
    data.name = this.changeObjectGroupForm.value.name;
    this.editEnabled = !this.editEnabled;
  }

  public refreshStatus(_event: { id: string; checked: boolean }) {
    this.changeDetectorRef.detectChanges();
  }

  public nextBatch($event?: boolean) {
    if ($event && !this.dataLoaded) {
      this.page += 1;
      this.skip = this.skip + this.pageSize;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        keyWord: this.searchForm.value.search,
      } as IHttpGetRequest;
      this.store.dispatch(new GetObjectGroups(params));
    }
  }

  public copyRemarketingCode() {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(this.remarketingCode.nativeElement);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Đã copy vào clipboard'
      )
    );
  }

  private getObjectGroups() {
    this.$objectGroups.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      if (this.dataLoaded || !response || !response?.length) {
        if (this.page === 1 && !response?.length) {
          this.listOfData = [];
        }
        this.changeDetectorRef.detectChanges();
        return;
      }
      const data = cloneDeep(response);
      this.listOfData = this.listOfData.concat(data);
      if (response?.length < this.pageSize) {
        this.dataLoaded = true;
      }
      this.changeDetectorRef.reattach();
      this.changeDetectorRef.detectChanges();
    });
  }

  private buildObjectGroupForm(data) {
    const config = {
      ['id']: [data.id, [Validators.required]],
      ['name']: [data.name, [Validators.required]],
    };
    this.changeObjectGroupForm = this.formBuilder.group(config);
  }

  private buildSearchForm() {
    const config = {
      ['search']: [''],
    };
    this.searchForm = this.formBuilder.group(config);
  }
}
