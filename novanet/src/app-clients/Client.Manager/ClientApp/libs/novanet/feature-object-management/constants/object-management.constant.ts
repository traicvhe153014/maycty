import { IDropdownValues } from '@core/models';
import {
  EBehavior,
  EBehaviorProductType,
  EPurchaseFrequencyUnit,
} from '@features/feature-object-management/enums';

export const PurchaseFrequencyUnitDropdown = [
  {
    id: EPurchaseFrequencyUnit.month,
    label: 'Tháng',
  },
  {
    id: EPurchaseFrequencyUnit.year,
    label: 'Năm',
  },
] as IDropdownValues[];

export const BehaviorDropdown = [
  {
    id: EBehavior.ALL,
    label: 'Tất cả',
  },
  {
    id: EBehavior.BOUGHT,
    label: 'Đã mua hàng',
  },
  {
    id: EBehavior.VIEWED_PRODUCT,
    label: 'Xem sản phẩm',
  },
  {
    id: EBehavior.ADD_TO_CART,
    label: 'Thêm vào giỏ hàng',
  },
  {
    id: EBehavior.CLICKED_BUY_BUTTON,
    label: 'Nhấn nút mua hàng',
  },
] as IDropdownValues[];

export const ProductDropdown = [
  {
    id: EBehaviorProductType.Product,
    label: 'Sản phẩm',
  },
  {
    id: EBehaviorProductType.ProductGroup,
    label: 'Nhóm sản phẩm',
  },
] as IDropdownValues[];
