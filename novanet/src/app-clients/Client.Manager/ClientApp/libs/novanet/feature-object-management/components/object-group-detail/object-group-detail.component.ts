import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  BehaviorDropdown,
  objectGroupFormFieldErrors,
  ObjectGroupFormFields,
} from '@features/feature-object-management/constants';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { IFormFieldErrors, IFormFields } from '@models';
import {
  IConsumerBehaviour,
  IObjectGroupsResponse,
  IWebsiteCondition,
} from '@features/feature-object-management/models';
import {
  EBehaviorProductType,
  EConditionType,
  EMatchingType,
} from '@features/feature-object-management/enums';
import { cloneDeep } from 'lodash';
import { EObjectType } from './enums';
import { Select, Store } from '@ngxs/store';
import {
  ClearObjectGroups,
  CreateObjectGroup,
  GetObjectGroupById,
  GetObjectGroups,
  ObjectManagementState,
} from '@features/feature-object-management/store';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import {
  BehaviourConsumerComponent,
  VisitedWebsiteComponent,
} from '../object-group-detail/components';
import { IHttpGetRequest } from '@core/models';
import { takeUntil } from 'rxjs/operators';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { v4 as uuidv4 } from 'uuid';
import { scrollIntoViewBySelector } from '@core/utils';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'novanet-object-group-detail',
  templateUrl: './object-group-detail.component.html',
})
export class ObjectGroupDetailComponent implements OnInit, OnDestroy {
  @Select(ObjectManagementState.getObjectGroups)
  public $objectGroups: Observable<IObjectGroupsResponse[]>;

  @Select(ObjectManagementState.getLoading)
  public $loadingObjectGroupsStatus: Observable<boolean>;

  @ViewChild('visitedWebsite', { static: false })
  visitedWebsite: VisitedWebsiteComponent;

  @ViewChild('consumerBehavior', { static: false })
  consumerBehavior: BehaviourConsumerComponent;

  @Select(ObjectManagementState.getObjectGroup)
  public $objectGroup: Observable<IObjectGroupsResponse>;

  public behaviorValues = BehaviorDropdown;
  public objectGroupForm: FormGroup;
  public objectGroup: IObjectGroupsResponse;
  public currentId: string;
  public listOfData: IObjectGroupsResponse[] = [];
  public pageSize = 10;
  public readonly objectGroupFormFields: IFormFields = ObjectGroupFormFields;
  public readonly objectGroupFormFieldsError: IFormFieldErrors =
    objectGroupFormFieldErrors;
  public readonly globalNotificationEnum = GlobalNotificationEnum;

  private skip = 0;
  private page = 1;
  private destroy$: Subject<void> = new Subject<void>();
  private dataLoaded = false;
  private acceptClearCopy = true;
  private readonly objectType = EObjectType;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(DOCUMENT) private document: Document,
    private elementRef: ElementRef,
    private store: Store,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  get copyObject(): FormControl {
    return this.objectGroupForm.get('copyObject') as FormControl;
  }

  get visitedWebsiteForm(): FormControl {
    return this.objectGroupForm.get('visitedWebsite') as FormControl;
  }

  get consumerBehaviour(): FormControl {
    return this.objectGroupForm.get('consumerBehaviour') as FormControl;
  }

  ngOnInit() {
    this.buildObjectGroupForm();
    this.getObjectGroups();
    this.$objectGroup.pipe().subscribe((objectGroup) => {
      if (objectGroup) {
        this.objectGroup = objectGroup;
        this.buildUpdateObjectGroupForm(objectGroup);
      }
    });

    const params = {
      page: this.page,
      skip: 0,
      pageSize: this.pageSize,
      sorts: ['-modifiedAt'],
    } as IHttpGetRequest;
    this.store.dispatch(new GetObjectGroups(params));

    this.objectGroupForm
      .get('applyAllProducts')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: () => {
          this.resetCopyObjectDropdown();
        },
      });
    this.objectGroupForm
      .get('productType')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: () => {
          this.resetCopyObjectDropdown();
        },
      });
    this.objectGroupForm
      .get('productName')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe({
        next: () => {
          this.resetCopyObjectDropdown();
        },
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearObjectGroups());
  }

  public onPatchBehaviorConsumer(data: IConsumerBehaviour) {
    this.objectGroupForm.get('consumerBehaviour').patchValue(data);
    this.resetCopyObjectDropdown();
  }

  public onPatchVisitedWebsite(data: IWebsiteCondition[][]) {
    this.objectGroupForm.get('visitedWebsite').patchValue(data);
    this.resetCopyObjectDropdown();
  }

  public onCreateObjectGroup() {
    const object = this.objectGroupForm.value;

    if (this.objectGroupForm.invalid) {
      this.objectGroupForm.markAllAsTouched();
      const errorMessage = this.objectGroupForm
        .get('groupName')
        .getError('required')
        ? 'Vui lòng điền tên nhóm đối tượng'
        : 'Tên nhóm đối tượng không được quá 150 ký tự';
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          errorMessage
        )
      );
      setTimeout(() => {
        scrollIntoViewBySelector('.error-message');
      }, 200);
      return;
    }

    if (this.visitedWebsite.visitedWebsiteFormArray.invalid) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Đường dẫn không đúng định dạng, vui lòng kiểm tra lại'
        )
      );
      this.visitedWebsite.visitedWebsiteFormArray.markAllAsTouched();
      setTimeout(() => {
        scrollIntoViewBySelector('.error-message');
      }, 200);
      return;
    }

    const duplicatedWebsites =
      this.visitedWebsite.visitedWebsiteFormArray.value.find((website) => {
        const seen = new Set();
        return website.some(function (currentObject) {
          return seen.size === seen.add(currentObject.value).size;
        });
      });
    if (duplicatedWebsites) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Link trang web không được trùng nhau'
        )
      );
      this.visitedWebsite.visitedWebsiteFormArray.markAllAsTouched();
      return;
    }

    if (this.consumerBehavior.behaviorFormArray.invalid) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Vui lòng kiểm tra lại điều kiện của hành vi khách hàng'
        )
      );
      this.visitedWebsite.visitedWebsiteFormArray.markAllAsTouched();
      setTimeout(() => {
        scrollIntoViewBySelector('.error-message');
      }, 200);
      return;
    }

    const duplicatedExcludes =
      this.consumerBehavior.behaviorFormArray.value.find((behavior) =>
        behavior.exclude?.find(
          (exclude) => exclude.behaviorType === behavior.behaviorType
        )
      );
    if (duplicatedExcludes) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Đối tượng loại trừ không trùng với đối tượng đã chọn'
        )
      );
      this.consumerBehavior.behaviorFormArray.markAllAsTouched();
      return;
    }

    if (
      !this.objectGroupForm.get('applyAllProducts').value &&
      this.objectGroupForm.get('productName').value === ''
    ) {
      this.objectGroupForm.get('productName').setErrors({
        required: true,
      });
      this.objectGroupForm.markAllAsTouched();
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Vui lòng điền tên sản phẩm'
        )
      );
      setTimeout(() => {
        scrollIntoViewBySelector('.error-message');
      }, 200);
      return;
    }

    const visitedWebsite = [] as IWebsiteCondition[];
    const consumerBehaviorsOrder = [] as IConsumerBehaviour[];
    const consumerBehaviors = [] as IConsumerBehaviour[];

    const objectTypes = this.returnEnumsString(this.objectType);

    object.visitedWebsite?.forEach((websiteArray) => {
      websiteArray.forEach((item, index) => {
        if (item.value) {
          item.matchingType = EMatchingType.Url;
          if (!index) {
            item.beforeConditionType = EConditionType.Or;
            item.afterConditionType = EConditionType.And;
          }
          if (index === websiteArray.length - 1) {
            item.beforeConditionType = EConditionType.And;
            item.afterConditionType = EConditionType.Or;
          }
          if (index && index < websiteArray.length - 1) {
            item.beforeConditionType = EConditionType.And;
            item.afterConditionType = EConditionType.And;
          }
          visitedWebsite.push(item);
        }
      });
    });

    object.consumerBehaviour.forEach((behaviour) => {
      behaviour.behaviorGroupId = uuidv4();
      const cloneBehaviorItem = cloneDeep(behaviour);
      delete cloneBehaviorItem.exclude;

      consumerBehaviorsOrder.push(cloneBehaviorItem);
      behaviour.exclude.forEach((excludeItem) => {
        excludeItem.behaviorGroupId = uuidv4();
        excludeItem.excludeId = behaviour.behaviorGroupId;
        const cloneExcludeItem = cloneDeep(excludeItem);

        consumerBehaviorsOrder.push(excludeItem);
        delete cloneExcludeItem.exclude;
      });
    });

    consumerBehaviorsOrder.forEach((item, index) => {
      item.beforeBehaviorId = index
        ? consumerBehaviors[index - 1].behaviorGroupId
        : null;
      item.afterBehaviorId = consumerBehaviorsOrder[index + 1]?.behaviorGroupId;
      const objectValues = this.getObjectTypes(objectTypes, item);
      objectValues.forEach((value) => {
        consumerBehaviors.push(value);
      });
    });

    visitedWebsite.forEach((item, index) => {
      item.beforeConditionId = index ? visitedWebsite[index - 1].id : null;
      item.afterConditionId = visitedWebsite[index + 1]?.id;
    });

    const objectGroupItem = {
      groupName: object.groupName,
      copyObject: object.copyObject,
      websiteConditions: visitedWebsite,
      consumerBehaviors,
      productType: object.productType,
      productName: object.productName,
      applyAllProducts: object.applyAllProducts,
    };

    this.store.dispatch(new CreateObjectGroup(objectGroupItem));
  }

  public nextBatch($event?: boolean) {
    if ($event && !this.dataLoaded) {
      this.page += 1;
      this.skip = this.skip + this.pageSize;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        sorts: ['-modifiedAt'],
      } as IHttpGetRequest;
      this.store.dispatch(new GetObjectGroups(params));
    }
  }

  public clearCopyObject($event: string) {
    this.acceptClearCopy = false;
    if (!$event) {
      this.objectGroupForm.patchValue({
        visitedWebsite: [],
        consumerBehaviour: [],
      });
      this.visitedWebsite.visitedWebsiteFormArray.clear();
      this.consumerBehavior.behaviorFormArray.clear();

      this.visitedWebsite.pushItemToArray();
      this.consumerBehavior.pushItemToArray();
      this.changeDetectorRef.detectChanges();
    }
    setTimeout(() => {
      this.acceptClearCopy = true;
    }, 1500);
  }

  private getObjectGroups() {
    this.$objectGroups.pipe(takeUntil(this.destroy$)).subscribe((response) => {
      if (this.dataLoaded || !response || !response.length) {
        return;
      }
      const data = cloneDeep(response);
      this.listOfData = this.listOfData.concat(data);
      if (response.length < this.pageSize) {
        this.dataLoaded = true;
      }
      this.changeDetectorRef.detectChanges();
    });
  }

  private getObjectTypes(
    objectTypes: string[],
    behaviorItem: IConsumerBehaviour
  ): IConsumerBehaviour[] {
    const consumerBehaviors: IConsumerBehaviour[] = [];
    const cloneBehaviorItem = cloneDeep(behaviorItem);
    objectTypes.forEach((key, index) => {
      const cloneBehaviorItem = cloneDeep(behaviorItem);

      if (cloneBehaviorItem[key]?.activated) {
        cloneBehaviorItem.objectType = index + 1;
        cloneBehaviorItem.from = cloneBehaviorItem[key].from;
        cloneBehaviorItem.to = cloneBehaviorItem[key].to;
        cloneBehaviorItem.unit = cloneBehaviorItem[key].unit
          ? cloneBehaviorItem[key].unit
          : null;
        objectTypes.forEach((objectKey) => {
          delete cloneBehaviorItem[objectKey];
        });
        consumerBehaviors.push(cloneBehaviorItem);
      }
    });
    if (!consumerBehaviors.length) {
      cloneBehaviorItem.objectType = EObjectType.all;
      consumerBehaviors.push(cloneBehaviorItem);
    }
    return consumerBehaviors;
  }

  private returnEnumsString(myEnum) {
    return Object.keys(myEnum)
      .map((key) => myEnum[key])
      .filter((value) => typeof value === 'string') as string[];
  }

  private buildObjectGroupForm() {
    const config = {
      [this.objectGroupFormFields['groupName'].name]: [
        '',
        [
          this.objectGroupFormFields['groupName'].validationParams.required
            ? Validators.required
            : null,
          Validators.maxLength(
            this.objectGroupFormFields['groupName'].validationParams
              .maxLength as number
          ),
        ],
      ],
      [this.objectGroupFormFields['copyObject'].name]: [''],
      [this.objectGroupFormFields['visitedWebsite'].name]: [],
      [this.objectGroupFormFields['consumerBehaviour'].name]: [],
      [this.objectGroupFormFields['applyAllProducts'].name]: [true],
      [this.objectGroupFormFields['productType'].name]: [
        EBehaviorProductType.Product,
      ],
      [this.objectGroupFormFields['productName'].name]: [''],
    };

    this.objectGroupForm = this.formBuilder.group(config);
    this.copyObject.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((item: string) => {
        if (item) {
          this.currentId = item;
          this.objectGroupForm.patchValue({
            visitedWebsite: null,
            consumerBehaviour: null,
          });
          this.store.dispatch(new GetObjectGroupById(item));
        }
      });
  }

  private buildUpdateObjectGroupForm(objectGroup: IObjectGroupsResponse) {
    this.visitedWebsite.patchData(objectGroup.websiteConditionsCore);
    this.consumerBehavior.patchData(objectGroup.consumerBehaviorsCore);
    this.objectGroupForm.patchValue({
      applyAllProducts: objectGroup.applyAllProducts ?? false,
      productType: objectGroup.productType ?? 0,
      productName: objectGroup.productName ?? '',
    });
    this.changeDetectorRef.detectChanges();
  }

  private resetCopyObjectDropdown() {
    if (this.acceptClearCopy) {
      this.copyObject.reset();
    }
  }
}
