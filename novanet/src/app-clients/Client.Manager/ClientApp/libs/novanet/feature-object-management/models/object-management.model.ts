import {
  EBehavior,
  EConditionType,
  EMatchingType,
  EPurchaseFrequencyUnit,
} from '@features/feature-object-management/enums';
import { EObjectType } from '../components/object-group-detail/enums';

export interface IObjectManagement {
  groupName: string;
  copyObject: string;
  visitedWebsite: IWebsiteCondition[];
  consumerBehaviour: IConsumerBehaviour[];
}

export interface IConsumerBehaviour {
  id: string;
  behaviorGroupId: string;
  behaviorType: EBehavior;
  beforeBehaviorId: string;
  afterBehaviorId: string;
  orderPrice: IFromTo;
  purchaseFrequency: IFromTo;
  viewedNotPurchased: IFromTo;
  viewedNotAddToCart: IFromTo;
  addedNotPurchased: IFromTo;
  removedFromCart: IFromTo;
  purchasedWithin: IFromTo;
  selectedNotCompleted: IFromTo;
  exclude?: IConsumerBehaviour[];
  excludeId?: string;
  objectType?: EObjectType;
  from?: number;
  to?: number;
  unit?: EPurchaseFrequencyUnit;
  applyAllProducts?: boolean;
  productType?: number;
  productName?: string;
}

export interface IWebsiteCondition {
  id: string;
  value: string;
  conditionType: number;
  beforeConditionType: EConditionType;
  afterConditionType: EConditionType;
  beforeConditionId: string;
  afterConditionId: string;
  matchingType: EMatchingType;
}

export interface IFromTo {
  id: string;
  objectType?: EObjectType;
  activated: boolean;
  from: number;
  to: number;
  unit: EPurchaseFrequencyUnit;
}

export interface IObjectGroupsResponse {
  id: string;
  name: string;
  connectionStatus?: boolean;
  subId?: number;
  modifiedAt?: string;
  websiteConditionsCore?: IWebsiteCondition[];
  consumerBehaviorsCore?: IConsumerBehaviour[];
  applyAllProducts?: boolean;
  productType?: number;
  productName?: string;
}
