import { IFormFieldErrors } from '@models';

export const BehaviourConsumerErrors: IFormFieldErrors = {
  from: [
    {
      type: 'noMatch',
      message: 'Giá trị không hợp lệ',
    },
  ],
};
