import { Injectable } from '@angular/core';
import { FormGroup, ValidatorFn } from '@angular/forms';
import { EUrlCondition } from '@features/feature-object-management/components/object-group-detail/components/visited-website/enums';
import { urlPattern } from '@shared/sharing/constants';
import { ObjectGroupFormFields } from '@features/feature-object-management/constants';

@Injectable({ providedIn: 'root' })
export class VisitedWebsiteValidator {
  public isValidUrl(): ValidatorFn {
    return (formGroup: FormGroup) => {
      const typeControl = formGroup.get(
        ObjectGroupFormFields.conditionType.name
      );
      if (typeControl.value !== EUrlCondition.EQUAL) {
        return null;
      }
      const nameControl = formGroup.get(ObjectGroupFormFields.url.name);
      if (!nameControl.value) {
        return null;
      }
      if (!urlPattern.test(nameControl.value)) {
        nameControl.setErrors({ pattern: true });
        return { pattern: true };
      }
      return null;
    };
  }
}
