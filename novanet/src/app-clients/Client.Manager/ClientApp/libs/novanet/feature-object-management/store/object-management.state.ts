import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { ObjectService } from './object-management-state.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import {
  ClearObjectGroups,
  CreateObjectGroup,
  CreateObjectGroupFail,
  CreateObjectGroupSuccess,
  GetMoreObjectGroups,
  GetMoreObjectGroupsFail,
  GetMoreObjectGroupsSuccess,
  GetObjectGroupById,
  GetObjectGroupByIdFail,
  GetObjectGroupByIdSuccess,
  GetObjectGroups,
  GetObjectGroupsFail,
  GetObjectGroupsSuccess,
  UpdateObjectGroup,
  UpdateObjectGroupFail,
  UpdateObjectGroupSuccess,
} from './object-management-state.actions';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { IObjectManagementState } from './object-management-state.model';
import { IObjectGroupsResponse } from '../models';
import { scrollIntoViewBySelector } from '@core/utils';
import { Observable } from 'rxjs';

@Injectable()
@State<IObjectManagementState>({
  name: 'objectGroup',
})
export class ObjectManagementState implements NgxsOnInit {
  constructor(
    private readonly objectService: ObjectService,
    private readonly router: Router
  ) {}

  @Selector()
  public static getObjectGroups(
    state: IObjectManagementState
  ): IObjectGroupsResponse[] {
    return state.objectGroups;
  }

  @Selector()
  public static getObjectGroup(
    state: IObjectManagementState
  ): IObjectGroupsResponse {
    return state.currentObjectGroup;
  }

  @Selector()
  public static getLoading(state: IObjectManagementState): boolean {
    return state.loading;
  }

  ngxsOnInit(ctx?: StateContext<any>): any {}

  @Action(UpdateObjectGroup)
  public updateObjectGroup(
    ctx: StateContext<IObjectManagementState>,
    { payload }: CreateObjectGroup
  ) {
    return this.objectService.updateObjectGroup(payload).pipe(
      map((response) => ctx.dispatch(new UpdateObjectGroupSuccess())),
      catchError((error) => ctx.dispatch(new UpdateObjectGroupFail()))
    );
  }

  @Action(CreateObjectGroup)
  public createObjectGroup(
    ctx: StateContext<IObjectManagementState>,
    { payload }: CreateObjectGroup
  ) {
    return this.objectService.createObjectGroup(payload).pipe(
      map((response) => ctx.dispatch(new CreateObjectGroupSuccess())),
      catchError((error) => ctx.dispatch(new CreateObjectGroupFail()))
    );
  }

  @Action(CreateObjectGroupSuccess)
  public createObjectGroupSuccess(
    ctx: StateContext<IObjectManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Tạo thành công nhóm đối tượng'
      )
    );
    this.router.navigate(['/object/list']);
  }

  @Action(CreateObjectGroupFail)
  public createObjectGroupFail(
    ctx: StateContext<IObjectManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.warning,
        'Vui lòng điền đầy đủ hành vi khách hàng.'
      )
    );
    scrollIntoViewBySelector('.consumer-behaviour');
  }

  @Action(UpdateObjectGroupSuccess)
  public updateObjectGroupSuccess(
    ctx: StateContext<IObjectManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Sửa thành công nhóm đối tượng'
      )
    );
  }

  @Action(UpdateObjectGroupFail)
  public updateObjectGroupFail(
    ctx: StateContext<IObjectManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.warning,
        'Sửa thật bại.'
      )
    );
  }

  @Action(GetObjectGroups)
  public getObjectGroups(
    ctx: StateContext<IObjectManagementState>,
    { request }: GetObjectGroups
  ) {
    ctx.patchState({
      loading: true,
    });
    return this.objectService.getObjectGroups(request).pipe(
      map((response) =>
        ctx.dispatch(new GetObjectGroupsSuccess(response, request))
      ),
      catchError((error) => ctx.dispatch(new GetObjectGroupsFail()))
    );
  }

  @Action(GetObjectGroupsSuccess)
  public getObjectGroupsSuccess(
    ctx: StateContext<IObjectManagementState>,
    { response, request }: GetObjectGroupsSuccess
  ): void {
    const hasMorePages = response.length === request.pageSize;
    ctx.patchState({
      objectGroups: response,
      loading: false,
      hasMorePages,
    });
  }

  @Action(GetObjectGroupsFail)
  public getObjectGroupsFail(ctx: StateContext<IObjectManagementState>): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.error,
        'Lỗi tải dữ liệu nhóm đối tượng'
      )
    );
  }

  @Action(GetMoreObjectGroups)
  public getMoreObjectGroups(
    ctx: StateContext<IObjectManagementState>,
    { request }: GetMoreObjectGroups
  ) {
    ctx.patchState({
      loading: true,
    });
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    return this.objectService.getObjectGroups(request).pipe(
      map((response) =>
        ctx.dispatch(new GetMoreObjectGroupsSuccess(response, request))
      ),
      catchError((error) => ctx.dispatch(new GetMoreObjectGroupsFail()))
    );
  }

  @Action(GetMoreObjectGroupsSuccess)
  public getMoreObjectGroupsSuccess(
    ctx: StateContext<IObjectManagementState>,
    { response, request }: GetMoreObjectGroupsSuccess
  ): void {
    const hasMorePages = response.length === request.pageSize;
    const currentObjectGroups = ctx.getState().objectGroups;
    ctx.patchState({
      objectGroups: [...currentObjectGroups, ...response],
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreObjectGroupsFail)
  public getMoreObjectGroupsFail(
    ctx: StateContext<IObjectManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.error,
        'Lỗi tải dữ liệu nhóm đối tượng'
      )
    );
  }

  @Action(GetObjectGroupById)
  public getObjectGroupById(
    ctx: StateContext<IObjectManagementState>,
    { groupId }: GetObjectGroupById
  ) {
    ctx.patchState({
      loading: true,
    });
    return this.objectService.getObjectGroupById(groupId).pipe(
      map((response) => ctx.dispatch(new GetObjectGroupByIdSuccess(response))),
      catchError((error) => ctx.dispatch(new GetObjectGroupByIdFail()))
    );
  }

  @Action(GetObjectGroupByIdSuccess)
  public getObjectGroupByIdSuccess(
    ctx: StateContext<IObjectManagementState>,
    { response }: GetObjectGroupByIdSuccess
  ): void {
    ctx.patchState({
      currentObjectGroup: response,
      loading: false,
    });
  }

  @Action(GetObjectGroupByIdFail)
  public getObjectGroupByIdFail(
    ctx: StateContext<IObjectManagementState>
  ): void {
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.error,
        'Lỗi tải dữ liệu nhóm đối tượng'
      )
    );
  }

  @Action(ClearObjectGroups)
  public clearObjectGroups(ctx: StateContext<IObjectManagementState>): void {
    ctx.patchState({
      objectGroups: null,
      loading: false,
    });
  }
}
