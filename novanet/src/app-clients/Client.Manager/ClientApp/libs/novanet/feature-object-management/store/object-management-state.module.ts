import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { ObjectManagementState } from './object-management.state';
import { ObjectService } from './object-management-state.service';

@NgModule({
  imports: [NgxsModule.forFeature([ObjectManagementState])],
  providers: [ObjectService],
})
export class ObjectManagementStateModule {}
