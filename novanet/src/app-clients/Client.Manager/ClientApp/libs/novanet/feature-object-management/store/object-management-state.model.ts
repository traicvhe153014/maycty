import { IObjectGroupsResponse } from '../models';

export interface IObjectManagementState {
  objectGroups: IObjectGroupsResponse[];
  currentObjectGroup: IObjectGroupsResponse;
  page: number;
  loading: boolean;
  hasMorePages: boolean;
}
