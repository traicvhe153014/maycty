import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
  },
  {
    path: 'object-group-detail',
    loadChildren: () =>
      import(
        '@features/feature-object-management/components/object-group-detail'
      ).then((m) => m.ObjectGroupDetailModule),
  },
  {
    path: 'list',
    loadChildren: () =>
      import('@features/feature-object-management/components/object-list').then(
        (m) => m.ObjectListModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ObjectManagementRouting {}
