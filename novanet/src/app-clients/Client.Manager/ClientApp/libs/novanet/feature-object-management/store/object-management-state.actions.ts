import { IHttpGetRequest } from '@core/models';
import { IObjectGroupsResponse } from '@features/feature-object-management/models';

const enum ObjectActions {
  GetObjectGroups = '[Object] GetObjectGroups',
  GetObjectGroupsSuccess = '[Object] GetObjectGroupsSuccess',
  GetObjectGroupsFail = '[Object] GetObjectGroupsFail',
  GetMoreObjectGroups = '[Object] GetMoreObjectGroups',
  GetMoreObjectGroupsSuccess = '[Object] GetMoreObjectGroupsSuccess',
  GetMoreObjectGroupsFail = '[Object] GetMoreObjectGroupsFail',
  GetObjectGroupById = '[Object] GetObjectGroupById',
  GetObjectGroupByIdSuccess = '[Object] GetObjectGroupByIdSuccess',
  GetObjectGroupByIdFail = '[Object] GetObjectGroupByIdFail',
  CreateObjectGroup = '[Object] CreateObjectGroup',
  UpdateObjectGroup = '[Object] UpdateObjectGroup',
  UpdateObjectGroupSuccess = '[Object] UpdateObjectGroupSuccess',
  UpdateObjectGroupFail = '[Object] UpdateObjectGroupFail',
  CreateObjectGroupSuccess = '[Object] CreateObjectGroupSuccess',
  CreateObjectGroupFail = '[Object] CreateObjectGroupFail',
  ClearObjectGroups = '[Object] ClearObjectGroups',
}

export class CreateObjectGroup {
  static type = ObjectActions.CreateObjectGroup;

  constructor(public payload: any) {}
}

export class CreateObjectGroupSuccess {
  static type = ObjectActions.CreateObjectGroupSuccess;
}

export class CreateObjectGroupFail {
  static type = ObjectActions.CreateObjectGroupFail;
}

export class GetObjectGroupById {
  static type = ObjectActions.GetObjectGroupById;

  constructor(public groupId: string) {}
}

export class GetObjectGroupByIdSuccess {
  static type = ObjectActions.GetObjectGroupByIdSuccess;

  constructor(public response: IObjectGroupsResponse) {}
}

export class GetObjectGroupByIdFail {
  static type = ObjectActions.GetObjectGroupByIdFail;
}

export class GetObjectGroups {
  static type = ObjectActions.GetObjectGroups;

  constructor(public request: IHttpGetRequest) {}
}

export class GetObjectGroupsSuccess {
  static type = ObjectActions.GetObjectGroupsSuccess;

  constructor(
    public response: IObjectGroupsResponse[],
    public request: IHttpGetRequest
  ) {}
}

export class GetObjectGroupsFail {
  static type = ObjectActions.GetObjectGroupsFail;
}

export class GetMoreObjectGroups {
  static type = ObjectActions.GetMoreObjectGroups;

  constructor(public request: IHttpGetRequest) {}
}

export class GetMoreObjectGroupsSuccess {
  static type = ObjectActions.GetMoreObjectGroupsSuccess;

  constructor(
    public response: IObjectGroupsResponse[],
    public request: IHttpGetRequest
  ) {}
}

export class GetMoreObjectGroupsFail {
  static type = ObjectActions.GetMoreObjectGroupsFail;
}

export class ClearObjectGroups {
  static type = ObjectActions.ClearObjectGroups;
}

export class UpdateObjectGroup {
  static type = ObjectActions.UpdateObjectGroup;

  constructor(public payload: any) {}
}

export class UpdateObjectGroupSuccess {
  static type = ObjectActions.UpdateObjectGroupSuccess;
}

export class UpdateObjectGroupFail {
  static type = ObjectActions.UpdateObjectGroupFail;
}
