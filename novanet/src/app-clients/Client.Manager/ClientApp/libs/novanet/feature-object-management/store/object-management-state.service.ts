import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService, baseUrl } from '@shared/services';
import { IObjectGroupsResponse } from '../models';
import { IHttpGetRequest } from '@core/models';
import { ISuccessHttpResponse } from '@models';

const ObjectUrls = {
  create: `create`,
  update: `update`,
  list: `list`,
  getById: `get`,
};

@Injectable()
export class ObjectService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1/ObjectGroup');
  }

  public updateObjectGroup(data): Observable<IObjectGroupsResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IObjectGroupsResponse>>(
        this.createUrl(ObjectUrls.update),
        data
      )
      .pipe(map((response) => response.data));
  }

  public createObjectGroup(data): Observable<IObjectGroupsResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IObjectGroupsResponse>>(
        this.createUrl(ObjectUrls.create),
        data
      )
      .pipe(map((response) => response.data));
  }

  public getObjectGroups(
    data: IHttpGetRequest
  ): Observable<IObjectGroupsResponse[]> {
    const params = this.createParams(data);
    return this.httpClient
      .get<ISuccessHttpResponse<IObjectGroupsResponse[]>>(
        this.createUrl(ObjectUrls.list),
        { params }
      )
      .pipe(map((response) => response.data));
  }

  public getObjectGroupById(
    groupId: string
  ): Observable<IObjectGroupsResponse> {
    const params = this.createParams({ objectGroupId: groupId });
    return this.httpClient
      .get<ISuccessHttpResponse<IObjectGroupsResponse>>(
        this.createUrl(ObjectUrls.getById),
        { params }
      )
      .pipe(map((response) => response.data));
  }
}
