import { NgModule } from '@angular/core';
import { ObjectListComponent } from './object-list.component';
import { ObjectListRouting } from './object-list.routing';
import { CommonModule } from '@angular/common';
import { NovanetInputModule } from '@shared/custom-input';
import { ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from '@core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

const MODULES = [
  ObjectListRouting,
  CommonModule,
  NovanetInputModule,
  ReactiveFormsModule,
  DataTableModule,
  NzToolTipModule,
];

const COMPONENTS = [ObjectListComponent];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [...MODULES],
})
export class ObjectListModule {}
