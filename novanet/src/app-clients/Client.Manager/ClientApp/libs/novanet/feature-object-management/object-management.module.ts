import { NgModule } from '@angular/core';
import { ObjectManagementRouting } from './object-management.routing';
import { ObjectManagementStateModule } from './store';

const MODULES = [ObjectManagementRouting, ObjectManagementStateModule];

const COMPONENTS = [];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [...MODULES],
})
export class ObjectManagementModule {}
