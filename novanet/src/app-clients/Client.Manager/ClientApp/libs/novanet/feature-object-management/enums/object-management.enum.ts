export enum EBehavior {
  VIEWED_PRODUCT = 1,
  ADD_TO_CART,
  CLICKED_BUY_BUTTON,
  BOUGHT,
  ALL,
}

export enum EPurchaseFrequencyUnit {
  month = 1,
  year,
}

export enum EConditionType {
  Equal = 1,
  Contains = 2,
  And = 3,
  Or = 4,
}

export enum EMatchingType {
  Url = 1,
}

export enum EBehaviorProductType {
  Product = 1,
  ProductGroup = 2,
}
