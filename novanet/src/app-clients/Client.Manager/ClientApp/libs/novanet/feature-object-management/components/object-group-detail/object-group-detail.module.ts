import { NgModule } from '@angular/core';
import { ObjectGroupDetailComponent } from './object-group-detail.component';
import { ObjectGroupDetailRouting } from './object-group-detail.routing';
import { NovanetInputModule } from '@shared/custom-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  BehaviourConsumerComponent,
  VisitedWebsiteComponent,
} from './components';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { CommonModule } from '@angular/common';

const MODULES = [
  ObjectGroupDetailRouting,
  NovanetInputModule,
  ReactiveFormsModule,
  FormsModule,
  NzSelectModule,
  CommonModule,
];

const COMPONENTS = [
  ObjectGroupDetailComponent,
  VisitedWebsiteComponent,
  BehaviourConsumerComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [...MODULES],
})
export class ObjectGroupDetailModule {}
