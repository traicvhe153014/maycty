import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IFormFields } from '@models';
import { ObjectGroupFormFields } from '@features/feature-object-management/constants';
import {
  UrlConditionDropdown,
  VisitedWebsiteFormFieldErrors,
} from './constants';
import { EUrlCondition } from './enums';
import { IWebsiteCondition } from '@features/feature-object-management/models';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { cloneDeep } from 'lodash';
import { EConditionType } from '@features/feature-object-management/enums';
import { VisitedWebsiteValidator } from '@features/feature-object-management/components/object-group-detail/validators';

@Component({
  selector: 'novanet-visited-website',
  templateUrl: './visited-website.component.html',
  styleUrls: ['./visited-website.component.scss'],
})
export class VisitedWebsiteComponent implements OnInit, OnDestroy {
  @Input() public visitedWebsite: IWebsiteCondition[];

  @Output() public onVisitedWebsiteData = new EventEmitter<
    IWebsiteCondition[][]
  >();

  public readonly createObjectGroupFormFields: IFormFields =
    ObjectGroupFormFields;

  public visitedWebsiteFormArray: FormArray;
  public readonly urlConditionDropdown = UrlConditionDropdown;
  public readonly visitedWebsiteFormFieldErrors = VisitedWebsiteFormFieldErrors;

  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private visitedWebsiteValidator: VisitedWebsiteValidator
  ) {}

  public urls(index: number): FormArray {
    return this.visitedWebsiteFormArray.at(index) as FormArray;
  }

  public urlFormGroup(orIndex: number, inIndex: number): FormGroup {
    return this.urls(orIndex).at(inIndex) as FormGroup;
  }

  ngOnInit(): void {
    this.buildArray();
    this.pushItemToArray();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public hasDuplicateWebsites(orIndex: number, inIndex: number): boolean {
    const urlFormGroup = this.urlFormGroup(orIndex, inIndex);
    const currentUrl = urlFormGroup.get('value').value;

    if (!currentUrl) {
      return false;
    }

    const urlForms = this.urls(orIndex);
    for (let i = 0; i < urlForms.length; i++) {
      if (i !== inIndex && urlForms.at(i).get('value').value === currentUrl) {
        return true;
      }
    }
    return false;
  }

  public pushItemToArray(visitedWebsite?: IWebsiteCondition[][]) {
    if (visitedWebsite) {
      this.visitedWebsiteFormArray.removeAt(0);
      visitedWebsite.forEach((arrayCondition) => {
        const array = this.formBuilder.array([]);
        arrayCondition.forEach((item) => {
          const formGroup = this.buildForm(item);
          array.push(formGroup);
        });
        this.visitedWebsiteFormArray.push(array);
      });
      return;
    }
    const formGroup = this.buildForm();
    const array = this.formBuilder.array([]);
    array.push(formGroup);
    this.visitedWebsiteFormArray.push(array);
  }

  public pushUrlToUrlArray(orIndex: number) {
    const formGroup = this.buildForm();
    this.urls(orIndex).push(formGroup);
  }

  public removeUrlFromUrlArray(orIndex: number, inIndex: number) {
    this.urls(orIndex).removeAt(inIndex);
    if (!this.urls(orIndex).length) {
      this.visitedWebsiteFormArray.removeAt(orIndex);
    }
  }

  public onUrlConditionChange(
    event: EUrlCondition,
    orIndex: number,
    inIndex: number
  ) {
    const urlFormGroup = this.urlFormGroup(orIndex, inIndex);
    urlFormGroup.get('value').updateValueAndValidity();
  }

  public patchData(visitedWebsite: IWebsiteCondition[]) {
    if (visitedWebsite.length) {
      this.visitedWebsiteFormArray.clear();
    }
    this.visitedWebsite = cloneDeep(visitedWebsite);
    const values = [];
    const finalValues = [];
    const firstItem = this.visitedWebsite.find((x) => !x.beforeConditionId);
    if (firstItem) {
      values.push(firstItem);
      this.sortWebsite(firstItem, values);
      this.sortTypeData(values, finalValues);
      this.pushItemToArray(finalValues);
      this.onVisitedWebsiteData.emit(this.visitedWebsiteFormArray.value);
    }
  }

  private sortWebsite(item: IWebsiteCondition, values) {
    if (item?.afterConditionId) {
      const exist = this.visitedWebsite.find(
        (x) => x.id === item.afterConditionId
      );
      values.push(exist);
      this.sortWebsite(exist, values);
    }
  }

  private sortTypeData(items: IWebsiteCondition[], finalValues) {
    if (!items.length) {
      return;
    }
    let cloneItems = cloneDeep(items);
    const values = [];
    for (let i = 0; i < items.length; i++) {
      values.push(items[i]);
      cloneItems = cloneItems.filter((x) => x.id !== items[i].id);
      if (items[i].afterConditionType === EConditionType.Or) {
        break;
      }
    }
    finalValues.push(values);
    this.sortTypeData(cloneItems, finalValues);
  }

  private buildArray() {
    this.visitedWebsiteFormArray = this.formBuilder.array([]);
    this.visitedWebsiteFormArray.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.onVisitedWebsiteData.emit(value);
      });
  }

  private buildForm(item?: IWebsiteCondition): FormGroup {
    const config = {
      id: [uuidv4(), [Validators.required]],
      [this.createObjectGroupFormFields['url'].name]: [
        item ? item.value : '',
        [
          this.createObjectGroupFormFields['url'].validationParams.required
            ? Validators.required
            : null,
        ],
      ],
      [this.createObjectGroupFormFields['conditionType'].name]: [
        item ? item.conditionType : EUrlCondition.CONTAINS,
        [
          this.createObjectGroupFormFields['conditionType'].validationParams
            .required
            ? Validators.required
            : null,
        ],
      ],
    };

    return this.formBuilder.group(config, {
      validators: [this.visitedWebsiteValidator.isValidUrl()],
    });
  }
}
