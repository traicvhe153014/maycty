import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ObjectGroupDetailComponent } from './object-group-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'create',
  },
  {
    path: 'create',
    component: ObjectGroupDetailComponent,
  },
  {
    path: ':id',
    component: ObjectGroupDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ObjectGroupDetailRouting {}
