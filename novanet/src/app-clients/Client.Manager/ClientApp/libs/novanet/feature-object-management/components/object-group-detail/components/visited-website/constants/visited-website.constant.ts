import { IDropdownValues } from '@core/models';
import { EUrlCondition } from '../enums';
import { IFormFieldErrors } from '@models';

export const UrlConditionDropdown = [
  {
    id: EUrlCondition.CONTAINS,
    label: 'Chứa',
  },
  {
    id: EUrlCondition.EQUAL,
    label: 'Bằng',
  },
] as IDropdownValues[];

export const VisitedWebsiteFormFieldErrors: IFormFieldErrors = {
  url: [
    {
      type: 'required',
      message: 'Không để trống đường dẫn.',
    },
    {
      type: 'pattern',
      message: 'Đường dẫn không hợp lệ.',
    },
  ],
};
