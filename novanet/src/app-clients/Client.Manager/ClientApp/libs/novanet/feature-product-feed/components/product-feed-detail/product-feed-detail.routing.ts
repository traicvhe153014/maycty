import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductFeedDetailComponent } from './product-feed-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ProductFeedDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductFeedDetailRouting {}
