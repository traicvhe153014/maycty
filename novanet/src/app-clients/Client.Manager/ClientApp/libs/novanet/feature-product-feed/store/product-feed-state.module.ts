import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { ProductFeedService } from './product-feed-state.service';
import { ProductFeedState } from './product-feed.state';

@NgModule({
  imports: [NgxsModule.forFeature([ProductFeedState])],
  providers: [ProductFeedService],
})
export class ProductFeedStateModule {}
