import { ProductFeedListEnum } from '@features/product-feed/components/product-feed-list/enums';
import {
  IProductFeedResponse,
  ProductFeedPermission,
  SourceType,
} from '@features/product-feed/index';
import * as dayjs from 'dayjs';
import { ISettingColumnTable } from '@data-table/models';

export const ProductFeedListColumns = [
  {
    id: ProductFeedListEnum.CHECKBOX,
    title: '',
    width: '48px',
  },
  {
    id: ProductFeedListEnum.INDEX,
    title: 'Stt',
    width: '72px',
  },
  {
    id: ProductFeedListEnum.PRODUCT_FEED,
    title: 'Nguồn cấp dữ liệu',
    compare: (a: IProductFeedResponse, b: IProductFeedResponse) =>
      a.name.localeCompare(b.name),
    sort: true,
    name: 'name',
    width: '30%',
  },
  {
    id: ProductFeedListEnum.INPUT_METHOD,
    title: 'Phương thức nhập',
    compare: (a: IProductFeedResponse, b: IProductFeedResponse) =>
      SourceType[a.sourceType] - SourceType[b.sourceType],
    sort: true,
    name: 'SourceType',
  },
  {
    id: ProductFeedListEnum.STATUS,
    title: 'Trạng thái',
    compare: (a: IProductFeedResponse, b: IProductFeedResponse) =>
      ProductFeedPermission[a.permission] - ProductFeedPermission[b.permission],
    sort: true,
    name: 'Permission',
  },
  {
    id: ProductFeedListEnum.MODIFIED_DATE,
    title: 'Cập nhật',
    compare: (a: IProductFeedResponse, b: IProductFeedResponse) =>
      dayjs(a.modifiedAt).diff(dayjs(b.modifiedAt)),
    sort: true,
    name: 'ModifiedAt',
    descending: true,
    ascending: false
  },
  {
    id: ProductFeedListEnum.PRODUCT_RUNNING,
    title: 'SP hiệu lực',
    compare: (a: IProductFeedResponse, b: IProductFeedResponse) =>
      a.activeProduct - b.activeProduct,
    width: '150px',
    sort: true,
    name: 'ValidProductsCount',
  },
  {
    id: ProductFeedListEnum.PRODUCT_ERROR,
    title: 'SP lỗi dữ liệu ',
    compare: (a: IProductFeedResponse, b: IProductFeedResponse) =>
      a.inActiveProduct - b.inActiveProduct,
    width: '150px',
    sort: true,
    name: 'InvalidProductsCount',
  },
] as ISettingColumnTable<any, any>[];
