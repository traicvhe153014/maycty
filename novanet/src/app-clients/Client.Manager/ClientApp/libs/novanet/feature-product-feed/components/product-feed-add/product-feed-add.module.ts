import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductFeedAddComponent } from './product-feed-add.component';
import { ProductFeedAddRouting } from './product-feed-add.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { NovanetInputModule } from '@shared/custom-input';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [ProductFeedAddComponent],
  imports: [
    CommonModule,
    ProductFeedAddRouting,
    SvgIconModule,
    NovanetInputModule,
    ReactiveFormsModule,
    NzToolTipModule,
  ],
})
export class ProductFeedAddModule {}
