import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import {
  ICreateProductFeed,
  IListProductFeedRequest,
  IProductFeedResponse,
  IProductFeedUpdateRequest,
  IRefreshProductFeedsRequest,
} from './product-feed-state.model';

const ProductFeedUrls = {
  getList: `ProductFeed/List`,
  getById: `ProductFeed/Get`,
  create: 'ProductFeed/Create',
  update: 'ProductFeed/Update',
  refresh: 'ProductFeed/Refresh',
  delete: 'ProductFeed/Delete',
};

@Injectable()
export class ProductFeedService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public getList(
    payload: IListProductFeedRequest
  ): Observable<IProductFeedResponse[]> {
    const params = new HttpParams({
      fromObject: {
        ...payload,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IProductFeedResponse[]>>(
        this.createUrl(ProductFeedUrls.getList),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public getMany(ids: string[]): Observable<IProductFeedResponse[]> {
    const params = new HttpParams({
      fromObject: {
        page: 1,
        pageSize: ids.length,
      },
    });

    return this.httpClient
      .get<ISuccessHttpResponse<IProductFeedResponse[]>>(
        this.createUrl(ProductFeedUrls.getList),
        { params }
      )
      .pipe(map((res) => res.data));
  }

  public getById(
    id: string,
    isFull: boolean
  ): Observable<IProductFeedResponse> {
    const params = new HttpParams({
      fromObject: {
        id,
        isFull,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IProductFeedResponse>>(
        this.createUrl(ProductFeedUrls.getById),
        {
          params,
        }
      )
      .pipe(map((res) => res.data));
  }

  public create(payload: ICreateProductFeed): Observable<IProductFeedResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IProductFeedResponse>>(
        this.createUrl(ProductFeedUrls.create),
        payload
      )
      .pipe(
        map((res) => {
          const tmp = 1;
          return res.data;
        })
      );
  }

  public update(
    payload: IProductFeedUpdateRequest
  ): Observable<IProductFeedResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IProductFeedResponse>>(
        this.createUrl(ProductFeedUrls.update),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public refresh(
    payload: IRefreshProductFeedsRequest
  ): Observable<IProductFeedResponse[]> {
    return this.httpClient
      .put<ISuccessHttpResponse<IProductFeedResponse[]>>(
        this.createUrl(ProductFeedUrls.refresh),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public delete(ids: string[]): Observable<string> {
    return this.httpClient
      .delete<ISuccessHttpResponse<string>>(
        this.createUrl(ProductFeedUrls.delete),
        {
          body: { ids },
        }
      )
      .pipe(map((res) => res.data));
  }
}
