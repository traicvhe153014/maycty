import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IFormFieldErrors, IFormFields } from '@models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  CreateErrorMessages,
  GoogleSheetAuthEmail,
  ProductFeedSourcePathPattern,
} from './constants/product-feed-add.constants';
import { Store } from '@ngxs/store';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  CreateProductFeed,
  ICreateProductFeed,
  SourceType,
} from '@features/product-feed/store';
import { CheckHasAnyProductFeedSuccess } from '@shared/common/store';

interface ISourceTypeTab {
  name: string;
  type: SourceType;
  toolTipMessage?: string;
}

@Component({
  selector: 'novanet-product-feed-add',
  templateUrl: './product-feed-add.component.html',
})
export class ProductFeedAddComponent implements OnInit {
  public selectedSourceType: SourceType = SourceType.GoogleSheet;
  public readonly GoogleSheetAuthEmail = GoogleSheetAuthEmail;
  public readonly SourceTypeEnum = SourceType;
  public readonly sourceTypes: ISourceTypeTab[] = [
    {
      name: 'Google Sheet',
      type: SourceType.GoogleSheet,
    },
    {
      name: 'Content API',
      type: SourceType.ContentApi,
      toolTipMessage: 'Tính năng này sẽ được cập nhật ở phiên bản sau',
    },
  ];

  public createForm: FormGroup;
  public readonly createFormFields: IFormFields = {
    name: {
      name: 'name',
      validationParams: {
        required: true,
      },
    },
    sourcePath: {
      name: 'sourcePath',
      validationParams: {
        required: true,
        pattern: ProductFeedSourcePathPattern,
      },
    },
  };
  public readonly createFormFieldErrors: IFormFieldErrors = {
    name: [
      {
        type: 'required',
        message: 'Thiếu tên nguồn dữ liệu',
      },
    ],
    sourcePath: [
      {
        type: 'required',
        message: 'Thiếu URL',
      },
      {
        type: 'pattern',
        message: 'Không đúng định dạng URL',
      },
      {
        type: 'noPermission',
        message: 'Chưa cấp quyền',
      },
    ],
  };

  @ViewChild('googleSheetAuthLink') public googleSheetAuthLink;

  public constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private notification: NzNotificationService,
    private router: Router,
    private el: ElementRef
  ) {}

  ngOnInit(): void {
    this.buildCreateForm();
  }

  public onSourceTypeChanged(sourceType: SourceType) {
    this.selectedSourceType = sourceType;
  }

  public copyGoogleSheetAuth() {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(this.googleSheetAuthLink.nativeElement);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Sao chép thành công'
      )
    );
  }

  public onSubmitForm(event: Event) {
    event.preventDefault();
    if (!this.createForm.valid) {
      this.createForm.markAllAsTouched();
      return;
    }
    const payload: ICreateProductFeed = {
      ...this.createForm.value,
      sourceType: this.selectedSourceType,
    };
    this.store
      .dispatch(new CreateProductFeed(payload))
      .pipe(take(1))
      .subscribe({
        next: (response) => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công',
              'Tạo nguồn dữ liệu thành công'
            )
          );
          this.store.dispatch(new CheckHasAnyProductFeedSuccess(true));
          this.router.navigateByUrl('/product-feed/list').then();
        },
        error: (err) => {
          const errorMessage = err?.error?.errors?.[0];
          const errorItem = CreateErrorMessages.find((item) =>
            errorMessage.includes(item.error)
          );
          if (errorItem.errorType === 'duplicatedSourcePath') {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.warning,
                errorItem.error
              )
            );
          } else {
            this.createForm.controls[errorItem?.field]?.setErrors({
              [errorItem?.errorType]: true,
            });
            this.el.nativeElement
              .querySelector(`#${errorItem?.field} [nz-input]`)
              ?.focus();
          }
        },
      });
  }

  private buildCreateForm() {
    const config = {
      [this.createFormFields.name.name]: ['', [Validators.required]],
      [this.createFormFields.sourcePath.name]: [
        '',
        [
          Validators.required,
          Validators.pattern(
            this.createFormFields.sourcePath.validationParams.pattern
          ),
        ],
      ],
    };
    this.createForm = this.formBuilder.group(config);
  }
}
