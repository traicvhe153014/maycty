import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductFeedComponent } from './product-feed.component';

const routes: Routes = [
  {
    path: '',
    component: ProductFeedComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./components/product-feed-components.module').then(
            (m) => m.ProductFeedComponentsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatasourceRoutingModule {}
