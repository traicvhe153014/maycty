import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import * as dayjs from 'dayjs';
import { shortDateTimeFormat } from '@features/product-feed/constants';
import {
  GetProductFeedDetail,
  IProductFeedResponse,
  ProductFeedState,
  RefreshProductFeeds,
} from '@features/product-feed/index';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { HideLoading, ShowLoading } from '@shared/global-loading';

@Component({
  selector: 'novanet-product-feed-detail',
  templateUrl: './product-feed-detail.component.html',
})
export class ProductFeedDetailComponent implements OnInit {
  productFeedId: string;
  @Select(ProductFeedState.getDetail)
  productFeed$: Observable<IProductFeedResponse>;
  @Select(ProductFeedState.getLoading)
  loading$: Observable<boolean>;

  constructor(private route: ActivatedRoute, private store: Store) {
    this.productFeedId = route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.store.dispatch(new GetProductFeedDetail(this.productFeedId));
  }

  public dateToString(date: string): string {
    return dayjs(date).format(shortDateTimeFormat);
  }

  public refreshMany() {
    const label = 'refresh product feed';
    this.store.dispatch(new ShowLoading(label));
    this.store
      .dispatch(
        new RefreshProductFeeds({
          productFeedIds: [this.productFeedId],
        })
      )
      .subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công',
              'Cập nhật nguồn dữ liệu thành công'
            )
          );
          this.store.dispatch(new HideLoading(label));
        },
        error: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              'Thất bại',
              'Có lỗi xảy ra trong quá trình cập nhật nguồn dữ liệu'
            )
          );
        },
      });
  }
}
