import { Component, Input } from '@angular/core';
import { ProductFeedPermission } from '@features/product-feed/index';

@Component({
  selector: 'novanet-permission-icon',
  template: `
    <span
      *ngIf="permission === ProductFeedPermissionEnum.Granted"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(33,150,83,1)] bg-[rgba(33,150,83,0.1)]">
      Đã cấp quyền
    </span>
    <span
      *ngIf="permission === ProductFeedPermissionEnum.Processing"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(0,97,193,1)] bg-[rgba(0,97,193,0.1)]">
      Đang xử lý
    </span>
    <span
      *ngIf="permission === ProductFeedPermissionEnum.CannotAccess"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(235,87,87,1)] bg-[rgba(235,87,87,0.1)]">
      Không thể truy cập
    </span>
    <span
      *ngIf="permission === ProductFeedPermissionEnum.CannotParse"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(242,153,74,1)] bg-[rgba(242,153,74,0.1)]">
      Thiếu định dạng
    </span>
  `,
})
export class PermissionIconComponent {
  @Input() permission: number | string;
  public readonly ProductFeedPermissionEnum = ProductFeedPermission;
}
