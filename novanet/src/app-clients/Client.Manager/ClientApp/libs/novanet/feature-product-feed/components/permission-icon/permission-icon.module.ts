import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionIconComponent } from './permission-icon.component';

@NgModule({
  declarations: [PermissionIconComponent],
  imports: [CommonModule],
  exports: [PermissionIconComponent],
})
export class PermissionIconModule {}
