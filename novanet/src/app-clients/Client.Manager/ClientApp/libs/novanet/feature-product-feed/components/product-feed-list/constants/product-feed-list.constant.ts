import { DataTableSettingModel } from '@data-table/models';

export const ProductFeedListSettingTable = {
  id: 'product-feed-list',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
} as DataTableSettingModel;

export const GoogleSheetAuthEmail =
  'novanet-google-sheets@novanet-dev.iam.gserviceaccount.com';
