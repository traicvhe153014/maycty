import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  IProductFeedResponse,
  IProductFeedUpdateRequest,
  UpdateProductFeed,
} from '@features/product-feed/index';

@Component({
  selector: 'novanet-feed-editable-name',
  templateUrl: './editable-name.component.html',
})
export class EditableNameComponent {
  @Input() productFeed: IProductFeedResponse;
  @Input() isAnySelected: boolean;
  public isEditing = false;
  public nameEditing: string;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public setEditing() {
    this.isEditing = true;
    this.nameEditing = this.productFeed.name;
  }

  public cancelEditing() {
    this.isEditing = false;
  }

  public saveEditing() {
    const payload: IProductFeedUpdateRequest = {
      id: this.productFeed.id,
      sourceType: this.productFeed.sourceType,
      name: this.nameEditing,
    };
    this.store.dispatch(new UpdateProductFeed(payload)).subscribe({
      next: (response) => {
        this.isEditing = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công',
            'Cập nhật tên nguồn dữ liệu thành công'
          )
        );
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Thất bại',
            'Có lỗi xảy ra trong quá trình cập nhật tên nguồn dữ liệu'
          )
        );
      },
    });
  }
}
