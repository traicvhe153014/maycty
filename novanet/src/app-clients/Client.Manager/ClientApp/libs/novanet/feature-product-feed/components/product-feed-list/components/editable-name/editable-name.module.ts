import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditableNameComponent } from './editable-name.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [EditableNameComponent],
  imports: [CommonModule, SvgIconModule, FormsModule, RouterModule],
  exports: [EditableNameComponent],
})
export class EditableNameModule {}
