import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductFeedListComponent } from './product-feed-list.component';

const routes: Routes = [
  {
    path: '',
    component: ProductFeedListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductFeedListRouting {}
