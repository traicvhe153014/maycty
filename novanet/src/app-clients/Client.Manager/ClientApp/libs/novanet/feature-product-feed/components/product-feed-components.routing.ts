import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        loadChildren: () =>
          import('./product-feed-list/product-feed-list.module').then(
            (m) => m.ProductFeedListModule
          ),
      },
      {
        path: 'add',
        loadChildren: () =>
          import('./product-feed-add/product-feed-add.module').then(
            (m) => m.ProductFeedAddModule
          ),
      },
      {
        path: ':id',
        loadChildren: () =>
          import('./product-feed-detail/product-feed-detail.module').then(
            (m) => m.ProductFeedDetailModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductFeedComponentsRouting {}
