import { NgModule } from '@angular/core';
import { ErrorListComponent } from './error-list.component';
import { CommonModule } from '@angular/common';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [ErrorListComponent],
  imports: [CommonModule, NzCollapseModule, SvgIconModule],
  exports: [ErrorListComponent],
})
export class ErrorListModule {}
