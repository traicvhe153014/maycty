import { ErrorMessageType } from '@features/product-feed/components/product-feed-add/types/product-feed-add.types';

export const ProductFeedSourcePathPattern =
  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
export const GoogleSheetAuthEmail =
  'novanet-google-sheets@novanet-dev.iam.gserviceaccount.com';

export const CreateErrorMessages: ErrorMessageType[] = [
  {
    error: 'The caller does not have permission [403]',
    errorType: 'noPermission',
    field: 'sourcePath',
  },
  {
    error: 'Requested entity was not found. [404]',
    errorType: 'pattern',
    field: 'sourcePath',
  },
  {
    error: 'Tạo nguồn mới không thành công do URL link tài khoản đã tồn tại',
    errorType: 'duplicatedSourcePath',
    field: 'sourcePath',
  },
];
