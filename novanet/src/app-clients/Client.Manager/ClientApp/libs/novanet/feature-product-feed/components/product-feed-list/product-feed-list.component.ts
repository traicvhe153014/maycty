import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import * as dayjs from 'dayjs';
import { Observable, switchMap, timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { shouldLoadMore } from '@core/utils';
import { shortDateTimeFormat } from '@features/product-feed/constants';
import { ProductFeedListEnum } from '@features/product-feed/components/product-feed-list/enums';
import {
  ClearAllProductFeedsData,
  DeleteProductFeeds,
  GetMoreProductFeedList,
  GetProductFeedList,
  IProductFeedResponse,
  ProductFeedState,
  RefreshProductFeeds,
} from '@features/product-feed/index';
import {
  GoogleSheetAuthEmail,
  ProductFeedListColumns,
  ProductFeedListSettingTable,
} from '@features/product-feed/components/product-feed-list/constants';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import {
  DataTableSettingModel,
  IFieldDetail,
  ISettingColumnTable,
} from '@data-table/models';
import { ESortType } from '@core/enums';
import { HideLoading, ShowLoading } from '@shared/global-loading';

@Component({
  selector: 'novanet-product-feed-list',
  templateUrl: './product-feed-list.component.html',
})
export class ProductFeedListComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @Select(ProductFeedState.getList) public productFeeds$: Observable<
    IProductFeedResponse[]
  >;
  @Select(ProductFeedState.getPage) public currentPage$: Observable<number>;
  @Select(ProductFeedState.getHasMorePages)
  public hasMorePages$: Observable<boolean>;
  @Select(ProductFeedState.getLoading) public loading$: Observable<boolean>;

  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public keyword = '';
  public sorts = ['-modifiedAt'];
  public baseSorts = ['-modifiedAt'];
  public selectedFeeds: string[] = [];
  public isConfirmDeleteVisible = false;
  public shouldShowNoData = true;
  public settings: DataTableSettingModel;
  public columns: ISettingColumnTable<any, any>[] = [];

  public readonly pageSize = 50;
  public readonly productFeedListEnum = ProductFeedListEnum;
  public readonly GoogleSheetAuthEmail = GoogleSheetAuthEmail;

  @ViewChild('googleSheetAuthLink') public googleSheetAuthLink;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  public get isAnySelected() {
    return this.selectedFeeds.length > 0;
  }

  ngOnInit() {
    const loadingLabel = 'loadingLabel';
    this.store.dispatch(new ShowLoading(loadingLabel));
    this.loading$
      .pipe(
        switchMap(
          (_) => timer(200),
          (x) =>
            !x ? this.store.dispatch(new HideLoading(loadingLabel)) : null
        )
      )
      .subscribe();
    this.productFeeds$.subscribe((productFeeds) => {
      if (productFeeds.length) {
        this.settings = ProductFeedListSettingTable;
        this.shouldShowNoData = false;
      }
    });

    this.store.dispatch(
      new GetProductFeedList({
        page: 1,
        pageSize: this.pageSize,
        keyword: this.keyword,
        sorts: this.sorts,
      })
    );
  }

  ngAfterViewInit() {
    this.columns = ProductFeedListColumns.map((column) =>
      column.id === ProductFeedListEnum.CHECKBOX
        ? {
            ...column,
            title: this.checkboxHeader,
          }
        : column
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearAllProductFeedsData());
  }

  public dateToString(date: string): string {
    return dayjs(date).format(shortDateTimeFormat);
  }

  public isFeedSelected(id: string): boolean {
    return !!this.selectedFeeds.find((feed) => feed === id);
  }

  public copyGoogleSheetAuth() {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(this.googleSheetAuthLink.nativeElement);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Sao chép thành công'
      )
    );
  }

  public viewProducts() {
    if (this.selectedFeeds.length > 1) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          'Không thể chọn nhiều hơn một nguồn dữ liệu để xem sản phẩm'
        )
      );
    }
  }

  public onScroll(event: Event) {
    const target = event.target as HTMLDivElement;
    if (shouldLoadMore(target)) {
      this.currentPage$.pipe(take(1)).subscribe({
        next: (currentPage) => {
          this.store.dispatch(
            new GetMoreProductFeedList({
              page: currentPage + 1,
              pageSize: this.pageSize,
              keyword: this.keyword,
              sorts: this.sorts,
            })
          );
        },
      });
    }
  }

  public onSelectFeedChanged(event: Event, id: string) {
    if (this.isFeedSelected(id)) {
      this.selectedFeeds = this.selectedFeeds.filter((feed) => feed !== id);
    } else {
      this.selectedFeeds = [...this.selectedFeeds, id];
    }
  }

  public onSelectAllFeedChanged(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.productFeeds$.subscribe({
        next: (productFeeds) => {
          this.selectedFeeds = productFeeds.map((feed) => feed.id);
        },
      });
    } else {
      this.selectedFeeds = [];
    }
  }

  public refreshMany() {
    this.store
      .dispatch(
        new RefreshProductFeeds({
          productFeedIds: this.selectedFeeds,
        })
      )
      .subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công',
              'Cập nhật nguồn dữ liệu thành công'
            )
          );
          this.selectedFeeds = [];
        },
        error: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              'Thất bại',
              'Có lỗi xảy ra trong quá trình cập nhật nguồn dữ liệu'
            )
          );
        },
      });
  }

  public showConfirmDelete() {
    this.isConfirmDeleteVisible = true;
  }

  public handleCancelDelete() {
    this.isConfirmDeleteVisible = false;
  }

  public confirmDelete() {
    this.store.dispatch(new DeleteProductFeeds(this.selectedFeeds)).subscribe({
      next: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công',
            'Xóa nguồn dữ liệu thành công'
          )
        );
        this.selectedFeeds = [];
        this.isConfirmDeleteVisible = false;
        this.changeDetectorRef.detectChanges();
      },
      error: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Thất bại',
            'Có lỗi xảy ra trong quá trình xóa nguồn dữ liệu'
          )
        );
        this.isConfirmDeleteVisible = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public refreshFeed(id: string) {
    this.store
      .dispatch(
        new RefreshProductFeeds({
          productFeedIds: [id],
        })
      )
      .subscribe({
        next: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công',
              'Cập nhật nguồn dữ liệu thành công'
            )
          );
        },
        error: () => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              'Thất bại',
              'Có lỗi xảy ra trong quá trình cập nhật nguồn dữ liệu'
            )
          );
        },
      });
  }

  public onSearchInputKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.store.dispatch(
        new GetProductFeedList({
          page: 1,
          pageSize: this.pageSize,
          keyword: this.keyword,
          sorts: this.sorts,
        })
      );
    }
  }

  public searchFeeds(values?: IFieldDetail[]) {
    if (values.length) {
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.store.dispatch(
      new GetProductFeedList({
        page: 1,
        pageSize: this.pageSize,
        keyword: this.keyword,
        sorts: this.sorts,
      })
    );
  }
}
