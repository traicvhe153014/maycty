import { ProductFeedErrorType } from '@features/product-feed/types';

export const shortDateTimeFormat = 'DD/MM/YYYY HH:mm';

export const ProductFeedErrors = {
  ValidateUnauthorized: 'ValidateUnauthorized',
  ValidateDuplicateColumn: 'ValidateDuplicateColumn',
  ValidateMissingAttribute: 'ValidateMissingAttribute',
  ValidateValueCouldNotSupport: 'ValidateValueCouldNotSupport',
  ValidateCouldNotParseRequired: 'ValidateCouldNotParseRequired',
  ValidateCouldNotParse: 'ValidateCouldNotParse',
  ValidateDuplicatedProduct: 'ValidateDuplicatedProduct',
};

export const ProductFeedErrorTypes: ProductFeedErrorType = {
  ValidateUnauthorized: {
    isCritical: true,
    title: 'URL nguồn dữ liệu không truy cập được vì đã bị hủy cấp quyền',
    message:
      'Nguồn dữ liệu không truy cập được bạn vui lòng cấp lại quyền cho chúng tôi vào email novanet-google-sheets@novanet-dev.iam.gserviceaccount.com',
  },
  ValidateDuplicateColumn: {
    isCritical: true,
    title: 'Cột thuộc tính xuất hiện nhiều hơn 1 lần',
    message: 'Cột dữ liệu "Id" xuất hiện nhiều hơn 1 lần',
  },
  ValidateMissingAttribute: {
    isCritical: true,
    title: 'Cột thuộc tính bắt buộc bị thiếu hoặc không hợp lệ',
  },
  ValidateCouldNotParseRequired: {
    isCritical: true,
    title: 'Sản phẩm thiếu hoặc sai kiểu dữ liệu ở thuộc tính bắt buộc',
  },
  ValidateCouldNotParse: {
    isCritical: false,
    title: 'Sản phẩm thiếu hoặc sai kiểu dữ liệu ở thuộc tính không bắt buộc',
  },
  ValidateValueCouldNotSupport: {
    isCritical: false,
    title: 'Sản phẩm có giá trị không nằm trong nhóm giá trị được hỗ trợ',
  },
  ValidateDuplicatedProduct: {
    isCritical: true,
    title: 'Sản phẩm bị trùng',
  },
};
