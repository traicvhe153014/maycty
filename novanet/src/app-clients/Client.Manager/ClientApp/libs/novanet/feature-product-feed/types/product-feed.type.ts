import { IProductStatus } from '@features/product-feed/store';

export interface ProductFeedErrorType {
  [key: string]: {
    isCritical: boolean;
    title: string;
    message?: string;
  };
}

export interface IGroupedStatus {
  [key: string]: IProductStatus[];
}

export interface IGroupedStatusByType {
  errorType: string;
  status: IProductStatus[];
  active: boolean;
}
