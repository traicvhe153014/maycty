import { Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import {
  ProductFeedErrors,
  ProductFeedErrorTypes,
} from '@features/product-feed/constants';
import { IGroupedStatusByType } from '@features/product-feed/types';
import { IProductStatus, ProductFeedState } from '@features/product-feed/index';

@Component({
  selector: 'novanet-feed-error-list',
  templateUrl: './error-list.component.html',
})
export class ErrorListComponent {
  @Select(ProductFeedState.getGroupedStatus)
  groupedStatus$: Observable<IGroupedStatusByType[]>;

  public readonly ProductFeedErrors = ProductFeedErrors;
  public readonly ProductFeedErrorTypes = ProductFeedErrorTypes;

  public shouldShowProductCount(errorType: string): boolean {
    return (
      errorType === ProductFeedErrors.ValidateCouldNotParse ||
      errorType === ProductFeedErrors.ValidateCouldNotParseRequired
    );
  }

  public getProductCount(groupedStatus: IProductStatus[]): number {
    return groupedStatus.filter(
      (val, i, arr) =>
        arr.findIndex(
          (item) => item.productEntityId === val.productEntityId
        ) === i
    ).length;
  }
}
