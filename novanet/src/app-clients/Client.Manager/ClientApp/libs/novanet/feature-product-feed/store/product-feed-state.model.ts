export enum SourceType {
  GoogleSheet = 1,
  ContentApi = 2,
}

export type SourceTypeValue = keyof typeof SourceType;

export enum ProductFeedPermission {
  Granted = 1,
  CannotParse = 2,
  Processing = 3,
  CannotAccess = 4,
}

export type ProductFeedPermissionValue = keyof typeof ProductFeedPermission;

export interface IListProductFeedRequest {
  page: number;
  pageSize: number;
  keyword?: string;
  ids?: string;
  sorts?: string[];
  permission?: ProductFeedPermission;
  productValidGreaterThan?: number;
  withAvailableProducts?: boolean;
  withProduct?: boolean;
}

export interface IProductEntityResponse {
  id: string;
  createdAt: Date;
  productFeedId: string;
  attributeValues?: { [key: string]: object | undefined | null };
}

export interface IProductStatus {
  id: string;
  success: boolean;
  errorMessage?: string;
  errorType?: string;
  productIdValue?: string;
  sheetPosition?: string;
  attributeValue?: string;
  productFeedId?: string;
  productEntityId?: string;
  productAttributeValueId?: string;
}

export interface IProductFeedResponse {
  id: string;
  name: string;
  sourceType: SourceTypeValue;
  sourcePath: string;
  permission: ProductFeedPermissionValue;
  modifiedAt: string;
  activeProduct: number;
  inActiveProduct: number;
  productEntities?: IProductEntityResponse[];
  productStatusList?: IProductStatus[];
}

export interface ICreateProductFeed {
  name: string;
  sourceType: SourceTypeValue;
  sourcePath: string;
}

export interface IProductFeedUpdateRequest {
  id: string;
  name: string;
  sourceType: SourceTypeValue;
}

export interface IRefreshProductFeedsRequest {
  productFeedIds: string[];
}

export interface IProductFeedStateModel {
  productFeeds: IProductFeedResponse[];
  detailedProductFeed?: IProductFeedResponse;
  page: number;
  loading: boolean;
  hasMorePages: boolean;
}
