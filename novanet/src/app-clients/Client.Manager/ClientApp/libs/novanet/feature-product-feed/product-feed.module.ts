import { NgModule } from '@angular/core';
import { ProductFeedComponent } from './product-feed.component';
import { CommonModule } from '@angular/common';
import { DatasourceRoutingModule } from './product-feed.routing';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ProductFeedComponent],
  imports: [CommonModule, DatasourceRoutingModule, RouterModule],
})
export class ProductFeedModule {}
