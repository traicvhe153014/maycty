import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SourceTypeIconComponent } from './source-type-icon.component';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [SourceTypeIconComponent],
  imports: [CommonModule, SvgIconModule],
  exports: [SourceTypeIconComponent],
})
export class SourceTypeIconModule {}
