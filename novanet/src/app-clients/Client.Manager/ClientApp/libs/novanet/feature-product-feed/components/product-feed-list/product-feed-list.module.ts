import { NgModule } from '@angular/core';
import { ProductFeedListComponent } from './product-feed-list.component';
import { CommonModule } from '@angular/common';
import { ProductFeedListRouting } from './product-feed-list.routing';
import { DataTableModule, LoadingIconModule } from '@core';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import {
  PermissionIconModule,
  SourceTypeIconModule,
} from '@features/product-feed/components';
import { EditableNameModule } from './components/editable-name';
import { FormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [ProductFeedListComponent],
  imports: [
    CommonModule,
    ProductFeedListRouting,
    SvgIconModule,
    NzModalModule,
    NzTableModule,
    SourceTypeIconModule,
    PermissionIconModule,
    LoadingIconModule,
    EditableNameModule,
    DataTableModule,
    FormsModule,
  ],
})
export class ProductFeedListModule {}
