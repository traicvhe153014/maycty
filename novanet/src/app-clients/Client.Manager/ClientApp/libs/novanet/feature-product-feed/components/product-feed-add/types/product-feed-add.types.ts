export interface ErrorMessageType {
  error: string;
  errorType: string;
  field: string;
}
