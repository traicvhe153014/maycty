import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingIconModule } from '@core';
import { ProductFeedDetailComponent } from './product-feed-detail.component';
import { ProductFeedDetailRouting } from './product-feed-detail.routing';
import {
  PermissionIconModule,
  SourceTypeIconModule,
} from '@features/product-feed/components';
import { ErrorListModule } from './components/error-list/error-list.module';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzEmptyModule } from 'ng-zorro-antd/empty';

@NgModule({
  declarations: [ProductFeedDetailComponent],
  imports: [
    CommonModule,
    ProductFeedDetailRouting,
    SvgIconModule,
    LoadingIconModule,
    SourceTypeIconModule,
    PermissionIconModule,
    ErrorListModule,
    NzEmptyModule,
  ],
})
export class ProductFeedDetailModule {}
