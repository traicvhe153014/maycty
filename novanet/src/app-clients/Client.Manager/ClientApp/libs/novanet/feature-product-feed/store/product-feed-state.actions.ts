import {
  ICreateProductFeed,
  IListProductFeedRequest,
  IProductFeedResponse,
  IProductFeedUpdateRequest,
  IRefreshProductFeedsRequest,
} from './product-feed-state.model';

const enum ProductFeedActions {
  GetProductFeedList = '[Product Feed] Get List',
  GetProductFeedListSuccess = '[Product Feed] Get List Success',
  GetProductFeedListError = '[Product Feed] Get List Error',
  GetMoreProductFeedList = '[Product Feed] Get More List',
  GetMoreProductFeedListSuccess = '[Product Feed] Get More List Success',
  GetMoreProductFeedListError = '[Product Feed] Get More List Error',
  GetProductFeedDetail = '[Product Feed] Get Detail',
  GetProductFeedDetailSuccess = '[Product Feed] Get Detail Success',
  GetProductFeedDetailError = '[Product Feed] Get Detail Error',
  CreateProductFeed = '[Product Feed] Create',
  CreateProductFeedSuccess = '[Product Feed] Create Success',
  CreateProductFeedError = '[Product Feed] Create Error',
  UpdateProductFeed = '[Product Feed] Update',
  UpdateProductFeedSuccess = '[Product Feed] Update Success',
  UpdateProductFeedError = '[Product Feed] Update Error',
  RefreshProductFeeds = '[Product Feed] Refresh',
  RefreshProductFeedsSuccess = '[Product Feed] Refresh Success',
  RefreshProductFeedsError = '[Product Feed] Refresh Error',
  DeleteProductFeeds = '[Product Feed] Delete',
  DeleteProductFeedsSuccess = '[Product Feed] Delete Success',
  DeleteProductFeedsError = '[Product Feed] Delete Error',
  ClearAllProductFeedsData = '[Product Feed] Clear All',
}

export class GetProductFeedList {
  public static readonly type = ProductFeedActions.GetProductFeedList;

  constructor(public payload: IListProductFeedRequest) {}
}

export class GetProductFeedListSuccess {
  public static readonly type = ProductFeedActions.GetProductFeedListSuccess;

  constructor(
    public productFeeds: IProductFeedResponse[],
    public page: number,
    public pageSize: number
  ) {}
}

export class GetProductFeedListError {
  public static readonly type = ProductFeedActions.GetProductFeedListError;

  constructor(public error: any) {}
}

export class GetMoreProductFeedList {
  public static readonly type = ProductFeedActions.GetMoreProductFeedList;

  constructor(public payload: IListProductFeedRequest) {}
}

export class GetMoreProductFeedListSuccess {
  public static readonly type =
    ProductFeedActions.GetMoreProductFeedListSuccess;

  constructor(
    public productFeeds: IProductFeedResponse[],
    public page: number,
    public pageSize: number
  ) {}
}

export class GetMoreProductFeedListError {
  public static readonly type = ProductFeedActions.GetMoreProductFeedListError;

  constructor(public error: any) {}
}

export class GetProductFeedDetail {
  public static readonly type = ProductFeedActions.GetProductFeedDetail;

  constructor(public id: string, public isFull: boolean = true) {}
}

export class GetProductFeedDetailSuccess {
  public static readonly type = ProductFeedActions.GetProductFeedDetailSuccess;

  constructor(public productFeed: IProductFeedResponse) {}
}

export class GetProductFeedDetailError {
  public static readonly type = ProductFeedActions.GetProductFeedDetailError;

  constructor(public error: any) {}
}

export class CreateProductFeed {
  public static readonly type = ProductFeedActions.CreateProductFeed;

  constructor(public payload: ICreateProductFeed) {}
}

export class CreateProductFeedSuccess {
  public static readonly type = ProductFeedActions.CreateProductFeedSuccess;

  constructor(public productFeed: IProductFeedResponse) {}
}

export class CreateProductFeedError {
  public static readonly type = ProductFeedActions.CreateProductFeedError;

  constructor(public error: any) {}
}

export class UpdateProductFeed {
  public static readonly type = ProductFeedActions.UpdateProductFeed;

  constructor(public payload: IProductFeedUpdateRequest) {}
}

export class UpdateProductFeedSuccess {
  public static readonly type = ProductFeedActions.UpdateProductFeedSuccess;

  constructor(public productFeed: IProductFeedResponse) {}
}

export class UpdateProductFeedError {
  public static readonly type = ProductFeedActions.UpdateProductFeedError;

  constructor(public error: any) {}
}

export class RefreshProductFeeds {
  public static readonly type = ProductFeedActions.RefreshProductFeeds;

  constructor(public payload: IRefreshProductFeedsRequest) {}
}

export class RefreshProductFeedsSuccess {
  public static readonly type = ProductFeedActions.RefreshProductFeedsSuccess;

  constructor(public productFeeds: IProductFeedResponse[]) {}
}

export class RefreshProductFeedsError {
  public static readonly type = ProductFeedActions.RefreshProductFeedsError;

  constructor(public error: any) {}
}

export class DeleteProductFeeds {
  public static readonly type = ProductFeedActions.DeleteProductFeeds;

  constructor(public ids: string[]) {}
}

export class DeleteProductFeedsSuccess {
  public static readonly type = ProductFeedActions.DeleteProductFeedsSuccess;

  constructor(public ids: string[]) {}
}

export class DeleteProductFeedsError {
  public static readonly type = ProductFeedActions.DeleteProductFeedsError;

  constructor(public error: any) {}
}

export class ClearAllProductFeedsData {
  public static readonly type = ProductFeedActions.ClearAllProductFeedsData;
}
