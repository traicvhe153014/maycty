import { Component, Input } from '@angular/core';
import { SourceType } from '@features/product-feed/index';

@Component({
  selector: 'novanet-source-type-icon',
  template: `
    <a
      [href]="sourcePath"
      target="_blank"
      *ngIf="sourceType === SourceTypeEnum.GoogleSheet"
      class="flex flex-row items-center">
      <svg-icon name="googleSheet" className="mr-2" size="20"></svg-icon>
      <span>Google Sheet</span>
    </a>
  `,
})
export class SourceTypeIconComponent {
  @Input() sourceType: number | string;
  @Input() sourcePath: string;
  public readonly SourceTypeEnum = SourceType;
}
