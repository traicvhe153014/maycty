import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductFeedComponentsRouting } from './product-feed-components.routing';

@NgModule({
  imports: [CommonModule, ProductFeedComponentsRouting, RouterModule],
})
export class ProductFeedComponentsModule {}
