import { Observable, throwError } from 'rxjs';
import {
  IProductFeedResponse,
  IProductFeedStateModel,
} from './product-feed-state.model';
import {
  ClearAllProductFeedsData,
  CreateProductFeed,
  CreateProductFeedError,
  CreateProductFeedSuccess,
  DeleteProductFeeds,
  DeleteProductFeedsError,
  DeleteProductFeedsSuccess,
  GetMoreProductFeedList,
  GetMoreProductFeedListError,
  GetMoreProductFeedListSuccess,
  GetProductFeedDetail,
  GetProductFeedDetailError,
  GetProductFeedDetailSuccess,
  GetProductFeedList,
  GetProductFeedListError,
  GetProductFeedListSuccess,
  RefreshProductFeeds,
  RefreshProductFeedsError,
  RefreshProductFeedsSuccess,
  UpdateProductFeed,
  UpdateProductFeedError,
  UpdateProductFeedSuccess,
} from './product-feed-state.actions';
import { ProductFeedErrorTypes } from '@features/product-feed/constants';
import {
  IGroupedStatus,
  IGroupedStatusByType,
} from '@features/product-feed/types';
import { GlobalNotificationEnum } from '@shared/global-notification';
import { catchError, map } from 'rxjs/operators';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { HideLoading } from '@shared/global-loading';
import { ProductFeedService } from './product-feed-state.service';

@Injectable()
@State<IProductFeedStateModel>({
  name: 'productFeed',
  defaults: {
    loading: false,
    page: 1,
    productFeeds: [],
    hasMorePages: true,
  },
})
export class ProductFeedState {
  private readonly globalNotificationEnum = GlobalNotificationEnum;

  constructor(private readonly productFeedService: ProductFeedService) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(state: IProductFeedStateModel): IProductFeedResponse[] {
    return state.productFeeds;
  }

  @Selector()
  public static getPage(state: IProductFeedStateModel): number {
    return state.page;
  }

  @Selector()
  public static getHasMorePages(state: IProductFeedStateModel): boolean {
    return state.hasMorePages;
  }

  @Selector()
  public static getLoading(state: IProductFeedStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getDetail(state: IProductFeedStateModel): IProductFeedResponse {
    return state.detailedProductFeed;
  }

  @Selector()
  public static getGroupedStatus(
    state: IProductFeedStateModel
  ): IGroupedStatusByType[] {
    const productFeed = state.detailedProductFeed;

    if (!productFeed) {
      return [];
    }
    const groupedStatus: IGroupedStatus =
      productFeed?.productStatusList?.reduce((result, x) => {
        (result[x.errorType] = result[x.errorType] || []).push(x);
        return result;
      }, {});
    return Object.keys(groupedStatus)
      .filter((key) => !!ProductFeedErrorTypes[key])
      .map(
        (key): IGroupedStatusByType => ({
          errorType: key,
          status: groupedStatus[key],
          active: false,
        })
      );
  }

  /**
   * Actions
   */
  @Action(GetProductFeedList)
  public getList(
    ctx: StateContext<IProductFeedStateModel>,
    { payload }: GetProductFeedList
  ) {
    const loadingLabel = 'Get Product Feed List';
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.productFeedService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(
          new GetProductFeedListSuccess(
            response,
            payload.page,
            payload.pageSize
          )
        )
      ),
      catchError((err) => ctx.dispatch(new GetProductFeedListError(err)))
    );
  }

  @Action(GetProductFeedListSuccess)
  public getListSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { productFeeds, page, pageSize }: GetProductFeedListSuccess
  ) {
    const loadingLabel = 'Get Product Feed List';
    const newPage = productFeeds.length === 0 && page !== 1 ? page - 1 : page;
    const hasMorePages = productFeeds.length === pageSize;
    const currentFeeds = ctx.getState().productFeeds;
    ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      productFeeds,
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetProductFeedListError)
  public getListError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: GetProductFeedListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetProductFeedDetail)
  public getDetail(
    ctx: StateContext<IProductFeedStateModel>,
    { id, isFull }: GetProductFeedDetail
  ) {
    ctx.patchState({ loading: true });
    return this.productFeedService.getById(id, isFull).pipe(
      map((response) =>
        ctx.dispatch(new GetProductFeedDetailSuccess(response))
      ),
      catchError((err) => ctx.dispatch(new GetProductFeedDetailError(err)))
    );
  }

  @Action(GetProductFeedDetailSuccess)
  public getDetailSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { productFeed }: GetProductFeedDetailSuccess
  ) {
    ctx.patchState({ detailedProductFeed: productFeed, loading: false });
  }

  @Action(GetProductFeedDetailError)
  public getDetailError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: GetProductFeedDetailError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetMoreProductFeedList)
  public getMoreList(
    ctx: StateContext<IProductFeedStateModel>,
    { payload }: GetMoreProductFeedList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.productFeedService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(
          new GetMoreProductFeedListSuccess(
            response,
            payload.page,
            payload.pageSize
          )
        )
      ),
      catchError((err) => ctx.dispatch(new GetMoreProductFeedListError(err)))
    );
  }

  @Action(GetMoreProductFeedListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { productFeeds, page, pageSize }: GetMoreProductFeedListSuccess
  ) {
    const newPage = productFeeds.length === 0 ? page - 1 : page;
    const hasMorePages = productFeeds.length === pageSize;
    const currentFeeds = ctx.getState().productFeeds;
    ctx.patchState({
      productFeeds: [...currentFeeds, ...productFeeds],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreProductFeedListError)
  public getMoreListError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: GetMoreProductFeedListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(CreateProductFeed)
  public create(
    ctx: StateContext<IProductFeedStateModel>,
    { payload }: CreateProductFeed
  ) {
    return this.productFeedService.create(payload).pipe(
      map((response) => ctx.dispatch(new CreateProductFeedSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateProductFeedError(error)))
    );
  }

  @Action(CreateProductFeedSuccess)
  public createSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { productFeed }: CreateProductFeedSuccess
  ) {}

  @Action(CreateProductFeedError)
  public createError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: CreateProductFeedError
  ) {
    return throwError(error);
  }

  @Action(UpdateProductFeed)
  public update(
    ctx: StateContext<IProductFeedStateModel>,
    { payload }: UpdateProductFeed
  ) {
    return this.productFeedService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateProductFeedSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateProductFeedError(error)))
    );
  }

  @Action(UpdateProductFeedSuccess)
  public updateSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { productFeed }: UpdateProductFeedSuccess
  ) {
    const { productFeeds } = ctx.getState();
    const updatedFeeds = productFeeds.map((feed) =>
      feed.id === productFeed.id ? { ...feed, ...productFeed } : feed
    );
    ctx.patchState({ productFeeds: updatedFeeds });
  }

  @Action(UpdateProductFeedError)
  public updateError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: UpdateProductFeedError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(RefreshProductFeeds)
  public refresh(
    ctx: StateContext<IProductFeedStateModel>,
    { payload }: RefreshProductFeeds
  ) {
    return this.productFeedService.refresh(payload).pipe(
      map((response) => ctx.dispatch(new RefreshProductFeedsSuccess(response))),
      catchError((error) => ctx.dispatch(new RefreshProductFeedsError(error)))
    );
  }

  @Action(RefreshProductFeedsSuccess)
  public refreshSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { productFeeds }: RefreshProductFeedsSuccess
  ) {
    const { productFeeds: currentFeeds } = ctx.getState();
    const updatedFeeds = currentFeeds.map(
      (feed) => productFeeds.find((newFeed) => newFeed.id === feed.id) ?? feed
    );
    ctx.patchState({ productFeeds: updatedFeeds });
  }

  @Action(RefreshProductFeedsError)
  public refreshError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: RefreshProductFeedsError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(DeleteProductFeeds)
  public delete(
    ctx: StateContext<IProductFeedStateModel>,
    { ids }: DeleteProductFeeds
  ) {
    return this.productFeedService.delete(ids).pipe(
      map((response) => ctx.dispatch(new DeleteProductFeedsSuccess(ids))),
      catchError((err) => ctx.dispatch(new DeleteProductFeedsError(err)))
    );
  }

  @Action(DeleteProductFeedsSuccess)
  public deleteSuccess(
    ctx: StateContext<IProductFeedStateModel>,
    { ids }: DeleteProductFeedsSuccess
  ) {
    const { productFeeds } = ctx.getState();
    const updatedFeeds = productFeeds.filter(
      (feed) => !ids.find((id) => id === feed.id)
    );
    ctx.patchState({ productFeeds: updatedFeeds, loading: false });
  }

  @Action(DeleteProductFeedsError)
  public deleteError(
    ctx: StateContext<IProductFeedStateModel>,
    { error }: DeleteProductFeedsError
  ) {
    return throwError(error);
  }

  @Action(ClearAllProductFeedsData)
  public clearAllData(ctx: StateContext<IProductFeedStateModel>) {
    const productFeedState = {
      productFeeds: [],
      detailedProductFeed: {},
    } as IProductFeedStateModel;

    ctx.patchState(productFeedState);
  }
}
