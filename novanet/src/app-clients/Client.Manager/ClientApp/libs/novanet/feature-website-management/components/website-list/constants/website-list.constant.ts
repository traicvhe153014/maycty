import { DataTableSettingModel } from '@data-table/models';
import { WebsiteStatus } from '@features/feature-website-management/store/website-state.model';
import { WebsiteSearchLevelEnum } from '@features/feature-website-management/components/website-list/enums';
import { PredicateOperatorEnum } from '@core/enums';

export const WebsiteListSettingTable = {
  id: 'publisher-list',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '1900px',
  sortType: 'single',
} as DataTableSettingModel;

export const websiteStatusOptions = [
  {
    value: '',
    label: 'Tất cả',
  },
  {
    value: WebsiteStatus.Running,
    label: 'Đang chạy',
  },
  {
    value: WebsiteStatus.Disabled,
    label: 'Tạm dừng',
  },
];

export const websiteSearchLevelLabels = {
  [WebsiteSearchLevelEnum.EmailPublisher]: 'Email publisher',
  [WebsiteSearchLevelEnum.NameDomainWebsites]: 'Domain Websites',
};

export const websiteSearchLevelFields = {
  [WebsiteSearchLevelEnum.EmailPublisher]: '0',
  [WebsiteSearchLevelEnum.NameDomainWebsites]: '1',
};

export const websiteSearchOperatorLabels = {
  [PredicateOperatorEnum.Contains]: 'bao gồm',
};
