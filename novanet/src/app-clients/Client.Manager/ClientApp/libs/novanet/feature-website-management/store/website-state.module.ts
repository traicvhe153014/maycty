import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { IdentityService } from '@shared/identity';
import { WebsiteService } from '@features/feature-website-management/store/website-state.service';
import { WebsiteState } from '@features/feature-website-management/store/website.state';
import { SharingService } from '@shared/sharing/store';

@NgModule({
  imports: [NgxsModule.forFeature([WebsiteState])],
  providers: [WebsiteService, IdentityService, SharingService],
})
export class WebsiteStateModule {}
