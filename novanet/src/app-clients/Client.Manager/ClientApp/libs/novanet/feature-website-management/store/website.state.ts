import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { GlobalNotificationEnum } from '@shared/global-notification';
import { WebsiteService } from '@features/feature-website-management/store/website-state.service';
import {
  IDomainAliasesResponse,
  IUpdatedDomainAliasData,
  IWebsiteResponse,
  IWebsiteStateModel,
  WebsiteStatusValue,
} from '@features/feature-website-management/store/website-state.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HideLoading } from '@shared/global-loading';
import {
  ClearAllWebsitesData,
  CreateDomainAlias,
  CreateDomainAliasError,
  CreateDomainAliasSuccess,
  CreateWebsite,
  CreateWebsiteError,
  CreateWebsiteSuccess,
  DeleteDomainAlias,
  DeleteDomainAliasError,
  DeleteDomainAliasSuccess,
  GetIsCallAPI,
  GetListDomainAlias,
  GetListDomainAliasError,
  GetListDomainAliasSuccess,
  GetListProhibitedCategories,
  GetMoreWebsiteList,
  GetMoreWebsiteListError,
  GetMoreWebsiteListSuccess,
  GetPublishersId,
  GetStatusWebsite,
  GetWebsiteId,
  GetWebsiteList,
  GetWebsiteListError,
  GetWebsiteListSuccess,
  UpdateDomainAlias,
  UpdateDomainAliasError,
  UpdateDomainAliasSuccess,
  UpdateWebsite,
  UpdateWebsiteError,
  UpdateWebsiteProhibitedCategory,
  UpdateWebsiteProhibitedCategoryError,
  UpdateWebsiteProhibitedCategorySuccess,
  UpdateWebsiteSuccess,
} from '@features/feature-website-management/store/website-state.actions';
import { IProhibitedCategory, SharingService } from '@shared/sharing/store';

@Injectable()
@State<IWebsiteStateModel>({
  name: 'website',
  defaults: {
    websites: [],
    page: 1,
    loading: false,
    hasMorePages: true,
    total: 0,
    status: null,
    ListProhibitedCategories: [],
    ListDomainAlias: [],
    isCallAPI: false,
    publisherId: null,
    websiteId: null,
    shouldShowNoData: false,
  },
})
export class WebsiteState {
  private readonly globalNotificationEnum = GlobalNotificationEnum;

  constructor(
    private readonly websiteService: WebsiteService,
    private readonly sharingService: SharingService
  ) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(state: IWebsiteStateModel): IWebsiteResponse[] {
    return state.websites;
  }

  @Selector()
  public static getPage(state: IWebsiteStateModel): number {
    return state.page;
  }

  @Selector()
  public static getLoading(state: IWebsiteStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getTotal(state: IWebsiteStateModel): number {
    return state.total;
  }

  @Selector()
  public static getStatus(state: IWebsiteStateModel): WebsiteStatusValue {
    return state.status;
  }

  @Selector()
  public static getPublishersId(state: IWebsiteStateModel): string {
    return state.publisherId;
  }

  @Selector()
  public static getListProhibitedCategories(
    state: IWebsiteStateModel
  ): IProhibitedCategory[] {
    return state.ListProhibitedCategories;
  }

  @Selector()
  public static getListDomainAlias(
    state: IWebsiteStateModel
  ): IDomainAliasesResponse[] {
    return state.ListDomainAlias;
  }

  @Selector()
  public static getIsCallAPI(state: IWebsiteStateModel): boolean {
    return state.isCallAPI;
  }

  @Selector()
  public static getUpdatedDomainAliasData(
    state: IWebsiteStateModel
  ): IUpdatedDomainAliasData {
    return state.updatedDomainAliasData;
  }

  @Selector()
  public static getShouldShowNoData(state: IWebsiteStateModel): boolean {
    return state.shouldShowNoData;
  }

  /**
   * Actions
   */
  @Action(GetWebsiteList)
  public getList(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: GetWebsiteList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.websiteService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(
          new GetWebsiteListSuccess(response, payload.page, payload.pageSize)
        )
      ),
      catchError((err) => ctx.dispatch(new GetWebsiteListError(err)))
    );
  }

  @Action(GetWebsiteListSuccess)
  public getListSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { response, page, pageSize }: GetWebsiteListSuccess
  ) {
    const { data: websites } = response;
    const loadingLabel = 'Get Website List';
    const newPage = websites.length === 0 && page !== 1 ? page - 1 : page;
    const hasMorePages = websites.length === pageSize;
    ctx.dispatch(new HideLoading(loadingLabel));
    ctx.patchState({
      websites,
      page: newPage,
      hasMorePages,
      loading: false,
      total: response.total,
      shouldShowNoData: !websites,
    });
  }

  @Action(GetWebsiteListError)
  public getListError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: GetWebsiteListError
  ) {
    ctx.patchState({
      loading: false,
      shouldShowNoData: !ctx.getState().websites,
    });
    return throwError(error);
  }

  @Action(UpdateWebsite)
  public update(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: UpdateWebsite
  ) {
    return this.websiteService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateWebsiteSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateWebsiteError(error)))
    );
  }

  @Action(UpdateWebsiteSuccess)
  public updateSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { websiteResponse }: UpdateWebsiteSuccess
  ) {
    const { websites } = ctx.getState();
    const updatedWebsites = websites.map((web) =>
      web.id === websiteResponse.id ? { ...web, ...websiteResponse } : web
    );
    ctx.patchState({ websites: updatedWebsites });
  }

  @Action(UpdateWebsiteError)
  public updateError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: UpdateWebsiteError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(GetStatusWebsite)
  public GetStatusPublisher(
    ctx: StateContext<IWebsiteStateModel>,
    { status }: GetStatusWebsite
  ) {
    ctx.patchState({ status: status });
  }

  @Action(ClearAllWebsitesData)
  public clearAllData(ctx: StateContext<IWebsiteStateModel>) {
    const websiteState = {
      websites: [],
      detailedWebsite: {},
      page: 1,
      loading: false,
      hasMorePages: false,
      total: 0,
      updatedDomainAliasData: undefined,
    } as IWebsiteStateModel;

    ctx.patchState(websiteState);
  }

  @Action(GetMoreWebsiteList)
  public getMoreList(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: GetMoreWebsiteList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.websiteService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(
          new GetMoreWebsiteListSuccess(
            response,
            payload.page,
            payload.pageSize
          )
        )
      ),
      catchError((err) => ctx.dispatch(new GetMoreWebsiteListError(err)))
    );
  }

  @Action(GetMoreWebsiteListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { response, page, pageSize }: GetMoreWebsiteListSuccess
  ) {
    const { data: websites } = response;
    const newPage = websites.length === 0 ? page - 1 : page;
    const hasMorePages = websites.length === pageSize;
    const currentWebsites = ctx.getState().websites;
    ctx.patchState({
      websites: [...currentWebsites, ...websites],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreWebsiteListError)
  public getMoreListError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: GetMoreWebsiteListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetListProhibitedCategories)
  public getListProhibitedCategories(ctx: StateContext<IWebsiteStateModel>) {
    this.sharingService.listProhibitedCategories().subscribe((response) => {
      ctx.patchState({ ListProhibitedCategories: response });
    });
  }

  @Action(UpdateWebsiteProhibitedCategory)
  public updateWebsiteProhibitedCategory(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: UpdateWebsiteProhibitedCategory
  ) {
    return this.websiteService.updateWebsiteProhibitedCategory(payload).pipe(
      map((response) =>
        ctx.dispatch(new UpdateWebsiteProhibitedCategorySuccess(response))
      ),
      catchError((error) =>
        ctx.dispatch(new UpdateWebsiteProhibitedCategoryError(error))
      )
    );
  }

  @Action(UpdateWebsiteProhibitedCategorySuccess)
  public updateWebsiteProhibitedCategorySuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { ProhibitedCategoryResponse }: UpdateWebsiteProhibitedCategorySuccess
  ) {
    const { websites, ListProhibitedCategories } = ctx.getState();
    const updatedWebsites = websites.map((web) =>
      ProhibitedCategoryResponse.length > 0 &&
      web.id === ProhibitedCategoryResponse[0].websiteId
        ? {
            ...web,
            prohibitedCategory: ProhibitedCategoryResponse.map((cat) => ({
              id: cat.prohibitedCategoryId,
              name: ListProhibitedCategories.find(
                (item) => item.id === cat.prohibitedCategoryId
              )?.name,
            })),
          }
        : ProhibitedCategoryResponse.length === 0 &&
          web.id === ctx.getState().websiteId
        ? { ...web, prohibitedCategory: [] }
        : web
    );
    ctx.patchState({ websites: updatedWebsites });
  }

  @Action(UpdateWebsiteProhibitedCategoryError)
  public updateWebsiteProhibitedCategoryError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: UpdateWebsiteProhibitedCategoryError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(GetListDomainAlias)
  public getListDomainAlias(
    ctx: StateContext<IWebsiteStateModel>,
    { websiteId }: GetListDomainAlias
  ) {
    return this.websiteService.getListDomainAliases(websiteId).pipe(
      map((response) => ctx.dispatch(new GetListDomainAliasSuccess(response))),
      catchError((err) => ctx.dispatch(new GetMoreWebsiteListError(err)))
    );
  }

  @Action(GetListDomainAliasSuccess)
  public getListDomainAliasSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { DomainAliasResponse }: GetListDomainAliasSuccess
  ) {
    ctx.patchState({ ListDomainAlias: DomainAliasResponse });
  }

  @Action(GetListDomainAliasError)
  public getListDomainAliasError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: GetListDomainAliasError
  ) {
    return throwError(error);
  }

  @Action(GetIsCallAPI)
  public getIsCallAPI(
    ctx: StateContext<IWebsiteStateModel>,
    { status }: GetIsCallAPI
  ) {
    ctx.patchState({ isCallAPI: status });
  }

  @Action(UpdateDomainAlias)
  public updateDomainAlias(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: UpdateDomainAlias
  ) {
    return this.websiteService.updateDomainAlias(payload).pipe(
      map((response) => ctx.dispatch(new UpdateDomainAliasSuccess(response))),
      catchError((error) =>
        ctx.dispatch(new UpdateWebsiteProhibitedCategoryError(error))
      )
    );
  }

  @Action(UpdateDomainAliasSuccess)
  public updateDomainAliasSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    domainAliasResponse: UpdateDomainAliasSuccess
  ) {
    const { ListDomainAlias, websites } = ctx.getState();
    const updatedListDomainAlias = ListDomainAlias.map((DomainAlias) =>
      DomainAlias.id === domainAliasResponse.domainAliasResponse.id
        ? {
            ...domainAliasResponse.domainAliasResponse,
          }
        : DomainAlias
    );
    const updateWebsites = websites.map((web) =>
      web.id === domainAliasResponse.domainAliasResponse.websiteId
        ? {
            ...web,
            domainAliases: updatedListDomainAlias.map((domainAlias) => ({
              id: domainAlias.id,
              name: domainAlias.name,
              websiteId: domainAlias.websiteId,
            })),
          }
        : web
    );
    const updatedWebsite = updateWebsites.find(
      (x) => x.id === domainAliasResponse.domainAliasResponse.websiteId
    );
    ctx.patchState({
      ListDomainAlias: updatedListDomainAlias,
      websites: updateWebsites,
      updatedDomainAliasData: {
        id: domainAliasResponse.domainAliasResponse.websiteId,
        value: updatedWebsite?.domainAliases,
      },
    });
  }

  @Action(UpdateDomainAliasError)
  public updateDomainAliasError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: UpdateDomainAliasError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(DeleteDomainAlias)
  public deleteDomainAlias(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: DeleteDomainAlias
  ) {
    return this.websiteService.deleteDomainAlias(payload.id).pipe(
      map((response) => ctx.dispatch(new DeleteDomainAliasSuccess(payload.id))),
      catchError((error) =>
        ctx.dispatch(new UpdateWebsiteProhibitedCategoryError(error))
      )
    );
  }

  @Action(DeleteDomainAliasSuccess)
  public deleteDomainAliasSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { domainAliasId }: DeleteDomainAliasSuccess
  ) {
    const { ListDomainAlias, websites } = ctx.getState();
    const websitesId = ListDomainAlias[0].websiteId;
    let updateDomainAlias = ListDomainAlias.filter(
      (item) => item.id !== domainAliasId
    );
    const updatedWebsites = websites.map((web) =>
      web.id === websitesId
        ? {
            ...web,
            domainAliases: updateDomainAlias.map((domainAlias) => ({
              id: domainAlias.id,
              name: domainAlias.name,
              websiteId: domainAlias.websiteId,
            })),
          }
        : web
    );
    const updatedWebsite = updatedWebsites.find((x) => x.id === websitesId);
    ctx.patchState({
      ListDomainAlias: updateDomainAlias,
      websites: updatedWebsites,
      updatedDomainAliasData: {
        id: websitesId,
        value: updatedWebsite?.domainAliases,
      },
    });
  }

  @Action(DeleteDomainAliasError)
  public deleteDomainAliasError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: DeleteDomainAliasError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(CreateDomainAlias)
  public createDomainAlias(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: CreateDomainAlias
  ) {
    return this.websiteService.createDomainAlias(payload).pipe(
      map((response) => ctx.dispatch(new CreateDomainAliasSuccess(response))),
      catchError((error) =>
        ctx.dispatch(new UpdateWebsiteProhibitedCategoryError(error))
      )
    );
  }

  @Action(CreateDomainAliasSuccess)
  public createDomainAliasSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    domainAliasResponse: CreateDomainAliasSuccess
  ) {
    const { ListDomainAlias, websites } = ctx.getState();
    const updatedListDomainAlias = [
      domainAliasResponse.domainAliasResponse,
    ].concat(ListDomainAlias);
    const updatedWebsites = websites.map((web) =>
      web.id === domainAliasResponse.domainAliasResponse.websiteId
        ? {
            ...web,
            domainAliases: updatedListDomainAlias.map((domainAlias) => ({
              id: domainAlias.id,
              name: domainAlias.name,
              websiteId: domainAlias.websiteId,
            })),
          }
        : web
    );
    ctx.patchState({
      ListDomainAlias: updatedListDomainAlias,
      websites: updatedWebsites,
    });
  }

  @Action(CreateDomainAliasError)
  public createDomainAliasError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: CreateDomainAliasError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(GetPublishersId)
  public getPublishersId(
    ctx: StateContext<IWebsiteStateModel>,
    { publishersId }: GetPublishersId
  ) {
    ctx.patchState({ publisherId: publishersId });
  }

  @Action(GetWebsiteId)
  public getWebsiteId(
    ctx: StateContext<IWebsiteStateModel>,
    { websiteId }: GetWebsiteId
  ) {
    ctx.patchState({ websiteId: websiteId });
  }

  @Action(CreateWebsite)
  public createWebsite(
    ctx: StateContext<IWebsiteStateModel>,
    { payload }: CreateWebsite
  ) {
    return this.websiteService.createWebsite(payload).pipe(
      map((response) => ctx.dispatch(new CreateWebsiteSuccess(response))),
      catchError((error) => ctx.dispatch(new CreateWebsiteError(error)))
    );
  }

  @Action(CreateWebsiteSuccess)
  public createWebsiteSuccess(
    ctx: StateContext<IWebsiteStateModel>,
    { websiteResponse }: CreateWebsiteSuccess
  ) {
    ctx.patchState({ createdWebsiteId: websiteResponse.id });
  }

  @Action(CreateWebsiteError)
  public createWebsiteError(
    ctx: StateContext<IWebsiteStateModel>,
    { error }: CreateWebsiteError
  ) {
    ctx.patchState({});
    return throwError(error);
  }
}
