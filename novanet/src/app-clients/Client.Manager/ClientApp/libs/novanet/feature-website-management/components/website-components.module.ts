import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteListModule } from '@features/feature-website-management/components/website-list/website-list.module';
import { WebsiteComponentsRoutingModule } from '@features/feature-website-management/components/website-components.routing';
import { WebsiteAddModule } from '@features/feature-website-management/components/website-add';

@NgModule({
  imports: [
    CommonModule,
    WebsiteListModule,
    WebsiteComponentsRoutingModule,
    WebsiteAddModule,
  ],
})
export class WebsiteComponentsModule {}
