import { IWebsiteSearchOption } from '@features/feature-website-management/components/website-list/type/website-group.type';
import { IProhibitedCategory } from '@shared/sharing/store';

export enum WebsiteStatus {
  Running = 1,
  Disabled = 2,
}

export type WebsiteStatusValue = keyof typeof WebsiteStatus;

export interface IListWebsiteRequest {
  page: number;
  pageSize: number;
  filters?: IWebsiteSearchOption[];
  sorts?: string[];
  status?: WebsiteStatusValue;
  publisherId: string;
}

export interface IListWebsiteResponse {
  data: IWebsiteResponse[];
  total: number;
}

export interface IWebsiteResponse {
  id: string;
  name: string;
  publisherId: string;
  publisherEmail?: string;
  publisherName?: string;
  publisherPhoneNumber?: string;
  domain: string;
  url: string;
  active: boolean;
  status: any;
  summaryZoneReport: number;
  summaryCampaignRunning: number;
  domainAliases: IDomainAlias[];
  prohibitedCategory: IProhibitedCategory[];
  isLoading?: boolean;
}

export interface IDomainAlias {
  name: string;
  websiteId: string;
  id: string;
  subId?: string;
  publisherId?: string;
}

export interface IWebsiteStateModel {
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  total: number;
  status?: WebsiteStatusValue;
  websites: IWebsiteResponse[];
  detailedWebsite?: IWebsiteResponse;
  ListProhibitedCategories?: IProhibitedCategory[];
  ListDomainAlias?: IDomainAliasesResponse[];
  isCallAPI: boolean;
  publisherId: string;
  createdWebsiteId?: string;
  websiteId: string;
  updatedDomainAliasData?: IUpdatedDomainAliasData;
  shouldShowNoData?: boolean;
}

export interface IUpdatedDomainAliasData {
  id: string;
  value: IDomainAlias[];
}

export interface IUpdateWebsiteRequest {
  id: string;
  isActive?: boolean;
  domain?: string;
}

export interface IUpdateWebsiteProhibitedCategoryRequest {
  websiteId: string;
  prohibitedCategoriesId: string[] | [];
}

export interface IListWebsiteProhibitedCategoryResponse {
  data: IWebsiteProhibitedCategoryResponse[];
}

export interface IWebsiteProhibitedCategoryResponse {
  id: string;
  prohibitedCategoryId: string;
  websiteId: string;
}

export interface IDomainAliasesResponse {
  id: string;
  name: string;
  websiteId: string;
  search?: string;
  subId: number;
}

export interface ICreateDomainAliasRequest {
  name: string;
  websiteId: string;
  publisherId?: string;
}

export interface ICreateWebsite {
  publisherId: string[];
  domain: string;
  url: string;
  prohibitedCategoryId: string;
  domainAliases: IDomainAlias[];
}
