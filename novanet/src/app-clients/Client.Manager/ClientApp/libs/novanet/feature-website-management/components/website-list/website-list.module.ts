import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DataTableModule,
  ExpandableListModule,
  FormatterModule,
  LoadingIconModule,
  MapModule,
} from '@core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';
import { NovanetInputModule } from '@shared/custom-input';
import { WebsiteListRoutingModule } from '@features/feature-website-management/components/website-list/website-list.routing';
import { WebsiteListComponent } from '@features/feature-website-management/components/website-list/website-list.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { WebsiteStatusIconModule } from '@features/feature-website-management/components/website-status-icon/website-status-icon.module';
import { WebsiteEditableFieldModule } from '@features/feature-website-management/components/website-list/components/website-editable-field/website-editable-field.module';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { CheckPolicyModule } from '@shared/layouts/navbar/pipes';

@NgModule({
  declarations: [WebsiteListComponent],
  exports: [WebsiteListComponent],
  imports: [
    CommonModule,
    WebsiteListRoutingModule,
    DataTableModule,
    LoadingIconModule,
    FormatterModule,
    FormsModule,
    SvgIconModule,
    SwitchNoAnimationModule,
    NovanetInputModule,
    ReactiveFormsModule,
    ExpandableListModule,
    MapModule,
    NzSpinModule,
    NzSelectModule,
    NzToolTipModule,
    NzModalModule,
    WebsiteStatusIconModule,
    WebsiteEditableFieldModule,
    NzButtonModule,
    CheckPolicyModule,
  ],
})
export class WebsiteListModule {}
