import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WebsiteManagementComponent } from './website-management.component';

const routes: Routes = [
  {
    path: '',
    component: WebsiteManagementComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./components/website-components.module').then(
            (m) => m.WebsiteComponentsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebsiteManagementRoutingModule {}
