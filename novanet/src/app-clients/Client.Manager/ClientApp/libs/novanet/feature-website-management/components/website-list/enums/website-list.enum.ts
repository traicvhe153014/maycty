export enum WebsiteListEnum {
  INDEX,
  IS_ACTIVE,
  DOMAIN_WEBSITES,
  PUBLISHER,
  STATUS,
  SUMMARY_ZONE_REPORT,
  DOMAIN_ALIASES,
  PROHIBITED_CATEGORY,
  SUMMARY_CAMPAIGN_RUNNING,
}

export enum WebsiteSearchLevelEnum {
  EmailPublisher,
  NameDomainWebsites,
}

export enum TypeEdit {
  EDIT_DOMAIN_ALIAS,
  SHOW_DOMAIN_ALIAS,
  SHOW_CATEGORY,
}

export enum ErrorType {
  DOMAIN_ALIASES_EXISTED = 1,
  URL_EXISTED = 2,
  DOMAIN_EXISTED = 3,
}
