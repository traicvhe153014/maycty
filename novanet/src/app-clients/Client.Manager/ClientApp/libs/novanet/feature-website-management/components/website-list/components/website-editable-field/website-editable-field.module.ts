import { WebsiteEditableFieldComponent } from '@features/feature-website-management/components/website-list/components/website-editable-field/website-editable-field.component';
import { CommonModule } from '@angular/common';
import { SvgIconModule } from '@core/components/svg-icon';
import { FormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NgModule } from '@angular/core';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [WebsiteEditableFieldComponent],
  imports: [
    CommonModule,
    SvgIconModule,
    FormsModule,
    NzInputModule,
    NzSpaceModule,
    NzIconModule,
    NzButtonModule,
    SvgIconModule,
    NzToolTipModule,
  ],
  exports: [WebsiteEditableFieldComponent],
})
export class WebsiteEditableFieldModule {}
