import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';
import { ValidateOnBlurModule } from '@shared/directives';
import { NzWaveModule } from 'ng-zorro-antd/core/wave';
import { WebsiteAddRoutingModule } from '@features/feature-website-management/components/website-add/website-add.routing';
import { WebsiteAddComponent } from './website-add.component';
import { ProhibitedCategoryOptionsPipe } from '@features/feature-website-management/components/website-add/pipes';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzButtonModule } from 'ng-zorro-antd/button';

@NgModule({
  declarations: [WebsiteAddComponent, ProhibitedCategoryOptionsPipe],
  exports: [WebsiteAddComponent],
  imports: [
    CommonModule,
    WebsiteAddRoutingModule,
    NzFormModule,
    ReactiveFormsModule,
    NovanetInputModule,
    ValidateOnBlurModule,
    NzWaveModule,
    FormsModule,
    SvgIconModule,
    NzButtonModule,
  ],
})
export class WebsiteAddModule {}
