import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, take } from 'rxjs/operators';
import { ISuccessHttpResponse } from '@models';
import {
  ICreateDomainAliasRequest,
  ICreateWebsite,
  IDomainAlias,
  IDomainAliasesResponse,
  IListWebsiteProhibitedCategoryResponse,
  IListWebsiteRequest,
  IListWebsiteResponse,
  IUpdateWebsiteProhibitedCategoryRequest,
  IUpdateWebsiteRequest,
  IWebsiteProhibitedCategoryResponse,
  IWebsiteResponse,
  WebsiteStatusValue,
} from '@features/feature-website-management/store/website-state.model';
import { WebsiteState } from '@features/feature-website-management/store/website.state';
import { Select } from '@ngxs/store';

const WebsiteUrls = {
  getList: `Website/List`,
  update: `Website/Update`,
  updateWebsiteProhibitedCategory: `Website/UpdateProhibitedCategory`,
  listDomainAliases: `Website/ListDomainAliases`,
  updateDomainAlias: `Website/UpdateDomainAlias`,
  deleteDomainAlias: `Website/DeleteDomainAlias`,
  createDomainAlias: `Website/CreateDomainAlias`,
  createWebsite: `Website/Create`,
};

@Injectable()
export class WebsiteService extends BaseService {
  @Select(WebsiteState.getStatus)
  public status$: Observable<WebsiteStatusValue>;

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1');
  }

  public getList(
    payload: IListWebsiteRequest
  ): Observable<IListWebsiteResponse> {
    this.status$.pipe(take(1)).subscribe((status) => {
      if (status != null) {
        payload.status = status;
      }
    });
    if (payload.filters === undefined) {
      payload.filters = [];
    }
    if (payload.publisherId === null) {
      delete payload.publisherId;
    }
    const params = new HttpParams({
      fromObject: {
        ...payload,
        filters: JSON.stringify(payload.filters),
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IListWebsiteResponse>>(
        this.createUrl(WebsiteUrls.getList),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public update(payload: IUpdateWebsiteRequest): Observable<IWebsiteResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IWebsiteResponse>>(
        this.createUrl(WebsiteUrls.update),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public updateWebsiteProhibitedCategory(
    payload: IUpdateWebsiteProhibitedCategoryRequest
  ): Observable<IWebsiteProhibitedCategoryResponse[]> {
    return this.httpClient
      .put<ISuccessHttpResponse<IWebsiteProhibitedCategoryResponse[]>>(
        this.createUrl(WebsiteUrls.updateWebsiteProhibitedCategory),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public getListDomainAliases(
    websiteId: string
  ): Observable<IDomainAliasesResponse[]> {
    const params = new HttpParams({
      fromObject: {
        websiteId,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IDomainAliasesResponse[]>>(
        this.createUrl(WebsiteUrls.listDomainAliases),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public updateDomainAlias(
    payload: IDomainAlias
  ): Observable<IDomainAliasesResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IDomainAliasesResponse>>(
        this.createUrl(WebsiteUrls.updateDomainAlias),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public deleteDomainAlias(id: string): Observable<string> {
    return this.httpClient
      .delete<ISuccessHttpResponse<string>>(
        this.createUrl(WebsiteUrls.deleteDomainAlias),
        {
          body: { id },
        }
      )
      .pipe(map((res) => res.data));
  }

  public createDomainAlias(
    payload: ICreateDomainAliasRequest
  ): Observable<IDomainAliasesResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IDomainAliasesResponse>>(
        this.createUrl(WebsiteUrls.createDomainAlias),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public createWebsite(payload: ICreateWebsite): Observable<IWebsiteResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IWebsiteResponse>>(
        this.createUrl(WebsiteUrls.createWebsite),
        payload
      )
      .pipe(map((res) => res.data));
  }
}
