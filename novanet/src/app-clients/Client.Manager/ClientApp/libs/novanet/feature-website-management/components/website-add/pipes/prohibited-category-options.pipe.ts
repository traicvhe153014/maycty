import { Pipe, PipeTransform } from '@angular/core';
import { IProhibitedCategory } from '@shared/sharing/store';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'prohibitedCategoryOptions',
})
export class ProhibitedCategoryOptionsPipe implements PipeTransform {
  transform(
    value: Observable<IProhibitedCategory[]>
  ): Observable<NzSelectOptionInterface[]> {
    return value.pipe(
      map((categories) =>
        categories.map((item) => ({
          value: item.id,
          label: item.name,
        }))
      )
    );
  }
}
