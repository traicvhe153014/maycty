import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteManagementRoutingModule } from './website-management.routing';
import { WebsiteManagementComponent } from './website-management.component';

@NgModule({
  imports: [CommonModule, WebsiteManagementRoutingModule],
  declarations: [WebsiteManagementComponent],
  exports: [WebsiteManagementComponent],
})
export class WebsiteManagementModule {}
