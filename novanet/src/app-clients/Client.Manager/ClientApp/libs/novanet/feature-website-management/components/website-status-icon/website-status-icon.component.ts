import { Component, Input } from '@angular/core';
import {
  WebsiteStatus,
  WebsiteStatusValue,
} from '@features/feature-website-management/store/website-state.model';

@Component({
  selector: 'novanet-website-status-icon',
  templateUrl: './website-status-icon.component.html',
})
export class WebsiteStatusIconComponent {
  @Input() status: WebsiteStatusValue;
  public readonly websiteStatusEnum = WebsiteStatus;
}
