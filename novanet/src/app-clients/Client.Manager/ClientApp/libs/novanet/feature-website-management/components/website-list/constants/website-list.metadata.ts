import { ISettingColumnTable } from '@data-table/models';
import { IPublisherResponse } from '@features/feature-publisher-management/store';
import { WebsiteListEnum } from '@features/feature-website-management/components/website-list/enums';
import { IWebsiteResponse } from '@features/feature-website-management/store/website-state.model';

export const WebsiteListColumnsForAdmin = [
  {
    id: WebsiteListEnum.INDEX,
    title: 'Stt',
    align: 'center',
    width: '60px',
    pinLeft: true,
  },
  {
    id: WebsiteListEnum.IS_ACTIVE,
    title: 'Bật/tắt QC',
    width: '80px',
    pinLeft: true,
  },
  {
    id: WebsiteListEnum.DOMAIN_WEBSITES,
    title: 'Domain website/ URL',
    sort: true,
    name: 'Domain',
    width: '390px',
    pinLeft: true,
  },
  {
    id: WebsiteListEnum.PUBLISHER,
    title: 'Publisher',
    sort: true,
    width: '390px',
    name: 'fullName',
  },
  {
    id: WebsiteListEnum.STATUS,
    title: 'Trạng thái',
    sort: true,
    name: 'Status',
    width: '110px',
  },
  {
    id: WebsiteListEnum.SUMMARY_ZONE_REPORT,
    title: 'SL Vùng',
    width: '90px',
  },
  {
    id: WebsiteListEnum.DOMAIN_ALIASES,
    title: 'Domain Alias',
    width: '390px',
  },
  {
    id: WebsiteListEnum.PROHIBITED_CATEGORY,
    title: 'Sản phẩm cấm',
    width: '390px',
  },
] as ISettingColumnTable<IWebsiteResponse, any>[];

export const WebsiteListColumnsForPublisher = [
  {
    id: WebsiteListEnum.INDEX,
    title: 'Stt',
    align: 'center',
    width: '40px',
    pinLeft: true,
  },
  {
    id: WebsiteListEnum.DOMAIN_WEBSITES,
    title: 'Domain website/ URL',
    sort: true,
    name: 'Domain',
    width: '390px',
    pinLeft: true,
  },
  {
    id: WebsiteListEnum.STATUS,
    title: 'Trạng thái',
    sort: true,
    name: 'Status',
    width: '110px',
  },
  {
    id: WebsiteListEnum.SUMMARY_ZONE_REPORT,
    title: 'SL Vùng',
    width: '90px',
  },
  {
    id: WebsiteListEnum.SUMMARY_CAMPAIGN_RUNNING,
    title: 'Chiến dịch đang chạy',
    width: '150px',
  },
  {
    id: WebsiteListEnum.DOMAIN_ALIASES,
    title: 'Domain Alias',
    width: '390px',
  },
  {
    id: WebsiteListEnum.PROHIBITED_CATEGORY,
    title: 'Sản phẩm cấm',
    width: '390px',
  },
] as ISettingColumnTable<IWebsiteResponse, any>[];
