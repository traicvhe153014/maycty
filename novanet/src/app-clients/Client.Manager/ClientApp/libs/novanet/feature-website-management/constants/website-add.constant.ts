import { IFormFieldErrors, IFormFields } from '@models';
import { domainPattern, urlPattern } from '@shared/sharing/constants';

export const AddWebsiteFormFields: IFormFields = {
  publisherId: {
    name: 'publisherId',
    validationParams: {
      required: true,
    },
  },
  prohibitedCategoryIds: {
    name: 'prohibitedCategoryIds',
    validationParams: {
      required: true,
    },
  },
  domain: {
    name: 'domain',
    validationParams: {
      required: true,
      maxLength: 2000,
      minLength: 1,
      pattern: domainPattern,
    },
  },
  url: {
    name: 'url',
    validationParams: {
      maxLength: 2048,
      required: true,
      pattern: urlPattern,
    },
  },
  domainAliases: {
    name: 'domainAliases',
    validationParams: {
      required: true,
      pattern: /^[a-zA-Z0-9.+#%-_\/:?=]*$/,
    },
  },
};

export const AddWebsiteFormFieldsErrors: IFormFieldErrors = {
  publisherId: [
    {
      type: 'required',
      message: 'Chưa chọn publisher',
    },
  ],
  domain: [
    {
      type: 'required',
      message: 'Chưa nhập Domain website',
    },
    {
      type: 'maxLength',
      message: 'Tối đa 2000 kí tự',
    },
    {
      type: 'minLength',
      message: 'Tối thiểu 1 kí tự',
    },
    {
      type: 'domainExisted',
      message: ' Trùng tên Domain Website ',
    },
    {
      type: 'pattern',
      message: 'Domain Website không hợp lệ',
    },
  ],
  prohibitedCategoryIds: [
    {
      type: 'requiured',
      message: 'Chưa chọn sản phẩm cấm',
    },
  ],
  url: [
    {
      type: 'required',
      message: 'Chưa nhập Url',
    },
    {
      type: 'maxLength',
      message: 'Tối đa 2048 kí tự',
    },
    {
      type: 'urlExisted',
      message: 'Url đã tồn tại',
    },
    {
      type: 'pattern',
      message: 'Url không hợp lệ',
    },
  ],
  domainAliases: [
    {
      type: 'required',
      message: 'Chưa nhập Domain Alias',
    },
    {
      type: 'domainAliasesExisted',
      message: 'Domain Alias đã tồn tại',
    },
  ],
};

export const AddDomainAliasFormFieldsErrors: IFormFieldErrors = {
  name: [
    {
      type: 'required',
      message: 'Chưa nhập Domain Alias',
    },
    {
      type: 'domainAliasesExisted',
      message: 'Domain Alias đã tồn tại',
    },
    {
      type: 'duplicated',
      message: 'Domain Alias không trùng nhau',
    },
    {
      type: 'pattern',
      message: 'Domain Alias không hợp lệ',
    },
  ],
};
