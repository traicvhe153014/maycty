import {
    WebsiteStatusIconComponent
} from "@features/feature-website-management/components/website-status-icon/website-status-icon.component";
import {CommonModule} from "@angular/common";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NgModule} from "@angular/core";

@NgModule({
    declarations: [WebsiteStatusIconComponent],
    imports: [CommonModule, NzToolTipModule],
    exports: [WebsiteStatusIconComponent],
})
export class WebsiteStatusIconModule {}