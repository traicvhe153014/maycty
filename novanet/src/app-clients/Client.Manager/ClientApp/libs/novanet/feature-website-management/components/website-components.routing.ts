import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        loadChildren: () =>
          import('./website-list/website-list.module').then(
            (m) => m.WebsiteListModule
          ),
      },
      {
        path: 'add',
        loadChildren: () =>
            import('./website-add/website-add.module').then(
                (m) => m.WebsiteAddModule
            ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebsiteComponentsRoutingModule {}
