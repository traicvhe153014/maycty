import {
  ICreateDomainAliasRequest,
  ICreateWebsite,
  IDomainAlias,
  IDomainAliasesResponse,
  IListWebsiteRequest,
  IListWebsiteResponse,
  IUpdateWebsiteProhibitedCategoryRequest,
  IUpdateWebsiteRequest,
  IWebsiteProhibitedCategoryResponse,
  IWebsiteResponse,
  WebsiteStatusValue,
} from '@features/feature-website-management/store/website-state.model';

const enum WebsiteActions {
  GetWebsiteList = '[Website] Get List Website',
  GetWebsiteListSuccess = '[Website] Get List Website Success',
  GetWebsiteListError = '[Website] Get List Website Error',
  UpdateWebsite = '[Website] Get Update Website',
  UpdateWebsiteSuccess = '[Website] Update Website Success',
  UpdateWebsiteError = '[Website] Update Website Error',
  GetStatusWebsite = '[Website] Get Status Website',
  ClearAllWebsitesData = '[Website] Clear All Websites',
  GetMoreWebsiteList = '[Website] Get More Websites',
  GetMoreWebsiteListSuccess = '[Website] Get More Websites Success',
  GetMoreWebsiteListError = '[Website] Get More Websites Error',
  GetListProhibitedCategories = '[Website] Get List Categories',
  UpdateWebsiteProhibitedCategory = '[Website] Get Update Categories',
  UpdateWebsiteProhibitedCategorySuccess = '[Website] Update Categories Success',
  UpdateWebsiteProhibitedCategoryError = '[Website] Update Categories Error',
  CreateDomainAlias = '[Website] Create Domain Alias',
  CreateDomainAliasSuccess = '[Website] Create Domain Alias Success',
  CreateDomainAliasError = '[Website] Create Domain Alias Error',
  UpdateDomainAlias = '[Website] Update Domain Alias',
  UpdateDomainAliasSuccess = '[Website] Update Domain Alias Success',
  UpdateDomainAliasError = '[Website] Update Domain Alias Error',
  GetListDomainAlias = '[Website] Get Domain Alias',
  GetListDomainAliasSuccess = '[Website] Get Domain Alias Success',
  GetListDomainAliasError = '[Website] Get Domain Alias Error',
  GetIsCallAPI = '[Website] Is Call API',
  DeleteDomainAlias = '[Website] Delete Domain Alias',
  DeleteDomainAliasSuccess = '[Website] Delete Domain Alias Success',
  DeleteDomainAliasError = '[Website] Delete Domain Alias Error',
  GetPublishersId = '[Website] Get Publisher Id',
  GetWebsiteId = '[Website] Get Website id',
  CreateWebsite = '[Website] Create',
  CreateWebsiteSuccess = '[Website] Create Website Success',
  CreateWebsiteError = '[Website] Create Website Error',
}

export class GetWebsiteList {
  public static readonly type = WebsiteActions.GetWebsiteList;

  constructor(public payload: IListWebsiteRequest) {}
}

export class GetWebsiteListSuccess {
  public static readonly type = WebsiteActions.GetWebsiteListSuccess;

  constructor(
    public response: IListWebsiteResponse,
    public page: number,
    public pageSize: number
  ) {}
}

export class GetWebsiteListError {
  public static readonly type = WebsiteActions.GetWebsiteListError;

  constructor(public error: any) {}
}

export class UpdateWebsite {
  public static readonly type = WebsiteActions.UpdateWebsite;

  constructor(public payload: IUpdateWebsiteRequest) {}
}

export class UpdateWebsiteSuccess {
  public static readonly type = WebsiteActions.UpdateWebsiteSuccess;

  constructor(public websiteResponse: IWebsiteResponse) {}
}

export class UpdateWebsiteError {
  public static readonly type = WebsiteActions.UpdateWebsiteError;

  constructor(public error: any) {}
}

export class GetStatusWebsite {
  public static readonly type = WebsiteActions.GetStatusWebsite;

  constructor(public status: WebsiteStatusValue) {}
}

export class ClearAllWebsitesData {
  public static readonly type = WebsiteActions.ClearAllWebsitesData;
}

export class GetMoreWebsiteList {
  public static readonly type = WebsiteActions.GetMoreWebsiteList;

  constructor(public payload: IListWebsiteRequest) {}
}

export class GetMoreWebsiteListSuccess {
  public static readonly type = WebsiteActions.GetMoreWebsiteListSuccess;

  constructor(
    public response: IListWebsiteResponse,
    public page: number,
    public pageSize: number
  ) {}
}

export class GetMoreWebsiteListError {
  public static readonly type = WebsiteActions.GetMoreWebsiteListError;

  constructor(public error: any) {}
}

export class GetListProhibitedCategories {
  public static readonly type = WebsiteActions.GetListProhibitedCategories;

  constructor() {}
}

export class UpdateWebsiteProhibitedCategory {
  public static readonly type = WebsiteActions.UpdateWebsiteProhibitedCategory;

  constructor(public payload: IUpdateWebsiteProhibitedCategoryRequest) {}
}

export class UpdateWebsiteProhibitedCategorySuccess {
  public static readonly type =
    WebsiteActions.UpdateWebsiteProhibitedCategorySuccess;

  constructor(
    public ProhibitedCategoryResponse: IWebsiteProhibitedCategoryResponse[]
  ) {}
}

export class UpdateWebsiteProhibitedCategoryError {
  public static readonly type =
    WebsiteActions.UpdateWebsiteProhibitedCategoryError;

  constructor(public error: any) {}
}

export class CreateDomainAlias {
  public static readonly type = WebsiteActions.CreateDomainAlias;

  constructor(public payload: ICreateDomainAliasRequest) {}
}

export class CreateDomainAliasSuccess {
  public static readonly type = WebsiteActions.CreateDomainAliasSuccess;

  constructor(public domainAliasResponse: IDomainAliasesResponse) {}
}

export class CreateDomainAliasError {
  public static readonly type = WebsiteActions.CreateDomainAliasError;

  constructor(public error: any) {}
}

export class DeleteDomainAlias {
  public static readonly type = WebsiteActions.DeleteDomainAlias;

  constructor(public payload: IDomainAlias) {}
}

export class DeleteDomainAliasSuccess {
  public static readonly type = WebsiteActions.DeleteDomainAliasSuccess;

  constructor(public domainAliasId: string) {}
}

export class DeleteDomainAliasError {
  public static readonly type = WebsiteActions.DeleteDomainAliasError;

  constructor(public error: any) {}
}

export class UpdateDomainAlias {
  public static readonly type = WebsiteActions.UpdateDomainAlias;

  constructor(public payload: IDomainAlias) {}
}

export class UpdateDomainAliasSuccess {
  public static readonly type = WebsiteActions.UpdateDomainAliasSuccess;

  constructor(public domainAliasResponse: IDomainAliasesResponse) {}
}

export class UpdateDomainAliasError {
  public static readonly type = WebsiteActions.UpdateDomainAliasError;

  constructor(public error: any) {}
}

export class GetListDomainAlias {
  public static readonly type = WebsiteActions.GetListDomainAlias;

  constructor(public websiteId: string) {}
}

export class GetListDomainAliasSuccess {
  public static readonly type = WebsiteActions.GetListDomainAliasSuccess;

  constructor(public DomainAliasResponse: IDomainAliasesResponse[]) {}
}

export class GetListDomainAliasError {
  public static readonly type = WebsiteActions.GetListDomainAliasError;

  constructor(public error: any) {}
}

export class GetIsCallAPI {
  public static readonly type = WebsiteActions.GetIsCallAPI;

  constructor(public status: boolean) {}
}

export class GetPublishersId {
  public static readonly type = WebsiteActions.GetPublishersId;

  constructor(public publishersId: string) {}
}

export class GetWebsiteId {
  public static readonly type = WebsiteActions.GetWebsiteId;

  constructor(public websiteId: string) {}
}

export class CreateWebsite {
  public static readonly type = WebsiteActions.CreateWebsite;

  constructor(public payload: ICreateWebsite) {}
}

export class CreateWebsiteSuccess {
  public static readonly type = WebsiteActions.CreateWebsiteSuccess;

  constructor(public websiteResponse: IWebsiteResponse) {}
}

export class CreateWebsiteError {
  public static readonly type = WebsiteActions.CreateWebsiteError;

  constructor(public error: any) {}
}
