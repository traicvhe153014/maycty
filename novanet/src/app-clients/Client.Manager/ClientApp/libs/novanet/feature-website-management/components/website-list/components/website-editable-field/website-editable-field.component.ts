import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {
  IDomainAliasesResponse,
  IUpdateWebsiteRequest,
  IWebsiteResponse,
} from '@features/feature-website-management/store/website-state.model';
import { Select, Store } from '@ngxs/store';
import {
  DeleteDomainAlias,
  GetIsCallAPI,
  UpdateDomainAlias,
  UpdateWebsite,
} from '@features/feature-website-management/store/website-state.actions';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { TypeEdit } from '@features/feature-website-management/components/website-list/enums';
import { WebsiteState } from '@features/feature-website-management/store/website.state';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { domainPattern } from '@shared/sharing/constants';

@Component({
  selector: 'novanet-website-editable-field',
  templateUrl: './website-editable-field.component.html',
  styleUrls: ['./website-editable-field.component.scss'],
})
export class WebsiteEditableFieldComponent implements OnInit {
  @Input() public website: IWebsiteResponse;
  @Input() public websiteId: string;
  @Input() public isAnySelected: boolean;
  @Input() public fieldName: string;
  @Input() public typeEdit: TypeEdit;
  @Input() public domainAlias: IDomainAliasesResponse;
  @Input() public publisherId: string;

  @Select(WebsiteState.getIsCallAPI) public isCallAPI$: Observable<boolean>;

  @Output() domainChange = new EventEmitter<{ id: string; value: string }>();

  public isEditing = false;
  public fieldEditing: string;
  public maxLengthInput: number;
  public minLengthInput: number;
  public isLoadingEditable: { [key: string]: boolean } = {};
  public iconEdit: { [key: string]: boolean } = {};
  public TypeEdit = TypeEdit;
  public valueBefore: string;
  public valueAfter: string;
  public notificationMessagesError: string;
  public isValueError = false;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (this.domainAlias !== undefined) {
      this.valueBefore = this.domainAlias.name;
      this.valueAfter = this.domainAlias.name;
    }
  }

  public setEditing() {
    this.isEditing = true;
    this.fieldEditing = this.website[this.fieldName];
  }

  public cancelEditing() {
    this.isEditing = false;
  }

  public saveEditing() {
    this.isLoadingEditable[this.website.id] = true;

    if (this.fieldName === 'domain') {
      if (this.fieldEditing.length < 1) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Domain website phải nhập tối thiểu 1 kí tự'
          )
        );
        this.isLoadingEditable[this.website.id] = false;
        return;
      }
      if (this.fieldEditing.length > 2000) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Domain website chỉ có thể nhập tối đa 2000 kí tự'
          )
        );
        this.isLoadingEditable[this.website.id] = false;
        return;
      }
      if (/https?:\/\//.test(this.fieldEditing)) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            `Nhập chính xác domain ( không cần nhập https:// )`
          )
        );
        this.isLoadingEditable[this.website.id] = false;
        return;
      }
      if (!domainPattern.test(this.fieldEditing)) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            `Domain Website không hợp lệ`
          )
        );
        this.isLoadingEditable[this.website.id] = false;
        return;
      }
      const payload: IUpdateWebsiteRequest = {
        id: this.website.id,
        domain: this.fieldEditing,
      };
      this.store.dispatch(new UpdateWebsite(payload)).subscribe({
        next: (response) => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công'
            )
          );
          this.isEditing = false;
          this.isLoadingEditable[this.website.id] = false;
          this.domainChange.emit({
            id: this.website.id,
            value: this.fieldEditing,
          });
          this.changeDetectorRef.detectChanges();
        },
        error: (error) => {
          if (error.error.errors[0]?.includes('Domain đã tồn tại')) {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.error,
                'Domain đã tồn tại'
              )
            );
          } else {
            this.store.dispatch(
              new ShowGlobalNotification(
                GlobalNotificationEnum.error,
                'Có lỗi xảy ra'
              )
            );
          }
          this.isLoadingEditable[this.website.id] = false;
          this.changeDetectorRef.detectChanges();
        },
      });
    }
  }

  public changeStatusIsCallAPI(val: boolean) {
    this.store.dispatch(new GetIsCallAPI(val));
  }

  public focusInput() {
    this.iconEdit[this.domainAlias.id] = true;
  }

  public cancelEditingDomainAlias() {
    this.isValueError = false;
    this.valueAfter = this.valueBefore;
    this.iconEdit[this.domainAlias.id] = false;
  }

  public saveEditingDomainAlias() {
    this.changeStatusIsCallAPI(true);
    this.valueAfter = this.valueAfter.trim();
    if (this.valueAfter.length < 1 || this.valueAfter.length > 2000) {
      this.onNotificationError('Sai định dạng');
      this.changeStatusIsCallAPI(false);
      return;
    }
    if (!/^[a-zA-Z0-9.+#%-_\/:?=]*$/.test(this.valueAfter)) {
      this.onNotificationError('Sai định dạng');
      this.changeStatusIsCallAPI(false);
      return;
    }
    this.store
      .dispatch(
        new UpdateDomainAlias({
          id: this.domainAlias.id,
          name: this.valueAfter,
          websiteId: this.websiteId,
          publisherId: this.publisherId,
        })
      )
      .subscribe({
        next: (response) => {
          this.valueBefore = this.valueAfter;
          this.changeStatusIsCallAPI(false);
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công'
            )
          );
          this.changeDetectorRef.detectChanges();
        },
        error: (error) => {
          if (error.error.errors[0]?.includes('Domain đã tồn tại')) {
            this.onNotificationError('Domain đã tồn tại');
          } else {
            this.onNotificationError('Có lỗi xảy ra');
          }
          this.changeStatusIsCallAPI(false);
        },
      });
  }

  public onNotificationError(message: string) {
    this.isValueError = true;
    this.notificationMessagesError = message;
  }

  public deleteDomainAlias() {
    this.isCallAPI$.pipe(take(1)).subscribe((isCallAPI) => {
      if (isCallAPI) {
        return;
      }
    });
    const domainAliasRequest = {
      id: this.domainAlias.id,
      websiteId: this.domainAlias.websiteId,
      name: this.domainAlias.name,
    };
    this.store.dispatch(new DeleteDomainAlias(domainAliasRequest)).subscribe({
      next: (response) => {
        this.changeStatusIsCallAPI(false);
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.onNotificationError('Có lỗi xảy ra');
        this.changeStatusIsCallAPI(false);
      },
    });
  }
}
