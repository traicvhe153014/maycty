import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { WebsiteState } from '@features/feature-website-management/store/website.state';
import {
  IDomainAliasesResponse,
  IUpdatedDomainAliasData,
  IUpdateWebsiteRequest,
  IWebsiteResponse,
  IWebsiteStateModel,
  WebsiteStatusValue,
} from '@features/feature-website-management/store/website-state.model';
import {
  WebsiteListColumnsForAdmin,
  WebsiteListColumnsForPublisher,
} from '@features/feature-website-management/components/website-list/constants/website-list.metadata';
import {
  TypeEdit,
  WebsiteListEnum,
  WebsiteSearchLevelEnum,
} from '@features/feature-website-management/components/website-list/enums';
import {
  WebsiteListSettingTable,
  websiteSearchLevelFields,
  websiteSearchLevelLabels,
  websiteSearchOperatorLabels,
  websiteStatusOptions,
} from '@features/feature-website-management/components/website-list/constants/website-list.constant';
import {
  ClearAllWebsitesData,
  CreateDomainAlias,
  GetIsCallAPI,
  GetListDomainAlias,
  GetListProhibitedCategories,
  GetMoreWebsiteList,
  GetPublishersId,
  GetStatusWebsite,
  GetWebsiteId,
  GetWebsiteList,
  UpdateWebsite,
  UpdateWebsiteProhibitedCategory,
} from '@features/feature-website-management/store/website-state.actions';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { IFieldDetail } from '@data-table/models';
import { ESortType, PredicateOperatorEnum } from '@core/enums';
import { IWebsiteSearchOption } from '@features/feature-website-management/components/website-list/type/website-group.type';
import { take, takeUntil } from 'rxjs/operators';
import { IProhibitedCategory } from '@shared/sharing/store';
import { User } from '@models';
import { AuthService } from '@features/auth/store';
import { cloneDeep } from 'lodash';
import { storage } from '@core';
import { IPredicateModel } from '@core/models';

@Component({
  selector: 'novanet-website-list',
  templateUrl: './website-list.component.html',
  styleUrls: ['./website-list.component.scss'],
})
export class WebsiteListComponent implements OnInit, OnDestroy {
  @Select(WebsiteState.getLoading) public loading$: Observable<boolean>;
  @Select(WebsiteState.getList) public websites$: Observable<
    IWebsiteResponse[]
  >;
  @Select(WebsiteState.getTotal) public total$: Observable<number>;
  @Select(WebsiteState.getStatus)
  public status$: Observable<WebsiteStatusValue>;
  @Select(WebsiteState.getPage) public currentPage$: Observable<number>;
  @Select(WebsiteState.getListProhibitedCategories)
  public listProhibitedCategories$: Observable<IProhibitedCategory[]>;
  @Select(WebsiteState.getListDomainAlias) public listDomainAlias$: Observable<
    IDomainAliasesResponse[]
  >;
  @Select(WebsiteState.getIsCallAPI) public isCallAPI$: Observable<boolean>;
  @Select(WebsiteState.getPublishersId)
  public PublishersId$: Observable<string>;
  @Select(WebsiteState.getUpdatedDomainAliasData)
  public updatedDomainAliasData$: Observable<IUpdatedDomainAliasData>;
  @Select(WebsiteState.getShouldShowNoData)
  public shouldShowNoData$: Observable<boolean>;

  public websites: IWebsiteResponse[] = [];
  public columns: any = WebsiteListColumnsForAdmin;
  public readonly websiteListEnum = WebsiteListEnum;
  public activeChangeStatus: boolean;
  public activeDataWebsite: IWebsiteResponse;
  public websiteLoadings: { [key: string]: boolean } = {};
  public readonly settings = WebsiteListSettingTable;
  public readonly pageSize = 22;
  public sorts = ['status,-subid'];
  public readonly baseSorts = ['status,-subid'];
  public messageConfirm: string;
  public isConfirmActiveChangeModalVisible = false;
  public filterPredicates = [];
  public filterStatus: WebsiteStatusValue | null = null;
  public websiteStatusOptions = websiteStatusOptions;
  public selectedSearchOptions: any[] = [];
  public searchOptions = [];
  public searchDropdownOpen = false;
  public websiteSearchLevelLabels = websiteSearchLevelLabels;
  public websiteSearchOperatorLabels = websiteSearchOperatorLabels;
  public listProhibitedCategories: IProhibitedCategory[] = [];
  public isLoadingDomainAlias = false;
  public isShowModalDomainAlias = false;
  public isAddDomainAlias = false;
  public websiteIdEdit: string;
  public typeEdit: TypeEdit;
  public typeEditDomain = TypeEdit;
  public valueDomainAlias: string = null;
  public isValueError = false;
  public notificationMessagesError: string;
  public publishersId: string = null;
  public user: User;
  public userPolicies: string[] = [];
  public valueCategory: IProhibitedCategory[] = [];
  public typeTileModal: string = 'Domain Alias';
  public withDomainAlias: string = 'w-[330px]';
  public lengthDomainAlias: number = 34;
  private isUpdated = false;
  public ListSelectedReportOptions: any[];
  public searchStorage = 'SearchOptionsWebsite';
  public statusStorage = 'StatusWebsite';
  public isFocus = false;
  public websiteSearchLevelFields = websiteSearchLevelFields;
  public websiteSearchLevelEnum = WebsiteSearchLevelEnum;
  public publisherIdEdit: string;

  private readonly destroy$: Subject<void> = new Subject<void>();

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    let options = storage.get(this.searchStorage) as IWebsiteSearchOption[];
    if (options) {
      this.selectedSearchOptions = options.map((item) => ({
        value: item.value,
        operator: item.operator,
        level: item.field as Number,
      }));
      this.searchOptions = options;
      this.filterPredicates = this.selectedSearchOptions;
    }
    this.filterStatus = storage.get(this.statusStorage) as WebsiteStatusValue;
    this.store.dispatch(new GetStatusWebsite(this.filterStatus));
    this.user = this.authService.getUserData();
    this.userPolicies = this.authService.getUserPolicies(this.user);
    if (this.user.roles === 'Publisher') {
      this.columns = WebsiteListColumnsForPublisher;
      this.withDomainAlias = 'w-[420px]';
      this.lengthDomainAlias = 47;
    }
    this.loadWebsites();

    this.store.dispatch(new GetListProhibitedCategories());
    this.listProhibitedCategories$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data) => {
        this.listProhibitedCategories = data;
      });

    this.updatedDomainAliasData$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data) => {
        const index = this.websites.findIndex((x) => x.id === data.id);
        if (index > -1) {
          this.websites[index].domainAliases = data.value;
          this.changeDetectorRef.detectChanges();
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new GetPublishersId(null));
    this.store.dispatch(new ClearAllWebsitesData());
  }

  public onIsActiveChange(event: boolean, data: IWebsiteResponse) {
    this.activeChangeStatus = event;
    this.activeDataWebsite = data;
    if (this.websiteLoadings[this.activeDataWebsite.id]) {
      return;
    }
    this.websiteLoadings[this.activeDataWebsite.id] = true;
    this.messageConfirm =
      'Bạn đang thay đổi trạng thái điều này có thể ảnh hưởng đến việc hiển thị quảng cáo trên website. Bạn vẫn muốn thay đổi?';
    this.isConfirmActiveChangeModalVisible = true;
  }

  public handleCancelConfirmChangeActive() {
    this.isConfirmActiveChangeModalVisible = false;
    this.isConfirmActiveChangeModalVisible = false;
    this.websiteLoadings[this.activeDataWebsite.id] = false;
  }

  public clickConfirmChangeActive() {
    const payload: IUpdateWebsiteRequest = {
      id: this.activeDataWebsite.id,
      isActive: this.activeChangeStatus,
    };
    this.store.dispatch(new UpdateWebsite(payload)).subscribe({
      next: (response) => {
        this.isConfirmActiveChangeModalVisible = false;
        this.websiteLoadings[this.activeDataWebsite.id] = false;
        const { websites } = response.website as IWebsiteStateModel;
        const index = this.websites.findIndex((x) => x.id === payload.id);
        const updatedWebsite = websites.find((x) => x.id === payload.id);
        if (index > -1) {
          this.websites[index].active = updatedWebsite?.active ?? false;
          this.websites[index].status = updatedWebsite?.status;
        }
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
        this.changeDetectorRef.detectChanges();
      },
      error: (error) => {
        this.isConfirmActiveChangeModalVisible = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.websiteLoadings[this.activeDataWebsite.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public searchWebsites(values?: IFieldDetail[]) {
    if (values.length) {
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.loadWebsites();
  }

  public onFilterStatusChange(status: WebsiteStatusValue) {
    this.filterStatus = status;
    this.store.dispatch(new GetStatusWebsite(status));
    storage.set(this.statusStorage, this.filterStatus);
    this.loadWebsites();
  }

  public filterPredicateComparator(option1: any, option2: any) {
    return (
      option1?.field === option2?.field &&
      option1?.value === option2?.value &&
      option1?.operator === option2?.operator
    );
  }

  public onNextPage() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store
          .dispatch(
            new GetMoreWebsiteList({
              page: currentPage + 1,
              pageSize: this.pageSize,
              filters: this.filterPredicates,
              sorts: this.sorts,
              publisherId: this.publishersId,
            })
          )
          .pipe(take(1))
          .subscribe({
            next: (data) => {
              this.websites = cloneDeep(data.website.websites);
              this.changeDetectorRef.detectChanges();
            },
          });
      },
    });
  }

  public setEditCategory(val: { optionsId: string; listValueKey: string[] }) {
    this.store.dispatch(new GetWebsiteId(val.optionsId));
    this.store
      .dispatch(
        new UpdateWebsiteProhibitedCategory({
          websiteId: val.optionsId,
          prohibitedCategoriesId: val.listValueKey,
        })
      )
      .subscribe({
        next: (response) => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công'
            )
          );
          const { websites } = response.website as IWebsiteStateModel;
          const index = this.websites.findIndex((x) => x.id === val.optionsId);
          const updatedWebsite = websites.find((x) => x.id === val.optionsId);
          if (index > -1) {
            this.websites[index].prohibitedCategory =
              updatedWebsite?.prohibitedCategory ?? [];
          }
          this.changeDetectorRef.detectChanges();
        },
        error: (error) => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              'Có lỗi xảy ra'
            )
          );
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public onListEditDomainAlias(websiteId: string, publisherId: string) {
    this.websiteIdEdit = websiteId;
    this.publisherIdEdit = publisherId;
    this.isLoadingDomainAlias = true;
    this.typeEdit = TypeEdit.EDIT_DOMAIN_ALIAS;
    this.store.dispatch(new GetListDomainAlias(websiteId)).subscribe({
      next: (response) => {
        this.isLoadingDomainAlias = false;
      },
      error: (response) => {
        this.isLoadingDomainAlias = false;
      },
    });
    this.isShowModalDomainAlias = true;
  }

  public cancelModalDomainAlias() {
    this.isLoadingDomainAlias = false;
    this.isShowModalDomainAlias = false;
    this.isAddDomainAlias = false;
    this.valueDomainAlias = null;
    this.isValueError = false;
  }

  public changeIsAddDomainAlias() {
    this.isAddDomainAlias = true;
  }

  public cancelAddDomainAlias() {
    this.isAddDomainAlias = false;
    this.valueDomainAlias = null;
    this.isValueError = false;
  }

  public changeStatusIsCallAPI(val: boolean) {
    this.store.dispatch(new GetIsCallAPI(val));
  }

  public onNotificationError(message: string) {
    this.isValueError = true;
    this.notificationMessagesError = message;
  }

  public saveAddDomainAlias() {
    this.changeStatusIsCallAPI(true);
    this.valueDomainAlias !== null
      ? (this.valueDomainAlias = this.valueDomainAlias.trim())
      : '';
    if (
      this.valueDomainAlias === '' ||
      this.valueDomainAlias === null ||
      this.valueDomainAlias.length > 2000
    ) {
      this.onNotificationError('Sai định dạng');

      this.changeStatusIsCallAPI(false);
      return;
    }
    if (!/^[a-zA-Z0-9.+#%-_\/:?=]*$/.test(this.valueDomainAlias)) {
      this.onNotificationError('Sai định dạng');
      this.changeStatusIsCallAPI(false);
      return;
    }
    this.store
      .dispatch(
        new CreateDomainAlias({
          name: this.valueDomainAlias,
          websiteId: this.websiteIdEdit,
          publisherId: this.publisherIdEdit,
        })
      )
      .subscribe({
        next: (response) => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Thành công'
            )
          );
          const { websites } = response.website as IWebsiteStateModel;
          const index = this.websites.findIndex(
            (x) => x.id === this.websiteIdEdit
          );
          const updatedWebsite = websites.find(
            (x) => x.id === this.websiteIdEdit
          );
          if (index > -1) {
            this.websites[index].domainAliases =
              updatedWebsite?.domainAliases ?? [];
          }
          this.isValueError = false;
          this.valueDomainAlias = null;
          this.changeStatusIsCallAPI(false);
          this.changeDetectorRef.detectChanges();
        },
        error: (error) => {
          if (error.error.errors[0]?.includes('Domain đã tồn tại')) {
            this.onNotificationError('Domain đã tồn tại');
          } else {
            this.onNotificationError('Có lỗi xảy ra');
          }
          this.changeStatusIsCallAPI(false);
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public expand(websiteId: string) {
    this.typeTileModal = 'Domain Alias';
    this.websiteIdEdit = websiteId;
    this.isLoadingDomainAlias = true;
    this.typeEdit = TypeEdit.SHOW_DOMAIN_ALIAS;
    this.store.dispatch(new GetListDomainAlias(websiteId)).subscribe({
      next: (response) => {
        this.isLoadingDomainAlias = false;
      },
      error: (response) => {
        this.isLoadingDomainAlias = false;
      },
    });
    this.isShowModalDomainAlias = true;
  }

  public countValueCategory(val: IProhibitedCategory[]): boolean {
    let Value: string;
    val.forEach((value) => {
      Value = Value + value.name;
    });
    if (Value === undefined) {
      return false;
    }
    return Value.length > 140;
  }

  public showCategory(val: IProhibitedCategory[]) {
    this.typeTileModal = 'Sản phẩm cấm';
    this.typeEdit = TypeEdit.SHOW_CATEGORY;
    if (val !== undefined) {
      this.valueCategory = val;
      this.isShowModalDomainAlias = true;
    }
  }

  public onDomainChange(event: { id: string; value: string }) {
    const index = this.websites.findIndex((x) => x.id === event.id);
    if (index > -1) {
      this.websites[index].domain = event.value;
      this.changeDetectorRef.detectChanges();
    }
  }

  public showZoneList(idWebsite: string) {
    const queryParams = {
      websiteId: idWebsite,
    };
    this.router.navigate(['/zone/list'], { queryParams });
  }

  private loadWebsites() {
    this.store
      .dispatch(
        new GetWebsiteList({
          page: 1,
          pageSize: this.pageSize,
          filters: this.filterPredicates,
          sorts: this.sorts,
          publisherId: this.publishersId,
        })
      )
      .pipe(take(1))
      .subscribe({
        next: (data) => {
          this.websites = cloneDeep(data.website.websites);
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public onSearchOptions(options: IPredicateModel[]) {
    this.isUpdated = false;
    this.filterPredicates = options.map((option) => ({
      value: option.value,
      operator: option.operator,
      field: Number(option.field),
    }));
    storage.set(this.searchStorage, this.filterPredicates);
    this.loadWebsites();
  }

  public onFocusSearch() {
    this.isFocus = true;
  }

  public onBlurSearch() {
    this.isFocus = false;
  }
}
