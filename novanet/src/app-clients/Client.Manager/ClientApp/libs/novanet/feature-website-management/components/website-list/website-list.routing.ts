import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    WebsiteListComponent
} from "@features/feature-website-management/components/website-list/website-list.component";

const routes: Routes = [
    {
        path: '',
        component: WebsiteListComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class WebsiteListRoutingModule {}