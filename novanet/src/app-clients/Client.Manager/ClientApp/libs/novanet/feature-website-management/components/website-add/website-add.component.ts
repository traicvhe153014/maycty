import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import {
  AddDomainAliasFormFieldsErrors,
  AddWebsiteFormFields,
  AddWebsiteFormFieldsErrors,
} from '@features/feature-website-management/constants/website-add.constant';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute, Router } from '@angular/router';
import { IFormFieldErrors, IFormFields } from '@models';
import {
  GetMorePublisherList,
  GetPublisherList,
  IPublisherResponse,
  PublisherState,
} from '@features/feature-publisher-management/store';
import { EMPTY, expand, Observable, switchMap } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  IProhibitedCategory,
  ListProhibitedCategory,
  SharingState,
} from '@shared/sharing/store';
import { WebsiteState } from '@features/feature-website-management/store/website.state';
import {
  ICreateWebsite,
  IWebsiteStateModel,
} from '@features/feature-website-management/store/website-state.model';
import { CreateWebsite } from '@features/feature-website-management/store/website-state.actions';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { ErrorType } from '@features/feature-website-management/components/website-list/enums';
import { PublisherSearchLevelEnum } from '@features/feature-publisher-management/components/publisher-list/enums';
import { PredicateOperatorEnum } from '@core/enums';

@Component({
  selector: 'novanet-website-add',
  templateUrl: './website-add.component.html',
})
export class WebsiteAddComponent implements OnInit {
  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;
  @Select(PublisherState.getList) public publishers$: Observable<
    IPublisherResponse[]
  >;
  @Select(SharingState.getProhibitedCategories)
  public prohibitedCategories$: Observable<IProhibitedCategory[]>;
  @Select(WebsiteState.getIsCallAPI) public isCallAPI$: Observable<boolean>;
  @Select(PublisherState.getPage)
  public currentPublisherPage$: Observable<number>;
  public createForm: FormGroup;
  public readonly addWebsiteFormFields = AddWebsiteFormFields;
  public readonly addWebsiteFormFieldsErrors = AddWebsiteFormFieldsErrors;
  public readonly addDomainAliasFormFieldsErrors =
    AddDomainAliasFormFieldsErrors;
  public publisherKeyword = '';
  private readonly publisherPageSize = 24;
  private publisherPage = 1;
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private store: Store,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  public get domainAliasesForm() {
    return this.createForm.get('domainAliases') as FormArray;
  }

  public domainAliasFormGroup(index: number) {
    return this.domainAliasesForm.at(index) as FormGroup;
  }

  ngOnInit() {
    const publisherId =
      this.activatedRoute.snapshot.queryParamMap.get('publisherId');

    this.store
      .dispatch(
        new GetPublisherList({
          page: 1,
          pageSize: this.publisherPageSize,
          filters: [],
        })
      )
      .pipe(
        expand((data) => {
          if (
            !publisherId ||
            data.publisher.publishers.find((item) => item.id === publisherId)
          ) {
            return EMPTY;
          }
          return this.currentPublisherPage$.pipe(
            take(1),
            switchMap((page) =>
              this.store.dispatch(
                new GetMorePublisherList({
                  page: page + 1,
                  pageSize: this.publisherPageSize,
                  filters: [],
                })
              )
            )
          );
        })
      )
      .subscribe(() => {});

    this.store.dispatch(new ListProhibitedCategory());

    this.buildForm();

    if (!!publisherId) {
      this.createForm.patchValue({ publisherId });
    }
  }

  public loadMorePublishers() {
    this.currentPublisherPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMorePublisherList({
            page: currentPage + 1,
            pageSize: this.publisherPageSize,
            filters: [],
          })
        );
      },
    });
  }

  public onSearchPublisher(event: string) {
    this.publisherKeyword = event;
    this.publisherPage = 1;
    this.store.dispatch(
      new GetPublisherList({
        page: this.publisherPage,
        pageSize: this.publisherPageSize,
        filters: [
          {
            value: this.publisherKeyword,
            field: PublisherSearchLevelEnum.EMAIL,
            operator: PredicateOperatorEnum.Contains,
          },
        ],
      })
    );
  }

  public addNewDomainAlias() {
    this.domainAliasesForm.push(
      this.formBuilder.group({
        name: [''],
      })
    );
  }

  public onsubmit(shouldCreate: boolean) {
    this.createForm.markAllAsTouched();

    if (!this.createForm.valid) {
      return;
    }

    const data = this.createForm.value;
    const payload: ICreateWebsite = {
      ...data,
      publisherId: data.publisherId,
      domainAliases: data.domainAliases
        .map((item) => item.name)
        .filter((item) => !!item),
    };
    this.store.dispatch(new CreateWebsite(payload)).subscribe({
      next: (state) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Website được tạo mới thành công'
          )
        );
        const createdWebsiteId = (state.website as IWebsiteStateModel)
          .createdWebsiteId;
        const queryParams = {
          websiteId: createdWebsiteId,
        };
        if (shouldCreate) {
          this.router.navigate(['/zone/add'], { queryParams });
        } else {
          this.router.navigate(['/website/list']);
        }
      },
      error: (err) => {
        const errorMessages = JSON.parse(err.error.errors[0]);
        for (const errorMessage of errorMessages) {
          switch (errorMessage.ErrorType) {
            case ErrorType.DOMAIN_EXISTED:
              this.createForm.get('domain').setErrors({ domainExisted: true });
              this.changeDetectorRef.detectChanges();
              break;
            case ErrorType.URL_EXISTED:
              this.createForm.get('url').setErrors({ urlExisted: true });
              this.changeDetectorRef.detectChanges();
              break;
            case ErrorType.DOMAIN_ALIASES_EXISTED:
              const domainAlias = errorMessage.Value;
              const aliasForm = this.domainAliasesForm.controls.find(
                (form) => form.get('name').value === domainAlias
              );
              if (!!aliasForm) {
                aliasForm.get('name').setErrors({ domainAliasesExisted: true });
              }
              break;
          }
        }
        this.changeDetectorRef.detectChanges();
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Tạo Website mới thất bại'
          )
        );
      },
    });
  }

  public deleteInput(index: number) {
    this.domainAliasesForm.removeAt(index);
  }

  private buildForm() {
    const config = {
      [this.addWebsiteFormFields.publisherId.name]: ['', [Validators.required]],
      [this.addWebsiteFormFields.prohibitedCategoryIds.name]: [[], []],
      [this.addWebsiteFormFields.domain.name]: [
        '',
        [
          Validators.maxLength(
            this.addWebsiteFormFields.domain.validationParams
              .maxLength as number
          ),
          Validators.minLength(
            this.addWebsiteFormFields.domain.validationParams
              .minLength as number
          ),
          Validators.pattern(
            this.addWebsiteFormFields.domain.validationParams.pattern
          ),
          Validators.required,
        ],
      ],
      [this.addWebsiteFormFields.url.name]: [
        '',
        [
          Validators.maxLength(
            this.addWebsiteFormFields.url.validationParams.maxLength as number
          ),
          Validators.pattern(
            this.addWebsiteFormFields.url.validationParams.pattern
          ),
          Validators.required,
        ],
      ],
      [this.addWebsiteFormFields.domainAliases.name]: this.formBuilder.array(
        []
      ),
    };
    this.createForm = this.formBuilder.group(config, {
      validators: this.createFormValidator,
    });
  }
  public onChange(val: string) {
    this.createForm.patchValue({ url: 'https://' + val });
  }

  private createFormValidator(group: AbstractControl): ValidationErrors | null {
    const domainAliases = group.get('domainAliases').value;
    const domainAliasControls = (group.get('domainAliases') as FormArray)
      .controls;
    for (let i = 0; i < domainAliasControls.length; i++) {
      const currentAlias = domainAliasControls[i].get('name').value;
      if (!currentAlias) {
        domainAliasControls[i].get('name').setErrors(null);
        continue;
      }
      const pattern = /^[a-zA-Z0-9.+#%-_\/:?=]*$/;
      if (!pattern.test(currentAlias)) {
        domainAliasControls[i].get('name').setErrors({
          pattern: true,
        });
        continue;
      }
      const existed = domainAliases.some(
        (item, index) => item.name === currentAlias && index !== i
      );
      if (existed) {
        domainAliasControls[i].get('name').setErrors({
          duplicated: true,
        });
      } else {
        domainAliasControls[i].get('name').setErrors(null);
      }
    }
    return null;
  }
}
