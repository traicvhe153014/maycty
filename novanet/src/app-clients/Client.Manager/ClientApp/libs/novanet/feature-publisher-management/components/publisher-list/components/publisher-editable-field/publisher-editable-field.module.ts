import { NgModule } from '@angular/core';
import { PublisherEditableFieldComponent } from './publisher-editable-field.component';
import { CommonModule } from '@angular/common';
import { SvgIconModule } from '@core/components/svg-icon';
import { FormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';

@NgModule({
  declarations: [PublisherEditableFieldComponent],
  imports: [
    CommonModule,
    SvgIconModule,
    FormsModule,
    NzInputModule,
    NzSpaceModule,
    NzIconModule,
    NzButtonModule,
  ],
  exports: [PublisherEditableFieldComponent],
})
export class PublisherEditableFieldModule {}
