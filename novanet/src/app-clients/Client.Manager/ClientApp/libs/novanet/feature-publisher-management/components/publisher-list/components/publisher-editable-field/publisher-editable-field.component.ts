import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import {
  IPublisherResponse,
  IUpdatePublisherRequest,
  UpdatePublisher,
} from '@features/feature-publisher-management/store';
import { Store } from '@ngxs/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { count } from 'rxjs';

@Component({
  selector: 'novanet-publisher-editable-field',
  templateUrl: './publisher-editable-field.component.html',
})
export class PublisherEditableFieldComponent implements OnInit {
  @Input() public publisher: IPublisherResponse;
  @Input() public isAnySelected: boolean;
  @Input() public fieldName: string;

  public isEditing = false;
  public fieldEditing: string;
  public maxLengthInput: number;
  public minLengthInput: number;
  public notificationError: string;
  public isLoadingEditable: { [key: string]: boolean } = {};
  public iconEdit: { [key: string]: boolean } = {};

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (this.fieldName === 'fullName') {
      this.maxLengthInput = 30;
      this.minLengthInput = 3;
      this.notificationError = 'Họ tên Publisher tối thiểu là 3 kí tự.';
    } else {
      this.maxLengthInput = 10;
      this.minLengthInput = 10;
      this.notificationError = 'Số điện thoại Publisher không đúng định dạng.';
    }
  }

  public setEditing() {
    this.isEditing = true;
    this.fieldEditing = this.publisher[this.fieldName];
  }

  public cancelEditing() {
    this.isEditing = false;
  }

  public saveEditing() {
    this.isLoadingEditable[this.publisher.id] = true;
    if (this.fieldName === 'fullName') {
      this.fieldEditing = this.fieldEditing.trim();
      this.fieldEditing = this.fieldEditing.replace(/\s+/g, ' ');
      if (this.fieldEditing.length < this.minLengthInput) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Họ tên Publisher phải nhập tối thiểu 3 kí tự.'
          )
        );
        this.isLoadingEditable[this.publisher.id] = false;
        return;
      }
      if (this.fieldEditing.length > this.maxLengthInput) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Họ tên Publisher chỉ có thể nhập tối đa 30 kí tự.'
          )
        );
        this.isLoadingEditable[this.publisher.id] = false;
        return;
      }
    }

    if (this.fieldName === 'phoneNumber' && this.fieldEditing.length !== 0) {
      if (
        !/^[0-9]*$/.test(this.fieldEditing) ||
        this.fieldEditing[0] !== '0' ||
        !'35789'.includes(this.fieldEditing[1])
      ) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Số điện thoại sai định dạng dữ liệu'
          )
        );
        this.isLoadingEditable[this.publisher.id] = false;
        return;
      }
      if (this.fieldEditing.length !== 10) {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Số điện thoại sai phải nhập 10 số'
          )
        );
        this.isLoadingEditable[this.publisher.id] = false;
        return;
      }
    }

    const payload: IUpdatePublisherRequest = {
      id: this.publisher.id,
      [this.fieldName]: this.fieldEditing,
    };
    this.store.dispatch(new UpdatePublisher(payload)).subscribe({
      next: () => {
        this.isEditing = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Cập nhật publisher thành công'
          )
        );
        this.isLoadingEditable[this.publisher.id] = false;
        this.changeDetectorRef.detectChanges();
      },
      error: () => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra trong quá trình cập nhật publisher'
          )
        );
        this.isLoadingEditable[this.publisher.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }
}
