import { PredicateOperatorEnum } from '@core/enums';
import { PublisherSearchLevelEnum } from '@features/feature-publisher-management/components/publisher-list/enums';

export interface IPublisherSearchOption {
  value: string;
  operator: PredicateOperatorEnum;
  field: PublisherSearchLevelEnum;
}
