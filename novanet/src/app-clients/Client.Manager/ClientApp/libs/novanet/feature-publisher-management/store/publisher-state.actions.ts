import {
  ICreatePublisher,
  IListPublisherRequest,
  IListPublisherResponse,
  IPublisherResponse,
  IUpdatePasswordPublisherRequest,
  IUpdatePublisherRequest,
  PublisherStatusValue,
} from './publisher-state.model';

const enum PublisherActions {
  GetPublisherList = '[Publisher] Get List',
  GetPublisherListSuccess = '[Publisher] Get List Success',
  GetPublisherListError = '[Publisher] Get List Error',
  GetMorePublisherList = '[Publisher] Get More List',
  GetMorePublisherListSuccess = '[Publisher] Get More List Success',
  GetMorePublisherListError = '[Publisher] Get More List Error',
  GetPublisherDetail = '[Publisher] Get Detail',
  GetPublisherDetailSuccess = '[Publisher] Get Detail Success',
  GetPublisherDetailError = '[Publisher] Get Detail Error',
  CreatePublisher = '[Publisher] Create',
  CreatePublisherSuccess = '[Publisher] Create Success',
  CreatePublisherError = '[Publisher] Create Error',
  UpdatePublisher = '[Publisher] Update',
  UpdatePublisherSuccess = '[Publisher] Update Success',
  UpdatePublisherError = '[Publisher] Update Error',
  UpdatePasswordPublisher = '[Publisher] Update Password',
  UpdatePasswordPublisherSuccess = '[Publisher] Update Password Success',
  UpdatePasswordPublisherError = '[Publisher] Update Password Error',
  ClearAllPublishersData = '[Publisher] Clear All',
  GetStatusPublisher = '[Publisher] Get Status',
  getStatusPublisherAdd = '[Publisher] Get Status Publisher Add'
}

export class GetPublisherList {
  public static readonly type = PublisherActions.GetPublisherList;

  constructor(public payload: IListPublisherRequest) {}
}

export class GetPublisherListSuccess {
  public static readonly type = PublisherActions.GetPublisherListSuccess;

  constructor(
    public response: IListPublisherResponse,
    public page: number,
    public pageSize: number
  ) {}
}

export class GetPublisherListError {
  public static readonly type = PublisherActions.GetPublisherListError;

  constructor(public error: any) {}
}

export class GetMorePublisherList {
  public static readonly type = PublisherActions.GetMorePublisherList;

  constructor(public payload: IListPublisherRequest) {}
}

export class GetMorePublisherListSuccess {
  public static readonly type = PublisherActions.GetMorePublisherListSuccess;

  constructor(
    public response: IListPublisherResponse,
    public page: number,
    public pageSize: number
  ) {}
}

export class GetMorePublisherListError {
  public static readonly type = PublisherActions.GetMorePublisherListError;

  constructor(public error: any) {}
}

export class GetPublisherDetail {
  public static readonly type = PublisherActions.GetPublisherDetail;

  constructor(public id: string, public isFull: boolean = true) {}
}

export class GetPublisherDetailSuccess {
  public static readonly type = PublisherActions.GetPublisherDetailSuccess;

  constructor(public publisher: IPublisherResponse) {}
}

export class GetPublisherDetailError {
  public static readonly type = PublisherActions.GetPublisherDetailError;

  constructor(public error: any) {}
}

export class CreatePublisher {
    public static readonly type = PublisherActions.CreatePublisher;

    constructor(public payload: ICreatePublisher) {}
}

export class CreatePublisherSuccess {
    public static readonly type = PublisherActions.CreatePublisherSuccess;

    constructor(public publisher: IPublisherResponse) {}
}

export class CreatePublisherError {
    public static readonly type = PublisherActions.CreatePublisherError;

    constructor(public error: any) {}
}

export class UpdatePublisher {
  public static readonly type = PublisherActions.UpdatePublisher;

  constructor(public payload: IUpdatePublisherRequest) {}
}

export class UpdatePublisherSuccess {
  public static readonly type = PublisherActions.UpdatePublisherSuccess;

  constructor(public publisher: IPublisherResponse) {}
}

export class UpdatePublisherError {
  public static readonly type = PublisherActions.UpdatePublisherError;

  constructor(public error: any) {}
}

export class UpdatePasswordPublisher {
  public static readonly type = PublisherActions.UpdatePasswordPublisher;

  constructor(public payload: IUpdatePasswordPublisherRequest) {}
}

export class UpdatePasswordPublisherSuccess {
  public static readonly type = PublisherActions.UpdatePasswordPublisherSuccess;

  constructor(public response: boolean) {}
}

export class UpdatePasswordPublisherError {
  public static readonly type = PublisherActions.UpdatePasswordPublisherError;

  constructor(public error: any) {}
}

export class ClearAllPublishersData {
  public static readonly type = PublisherActions.ClearAllPublishersData;
}

export class GetStatusPublisher {
  public static readonly type = PublisherActions.GetStatusPublisher;

  constructor(public status: PublisherStatusValue) {}
}

export class GetStatusPublisherAdd {
  public static readonly type = PublisherActions.getStatusPublisherAdd;

  constructor(public statusPublisherAdd: boolean) {}
}
