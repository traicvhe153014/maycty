import { NgModule } from '@angular/core';
import { PublisherAddComponent } from './publisher-add.component';
import { CommonModule } from '@angular/common';
import { PublisherAddRoutingModule } from './publisher-add.routing';
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {NovanetInputModule} from "@shared/custom-input";
import {ValidateOnBlurModule} from "@shared/directives";
import {NzWaveModule} from "ng-zorro-antd/core/wave";

@NgModule({
  declarations: [PublisherAddComponent],
  exports: [PublisherAddComponent],
    imports: [CommonModule,
      PublisherAddRoutingModule,
      NzFormModule,
      ReactiveFormsModule, 
      NovanetInputModule, 
      ValidateOnBlurModule,
      NzWaveModule],
})
export class PublisherAddModule {}
