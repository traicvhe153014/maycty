import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublisherManagementRoutingModule } from './publisher-management.routing';
import { PublisherManagementComponent } from './publisher-management.component';

@NgModule({
  imports: [CommonModule, PublisherManagementRoutingModule],
  declarations: [PublisherManagementComponent],
  exports: [PublisherManagementComponent],
})
export class PublisherManagementModule {}
