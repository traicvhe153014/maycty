export enum PublisherListEnum {
  CHECKBOX,
  INDEX,
  IS_ACTIVE,
  FULL_NAME,
  EMAIL,
  PHONE_NUMBER,
  STATUS,
  DOMAIN_WEBSITES,
  CREATED_AT,
}

export enum PublisherSearchLevelEnum {
  EMAIL,
  DOMAIN_WEBSITES,
}
