import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublisherManagementComponent } from './publisher-management.component';

const routes: Routes = [
  {
    path: '',
    component: PublisherManagementComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./components/publisher-components.module').then(
            (m) => m.PublisherComponentsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublisherManagementRoutingModule {}
