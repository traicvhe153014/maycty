import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { PublisherService } from './publisher-state.service';
import { PublisherState } from './publisher.state';
import { IdentityService } from '@shared/identity';

@NgModule({
  imports: [NgxsModule.forFeature([PublisherState])],
  providers: [PublisherService, IdentityService],
})
export class PublisherStateModule {}
