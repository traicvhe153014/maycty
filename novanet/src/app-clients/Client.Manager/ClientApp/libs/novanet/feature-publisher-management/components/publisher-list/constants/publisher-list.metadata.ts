import { ISettingColumnTable } from '@data-table/models';
import { PublisherListEnum } from '../enums';
import { IPublisherResponse } from '@features/feature-publisher-management/store';
import { IFormFieldErrors, IFormFields } from '@models';
import { passwordPattern } from '@shared/sharing/constants';

export const PublisherListColumns = [
  {
    id: PublisherListEnum.CHECKBOX,
    title: '',
    align: 'center',
    width: '40px',
    pinLeft: true,
  },
  {
    id: PublisherListEnum.INDEX,
    title: 'Stt',
    width: '40px',
    pinLeft: true,
  },
  {
    id: PublisherListEnum.IS_ACTIVE,
    title: 'Mở/khóa',
    width: '80px',
    pinLeft: true,
  },
  {
    id: PublisherListEnum.FULL_NAME,
    title: 'Họ tên',
    sort: true,
    name: 'FullName',
    width: '220px',
    pinLeft: true,
  },
  {
    id: PublisherListEnum.EMAIL,
    title: 'Email',
    sort: true,
    name: 'Email',
    width: '260px',
  },
  {
    id: PublisherListEnum.PHONE_NUMBER,
    title: 'Số điện thoại',
    sort: true,
    name: 'PhoneNumber',
    width: '145px',
  },
  {
    id: PublisherListEnum.STATUS,
    title: 'Trạng thái',
    sort: true,
    name: 'Status',
    width: '110px',
  },
  {
    id: PublisherListEnum.DOMAIN_WEBSITES,
    title: 'Domain Website',
    width: '250px',
  },
  {
    id: PublisherListEnum.CREATED_AT,
    title: 'Ngày tạo',
    width: '130px',
    sort: true,
    name: 'CreatedAt',
  },
] as ISettingColumnTable<IPublisherResponse, any>[];

export const ChangePasswordFormFields: IFormFields = {
  newPassword: {
    name: 'newPassword',
    validationParams: {
      maxLength: 40,
      required: true,
      minLength: 6,
      pattern: passwordPattern,
    },
  },
  confirmNewPassword: {
    name: 'confirmNewPassword',
    validationParams: {
      maxLength: 40,
      required: true,
    },
  },
};

export const ChangePasswordFormFieldErrors: IFormFieldErrors = {
  newPassword: [
    {
      type: 'required',
      message: 'Không để trống mật khẩu',
    },
    {
      type: 'minlength',
      message: 'Mật khẩu có độ dài từ 6-40 ký tự.',
    },
    {
      type: 'maxlength',
      message: 'Mật khẩu có độ dài từ 6-40 ký tự.',
    },
    {
      type: 'pattern',
      message:
        'Mật khẩu phải có ít nhất một chữ thường (a-z) và không có space.',
    },
  ],
  confirmNewPassword: [
    {
      type: 'required',
      message: 'Không để trống mật khẩu',
    },
    {
      type: 'notSame',
      message: 'Mật khẩu không khớp',
    },
  ],
};
