import { NgModule } from '@angular/core';
import { PublisherComponentsRoutingModule } from './publisher-components.routing';
import { CommonModule } from '@angular/common';
import { PublisherListModule } from './publisher-list';
import { PublisherAddModule } from './publisher-add';

@NgModule({
  imports: [
    PublisherComponentsRoutingModule,
    CommonModule,
    PublisherListModule,
    PublisherAddModule,
  ],
})
export class PublisherComponentsModule {}
