import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { Store } from '@ngxs/store';
import { InputTypeEnum } from '@core/enums';
import { IFormFieldErrors, IFormFields } from '@models';
import {
  AddPublisherFormFieldErrors,
  AddPublisherFormFields,
} from '@features/feature-publisher-management/constants';
import {
  CreatePublisher,
  GetStatusPublisherAdd,
  ICreatePublisher,
  IPublisherStateModel,
} from '@features/feature-publisher-management/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { scrollIntoViewBySelector } from '@core/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { fullNamePattern } from '@shared/sharing/constants';
import { PublisherPriceType } from '@features/feature-website-price-management/store';

@Component({
  selector: 'novanet-publisher-add',
  templateUrl: './publisher-add.component.html',
})
export class PublisherAddComponent implements OnInit {
  public readonly inputTypeEnum = InputTypeEnum;
  public createForm: FormGroup;
  public readonly addPublisherFormFields = AddPublisherFormFields;
  public readonly addPublisherFormFieldErrors = AddPublisherFormFieldErrors;
  public globalNotificationEnum = GlobalNotificationEnum;
  public notificationMessagesEmail: string;
  public notificationMessagesFullName: string;
  public notificationMessagesPassword: string;
  public notificationMessagesConfirmPassword: string;
  @Input() formFields: IFormFields;
  @Input() formFieldErrors: IFormFieldErrors;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private store: Store,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
  }
  public onSubmit(shouldCreate: boolean) {
    this.createForm.markAllAsTouched();
    if (this.createForm.get('fullName').value.length === 0) {
      this.notificationMessagesFullName = 'Chưa nhập tên Publisher';
    } else {
      this.notificationMessagesFullName = 'Tên Publisher sai định dạng';
    }
    if (this.createForm.get('fullName').errors) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          this.notificationMessagesFullName
        )
      );
    }
    if (this.createForm.get('email').value.length === 0) {
      this.notificationMessagesEmail = 'Chưa nhập Email';
    } else {
      if (
        !/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(
          this.createForm.get('email').value
        )
      ) {
        this.notificationMessagesEmail = 'Email không đúng định dạng';
      } else {
      }
    }
    if (this.createForm.get('email').errors) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          this.notificationMessagesEmail
        )
      );
    }
    if (this.createForm.get('PhoneNumber').errors) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          'Số điện thoại sai định dạng'
        )
      );
    }
    if (this.createForm.get('password').value.length === 0) {
      this.notificationMessagesPassword = 'Chưa nhập mật khẩu';
    } else {
      this.notificationMessagesPassword = 'Mật khẩu sai định dạng';
    }
    if (this.createForm.get('password').errors) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          this.notificationMessagesPassword
        )
      );
    }
    if (this.createForm.get('confirmPassword').value.length === 0) {
      this.notificationMessagesConfirmPassword = 'Chưa nhập lại mật khẩu';
    } else {
      this.notificationMessagesConfirmPassword = 'Mật khẩu không khớp';
    }
    if (this.createForm.get('confirmPassword').errors) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.error,
          this.notificationMessagesConfirmPassword
        )
      );
    }
    if (!this.createForm.valid) {
      return;
    }

    const data = this.createForm.value;
    const payload: ICreatePublisher = {
      ...data,
      fullName: data.fullName.trim(),
      userName: data.email,
      role: 'Publisher',
    };
    this.store.dispatch(new CreatePublisher(payload)).subscribe({
      next: (state) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Tài khoản publisher đã tạo thành công'
          )
        );
        const createdPublisherId = (state.publisher as IPublisherStateModel)
          .createdPublisherId;
        const queryParams = {
          publisherId: createdPublisherId,
        };
        if (shouldCreate) {
          this.router.navigate(['/website/add'], { queryParams });
        } else {
          this.router.navigate(['/publisher/list']);
        }
      },
      error: (err) => {
        if (err.error.errors[0]?.includes('đã tồn tại')) {
          this.notificationMessagesEmail = 'Email đã tồn tại';
          this.createForm.get('email').setErrors({ emailExisted: true });
          this.changeDetectorRef.detectChanges();
        } else {
          this.notificationMessagesEmail = 'Có lỗi xảy ra';
        }
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            this.notificationMessagesEmail
          )
        );
      },
    });
  }

  private buildForm() {
    const config = {
      [this.addPublisherFormFields.fullName.name]: [
        '',
        [
          Validators.pattern(
            this.addPublisherFormFields.fullName.validationParams.pattern
          ),
          Validators.required,
        ],
      ],
      [this.addPublisherFormFields.email.name]: [
        '',
        [
          Validators.pattern(
            this.addPublisherFormFields.email.validationParams.pattern
          ),
          Validators.required,
        ],
      ],
      [this.addPublisherFormFields.PhoneNumber.name]: [
        '',
        [
          Validators.maxLength(
            this.addPublisherFormFields.PhoneNumber.validationParams
              .maxLength as number
          ),
          Validators.pattern(
            this.addPublisherFormFields.PhoneNumber.validationParams.pattern
          ),
          Validators.minLength(
            this.addPublisherFormFields.PhoneNumber.validationParams
              .minLength as number
          ),
        ],
      ],
      [this.addPublisherFormFields.IsActive.name]: [
        true,
        [Validators.required],
      ],
      [this.addPublisherFormFields.password.name]: [
        '',
        [
          Validators.minLength(
            this.addPublisherFormFields.password.validationParams
              .minLength as number
          ),
          Validators.maxLength(
            this.addPublisherFormFields.password.validationParams
              .maxLength as number
          ),
          Validators.pattern(
            this.addPublisherFormFields.password.validationParams.pattern
          ),
          Validators.required,
        ],
      ],
      [this.addPublisherFormFields.confirmPassword.name]: [
        '',
        [
          Validators.minLength(
            this.addPublisherFormFields.confirmPassword.validationParams
              .minLength as number
          ),
          Validators.maxLength(
            this.addPublisherFormFields.confirmPassword.validationParams
              .maxLength as number
          ),
          Validators.required,
        ],
      ],
    };
    this.createForm = this.formBuilder.group(config, {
      validators: this.validatePublisherForm,
    });
  }

  private validatePublisherForm(
    group: AbstractControl
  ): ValidationErrors | null {
    const name = group.get('fullName').value.trim();
    if (!name) {
      group.get('fullName').setErrors({
        required: true,
      });
    } else if (name.length < 3) {
      group.get('fullName').setErrors({
        minlength: true,
      });
    } else if (name.length > 30) {
      group.get('fullName').setErrors({
        maxlength: true,
      });
    } else if (!fullNamePattern.test(name)) {
      group.get('fullName').setErrors({
        pattern: true,
      });
    } else {
      group.get('fullName').setErrors(null);
    }

    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPassword').value;
    if (pass === confirmPass) {
      return null;
    }
    group.get('confirmPassword').setErrors({
      notSame: true,
    });
    return null;
  }
}
