import { DataTableSettingModel } from '@data-table/models';
import { PublisherSearchLevelEnum } from '@features/feature-publisher-management/components/publisher-list/enums';
import { PredicateOperatorEnum } from '@core/enums';
import { PublisherStatus } from '@features/feature-publisher-management/store';
import { IFormFieldErrors, IFormFields } from '@models';

export const PublisherListSettingTable = {
  id: 'publisher-list',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: true,
  header: true,
  footer: true,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'scroll',
  scrollX: '1940px',
  sortType: 'single',
} as DataTableSettingModel;

export const publisherSearchLevelLabels = {
  [PublisherSearchLevelEnum.EMAIL]: 'Email publisher',
  [PublisherSearchLevelEnum.DOMAIN_WEBSITES]: 'Domain Websites',
};
export const publisherSearchLevelFields = {
  [PublisherSearchLevelEnum.EMAIL]: '0',
  [PublisherSearchLevelEnum.DOMAIN_WEBSITES]: '1',
};
export const productSearchOperatorLabels = {
  [PredicateOperatorEnum.Contains]: 'bao gồm',
};

export const publisherStatusOptions = [
  {
    value: '',
    label: 'Tất cả',
  },
  {
    value: PublisherStatus.Running,
    label: 'Đang chạy',
  },
  {
    value: PublisherStatus.Disabled,
    label: 'Tạm khoá',
  },
];
