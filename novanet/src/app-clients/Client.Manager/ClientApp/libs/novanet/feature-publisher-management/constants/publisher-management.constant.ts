import { IFormFieldErrors, IFormFields } from '@models';
import { fullNamePattern } from '@shared/sharing/constants';

export const AddPublisherFormFields: IFormFields = {
  fullName: {
    name: 'fullName',
    validationParams: {
      maxLength: 30,
      minLength: 3,
      pattern: fullNamePattern,
    },
  },
  email: {
    name: 'email',
    validationParams: {
      required: true,
      pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
    },
  },
  PhoneNumber: {
    name: 'PhoneNumber',
    validationParams: {
      required: true,
      maxLength: 10,
      pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
    },
  },
  IsActive: {
    name: 'IsActive',
    validationParams: {
      required: true,
    },
  },
  password: {
    name: 'password',
    validationParams: {
      required: true,
      maxLength: 40,
      minLength: 6,
      pattern: /(?!^[^a-z]+$)^\S+$/,
    },
  },
  confirmPassword: {
    name: 'confirmPassword',
    validationParams: {
      required: true,
    },
  },
};
export const AddPublisherFormFieldErrors: IFormFieldErrors = {
  fullName: [
    {
      type: 'required',
      message: 'Chưa nhập tên Publisher',
    },
    {
      type: 'maxlength',
      message: 'Số lượng kí tự tối đa là 30 kí tự',
    },
    {
      type: 'minlength',
      message: 'Số lượng kí tự tối thiểu là 3 kí tự',
    },
    {
      type: 'pattern',
      message: 'Không dùng nhiều khoảng trắng giữa các kí tự',
    },
  ],
  email: [
    {
      type: 'required',
      message: 'Chưa nhập email',
    },
    {
      type: 'pattern',
      message: 'Email không đúng định dạng',
    },
    {
      type: 'emailExisted',
      message: 'Email đã tồn tại',
    },
  ],
  PhoneNumber: [
    {
      type: 'maxlength',
      message: 'Số điện thoại chuẩn là 10 số',
    },
    {
      type: 'pattern',
      message: 'Số điện thoại sai định dạng',
    },
  ],
  password: [
    {
      type: 'required',
      message: 'Mật khẩu không được để trống',
    },
    {
      type: 'maxlength',
      message: 'Tối đa 40 kí tự ',
    },
    {
      type: 'minlength',
      message: 'Tối thiểu 6 kí tự',
    },
    {
      type: 'pattern',
      message:
        'Mật khẩu ít nhất có một chữ cái thường từ a-z và không có khoảng trắng',
    },
  ],
  confirmPassword: [
    {
      type: 'required',
      message: 'Chưa nhập lại mật khẩu',
    },
    {
      type: 'notSame',
      message: 'Mật khẩu không khớp',
    },
  ],
};
