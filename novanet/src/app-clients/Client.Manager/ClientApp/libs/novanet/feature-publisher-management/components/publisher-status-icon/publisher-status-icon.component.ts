import {Component, Input} from '@angular/core';
import {PublisherStatus, PublisherStatusValue} from '@features/feature-publisher-management/store';

@Component({
    selector: 'novanet-publisher-status-icon',
    templateUrl: './publisher-status-icon.component.html',
    styleUrls: ['./publisher-status-icon.component.scss'],
})
export class PublisherStatusIconComponent {
    @Input() status: PublisherStatusValue;
    public readonly publisherStatusEnum = PublisherStatus;
}