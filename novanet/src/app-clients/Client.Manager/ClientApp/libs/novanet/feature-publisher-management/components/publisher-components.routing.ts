import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        loadChildren: () =>
          import('./publisher-list/publisher-list.module').then(
            (m) => m.PublisherListModule
          ),
      },
      {
        path: 'add',
        loadChildren: () =>
          import('./publisher-add/publisher-add.module').then(
            (m) => m.PublisherAddModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublisherComponentsRoutingModule {}
