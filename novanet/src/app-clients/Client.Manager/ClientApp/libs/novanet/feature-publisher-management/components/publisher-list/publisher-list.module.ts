import { NgModule } from '@angular/core';
import { PublisherListComponent } from './publisher-list.component';
import { CommonModule } from '@angular/common';
import { PublisherListRoutingModule } from './publisher-list.routing';
import {
  DataTableModule,
  ExpandableListModule,
  FormatterModule,
  LoadingIconModule,
  MapModule,
} from '@core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SvgIconModule } from '@core/components/svg-icon';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';
import { PublisherEditableFieldModule } from './components';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NovanetInputModule } from '@shared/custom-input';
import { PublisherStatusIconModule } from '@features/feature-publisher-management/components/publisher-status-icon/publisher-status-icon.module';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
  declarations: [PublisherListComponent],
  exports: [PublisherListComponent],
  imports: [
    CommonModule,
    PublisherListRoutingModule,
    DataTableModule,
    LoadingIconModule,
    FormatterModule,
    FormsModule,
    SvgIconModule,
    SwitchNoAnimationModule,
    PublisherEditableFieldModule,
    NzModalModule,
    NovanetInputModule,
    ReactiveFormsModule,
    ExpandableListModule,
    MapModule,
    PublisherStatusIconModule,
    NzSpinModule,
    NzSelectModule,
    NzToolTipModule,
  ],
})
export class PublisherListModule {}
