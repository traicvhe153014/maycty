import { Injectable } from '@angular/core';
import {
  IPublisherResponse,
  IPublisherStateModel,
} from './publisher-state.model';
import { Observable, throwError } from 'rxjs';
import { PublisherService } from './publisher-state.service';
import {
  ClearAllPublishersData,
  CreatePublisher,
  CreatePublisherError,
  CreatePublisherSuccess,
  GetMorePublisherList,
  GetMorePublisherListError,
  GetMorePublisherListSuccess,
  GetPublisherDetail,
  GetPublisherDetailError,
  GetPublisherDetailSuccess,
  GetPublisherList,
  GetPublisherListError,
  GetPublisherListSuccess,
  GetStatusPublisher,
  GetStatusPublisherAdd,
  UpdatePasswordPublisher,
  UpdatePasswordPublisherError,
  UpdatePasswordPublisherSuccess,
  UpdatePublisher,
  UpdatePublisherError,
  UpdatePublisherSuccess,
} from './publisher-state.actions';
import { GlobalNotificationEnum } from '@shared/global-notification';
import { catchError, map } from 'rxjs/operators';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { HideLoading } from '@shared/global-loading';

@Injectable()
@State<IPublisherStateModel>({
  name: 'publisher',
  defaults: {
    loading: false,
    page: 1,
    publishers: [],
    hasMorePages: true,
    total: 0,
    status: null,
    statusPublisherAdd: false,
  },
})
export class PublisherState {
  private readonly globalNotificationEnum = GlobalNotificationEnum;

  constructor(private readonly publisherService: PublisherService) {}

  /**
   * Selectors
   */
  @Selector()
  public static getList(state: IPublisherStateModel): IPublisherResponse[] {
    return state.publishers;
  }

  @Selector()
  public static getPage(state: IPublisherStateModel): number {
    return state.page;
  }

  @Selector()
  public static getHasMorePages(state: IPublisherStateModel): boolean {
    return state.hasMorePages;
  }

  @Selector()
  public static getTotal(state: IPublisherStateModel): number {
    return state.total;
  }

  @Selector()
  public static getLoading(state: IPublisherStateModel): boolean {
    return state.loading;
  }

  @Selector()
  public static getDetail(state: IPublisherStateModel): IPublisherResponse {
    return state.detailedPublisher;
  }

  @Selector()
  public static getStatus(state: IPublisherStateModel) {
    return state.status;
  }
  /**
   * Actions
   */
  @Action(GetPublisherList)
  public getList(
    ctx: StateContext<IPublisherStateModel>,
    { payload }: GetPublisherList
  ) {
    const loadingLabel = 'Get Publisher List';
    if (ctx.getState().loading) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.publisherService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(
          new GetPublisherListSuccess(response, payload.page, payload.pageSize)
        )
      ),
      catchError((err) => ctx.dispatch(new GetPublisherListError(err)))
    );
  }

  @Action(GetPublisherListSuccess)
  public getListSuccess(
    ctx: StateContext<IPublisherStateModel>,
    { response, page, pageSize }: GetPublisherListSuccess
  ) {
    const { data: publishers } = response;
    const loadingLabel = 'Get Publisher List';
    const newPage = publishers.length === 0 && page !== 1 ? page - 1 : page;
    const hasMorePages = publishers.length === pageSize;
    ctx.dispatch(new HideLoading(loadingLabel));

    ctx.patchState({
      publishers,
      page: newPage,
      hasMorePages,
      loading: false,
      total: response.total,
    });
  }

  @Action(GetPublisherListError)
  public getListError(
    ctx: StateContext<IPublisherStateModel>,
    { error }: GetPublisherListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetMorePublisherList)
  public getMoreList(
    ctx: StateContext<IPublisherStateModel>,
    { payload }: GetMorePublisherList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (!ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.publisherService.getList(payload).pipe(
      map((response) =>
        ctx.dispatch(
          new GetMorePublisherListSuccess(
            response,
            payload.page,
            payload.pageSize
          )
        )
      ),
      catchError((err) => ctx.dispatch(new GetMorePublisherListError(err)))
    );
  }

  @Action(GetMorePublisherListSuccess)
  public getMoreListSuccess(
    ctx: StateContext<IPublisherStateModel>,
    { response, page, pageSize }: GetMorePublisherListSuccess
  ) {
    const { data: publishers } = response;
    const newPage = publishers.length === 0 ? page - 1 : page;
    const hasMorePages = publishers.length === pageSize;
    const currentPublishers = ctx.getState().publishers;
    ctx.patchState({
      publishers: [...currentPublishers, ...publishers],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMorePublisherListError)
  public getMoreListError(
    ctx: StateContext<IPublisherStateModel>,
    { error }: GetMorePublisherListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(GetPublisherDetail)
  public getDetail(
    ctx: StateContext<IPublisherStateModel>,
    { id, isFull }: GetPublisherDetail
  ) {
    ctx.patchState({ loading: true });
    return this.publisherService.getById(id, isFull).pipe(
      map((response) => ctx.dispatch(new GetPublisherDetailSuccess(response))),
      catchError((err) => ctx.dispatch(new GetPublisherDetailError(err)))
    );
  }

  @Action(GetPublisherDetailSuccess)
  public getDetailSuccess(
    ctx: StateContext<IPublisherStateModel>,
    { publisher }: GetPublisherDetailSuccess
  ) {
    ctx.patchState({ detailedPublisher: publisher, loading: false });
  }

  @Action(GetPublisherDetailError)
  public getDetailError(
    ctx: StateContext<IPublisherStateModel>,
    { error }: GetPublisherDetailError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }

  @Action(CreatePublisher)
  public create(
    ctx: StateContext<IPublisherStateModel>,
    { payload }: CreatePublisher
  ) {
    return this.publisherService.create(payload).pipe(
      map((response) => ctx.dispatch(new CreatePublisherSuccess(response))),
      catchError((error) => ctx.dispatch(new CreatePublisherError(error)))
    );
  }

  @Action(CreatePublisherSuccess)
  public createSuccess(
    ctx: StateContext<IPublisherStateModel>,
    { publisher }: CreatePublisherSuccess
  ) {
    ctx.patchState({ createdPublisherId: publisher.id });
  }

  @Action(CreatePublisherError)
  public createError(
    ctx: StateContext<IPublisherStateModel>,
    { error }: CreatePublisherError
  ) {
    return throwError(error);
  }

  @Action(UpdatePublisher)
  public update(
    ctx: StateContext<IPublisherStateModel>,
    { payload }: UpdatePublisher
  ) {
    return this.publisherService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdatePublisherSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdatePublisherError(error)))
    );
  }

  @Action(UpdatePublisherSuccess)
  public updateSuccess(
    ctx: StateContext<IPublisherStateModel>,
    { publisher }: UpdatePublisherSuccess
  ) {
    const { publishers } = ctx.getState();
    const updatedPublishers = publishers.map((pub) =>
      pub.id === publisher.id ? { ...pub, ...publisher } : pub
    );
    ctx.patchState({ publishers: updatedPublishers });
  }

  @Action(UpdatePublisherError)
  public updateError(
    ctx: StateContext<IPublisherStateModel>,
    { error }: UpdatePublisherError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(UpdatePasswordPublisher)
  public updatePassword(
    ctx: StateContext<IPublisherStateModel>,
    { payload }: UpdatePasswordPublisher
  ) {
    return this.publisherService.updatePassword(payload).pipe(
      map((response) =>
        ctx.dispatch(new UpdatePasswordPublisherSuccess(response))
      ),
      catchError((error) =>
        ctx.dispatch(new UpdatePasswordPublisherError(error))
      )
    );
  }

  @Action(UpdatePasswordPublisherSuccess)
  public updatePasswordSuccess(
    ctx: StateContext<IPublisherStateModel>,
    { response }: UpdatePasswordPublisherSuccess
  ) {
    ctx.patchState({});
  }

  @Action(UpdatePasswordPublisherError)
  public updatePasswordError(
    ctx: StateContext<IPublisherStateModel>,
    { error }: UpdatePasswordPublisherError
  ) {
    ctx.patchState({});
    return throwError(error);
  }

  @Action(ClearAllPublishersData)
  public clearAllData(ctx: StateContext<IPublisherStateModel>) {
    const publisherState = {
      publishers: [],
      detailedPublisher: {},
      page: 1,
      loading: false,
      hasMorePages: false,
      total: 0,
    } as IPublisherStateModel;

    ctx.patchState(publisherState);
  }

  @Action(GetStatusPublisher)
  public GetStatusPublisher(
    ctx: StateContext<IPublisherStateModel>,
    { status }: GetStatusPublisher
  ) {
    ctx.patchState({ status: status });
  }

  @Action(GetStatusPublisherAdd)
  public GetStatusPublisherAdd(
    ctx: StateContext<IPublisherStateModel>,
    { statusPublisherAdd }: GetStatusPublisherAdd
  ) {
    ctx.patchState({ statusPublisherAdd: statusPublisherAdd });
  }
}
