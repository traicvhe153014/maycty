import { Observable } from 'rxjs';
import {
  ICreatePublisher,
  IListPublisherRequest,
  IListPublisherResponse,
  IPublisherResponse,
  IUpdatePasswordPublisherRequest,
  IUpdatePublisherRequest,
  PublisherStatusValue,
} from './publisher-state.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService, baseUrl } from '@shared/services';
import { Inject, Injectable } from '@angular/core';
import { ISuccessHttpResponse } from '@models';
import { map, take } from 'rxjs/operators';
import { Select } from '@ngxs/store';
import { PublisherState } from '@features/feature-publisher-management/store/publisher.state';

const PublisherUrls = {
  getList: `publisher/List`,
  getById: `publisher/Get`,
  create: 'SignUp',
  update: 'Update',
  updatePassword: 'UpdatePassword',
};

@Injectable()
export class PublisherService extends BaseService {
  @Select(PublisherState.getStatus)
  public status$: Observable<PublisherStatusValue>;

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'identity/api/v1');
  }

  public getList(
    payload: IListPublisherRequest
  ): Observable<IListPublisherResponse> {
    this.status$.pipe(take(1)).subscribe((status) => {
      if (status != null) {
        payload.status = status;
      }
    });
    const params = new HttpParams({
      fromObject: {
        ...payload,
        filters: payload.filters ? JSON.stringify(payload.filters) : '',
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IListPublisherResponse>>(
        this.createUrl(PublisherUrls.getList),
        {
          params,
        }
      )
      .pipe(map((response) => response.data));
  }

  public getById(id: string, isFull: boolean): Observable<IPublisherResponse> {
    const params = new HttpParams({
      fromObject: {
        id,
        isFull,
      },
    });
    return this.httpClient
      .get<ISuccessHttpResponse<IPublisherResponse>>(
        this.createUrl(PublisherUrls.getById),
        {
          params,
        }
      )
      .pipe(map((res) => res.data));
  }

  public create(payload: ICreatePublisher): Observable<IPublisherResponse> {
    return this.httpClient
      .post<ISuccessHttpResponse<IPublisherResponse>>(
        this.createUrl(PublisherUrls.create),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public update(
    payload: IUpdatePublisherRequest
  ): Observable<IPublisherResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IPublisherResponse>>(
        this.createUrl(PublisherUrls.update),
        payload
      )
      .pipe(map((res) => res.data));
  }

  public updatePassword(
    payload: IUpdatePasswordPublisherRequest
  ): Observable<boolean> {
    return this.httpClient
      .put<ISuccessHttpResponse<boolean>>(
        this.createUrl(PublisherUrls.updatePassword),
        payload
      )
      .pipe(map((res) => res.data));
  }
}
