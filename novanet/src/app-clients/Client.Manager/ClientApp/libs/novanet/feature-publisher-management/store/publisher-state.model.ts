import { IPredicateModel } from '@core/models';
import { IPublisherSearchOption } from '@features/feature-publisher-management/components/publisher-list/types/publisher-group.type';

export enum PublisherStatus {
  Running = 1,
  Disabled = 2,
}

export type PublisherStatusValue = keyof typeof PublisherStatus;

export interface IListPublisherRequest {
  page: number;
  pageSize: number;
  filters?: IPublisherSearchOption[];
  ids?: string;
  sorts?: string[];
  status?: PublisherStatusValue;
  keyword?: string;
}

export interface IPublisherDomain {
  id: string;
  domain: string;
}

export interface IListPublisherResponse {
  data: IPublisherResponse[];
  total: number;
}

export interface IPublisherResponse {
  id: string;
  isActive: boolean;
  fullName: string;
  email: string;
  phoneNumber: string;
  status: any;
  domainWebsites?: IPublisherDomain[];
  createdAt: string;
  isLoading?: boolean;
}

export interface IUpdatePublisherRequest {
  id: string;
  fullName?: string;
  phoneNumber?: string;
  isActive?: boolean;
}

export interface IUpdatePasswordPublisherRequest {
  email: string;
  newPassword: string;
  confirmNewPassword: string;
}

export interface IPublisherStateModel {
  publishers: IPublisherResponse[];
  detailedPublisher?: IPublisherResponse;
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  total: number;
  status?: PublisherStatusValue;
  statusPublisherAdd?: boolean;
  createdPublisherId?: string;
}

export interface ICreatePublisher {
  fullName: string;
  userName: string;
  email: string;
  password: string;
  confirmPassword: string;
  role: string;
}
