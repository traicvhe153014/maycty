import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import {
    PublisherStatusIconComponent
} from "@features/feature-publisher-management/components/publisher-status-icon/publisher-status-icon.component";

@NgModule({
    declarations: [PublisherStatusIconComponent],
    imports: [CommonModule, NzToolTipModule],
    exports: [PublisherStatusIconComponent],
})
export class PublisherStatusIconModule {}