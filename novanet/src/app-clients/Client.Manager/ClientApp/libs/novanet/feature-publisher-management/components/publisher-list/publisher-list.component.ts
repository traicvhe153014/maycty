import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ClearAllPublishersData,
  GetMorePublisherList,
  GetPublisherList,
  GetStatusPublisher,
  GetStatusPublisherAdd,
  IPublisherResponse,
  IUpdatePasswordPublisherRequest,
  IUpdatePublisherRequest,
  PublisherState,
  PublisherStatusValue,
  UpdatePasswordPublisher,
  UpdatePublisher,
} from '@features/feature-publisher-management/store';
import { PublisherListEnum, PublisherSearchLevelEnum } from './enums';
import { Select, Store } from '@ngxs/store';
import {
  ChangePasswordFormFieldErrors,
  ChangePasswordFormFields,
  productSearchOperatorLabels,
  PublisherListColumns,
  PublisherListSettingTable,
  publisherSearchLevelFields,
  publisherSearchLevelLabels,
  publisherStatusOptions,
} from './constants';
import { Observable } from 'rxjs';
import { IFieldDetail } from '@data-table/models';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';
import { take } from 'rxjs/operators';
import {
  ESortType,
  InputTypeEnum,
  PredicateOperatorEnum,
  TypeExpandableList,
} from '@core/enums';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { IPublisherSearchOption } from '@features/feature-publisher-management/components/publisher-list/types/publisher-group.type';
import { Router } from '@angular/router';
import { GetPublishersId } from '@features/feature-website-management/store/website-state.actions';
import { storage } from '@core';
import { IPredicateModel } from '@core/models';

@Component({
  selector: 'novanet-publisher-list',
  templateUrl: './publisher-list.component.html',
  styleUrls: ['./publisher-list.component.scss'],
})
export class PublisherListComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @Select(PublisherState.getList) public publishers$: Observable<
    IPublisherResponse[]
  >;
  @Select(PublisherState.getPage) public currentPage$: Observable<number>;
  @Select(PublisherState.getHasMorePages)
  public hasMorePages$: Observable<boolean>;
  @Select(PublisherState.getLoading) public loading$: Observable<boolean>;
  @Select(PublisherState.getTotal) public total$: Observable<number>;
  @Select(PublisherState.getStatus)
  public status$: Observable<PublisherStatusValue>;

  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public readonly publisherListEnum = PublisherListEnum;
  public columns = PublisherListColumns;
  public readonly settings = PublisherListSettingTable;
  public readonly inputTypeEnum = InputTypeEnum;
  public readonly changePasswordFormFields = ChangePasswordFormFields;
  public readonly changePasswordFormFieldErrors = ChangePasswordFormFieldErrors;
  public publisherLoadings: { [key: string]: boolean } = {};

  public changePasswordFormGroup: FormGroup;

  public keyword = '';
  public sorts = ['status,-subid'];
  public readonly baseSorts = ['status,-subid'];
  public selectedPublishers: string[] = [];
  public readonly pageSize = 25;
  public isChangePasswordModalVisible = false;
  public editingPasswordPublisherEmail: string | undefined;
  public errMessagePassword: string | undefined;
  public errMessageConfirmPassword: string | undefined;
  public isConfirmChangePasswordModalVisible = false;
  public filterPredicates = [];
  public selectedSearchOptions: any[] = [];
  public searchOptions = [];
  public searchDropdownOpen = false;
  public publisherSearchLevelLabels = publisherSearchLevelLabels;
  public publisherSearchOperatorLabels = productSearchOperatorLabels;
  public publisherStatusOptions = publisherStatusOptions;
  public filterStatus: PublisherStatusValue | null = null;
  public messageConfirm: string;
  public activeChangeStatus: boolean;
  public activeDataPublisher: IPublisherResponse;
  public isconfirmActiveChangeModalVisible = false;
  public typeExpandable = TypeExpandableList;
  private isUpdated = false;
  public searchStorage = 'SearchOptionsPublisher';
  public statusStorage = 'StatusPublisher';
  public emailPublisherCheckBox: string[] = [];
  public isFocus = false;
  public publisherSearchLevelEnum = PublisherSearchLevelEnum;
  public publisherSearchLevelFields = publisherSearchLevelFields;

  constructor(
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  public get isAnySelected() {
    return this.selectedPublishers.length > 0;
  }

  ngOnInit() {
    let options = storage.get(this.searchStorage) as IPublisherSearchOption[];
    if (options) {
      this.selectedSearchOptions = options.map((item) => ({
        value: item.value,
        operator: item.operator,
        level: item.field as Number,
      }));
      this.searchOptions = options;
      this.filterPredicates = this.selectedSearchOptions;
    }
    this.filterStatus = storage.get(this.statusStorage) as PublisherStatusValue;
    this.store.dispatch(new GetStatusPublisher(this.filterStatus));

    this.store.dispatch(
      new GetPublisherList({
        page: 1,
        pageSize: this.pageSize,
        filters: this.filterPredicates,
        sorts: this.sorts,
      })
    );

    this.buildChangePasswordForm();
  }

  ngAfterViewInit() {
    this.columns = PublisherListColumns.map((column) =>
      column.id === PublisherListEnum.CHECKBOX
        ? {
            ...column,
            title: this.checkboxHeader,
          }
        : column
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearAllPublishersData());
  }

  public isPublisherSelected(id: string): boolean {
    return !!this.selectedPublishers.find((feed) => feed === id);
  }

  public viewWebsites() {
    if (this.selectedPublishers.length > 1) {
      this.store.dispatch(
        new ShowGlobalNotification(
          GlobalNotificationEnum.warning,
          `Không thể chọn nhiều hơn một Publisher để xem website`
        )
      );
      return;
    }
    this.store.dispatch(
      new GetPublishersId(
        this.selectedPublishers[0] !== undefined
          ? this.selectedPublishers[0]
          : null
      )
    );
    if (this.emailPublisherCheckBox.length > 0) {
      const storagePublisher: IPublisherSearchOption[] = [
        { field: 0, operator: 0, value: this.emailPublisherCheckBox[0] },
      ];
      storage.set('SearchOptionsWebsite', storagePublisher);
      storage.set('StatusWebsite', []);
    } else {
      storage.set('SearchOptionsWebsite', []);
      storage.set('StatusWebsite', []);
    }
    this.router.navigate(['/website/list']);
  }

  public onSelectPublisherChanged(event: Event, id: string, email: string) {
    if (this.isPublisherSelected(id)) {
      this.selectedPublishers = this.selectedPublishers.filter(
        (feed) => feed !== id
      );
      this.emailPublisherCheckBox = this.emailPublisherCheckBox.filter(
        (feed) => feed !== email
      );
    } else {
      this.selectedPublishers = [...this.selectedPublishers, id];
      this.emailPublisherCheckBox = [...this.emailPublisherCheckBox, email];
    }
  }

  public onSelectAllPublishersChanged(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.publishers$.pipe(take(1)).subscribe({
        next: (publishers) => {
          this.selectedPublishers = publishers.map((publisher) => publisher.id);
        },
      });
    } else {
      this.selectedPublishers = [];
    }
  }

  public searchPublishers(values?: IFieldDetail[]) {
    if (values.length) {
      const sorts = [];
      values.forEach((value) => {
        const exist = this.columns.find((x) => x.name === value.name);
        this.columns.find((column) => {
          const names = values.map((x) => x.name);
          if (!names.includes(column.name)) {
            column.ascending = false;
            column.descending = false;
          }
        });
        if (exist) {
          switch (value.direction) {
            case ESortType.Ascending:
              sorts.push(value.name);
              exist.ascending = true;
              exist.descending = false;
              break;
            case ESortType.Descending:
              sorts.push('-' + value.name);
              exist.ascending = false;
              exist.descending = true;
              break;
          }
        }
      });
      this.sorts = sorts;
    } else {
      this.columns.find((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.store.dispatch(
      new GetPublisherList({
        page: 1,
        pageSize: this.pageSize,
        filters: this.filterPredicates,
        sorts: this.sorts,
      })
    );
  }
  public clickConfirmChangeActive() {
    const payload: IUpdatePublisherRequest = {
      id: this.activeDataPublisher.id,
      isActive: this.activeChangeStatus,
    };
    this.store.dispatch(new UpdatePublisher(payload)).subscribe({
      next: (response) => {
        this.isconfirmActiveChangeModalVisible = false;
        this.publisherLoadings[this.activeDataPublisher.id] = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Thành công'
          )
        );
      },
      error: (error) => {
        this.isconfirmActiveChangeModalVisible = false;
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
        this.publisherLoadings[this.activeDataPublisher.id] = false;
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public handleCancelConfirmChangeActive() {
    this.isconfirmActiveChangeModalVisible = false;
    this.publisherLoadings[this.activeDataPublisher.id] = false;
  }

  public onIsActiveChange(event: boolean, data: IPublisherResponse) {
    this.activeChangeStatus = event;
    this.activeDataPublisher = data;
    if (this.publisherLoadings[this.activeDataPublisher.id]) {
      return;
    }
    this.publisherLoadings[this.activeDataPublisher.id] = true;
    if (!event) {
      this.messageConfirm =
        'Hành động sẽ khiến publisher sẽ không thể truy cập tài khoản, bạn vẫn muốn khóa ?';
      this.isconfirmActiveChangeModalVisible = true;
    } else {
      this.clickConfirmChangeActive();
    }
  }

  public showChangePasswordModal(email: string) {
    this.editingPasswordPublisherEmail = email;
    this.isChangePasswordModalVisible = true;
  }

  public handleCancelChangePassword() {
    this.editingPasswordPublisherEmail = undefined;
    this.isChangePasswordModalVisible = false;
    this.changePasswordFormGroup.reset();
    this.errMessageConfirmPassword = undefined;
    this.errMessagePassword = undefined;
  }

  public confirmChangePassword() {
    this.changePasswordFormGroup.markAllAsTouched();
    const newPassword = this.changePasswordFormGroup.get('newPassword').value;
    const confirmNewPassword =
      this.changePasswordFormGroup.get('confirmNewPassword').value;
    if (newPassword.length == 0) {
      this.errMessagePassword = 'Không để trống mật khẩu.';
      return;
    }
    if (
      newPassword.length < 6 ||
      newPassword.length > 40 ||
      (newPassword && !/[a-z]/.test(newPassword))
    ) {
      this.errMessagePassword =
        'Mật khẩu phải có ít nhất một chữ thường (a-z) và độ dài từ 6-40 ký tự.';
      this.errMessageConfirmPassword = undefined;
      return;
    }
    if (newPassword != confirmNewPassword) {
      this.errMessagePassword = undefined;
      this.errMessageConfirmPassword = 'Mật khẩu không khớp';
      return;
    }

    this.isChangePasswordModalVisible = false;
    this.messageConfirm =
      'Bạn đang thay đổi mật khẩu điều này có thể ảnh hưởng đến User hiện tại.\n' +
      'Bạn vẫn muốn sử dụng mật khẩu mới?';
    this.isConfirmChangePasswordModalVisible = true;
    this.errMessageConfirmPassword = undefined;
    this.errMessagePassword = undefined;
  }

  private buildChangePasswordForm() {
    const config = {
      [this.changePasswordFormFields['newPassword'].name]: [
        '',
        [
          Validators.maxLength(
            this.changePasswordFormFields['newPassword'].validationParams
              .maxLength as number
          ),
          Validators.required,
          Validators.minLength(
            this.changePasswordFormFields['newPassword'].validationParams
              .minLength as number
          ),
          Validators.pattern(
            this.changePasswordFormFields['newPassword'].validationParams
              .pattern
          ),
        ],
      ],
      [this.changePasswordFormFields['confirmNewPassword'].name]: [
        '',
        [Validators.required],
      ],
    };
    this.changePasswordFormGroup = this.formBuilder.group(config, {
      validators: this.validateEnterPassword,
    });
  }

  private validateEnterPassword(
    group: AbstractControl
  ): ValidationErrors | null {
    let pass = group.get('newPassword').value;
    let confirmPass = group.get('confirmNewPassword').value;
    if (pass === confirmPass) {
      return null;
    }
    group.get('confirmNewPassword').setErrors({
      notSame: true,
    });
    return null;
  }

  handleCancelConfirmChangePassword() {
    this.changePasswordFormGroup.reset();
    this.isConfirmChangePasswordModalVisible = false;
  }

  clickConfirmChangePassword() {
    const payload: IUpdatePasswordPublisherRequest = {
      email: this.editingPasswordPublisherEmail,
      newPassword: this.changePasswordFormGroup.get('newPassword').value,
      confirmNewPassword:
        this.changePasswordFormGroup.get('confirmNewPassword').value,
    };
    this.store.dispatch(new UpdatePasswordPublisher(payload)).subscribe({
      next: () => {
        this.editingPasswordPublisherEmail = undefined;
        this.isChangePasswordModalVisible = false;
        this.changeDetectorRef.detectChanges();
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.success,
            'Đổi mật khẩu thành công'
          )
        );
      },
      error: (err) => {
        this.store.dispatch(
          new ShowGlobalNotification(
            GlobalNotificationEnum.error,
            'Có lỗi xảy ra'
          )
        );
      },
    });
    this.changePasswordFormGroup.reset();
    this.isConfirmChangePasswordModalVisible = false;
  }

  onNextPage() {
    this.currentPage$.pipe(take(1)).subscribe({
      next: (currentPage) => {
        this.store.dispatch(
          new GetMorePublisherList({
            page: currentPage + 1,
            pageSize: this.pageSize,
            filters: this.filterPredicates,
            sorts: this.sorts,
          })
        );
      },
    });
  }

  public filterPredicateComparator(option1: any, option2: any) {
    return (
      option1?.field === option2?.field &&
      option1?.value === option2?.value &&
      option1?.operator === option2?.operator
    );
  }

  public onFilterStatusChange(status: PublisherStatusValue) {
    this.filterStatus = status;
    storage.set(this.statusStorage, this.filterStatus);
    this.store.dispatch(new GetStatusPublisher(status));
    this.store.dispatch(new ClearAllPublishersData());
    this.store.dispatch(
      new GetPublisherList({
        page: 1,
        pageSize: this.pageSize,
        filters: this.filterPredicates,
        sorts: this.sorts,
      })
    );
  }

  public onSearchOptions(options: IPredicateModel[]) {
    this.isUpdated = false;
    this.filterPredicates = options.map((option) => ({
      value: option.value,
      operator: option.operator,
      field: Number(option.field),
    }));
    storage.set(this.searchStorage, this.filterPredicates);
    this.store.dispatch(new ClearAllPublishersData());
    this.store.dispatch(
      new GetPublisherList({
        page: 1,
        pageSize: this.pageSize,
        filters: this.filterPredicates,
        sorts: this.sorts,
      })
    );
  }

  public onFocusSearch() {
    this.isFocus = true;
  }

  public onBlurSearch() {
    this.isFocus = false;
  }
}
