import { Component } from '@angular/core';

@Component({
  selector: 'novanet-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {}
