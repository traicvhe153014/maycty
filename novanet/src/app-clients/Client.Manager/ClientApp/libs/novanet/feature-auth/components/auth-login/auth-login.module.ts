import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLoginComponent } from './auth-login.component';
import { AuthLoginRouting } from './auth-login.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';

const COMPONENTS = [AuthLoginComponent];

const MODULES = [
  CommonModule,
  AuthLoginRouting,
  ReactiveFormsModule,
  NovanetInputModule,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
})
export class AuthLoginModule {}
