import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'login',
        loadChildren: () =>
          import('./auth-login/auth-login.module').then(
            (m) => m.AuthLoginModule
          ),
      },
      {
        path: 'registration',
        loadChildren: () =>
          import('./auth-registration/auth-registration.module').then(
            (m) => m.AuthRegistrationModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthComponentsRouting {}
