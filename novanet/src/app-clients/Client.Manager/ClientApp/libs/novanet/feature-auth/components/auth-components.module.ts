import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthComponentsRouting } from './auth-components.routing';

@NgModule({
  imports: [CommonModule, AuthComponentsRouting, RouterModule],
})
export class AuthComponentsModule {}
