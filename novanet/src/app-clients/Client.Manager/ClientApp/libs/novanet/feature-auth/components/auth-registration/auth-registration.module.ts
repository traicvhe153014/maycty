import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRegistrationComponent } from './auth-registration.component';
import { AuthRegistrationRouting } from './auth-registration.routing';

@NgModule({
  declarations: [AuthRegistrationComponent],
  imports: [CommonModule, AuthRegistrationRouting],
})
export class AuthRegistrationModule {}
