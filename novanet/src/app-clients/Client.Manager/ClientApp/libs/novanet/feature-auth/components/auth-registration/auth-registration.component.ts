import { Component } from '@angular/core';

@Component({
  selector: 'novanet-auth-registration',
  templateUrl: './auth-registration.component.html',
  styleUrls: ['./auth-registration.component.scss'],
})
export class AuthRegistrationComponent {}
