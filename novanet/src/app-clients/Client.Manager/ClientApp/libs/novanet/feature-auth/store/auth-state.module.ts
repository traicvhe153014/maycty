import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './auth-state.service';
import { AuthState } from './auth.state';

@NgModule({
  imports: [NgxsModule.forFeature([AuthState])],
  providers: [AuthService, CookieService],
})
export class AuthStateModule {}
