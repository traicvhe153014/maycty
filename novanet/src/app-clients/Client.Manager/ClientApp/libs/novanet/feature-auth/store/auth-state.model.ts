import { Params } from '@angular/router';

export interface IAuthStateModel<T = any> {
  user: T;
  authorizationChecked: boolean;
}

export interface IAuthTokens {
  accessToken: string;
  refreshToken: string;
}

export interface ILoginOptions extends Params {
  silently?: boolean;
  redirectUrl?: string;
}

export interface ILoginModel {
  username: string;
  password: string;
}
