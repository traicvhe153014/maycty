import {
  Action,
  NgxsOnInit,
  Selector,
  State,
  StateContext,
  Store,
} from '@ngxs/store';

import {
  AuthClear,
  ClearStorages,
  GetAuthTokens,
  GetAuthTokensError,
  GetAuthTokensSuccess,
  Login,
  LoginError,
  LoginSuccess,
  Logout,
  LogoutError,
  LogoutSuccess,
  RedirectToLogin,
  RedirectToMainPage,
  TokensSet,
  UserInformationSet,
} from './auth-state.actions';
import { IAuthStateModel, ILoginModel } from './auth-state.model';
import { AuthService } from './auth-state.service';
import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { User } from '@models';
import { accessTokenKey, refreshTokenKey } from '@core/constants';
import { Router } from '@angular/router';
import {
  CancelDownloading,
  GlobalNotificationEnum,
  ShowGlobalNotification,
  UpdateisNotificationDownloading,
  UpdateIsReportDownloading,
} from '@shared/global-notification';
import { CheckHasAnyProductFeedSuccess } from '@shared/common/store';
import { GetUserSuccess } from '@shared/identity';
import { HideLoading } from '@shared/global-loading';
import {
  ClearAdset,
  ClearAdvertising,
  ClearCampaign,
} from '@features/campaign-management/store';
import { ClearAllProductFeedsData } from '@features/product-feed/store';
import { ClearAllProductsData } from '@features/product-management/store';
import { ClearAllReports } from '@features/feature-report/store';

@Injectable()
@State<IAuthStateModel>({
  name: 'auth',
})
export class AuthState implements NgxsOnInit {
  public readonly accessTokenKey: string = accessTokenKey;
  public readonly refreshTokenKey: string = refreshTokenKey;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly store: Store
  ) {}

  @Selector()
  public static getUser<T>(state: IAuthStateModel<T>): T {
    return state.user;
  }

  @Selector()
  public static getAuthorizationChecked(state: IAuthStateModel): boolean {
    return state.authorizationChecked;
  }

  public ngxsOnInit(ctx: StateContext<IAuthStateModel>): void {
    const sessionToken = localStorage.getItem(this.accessTokenKey);

    if (sessionToken) {
      ctx.dispatch(new GetAuthTokens(sessionToken));
    } else {
    }
  }

  @Action(GetAuthTokens)
  public getAuthTokens(
    ctx: StateContext<IAuthStateModel>,
    { sessionToken }: GetAuthTokens
  ) {
    const userInfoDecoded = jwt_decode(sessionToken) as any;
    const userInfo = new User(
      userInfoDecoded.unique_name,
      userInfoDecoded.email,
      userInfoDecoded.features,
      userInfoDecoded.role
    );

    ctx.dispatch(new UserInformationSet(userInfo));
  }

  @Action(GetAuthTokensSuccess)
  public getAuthTokensSuccess(
    ctx: StateContext<IAuthStateModel>,
    { tokens }: GetAuthTokensSuccess
  ) {
    ctx.dispatch(new TokensSet(tokens));
  }

  @Action(GetAuthTokensError)
  public getAuthTokensError(
    ctx: StateContext<any>,
    { error }: GetAuthTokensError
  ) {}

  @Action(RedirectToLogin)
  public redirectToLogin(
    ctx: StateContext<IAuthStateModel>,
    { options }: RedirectToLogin
  ): void {
    if (!options) {
      ctx.dispatch(new RedirectToMainPage());
      return;
    }

    if (options.redirectUrl !== undefined && options.redirectUrl !== null) {
      this.authService.redirectToUrl(options.redirectUrl);
    }
  }

  @Action(RedirectToMainPage)
  redirectToMainPage() {
    this.authService.redirectToMainPage();
  }

  @Action(Logout)
  public logout(ctx: StateContext<IAuthStateModel>, { options }: Logout): any {
    localStorage.clear();
    ctx.patchState({
      user: null,
    });
    this.store.dispatch(new CheckHasAnyProductFeedSuccess(false));
    this.store.dispatch(new GetUserSuccess(undefined));
    ctx.dispatch(new ClearCampaign());
    ctx.dispatch(new ClearAdset());
    ctx.dispatch(new ClearAdvertising());
    ctx.dispatch(new ClearAllProductFeedsData());
    ctx.dispatch(new ClearAllProductsData());
    ctx.dispatch(new ClearAllReports());
    ctx.dispatch(new UpdateIsReportDownloading(false));
    ctx.dispatch(new UpdateisNotificationDownloading(true));
    ctx.dispatch(new CancelDownloading());
    this.router.navigate(['/auth/login']).then();
  }

  @Action([LogoutSuccess, LogoutError])
  public logoutSuccess(
    ctx: StateContext<IAuthStateModel>,
    { options }: Logout
  ): void {
    ctx.dispatch(new ClearStorages());

    if (!options) {
      ctx.dispatch(new RedirectToMainPage());
      return;
    }

    if (options.redirectUrl !== undefined && options.redirectUrl !== null) {
      this.authService.redirectToUrl(options.redirectUrl);
    }
  }

  @Action(ClearStorages)
  public clearStorages(ctx: StateContext<IAuthStateModel>): void {
    ctx.dispatch(new AuthClear());
  }

  @Action(TokensSet)
  public tokensSet(
    ctx: StateContext<IAuthStateModel>,
    { tokens }: TokensSet
  ): void {
    const { accessToken, refreshToken } = tokens;

    this.authService.setTokens(accessToken, refreshToken);
  }

  @Action(Login)
  public login(ctx: StateContext<ILoginModel>, { loginInfo }: Login) {
    return this.authService.login(loginInfo).subscribe(
      (response) => {
        ctx.dispatch(new LoginSuccess(response));
      },
      (error) => {
        ctx.dispatch(new LoginError(error));
      }
    );
  }

  @Action(LoginSuccess)
  public loginSuccess(
    ctx: StateContext<IAuthStateModel>,
    { token }: LoginSuccess
  ): void {
    const userInfoDecoded = jwt_decode(token.accessToken) as any;
    const userInfo = new User(
      userInfoDecoded.unique_name,
      userInfoDecoded.email,
      userInfoDecoded.features,
      userInfoDecoded.role
    );
    const loginLabel = 'login';
    this.store.dispatch(new HideLoading(loginLabel));

    ctx.dispatch(new UserInformationSet(userInfo));
    ctx.dispatch(new TokensSet(token));
    ctx.dispatch(new RedirectToLogin());
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.success,
        'Đăng nhập thành công'
      )
    );
    if (!token) {
      ctx.dispatch(new RedirectToMainPage());
    }
  }

  @Action(LoginError)
  public loginError(
    ctx: StateContext<IAuthStateModel>,
    { error }: LoginError
  ): void {
    ctx.dispatch(new ClearStorages());
    const loginLabel = 'login';
    this.store.dispatch(new HideLoading(loginLabel));
    ctx.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.error,
        'Đăng nhập không thành công',
        error?.error?.errors?.[0] ??
          'Vui lòng kiểm tra lại tài khoản hoặc mật khẩu'
      )
    );
  }

  @Action(UserInformationSet)
  public userInformationSet(
    ctx: StateContext<IAuthStateModel<User>>,
    { userInfo }: UserInformationSet
  ): void {
    ctx.patchState({ user: userInfo, authorizationChecked: true });
  }
}
