export const rolesMainPage: { [key: string]: string } = {
  Admin: '/product-management',
  Account: '/product-management',
  Accountant: '/recharge/list',
  Publisher: '/corereport/website',
  PublisherManager: 'corereport/publisher',
};

export const defaultMainPage = '/product-management';
