import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth.routing';
import { RouterModule } from '@angular/router';
import { SvgIconModule } from '@core/components/svg-icon';

@NgModule({
  declarations: [AuthComponent],
  imports: [CommonModule, AuthRoutingModule, RouterModule, SvgIconModule],
})
export class AuthModule {}
