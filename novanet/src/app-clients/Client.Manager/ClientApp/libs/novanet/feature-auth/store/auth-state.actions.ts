import { IAuthTokens, ILoginModel, ILoginOptions } from './auth-state.model';
import { User } from '@models';

const enum AuthActions {
  GetAuthTokens = '[Auth] GetAuthTokens',
  GetAuthTokensSuccess = '[Auth] GetAuthTokensSuccess',
  GetAuthTokensError = '[Auth] GetAuthTokensError',
  RedirectToLogin = '[Auth] RedirectToLogin',
  RedirectToMainPage = '[Auth] RedirectToMainPage',
  CheckAuthorization = '[Auth] CheckAuthorization',
  TokensSet = '[Auth] LogoutSuccess',
  ClearStorages = '[Auth] ClearStorages',
  AuthClear = '[Auth] AuthClear',
  Logout = '[Auth] Logout',
  LogoutSuccess = '[Auth] LogoutSuccess',
  LogoutError = '[Auth] LogoutError',
  Login = '[Auth] Login',
  LoginSuccess = '[Auth] LoginSuccess',
  LoginError = '[Auth] LoginError',
  UserInformationSet = '[Auth] UserInformationSet',
}

export class GetAuthTokens {
  static type = AuthActions.GetAuthTokens;

  constructor(public sessionToken: string) {}
}

export class GetAuthTokensSuccess {
  static type = AuthActions.GetAuthTokensSuccess;

  constructor(public tokens: IAuthTokens) {}
}

export class GetAuthTokensError {
  static type = AuthActions.GetAuthTokensError;

  constructor(public error: any) {}
}

export class RedirectToLogin {
  public static type = AuthActions.RedirectToLogin;

  constructor(public options?: ILoginOptions) {}
}

export class RedirectToMainPage {
  static type = AuthActions.RedirectToMainPage;
}

export class CheckAuthorization {
  public static type = AuthActions.CheckAuthorization;
}

export class TokensSet {
  public static type = AuthActions.TokensSet;

  constructor(public tokens: IAuthTokens) {}
}

export class ClearStorages {
  public static type = AuthActions.ClearStorages;
}

export class AuthClear {
  public static type = AuthActions.AuthClear;
}

export class Logout {
  public static type = AuthActions.Logout;

  constructor(public options?: ILoginOptions) {}
}

export class LogoutSuccess extends Logout {
  public static override type = AuthActions.LogoutSuccess;
}

export class LogoutError extends Logout {
  public static override type = AuthActions.LogoutError;
}

export class Login {
  public static type = AuthActions.Login;

  constructor(public loginInfo?: ILoginModel) {}
}

export class LoginSuccess {
  public static type = AuthActions.LoginSuccess;

  constructor(public token: IAuthTokens) {}
}

export class LoginError {
  public static type = AuthActions.LoginError;

  constructor(public error: any) {}
}

export class UserInformationSet {
  public static type = AuthActions.UserInformationSet;

  constructor(public userInfo: User) {}
}
