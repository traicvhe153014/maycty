import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ClearStorages } from './auth-state.actions';
import { IAuthTokens, ILoginModel, ILoginOptions } from './auth-state.model';
import { BaseService, baseUrl } from '@shared/services';
import { ISuccessHttpResponse, IUser, User } from '@models';
import jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import { defaultMainPage, rolesMainPage } from '@features/auth/constants';
import { Policies } from '@core/constants';

const AuthUrls = {
  login: `SignIn`,
};

@Injectable()
export class AuthService extends BaseService {
  public readonly accessTokenKey: string = 'ACCESS_TOKEN';
  public readonly refreshTokenKey: string = 'REFRESH_TOKEN';
  public readonly userDataKey: string = 'USER_DATA';

  private readonly authApiUrls: {
    profile: string;
    validateSessionToken: string;
    logout: string;
    refreshTokens: string;
  };

  constructor(
    httpClient: HttpClient,
    private readonly router: Router,
    private readonly store: Store,
    private readonly cookieService: CookieService,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'identity/api/v1');
  }

  public getAuthTokens(sessionToken: string): Observable<IAuthTokens> {
    return this.httpClient
      .post<any>(this.authApiUrls.validateSessionToken, { token: sessionToken })
      .pipe(map((response) => response.data));
  }

  public setTokens(accessToken: string, refreshToken: string): void {
    this.setAccessToken(accessToken);
    this.setRefreshToken(refreshToken);
  }

  public getAccessToken(): string {
    return localStorage.getItem(this.accessTokenKey);
  }

  public getRefreshToken(): string {
    return localStorage.getItem(this.refreshTokenKey);
  }

  public isAccessTokenExpired(): boolean {
    try {
      const accessToken = this.getAccessToken();
      const expiry =
        accessToken && JSON.parse(atob(accessToken.split('.')[1])).exp;

      return Math.floor(new Date().getTime() / 1000) >= expiry;
    } catch (e) {
      return true;
    }
  }

  public refreshTokens(loginOptions?: ILoginOptions): Observable<any> {
    const savedRefreshToken = this.getRefreshToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${savedRefreshToken}`,
      }),
    };

    return this.httpClient
      .post(this.authApiUrls.refreshTokens, null, options)
      .pipe(
        tap(({ data }: any) => {
          const { accessToken, refreshToken } = data;

          this.setTokens(accessToken, refreshToken);
        }),
        catchError((err: any) => {
          this.store.dispatch(new ClearStorages());
          this.redirectToLogin(loginOptions);

          return throwError(err);
        })
      );
  }

  public redirectToMainPage(): void {
    const url = rolesMainPage[this.getUserData()?.roles] ?? defaultMainPage;
    this.router.navigate([url]).then();
  }

  public login(loginInfo: ILoginModel): Observable<IAuthTokens> {
    return this.httpClient
      .post<ISuccessHttpResponse<IAuthTokens>>(
        this.createUrl(AuthUrls.login),
        loginInfo
      )
      .pipe(map((response) => response.data));
  }

  public redirectToLogin(options?: ILoginOptions) {
    this.router.navigate(['/auth/login']).then();
  }

  public redirectToUrl(url: string): void {
    this.router.navigate([url]).then();
  }

  public logout(): Observable<null> {
    return this.httpClient.post<null>(this.authApiUrls.logout, null);
  }

  public getUser<T = any>(params: ILoginOptions): Observable<T> {
    return this.httpClient
      .get<any>(this.authApiUrls.profile, { params })
      .pipe(map((response) => response.data));
  }

  public decodeToken(token: string) {
    const userData = jwt_decode<IUser>(token);
    const user = new User(
      userData.aud,
      userData.fullname,
      userData.email,
      userData.exp,
      userData.iat,
      userData.id,
      userData.iss,
      userData.nbf,
      userData.policies,
      userData.roles,
      userData.username
    );
    localStorage.setItem(this.userDataKey, JSON.stringify(user));
    return user;
  }

  public getUserData(): User {
    const userData = localStorage.getItem(this.userDataKey);
    return JSON.parse(userData) as User;
  }

  public getUserPolicies(user: User): string[] {
    return user.policies.map((x) => Policies[x]);
  }

  private setAccessToken(token: string): void {
    const user = this.decodeToken(token);
    this.cookieService.set('Authorization', `Bearer ${token}`);
    localStorage.setItem(this.accessTokenKey, token);
  }

  private setRefreshToken(token: string): void {
    localStorage.setItem(this.refreshTokenKey, token);
  }
}
