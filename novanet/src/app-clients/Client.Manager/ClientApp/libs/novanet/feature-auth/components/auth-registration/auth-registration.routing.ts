import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthRegistrationComponent } from './auth-registration.component';

const routes: Routes = [
  {
    path: '',
    component: AuthRegistrationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRegistrationRouting {}
