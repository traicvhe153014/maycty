import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailPattern } from './constants';
import { Store } from '@ngxs/store';
import { IFormFieldErrors, IFormFields } from '@models';
import { ILoginModel, Login } from '@core/auth';
import { InputTypeEnum } from '@core/enums';
import { ShowLoading } from '@shared/global-loading';

@Component({
  selector: 'novanet-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss'],
})
export class AuthLoginComponent implements OnInit {
  public loginForm: FormGroup;
  public inputTypeEnum = InputTypeEnum;
  public passwordType = InputTypeEnum.PASSWORD;

  public readonly loginMemberFormFields: IFormFields = {
    email: {
      name: 'email',
      validationParams: {
        maxLength: 50,
        required: true,
        pattern: EmailPattern,
      },
    },
    password: {
      name: 'password',
      validationParams: {
        maxLength: 50,
        required: true,
      },
    },
  };

  public readonly loginMemberFormFieldErrors: IFormFieldErrors = {
    email: [
      {
        type: 'pattern',
        message: 'Tài khoản không hợp lệ.',
      },
      {
        type: 'required',
        message: 'Không để trống tài khoản.',
      },
    ],
    password: [
      {
        type: 'required',
        message: 'Không để trống mật khẩu',
      },
    ],
  };

  constructor(private formBuilder: FormBuilder, private store: Store) {}

  get officialEmail() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  ngOnInit(): void {
    this.buildLoginForm();
  }

  public onSubmitLoginForm() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    const loginLabel = 'login';
    this.store.dispatch(new ShowLoading(loginLabel));
    this.store.dispatch(new Login(this.loginForm.value as ILoginModel));
  }

  public changePasswordType(type: InputTypeEnum) {
    this.passwordType = type;
  }

  private buildLoginForm() {
    const config = {
      [this.loginMemberFormFields['email'].name]: [
        '',
        [
          this.loginMemberFormFields['email'].validationParams.required
            ? Validators.required
            : null,
          Validators.maxLength(
            this.loginMemberFormFields['email'].validationParams
              .maxLength as number
          ),
          Validators.pattern(
            this.loginMemberFormFields['email'].validationParams.pattern
          ),
        ],
      ],
      [this.loginMemberFormFields['password'].name]: [
        '',
        [
          this.loginMemberFormFields['password'].validationParams.required
            ? Validators.required
            : null,
          Validators.maxLength(
            this.loginMemberFormFields['password'].validationParams
              .maxLength as number
          ),
        ],
      ],
    };
    this.loginForm = this.formBuilder.group(config);
  }
}
