export interface IProductManagementStatus {
  id: number;
  label: string;
}
