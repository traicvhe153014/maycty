import {
  IProductAttributeResponse,
  IProductAttributeValueResponse,
  IProductResponse,
  IProductSummary,
} from '@features/product-management/_index';

export interface IProductManagementState {
  productAttributesState: IProductAttribute;
  productAttributeValuesState: IProductAttributeValue;
  products: IProductResponse[];
  page: number;
  loading: boolean;
  hasMorePages: boolean;
  overview: IProductOverviewResponse;
  updatedProduct?: IProductResponse;
  productSummary?: IProductSummary;
}

export interface IProductAttribute {
  productAttributes: IProductAttributeResponse[];
  page?: number;
  loading: boolean;
  hasMorePages?: boolean;
}

export interface IProductAttributeValue {
  productAttributeValues: IProductAttributeValueResponse[];
  summary: IProductSummary;
  page?: number;
  loading: boolean;
  hasMorePages?: boolean;
}

export interface IProductOverviewResponse {
  total: number;
  running?: number;
  deactivated?: number;
  error?: number;
  outOfStock?: number;
}

export interface IProductUpdateRequest {
  id: string;
  isActive: boolean;
}
