import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductStatusIconComponent } from './product-status-icon.component';

@NgModule({
  declarations: [ProductStatusIconComponent],
  imports: [CommonModule],
  exports: [ProductStatusIconComponent],
})
export class ProductStatusIconModule {}
