import { ClearObjectGroups } from '@features/feature-object-management/store';
import {
  GetMoreProductFeedList,
  GetProductFeedDetail,
  GetProductFeedList,
  IProductFeedResponse,
  IProductFeedStateModel,
  ProductFeedState,
} from '@features/product-feed/store';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ESortType, PredicateOperatorEnum } from '@core/enums';
import {
  productCards,
  ProductManagementSetting,
  productSearchLevelFields,
  productSearchLevelLabels,
  productSearchOperatorLabels,
  ProductStateOption,
  ProductStatusOption,
  productSummaryColumns,
} from '@features/product-management/constants/product-management.constant';
import { Select, Store } from '@ngxs/store';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ExportApi,
  ExportProductReport,
  GlobalNotificationEnum,
  GlobalNotificationState,
  ShowGlobalNotification,
  UpdateIsReportDownloading,
} from '@shared/global-notification';
import {
  ClearAllProductsData,
  GetMoreProductList,
  GetOverviewProducts,
  GetProducts,
  IProductManagementState,
  IProductOverviewResponse,
  ProductManagementState,
  UpdateProduct,
} from '@features/product-management/store';
import { ProductManagementColumn } from '@features/product-management/constants/product-management-column.metadata';
import { metricSortFields } from '@core';
import { IProductSearchOption } from '@features/product-management/types/product-group.type';
import {
  EProductOverview,
  ProductManagementEnum,
  ProductManagementSummaryEnum,
  productSearchLevelEnum,
} from '@features/product-management/enums';
import { IProductGroupManagementData } from '@features/product-group-management/models';
import {
  DataTableSettingModel,
  IFieldDetail,
  ISettingColumnTable,
} from '@data-table/models';
import {
  IProductResponse,
  IProductSummary,
} from '@features/product-management/models/product-management-table.model';
import {
  CampaignState,
  GetCampaignList,
  GetMoreCampaignList,
  ICampaignResponse,
} from '@features/campaign-management/store';
import { cloneDeep } from 'lodash';
import { Observable, Subject, timer } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { switchMap, take, takeUntil } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd/modal';
import { IHttpGetRequest, IPredicateModel } from '@core/models';
import { productFallback } from '@features/product-management/constants/product-fallback.constant';

const shortDateFormat = 'dd/MM/yyyy';

@Component({
  selector: 'novanet-product-management-table',
  templateUrl: './product-management-table.component.html',
  styleUrls: ['./product-management-table.component.scss'],
})
export class ProductManagementTableComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @Select(ProductFeedState.getList)
  public $productFeeds: Observable<IProductFeedResponse[]>;

  @Select(ProductFeedState.getDetail)
  public $detailedProductFeed: Observable<IProductFeedResponse>;

  @Select(ProductFeedState.getLoading)
  public $loadingProductFeedsStatus: Observable<boolean>;

  @Select(ProductManagementState.getList)
  public $products: Observable<IProductResponse[]>;

  @Select(ProductManagementState.getOverview)
  public $productOverview: Observable<IProductOverviewResponse>;

  @Select(ProductManagementState.getLoadingStatus)
  public $loadingStatus: Observable<boolean>;

  public settings: DataTableSettingModel;
  public searchForm: FormGroup;
  public changeProductGroupForm: FormGroup;
  public scrollX: string | null;
  public scrollY: string | null;
  public queryParamProductFeedId: string | null;
  public editEnabled = false;
  public showAdvancedFilter = false;
  public products: IProductResponse[] = [];
  public productCardsInformation = cloneDeep(productCards);
  public productFeedsResponse: IProductFeedResponse[] = [];
  public readonly summaryColumns = productSummaryColumns;
  public filterDateRange: Date[] = [];
  public productDetail: IProductResponse;
  public selectedSearchOptions: IProductSearchOption[] = [];
  public searchDropdownOpen = false;
  public readonly shortDateFormat = shortDateFormat;
  public productSearchLevelLabels = productSearchLevelLabels;
  public productSearchLevelFields = productSearchLevelFields;
  public searchOptions = [];
  public filterPredicates = [];
  public productSearchOperatorLabels = productSearchOperatorLabels;
  public sorts: string[] | null = [
    'ProductEntity.Status',
    '-ProductEntity.ModifiedAt',
  ];
  public baseSorts: string[] | null = [
    'ProductEntity.Status',
    '-ProductEntity.ModifiedAt',
  ];
  public isProductSelectedAll = false;
  public isFocus = false;
  public productSearchLevelEnum = productSearchLevelEnum;
  @ViewChild('checkboxHeader') checkboxHeader: TemplateRef<any>;

  public fallback = productFallback;

  @Select(ProductManagementState.getProductSummary)
  public productSummary$: Observable<IProductSummary>;
  @Select(CampaignState.getList)
  campaigns$: Observable<ICampaignResponse[]>;
  @Select(CampaignState.getPage) campaignCurrentPage$: Observable<number>;

  public columns: ISettingColumnTable<any, any>[] = ProductManagementColumn;
  public ListSelectedReportOptions = [
    {
      valueKey: null,
      label: 'Tải xuống báo cáo sản phẩm',
    },
  ];

  public popoverNotification: string;

  @Select(GlobalNotificationState.isReportDownloading)
  isReportDownloading$: Observable<boolean>;

  public readonly productManagementEnum = ProductManagementEnum;
  public readonly productManagementSummaryEnum = ProductManagementSummaryEnum;

  public readonly globalNotificationEnum = GlobalNotificationEnum;

  public readonly statusOption = ProductStatusOption;
  public readonly productStatesOption = ProductStateOption;
  private readonly destroy$: Subject<void> = new Subject<void>();

  private loading = false;
  private isUpdated = false;
  private skip = 0;
  private page = 1;
  private pageSize = 10;
  private fields = [
    'Title',
    'ImageLink',
    'Price',
    'ProductType',
    'Id',
    'Views',
    'CTR',
    'CPC',
    'CPS',
    'Modified',
    'Conversion',
    'PurchaseQuantity',
    'ProductState',
    'ProductStatus',
    'IsActivated',
  ];
  private productsLoaded = false;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private changeDetectorRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private modal: NzModalService
  ) {
    this.queryParamProductFeedId =
      activatedRoute.snapshot.queryParamMap.get('product-feed');
  }

  get productFeedId(): FormControl {
    return this.searchForm.get('productFeedId') as FormControl;
  }

  get isSelected(): boolean {
    return !!this.products.find((x) => x.checked);
  }

  get status(): FormControl {
    return this.searchForm.get('status') as FormControl;
  }

  get Availability(): FormControl {
    return this.searchForm.get('availability') as FormControl;
  }

  get campaign(): FormControl {
    return this.searchForm.get('campaign') as FormControl;
  }

  public get isAnySelected(): boolean {
    return this.products.some((product) => product.checked);
  }

  public get countSelected(): number {
    return this.products.filter((product) => product.checked).length;
  }

  ngOnInit(): void {
    this.$productFeeds
      .pipe(takeUntil(this.destroy$))
      .subscribe((productFeeds) => {
        if (productFeeds) {
          this.productFeedsResponse = cloneDeep(productFeeds);
        }
      });
    this.$detailedProductFeed.pipe(takeUntil(this.destroy$)).subscribe({
      next: (productFeed) => {
        if (!productFeed) {
          return;
        }
        this.productFeedsResponse = [
          cloneDeep(productFeed),
          ...this.productFeedsResponse,
        ];
        this.changeDetectorRef.detectChanges();
      },
    });
    this.getOverview();
    this.getProductFeeds();
    this.getCampaigns();
    this.$products.pipe(takeUntil(this.destroy$)).subscribe((products) => {
      if (products && !this.isUpdated) {
        const checkedProduct = this.products
          .filter((x) => x.checked)
          .map((x) => x.productId);
        this.products = cloneDeep(products);
        checkedProduct.forEach((x) => {
          const exist = this.products.find(
            (product) => product.productId === x
          );
          if (exist) {
            exist.checked = true;
          }
        });
        this.changeDetectorRef.detectChanges();
      }
    });
    this.$loadingStatus.pipe(takeUntil(this.destroy$)).subscribe((status) => {
      this.loading = status;
    });
    this.buildSearchForm();
    const params = {
      page: this.page,
      skip: this.skip,
      pageSize: this.pageSize,
      fields: this.fields,
      withReport: true,
      status: this.searchForm.value.status,
      productFeedIds: this.searchForm.value.productFeedId
        ? this.searchForm.value.productFeedId
        : this.queryParamProductFeedId,
      sorts: this.sorts,
    } as IHttpGetRequest;
    this.store.dispatch(new GetProducts(params));
    this.settings = ProductManagementSetting;
    if (this.queryParamProductFeedId) {
      this.showAdvancedFilter = true;
    }
  }

  ngAfterViewInit(): void {
    this.columns = ProductManagementColumn.map((item) =>
      item.id !== ProductManagementEnum.CHECKBOX
        ? item
        : {
            ...item,
            title: this.checkboxHeader,
          }
    );
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(new ClearObjectGroups());
    this.store.dispatch(new ClearAllProductsData());
    this.productsLoaded = false;
  }

  public sortProducts(values?: IFieldDetail[]) {
    if (values.length) {
      if (values.find((value) => metricSortFields[value.name])) {
        const columnToSort = values[values.length - 1];
        this.columns.forEach((column) => {
          if (column.name === columnToSort.name) {
            switch (columnToSort.direction) {
              case ESortType.Ascending:
                this.sorts = [columnToSort.name];
                column.ascending = true;
                column.descending = false;
                break;
              case ESortType.Descending:
                this.sorts = ['-' + columnToSort.name];
                column.ascending = false;
                column.descending = true;
                break;
            }
          } else {
            column.ascending = false;
            column.descending = false;
          }
        });
      } else {
        const sorts = [];
        values.forEach((value) => {
          const exist = this.columns.find((x) => x.name === value.name);
          this.columns.find((column) => {
            const names = values.map((x) => x.name);
            if (!names.includes(column.name)) {
              column.ascending = false;
              column.descending = false;
            }
          });
          if (exist) {
            switch (value.direction) {
              case ESortType.Ascending:
                sorts.push(value.name);
                exist.ascending = true;
                exist.descending = false;
                break;
              case ESortType.Descending:
                sorts.push('-' + value.name);
                exist.ascending = false;
                exist.descending = true;
                break;
            }
          }
        });
        this.sorts = sorts;
        this.changeDetectorRef.detectChanges();
      }
    } else {
      this.columns.forEach((column) => {
        column.ascending = false;
        column.descending = false;
      });
      this.sorts = this.baseSorts;
    }
    this.onSearch();
    this.isUpdated = false;
  }

  public onSearch() {
    this.store.dispatch(new ClearAllProductsData());
    this.isUpdated = false;
    this.products = [];
    this.productsLoaded = false;
    this.skip = 0;
    this.page = 1;
    const params = {
      page: 1,
      skip: 0,
      pageSize: this.pageSize,
      keyWord: this.searchForm.value.search,
      fields: this.fields,
      productFeedIds: this.searchForm.value.productFeedId,
      status: this.searchForm.value.status,
      availability: this.searchForm.value.availability,
      campaignId: this.searchForm.value.campaign,
      endDate: this.filterDateRange?.length
        ? this.filterDateRange[1].toISOString()
        : null,
      startDate: this.filterDateRange?.length
        ? this.filterDateRange[0].toISOString()
        : null,
      withReport: true,
      sorts: this.sorts,
      filters: JSON.stringify(this.filterPredicates),
    } as IHttpGetRequest;
    this.store.dispatch(new GetProducts(params));
  }

  public onReload() {
    this.filterDateRange = [];
    this.searchForm.reset();
  }

  public checkAll(value: boolean) {
    this.products.forEach((data) => {
      data.checked = value;
    });
    this.changeDetectorRef.detectChanges();
  }

  public refreshStatus(_event: { id: string; checked: boolean }) {
    const existItem = this.products.find((item) => item.data.Id === _event.id);
    if (existItem) {
      existItem.checked = _event.checked;
    }
    this.changeDetectorRef.detectChanges();
  }

  public openProductDetail(tplContent: TemplateRef<any>, product) {
    this.productDetail = product;
    this.modal.create({
      nzContent: tplContent,
      nzFooter: null,
      nzTitle: null,
      nzWidth: '65%',
      nzStyle: {
        'border-radius': '8px',
      },
    });
  }

  public onToggleEdit(data: IProductGroupManagementData): void {
    if (this.editEnabled && !data.enableChangeValue) {
      this.store.dispatch(
        new ShowGlobalNotification(
          this.globalNotificationEnum.warning,
          'Cảnh báo',
          'Hãy lưu dữ liệu trước khi sửa nhóm sản phẩm khác'
        )
      );
      return;
    }
    data.enableChangeValue = !data.enableChangeValue;
    this.editEnabled = !this.editEnabled;
    if (data.enableChangeValue) {
      this.buildProductGroupForm(data);
    } else {
      this.changeProductGroupForm.reset();
    }
    this.changeDetectorRef.detectChanges();
  }

  public onSubmitChangeProductGroup() {}

  public onToggleAdvancedFilter() {
    this.showAdvancedFilter = !this.showAdvancedFilter;
  }

  public nextBatch($event: boolean) {
    if ($event && !this.loading && !this.productsLoaded) {
      this.isUpdated = false;
      this.page += 1;
      this.skip = this.skip + this.pageSize;
      const params = {
        page: this.page,
        skip: this.skip,
        pageSize: this.pageSize,
        fields: this.fields,
        keyWord: this.searchForm.value.search,
        availability: this.searchForm.value.availability,
        campaignId: this.searchForm.value.campaign,
        productFeedIds: this.searchForm.value.productFeedId,
        status: this.searchForm.value.status,
        endDate: this.filterDateRange.length
          ? this.filterDateRange[1].toISOString()
          : null,
        startDate: this.filterDateRange.length
          ? this.filterDateRange[0].toISOString()
          : null,
        withReport: true,
        sorts: this.sorts,
        filters: JSON.stringify(this.filterPredicates),
      } as IHttpGetRequest;
      this.store.dispatch(new GetMoreProductList(params));
    }
  }

  public getNextProductFeedsBatch($event: IHttpGetRequest) {
    this.$loadingProductFeedsStatus.pipe(take(1)).subscribe({
      next: (loading) => {
        if ($event && !loading) {
          this.store.dispatch(
            new GetMoreProductFeedList({
              page: $event.page,
              pageSize: $event.pageSize,
              keyword: '',
              sorts: ['name'],
            })
          );
        }
      },
    });
  }

  public getNextCampaignsBatch($event: IHttpGetRequest) {
    this.campaignCurrentPage$.pipe(take(1)).subscribe({
      next: (loading) => {
        if ($event && !loading) {
          this.store.dispatch(
            new GetMoreCampaignList({
              page: $event.page,
              pageSize: $event.pageSize,
              withReport: false,
              withSummary: false,
            })
          );
        }
      },
    });
  }

  public onRedirectPage() {
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.info,
        'Thông báo',
        'Bạn cần thêm thông tin sản phẩm vào trong nguồn dữ liệu'
      )
    );
  }

  public updateProductIsActive(event: boolean, product: IProductResponse) {
    if (product.isLoading) {
      return;
    }
    product.isLoading = true;
    this.isUpdated = false;
    this.store
      .dispatch(new UpdateProduct({ id: product.productId, isActive: event }))
      .subscribe({
        next: (state) => {
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.success,
              'Cập nhật sản phẩm thành công'
            )
          );
          const updatedProduct = (
            state.ProductManagementState as IProductManagementState
          ).updatedProduct;

          product.isActive = event;
          product.isLoading = false;
          product.status = updatedProduct?.status ?? product.status;
          this.store.dispatch(new GetOverviewProducts());
          this.changeDetectorRef.detectChanges();
        },
        error: () => {
          product.isLoading = false;
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              'Xảy ra lỗi khi cập nhật sản phẩm'
            )
          );
          this.changeDetectorRef.detectChanges();
        },
      });
  }

  public onSearchInput(value: any): void {
    if (!value) {
      this.searchOptions = [];
      this.changeDetectorRef.detectChanges();
      return;
    }
    this.searchOptions = Object.values(productSearchLevelEnum)
      .filter(isNaN)
      .map((level) => ({
        level: productSearchLevelEnum[level],
        operator: PredicateOperatorEnum.Contains,
        value,
      }));
    this.changeDetectorRef.detectChanges();
  }

  public onSelectProductChanged(event: Event, id: string) {
    this.products.forEach((data) => {
      if (data.productId === id) {
        data.checked = (event.target as HTMLInputElement).checked;
      }
    });
    this.changeDetectorRef.detectChanges();
  }

  public isProductSelected(id: string) {
    return this.products.find((productId) => productId.productId === id)
      .checked;
  }

  public onSelectAllProductChanged(event: Event) {
    this.isProductSelectedAll = (event.target as HTMLInputElement).checked;
    this.products.forEach((data) => {
      data.checked = (event.target as HTMLInputElement).checked;
    });
    this.changeDetectorRef.detectChanges();
  }

  public onExportData($event) {
    this.store.dispatch(
      new ShowGlobalNotification(
        GlobalNotificationEnum.download,
        `Đang tải xuống báo cáo`,
        `Quá trình tải xuống có thể mất nhiều thời gian.
              Vui lòng không reload lại trang và duy trì mạng ổn định.`
      )
    );
    this.store.dispatch(new UpdateIsReportDownloading(true));
    const checkedProduct = this.products
      .filter((x) => x.checked)
      .map((x) => x.productId);
    const param = {
      startDate: this.filterDateRange[0]?.toISOString(),
      endDate: this.filterDateRange[1]?.toISOString(),
      exportApi: ExportApi.Product,
      productIds: checkedProduct,
    } as IHttpGetRequest;

    this.store.dispatch(new ExportProductReport(param));
  }

  private getProductFeeds() {
    const params = {
      page: this.page,
      skip: this.skip,
      pageSize: this.pageSize,
      keyWord: '',
      sorts: ['name'],
      endDate: this.filterDateRange?.length
        ? this.filterDateRange[1].toISOString()
        : null,
      startDate: this.filterDateRange?.length
        ? this.filterDateRange[0].toISOString()
        : null,
      withReport: true,
    } as IHttpGetRequest;
    this.store
      .dispatch(
        new GetProductFeedList({
          page: params.page,
          pageSize: params.pageSize,
          keyword: params.keyWord,
          sorts: params.sorts,
        })
      )
      .pipe(take(1))
      .subscribe({
        next: (state) => {
          if (!this.queryParamProductFeedId) {
            return;
          }
          const queryProductFeed = (
            state.productFeed as IProductFeedStateModel
          ).productFeeds.find(
            (item) => item.id === this.queryParamProductFeedId
          );
          if (queryProductFeed) {
            return;
          }
          this.store.dispatch(
            new GetProductFeedDetail(this.queryParamProductFeedId, false)
          );
        },
      });
  }

  private getCampaigns() {
    this.store.dispatch(
      new GetCampaignList({
        page: this.page,
        pageSize: this.pageSize,
        withSummary: false,
        withReport: false,
      })
    );
  }

  private getOverview() {
    this.$productOverview
      .pipe(takeUntil(this.destroy$))
      .subscribe((overview) => {
        this.productCardsInformation.forEach((card) => {
          switch (card.id) {
            case EProductOverview.deactivated:
              card.records = overview?.deactivated;
              card.percent = (card.records / overview?.total) * 100;
              break;
            case EProductOverview.error:
              card.records = overview?.error;
              card.percent = (card.records / overview?.total) * 100;
              break;
            case EProductOverview.total:
              card.records = overview?.total;
              break;
            case EProductOverview.outOfStock:
              card.records = overview?.outOfStock;
              card.percent = (card.records / overview?.total) * 100;
              break;
            case EProductOverview.running:
              card.records = overview?.running;
              card.percent = (card.records / overview?.total) * 100;
              break;
          }
        });
        // this.changeDetectorRef.detectChanges();
      });
    this.store.dispatch(new GetOverviewProducts());
  }

  private buildSearchForm() {
    const config = {
      ['search']: [''],
      ['date']: [[]],
      ['productFeedId']: [
        this.queryParamProductFeedId ? this.queryParamProductFeedId : '',
      ],
      ['status']: [null],
      ['availability']: [null],
      ['campaign']: [null],
    };
    this.searchForm = this.formBuilder.group(config);
    this.searchForm
      .get('date')
      .valueChanges.pipe(
        switchMap((result) => timer((this.filterDateRange = result)))
      )
      .subscribe(() => {
        this.onSearch();
      });
    this.productFeedId.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        setTimeout(() => {
          this.onSearch();
        }, 100);
      });
    this.status.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        setTimeout(() => {
          this.onSearch();
        }, 100);
      });
    this.Availability.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        setTimeout(() => {
          this.onSearch();
        }, 100);
      });
    this.campaign.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        setTimeout(() => {
          this.onSearch();
        }, 100);
      });
  }

  private buildProductGroupForm(data: IProductGroupManagementData) {
    const config = {
      ['id']: [data.id, [Validators.required]],
      ['value']: [data.productFeed, [Validators.required]],
    };
    this.changeProductGroupForm = this.formBuilder.group(config);
  }

  public onSearchOptions(options: IPredicateModel[]) {
    this.filterPredicates = options;
    this.isUpdated = false;
    this.page = 1;
    this.skip = 0;
    this.store.dispatch(new ClearAllProductsData());
    const params = {
      page: 1,
      pageSize: this.pageSize,
      fields: this.fields,
      keyWord: this.searchForm.value.search,
      availability: this.searchForm.value.availability,
      campaignId: this.searchForm.value.campaign,
      productFeedIds: this.searchForm.value.productFeedId,
      status: this.searchForm.value.status,
      endDate: this.filterDateRange.length
        ? this.filterDateRange[1].toISOString()
        : null,
      startDate: this.filterDateRange.length
        ? this.filterDateRange[0].toISOString()
        : null,
      withReport: true,
      sorts: this.sorts,
      filters: JSON.stringify(this.filterPredicates),
    } as IHttpGetRequest;
    this.store.dispatch(new GetProducts(params)).subscribe({
      next: () => {
        this.changeDetectorRef.detectChanges();
      },
    });
  }

  public onFocusSearch() {
    this.isFocus = true;
  }

  public onBlurSearch() {
    this.isFocus = false;
  }
}
