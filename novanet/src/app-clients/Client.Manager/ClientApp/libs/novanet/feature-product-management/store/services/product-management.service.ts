import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { IHttpGetRequest } from '@core/models';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {
  IListProductResponse,
  IProductResponse,
} from '@features/product-management/models/_index';
import { Observable } from 'rxjs';
import {
  IProductOverviewResponse,
  IProductUpdateRequest,
} from '@features/product-management/store';
import { ICampaignListRequest } from '@features/campaign-management/store';

const ProductUrls = {
  list: `list`,
  overview: 'overview',
  update: 'update',
  exportDataToExcel: 'ExportDataToExcel',
};

@Injectable()
export class ProductManagementService extends BaseService {
  constructor(
    httpClient: HttpClient,
    private readonly router: Router,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1/Product');
  }

  public getProducts(data: IHttpGetRequest) {
    const params = this.createParams(data);
    return this.httpClient
      .get<ISuccessHttpResponse<IListProductResponse>>(
        this.createUrl(ProductUrls.list),
        {
          params,
        }
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public getOverview(): Observable<IProductOverviewResponse> {
    return this.httpClient
      .get<ISuccessHttpResponse<IProductOverviewResponse>>(
        this.createUrl(ProductUrls.overview)
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }

  public update(
    payload: Partial<IProductUpdateRequest>
  ): Observable<IProductResponse> {
    return this.httpClient
      .put<ISuccessHttpResponse<IProductResponse>>(
        this.createUrl(ProductUrls.update),
        payload
      )
      .pipe(map((response) => response.data));
  }

  public exportExcel(params: IHttpGetRequest) {
    const processedParams = {
      ...params,
      startDate: params.startDate,
      endDate: params.endDate,
    };
    return this.httpClient.post<any>(
      this.createUrl(ProductUrls.exportDataToExcel),
      processedParams
    );
  }
}
