export enum EProductStatus {
  PRODUCT_RUNNING,
  OUT_OF_STOCK,
  DISABLED,
}
