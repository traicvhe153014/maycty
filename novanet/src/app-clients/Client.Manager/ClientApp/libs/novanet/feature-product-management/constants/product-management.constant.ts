import { ICardData, IProductResponse } from '../models/_index';
import { DataTableSettingModel, ISummaryColumnTable } from '@data-table/models';
import { IOption } from '@shared/custom-input/components/dropdown/models/_index';
import {
  EProductOverview,
  EProductStatus,
  ProductManagementSummaryEnum,
  productSearchLevelEnum,
} from '@features/product-management/enums';
import { PredicateOperatorEnum } from '@core/enums';

export const productCards = [
  {
    id: EProductOverview.total,
    title: 'Tổng số sản phẩm',
    records: 0,
    percent: 0,
    bgColor: '#fff',
    textColor: '#0061C1',
  },
  {
    id: EProductOverview.running,
    title: 'Sản phẩm thực chạy',
    records: 0,
    percent: 0,
    bgColor: '#fff',
    textColor: '#219653',
  },
  {
    id: EProductOverview.outOfStock,
    title: 'Sản phẩm hết hàng',
    records: 0,
    percent: 0,
    bgColor: '#fff',
    textColor: '#F2994A',
  },
  {
    id: EProductOverview.deactivated,
    title: 'Sản phẩm đã tắt',
    records: 0,
    percent: 0,
    bgColor: '#fff',
    textColor: '#4F4F4F',
  },
  {
    id: EProductOverview.error,
    title: 'Sán phẩm bị lỗi dữ liệu',
    records: 0,
    percent: 0,
    bgColor: '#fff',
    textColor: '#EB5757',
  },
] as ICardData[];

export const ProductManagementSetting = {
  id: 'product-management',
  bordered: false,
  loading: false,
  pagination: false,
  sizeChanger: false,
  title: false,
  header: true,
  footer: false,
  expandable: false,
  checkbox: false,
  fixHeader: true,
  noResult: false,
  ellipsis: false,
  simple: false,
  summaryRow: true,
  size: 'small',
  tableLayout: 'auto',
  position: 'bottom',
  paginationType: 'default',
  tableScroll: 'unset',
  scrollX: '3020px',
} as DataTableSettingModel;

export const ProductStatusOption = [
  {
    id: EProductStatus.PRODUCT_RUNNING,
    label: 'Thực chạy',
  },
  {
    id: EProductStatus.OUT_OF_STOCK,
    label: 'Hết hàng',
  },
  {
    id: EProductStatus.DISABLED,
    label: 'Đã tắt',
  },
] as IOption[];

export const ProductStateOption = [
  {
    id: 'in_stock',
    label: 'Còn hàng',
  },
  {
    id: 'out_of_stock',
    label: 'Hết hàng',
  },
  {
    id: 'preorder',
    label: 'Đặt hàng trước',
  },
  {
    id: 'backorder',
    label: 'Giao sau',
  },
] as IOption[];

export const productSummaryColumns: ISummaryColumnTable<IProductResponse>[] = [
  {
    id: ProductManagementSummaryEnum.COUNT,
    colspan: 5,
    pinLeft: true,
  },
  {
    id: ProductManagementSummaryEnum.AD_RUNNING,
  },
  {
    id: ProductManagementSummaryEnum.PRODUCT_STATUS,
  },
  {
    id: ProductManagementSummaryEnum.PRODUCT_STATE,
  },
  {
    id: ProductManagementSummaryEnum.COST,
  },
  {
    id: ProductManagementSummaryEnum.VIEWS,
  },
  {
    id: ProductManagementSummaryEnum.CLICKS,
  },
  {
    id: ProductManagementSummaryEnum.CTR,
  },
  {
    id: ProductManagementSummaryEnum.CPC,
  },
  {
    id: ProductManagementSummaryEnum.PURCHASE_QUANTITY,
  },
  {
    id: ProductManagementSummaryEnum.CONVERSION,
  },
  {
    id: ProductManagementSummaryEnum.CPS,
  },
  {
    id: ProductManagementSummaryEnum.PRODUCT_TYPE,
  },
  {
    id: ProductManagementSummaryEnum.MODIFIED,
  },
  {
    id: ProductManagementSummaryEnum.PRODUCT_FEED,
  },
];


export const productSearchLevelLabels = {
  [productSearchLevelEnum.title]: 'Tiêu đề',
  [productSearchLevelEnum.type]: 'Loại sản phẩm',
  [productSearchLevelEnum.source]: 'Nguồn dữ liệu',
  [productSearchLevelEnum.id]: 'Id sản phẩm',
};

export const productSearchLevelFields = {
  [productSearchLevelEnum.title]: 'title',
  [productSearchLevelEnum.type]: 'type',
  [productSearchLevelEnum.source]: 'source',
  [productSearchLevelEnum.id]: 'Id',
};

export const productSearchOperatorLabels = {
  [PredicateOperatorEnum.Contains]: 'Bao gồm',
};
