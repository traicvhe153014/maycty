import { Component, Input } from '@angular/core';
import { ICampaignResponse } from '@features/campaign-management/store';

@Component({
  selector: 'novanet-product-running-campaign',
  templateUrl: './product-running-campaigns.component.html',
})
export class ProductRunningCampaignsComponent {
  @Input() runningCampaigns: ICampaignResponse[] | undefined;
  public isShowMore = false;

  public setShowMore(value: boolean) {
    this.isShowMore = value;
  }
}
