import { IHttpGetRequest } from '@core/models';
import {
  IListProductAttributeValueResponse,
  IListProductResponse,
  IProductAttributeResponse,
  IProductOverviewResponse,
  IProductResponse,
  IProductUpdateRequest,
} from '@features/product-management/_index';

const enum ProductManagementAction {
  GetOverviewProducts = '[Product] GetOverviewProducts',
  GetOverviewProductsSuccess = '[Product] GetOverviewProductsSuccess',
  GetOverviewProductsFail = '[Product] GetOverviewProductsFail',
  GetProducts = '[Product] GetProduct',
  GetProductsSuccess = '[Product] GetProductSuccess',
  GetProductsFail = '[Product] GetProductFail',
  GetProductAttributes = '[Product] GetProductAttributes',
  GetProductAttributesFail = '[Product] GetProductAttributesFail',
  GetProductAttributesSuccess = '[Product] GetProductAttributesSuccess',
  GetProductAttributeValues = '[Product] GetProductAttributeValues',
  GetProductAttributeValuesFail = '[Product] GetProductAttributeValuesFail',
  GetProductAttributeValuesSuccess = '[Product] GetProductAttributeValuesSuccess',
  ClearAllData = '[Product] ClearAllData',
  UpdateProduct = '[Product] Update',
  UpdateProductSuccess = '[Product] Update Success',
  UpdateProductError = '[Product] Update Error',
  GetMoreProductList = '[Product] GetMoreProductList',
  GetMoreProductListSuccess = '[Product] GetMoreProductListSuccess',
  GetMoreProductListError = '[Product] GetMoreProductListError',
}

export class GetProducts {
  static type = ProductManagementAction.GetProducts;

  constructor(public payload: IHttpGetRequest) {}
}

export class GetProductsSuccess {
  static type = ProductManagementAction.GetProductsSuccess;

  constructor(public payload: IListProductResponse) {}
}

export class GetProductsFail {
  static type = ProductManagementAction.GetProductsFail;
}

export class GetProductAttributes {
  static type = ProductManagementAction.GetProductAttributes;

  constructor(public payload: IHttpGetRequest) {}
}

export class GetProductAttributesSuccess {
  static type = ProductManagementAction.GetProductAttributesSuccess;

  constructor(public payload: IProductAttributeResponse[]) {}
}

export class GetProductAttributesFail {
  static type = ProductManagementAction.GetProductAttributesFail;
}

export class GetProductAttributeValues {
  static type = ProductManagementAction.GetProductAttributeValues;

  constructor(
    public payload: IHttpGetRequest,
    public productFeedId: string,
    public productAttributeValueId: string
  ) {}
}

export class GetProductAttributeValuesFail {
  static type = ProductManagementAction.GetProductAttributeValuesFail;
}

export class GetProductAttributeValuesSuccess {
  static type = ProductManagementAction.GetProductAttributeValuesSuccess;

  constructor(public payload: IListProductAttributeValueResponse) {}
}

export class GetOverviewProducts {
  static type = ProductManagementAction.GetOverviewProducts;
}

export class GetOverviewProductsSuccess {
  static type = ProductManagementAction.GetOverviewProductsSuccess;

  constructor(public payload: IProductOverviewResponse) {}
}

export class GetOverviewProductsFail {
  static type = ProductManagementAction.GetOverviewProductsFail;
}

export class ClearAllProductsData {
  static type = ProductManagementAction.ClearAllData;
}

export class UpdateProduct {
  public static readonly type = ProductManagementAction.UpdateProduct;

  constructor(public payload: Partial<IProductUpdateRequest>) {}
}

export class UpdateProductSuccess {
  public static readonly type = ProductManagementAction.UpdateProductSuccess;

  constructor(public product: IProductResponse) {}
}

export class UpdateProductError {
  public static readonly type = ProductManagementAction.UpdateProductError;

  constructor(public error: any) {}
}

export class GetMoreProductList {
  public static readonly type = ProductManagementAction.GetMoreProductList;

  constructor(public params: IHttpGetRequest) {}
}

export class GetMoreProductListSuccess {
  public static readonly type =
    ProductManagementAction.GetMoreProductListSuccess;

  constructor(
    public response: IListProductResponse,
    public params: IHttpGetRequest
  ) {}
}

export class GetMoreProductListError {
  public static readonly type = ProductManagementAction.GetMoreProductListError;

  constructor(public error: any) {}
}