import { Component } from '@angular/core';

@Component({
  selector: 'novanet-product-management',
  templateUrl: './feature-product-management.component.html',
})
export class ProductManagementComponent {
  public offsetTop = 10;
}
