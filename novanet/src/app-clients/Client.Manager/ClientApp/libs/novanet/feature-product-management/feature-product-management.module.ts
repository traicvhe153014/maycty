import { NgModule } from '@angular/core';
import { ProductManagementComponent } from './feature-product-management.component';
import { FeatureProductManagementRouting } from './feature-product-management.routing';
import { ProductManagementTableComponent } from './components/product-management-table/_index';
import { InformationProductCardComponent } from './components/information-product-card/_index';
import { CommonModule } from '@angular/common';
import { DataTableModule, FormatterModule, LoadingIconModule } from '@core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NovanetInputModule } from '@shared/custom-input';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { ProductManagementStoreModule } from './store';
import { SvgIconModule } from '@core/components/svg-icon';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { ProductDetailComponent } from './components/product-detail/_index';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { SwiperModule } from 'swiper/angular';
import { ProductStatusIconModule } from '@features/product-management/components/product-status-icon';
import { ProductRunningCampaignsModule } from '@features/product-management/components/product-running-campaigns';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { SwitchNoAnimationModule } from '@shared/custom-input/components/switch-no-animation';

const COMPONENTS = [
  ProductManagementComponent,
  ProductManagementTableComponent,
  InformationProductCardComponent,
  ProductDetailComponent,
];

const MODULES = [
  CommonModule,
  FeatureProductManagementRouting,
  DataTableModule,
  RouterModule,
  ReactiveFormsModule,
  NovanetInputModule,
  NzTableModule,
  SvgIconModule,
  NzSwitchModule,
  FormsModule,
  ProductManagementStoreModule,
  NzAffixModule,
  LoadingIconModule,
  FormatterModule,
  NzDatePickerModule,
  NzModalModule,
  SwiperModule,
  ProductStatusIconModule,
  ProductRunningCampaignsModule,
  NzImageModule,
  NzSelectModule,
  NzDropDownModule,
  SwitchNoAnimationModule,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
})
export class ProductManagementModule {}
