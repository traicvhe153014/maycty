import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ICardData } from '@features/product-management/models/_index';

@Component({
  selector: 'novanet-information-product-card',
  templateUrl: 'information-product-card.component.html',
})
export class InformationProductCardComponent implements OnInit {
  @Input() public cardInformation: ICardData;
  @ViewChild('card', { static: true }) private card: ElementRef;
  @ViewChild('cardValue', { static: true }) private cardValue: ElementRef;

  constructor(private renderer: Renderer2) {}

  ngOnInit() {
    this.renderer.setStyle(
      this.card.nativeElement,
      'backgroundColor',
      this.cardInformation.bgColor
    );

    this.renderer.setStyle(
      this.cardValue.nativeElement,
      'color',
      this.cardInformation.textColor
    );
  }
}
