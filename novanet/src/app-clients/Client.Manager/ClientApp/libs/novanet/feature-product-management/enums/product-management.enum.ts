export enum ProductManagementEnum {
  INDEX,
  TOGGLE,
  IMAGE,
  TITLE,
  AD_RUNNING,
  PRODUCT_STATE,
  PRODUCT_STATUS,
  COST,
  VIEWS,
  CLICKS,
  CTR,
  CPC,
  PURCHASE_QUANTITY,
  CONVERSION,
  CPS,
  PRODUCT_TYPE,
  MODIFIED,
  PRODUCT_FEED,
  CHECKBOX,
}

export enum ProductManagementSummaryEnum {
  COUNT,
  AD_RUNNING,
  PRODUCT_STATE,
  PRODUCT_STATUS,
  COST,
  VIEWS,
  CLICKS,
  CTR,
  CPC,
  PURCHASE_QUANTITY,
  CONVERSION,
  CPS,
  PRODUCT_TYPE,
  MODIFIED,
  PRODUCT_FEED,
}

export enum EProductOverview {
  total = 1,
  running,
  deactivated,
  error,
  outOfStock,
}

export enum EProductEntityStatus {
  Running,
  OutOfStock,
  Deactivated,
}

export enum productSearchLevelEnum {
  title,
  type,
  source,
  id,
}
