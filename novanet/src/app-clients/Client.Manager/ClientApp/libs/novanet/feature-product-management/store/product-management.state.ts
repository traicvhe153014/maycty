import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import {
  IProductAttribute,
  IProductAttributeValue,
  IProductManagementState,
  IProductOverviewResponse,
} from './product-management-state.model';
import { ProductManagementService } from './services/product-management.service';
import {
  ClearAllProductsData,
  GetMoreProductList,
  GetMoreProductListError,
  GetMoreProductListSuccess,
  GetOverviewProducts,
  GetOverviewProductsFail,
  GetOverviewProductsSuccess,
  GetProductAttributes,
  GetProductAttributesFail,
  GetProductAttributesSuccess,
  GetProductAttributeValues,
  GetProductAttributeValuesFail,
  GetProductAttributeValuesSuccess,
  GetProducts,
  GetProductsFail,
  GetProductsSuccess,
  UpdateProduct,
  UpdateProductError,
  UpdateProductSuccess,
} from './product-management.action';
import { HideLoading } from '@shared/global-loading';
import {
  IListProductAttributeValueResponse,
  IProductAttributeResponse,
  IProductResponse,
  IProductSummary,
} from '@features/product-management/_index';
import { ProductAttributeService } from './services/product-attribute.service';
import { ProductAttributeValueService } from './services/product-attribute-value.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { cloneDeep } from 'lodash';

@Injectable()
@State<IProductManagementState>({
  name: 'ProductManagementState',
})
export class ProductManagementState implements NgxsOnInit {
  private label = 'Get Products';

  constructor(
    private productManagementService: ProductManagementService,
    private productAttributeValueService: ProductAttributeValueService,
    private productAttributeService: ProductAttributeService
  ) {}

  @Selector()
  public static getList(state: IProductManagementState): IProductResponse[] {
    return state.products;
  }

  @Selector()
  public static getOverview(
    state: IProductManagementState
  ): IProductOverviewResponse {
    return state.overview;
  }

  @Selector()
  public static getLoadingStatus(state: IProductManagementState): boolean {
    return state.loading;
  }

  @Selector()
  public static getProductAttributeList(
    state: IProductManagementState
  ): IProductAttributeResponse[] {
    return state.productAttributesState.productAttributes;
  }

  @Selector()
  public static getLoadingStatusProductAttributeList(
    state: IProductManagementState
  ): boolean {
    return state.productAttributesState.loading;
  }

  @Selector()
  public static getProductAttributeValueList(
    state: IProductManagementState
  ): IListProductAttributeValueResponse {
    return {
      data: state.productAttributeValuesState.productAttributeValues,
      summary: state.productAttributeValuesState.summary,
    } as IListProductAttributeValueResponse;
  }

  @Selector()
  public static getLoadingStatusProductAttributeValueList(
    state: IProductManagementState
  ): boolean {
    return state.productAttributeValuesState.loading;
  }

  @Selector()
  public static getProductSummary(
    state: IProductManagementState
  ): IProductSummary {
    return state.productSummary;
  }

  ngxsOnInit(ctx?: StateContext<IProductManagementState>): void {}

  @Action(GetProducts)
  public getProducts(
    ctx: StateContext<IProductManagementState>,
    { payload }: GetProducts
  ) {
    ctx.patchState({
      loading: true,
    });
    this.productManagementService.getProducts(payload).subscribe(
      (response) => {
        ctx.dispatch(new GetProductsSuccess(response));
      },
      (error) => {
        ctx.dispatch(new GetProductsFail());
      }
    );
  }

  @Action(GetProductsSuccess)
  public getProductsSuccess(
    ctx: StateContext<IProductManagementState>,
    { payload }: GetProductsSuccess
  ) {
    const products = payload.data.map((item) => ({
      ...item,
      checked: false,
    }));
    ctx.patchState({
      products,
      loading: false,
      productSummary: payload.summary,
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(GetProductsFail)
  public getProductsFail(ctx: StateContext<IProductManagementState>) {
    ctx.patchState({
      loading: false,
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(GetProductAttributes)
  public getProductAttributes(
    ctx: StateContext<IProductManagementState>,
    { payload }: GetProductAttributes
  ) {
    const productAttributesState = {
      loading: true,
    } as IProductAttribute;
    ctx.patchState({
      productAttributesState,
    });
    this.productAttributeService.getProductAttributes(payload).subscribe(
      (response) => {
        ctx.dispatch(new GetProductAttributesSuccess(response));
      },
      (error) => {
        ctx.dispatch(new GetProductAttributesFail());
      }
    );
  }

  @Action(GetProductAttributesSuccess)
  public getProductAttributesSuccess(
    ctx: StateContext<IProductManagementState>,
    { payload }: GetProductAttributesSuccess
  ) {
    const productAttributes = payload.map((item) => ({
      ...item,
      checked: false,
    }));
    ctx.patchState({
      productAttributesState: {
        productAttributes,
        loading: false,
      },
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(GetProductAttributesFail)
  public getProductAttributesFail(ctx: StateContext<IProductManagementState>) {
    const productAttributesState = {
      loading: false,
    } as IProductAttribute;
    ctx.patchState({
      productAttributesState,
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(GetProductAttributeValues)
  public getProductAttributeValues(
    ctx: StateContext<IProductManagementState>,
    {
      payload,
      productFeedId,
      productAttributeValueId,
    }: GetProductAttributeValues
  ) {
    const productAttributeValuesState = {
      loading: true,
    } as IProductAttributeValue;
    ctx.patchState({
      productAttributeValuesState,
    });
    this.productAttributeValueService
      .getProductAttributeValues(
        payload,
        productFeedId,
        productAttributeValueId
      )
      .subscribe(
        (response) => {
          ctx.dispatch(new GetProductAttributeValuesSuccess(response));
        },
        (error) => {
          ctx.dispatch(new GetProductAttributeValuesFail());
        }
      );
  }

  @Action(GetProductAttributeValuesSuccess)
  public getProductAttributeValuesSuccess(
    ctx: StateContext<IProductManagementState>,
    { payload }: GetProductAttributeValuesSuccess
  ) {
    const productAttributeValues = payload.data.map((item) => ({
      ...item,
      checked: false,
    }));
    ctx.patchState({
      productAttributeValuesState: {
        productAttributeValues,
        summary: payload.summary,
        loading: false,
      },
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(GetProductAttributeValuesFail)
  public getProductAttributeValuesFail(
    ctx: StateContext<IProductManagementState>
  ) {
    const productAttributeValuesState = {
      loading: false,
    } as IProductAttributeValue;
    ctx.patchState({
      productAttributeValuesState,
    });
    ctx.dispatch(new HideLoading(this.label));
  }

  @Action(ClearAllProductsData)
  public clearAllData(ctx: StateContext<IProductManagementState>) {
    const productManagementState = {
      productAttributesState: {},
      productAttributeValuesState: {},
      products: null,
      loading: false,
      hasMorePages: false,
    } as IProductManagementState;
    ctx.patchState(productManagementState);
  }

  @Action(GetOverviewProducts)
  public getOverviewProducts(ctx: StateContext<IProductManagementState>) {
    ctx.patchState({
      loading: true,
    });
    this.productManagementService.getOverview().subscribe(
      (response) => {
        ctx.dispatch(new GetOverviewProductsSuccess(response));
      },
      (error) => {
        ctx.dispatch(new GetOverviewProductsFail());
      }
    );
  }

  @Action(GetOverviewProductsSuccess)
  public getOverviewProductsSuccess(
    ctx: StateContext<IProductManagementState>,
    { payload }: GetOverviewProductsSuccess
  ) {
    ctx.patchState({
      overview: payload,
      loading: false,
    });
  }

  @Action(GetOverviewProductsFail)
  public getOverviewProductsFail(ctx: StateContext<IProductManagementState>) {
    ctx.patchState({
      overview: null,
    });
  }

  @Action(UpdateProduct)
  public update(
    ctx: StateContext<IProductManagementState>,
    { payload }: UpdateProduct
  ) {
    return this.productManagementService.update(payload).pipe(
      map((response) => ctx.dispatch(new UpdateProductSuccess(response))),
      catchError((error) => ctx.dispatch(new UpdateProductError(error)))
    );
  }

  @Action(UpdateProductSuccess)
  public updateSuccess(
    ctx: StateContext<IProductManagementState>,
    { product }: UpdateProductSuccess
  ) {
    const productUpdate = cloneDeep(ctx.getState().products).map((x) => {
      if (x.productId === product.productId) {
        x.isActive = product.isActive;
        x.status = product.status;
      }
      return x;
    });
    ctx.patchState({ updatedProduct: product, products: productUpdate });
  }

  @Action(UpdateProductError)
  public updateError(
    ctx: StateContext<IProductManagementState>,
    { error }: UpdateProductError
  ) {
    return throwError(error);
  }

  @Action(GetMoreProductList)
  public GetMoreProductList(
    ctx: StateContext<IProductManagementState>,
    { params }: GetMoreProductList
  ) {
    if (ctx.getState().loading) {
      return new Observable();
    }
    if (ctx.getState().hasMorePages) {
      return new Observable();
    }
    ctx.patchState({ loading: true });
    return this.productManagementService
      .getProducts({
        ...params,
      })
      .pipe(
        map((response) =>
          ctx.dispatch(new GetMoreProductListSuccess(response, params))
        ),
        catchError((err) => ctx.dispatch(new GetMoreProductListError(err)))
      );
  }

  @Action(GetMoreProductListSuccess)
  public GetMoreProductListSuccess(
    ctx: StateContext<IProductManagementState>,
    { response, params }: GetMoreProductListSuccess
  ) {
    const newPage = response.data.length === 0 ? params.page - 1 : params.page;
    const hasMorePages = response.data.length !== params.pageSize;
    const currentAdsets = ctx.getState().products;
    ctx.patchState({
      products: [...currentAdsets, ...response.data],
      page: newPage,
      hasMorePages,
      loading: false,
    });
  }

  @Action(GetMoreProductListError)
  public GetMoreProductListError(
    ctx: StateContext<IProductManagementState>,
    { error }: GetMoreProductListError
  ) {
    ctx.patchState({ loading: false });
    return throwError(error);
  }
}
