import { EProductOverview } from '@features/product-management/enums';

export interface ICardData {
  id: EProductOverview;
  title: string;
  percent: number;
  records: number;
  bgColor: string;
  textColor: string;
}
