import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductManagementComponent } from './feature-product-management.component';

const routes: Routes = [
  {
    path: '',
    component: ProductManagementComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
})
export class FeatureProductManagementRouting {}
