import { Component, Input } from '@angular/core';
import { EProductEntityStatus } from '@features/product-management/enums';

@Component({
  selector: 'novanet-product-status-icon',
  template: `
    <span
      *ngIf="status === EProductEntityStatus.Running"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(33,150,83,1)] bg-[rgba(33,150,83,0.1)]">
      Thực chạy
    </span>
    <span
      *ngIf="status === EProductEntityStatus.OutOfStock"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(242,153,74,1)] bg-[rgba(242,153,74,0.1)]">
      Hết hàng
    </span>
    <span
      *ngIf="status === EProductEntityStatus.Deactivated"
      class="py-1 px-2 rounded-md text-xs font-medium whitespace-nowrap text-[rgba(79,79,79,1)] bg-[rgba(79,79,79,0.1)]">
      Đẵ tắt
    </span>
  `,
})
export class ProductStatusIconComponent {
  @Input() status: EProductEntityStatus;
  public readonly EProductEntityStatus = EProductEntityStatus;
}
