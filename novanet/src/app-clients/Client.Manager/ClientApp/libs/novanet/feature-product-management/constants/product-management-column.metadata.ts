import { ISettingColumnTable } from '@data-table/models';
import { ProductManagementEnum } from '@features/product-management/enums';
import { metricSortFields } from '@core';

export const ProductManagementColumn: ISettingColumnTable<any, any>[] = [
  {
    id: ProductManagementEnum.CHECKBOX,
    title: 'Checkbox',
    align: 'center',
    pinLeft: true,
    width: '50px',
  },
  {
    id: ProductManagementEnum.INDEX,
    title: 'Stt',
    width: '50px',
    align: 'center',
    pinLeft: true,
  },
  {
    id: ProductManagementEnum.TOGGLE,
    title: 'Bật/tắt',
    width: '100px',
    align: 'center',
    pinLeft: true,
  },
  {
    id: ProductManagementEnum.IMAGE,
    title: 'Hình ảnh sản phẩm',
    width: '150px',
    align: 'center',
    pinLeft: true,
  },
  {
    id: ProductManagementEnum.TITLE,
    title: 'Tiêu đề',
    width: '300px',
    pinLeft: true,
    name: 'Title',
    sort: true,
  },
  {
    id: ProductManagementEnum.AD_RUNNING,
    title: 'Chiến dịch QC',
  },
  {
    id: ProductManagementEnum.PRODUCT_STATUS,
    title: 'Trạng thái',
    sort: true,
    name: 'ProductEntity.Status',
    descending: false,
    ascending: true,
  },
  {
    id: ProductManagementEnum.PRODUCT_STATE,
    title: 'Tình trạng',
    sort: true,
    name: 'Condition',
  },
  {
    id: ProductManagementEnum.COST,
    title: 'Chi phí',
    sort: true,
    name: metricSortFields.totalCost,
  },
  {
    id: ProductManagementEnum.VIEWS,
    title: 'Lượt xem',
    sort: true,
    name: metricSortFields.totalImpressions,
  },
  {
    id: ProductManagementEnum.CLICKS,
    title: 'Lượt nhấn',
    sort: true,
    name: metricSortFields.totalClicks,
  },
  {
    id: ProductManagementEnum.CTR,
    title: 'CTR (%)',
    sort: true,
    name: metricSortFields.totalCtr,
  },
  {
    id: ProductManagementEnum.CPC,
    title: 'CPC (VNĐ)',
    sort: true,
    name: metricSortFields.totalCpc,
  },
  {
    id: ProductManagementEnum.PURCHASE_QUANTITY,
    title: 'SL mua hàng',
    sort: true,
    name: metricSortFields.totalPurchase,
  },
  {
    id: ProductManagementEnum.CONVERSION,
    title: '% chuyển đổi mua hàng',
    sort: true,
    name: metricSortFields.totalPurchaseConversion,
  },
  {
    id: ProductManagementEnum.CPS,
    title: 'CPS (VNĐ)',
    sort: true,
    name: metricSortFields.totalCps,
  },
  {
    id: ProductManagementEnum.PRODUCT_TYPE,
    title: 'Loại sản phẩm',
    sort: true,
    name: 'ProductType',
  },
  {
    id: ProductManagementEnum.MODIFIED,
    name: 'ProductEntity.ModifiedAt',
    sort: true,
    title: 'Cập nhật',
    descending: true,
    ascending: false,
  },
  {
    id: ProductManagementEnum.PRODUCT_FEED,
    title: 'Nguồn dữ liệu',
    name: 'ProductEntity.ProductFeed.Name',
    sort: true,
  },
];
