import { PredicateOperatorEnum } from '@core/enums';
import { productSearchLevelEnum } from '../enums';

export interface IProductSearchOption {
  value: string;
  operator: PredicateOperatorEnum;
  level: productSearchLevelEnum;
}
