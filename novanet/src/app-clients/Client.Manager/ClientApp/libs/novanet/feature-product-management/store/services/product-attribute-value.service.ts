import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { IHttpGetRequest } from '@core/models';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { IListProductAttributeValueResponse } from '@features/product-management/models/_index';

const ProductAttributeValueUrls = {
  list: `list`,
};

@Injectable()
export class ProductAttributeValueService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1/ProductAttributeValue');
  }

  public getProductAttributeValues(
    data: IHttpGetRequest,
    productFeedId: string,
    productAttributeId: string
  ) {
    const paramData = data;
    paramData['productFeedId'] = productFeedId;
    paramData['productAttributeId'] = productAttributeId;
    const params = this.createParams(paramData);
    return this.httpClient
      .get<ISuccessHttpResponse<IListProductAttributeValueResponse>>(
        this.createUrl(ProductAttributeValueUrls.list),
        {
          params,
        }
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }
}
