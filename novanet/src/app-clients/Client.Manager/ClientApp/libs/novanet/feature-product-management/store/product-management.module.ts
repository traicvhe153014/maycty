import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import {
  ProductAttributeService,
  ProductAttributeValueService,
  ProductManagementService,
} from './services';
import { ProductManagementState } from './product-management.state';

@NgModule({
  imports: [NgxsModule.forFeature([ProductManagementState])],
  providers: [
    ProductManagementService,
    ProductAttributeService,
    ProductAttributeValueService,
  ],
})
export class ProductManagementStoreModule {}
