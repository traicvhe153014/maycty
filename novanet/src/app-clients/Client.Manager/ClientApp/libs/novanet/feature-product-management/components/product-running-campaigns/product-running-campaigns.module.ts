import { NgModule } from '@angular/core';
import { ProductRunningCampaignsComponent } from './product-running-campaigns.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ProductRunningCampaignsComponent],
  exports: [ProductRunningCampaignsComponent],
  imports: [CommonModule],
})
export class ProductRunningCampaignsModule {}
