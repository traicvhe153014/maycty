import { EProductEntityStatus } from '@features/product-management/enums';

export interface IListProductResponse {
  data: IProductResponse[];
  summary?: IProductSummary;
}

export interface IProductResponse {
  productId: string;
  productFeedId: string;
  productFeedName: string;
  isLoading?: boolean;
  data: IProductData;
  checked?: boolean;
  status?: EProductEntityStatus;
  isActive?: boolean;
  runningCampaigns?: string[];
}

export interface IProductData {
  [label: string]: string;
}

export interface IProductSummary {
  [label: string]: number;
}

export interface IListProductAttributeValueResponse {
  data: IProductAttributeValueResponse[];
  summary?: IProductSummary;
}

export interface IProductAttributeValueResponse {
  productAttributeHashcodeValue: string;
  productAttributeValue: string;
  amount: number;
  checked?: boolean;
}

export interface IProductAttributeResponse {
  id: string;
  name: string;
  checked?: boolean;
}
