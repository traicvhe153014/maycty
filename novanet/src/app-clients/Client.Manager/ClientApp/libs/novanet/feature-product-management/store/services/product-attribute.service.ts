import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { IHttpGetRequest } from '@core/models';
import { ISuccessHttpResponse } from '@models';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { IProductAttributeResponse } from '@features/product-management/models/_index';

const ProductAttributeUrls = {
  list: `list`,
};

@Injectable()
export class ProductAttributeService extends BaseService {
  constructor(
    httpClient: HttpClient,
    private readonly router: Router,
    @Inject(baseUrl) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'settings/api/v1/ProductAttribute');
  }

  public getProductAttributes(data: IHttpGetRequest) {
    const params = this.createParams(data);
    return this.httpClient
      .get<ISuccessHttpResponse<IProductAttributeResponse[]>>(
        this.createUrl(ProductAttributeUrls.list),
        {
          params,
        }
      )
      .pipe(
        map(
          (response) => response.data,
          (error) => {}
        )
      );
  }
}
