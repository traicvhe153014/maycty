import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '@core/auth';
import { CommonService } from '@shared/common/store';

@Injectable()
export class HasProductFeedGuard implements CanActivate {
  constructor(
    private readonly store: Store,
    private readonly authService: AuthService,
    private readonly commonService: CommonService
  ) {}

  public canActivate(): Observable<boolean> {
    return this.commonService.hasAnyProductFeed().pipe(
      map((response) => {
        if (response) {
          return true;
        }
        this.authService.redirectToMainPage();
        return false;
      })
    );
  }
}
