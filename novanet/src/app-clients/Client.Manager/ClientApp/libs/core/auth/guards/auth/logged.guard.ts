import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AuthService, AuthState } from '@core';

@Injectable()
export class LoggedGuard implements CanActivate {
  constructor(
    private readonly store: Store,
    private readonly authService: AuthService
  ) {}

  public canActivate(): Observable<boolean> {
    return this.store.select(AuthState.getUser).pipe(
      take(1),
      map(() => {
        if (this.store.selectSnapshot(AuthState.getUser)) {
          this.authService.redirectToMainPage();
          return false;
        }

        return true;
      })
    );
  }
}
