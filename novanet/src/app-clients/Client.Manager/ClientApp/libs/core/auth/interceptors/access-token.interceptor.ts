import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { BehaviorSubject, EMPTY, Observable, throwError } from 'rxjs';
import { catchError, filter, finalize, switchMap, take } from 'rxjs/operators';

import { Policies, ResponseStatusCodesEnum } from '@core/constants';
import { AuthService, ILoginOptions, Logout } from '@features/auth/store';
import { User } from '@models';
import { Store } from '@ngxs/store';
import {
  GlobalNotificationEnum,
  ShowGlobalNotification,
} from '@shared/global-notification';

export class AccessTokenInterceptor implements HttpInterceptor {
  private noAuthUrlsRegexps = [];
  private refreshTokenInProgress = false;
  private refreshTokenError = false;
  private refreshTokenSuccessSubject: BehaviorSubject<any> =
    new BehaviorSubject<any>(false);

  constructor(
    private readonly authService: AuthService,
    private readonly store: Store
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const user = this.authService.getUserData();
    let continues = true;

    if (user) {
      if (!this.authorized(user)) {
        continues = false;
      }
    }

    const shouldHaveAuthorizationHeader = this.shouldHaveAuthorizationHeader(
      request.url
    );

    if (shouldHaveAuthorizationHeader) {
      request = this.addAuthenticationToken(request);
    }

    if (!continues) {
      return EMPTY;
    }

    return next.handle(request).pipe(
      catchError((error: any) => {
        if (error?.status === ResponseStatusCodesEnum.FORBIDDEN) {
          this.store.dispatch(new Logout());
          this.store.dispatch(
            new ShowGlobalNotification(
              GlobalNotificationEnum.error,
              'Tài khoản của bạn đã bị khóa, vui lòng đăng nhập lại'
            )
          );
          return throwError(error);
        }
        if (
          !(error instanceof HttpErrorResponse) ||
          error.status !== ResponseStatusCodesEnum.UNAUTHORIZED ||
          !this.shouldHaveAuthorizationHeader(request.url)
        ) {
          return throwError(error);
        }

        if (this.refreshTokenInProgress) {
          return this.refreshTokenSuccessSubject.pipe(
            filter(Boolean),
            take(1),
            switchMap(() => {
              if (this.refreshTokenError) {
                return EMPTY;
              }

              request = this.addAuthenticationToken(request);

              return next.handle(request);
            })
          );
        }

        const refreshTokensOptions: ILoginOptions = {
          redirectUrl: window.location.origin,
        };

        this.refreshTokenInProgress = true;
        this.refreshTokenError = false;
        this.refreshTokenSuccessSubject.next(false);

        return this.authService.refreshTokens(refreshTokensOptions).pipe(
          switchMap(() => {
            request = this.addAuthenticationToken(request);

            return next.handle(request);
          }),
          catchError((err) => {
            if (err instanceof HttpErrorResponse) {
              this.refreshTokenError = true;
            }

            return throwError(err);
          }),
          finalize(() => {
            this.refreshTokenInProgress = false;
            this.refreshTokenSuccessSubject.next(true);
          })
        );
      })
    );
  }

  private shouldHaveAuthorizationHeader(url: string): boolean {
    return !this.noAuthUrlsRegexps.some((noAuthUrl) => noAuthUrl.test(url));
  }

  private addAuthenticationToken(request: HttpRequest<any>): HttpRequest<any> {
    const accessToken = this.authService.getAccessToken();

    if (!accessToken) {
      request.headers.delete('Authorization');
      return request;
    }

    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
  }

  private authorized(user: User): boolean {
    return Policies.some((x) => user.policies.includes(Policies.indexOf(x)));
  }
}
