import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { catchError, filter, switchMap, take } from 'rxjs/operators';
import { AuthService, AuthState } from '@core/auth';

@Injectable()
export class AuthRefreshGuard implements CanActivate {
  constructor(
    private readonly store: Store,
    private readonly authService: AuthService
  ) {}

  public canActivate(): Observable<boolean> {
    return this.store.select(AuthState.getAuthorizationChecked).pipe(
      filter(Boolean),
      take(1),
      switchMap(() => {
        if (this.authService.isAccessTokenExpired()) {
          return this.authService.refreshTokens({ silently: true }).pipe(
            switchMap(() => of(true)),
            catchError(() => of(true))
          );
        }

        return of(true);
      })
    );
  }
}
