import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
  AccessTokenInterceptor,
  AuthGuard,
  AuthRefreshGuard,
  AuthService,
  AuthStateModule,
  HasProductFeedGuard,
  LoggedGuard,
} from './auth';
import { NgZorroModule } from './ng-zorro';
import { ProductFeedStateModule } from '@features/product-feed/index';
import { CampaignStateModule } from '@features/campaign-management/store/campaign';
import { CommonService, CommonStateModule } from '@shared/common/store';
import { AdsetStateModule } from '@features/campaign-management/store/adset';
import { SharingStateModule } from '@shared/sharing/store';
import { AdvertisingStateModule } from '@features/campaign-management/store';
import { ReportStateModule } from '@features/feature-report/store';
import { SettingsStateModule } from '@shared/settings/store';
import { StorageService } from './services/storage.service';
import { IdentityStateModule } from '@shared/identity';
import { PublisherStateModule } from '@features/feature-publisher-management/store';
import { WebsitePriceStateModule } from '@features/feature-website-price-management/store';
import { WebsiteStateModule } from '@features/feature-website-management/store/website-state.module';
import { Store } from '@ngxs/store';

const GUARDS = [AuthGuard, LoggedGuard, AuthRefreshGuard, HasProductFeedGuard];

const SERVICES = [StorageService];

const INTERCEPTORS = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AccessTokenInterceptor,
    multi: true,
    deps: [AuthService, Store],
  },
  CommonService,
];

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    RouterModule,
    AuthStateModule,
    NgZorroModule,
    ProductFeedStateModule,
    CampaignStateModule,
    CommonStateModule,
    AdsetStateModule,
    SharingStateModule,
    AdvertisingStateModule,
    ReportStateModule,
    SettingsStateModule,
    IdentityStateModule,
    PublisherStateModule,
    WebsitePriceStateModule,
    WebsiteStateModule,
  ],
  providers: [...INTERCEPTORS, ...GUARDS, ...SERVICES],
  exports: [],
})
export class CoreModule {}
