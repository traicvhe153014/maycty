import { Inject, Injectable } from '@angular/core';
import { BaseService, baseUrl } from '@shared/services';
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ISuccessHttpResponse } from '@models';
import {
  AddUploadProcessingVideo,
  EUploadNotificationStep,
  IGlobalUploadNotificationModel,
  ShowGlobalUploadNotification,
  uploadingVideosKey,
} from '@shared/global-notification';
import { v4 as uuidv4 } from 'uuid';
import { Store } from '@ngxs/store';
import { environment } from '@environment';

const StorageUrls = {
  uploadFile: 'Upload',
  uploadZip: 'UploadZip',
  processVideo: 'media/process-video',
};

@Injectable()
export class StorageService extends BaseService {
  constructor(
    httpClient: HttpClient,
    @Inject(baseUrl) protected hostUrl: string,
    private store: Store
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'storage/api/v1');
  }

  public upload(payload: File | Blob): Observable<string> {
    const formData = new FormData();
    formData.append('file', payload);
    return this.httpClient
      .post<ISuccessHttpResponse>(
        this.createUrl(StorageUrls.uploadFile),
        formData
      )
      .pipe(map((response) => response.data));
  }

  public uploadZip(payload: File | Blob): Observable<string> {
    const formData = new FormData();
    formData.append('file', payload);
    return this.httpClient
      .post<ISuccessHttpResponse>(
        this.createUrl(StorageUrls.uploadZip),
        formData
      )
      .pipe(map((response) => response.data));
  }

  public processVideo(
    payload: File,
    advertisingId?: string
  ): Observable<HttpEvent<any>> {
    const formData = new FormData();
    formData.append('file', payload);
    if (advertisingId) {
      formData.append('advertisingId', advertisingId);
    }
    const processId = uuidv4();
    return this.httpClient
      .post(
        `${environment.streamingRootUrl}/streaming/api/v1/${StorageUrls.processVideo}`,
        formData,
        {
          reportProgress: true,
          observe: 'events',
          responseType: 'text',
        }
      )
      .pipe(
        tap((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              this.store.dispatch(
                new ShowGlobalUploadNotification({
                  processId,
                  fileName: payload.name,
                  percentage: Math.round((event.loaded / event.total) * 100),
                  step: EUploadNotificationStep.Upload,
                })
              );
              break;
            case HttpEventType.Response:
              const processPayload: IGlobalUploadNotificationModel = {
                processId,
                fileName: payload.name,
                percentage: 0,
                step: EUploadNotificationStep.Process,
                videoId: event.body,
              };
              this.store
                .dispatch(new AddUploadProcessingVideo(processPayload))
                .subscribe({
                  next: () => {
                    this.store.dispatch(
                      new ShowGlobalUploadNotification(processPayload)
                    );
                  },
                });
              const uploadingVideosStr =
                localStorage.getItem(uploadingVideosKey);
              let uploadingVideos: IGlobalUploadNotificationModel[] = [];
              if (uploadingVideosStr) {
                uploadingVideos = JSON.parse(
                  uploadingVideosStr
                ) as IGlobalUploadNotificationModel[];
              }
              uploadingVideos.push(processPayload);
              localStorage.setItem(
                uploadingVideosKey,
                JSON.stringify(uploadingVideos)
              );
              break;
          }
        })
      );
  }

  public avatar(dimension: number, text: string) {
    return this.createUrl('avatar' + '/' + dimension + '/' + text);
  }
}
