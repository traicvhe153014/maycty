import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TypeExpandableList } from '@core/enums';

@Component({
  selector: 'novanet-expandable-list',
  templateUrl: './expandable-list.component.html',
  styleUrls: ['./expandable-list.component.scss'],
})
export class ExpandableListComponent {
  @Input() data: string[] = [];
  @Input() collapsedLength = 2;
  @Input() typeExpandableList: TypeExpandableList;
  @Input() typeTileModal: string;

  @Output() isExpandedChange = new EventEmitter<boolean>();

  _isExpanded = false;
  public typeExpandable = TypeExpandableList;
  isShowModal = false;

  get isExpanded(): boolean {
    return this._isExpanded;
  }

  @Input() set isExpanded(value: boolean) {
    this._isExpanded = value;
  }

  expand() {
    this._isExpanded = true;
    this.isExpandedChange.emit(true);
  }

  collapse() {
    this._isExpanded = false;
    this.isExpandedChange.emit(false);
  }

  cancelModal(){
    this.isShowModal =false;
  }
  showPopup(){
    this.isShowModal =true;
  }
}
