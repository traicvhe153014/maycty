import { cloneDeep } from 'lodash';

export const storage = {
  set(key, value) {
    try {
      const cloneValue = cloneDeep(value);
      localStorage.setItem(key, JSON.stringify(cloneValue));
    } catch (e) {}
  },

  get(key: string) {
    try {
      return localStorage.getItem(key)
        ? JSON.parse(localStorage.getItem(key) || '')
        : null;
    } catch (e) {}
  },

  remove(key) {
    localStorage.removeItem(key);
  },

  clear() {
    localStorage.clear();
  },
};
