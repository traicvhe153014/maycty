import { NgModule } from '@angular/core';
import { ExpandableListComponent } from './expandable-list.component';
import { CommonModule } from '@angular/common';
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule({
  declarations: [ExpandableListComponent],
  exports: [ExpandableListComponent],
  imports: [CommonModule, NzModalModule],
})
export class ExpandableListModule {}
