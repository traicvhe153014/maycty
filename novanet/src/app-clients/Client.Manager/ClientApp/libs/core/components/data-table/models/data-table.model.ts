import {
  NzTableLayout,
  NzTablePaginationPosition,
  NzTablePaginationType,
  NzTableSize,
  NzTableSortFn,
} from 'ng-zorro-antd/table';
import { TemplateRef } from '@angular/core';
import { ESortType } from '@core/enums';

export interface DataTableSettingModel {
  id?: string;
  bordered: boolean;
  loading: boolean;
  pagination: boolean;
  sizeChanger: boolean;
  title: boolean;
  header: boolean;
  footer: boolean;
  expandable: boolean;
  checkbox: boolean;
  fixHeader: boolean;
  noResult: boolean;
  ellipsis: boolean;
  simple: boolean;
  size: NzTableSize;
  tableLayout: NzTableLayout;
  position: NzTablePaginationPosition;
  paginationType: NzTablePaginationType;
  scrollX?: string;
  scrollY?: string;
  tableScroll?: string;
  summaryRow?: boolean;
  sortType: 'single' | 'multiple';
}

export interface ISettingColumnTable<T, G> {
  id: number;
  title: string | TemplateRef<G>;
  name?: string;
  width?: string;
  align?: 'left' | 'right' | 'center' | null;
  pinLeft?: boolean;
  pinRight?: boolean;
  compare?: NzTableSortFn<T>;
  sort?: boolean;
  ascending?: boolean;
  descending?: boolean;
}

export interface ISummaryColumnTable<T> {
  id: number;
  align?: 'left' | 'right' | 'center' | null;
  pinLeft?: boolean;
  pinRight?: boolean;
  compare?: NzTableSortFn<T>;
  colspan?: number;
}

export interface IFieldDetail {
  name: string;
  direction: ESortType;
}
