import { Component, Input } from '@angular/core';

@Component({
  selector: 'novanet-user-avatar',
  templateUrl: './user-avatar.component.html',
})
export class UserAvatarComponent {
  @Input() size = 16;
  @Input() src: string;

  public get sizePixel() {
    return this.size + 'px';
  }
}
