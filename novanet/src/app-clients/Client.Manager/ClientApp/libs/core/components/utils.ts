import { ETemplateType } from '@features/campaign-management/enums/template-type.enum';

export function calculateHeightDataTable(value: number): string {
  return window.innerHeight - value + 'px';
}

export function shouldLoadMore(target: HTMLElement): boolean {
  return (
    target.offsetHeight + target.scrollTop >= (target.scrollHeight * 5) / 6
  );
}

export function deepSearch(
  input: {
    [key: string]: any;
  }[],
  nestedFieldKey: string,
  predicate: (value: { [key: string]: any }) => boolean
): {
  [key: string]: any;
} | null {
  return input.reduce((result, item) => {
    if (result) {
      return result;
    }
    if (predicate(item)) {
      return item;
    }
    if (Array.isArray(item[nestedFieldKey])) {
      return deepSearch(item[nestedFieldKey], nestedFieldKey, predicate);
    }
    return null;
  }, null);
}

export class SumArray extends Array<any> {
  sum(key: string) {
    return this.reduce((a, b) => a + (b[key] || 0), 0);
  }
}

export function scrollIntoViewBySelector(
  selector: string,
  block?: ScrollLogicalPosition
): void {
  const element = document.querySelector(selector);

  if (!element) {
    return;
  }

  scrollIntoView(element, block);
}

function scrollIntoView(element: Element, block?: ScrollLogicalPosition): void {
  element.scrollIntoView({
    behavior: 'smooth',
    block: block ? block : 'center',
  });

  setTimeout(() => {
    const targetElements = Array.from(
      document.querySelectorAll('.error-message')
    );

    if (targetElements.length) {
      const elementTop = element.getBoundingClientRect().top;
      const lastElementBottom = targetElements
        .pop()
        .getBoundingClientRect().bottom;
      const isElementTopVisible = elementTop >= lastElementBottom;

      if (isElementTopVisible) {
        return;
      }

      const scrolledY = window.scrollY;

      if (scrolledY) {
        element.scroll(0, scrolledY - lastElementBottom + 300);
      }
    }
  });
}

export function isFileSizeValid(files: File[], maxSize: number): boolean {
  return files.every((file: File) => file.size <= maxSize);
}

export function getTemplateName(value: ETemplateType): string {
  let template = '';
  switch (value) {
    case ETemplateType.MobileBannerCard:
      template = 'Mobile Banner Card';
      break;
    case ETemplateType.InteractiveBanner:
      template = 'Interactive Banner';
      break;
    case ETemplateType.FlyingCarpet:
      template = 'Flying Carpet';
      break;
    case ETemplateType.InReadEcommerce:
      template = 'In Read Ecommerce';
      break;
    case ETemplateType.PostInRead:
      template = 'Post In Read';
      break;
    case ETemplateType.CatfishEcom:
      template = 'Catfish Ecom';
      break;
    case ETemplateType.NotificationBanner:
      template = 'Notification Banner';
      break;
    case ETemplateType.CatFishCollabBranding:
      template = 'CatFish Collab Branding';
      break;
    case ETemplateType.PopupBannerBranding:
      template = 'Popup Banner Branding';
      break;
  }
  return template;
}

export function convertPrice(price: number | string): number {
  return !price
    ? 0
    : typeof price === 'number'
    ? price
    : parseInt(price.replace(/,/g, ''), 10);
}
