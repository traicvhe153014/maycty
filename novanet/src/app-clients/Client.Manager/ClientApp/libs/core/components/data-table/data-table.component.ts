import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  DataTableSettingModel,
  IFieldDetail,
  ISettingColumnTable,
  ISummaryColumnTable,
} from './models';
import { NzTableComponent } from 'ng-zorro-antd/table';
import {
  debounce,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  takeUntil,
  throttleTime,
} from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { calculateHeightDataTable } from '@core/utils';
import { ESortType } from '../../enums/sort-type.enum';
import { cloneDeep } from 'lodash';
import { DOCUMENT } from '@angular/common';
import { session } from '../storage';

interface TableSortDetail {
  id: string;
  values: IFieldDetail[];
}

@Component({
  selector: ' novanet-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataTableComponent<T> implements OnInit, AfterViewInit, OnChanges {
  public readonly TemplateRef = TemplateRef;

  @ViewChild('dynamicTable', { static: false })
  nzTableComponent?: NzTableComponent<T>;

  @Input() public listOfData: readonly T[];
  @Input() public settings: DataTableSettingModel;
  @Input() public titleTemplate: TemplateRef<any>;
  @Input() public itemTemplate: TemplateRef<any>;
  @Input() public footerTemplate: TemplateRef<any>;
  @Input() public headerTemplate: TemplateRef<any>;
  @Input() public summaryTemplate: TemplateRef<any>;
  @Input() public columns: ISettingColumnTable<any, any>[];
  @Input() public columnsSearchPlaceHolder = '';
  @Input() public enableSearch = false;
  @Input() public summaryColumns: ISummaryColumnTable<any>[];
  @Input() public virtualItemSize: number;
  @Input() public loading: boolean;
  @Input() public height = 350;
  @Input() public nzNoResult: TemplateRef<any> | string;
  @Input() public cellClass = '';
  @Input() public sortDefault: string[];
  @Input() public isRemoved: boolean;

  @Output() public statusItem = new EventEmitter<{
    id: string;
    checked: boolean;
  }>();

  @Output() public checkAllItem = new EventEmitter<boolean>();
  @Output() public sortByField = new EventEmitter<IFieldDetail[]>();
  @Output() public nextPage = new EventEmitter<boolean>();
  @Output() public onSearch = new EventEmitter<string>();
  @Output() public RemoveAllSearch = new EventEmitter<void>();
  @Output() public onChangeSearchInput = new EventEmitter<string>();
  public valueSearch: string;
  public allChecked = false;
  public eSortType = ESortType;
  public scrollX: string | null = null;
  public scrollY: string | null = null;
  public sortArray: IFieldDetail[] = [];
  private destroy$ = new Subject();
  public searchChanged: Subject<string> = new Subject<string>();
  private searchChangeSubscription: Subscription;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    @Inject(DOCUMENT) private document: Document
  ) {}

  public get sortType() {
    return this.settings.sortType ?? 'multiple';
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: UIEvent) {
    if (this.settings.fixHeader) {
      this.scrollY = calculateHeightDataTable(this.height);
    }
  }

  ngOnInit(): void {
    const tableSortDetail = session.get('TableSortDetail')
      ? session.get('TableSortDetail')
      : ([] as TableSortDetail[]);

    if (!tableSortDetail) {
      session.set('TableSortDetail', []);
    }

    const existItem = tableSortDetail?.find((x) => x.id === this.settings.id);

    if (!existItem && this.settings.id) {
      tableSortDetail?.push({
        id: this.settings.id,
        values: [],
      });
      session.set('TableSortDetail', tableSortDetail);
    }

    this.settings.scrollY = cloneDeep(calculateHeightDataTable(this.height));
    this.scrollY = this.settings.fixHeader
      ? this.settings.scrollY
      : this.height + 'px';
    this.scrollX = this.settings.scrollX
      ? this.settings.scrollX
      : this.settings.fixHeader
      ? 'auto'
      : null;
    this.document.addEventListener('scroll', this.scroll, true);

    this.searchChangeSubscription = this.searchChanged
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe({
        next: (model) => {
          this.onChangeSearch(model);
        },
      });
  }

  scroll = (event): void => {
    this.changeDetectorRef.detectChanges();
    if (event.target.scrollTop * 2 + 100 >= event.target.scrollHeight) {
      const className = 'ant-table-body';
      if (event.target.className.includes(className)) {
        if (!this.loading) {
          this.nextPage.emit(true);
        } else {
          this.changeDetectorRef.detectChanges();
        }
        this.changeDetectorRef.detectChanges();
      }
    }
  };

  ngAfterViewInit(): void {
    this.nzTableComponent?.cdkVirtualScrollViewport
      ?.elementScrolled()
      .pipe(
        takeUntil(this.destroy$),
        map(() =>
          this.nzTableComponent.cdkVirtualScrollViewport.measureScrollOffset(
            'bottom'
          )
        ),
        pairwise(),
        filter(([y1, y2]) => y2 < y1 && y2 < 300),
        throttleTime(100)
      )
      .subscribe(() => {
        if (!this.loading) {
          this.nextPage.emit(true);
        } else {
          this.changeDetectorRef.detectChanges();
        }
        this.nzTableComponent.cdkVirtualScrollViewport.checkViewportSize();
        this.changeDetectorRef.detectChanges();
      });
  }

  ngOnChanges() {
    setTimeout(() => {
      if (this.nzTableComponent) {
        this.nzTableComponent.cdkVirtualScrollViewport?.checkViewportSize();
      }
    }, 150);
  }

  public refreshStatus(value: { id: string; checked: boolean }): void {
    this.statusItem.emit(value);
  }

  public checkAll(value: boolean): void {
    this.checkAllItem.emit(value);
    this.allChecked = value;
  }

  public sort(value: string, direction: ESortType): void {
    const tableSortDetail = session.get('TableSortDetail') as TableSortDetail[];

    const existSessionItem = tableSortDetail?.find(
      (x) => x.id === this.settings.id
    );

    if (this.sortType === 'multiple') {
      if (!existSessionItem && !this.settings.id) {
        const emitValue = {
          name: value,
          direction,
        } as IFieldDetail;
        const existItem = this.sortArray.find((x) => x.name === emitValue.name);
        const existIndexItem = this.sortArray.findIndex(
          (x) => x.name === emitValue.name
        );

        if (existItem) {
          if (existItem.direction === emitValue.direction) {
            this.sortArray.splice(existIndexItem, 1);
          } else {
            existItem.direction = emitValue.direction;
          }
        } else {
          this.sortArray.push(emitValue);
        }
        this.sortByField.emit(this.sortArray);
      } else {
        const emitValue = {
          name: value,
          direction,
        } as IFieldDetail;
        const existItem = existSessionItem.values.find(
          (x) => x.name === emitValue.name
        );
        const existIndexItem = existSessionItem.values.findIndex(
          (x) => x.name === emitValue.name
        );

        if (existItem) {
          if (existItem.direction === emitValue.direction) {
            existSessionItem.values.splice(existIndexItem, 1);
          } else {
            existItem.direction = emitValue.direction;
          }
        } else {
          existSessionItem.values.push(emitValue);
        }
        this.sortByField.emit(existSessionItem.values);
        session.set('TableSortDetail', tableSortDetail);
      }
    } else {
      const emitValue = {
        name: value,
        direction,
      } as IFieldDetail;
      if (!existSessionItem && !this.settings.id) {
        const existItem = this.sortArray.find((x) => x.name === emitValue.name);
        this.sortArray = existItem ? [] : [emitValue];
        this.sortByField.emit(this.sortArray);
      } else {
        const existItem = existSessionItem.values.find(
          (x) => x.name === emitValue.name
        );
        existSessionItem.values.splice(0, existSessionItem.values.length);
        if (!existItem) {
          existSessionItem.values.push(emitValue);
        }
        this.sortByField.emit(existSessionItem.values);
        session.set('TableSortDetail', tableSortDetail);
      }
    }
  }

  public isItemTemplate(item: string | TemplateRef<any>): boolean {
    return item instanceof TemplateRef;
  }

  public onChangeSearch(val: string) {
    this.onChangeSearchInput.emit(val);
  }
}
