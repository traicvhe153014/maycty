import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { LoadingIconModule } from '../loading-icon/loading-icon.module';
import { CastModule } from '../../pipes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DataTableComponent],
  imports: [CommonModule, NzTableModule, LoadingIconModule, CastModule, ReactiveFormsModule, FormsModule],
  exports: [DataTableComponent],
})
export class DataTableModule {}
