import { Component } from '@angular/core';

@Component({
  selector: 'novanet-loading-icon',
  templateUrl: './loading-icon.component.html',
  styleUrls: ['./loading-icon.component.scss'],
})
export class LoadingIconComponent {}
