export const session = {
  set(key, value) {
    try {
      sessionStorage.setItem(key, JSON.stringify(value));
    } catch (e) {}
  },

  get(key: string) {
    try {
      return JSON.parse(sessionStorage.getItem(key) || '');
    } catch (e) {}
  },

  remove(key) {
    sessionStorage.removeItem(key);
  },

  clear() {
    sessionStorage.clear();
  },
};
