import { ExportApi } from '@shared/global-notification';

export interface IHttpGetRequest {
  page?: number;
  pageSize?: number;
  skip?: number;
  keyWord?: string;
  fields?: string[];
  startDate?: string;
  endDate?: string;
  sorts?: string[];
  productIds?: string[];
  withReport?: boolean;
  withSummary?: boolean;
  status?: number;
  exportApi?: ExportApi;
}
