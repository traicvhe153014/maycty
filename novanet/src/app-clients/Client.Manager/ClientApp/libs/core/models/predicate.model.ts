import { PredicateOperatorEnum } from '@core/enums';

export interface IPredicateModel {
  field: string;
  operator: PredicateOperatorEnum;
  value: string;
}
