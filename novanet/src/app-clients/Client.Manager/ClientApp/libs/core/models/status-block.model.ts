export interface IStatusBlockConfig<T> {
  value: T;
  textColor: string;
  backgroundColor: string;
  text: string;
  tooltip?: string;
}
