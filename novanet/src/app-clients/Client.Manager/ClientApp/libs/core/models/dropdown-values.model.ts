export interface IDropdownValues {
  id: number;
  label: string;
}
