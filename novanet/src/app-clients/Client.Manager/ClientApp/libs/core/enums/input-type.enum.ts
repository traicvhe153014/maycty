export enum InputTypeEnum {
  TEXT = 'text',
  PASSWORD = 'password',
  CANCEL = 'cancel',
}

export enum TypeExpandableList {
  POPUP = 1,
}
