export enum PredicateOperatorEnum {
  Contains,
  NotContains,
  StartsWith,
  EndsWith,
  Equals,
}
