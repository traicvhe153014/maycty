import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapObject',
})
export class MapObjectPipe implements PipeTransform {
  transform(
    value: { [key: string | number | symbol]: any },
    paths?: string | string[]
  ): { key: any; value: any }[] {
    if (!paths) {
      return Object.keys(value).map((item) => {
        const parsedKey = parseInt(item);
        return {
          key: Number.isNaN(parsedKey) ? item : parsedKey,
          value: value[item],
        };
      });
    }
    let transformedPath: string[];
    if (Array.isArray(paths)) {
      transformedPath = paths;
    } else {
      transformedPath = paths.split('.');
    }
    return Object.keys(value).map((item) => {
      const parsedKey = parseInt(item);
      let result = value[item];
      for (const path of transformedPath) {
        if (!result) {
          return {
            key: Number.isNaN(parsedKey) ? item : parsedKey,
            value: result,
          };
        }
        result = result[path];
      }
      return {
        key: Number.isNaN(parsedKey) ? item : parsedKey,
        value: result,
      };
    });
  }
}
