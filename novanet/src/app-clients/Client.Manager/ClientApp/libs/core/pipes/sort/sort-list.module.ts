import { NgModule } from '@angular/core';
import { SortListPipe } from './sort-list.pipe';

@NgModule({
  declarations: [SortListPipe],
  exports: [SortListPipe],
})
export class SortListModule {}
