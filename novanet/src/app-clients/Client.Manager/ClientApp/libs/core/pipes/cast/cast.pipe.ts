import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cast',
  pure: true,
})
export class CastPipe implements PipeTransform {
  transform<T>(value: any, args?: any): T {
    return value as T;
  }
}
