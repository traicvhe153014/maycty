import { NgModule } from '@angular/core';
import { MapPipe } from './map.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [MapPipe],
  imports: [CommonModule],
  exports: [MapPipe],
})
export class MapModule {}
