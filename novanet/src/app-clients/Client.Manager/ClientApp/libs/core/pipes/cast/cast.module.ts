import { NgModule } from '@angular/core';
import { CastPipe } from './cast.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [CastPipe],
  imports: [CommonModule],
  exports: [CastPipe],
})
export class CastModule {}
