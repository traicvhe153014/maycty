import { NgModule } from '@angular/core';
import { GetTemplateNamePipe } from './get-template-name.pipe';

@NgModule({
  declarations: [GetTemplateNamePipe],
  exports: [GetTemplateNamePipe],
})
export class GetTemplateNameModule {}
