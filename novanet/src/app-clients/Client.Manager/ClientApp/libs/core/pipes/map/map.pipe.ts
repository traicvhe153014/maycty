import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'map',
})
export class MapPipe implements PipeTransform {
  transform(value: any[], paths: undefined | string | string[]): any[] {
    if (!paths) {
      return value;
    }
    let transformedPath: string[];
    if (Array.isArray(paths)) {
      transformedPath = paths;
    } else {
      transformedPath = paths.split('.');
    }
    return value.map((item) => {
      let result = item;
      for (const path of transformedPath) {
        if (!result) {
          return result;
        }
        result = result[path];
      }
      return result;
    });
  }
}
