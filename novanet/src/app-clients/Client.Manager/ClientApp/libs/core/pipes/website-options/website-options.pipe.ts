import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select';
import { map } from 'rxjs/operators';
import { IWebsitesResponse } from '@shared/settings/store';

@Pipe({
  name: 'websiteOptions',
})
export class WebsiteOptionsPipe implements PipeTransform {
  transform(
    value: Observable<IWebsitesResponse>,
    roles: string | undefined
  ): Observable<NzSelectOptionInterface[]> {
    const isAdmin = roles === 'Admin' || roles === 'PublisherManager';
    return value.pipe(
      map((data) =>
        data.data.map((item) => ({
          label:
            isAdmin && !!item.publisherEmail
              ? `${item.domain} - ${item.publisherEmail}`
              : item.domain,
          value: item.id,
        }))
      )
    );
  }
}
