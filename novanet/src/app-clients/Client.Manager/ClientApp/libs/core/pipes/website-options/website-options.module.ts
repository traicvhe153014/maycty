import { NgModule } from '@angular/core';
import { WebsiteOptionsPipe } from './website-options.pipe';

@NgModule({
  declarations: [WebsiteOptionsPipe],
  exports: [WebsiteOptionsPipe],
})
export class WebsiteOptionsModule {}
