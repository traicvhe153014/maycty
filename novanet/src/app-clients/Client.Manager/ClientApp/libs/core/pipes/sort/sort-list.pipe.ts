import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortList',
})
export class SortListPipe implements PipeTransform {
  transform(value: any[], typeSort: string | 'asc'): any[] {
    const listCampaignType = [...value];
    if (typeSort === 'desc') {
      return listCampaignType.sort();
    } else {
      return listCampaignType.sort((one, two) => (one > two ? -1 : 1));
    }
  }
}
