import { Pipe, PipeTransform } from '@angular/core';
import { defaultDateTimeFormat } from './constants';
import * as dayjs from 'dayjs';

@Pipe({
  name: 'toDateString',
  pure: true,
})
export class DateFormatterPipe implements PipeTransform {
  transform(
    value: Date | string | undefined,
    format: string = defaultDateTimeFormat
  ): string {
    if (!value) {
      return '-';
    }
    return dayjs(value).format(format);
  }
}
