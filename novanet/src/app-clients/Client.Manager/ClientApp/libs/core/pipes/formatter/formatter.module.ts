import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { CurrencyFormatterPipe } from './currency-formatter.pipe';
import { PercentageFormatterPipe } from './percentage-formatter.pipe';
import { DateFormatterPipe } from './date-formatter.pipe';

@NgModule({
  declarations: [
    CurrencyFormatterPipe,
    PercentageFormatterPipe,
    DateFormatterPipe,
  ],
  imports: [CommonModule],
  exports: [CurrencyFormatterPipe, PercentageFormatterPipe, DateFormatterPipe],
  providers: [DecimalPipe],
})
export class FormatterModule {}
