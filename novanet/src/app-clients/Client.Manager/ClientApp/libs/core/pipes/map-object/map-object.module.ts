import { NgModule } from '@angular/core';
import { MapObjectPipe } from './map-object.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [MapObjectPipe],
  imports: [CommonModule],
  exports: [MapObjectPipe],
})
export class MapObjectModule {}
