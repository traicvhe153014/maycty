import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'toCurrency',
  pure: true,
})
export class CurrencyFormatterPipe implements PipeTransform {
  constructor(private dp: DecimalPipe) {}

  transform(value: number): string {
    if (!value) {
      return '-';
    }
    return this.dp.transform(value, '1.0-0');
  }
}
