import { Pipe, PipeTransform } from '@angular/core';
import { getTemplateName } from '@core/utils';
import { ETemplateType } from '@features/campaign-management/enums';

@Pipe({
  name: 'getTemplateName',
})
export class GetTemplateNamePipe implements PipeTransform {
  transform(value: ETemplateType): string {
    return getTemplateName(value);
  }
}
