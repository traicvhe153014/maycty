import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'toPercentage',
  pure: true,
})
export class PercentageFormatterPipe implements PipeTransform {
  constructor(private dp: DecimalPipe) {}

  transform(value: number, digitInfo = '1.0-0'): string {
    if (!value) {
      return '-';
    }
    return this.dp.transform(value * 100, digitInfo);
  }
}
