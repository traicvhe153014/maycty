import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzEmptyModule } from 'ng-zorro-antd/empty';

const ANT_MODULES = [
  NzMenuModule,
  NzSelectModule,
  NzInputModule,
  NzInputNumberModule,
  NzTableModule,
  NzAffixModule,
  NzSpinModule,
  NzNotificationModule,
  NzSwitchModule,
  NzCollapseModule,
  NzCheckboxModule,
  NzButtonModule,
  NzImageModule,
  NzEmptyModule,
];

@NgModule({
  imports: [BrowserAnimationsModule, ...ANT_MODULES],
})
export class NgZorroModule {}
