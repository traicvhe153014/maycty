#!/bin/sh

dotnet restore Novanet.sln

rm -rf "src/publish/Client.Manager" && mkdir "src/publish/Client.Manager"
rm -rf "src/publish/Service.Display" && mkdir "src/publish/Service.Display"
rm -rf "src/publish/Service.Health" && mkdir "src/publish/Service.Health"
rm -rf "src/publish/Service.Identity" && mkdir "src/publish/Service.Identity"
rm -rf "src/publish/Service.Notification" && mkdir "src/publish/Service.Notification"
rm -rf "src/publish/Service.Report" && mkdir "src/publish/Service.Report"
rm -rf "src/publish/Service.Schedule" && mkdir "src/publish/Service.Schedule"
rm -rf "src/publish/Service.Settings" && mkdir "src/publish/Service.Settings"
rm -rf "src/publish/Service.Sharing" && mkdir "src/publish/Service.Sharing"
rm -rf "src/publish/Service.Storage" && mkdir "src/publish/Service.Storage"

cd "src/app-clients/Client.Manager/ClientApp" && npm run build:prod && rm -f package-lock.json
cp -a "dist/novanet-manager-prod/." "../../../publish/Client.Manager"
cd "../../../.."

dotnet build "src/services/Service.Display/Service.Display.csproj" -c Release
dotnet build "src/services/Service.Health/Service.Health.csproj" -c Release
dotnet build "src/services/Service.Identity/Service.Identity.csproj" -c Release
dotnet build "src/services/Service.Notification/Service.Notification.csproj" -c Release
dotnet build "src/services/Service.Report/Service.Report.csproj" -c Release
dotnet build "src/services/Service.Schedule/Service.Schedule.csproj" -c Release
dotnet build "src/services/Service.Settings/Service.Settings.csproj" -c Release
dotnet build "src/services/Service.Sharing/Service.Sharing.csproj" -c Release
dotnet build "src/services/Service.Storage/Service.Storage.csproj" -c Release

dotnet publish "src/services/Service.Display/Service.Display.csproj" -c Release
dotnet publish "src/services/Service.Health/Service.Health.csproj" -c Release
dotnet publish "src/services/Service.Identity/Service.Identity.csproj" -c Release
dotnet publish "src/services/Service.Notification/Service.Notification.csproj" -c Release
dotnet publish "src/services/Service.Report/Service.Report.csproj" -c Release
dotnet publish "src/services/Service.Schedule/Service.Schedule.csproj" -c Release
dotnet publish "src/services/Service.Settings/Service.Settings.csproj" -c Release
dotnet publish "src/services/Service.Sharing/Service.Sharing.csproj" -c Release
dotnet publish "src/services/Service.Storage/Service.Storage.csproj" -c Release

cp -a "src/services/Service.Display/bin/Release/net6.0/publish/." "src/publish/Service.Display"
cp -a "src/services/Service.Health/bin/Release/net6.0/publish/." "src/publish/Service.Health"
cp -a "src/services/Service.Identity/bin/Release/net6.0/publish/." "src/publish/Service.Identity"
cp -a "src/services/Service.Notification/bin/Release/net6.0/publish/." "src/publish/Service.Notification"
cp -a "src/services/Service.Report/bin/Release/net6.0/publish/." "src/publish/Service.Report"
cp -a "src/services/Service.Schedule/bin/Release/net6.0/publish/." "src/publish/Service.Schedule"
cp -a "src/services/Service.Settings/bin/Release/net6.0/publish/." "src/publish/Service.Settings"
cp -a "src/services/Service.Sharing/bin/Release/net6.0/publish/." "src/publish/Service.Sharing"
cp -a "src/services/Service.Storage/bin/Release/net6.0/publish/." "src/publish/Service.Storage"