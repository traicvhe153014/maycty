﻿#!/bin/sh

dotnet restore

cd "src/app-clients/Client.Manager/ClientApp" && npm run build:test && rm -f package-lock.json
sudo rm -rf /var/www/testv6.novanet.vn/*

cp -a "dist/novanet-manager-test/." "/var/www/testv6.novanet.vn"
cd ../../../.. || exit

sudo lsof -iTCP -sTCP:LISTEN -P | grep :5011
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5012
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5013
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5014
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5015
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5016
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5017
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5018

pm2 kill
pm2 start "dotnet src/services/Service.Display/bin/Release/net6.0/publish/Service.Display.dll --environment=test --urls=http://localhost:5011/"
pm2 start "dotnet src/services/Service.Identity/bin/Release/net6.0/publish/Service.Identity.dll --environment=test --urls=http://localhost:5012/"
pm2 start "dotnet src/services/Service.Notification/bin/Release/net6.0/publish/Service.Notification.dll --environment=test --urls=http://localhost:5013/"
pm2 start "dotnet src/services/Service.Report/bin/Release/net6.0/publish/Service.Report.dll --environment=test --urls=http://localhost:5014/"
pm2 start "dotnet src/services/Service.Schedule/bin/Release/net6.0/publish/Service.Schedule.dll --environment=test --urls=http://localhost:5015/"
pm2 start "dotnet src/services/Service.Settings/bin/Release/net6.0/publish/Service.Settings.dll --environment=test --urls=http://localhost:5016/"
pm2 start "dotnet src/services/Service.Sharing/bin/Release/net6.0/publish/Service.Sharing.dll --environment=test --urls=http://localhost:5017/"
pm2 start "dotnet src/services/Service.Storage/bin/Release/net6.0/publish/Service.Storage.dll --environment=test --urls=http://localhost:5018/"
