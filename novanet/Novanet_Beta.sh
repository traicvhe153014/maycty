﻿#!/bin/sh

dotnet restore

cd "src/app-clients/Client.Manager/ClientApp" && npm run build:beta && rm -f package-lock.json
sudo rm -rf /var/www/betav6.novanet.vn/*

cp -a "dist/novanet-manager-beta/." "/var/www/betav6.novanet.vn"
uglifyjs /var/www/betav6.novanet.vn/assets/beta-sdk.js > /var/www/betav6.novanet.vn/beta-sdk.min.js
cd ../../../.. || exit

dotnet build "src/services/Service.Display/Service.Display.csproj" -c Release
dotnet build "src/services/Service.Health/Service.Health.csproj" -c Release
dotnet build "src/services/Service.Identity/Service.Identity.csproj" -c Release
dotnet build "src/services/Service.Notification/Service.Notification.csproj" -c Release
dotnet build "src/services/Service.Report/Service.Report.csproj" -c Release
dotnet build "src/services/Service.Schedule/Service.Schedule.csproj" -c Release
dotnet build "src/services/Service.Settings/Service.Settings.csproj" -c Release
dotnet build "src/services/Service.Sharing/Service.Sharing.csproj" -c Release
dotnet build "src/services/Service.Storage/Service.Storage.csproj" -c Release

dotnet publish "src/services/Service.Display/Service.Display.csproj" -c Release
dotnet publish "src/services/Service.Health/Service.Health.csproj" -c Release
dotnet publish "src/services/Service.Identity/Service.Identity.csproj" -c Release
dotnet publish "src/services/Service.Notification/Service.Notification.csproj" -c Release
dotnet publish "src/services/Service.Report/Service.Report.csproj" -c Release
dotnet publish "src/services/Service.Schedule/Service.Schedule.csproj" -c Release
dotnet publish "src/services/Service.Settings/Service.Settings.csproj" -c Release
dotnet publish "src/services/Service.Sharing/Service.Sharing.csproj" -c Release
dotnet publish "src/services/Service.Storage/Service.Storage.csproj" -c Release

sudo lsof -iTCP -sTCP:LISTEN -P | grep :5011
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5012
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5013
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5014
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5015
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5016
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5017
sudo lsof -iTCP -sTCP:LISTEN -P | grep :5018

pm2 kill
sudo pm2 start "dotnet src/services/Service.Display/bin/Release/net6.0/publish/Service.Display.dll --environment=beta --urls=http://localhost:5011/"
sudo pm2 start "dotnet src/services/Service.Identity/bin/Release/net6.0/publish/Service.Identity.dll --environment=beta --urls='http://localhost:5012/;https://localhost:5022/'"
sudo pm2 start "dotnet src/services/Service.Notification/bin/Release/net6.0/publish/Service.Notification.dll --environment=beta --urls=http://localhost:5013/"
sudo pm2 start "dotnet src/services/Service.Report/bin/Release/net6.0/publish/Service.Report.dll --environment=beta --urls=http://localhost:5014/"
sudo pm2 start "dotnet src/services/Service.Schedule/bin/Release/net6.0/publish/Service.Schedule.dll --environment=beta --urls=http://localhost:5015/"
sudo pm2 start "dotnet src/services/Service.Settings/bin/Release/net6.0/publish/Service.Settings.dll --environment=beta --urls='http://localhost:5016/;https://localhost:5026/'"
sudo pm2 start "dotnet src/services/Service.Sharing/bin/Release/net6.0/publish/Service.Sharing.dll --environment=beta --urls='http://localhost:5017/;https://localhost:5027/'"
sudo pm2 start "dotnet src/services/Service.Storage/bin/Release/net6.0/publish/Service.Storage.dll --environment=beta --urls=http://localhost:5018/"
