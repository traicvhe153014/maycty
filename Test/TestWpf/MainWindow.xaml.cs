﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.OCR;
using Microsoft.Win32;
using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
// using OpenCvSharp.Text;
using Image = System.Windows.Controls.Image;

namespace TestWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Tesseract ocr;
        
        public MainWindow()
        {
            InitializeComponent();
            
            // Khởi tạo OCR engine
            ocr = new Tesseract("./tessdata", "eng", OcrEngineMode.Default);
            ocr.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"); // Chỉ nhận dạng các ký tự chữ cái và số
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image files (*.jpg, *.jpeg, *.png)|*.jpg;*.jpeg;*.png"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                var imagePath = openFileDialog.FileName;

                // Tạo một đối tượng hình ảnh và hiển thị nó trong một Image control
                var image = new Image()
                {
                    Source = new BitmapImage(new Uri(imagePath))
                };

                // Thêm Image control vào StackPanel
                imageStackPanel.Children.Clear();
                imageStackPanel.Children.Add(image);
                
                // Đọc hình ảnh sử dụng Emgu.CV
                Image<Rgb, byte> image2 = new Image<Rgb, byte>(imagePath);

                // Chuyển đổi hình ảnh sang grayscale để tăng cường quá trình nhận dạng
                Image<Gray, byte> grayImage = image2.Convert<Gray, byte>();

                // Áp dụng phân ngưỡng để tách biển số xe
                Image<Gray, byte> thresholdedImage = grayImage.ThresholdBinaryInv(new Gray(125), new Gray(255));

                // Tìm contour trong hình ảnh
                VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
                Mat hierachy = new Mat();
                CvInvoke.FindContours(thresholdedImage, contours, hierachy, RetrType.List, ChainApproxMethod.ChainApproxSimple);

                // Lọc các contour dựa trên diện tích và tỷ lệ chiều dài / chiều rộng
                for (int i = 0; i < contours.Size; i++)
                {
                    double area = CvInvoke.ContourArea(contours[i]);
                    Rectangle boundingRect = CvInvoke.BoundingRectangle(contours[i]);
                    double aspectRatio = (double)boundingRect.Width / boundingRect.Height;

                    if (area > 1000 && area < 10000 && aspectRatio > 2 && aspectRatio < 6)
                    {
                        // Vẽ bounding box cho biển số xe
                        CvInvoke.Rectangle(image2, boundingRect, new MCvScalar(0, 0, 255), 2);

                        // Nhận dạng biển số xe
                        string licensePlateNumber = RecognizeLicensePlate(grayImage, boundingRect);

                        Label1.Content = licensePlateNumber;
                    }
                }
                
            }

                

                // Label1.Content = "haha";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private string RecognizeLicensePlate(Image<Gray, byte> grayImage, Rectangle licensePlateRect)
        {
            // Sao chép hình ảnh vùng biển số xe từ hình ảnh gốc
            Image<Gray, byte> licensePlateImage = grayImage.Copy(licensePlateRect);
        
            // Sử dụng OCR để nhận dạng chữ trong hình ảnh vùng biển số xe
            ocr.Init("", "eng", OcrEngineMode.Default);
        
            // Chuyển đổi từ Image<Gray, byte> sang Mat
            using (Mat licensePlateMat = licensePlateImage.Mat)
            {
                ocr.SetImage(licensePlateMat);
                ocr.Recognize();
        
                var result = ocr.GetCharacters();
        
                string licensePlateNumber = string.Empty;
        
                foreach (var chr in result)
                {
                    licensePlateNumber += chr.Text;
                }
        
                return licensePlateNumber;
            }
        }
        
        // private string RecognizeLicensePlate(Mat grayImage, Rect licensePlateRect)
        // {
        //     // Sao chép phần biển số xe từ hình ảnh grayscale
        //     Mat licensePlateImage = grayImage[licensePlateRect];
        //
        //     // Khởi tạo OCR engine
        //     using (OCRTesseract ocr = OCRTesseract.Create())
        //     {
        //         ocr.Run(licensePlateImage, null, OCRRecognizeType.Text);
        //         string licensePlateNumber = ocr.GetUTF8Text();
        //
        //         return licensePlateNumber;
        //     }
        // }
    }
}