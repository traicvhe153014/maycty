﻿namespace RandomWord // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("so text: ");
                var number = Convert.ToInt32(Console.ReadLine());
                var full = "";
                for (int i = 0; i < number; i++)
                {
                    Random random = new Random();
                    string word = "";
                    for (int j = 0; j < random.Next(1, 8); j++)
                    {
                        char randomChar = (char)('a' + random.Next(0, 26)); // Random một chữ cái ngẫu nhiên
                        word += randomChar.ToString(); // Thêm chữ cái vào chuỗi
                    }
                    word = i != 0 ? " " + word : "" + word;
                    full += word;
                    if (random.Next(1, 8)==word.Length||i==number-1)
                    {
                        full += ".";
                    }
                    else
                    {
                        if (random.Next(1, 8)==word.Length)
                        {
                            full += ",";
                        }
                    }
                }
                Console.WriteLine(" " + full);
            }
        }
    }
}