﻿using System.Resources;
using System.Text.Json.Serialization;
using CRUD_Redis.Commons;
using CRUD_Redis.Infastructure.Caching;
using CRUD_Redis.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CRUD_Redis.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ICachingService _cache;

    public HomeController(ICachingService cache)
    {
        _cache = cache;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(int id)
    {
        var listTodo = await _cache.GetRecordAsync<List<ToDo>>(TableEnum.ToDoes.ToString());
        if (listTodo is null)
        {
            return NotFound();
        }

        var todo = listTodo.FirstOrDefault(x => x.Id == id);
        return Ok(todo);
    }
    [HttpGet("All")]
    public async Task<IActionResult> GetAll()
    {
        var listTodo = await _cache.GetRecordAsync<List<ToDo>>(TableEnum.ToDoes.ToString());
        if (listTodo is null)
        {
            return NotFound();
        }
        return Ok(listTodo);
    }
    [HttpGet("Length")]
    public async Task<IActionResult> GetLength()
    {
        var listTodo = await _cache.GetRecordAsync<List<ToDo>>(TableEnum.ToDoes.ToString());
        if (listTodo is null)
        {
            return NotFound();
        }
        return Ok(listTodo.Count);
    }
    [HttpPost("CreateToDo")]
    public async Task<IActionResult> CreateToDo(ToDo todo)
    {
        var listTodo = await _cache.GetRecordAsync<List<ToDo>>(TableEnum.ToDoes.ToString()) ?? new List<ToDo>();
        listTodo.Add(todo);
        await _cache.SetRecordAsync(TableEnum.ToDoes.ToString(), listTodo);
        return Ok(listTodo.TakeLast(10));
    }
    [HttpPost("CreateData")]
    public async Task<IActionResult> CreateData(int number)
    {

        var listTodo = await _cache.GetRecordAsync<List<ToDo>>(TableEnum.ToDoes.ToString()) ?? new List<ToDo>();
        var id = listTodo[listTodo.Count - 1].Id;
        for (int i = 0; i < number; i++)
        {
            var todo = new ToDo() { Id = id + i, Name = id + "traicv" };
            listTodo.Add(todo);
        }

        await _cache.SetRecordAsync(TableEnum.ToDoes.ToString(), listTodo);
        return Ok(listTodo.TakeLast(10));
    }
}