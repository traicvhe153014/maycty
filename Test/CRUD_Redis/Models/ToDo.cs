﻿namespace CRUD_Redis.Models;

public class ToDo
{
    public int Id { get; set; }
    public string Name { get; set; }
}