﻿// See https://aka.ms/new-console-template for more information

using System.Text;

namespace tool_coursera_random_document // Note: actual namespace depends on the project name.
{
    
    internal class Program
    {
        private static Random random = new Random();
        
        static void Main(string[] args)
        {
            var checkNext = "";
            do
            {
                Console.OutputEncoding = Encoding.Unicode;
                Console.WriteLine("số chữ: ");
                int lettersNumber = Int32.Parse(Console.ReadLine());
                var document = "";
                for (int i = 0; i < lettersNumber; i++)
                {
                    int card = random.Next(2,7);
                    var textRandom = RandomString(card).ToLower();
                    document += textRandom + " ";
                    // if (i % 15 == 0)
                    // {
                    //     document += Environment.NewLine;
                    // }
                }
                
                Console.WriteLine(document);
                
                Console.WriteLine("tiếp tục(y/n): ");
                checkNext = Console.ReadLine();
            } while (checkNext.ToLower().Contains("y"));
        }

        private static string RandomString(int lenght)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, lenght)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}