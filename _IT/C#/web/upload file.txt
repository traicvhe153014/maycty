___________up 1 file____________(view: <form enctype="multipart/form-data" asp-action="/nguoidung">)
if (myfile != null)
{
   string fullPath = Path.Combine(Directory.GetCurrentDirectory(), 
        "wwwroot", "myfiles", myfile.FileName);
   using( var file = new FileStream(fullPath, FileMode.Create))
   {
        myfile.CopyTo(file);
    }
}

__________delete 1 file____________
string imgPath = Path.Combine(Directory.GetCurrentDirectory(),
        "wwwroot", "myfiles", user.ImgAvar);
FileInfo fi = new FileInfo(imgPath);
if (fi != null)
{
 System.IO.File.Delete(imgPath);
fi.Delete();
}


___________________________________________________
[HttpPost]
        public IActionResult DoThongtincanhan(IFormFile myfile)
        {
            string jsonStr = HttpContext.Session.GetString("useraccount");
            User user = null;
            if (jsonStr != null) user = JsonConvert.DeserializeObject<User>(jsonStr);

            if (myfile == null)
            {
                ViewBag.thongbao = "chua co file";
                return View("/Views/User/Profile.cshtml");
            }

            //add img
            var newfilename = Guid.NewGuid();
            var _extension = Path.GetExtension(myfile.FileName);
            string _FileName = newfilename + _extension;
            string fullPath = Path.Combine(Directory.GetCurrentDirectory(),
                    "wwwroot", "myfiles", _FileName);
            using (var file = new FileStream(fullPath, FileMode.Create))
            {
                myfile.CopyTo(file);
            }

            // delete img 
            string imgPath = Path.Combine(Directory.GetCurrentDirectory(),
                    "wwwroot", "myfiles", user.ImgAvar);
            FileInfo fi = new FileInfo(imgPath);
            if (fi != null)
            {
                System.IO.File.Delete(imgPath);
                fi.Delete();
            }
            // lưu name file vao database
            user.ImgAvar = _FileName;
            context.Users.Update(user);
            context.SaveChanges();

            // set session mới
            string jsonStr2 = JsonConvert.SerializeObject(user);
            HttpContext.Session.SetString("useraccount", jsonStr2);

            ViewBag.thongbao = "co file";
            return View("/Views/User/Profile.cshtml");
        }