﻿using De1_B1.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace De1_B1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly PRN_Sum22_B1Context _context;

        public CustomerController(PRN_Sum22_B1Context context)
        {
            _context = context;
        }

        // DELETE api/<CustomerController>/5
        [HttpPost("{CustomerId}")]
        public IActionResult Delete(string CustomerId)
        {
            try
            {
                var customer = _context.Customers.FirstOrDefault(x => x.CustomerId == CustomerId);
                if (customer != null)
                {
                    var listOrders = _context.Orders.Where(x => x.CustomerId == CustomerId);
                    var countlistOrders = 0;
                    var countlistOrderDetail = 0;
                    if (listOrders != null)
                    {
                        countlistOrders = listOrders.Count();
                        _context.Orders.RemoveRange(listOrders);
                        var listOrderDetail = _context.OrderDetails.Where(x => listOrders.FirstOrDefault(y => y.OrderId == x.OrderId) != null);
                        if (listOrders != null)
                        {
                            countlistOrderDetail = listOrderDetail.Count();
                            _context.OrderDetails.RemoveRange(listOrderDetail);
                        }
                    }
                    _context.Customers.Remove(customer);
                    _context.SaveChanges();
                    return Ok(new
                    {
                        customerDeletedCount = 1,
                        orderDeletedCount = countlistOrders,
                        orderDetailDeletedCount = countlistOrderDetail
                    });
                }
                return NotFound();
            }
            catch 
            {
                return Conflict();
            }
            
        }
    }
}
