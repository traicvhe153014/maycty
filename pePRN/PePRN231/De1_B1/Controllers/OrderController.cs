﻿using De1_B1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace De1_B1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly PRN_Sum22_B1Context _context;

        public OrderController(PRN_Sum22_B1Context context)
        {
            _context = context;
        }

        // GET api/<OrderController>/5
        [HttpGet]
        public IActionResult GetAllOrder()
        {
            var listOrder = _context.Orders;
            return Ok(listOrder);
        }
        // GET api/<OrderController>/5
        [HttpGet("{From}/{To}")]
        public IActionResult GetOrderByDate(DateTime From, DateTime To)
        {
            var listOrder = _context.Orders
                .Include(x => x.Customer)
                .Include(x => x.Employee)
                .ThenInclude(x => x!.Department)
                .Where(x => x.OrderDate >= From && x.OrderDate <= To)
                .Select(x => new
                {
                    orderId = x.OrderId,
                    customerId = x.CustomerId,
                    customerName = x.Customer!.ContactName,
                    employeeId = x.EmployeeId,
                    employeeName = x.Employee!.FirstName + x.Employee!.LastName,
                    employeeDepartmentId = x.Employee!.DepartmentId,
                    employeeDepartmentName = x.Employee!.Department!.DepartmentName,
                    orderDate = x.OrderDate,
                    requiredDate = x.RequiredDate,
                    shippedDate = x.ShippedDate,
                    freight = x.Freight,
                    shipName = x.ShipName,
                    shipAddress = x.ShipAddress,
                    shipCity = x.ShipCity,
                    shipRegion = x.ShipRegion,
                    shipPostalCode = x.ShipPostalCode,
                    shipCountry = x.ShipCountry,
                });
            return Ok(listOrder);
        }
    }
}
