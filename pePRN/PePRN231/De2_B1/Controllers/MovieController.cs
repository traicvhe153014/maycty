﻿using De2_B1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace De2_B1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly PE_PRN231_Trial_02Context _context;

        public MovieController(PE_PRN231_Trial_02Context context)
        {
            _context = context;
        }
        // DELETE api/<MovieController>/5
        [HttpDelete("{movieId}/{starId}")]
        public IActionResult RemoveStarFromMovie(int movieId, int starId)
        {
            try
            {
                var movie = _context.Movies
                    .Include(x => x.Stars)
                    .Include(x=>x.Genres)
                    .FirstOrDefault(x => x.Id == movieId && x.Stars.FirstOrDefault(y => y.Id == starId) != null);
                if (movie != null)
                { 
                    movie.Stars.Clear();
                    movie.Genres.Clear();
                    _context.Movies.Remove(movie);
                    _context.SaveChanges();
                    return Ok();
                }
                else
                {
                    if (_context.Movies.FirstOrDefault(x => x.Id == movieId)==null)
                    {
                        return NotFound("The requested movie could not be found.");
                    }
                    return NotFound("The requested actor could not be found in the list of actors of the requested movie.");
                }
                
            }
            catch
            {
                return Conflict();
            }
        }
    }
}
