﻿using De2_B1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace De2_B1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DirectorController : ControllerBase
    {
        private readonly PE_PRN231_Trial_02Context _context;

        public DirectorController(PE_PRN231_Trial_02Context context)
        {
            _context = context;
        }

        // GET api/<DirectorController>/5
        [HttpGet("{nationality}/{gender}")]
        public IActionResult GetDirectors(string nationality, string gender)
        {
            var male = false;
            if (gender == "Male") { male = true; }
            var listDirectors = _context.Directors.Where(x => x.Nationality == nationality && x.Male == male)
                .ToList();
            if (listDirectors.Count > 0)
            {
                return Ok(listDirectors.Select(x => new
                {
                    id = x.Id,
                    fullName = x.FullName,
                    gender = x.Male ? "Male" : "Female",
                    dob = x.Dob,
                    dobString = x.Dob.ToString("d/M/yyyy"),
                    nationality = x.Nationality,
                    description = x.Description,
                }));
            }
            return Ok(listDirectors);
        }

        [HttpGet("{id}")]
        public IActionResult GetDirector(int id)
        {
            var derector = _context.Directors
                .Include(x => x.Movies)
                .ThenInclude(x=>x.Genres)
                .Include(x=>x.Movies)
                .ThenInclude(x=>x.Stars)
                .FirstOrDefault(x => x.Id == id);
            
            if (derector != null)
            {
                var movie = derector.Movies
                    .Select(x => new
                    {
                        id = x.Id,
                        releaseDate = x.ReleaseDate,
                        genres = x.Genres,
                        stars = x.Stars
                    })
                    .ToList();

                return Ok(new
                {
                    id = derector.Id,
                    fullName = derector.FullName,
                    gender = derector.Male ? "Male" : "Female",
                    dob = derector.Dob,
                    dobString = derector.Dob.ToString("d/M/yyyy"),
                    nationality = derector.Nationality,
                    description = derector.Description,
                    movie = derector.Movies
                    .Select(x => new
                    {
                        id = x.Id,
                        releaseDate = x.ReleaseDate,
                        releaseYear = ((DateTime)x.ReleaseDate!).Year,
                        genres = x.Genres.Select(y=>new
                        {
                            id=y.Id,
                            title=y.Title
                        }),
                        stars = x.Stars.Select(y => new
                        {
                            id = y.Id,
                            FullName = y.FullName,
                            Male = y.Male,
                            Dob = y.Dob,
                            Description = y.Description,
                            Nationality = y.Nationality
                        })
                    })
                    .ToList()
                });
            }
            else
            {
                return NotFound();
            }

        }

        // POST api/<DirectorController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<DirectorController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<DirectorController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
