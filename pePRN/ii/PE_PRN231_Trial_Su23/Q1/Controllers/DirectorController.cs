﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Q1.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Q1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DirectorController : ControllerBase
    {
        private readonly PE_PRN_Fall22B1Context _context;

        public DirectorController(PE_PRN_Fall22B1Context context)
        {
            _context = context;
        }
        // GET api/<DirectorController>/5
        [HttpGet("{nationality}/{gender}")]
        public IActionResult GetDirectors(string nationality, string gender)
        {
            var male = false;
            if (gender.ToLower() == "male") { male = true; } else
            {
                if (gender.ToLower() != "female") { return Ok(); }
            }
            var listDirectors = _context.Directors.Where(x => x.Nationality == nationality && x.Male == male)
                .ToList();
            if (listDirectors.Count > 0)
            {
                return Ok(listDirectors.Select(x => new directorRespones
                {
                    id = x.Id,
                    fullName = x.FullName,
                    gender = x.Male ? "Male" : "Female",
                    dob = x.Dob,
                    dobString = x.Dob.ToString("d/M/yyyy"),
                    nationality = x.Nationality,
                    description = x.Description,
                }));
            }
            return Ok(listDirectors);
        }

        [HttpGet("{id}")]
        public IActionResult GetDirector(int id)
        {
            var derector = _context.Directors
                .Include(x => x.Movies)
                .ThenInclude(x => x.Genres)
                .Include(x => x.Movies)
                .ThenInclude(x => x.Stars)
                .FirstOrDefault(x => x.Id == id);

            if (derector != null)
            {
                var movie = derector.Movies
                    .Select(x => new
                    {
                        id = x.Id,
                        releaseDate = x.ReleaseDate,
                        genres = x.Genres,
                        stars = x.Stars
                    })
                    .ToList();

                return Ok(new
                {
                    id = derector.Id,
                    fullName = derector.FullName,
                    gender = derector.Male ? "Male" : "Female",
                    dob = derector.Dob,
                    dobString = derector.Dob.ToString("d/M/yyyy"),
                    nationality = derector.Nationality,
                    description = derector.Description,
                    movie = derector.Movies
                    .Select(x => new
                    {
                        id = x.Id,
                        releaseDate = x.ReleaseDate,
                        releaseYear = ((DateTime)x.ReleaseDate!).Year,
                        genres = x.Genres.Select(y => new
                        {
                            id = y.Id,
                            title = y.Title
                        }),
                        stars = x.Stars.Select(y => new
                        {
                            id = y.Id,
                            FullName = y.FullName,
                            Male = y.Male,
                            Dob = y.Dob,
                            Description = y.Description,
                            Nationality = y.Nationality
                        })
                    })
                    .ToList()
                });
            }
            else
            {
                return NotFound();
            }

        }

        [HttpPost]
        public IActionResult Create([FromBody] directorRequest director)
        {
            try
            {
                var d = new Director()
                {
                    FullName = director.FullName,
                    Male = director.male,
                    Dob = director.Dob,
                    Nationality = director.Nationality,
                    Description = director.Description,
                };
                _context.Directors.Add(d);
                _context.SaveChanges();
                return Ok(1);
            }
            catch
            {
                return Conflict("There is an error while adding");
            }
            
        }
    }
}

public class directorRequest{
    public string FullName { get; set; }
    public bool male { get; set; }
    public DateTime Dob { get; set; }
    public string Nationality { get; set; }
    public string Description { get; set; }
}
public class directorRespones
{
    public int id { get; set; }
    public string fullName { get; set; }
    public string gender { get; set; }
    public string dobString { get; set; }
    public DateTime dob { get; set; }
    public string nationality { get; set; }
    public string description { get; set; }
}