﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Q1.Models;
using System.Net.Http.Headers;
using System.Text.Json;

namespace Q2.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly HttpClient client = null;
        private string ServiceMangaUrl = "";
        public List<directorRespones> listDirector { get; set; } = null!;
        [BindProperty]
        public string? nationality { get; set; }
        [BindProperty]
        public string? gender { get; set; }

        public IndexModel(ILogger<IndexModel> logger)
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            ServiceMangaUrl = "http://localhost:5000/";
            _logger = logger;
        }

        public async Task OnGet()
        {
            //HttpResponseMessage response = await client.GetAsync(ServiceMangaUrl + "api/Director/GetDirectors/USA/Male");
            //if (response.IsSuccessStatusCode)
            //{
            //    string responseBody = await response.Content.ReadAsStringAsync();
            //    var option = new JsonSerializerOptions()
            //    { PropertyNameCaseInsensitive = true };
            //    listDirector = JsonSerializer.Deserialize<List<directorRespones>>(responseBody, option);
            //}
            //listDirector = null;
        }

        public async Task OnPost()
        {
            if (gender != null && nationality != null)
            {
                //nationality = "USA";
                //gender = "male";
                HttpResponseMessage response = await client.GetAsync(ServiceMangaUrl + "api/Director/GetDirectors/" + nationality + "/" + gender);
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    var option = new JsonSerializerOptions()
                    { PropertyNameCaseInsensitive = true };
                    listDirector = JsonSerializer.Deserialize<List<directorRespones>>(responseBody, option);
                }
            }
            else { listDirector = null; }

        }
    }
}