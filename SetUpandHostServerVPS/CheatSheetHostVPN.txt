--copy file trong folder vao CentOS
scp -r folderName root@103.82.27.9:/home/vagrant/

---Run file .sh(install.sh)
chmod +x install.sh
./install.sh

--truy cập bằng ssh
ssh root@103.82.27.9

--check version
dotnet --version
systemctl status mssql-server
systemctl status nginx

-- start/restart
systemctl start nginx
systemctl restart nginx

-- firewall
sudo firewall-cmd --list-ports
systemctl restart firewalld
firewall-cmd --zone=public --add-port=22/tcp --permanent
systemctl stop firewalld

--- service
systemctl enable ManagerVPS
systemctl start ManagerVPS
systemctl restart ManagerVPS
systemctl stop Schedule
systemctl disable Schedule

systemctl status ManagerVPS
systemctl status FileManager
systemctl status BrokerApi
systemctl status WorkerVPS

-- step create service
step1:
	vào thư mục /etc/systemd/system/
step2:
	tạo file service vd: ManagerVPS.service

-- app conext to server vps
FileZilla
vscode (ssh remote)

---- server

IPv4:
103.82.133.210
Username:
root
Password:
k9HbZ8458t0eJHq7

------messages log server
var/log/messages